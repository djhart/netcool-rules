--###############################################################################
--#
--#    Licensed Materials - Property of IBM
--#    "Restricted Materials of IBM"
--#
--#    5724-S44
--#
--#    (C) Copyright IBM Corp. 2005, 2012
--#
--#    Netcool Knowledge Library
--#
--#############################################################################
--#
--# 1.1 - Updated Release.
--#
--#         Update to include commands to remove index and conversions.
--#
--# 1.0 - Initial Release.
--#
--#############################################################################
--#
--# This script is used to uninstall the AdvCorr correlations and related
--# objects from an OMNIbus 7.x ObjectServer. It removes:
--#   - Automations (triggers and a trigger group)
--#   - Additional Fields in alerts.status
--#   - Additional Tables in the alerts database
--#   - Additional Index in the catalog database
--#   - Additional Conversions in the catalog database
--#
--# To remove the AdvCorr correlation support run one of the following
--# platform-dependent commands:
--#
--# Unix: 
--#   $OMNIHOME/bin/nco_sql �server <objectserver_name> �user <username>
--#   �password <password> < <path_to_file>/removeadvcorr.sql
--#
--# Windows:
--#   %OMNIHOME%\bin\redist\isql.exe -S <objectserver name> -U <username> -P
--#   <password> -i <path_to_file>\removeadvcorr.sql
--#
--#############################################################################

-- Remove triggers
drop trigger AdvCorr_SetCauseType;
drop trigger AdvCorr_LPC_RC;
drop trigger AdvCorr_LPC_Sym;
go

-- Remove trigger group
drop trigger group AdvCorr;
go

-- Remove tables
drop table alerts.AdvCorrLpcSymCand;
drop table alerts.AdvCorrLpcRcCand;
go

-- Remove required fields from alerts.status
-- alter table alerts.status drop column LocalTertObj;
-- alter table alerts.status drop column LocalObjRelate;
-- alter table alerts.status drop column RemoteTertObj;
-- alter table alerts.status drop column RemoteObjRelate;
-- alter table alerts.status drop column CorrScore;
-- alter table alerts.status drop column CauseType;
-- alter table alerts.status drop column AdvCorrCauseType;
-- alter table alerts.status drop column AdvCorrServerName varchar(64);
-- alter table alerts.status drop column AdvCorrSerialSerial int;
-- go

-- Remove index
-- drop index localobjrelateIdx;
-- go

-- Remove conversions
-- delete from alerts.conversions where Colname = 'AdvCorrCauseType'
-- delete from alerts.conversions where Colname = 'CauseType'
-- go
