###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
###############################################################################
#
# 1.6 - Updated Release.
#
#     - Set NmosEventMap field with event map name and precedence value. The
#       entries in NcoGateInserts.cfg file are migrated into NcKL rules and
#       this file will be deprecated. Entries for deprecated or unsupported 
#       traps in NcKL are excluded.
#
# 1.5 - The details function is now disabled by default to reduce table queries.
#       NcKL now uses the nvp_add function to store additional data in the
#       ExtendedAttr column in alerts.status table. Refer to
#       OMNIbus best practices guide for more info.
#
#     - If the customer needs to use the details function, set the
#       global flag $OPTION_EnableDetails to "1" or set the
#       $OPTION_EnableDetails_vendor to "1" to enable details for specific
#       vendor rules file only.
#
# 1.4 - Added Juniper Junos Syslog rules.
#
# 1.3 - Removed Contrib from rules Directory
#
# 1.2 - Updated handling of ProbeWatch messages.
#
#     - Added basic debug logging.
#
#     - 2005/03/07 Robert Cowart
#
# 1.1 - Added comments for OMNIbus 3.6 compatibility and CIC-specific
#       modifcations.
#
# 1.0 - Initial Release.
#
###############################################################################

table syslogSrcType = "$NC_RULES_HOME/syslog-SrcType.lookup"
default = "Unknown"

table syslogCorrScore = "$NC_RULES_HOME/include-syslog/CorrScore.syslog.lookup"
default = "0"
table syslogPreClass = "$NC_RULES_HOME/include-syslog/PreClass.syslog.lookup"
default = "0"

table ciscoIosDefaults = "$NC_RULES_HOME/include-syslog/cisco-ios/cisco-ios-defaults.syslog.lookup"
default = {"Unknown","Unknown"}

table cisco-ios_sev = "$NC_RULES_HOME/include-syslog/cisco-ios.sev.syslog.lookup"
default = {"Unknown","Unknown","Unknown"}

include "$NC_RULES_HOME/include-syslog/juniper-junos.sev.syslog.lookup"

###############################################################################
# Enter lookup table Includes below with the following syntax:
#
# include "<$NCHOME>/etc/rules/include-syslog/<lookuptable>.include.lookup"
###############################################################################

include "$NC_RULES_HOME/include-common/IANA.include.common.lookup"



###############################################################################
# End of lookup table Includes
###############################################################################


###############################################################################
# Set the following option to "1" to use details() for debugging
# or set $OPTION_EnableDetails_<vendor> = "1" to enable details() for specifc
# vendor rules only.
# Example: $OPTION_EnableDetails_ibm = "1"
###############################################################################
$OPTION_EnableDetails = "0"


if(match(@Manager, "ProbeWatch"))
{
    $ProbeName = @Agent
    $ProbeStatus = @Summary
    
    @Agent = "ProbeWatch"
    
    @Node = hostname()
    
    @AlertGroup = "Probe Status"
    @AlertKey = "Probe: " + $ProbeName + ", Host: " + hostname() + ", ObjectServer: " + @ServerName
    @Summary = "Probe " + $ProbeStatus + "  ( " + @AlertKey + " )"
    switch($ProbeStatus)
    {
        case "Running ...":
            @Severity = 1
            @Type = 2
        case "Going Down ...":
            @Severity = 5
            @Type = 1
        default:
            @Severity = 2
            @Type = 1
    }
    @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + @Type + " " + @Agent + " " + @Manager + " " + $ProbeStatus
}
else
{
    log(DEBUG, "<<<<< Entering... syslog.rules >>>>>")

    #######################################################################
    # Set default Manager, Agent, Class and Identifier
    #######################################################################
    
    @Manager = "Syslog Probe on " + hostname()
    @Agent = "syslog"
    @Class = "200"
    @Identifier = $Token4 + " " + $Details  
        
    #######################################################################
    # End of default Manager, Agent, Class and Identifier
    #######################################################################

    if(match($Details, ""))
    {
        discard
    }    
    else if(regmatch($Details, "[Mm]essage [Rr]epeat"))
    {
        discard
    }
    else
    {
        @Node = $Token4
        
        $SrcKey = $Token4
        
        ##########
        # Add any logic necessary to modify $SrcKey before the source type
        # lookup is performed.  Primarily this is to convert IP Addresses
        # encoded as the snmpUDPAddress textual convention in [RFC1906] to
        # a common 4 octet IP Address.  Additional logic may however be added
        # if necessary.
        ##########
        
        if(regmatch($Token4, "^\[[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\."))
        {
            $SrcKey = extract($Token4, "^\[([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)\.")
        }
        
        ##########
        # End of $SrcKey modification logic.
        ##########
        
        if(regmatch($SrcKey, "^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$"))
        {
            @Node = $SrcKey
            @NodeAlias = $SrcKey
        }
        
        $SrcType = lookup($SrcKey, syslogSrcType)
        
        
        if(match($SrcType, "Unknown")) ### If the source type is unknown we will try some pattern matching to make a best guess.
        {
            if(match($Details, "dummy if statement")){} ### This provides the initial "if" statement for the "else if" statements included below.
            
            ###################################################################
            # When the source type of a message cannot be determined by a
            # lookup in the syslogSrcType the regmatch rules file Includes
            # attempt to make a "best guess" using regular expression pattern
            # matching.
            #
            # Enter regmatch rules file Includes below with the following
            # syntax:
            #
            # include "<$NCHOME>/etc/rules/include-syslog/regmatch/<rulesfile>
            # .regmatch.include.rules"
            ###################################################################
        
            include "$NC_RULES_HOME/include-syslog/regmatch/cisco-ios.regmatch.include.syslog.rules"
            include "$NC_RULES_HOME/include-syslog/regmatch/juniper-junos.regmatch.include.syslog.rules"
            
            ###################################################################
            # End of regmatch rules file Includes
            ###################################################################
        }
        
        
        switch($SrcType)
        {
            case "dummy case statement": ### This will prevent syntax errors in case no includes are added below.
            
            ###################################################################
            # Enter rules file Includes below with the following syntax:
            #
            # include "<$NCHOME>/etc/rules/include-syslog/<rulesfile>.include
            # .rules"
            ###################################################################
            
            
            include "$NC_RULES_HOME/include-syslog/cisco-ios.include.syslog.rules"
            include "$NC_RULES_HOME/include-syslog/juniper-junos.include.syslog.rules"
            
            ###################################################################
            # End of rules file Includes
            ###################################################################
            
            default:
            
                @AlertGroup = "[Generic Syslog]"
                @AlertKey = ""
                @Summary = $Details
                @Severity = 1
                @Type = 1
                @Identifier = @Node + " " + @AlertGroup + " " + @Manager + " " + $Details
                
                if(match($OPTION_EnableDetails, "1")) {
                    details($*)
                }
                @ExtendedAttr = nvp_add($*)
        }
    }
    
    $OS_LocalNodeAlias = @Node
}

##########
# The following include statement is required by Netcool's advanced correlation
# logic.
##########

include "$NC_RULES_HOME/include-syslog/CorrScore.include.syslog.rules"
include "$NC_RULES_HOME/include-syslog/PreClass.include.syslog.rules"

##########
# End of advanced correlation include files.
##########

##########
# Enter "compatibility" includes below with the following syntax:
#
# include "<$NCHOME>/etc/rules/include-compat/<rulesfile>.include.compat.rules"
##########

include "$NC_RULES_HOME/include-compat/omnibus36.include.compat.rules"
include "$NC_RULES_HOME/include-compat/AdvCorr36.include.compat.rules"

##########
# End of "compatibility" includes.
##########

log(DEBUG, "<<<<< Leaving... syslog.rules >>>>>")
