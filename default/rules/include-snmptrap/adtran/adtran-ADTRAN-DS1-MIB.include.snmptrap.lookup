###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  ADTRAN-DS1-MIB
#
###############################################################################

table adDS1LineEvent =
{
    {"1","No Alarm Present"}, ### 1 - No Alarm Present 
    {"2","Far-End Loss of Frame"}, ### 2 - Far end LOF ( a.k.a., Yellow Alarm ) 
    {"4","Near-End Sending Loss of Frame"}, ### 4 - Near end sending LOF Indication 
    {"8","Far-End Sending AIS"}, ### 8 - Far end sending AIS ( Blue Alarm ) 
    {"16","Near-End Sending AIS"}, ### 16 - Near end sending AIS 
    {"32","Near-End Loss of Frame"}, ### 32 - Near end LOF ( a.k.a., Red Alarm )  
    {"64","Near-End Loss of Signal"}, ### 64 - Near end Loss Of Signal 
    {"128","Near-End Looped"}, ### 128 - Near end is looped 
    {"256","E1 TS16 AIS"}, ### 256 - E1 TS16 AIS 
    {"512","Far-End Sending TS16 LOMF"}, ### 512 - Far End Sending TS16 LOMF 
    {"1024","Near-End Sending TS16 LOMF"}, ### 1024 - Near End Sending TS16 LOMF 
    {"2048","Near-End Detects Test Code"}, ### 2048 - Near End detects a test code 
    {"4096","Other"} ### 4096 - any line status not defined here
}
default = "Unknown"

table adDS1CurrentAlert =
{
    {"1","ES","Errored Seconds"}, ### 1 - ES, Errored Seconds 
    {"2","SES","Severely Errored Seconds"}, ### 2 - SES, Severely Errored Seconds 
    {"4","SEFS","Severely Errored Framing Seconds"}, ### 4 - SEFS, Severely Errored Framing Seconds 
    {"8","UAS","Unavailable Seconds"}, ### 8 - UAS, Unavailable Seconds 
    {"16","CSS","Controlled Slip Seconds"}, ### 16 - CSS, Controlled Slip Seconds 
    {"32","PCV","Path Coding Violations"}, ### 32 - PCV, Path Coding Violations 
    {"64","LES","Line Errored Seconds"}, ### 64 - LES, Line Errored Seconds 
    {"128","BES","Bursty Errored Seconds"}, ### 128 - BES, Bursty Errored Seconds 
    {"256","DM","Degraded Minutes"}, ### 256 - DM, Degraded Minutes 
    {"512","LCV","Line Code Violations"} ### 512 - LCV, Line Code Violations
}
default = {"Unknown","Unknown"}

table adDS1TotalAlert =
{
    {"1","ES","Errored Seconds"}, ### 1 - ES, Errored Seconds 
    {"2","SES","Severely Errored Seconds"}, ### 2 - SES, Severely Errored Seconds 
    {"4","SEFS","Severely Errored Framing Seconds"}, ### 4 - SEFS, Severely Errored Framing Seconds 
    {"8","UAS","Unavailable Seconds"}, ### 8 - UAS, Unavailable Seconds 
    {"16","CSS","Controlled Slip Seconds"}, ### 16 - CSS, Controlled Slip Seconds 
    {"32","PCV","Path Coding Violations"}, ### 32 - PCV, Path Coding Violations 
    {"64","LES","Line Errored Seconds"}, ### 64 - LES, Line Errored Seconds 
    {"128","BES","Bursty Errored Seconds"}, ### 128 - BES, Bursty Errored Seconds 
    {"256","DM","Degraded Minutes"}, ### 256 - DM, Degraded Minutes 
    {"512","LCV","Line Code Violations"} ### 512 - LCV, Line Code Violations
}
default = {"Unknown","Unknown"}

table adDS1FarCurrentAlert =
{
    {"1","ES","Errored Seconds"}, ### 1 - ES, Errored Seconds 
    {"2","SES","Severely Errored Seconds"}, ### 2 - SES, Severely Errored Seconds 
    {"4","SEFS","Severely Errored Framing Seconds"}, ### 4 - SEFS, Severely Errored Framing Seconds 
    {"8","UAS","Unavailable Seconds"}, ### 8 - UAS, Unavailable Seconds 
    {"16","CSS","Controlled Slip Seconds"}, ### 16 - CSS, Controlled Slip Seconds 
    {"32","PCV","Path Coding Violations"}, ### 32 - PCV, Path Coding Violations 
    {"64","LES","Line Errored Seconds"}, ### 64 - LES, Line Errored Seconds 
    {"128","BES","Bursty Errored Seconds"}, ### 128 - BES, Bursty Errored Seconds 
    {"256","DM","Degraded Minutes"}, ### 256 - DM, Degraded Minutes 
    {"512","LCV","Line Code Violations"} ### 512 - LCV, Line Code Violations
}
default = {"Unknown","Unknown"}

table adDS1FarTotalAlert =
{
    {"1","ES","Errored Seconds"}, ### 1 - ES, Errored Seconds 
    {"2","SES","Severely Errored Seconds"}, ### 2 - SES, Severely Errored Seconds 
    {"4","SEFS","Severely Errored Framing Seconds"}, ### 4 - SEFS, Severely Errored Framing Seconds 
    {"8","UAS","Unavailable Seconds"}, ### 8 - UAS, Unavailable Seconds 
    {"16","CSS","Controlled Slip Seconds"}, ### 16 - CSS, Controlled Slip Seconds 
    {"32","PCV","Path Coding Violations"}, ### 32 - PCV, Path Coding Violations 
    {"64","LES","Line Errored Seconds"}, ### 64 - LES, Line Errored Seconds 
    {"128","BES","Bursty Errored Seconds"}, ### 128 - BES, Bursty Errored Seconds 
    {"256","DM","Degraded Minutes"}, ### 256 - DM, Degraded Minutes 
    {"512","LCV","Line Code Violations"} ### 512 - LCV, Line Code Violations
}
default = {"Unknown","Unknown"}
