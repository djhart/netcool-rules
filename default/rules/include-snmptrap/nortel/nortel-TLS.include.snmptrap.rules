###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  nortel-TLS
#
###############################################################################

case ".1.3.6.1.4.1.562.15.3.1": ### Nortel Transparent LAN Service - Notifications from TLS (991011)

    log(DEBUG, "<<<<< Entering... nortel-TLS.include.snmptrap.rules >>>>>")

    @Agent = "Nortel-Transparent LAN Service"
    @Class = "70000"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "10": ### uniOnRBDTrap

            ##########
            # $1 = tlsTDIfIndex 
            # $2 = tlsTDIfTagValue 
            ##########

            $tlsTDIfIndex = $1
            $tlsTDIfTagValue = $2

            $OS_EventId = "SNMPTRAP-nortel-TLS-uniOnRBDTrap"

            @AlertGroup = "UNI Status"
            @AlertKey = "tlsTDIfEntry." + $tlsTDIfIndex + "." + $tlsTDIfTagValue 
            @Summary = "User to Network Interface On Ring-Broadcast-Domain  ( " + @AlertKey + " )"

            $DEFAULT_Severity = 1 
            $DEFAULT_Type = 2 
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_nortel, "1")) {
                details($tlsTDIfIndex,$tlsTDIfTagValue)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "tlsTDIfIndex", $tlsTDIfIndex, "tlsTDIfTagValue", $tlsTDIfTagValue)

        case "20": ### uniOffRBDTrap

            ##########
            # $1 = tlsTDIfIndex 
            # $2 = tlsTDIfTagValue 
            ##########

            $tlsTDIfIndex = $1
            $tlsTDIfTagValue = $2

            $OS_EventId = "SNMPTRAP-nortel-TLS-uniOffRBDTrap"

            @AlertGroup = "UNI Status"
            @AlertKey = "tlsTDIfEntry." + $tlsTDIfIndex + "." + $tlsTDIfTagValue 
            @Summary = "User to Network Interface Off Ring-Broadcast-Domain  ( " + @AlertKey + " )"

            $DEFAULT_Severity = 2 
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_nortel, "1")) {
                details($tlsTDIfIndex,$tlsTDIfTagValue)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "tlsTDIfIndex", $tlsTDIfIndex, "tlsTDIfTagValue", $tlsTDIfTagValue)

        case "30": ### craftPortEnabledTrap

            ##########
            # $1 = tlsIfIndex 
            ##########

            $tlsIfIndex = $1

            $OS_EventId = "SNMPTRAP-nortel-TLS-craftPortEnabledTrap"

            @AlertGroup = "LAN Port Status"
            @AlertKey = "tlsIfEntry." + $tlsIfIndex 
            @Summary = "LAN Port Changed to Craft Port  ( " + @AlertKey + " )"

            $DEFAULT_Severity = 2 
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_nortel, "1")) {
                details($tlsIfIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "tlsIfIndex", $tlsIfIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_nortel, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, nortel-TLS_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, nortel-TLS_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/nortel/nortel-TLS.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/nortel/nortel-TLS.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... nortel-TLS.include.snmptrap.rules >>>>>")
