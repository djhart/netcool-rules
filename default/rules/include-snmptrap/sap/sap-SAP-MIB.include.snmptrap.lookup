###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  SAP-MIB
#
###############################################################################

table R3alertClass =
{
    {"1","Class State Change"}, ### classStateChange - Indicates a state change if an instance services of an instance have changed
    {"2","Class SAP Sys Up"}, ### classSAPsysUp - R3 instance up
    {"3","Class SAP Sys Down"}, ### classSAPsysDown - R3 instance shut down
    {"10","Class System Log Id"}, ### classSlogId - critical systemlog occurred
    {"11","Class System Log Frequent Write"}, ### classSlogFreq - frequent writes into systemlog
    {"12","Class Buffers"}, ### classBuf - local buffers alert, directory or storage shortage, bad efficiency
    {"13","Class Enqueue"}, ### classEnqueue - lock facility alert, directory or storage shortage, errors occurred
    {"14","Class Roll Page File"}, ### classRollpag - rollfile, paging file shortage
    {"15","Class Trace"}, ### classTrace - trace on, may cause reduced performance
    {"16","Class Dispatch Queue"}, ### classDpQueue - shortage in the request queues of an application server
    {"20","Class Performance Dialog"}, ### classPerfDia - dialog task performance alert
    {"21","Class Performance Update"}, ### classPerfUpd - update task performance alert
    {"22","Class Performance Background"}, ### classPerfBtc - background task performance alert
    {"23","Class Performance Spool"}, ### classPerfSpo - spool task performance alert
    {"30","Class ABAP Update"}, ### classAbapUpd - error in update processing
    {"31","Class ABAP Error"}, ### classAbapErr - other error
    {"32","Class ABAP SQL"}, ### classAbapSql - error during database access
    {"41","Class Db Indices"}, ### classDbIndcs - missing indices
    {"42","Class Db Free Space"}, ### classDbFreSp - free space shortage
    {"43","Class Db Archive Stuck"}, ### classDbArcSt - archiver stuck
    {"44","Class Db Backup"}, ### classDbBckup - backup failed
    {"51","Class Spool"}, ### classSpo - spooler subsystem alert, exceeding time or spool database shortage
    {"52","Class Archive"}, ### classArch - archive subsystem alert, archive-link
    {"53","Class Gen P3"}, ### classGenP3 - reserved for further use
    {"54","Class Gen P4"}, ### classGenP4 - reserved for further use
    {"55","Class Gen P5"}, ### classGenP5 - reserved for further use
    {"56","Class Gen P6"}, ### classGenP6 - reserved for further use
    {"57","Class Gen P7"}, ### classGenP7 - reserved for further use
    {"58","Class Gen P8"} ### classGenP8 - reserved for further use
}
default = "Unknown"

table R3alertSeverity =
{
    {"1","Normal"}, ### severityNormal - Severity Normal
    {"2","Warning"}, ### severityWarning - Severity Warning
    {"3","Critical"} ### severityCritical - Severity Critical
}
default = "Unknown"

table R3maAlertValue =
{
    {"1","Normal"}, ### valueNormal - Value Normal
    {"2","Warning"}, ### valueWarning - Value Warning
    {"3","Critical"}, ### valueCritical - Value Critical
    {"4","Unknown"} ### valueUnknown - Value Unknown
}
default = "Unknown"

table R3maiAlertRating =
{
    {"0","Unknown"}, ### valueUnknown - Value Unknown
    {"1","Normal"}, ### valueNormal - Value Normal
    {"2","Warning"}, ### valueWarning - Value Normal
    {"3","Critical"} ### valueCritical - Value Critical
}
default = "Unknown"
