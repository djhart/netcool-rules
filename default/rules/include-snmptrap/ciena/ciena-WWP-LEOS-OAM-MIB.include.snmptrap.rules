###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 2.0 - Updated to support notifications from WWP-LEOS-OAM-MIB (200801030000Z)
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  WWP-LEOS-OAM-MIB
#
###############################################################################

case ".1.3.6.1.4.1.6141.2.60.400.1.3": ###  - Notifications from WWP-LEOS-OAM-MIB (200801030000Z)

    log(DEBUG, "<<<<< Entering... ciena-WWP-LEOS-OAM-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Ciena-WWP-LEOS-OAM-MIB"
    @Class = "40506"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### wwpLeosOamLinkEventTrap

            ##########
            # $1 = wwpLeosOamEventLogPort 
            # $2 = wwpLeosOamEventLogType 
            # $3 = wwpLeosOamEventLogLocation 
            ##########

            $wwpLeosOamEventLogPort = $1
            $wwpLeosOamEventLogType = lookup($2, WwpLeosOamEventLogType)
            $wwpLeosOamEventLogLocation = lookup($3, WwpLeosOamEventLogLocation) 

            $OS_EventId = "SNMPTRAP-ciena-WWP-LEOS-OAM-MIB-wwpLeosOamLinkEventTrap"

            @AlertGroup = "Local / Remote Link Event Status"
            $wwpLeosOamEventLogIndex = extract($OID1, "\.([0-9]+)$")
            @AlertKey = "wwpLeosOamEventLogEntry." + $wwpLeosOamEventLogPort + "." + $wwpLeosOamEventLogIndex

            switch($2)
            {
                case "1":### erroredSymbolEvent
                    $SEV_KEY = $OS_EventId + "_erroredSymbolEvent"

                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
 
                case "2":### erroredFramePeriodEvent
                    $SEV_KEY = $OS_EventId + "_erroredFramePeriodEvent"

                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
 
                case "3":### erroredFrameEvent
                    $SEV_KEY = $OS_EventId + "_erroredFrameEvent"

                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
 
                case "4":### erroredFrameSecondsEvent
                    $SEV_KEY = $OS_EventId + "_erroredFrameSecondsEvent"

                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
 
                case "256":### linkFault
                    $SEV_KEY = $OS_EventId + "_linkFault"

                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
 
                case "257":### dyingGaspEvent
                    $SEV_KEY = $OS_EventId + "_dyingGaspEvent"

                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "258":### criticalLinkEvent
                    $SEV_KEY = $OS_EventId + "_criticalLinkEvent"

                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
    
                default:
                    $SEV_KEY = $OS_EventId + "_unknown"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }
            
            if(match($3, "1"))
            {
                @Summary = "Local Link Event is Detected and Recorded in the wwpLeosOamEventLogTable" + " ( " + @AlertKey + " ) "
            }
            else       
            if(match($3, "2"))
            {
                @Summary = "Remote Link Event is Detected and Recorded in the wwpLeosOamEventLogTable" + " ( " + @AlertKey + " ) "
            }
            else
            {
                @Summary = "Local or Remote Link Event is Detected and Recorded in the wwpLeosOamEventLogTable" + " ( " + @AlertKey + " ) "
            }
            
            update(@Summary)
            update(@Severity)
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            $wwpLeosOamEventLogType = $wwpLeosOamEventLogType + " ( " + $2 + " ) "
            $wwpLeosOamEventLogLocation = $wwpLeosOamEventLogLocation + " ( " + $3 + " ) "
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpLeosOamEventLogPort,$wwpLeosOamEventLogType,$wwpLeosOamEventLogLocation,$wwpLeosOamEventLogIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpLeosOamEventLogPort", $wwpLeosOamEventLogPort, "wwpLeosOamEventLogType", $wwpLeosOamEventLogType, "wwpLeosOamEventLogLocation", $wwpLeosOamEventLogLocation,
                 "wwpLeosOamEventLogIndex", $wwpLeosOamEventLogIndex)
			
        case "2": ### wwpLeosOamLinkLostTimerActiveTrap

            ##########
            # $1 = wwpLeosOamPort 
            # $2 = wwpLeosEtherPortDesc 
            # $3 = wwpLeosEtherPortOperStatus 
            # $4 = wwpLeosOamOperStatus 
            # $5 = wwpLeosOamPeerStatus 
            # $6 = wwpLeosOamPeerMacAddress 
            ##########

            $wwpLeosOamPort = $1
            $wwpLeosEtherPortDesc = $2
            $wwpLeosEtherPortOperStatus = lookup($3, WwpLeosEtherPortOperStatus)
            $wwpLeosOamOperStatus = lookup($4, WwpLeosOamOperStatus)
            $wwpLeosOamPeerStatus = lookup($5, WwpLeosOamPeerStatus)
            $wwpLeosOamPeerMacAddress = $6
			
			$wwpLeosEtherPortId = extract($OID2, "\.([0-9]+)$")
			$wwpLeosOamLocalPort = extract($OID5, "\.([0-9]+)$")
			
            $OS_EventId = "SNMPTRAP-ciena-WWP-LEOS-OAM-MIB-wwpLeosOamLinkLostTimerActiveTrap"

            @AlertGroup = "Local / Remote Link Event Status"
            @AlertKey =  "wwpLeosOamEntry." + $wwpLeosOamPort + " , wwpLeosEtherPortEntry." + $wwpLeosEtherPortId + " , wwpLeosOamPeerEntry." + $wwpLeosOamLocalPort
            @Summary = "Link Lost Timer Active, Port : " + $wwpLeosOamPort + " ( " + @AlertKey + " ) "

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

			$wwpLeosEtherPortOperStatus = $wwpLeosEtherPortOperStatus + " ( " + $3 + " ) "
			$wwpLeosOamOperStatus = $wwpLeosOamOperStatus + " ( " + $4 + " ) "
			$wwpLeosOamPeerStatus = $wwpLeosOamPeerStatus + " ( " + $5 + " ) "
			
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpLeosOamPort,$wwpLeosEtherPortDesc,$wwpLeosEtherPortOperStatus,$wwpLeosOamOperStatus,$wwpLeosOamPeerStatus,$wwpLeosOamPeerMacAddress,$wwpLeosEtherPortId,$wwpLeosOamLocalPort)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpLeosOamPort", $wwpLeosOamPort, "wwpLeosEtherPortDesc", $wwpLeosEtherPortDesc, "wwpLeosEtherPortOperStatus", $wwpLeosEtherPortOperStatus,
                 "wwpLeosOamOperStatus", $wwpLeosOamOperStatus, "wwpLeosOamPeerStatus", $wwpLeosOamPeerStatus, "wwpLeosOamPeerMacAddress", $wwpLeosOamPeerMacAddress,
                 "wwpLeosEtherPortId", $wwpLeosEtherPortId, "wwpLeosOamLocalPort", $wwpLeosOamLocalPort)

        case "3": ### wwpLeosOamLinkLostTimerExpiredTrap

            ##########
            # $1 = wwpLeosOamPort 
            # $2 = wwpLeosEtherPortDesc 
            # $3 = wwpLeosEtherPortOperStatus 
            # $4 = wwpLeosOamOperStatus 
            # $5 = wwpLeosOamPeerStatus 
            # $6 = wwpLeosOamPeerMacAddress 
            ##########

            $wwpLeosOamPort = $1
            $wwpLeosEtherPortDesc = $2
            $wwpLeosEtherPortOperStatus = lookup($3, WwpLeosEtherPortOperStatus)
            $wwpLeosOamOperStatus = lookup($4, WwpLeosOamOperStatus)
            $wwpLeosOamPeerStatus = lookup($5, WwpLeosOamPeerStatus)
            $wwpLeosOamPeerMacAddress = $6
			
			$wwpLeosEtherPortId = extract($OID2, "\.([0-9]+)$")
			$wwpLeosOamLocalPort = extract($OID5, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-ciena-WWP-LEOS-OAM-MIB-wwpLeosOamLinkLostTimerExpiredTrap"

            @AlertGroup = "Local / Remote Link Event Status"
            @AlertKey =  "wwpLeosOamEntry." + $wwpLeosOamPort + " , wwpLeosEtherPortEntry." + $wwpLeosEtherPortId + " , wwpLeosOamPeerEntry." + $wwpLeosOamLocalPort
            @Summary = "Link Lost Timer Expired, Port : " + $wwpLeosOamPort + " ( " + @AlertKey + " ) "

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0
			
			$wwpLeosEtherPortOperStatus = $wwpLeosEtherPortOperStatus + " ( " + $3 + " ) "
			$wwpLeosOamOperStatus = $wwpLeosOamOperStatus + " ( " + $4 + " ) "
			$wwpLeosOamPeerStatus = $wwpLeosOamPeerStatus + " ( " + $5 + " ) "

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpLeosOamPort,$wwpLeosEtherPortDesc,$wwpLeosEtherPortOperStatus,$wwpLeosOamOperStatus,$wwpLeosOamPeerStatus,$wwpLeosOamPeerMacAddress,$wwpLeosEtherPortId,$wwpLeosOamLocalPort)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpLeosOamPort", $wwpLeosOamPort, "wwpLeosEtherPortDesc", $wwpLeosEtherPortDesc, "wwpLeosEtherPortOperStatus", $wwpLeosEtherPortOperStatus,
                 "wwpLeosOamOperStatus", $wwpLeosOamOperStatus, "wwpLeosOamPeerStatus", $wwpLeosOamPeerStatus, "wwpLeosOamPeerMacAddress", $wwpLeosOamPeerMacAddress,
                 "wwpLeosEtherPortId", $wwpLeosEtherPortId, "wwpLeosOamLocalPort", $wwpLeosOamLocalPort)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, ciena-WWP-LEOS-OAM-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, ciena-WWP-LEOS-OAM-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-WWP-LEOS-OAM-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-WWP-LEOS-OAM-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... ciena-WWP-LEOS-OAM-MIB.include.snmptrap.rules >>>>>")
