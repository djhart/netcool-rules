###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CIENA-CES-PORT-XCVR-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table ciena-CIENA-CES-PORT-XCVR-MIB_sev =
{
    {"SNMPTRAP-ciena-CIENA-CES-PORT-XCVR-MIB-cienaCesPortXcvrRemovedNotification","2","13","1800"},
    {"SNMPTRAP-ciena-CIENA-CES-PORT-XCVR-MIB-cienaCesPortXcvrInsertedNotification","2","13","1800"},
    {"SNMPTRAP-ciena-CIENA-CES-PORT-XCVR-MIB-cienaCesPortXcvrErrorTypeNotification","3","1","0"},
    {"SNMPTRAP-ciena-CIENA-CES-PORT-XCVR-MIB-cienaCesPortXcvrTempHighNotification","3","1","0"},
    {"SNMPTRAP-ciena-CIENA-CES-PORT-XCVR-MIB-cienaCesPortXcvrTempLowNotification","3","1","0"},
    {"SNMPTRAP-ciena-CIENA-CES-PORT-XCVR-MIB-cienaCesPortXcvrTempNormalNotification","1","2","0"},
    {"SNMPTRAP-ciena-CIENA-CES-PORT-XCVR-MIB-cienaCesPortXcvrVoltageHighNotification","3","1","0"},
    {"SNMPTRAP-ciena-CIENA-CES-PORT-XCVR-MIB-cienaCesPortXcvrVoltageLowNotification","3","1","0"},
    {"SNMPTRAP-ciena-CIENA-CES-PORT-XCVR-MIB-cienaCesPortXcvrVoltageNormalNotification","1","2","0"},
    {"SNMPTRAP-ciena-CIENA-CES-PORT-XCVR-MIB-cienaCesPortXcvrBiasHighNotification","3","1","0"},
    {"SNMPTRAP-ciena-CIENA-CES-PORT-XCVR-MIB-cienaCesPortXcvrBiasLowNotification","3","1","0"},
    {"SNMPTRAP-ciena-CIENA-CES-PORT-XCVR-MIB-cienaCesPortXcvrBiasNormalNotification","1","2","0"},
    {"SNMPTRAP-ciena-CIENA-CES-PORT-XCVR-MIB-cienaCesPortXcvrTxPowerHighNotification","3","1","0"},
    {"SNMPTRAP-ciena-CIENA-CES-PORT-XCVR-MIB-cienaCesPortXcvrTxPowerLowNotification","3","1","0"},
    {"SNMPTRAP-ciena-CIENA-CES-PORT-XCVR-MIB-cienaCesPortXcvrTxPowerNormalNotification","1","2","0"},
    {"SNMPTRAP-ciena-CIENA-CES-PORT-XCVR-MIB-cienaCesPortXcvrRxPowerHighNotification","3","1","0"},
    {"SNMPTRAP-ciena-CIENA-CES-PORT-XCVR-MIB-cienaCesPortXcvrRxPowerLowNotification","3","1","0"},
    {"SNMPTRAP-ciena-CIENA-CES-PORT-XCVR-MIB-cienaCesPortXcvrRxPowerNormalNotification","1","2","0"},
    {"SNMPTRAP-ciena-CIENA-CES-PORT-XCVR-MIB-cienaCesPortXcvrSpeedInfoMissingNotification","3","1","0"}
}
default = {"Unknown","Unknown","Unknown"}
