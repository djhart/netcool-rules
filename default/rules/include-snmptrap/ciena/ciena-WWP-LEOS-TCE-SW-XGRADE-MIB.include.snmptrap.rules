###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  WWP-LEOS-TCE-SW-XGRADE-MIB
#
###############################################################################

case ".1.3.6.1.4.1.6141.2.61.14.2": ### - Notifications from WWP-LEOS-TCE-SW-XGRADE-MIB (200304211700Z)

    log(DEBUG, "<<<<< Entering... ciena-WWP-LEOS-TCE-SW-XGRADE-MIB.include.snmptrap.rules >>>>>")

    @Agent = "ciena-WWP-LEOS-TCE-SW-XGRADE-MIB"
    @Class = "40506"
    
    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
     
        case "1": ### wwpLeosTceSwXgradeCompletion
        
            ##########
            # $1 = wwpLeosTceSwXgradeOp
            # $2 = wwpLeosTceSwXgradeStatus
            # $3 = wwpLeosTceSwXgradeGracefulUpgrade
            ##########
            
            $wwpLeosTceSwXgradeOp = lookup($1, WwpLeosTceSwXgradeOp)
            $wwpLeosTceSwXgradeStatus = lookup($2, WwpLeosTceSwXgradeStatus)
            $wwpLeosTceSwXgradeGracefulUpgrade = lookup($3, TruthValue)
            
            $OS_EventId = "SNMPTRAP-ciena-WWP-LEOS-TCE-SW-XGRADE-MIB-wwpLeosTceSwXgradeCompletion"

            @AlertGroup = "WWP Leos TCE Software Upgrade Status"
            @Summary = "WWP Leos TCE Software Upgrade Status : " + $wwpLeosTceSwXgradeStatus + ", Operation : " + $wwpLeosTceSwXgradeOp
            
            switch($2)
            {
                case "1":### success
                    $SEV_KEY = $OS_EventId + "_success"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0               
                case "2":### failed
                    $SEV_KEY = $OS_EventId + "_failed"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "3":### unknown
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "4":### processing
                    $SEV_KEY = $OS_EventId + "_processing"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800               
                case "5":### invalidCfgRule
                    $SEV_KEY = $OS_EventId + "_invalidCfgRule"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "6":### invalidFileName
                    $SEV_KEY = $OS_EventId + "_invalidFileName"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "7":### fileSystemError
                    $SEV_KEY = $OS_EventId + "_fileSystemError"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "8":### cannotResolveHostName
                    $SEV_KEY = $OS_EventId + "_cannotResolveHostName"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "9":### tftpClientTimeout
                    $SEV_KEY = $OS_EventId + "_tftpClientTimeout"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "10":### tftpServerError
                    $SEV_KEY = $OS_EventId + "_tftpServerError"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "11":### tftpBadTag
                    $SEV_KEY = $OS_EventId + "_tftpBadTag"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "12":### tftpBadValue
                    $SEV_KEY = $OS_EventId + "_tftpBadValue"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "13":### networkError
                    $SEV_KEY = $OS_EventId + "_networkError"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "14":### platformTypeNotSupported
                    $SEV_KEY = $OS_EventId + "_platformTypeNotSupported"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "15":### swMgrBusy
                    $SEV_KEY = $OS_EventId + "_swMgrBusy"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "16":### needBackupSw
                    $SEV_KEY = $OS_EventId + "_needBackupSw"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "17":### internalError
                    $SEV_KEY = $OS_EventId + "_internalError"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "18":### fileNotExist
                    $SEV_KEY = $OS_EventId + "_fileNotExist"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "19":### missingAttribute
                    $SEV_KEY = $OS_EventId + "_missingAttribute"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "20":### invalidXgradeOp
                    $SEV_KEY = $OS_EventId + "_invalidXgradeOp"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "21":### noDefaultTftpConfigured
                    $SEV_KEY = $OS_EventId + "_noDefaultTftpConfigured"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                default: 
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = "WWP Leos TCE Software Upgrade Status Is Unknown : " + $wwpLeosTceSwXgradeStatus + ", Operation : " + $wwpLeosTceSwXgradeOp
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }
                     
            update(@Severity)
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $wwpLeosTceSwXgradeOp = $wwpLeosTceSwXgradeOp + " ( " + $1 + " )"
            $wwpLeosTceSwXgradeStatus = $wwpLeosTceSwXgradeStatus + " ( " + $2 + " )"
            $wwpLeosTceSwXgradeGracefulUpgrade = $wwpLeosTceSwXgradeGracefulUpgrade + " ( " + $3 + " )"
            
             if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                 details($wwpLeosTceSwXgradeOp, $wwpLeosTceSwXgradeStatus, $wwpLeosTceSwXgradeGracefulUpgrade)
             }
             @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpLeosTceSwXgradeOp", $wwpLeosTceSwXgradeOp, "wwpLeosTceSwXgradeStatus", $wwpLeosTceSwXgradeStatus, "wwpLeosTceSwXgradeGracefulUpgrade", $wwpLeosTceSwXgradeGracefulUpgrade)
         
        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, ciena-WWP-LEOS-TCE-SW-XGRADE-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, ciena-WWP-LEOS-TCE-SW-XGRADE-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-WWP-LEOS-TCE-SW-XGRADE-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-WWP-LEOS-TCE-SW-XGRADE-MIB.user.include.snmptrap.rules"


##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... ciena-WWP-LEOS-TCE-SW-XGRADE-MIB.include.snmptrap.rules >>>>>")


