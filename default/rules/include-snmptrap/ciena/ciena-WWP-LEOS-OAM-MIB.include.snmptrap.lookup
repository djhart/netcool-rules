###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 2.0 - Updated to support notifications from WWP-LEOS-OAM-MIB (200801030000Z)
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  WWP-LEOS-OAM-MIB
#
###############################################################################

table WwpLeosOamEventLogLocation =
{
    {"1","Local"}, ### local
    {"2","Remote"} ### remote
}
default = "Unknown"

table WwpLeosOamEventLogType =
{
    {"1","Errored Symbol Event"}, ### erroredSymbolEvent
    {"2","Errored Frame Period Event"}, ### erroredFramePeriodEvent
    {"3","Errored Frame Event"}, ### erroredFrameEvent
    {"4","Errored Frame Seconds Event"}, ### erroredFrameSecondsEvent
    {"256","Link Fault"}, ### linkFault   
    {"257","Dying Gasp Event"}, ### dyingGaspEvent    
    {"258","Critical Link Event"} ### criticalLinkEvent   
}
default = "Unknown"

table WwpLeosOamOperStatus =
{
    ##########
    # This table indicates the status of the OAM initialization and failure conditions
    ##########
	
    {"1","Disabled"}, ### disabled
    {"2","Link Fault"}, ### linkfault
    {"3","Passive Wait"}, ### passiveWait
    {"4","Active Sent Local"}, ### activeSendLocal
    {"5","Send Local And Remote"}, ### sendLocalAndRemote   
    {"6","Send Local And Remote Okay"}, ### sendLocalAndRemoteOk    
    {"7","OAM Peering Locally Rejected"}, ### oamPeeringLocallyRejected
	{"8","OAM Peering Remotely Rejected"}, ### oamPeeringRemotelyRejected
	{"9","Operational"} ### operational
}
default = "Unknown"


table WwpLeosOamPeerStatus =
{
    ##########
    # This table indicates the validity of the information in the row
    ##########
	
    {"1","Active"}, ### active
    {"2","Inactive"} ### inactive
}
default = "Unknown"
