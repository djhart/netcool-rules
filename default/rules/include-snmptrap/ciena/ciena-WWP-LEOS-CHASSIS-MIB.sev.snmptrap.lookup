###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  WWP-LEOS-CHASSIS-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table ciena-WWP-LEOS-CHASSIS-MIB_sev =
{
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisPowerSupplyStatusNotification_online","1","2","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisPowerSupplyStatusNotification_offline","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisPowerSupplyStatusNotification_faulted","4","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisPowerSupplyStatusNotification_unknown","2","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisFanModuleNotification_ok","1","2","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisFanModuleNotification_pending","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisFanModuleNotification_failure","4","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisFanModuleNotification_unknown","2","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisTempNotification_higherThanThreshold","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisTempNotification_normal","1","2","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisTempNotification_lowerThanThreshold","4","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisTempNotification_unknown","2","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisPowerSwitchNotification","2","13","1800"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisBatteryStatusNotification_online","1","2","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisBatteryStatusNotification_present","1","2","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisBatteryStatusNotification_missing","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisBatteryStatusNotification_unknown","2","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisBatteryVoltageLevelNotification_normal","1","2","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisBatteryVoltageLevelNotification_low","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisBatteryVoltageLevelNotification_unknown","2","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisBatteryConditionNotification_good","1","2","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisBatteryConditionNotification_bad","4","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisBatteryConditionNotification_unknown","2","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisPowerSourceNotification","2","13","1800"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisPostErrorNotification","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisRebootNotification_snmp","2","13","1800"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisRebootNotification_powerFailure","4","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisRebootNotification_appload","2","13","1800"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisRebootNotification_errorHandler","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisRebootNotification_watchDog","2","13","1800"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisRebootNotification_upgrade","2","13","1800"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisRebootNotification_cli","2","13","1800"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisRebootNotification_resetButton","2","13","1800"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisRebootNotification_serviceModeChange","2","13","1800"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisRebootNotification_unknown","2","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisSnmpStateNotification_disabled","4","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisSnmpStateNotification_enabled","1","2","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisSnmpStateNotification_unknown","2","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisDyingGaspNotification","4","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisDoorStatusChangeNotification_none_none","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisDoorStatusChangeNotification_none_open","2","2","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisDoorStatusChangeNotification_none_closed","4","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisDoorStatusChangeNotification_none_override","2","13","1800"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisDoorStatusChangeNotification_none_unknown","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisDoorStatusChangeNotification_open_none","2","2","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisDoorStatusChangeNotification_open_open","1","2","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisDoorStatusChangeNotification_open_closed","2","2","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisDoorStatusChangeNotification_open_override","2","2","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisDoorStatusChangeNotification_open_unknown","2","2","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisDoorStatusChangeNotification_closed_none","4","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisDoorStatusChangeNotification_closed_open","2","2","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisDoorStatusChangeNotification_closed_closed","4","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisDoorStatusChangeNotification_closed_override","4","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisDoorStatusChangeNotification_closed_unknown","4","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisDoorStatusChangeNotification_unknown_none","2","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisDoorStatusChangeNotification_unknown_open","2","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisDoorStatusChangeNotification_unknown_closed","2","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisDoorStatusChangeNotification_unknown_override","2","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisDoorStatusChangeNotification_unknown_unknown","2","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisInnerDoorStatusChangeNotification_none","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisInnerDoorStatusChangeNotification_open","1","2","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisInnerDoorStatusChangeNotification_closed","4","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisInnerDoorStatusChangeNotification_override","2","13","1800"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisInnerDoorStatusChangeNotification_unknown","2","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisOuterDoorStatusChangeNotification_none","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisOuterDoorStatusChangeNotification_open","1","2","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisOuterDoorStatusChangeNotification_closed","4","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisOuterDoorStatusChangeNotification_unknown","2","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisExternalAlarmStatusChangeNotification_raised","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisExternalAlarmStatusChangeNotification_cleared","1","2","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-CHASSIS-MIB-wwpLeosChassisExternalAlarmStatusChangeNotification_unknown","2","1","0"}
}
default = {"Unknown","Unknown","Unknown"}
