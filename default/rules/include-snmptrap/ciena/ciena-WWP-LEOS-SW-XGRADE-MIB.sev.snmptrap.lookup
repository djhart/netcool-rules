###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 2.0 - Update to support notifications from WWP-LEOS-SW-XGRADE-MIB (200712260000z)
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  WWP-LEOS-SW-XGRADE-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table ciena-WWP-LEOS-SW-XGRADE-MIB_sev =
{
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwDownloadCompletion_downloadSuccess","1","2","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwDownloadCompletion_invalidPkgFile","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwDownloadCompletion_couldNotGetFile","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwDownloadCompletion_tftpServerNotFound","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwDownloadCompletion_cmdFileParseError","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwDownloadCompletion_internalFilesystemError","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwDownloadCompletion_flashOffline","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwDownloadCompletion_noStatus","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwDownloadCompletion_badFileCrc","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwDownloadCompletion_alreadyUpgradeMode","2","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwDownloadCompletion_unknownError","2","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwDownloadCompletion_unknown","2","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeOpCompletion_none","2","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeOpCompletion_processing","2","13","1800"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeOpCompletion_success","1","2","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeOpCompletion_failure","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeOpCompletion_unknown","2","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeBladePkgIncorrect","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeCompletion_success","1","2","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeCompletion_failed","4","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeCompletion_unknown","2","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeCompletion_processing","2","13","1800"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeCompletion_invalidCfgRule","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeCompletion_invalidFileName","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeCompletion_fileSystemError","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeCompletion_cannotResolveHostName","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeCompletion_tftpClientTimeout","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeCompletion_tftpServerError","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeCompletion_tftpBadTag","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeCompletion_tftpBadValue","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeCompletion_networkError","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeCompletion_platformTypeNotSupported","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeCompletion_swMgrBusy","2","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeCompletion_needBackupSw","2","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeCompletion_internalError","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeCompletion_fileNotExist","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeCompletion_missingAttribute","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeCompletion_invalidXgradeOp","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-SW-XGRADE-MIB-wwpLeosSwXgradeCompletion_noDefaultTftpConfigured","3","1","0"},
}
default = {"Unknown","Unknown","Unknown"}
