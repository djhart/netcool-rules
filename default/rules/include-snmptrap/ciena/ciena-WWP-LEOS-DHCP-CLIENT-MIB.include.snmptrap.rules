###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  WWP-LEOS-DHCP-CLIENT-MIB
#
###############################################################################

case ".1.3.6.1.4.1.6141.2.60.17.2": ###  - Notifications from WWP-LEOS-DHCP-CLIENT-MIB (200104031700Z)

    log(DEBUG, "<<<<< Entering... ciena-WWP-LEOS-DHCP-CLIENT-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Ciena-WWP-LEOS-DHCP-CLIENT-MIB"
    @Class = "40506"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### wwpLeosDhcpClientOptionDisabledNotification

            ##########
            # $1 = wwpLeosDhcpOptionCode 
            ##########

            $wwpLeosDhcpOptionCode = $1
            $wwpLeosDhcpOptionCodeIndex = extract($OID1, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-ciena-WWP-LEOS-DHCP-CLIENT-MIB-wwpLeosDhcpClientOptionDisabledNotification"

            @AlertGroup = "WWP Leos DHCP Client Option Status"
            @AlertKey = "wwpLeosDhcpClientOptionEntry." + $wwpLeosDhcpOptionCodeIndex 
            @Summary = "WWP Leos DHCP Client Option Is Disabled, Option Code : " + $wwpLeosDhcpOptionCode + " ( " + @AlertKey + " )"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpLeosDhcpOptionCode,$wwpLeosDhcpOptionCodeIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpLeosDhcpOptionCode", $wwpLeosDhcpOptionCode, "wwpLeosDhcpOptionCodeIndex", $wwpLeosDhcpOptionCodeIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, ciena-WWP-LEOS-DHCP-CLIENT-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, ciena-WWP-LEOS-DHCP-CLIENT-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-WWP-LEOS-DHCP-CLIENT-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-WWP-LEOS-DHCP-CLIENT-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... ciena-WWP-LEOS-DHCP-CLIENT-MIB.include.snmptrap.rules >>>>>")
