###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CIENA-CES-RSTP-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table ciena-CIENA-CES-RSTP-MIB_sev =
{
    {"SNMPTRAP-ciena-CIENA-CES-RSTP-MIB-cienaCesRstpPortBackupNotification","2","13","1800"},
    {"SNMPTRAP-ciena-CIENA-CES-RSTP-MIB-cienaCesRstpPvstBpduReceivedNotification","2","13","1800"},
    {"SNMPTRAP-ciena-CIENA-CES-RSTP-MIB-cienaCesRstpSelfLoopNotification","2","13","1800"},
    {"SNMPTRAP-ciena-CIENA-CES-RSTP-MIB-cienaCesRstpPortOperEdgeNotification","2","13","1800"},
    {"SNMPTRAP-ciena-CIENA-CES-RSTP-MIB-cienaCesRstpPortFlapNotification","2","13","1800"},
    {"SNMPTRAP-ciena-CIENA-CES-RSTP-MIB-cienaCesRstpBridgeRootPortLostNotification","2","13","1800"}
}
default = {"Unknown","Unknown","Unknown"}
