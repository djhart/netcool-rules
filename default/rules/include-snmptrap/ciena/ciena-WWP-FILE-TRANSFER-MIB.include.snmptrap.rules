###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  WWP-FILE-TRANSFER-MIB
#
###############################################################################

case ".1.3.6.1.4.1.6141.2.7.2": ###  - Notifications from WWP-FILE-TRANSFER-MIB (200104031700Z)

    log(DEBUG, "<<<<< Entering... ciena-WWP-FILE-TRANSFER-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Ciena-WWP-FILE-TRANSFER-MIB"
    @Class = "40506"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### wwpFTransferCompletion

            ##########
            # $1 = wwpFTransferRemoteFilename 
            # $2 = wwpFTransferLocalFilename 
            # $3 = wwpFTransferNotificationStatus 
            # $4 = wwpFTransferNotificationInfo 
            ##########

            $wwpFTransferRemoteFilename = $1
            $wwpFTransferLocalFilename = $2
            $wwpFTransferNotificationStatus = lookup($3, wwpFTransferNotificationStatus)
            $wwpFTransferNotificationInfo = $4

            $OS_EventId = "SNMPTRAP-ciena-WWP-FILE-TRANSFER-MIB-wwpFTransferCompletion"

            @AlertGroup = "WWP File Transfer Completion Notification"
            @AlertKey = ""
            @Summary = "Completion For File Transfer Request"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            $wwpFTransferNotificationStatus = $wwpFTransferNotificationStatus + " ( " + $3 + " )"

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpFTransferRemoteFilename,$wwpFTransferLocalFilename,$wwpFTransferNotificationStatus,$wwpFTransferNotificationInfo)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpFTransferRemoteFilename", $wwpFTransferRemoteFilename, "wwpFTransferLocalFilename", $wwpFTransferLocalFilename, "wwpFTransferNotificationStatus", $wwpFTransferNotificationStatus,
                 "wwpFTransferNotificationInfo", $wwpFTransferNotificationInfo)


        case "2": ### wwpFTransferCmdParseError

            ##########
            # $1 = wwpFTransferRemoteFilename 
            ##########

            $wwpFTransferRemoteFilename = $1

            $OS_EventId = "SNMPTRAP-ciena-WWP-FILE-TRANSFER-MIB-wwpFTransferCmdParseError"

            @AlertGroup = "WWP File Transfer Cmd Parse Error Notification"
            @AlertKey = ""
            @Summary = "Parsing Of The Cmd File Returns Error"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpFTransferRemoteFilename)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpFTransferRemoteFilename", $wwpFTransferRemoteFilename)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, ciena-WWP-FILE-TRANSFER-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, ciena-WWP-FILE-TRANSFER-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-WWP-FILE-TRANSFER-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-WWP-FILE-TRANSFER-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... ciena-WWP-FILE-TRANSFER-MIB.include.snmptrap.rules >>>>>")
