###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  WWP-MAC-MGMT-MIB
#
###############################################################################

case ".1.3.6.1.4.1.6141.2.28.2": ###  - Notifications from WWP-MAC-MGMT-MIB (200104031700Z)

    log(DEBUG, "<<<<< Entering... ciena-WWP-MAC-MGMT-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Ciena-WWP-MAC-MGMT-MIB"
    @Class = "40506"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### wwpMacMgmtSacHighThreshold

            ##########
            # $1 = wwpMacMgmtSacVlanID 
            # $2 = wwpMacMgmtSacPortId 
            ##########

            $wwpMacMgmtSacVlanID = $1
            $wwpMacMgmtSacPortId = $2

            $OS_EventId = "SNMPTRAP-ciena-WWP-MAC-MGMT-MIB-wwpMacMgmtSacHighThreshold"

            @AlertGroup = "WWP Mac Mgmt Sac Threshold Status"
            @AlertKey = "wwpMacMgmtSacEntry." + $wwpMacMgmtSacVlanID + "." + $wwpMacMgmtSacPortId
            @Summary = "MACS Learned Is Exceeds SAC Threshold Limit ( " + @AlertKey + " )"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpMacMgmtSacVlanID,$wwpMacMgmtSacPortId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpMacMgmtSacVlanID", $wwpMacMgmtSacVlanID, "wwpMacMgmtSacPortId", $wwpMacMgmtSacPortId)


        case "2": ### wwpMacMgmtSacNormalThreshold

            ##########
            # $1 = wwpMacMgmtSacVlanID 
            # $2 = wwpMacMgmtSacPortId 
            ##########

            $wwpMacMgmtSacVlanID = $1
            $wwpMacMgmtSacPortId = $2

            $OS_EventId = "SNMPTRAP-ciena-WWP-MAC-MGMT-MIB-wwpMacMgmtSacNormalThreshold"

            @AlertGroup = "WWP Mac Mgmt Sac Threshold Status"
            @AlertKey = "wwpMacMgmtSacEntry." + $wwpMacMgmtSacVlanID + "." + $wwpMacMgmtSacPortId
            @Summary = "MACS Learned Returns To Normal After Exceeding SAC Threshold Limit ( " + @AlertKey + " )"

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpMacMgmtSacVlanID,$wwpMacMgmtSacPortId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpMacMgmtSacVlanID", $wwpMacMgmtSacVlanID, "wwpMacMgmtSacPortId", $wwpMacMgmtSacPortId)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, ciena-WWP-MAC-MGMT-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, ciena-WWP-MAC-MGMT-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-WWP-MAC-MGMT-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-WWP-MAC-MGMT-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... ciena-WWP-MAC-MGMT-MIB.include.snmptrap.rules >>>>>")
