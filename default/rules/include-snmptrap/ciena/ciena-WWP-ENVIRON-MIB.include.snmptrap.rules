###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  WWP-ENVIRON-MIB
#
###############################################################################

case ".1.3.6.1.4.1.6141.2.13.2": ###  - Notifications from WWP-ENVIRON-MIB (200104031700Z)

    log(DEBUG, "<<<<< Entering... ciena-WWP-ENVIRON-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Ciena-WWP-ENVIRON-MIB"
    @Class = "40506"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### wwpEnvPowerSupplyStatusNotification

            ##########
            # $1 = wwpEnvPowerSupplyNum 
            # $2 = wwpEnvPowerSupplyState 
            # $3 = wwpEnvPowerSupplyType 
            ##########

            $wwpEnvPowerSupplyNum = $1
            $wwpEnvPowerSupplyState = lookup($2, wwpEnvPowerSupplyState)
            $wwpEnvPowerSupplyType = lookup($3, wwpEnvPowerSupplyType) + " ( " + $3 + " )"

            $OS_EventId = "SNMPTRAP-ciena-WWP-ENVIRON-MIB-wwpEnvPowerSupplyStatusNotification"

            @AlertGroup = "WWP Environment Power Supply Status"
            @AlertKey = "wwpEnvPowerEntry." + $wwpEnvPowerSupplyNum

            switch($2)
            {
                        case "1": ### infoNotAvailable
                            $SEV_KEY = $OS_EventId + "_infoNotAvailable"
                            @Summary = "WWP Environment Power Supply Status : " + $wwpEnvPowerSupplyState + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "2": ### notInstalled
                            $SEV_KEY = $OS_EventId + "_notInstalled"
                            @Summary = "WWP Environment Power Supply Status : " + $wwpEnvPowerSupplyState + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "3": ### installedAndOperating
                            $SEV_KEY = $OS_EventId + "_installedAndOperating"
                            @Summary = "WWP Environment Power Supply Status : " + $wwpEnvPowerSupplyState + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 1
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0

                        case "4": ### installedAndNotOperating
                            $SEV_KEY = $OS_EventId + "_installedAndNotOperating"
                            @Summary = "WWP Environment Power Supply Status : " + $wwpEnvPowerSupplyState + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        default:
                            $SEV_KEY = $OS_EventId + "_unknown"
                            @Summary = "WWP Environment Power Supply Status Is Unknown ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0
            }

            update(@Severity)
            update(@Summary) 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            $wwpEnvPowerSupplyState = $wwpEnvPowerSupplyState + " ( " + $2 + " )"

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpEnvPowerSupplyNum,$wwpEnvPowerSupplyState,$wwpEnvPowerSupplyType)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpEnvPowerSupplyNum", $wwpEnvPowerSupplyNum, "wwpEnvPowerSupplyState", $wwpEnvPowerSupplyState, "wwpEnvPowerSupplyType", $wwpEnvPowerSupplyType)


        case "2": ### wwpEnvFanModuleNotification

            ##########
            # $1 = wwpEnvFanModuleNum 
            # $2 = wwpEnvFanModuleState 
            ##########

            $wwpEnvFanModuleNum = $1
            $wwpEnvFanModuleState = lookup($2, wwpEnvFanModuleState)

            $OS_EventId = "SNMPTRAP-ciena-WWP-ENVIRON-MIB-wwpEnvFanModuleNotification"

            @AlertGroup = "WWP Environment Fan Module Status"
            @AlertKey = "wwpEnvFanModuleEntry." + $wwpEnvFanModuleNum

            switch($2)
            {
                        case "1": ### infoNotAvailable
                            $SEV_KEY = $OS_EventId + "_infoNotAvailable"
                            @Summary = "WWP Environment Fan Module Status : " + $wwpEnvFanModuleState + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "2": ### notInstalled
                            $SEV_KEY = $OS_EventId + "_notInstalled"
                            @Summary = "WWP Environment Fan Module Status : " + $wwpEnvFanModuleState + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "3": ### installedAndOperating
                            $SEV_KEY = $OS_EventId + "_installedAndOperating"
                            @Summary = "WWP Environment Fan Module Status : " + $wwpEnvFanModuleState + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 1
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0

                        case "4": ### installedAndNotOperating
                            $SEV_KEY = $OS_EventId + "_installedAndNotOperating"
                            @Summary = "WWP Environment Fan Module Status : " + $wwpEnvFanModuleState + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        default:
                            $SEV_KEY = $OS_EventId + "_unknown"
                            @Summary = "WWP Environment Fan Module Status Is Unknown ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0
            }

            update(@Severity)
            update(@Summary) 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            $wwpEnvFanModuleState = $wwpEnvFanModuleState + " ( " + $2 + " )"

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpEnvFanModuleNum,$wwpEnvFanModuleState)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpEnvFanModuleNum", $wwpEnvFanModuleNum, "wwpEnvFanModuleState", $wwpEnvFanModuleState)


        case "3": ### wwpEnvTempNotification

            ##########
            # $1 = wwpEnvTempSensorState 
            # $2 = wwpEnvTempSensorValue 
            # $3 = wwpEnvTempSensorHighThreshold 
            # $4 = wwpEnvTempSensorLowThreshold 
            ##########

            $wwpEnvTempSensorState = lookup($1, wwpEnvTempSensorState)
            $wwpEnvTempSensorValue = $2
            $wwpEnvTempSensorHighThreshold = $3
            $wwpEnvTempSensorLowThreshold = $4

            $wwpEnvTempSensorNum = extract($OID1, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-ciena-WWP-ENVIRON-MIB-wwpEnvTempNotification"

            @AlertGroup = "WWP Environment Temp Status"
            @AlertKey = "wwpEnvTempSensorEntry." + $wwpEnvTempSensorNum

            switch($1)
            {
                        case "0": ### higherThanThreshold
                            $SEV_KEY = $OS_EventId + "_higherThanThreshold"
                            @Summary = "WWP Environment Temp Sensor State: " + $wwpEnvTempSensorState + " ( " + @AlertKey + " )" 
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "1": ### normal
                            $SEV_KEY = $OS_EventId + "_normal"
                            @Summary = "WWP Environment Temp Sensor State: " + $wwpEnvTempSensorState + " ( " + @AlertKey + " )" 
                            $DEFAULT_Severity = 1
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0

                        case "2": ### lowerThanThreshold
                            $SEV_KEY = $OS_EventId + "_lowerThanThreshold"
                            @Summary = "WWP Environment Temp Sensor State: " + $wwpEnvTempSensorState + " ( " + @AlertKey + " )" 
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        default:
                            $SEV_KEY = $OS_EventId + "_unknown"
                            @Summary = "WWP Environment Temp Sensor State Is Unknown ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0
            }

            update(@Severity)
            update(@Summary) 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            $wwpEnvTempSensorState = $wwpEnvTempSensorState + " ( " + $1 + " )"

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpEnvTempSensorState,$wwpEnvTempSensorValue,$wwpEnvTempSensorHighThreshold,$wwpEnvTempSensorLowThreshold,$wwpEnvTempSensorNum)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpEnvTempSensorState", $wwpEnvTempSensorState, "wwpEnvTempSensorValue", $wwpEnvTempSensorValue, "wwpEnvTempSensorHighThreshold", $wwpEnvTempSensorHighThreshold,
                 "wwpEnvTempSensorLowThreshold", $wwpEnvTempSensorLowThreshold, "wwpEnvTempSensorNum", $wwpEnvTempSensorNum)


        case "4": ### wwpEnvPowerSwitchNotification

            ##########
            # $1 = wwpPowerSwitchingOp 
            ##########

            $wwpPowerSwitchingOp = lookup($1, wwpPowerSwitchingOp)

            $OS_EventId = "SNMPTRAP-ciena-WWP-ENVIRON-MIB-wwpEnvPowerSwitchNotification"

            @AlertGroup = "WWP Environment Power Switch Status"
            @AlertKey = ""
            @Summary = "WWP Environment Power Switch Status, Input Power Supply Is Switched To " + $wwpPowerSwitchingOp

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            $wwpPowerSwitchingOp = $wwpPowerSwitchingOp + " ( " + $1 + " )"

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpPowerSwitchingOp)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpPowerSwitchingOp", $wwpPowerSwitchingOp)


        case "5": ### wwpEnvPortalBatteryStatusNotification

            ##########
            # $1 = wwpEnvPortalBatteryStatus 
            ##########

            $wwpEnvPortalBatteryStatus = lookup($1, wwpEnvPortalBatteryStatus)

            $OS_EventId = "SNMPTRAP-ciena-WWP-ENVIRON-MIB-wwpEnvPortalBatteryStatusNotification"

            @AlertGroup = "WWP Environment Portal Battery Status"
            @AlertKey = ""

            switch($1)
            {
                        case "1": ### present
                            $SEV_KEY = $OS_EventId + "_present"
                            @Summary = "WWP Environment Portal Battery Status: " + $wwpEnvPortalBatteryStatus
                            $DEFAULT_Severity = 1
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0

                        case "2": ### missing
                            $SEV_KEY = $OS_EventId + "_missing"
                            @Summary = "WWP Environment Portal Battery Status: " + $wwpEnvPortalBatteryStatus
                            $DEFAULT_Severity = 4
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        default:
                            $SEV_KEY = $OS_EventId + "_unknown"
                            @Summary = "WWP Environment Portal Battery Is Unknown Status : " + $wwpEnvPortalBatteryStatus
                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0
            }

            update(@Severity)
            update(@Summary) 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            $wwpEnvPortalBatteryStatus = $wwpEnvPortalBatteryStatus + " ( " + $1 + " )"

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpEnvPortalBatteryStatus)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpEnvPortalBatteryStatus", $wwpEnvPortalBatteryStatus)


        case "6": ### wwpEnvPortalBatteryVoltageLevelNotification

            ##########
            # $1 = wwpEnvPortalBatteryVoltageLevel 
            ##########

            $wwpEnvPortalBatteryVoltageLevel = lookup($1, wwpEnvPortalBatteryVoltageLevel)

            $OS_EventId = "SNMPTRAP-ciena-WWP-ENVIRON-MIB-wwpEnvPortalBatteryVoltageLevelNotification"

            @AlertGroup = "WWP Environment Portal Battery Voltage Level Status"
            @AlertKey = ""

            switch($1)
            {
                        case "1": ### normal
                            $SEV_KEY = $OS_EventId + "_normal"
                            @Summary = "WWP Environment Portal Battery Voltage Level: " + $wwpEnvPortalBatteryVoltageLevel 
                            $DEFAULT_Severity = 1
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0

                        case "2": ### low
                            $SEV_KEY = $OS_EventId + "_low"
                            @Summary = "WWP Environment Portal Battery Voltage Level: " + $wwpEnvPortalBatteryVoltageLevel 
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        default:
                            $SEV_KEY = $OS_EventId + "_unknown"
                            @Summary = "WWP Environment Portal Battery Voltage Is Unknown Level: " + $wwpEnvPortalBatteryVoltageLevel
                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0
            }

            update(@Severity)
            update(@Summary) 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            $wwpEnvPortalBatteryVoltageLevel = $wwpEnvPortalBatteryVoltageLevel + " ( " + $1 + " )"

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpEnvPortalBatteryVoltageLevel)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpEnvPortalBatteryVoltageLevel", $wwpEnvPortalBatteryVoltageLevel)


        case "7": ### wwpEnvPortalBatteryConditionNotification

            ##########
            # $1 = wwpEnvPortalBatteryCondition 
            ##########

            $wwpEnvPortalBatteryCondition = lookup($1, wwpEnvPortalBatteryCondition)

            $OS_EventId = "SNMPTRAP-ciena-WWP-ENVIRON-MIB-wwpEnvPortalBatteryConditionNotification"

            @AlertGroup = "WWP Environment Portal Battery Condition Status"
            @AlertKey = ""

            switch($1)
            {
                        case "1": ### good
                            $SEV_KEY = $OS_EventId + "_good"
                            @Summary = "WWP Environment Portal Battery Condition: " + $wwpEnvPortalBatteryCondition
                            $DEFAULT_Severity = 1
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0

                        case "2": ### bad
                            $SEV_KEY = $OS_EventId + "_bad"
                            @Summary = "WWP Environment Portal Battery Condition: " + $wwpEnvPortalBatteryCondition
                            $DEFAULT_Severity = 4
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        default:
                            $SEV_KEY = $OS_EventId + "_unknown"
                            @Summary = "WWP Environment Portal Battery Is Unknown Condition: " + $wwpEnvPortalBatteryCondition
                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0
            }

            update(@Severity)
            update(@Summary) 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            $wwpEnvPortalBatteryCondition = $wwpEnvPortalBatteryCondition + " ( " + $1 + " )"

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpEnvPortalBatteryCondition)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpEnvPortalBatteryCondition", $wwpEnvPortalBatteryCondition)


        case "8": ### wwpEnvPortalPowerSourceNotification

            ##########
            # $1 = wwpEnvPortalPowerSource 
            ##########

            $wwpEnvPortalPowerSource = lookup($1, wwpEnvPortalPowerSource)

            $OS_EventId = "SNMPTRAP-ciena-WWP-ENVIRON-MIB-wwpEnvPortalPowerSourceNotification"

            @AlertGroup = "WWP Environment Portal Power Source Status"
            @AlertKey = ""

            @Summary = "WWP Environment Portal Power Source: " + $wwpEnvPortalPowerSource
            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            $wwpEnvPortalPowerSource = $wwpEnvPortalPowerSource + " ( " + $1 + " )"

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpEnvPortalPowerSource)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpEnvPortalPowerSource", $wwpEnvPortalPowerSource)


        case "9": ### wwpEnvDoorStateChgNotification

            ##########
            # $1 = wwpEnvDoorState 
            ##########

            $wwpEnvDoorState = lookup($1, wwpEnvDoorState )

            $OS_EventId = "SNMPTRAP-ciena-WWP-ENVIRON-MIB-wwpEnvDoorStateChgNotification"

            @AlertGroup = "WWP Environment Door State Chg Status"
            @AlertKey = ""

            switch($1)
            {
                        case "1": ### open
                            $SEV_KEY = $OS_EventId + "_open"
                            @Summary = "WWP Environment Door State: " + $wwpEnvDoorState
                            $DEFAULT_Severity = 1
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0

                        case "2": ### close
                            $SEV_KEY = $OS_EventId + "_close"
                            @Summary = "WWP Environment Door State: " + $wwpEnvDoorState
                            $DEFAULT_Severity = 4
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        default:
                            $SEV_KEY = $OS_EventId + "_unknown"
                            @Summary = "WWP Environment Door Is Unknown State: " + $wwpEnvDoorState
                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0
            }

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            $wwpEnvDoorState = $wwpEnvDoorState + " ( " + $1 + " )"

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpEnvDoorState)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpEnvDoorState", $wwpEnvDoorState)


        case "10": ### wwpEnvDryContactOpenStateNotification

            ##########
            # $1 = wwpEnvDryContactOpenStateName 
            # $2 = wwpEnvDryContactOpenStateCount 
            ##########

            $wwpEnvDryContactOpenStateName = $1
            $wwpEnvDryContactOpenStateCount = $2

            $OS_EventId = "SNMPTRAP-ciena-WWP-ENVIRON-MIB-wwpEnvDryContactOpenStateNotification"

            @AlertGroup = "WWP Environment Dry Contact State Status"
            @AlertKey = ""
            @Summary = "WWP Environment Dry Contact Open State, Name: " + $wwpEnvDryContactOpenStateName + ", Count: " + $wwpEnvDryContactOpenStateCount

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpEnvDryContactOpenStateName,$wwpEnvDryContactOpenStateCount)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpEnvDryContactOpenStateName", $wwpEnvDryContactOpenStateName, "wwpEnvDryContactOpenStateCount", $wwpEnvDryContactOpenStateCount)


        case "11": ### wwpEnvDryContactCloseStateNotification

            ##########
            # $1 = wwpEnvDryContactCloseStateName 
            # $2 = wwpEnvDryContactCloseStateCount 
            ##########

            $wwpEnvDryContactCloseStateName = $1
            $wwpEnvDryContactCloseStateCount = $2

            $OS_EventId = "SNMPTRAP-ciena-WWP-ENVIRON-MIB-wwpEnvDryContactCloseStateNotification"

            @AlertGroup = "WWP Environment Dry Contact State Status"
            @AlertKey = ""
            @Summary = "WWP Environment Dry Contact Close State, Name: " + $wwpEnvDryContactCloseStateName + ", Count: " + $wwpEnvDryContactCloseStateCount

            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpEnvDryContactCloseStateName,$wwpEnvDryContactCloseStateCount)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpEnvDryContactCloseStateName", $wwpEnvDryContactCloseStateName, "wwpEnvDryContactCloseStateCount", $wwpEnvDryContactCloseStateCount)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, ciena-WWP-ENVIRON-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, ciena-WWP-ENVIRON-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-WWP-ENVIRON-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-WWP-ENVIRON-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... ciena-WWP-ENVIRON-MIB.include.snmptrap.rules >>>>>")
