###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  WWP-LEOS-RAPS-MIB
#
###############################################################################

case ".1.3.6.1.4.1.6141.2.60.47.2": ### - Notifications from WWP-LEOS-RAPS-MIB ("201009161700Z")

    log(DEBUG, "<<<<< Entering... ciena-WWP-LEOS-RAPS-MIB.include.snmptrap.rules >>>>>")

    @Agent = "ciena-WWP-LEOS-RAPS-MIB"
    @Class = "40506"
    
    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
     
        case "1": ### wwpLeosRapsAlarm
        
            ##########
            # $1 = wwpLeosRapsVirtualRingName
            # $2 = wwpLeosRapsVirtualRingAlarm
            ##########
            
            $wwpLeosRapsVirtualRingName = $1
            $wwpLeosRapsVirtualRingAlarm = lookup($2, WwpLeosRapsVirtualRingAlarm)
            
            $wwpLeosRapsVirtualRingIndex = extract($OID1, "\.([0-9]+)$")
            
            $OS_EventId = "SNMPTRAP-ciena-WWP-LEOS-RAPS-MIB-wwpLeosRapsAlarm"

            @AlertGroup = "RAPS Virtual Ring Status"
            @AlertKey = "wwpLeosRapsVirtualRingEntry." + $wwpLeosRapsVirtualRingIndex
            @Summary = "RAPS Virtual Ring Status, Virtual Ring Name: " + $wwpLeosRapsVirtualRingName + " , Status: " + $wwpLeosRapsVirtualRingAlarm + " ( " + @AlertKey + " ) " 
            
            switch($2)
            {
                case "1":### clear
                    $SEV_KEY = $OS_EventId + "_clear"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0               
                case "2":### protectionSwitching
                    $SEV_KEY = $OS_EventId + "_protectionSwitching"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "3":### provisionMismatch
                    $SEV_KEY = $OS_EventId + "_provisionMismatch"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                default: 
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }
                     
            update(@Severity)
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $wwpLeosRapsVirtualRingAlarm = $wwpLeosRapsVirtualRingAlarm + " ( " + $2 + " )"
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpLeosRapsVirtualRingName,$wwpLeosRapsVirtualRingAlarm,$wwpLeosRapsVirtualRingEntry)            
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpLeosRapsVirtualRingName", $wwpLeosRapsVirtualRingName, "wwpLeosRapsVirtualRingAlarm", $wwpLeosRapsVirtualRingAlarm, "wwpLeosRapsVirtualRingEntry", $wwpLeosRapsVirtualRingEntry)
         
        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, ciena-WWP-LEOS-RAPS-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, ciena-WWP-LEOS-RAPS-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-WWP-LEOS-RAPS-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-WWP-LEOS-RAPS-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... ciena-WWP-LEOS-RAPS-MIB.include.snmptrap.rules >>>>>")


