###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CIENA-CES-CHASSIS-MIB
#
###############################################################################

table CienaCesChassisPowerSupplyState =
{

    {"1","Online"}, ### online
    {"2","Faulted"}, ### faulted
    {"3","Offline"}, ### offline
    {"4","Uninstalled"} ### uninstalled    
}
default = "Unknown"

table CienaCesChassisPowerSupplyType =
{

    {"1","AC"}, ### ac
    {"2","DC"}, ### dc
    {"3","Unequipped"} ### unequipped
}
default = "Unknown"

table CienaCesChassisFanTrayType =
{

    {"1","Fixed"}, ### fixed
    {"2","Hot Swappable"}, ### hotSwappable
    {"3","Unequipped"} ### unequipped
}
default = "Unknown"

table CienaCesChassisFanTrayStatus =
{

    {"1","Ok"}, ### ok
    {"2","Pending"}, ### pending
    {"3","RPM-Warning"}, ### rpm-warning
    {"4","Uninstalled"} ### uninstalled
}
default = "Unknown"

table TceHealthCategory =
{

    {"2","CPU"}, ### cpu
    {"3","Datapath"}, ### datapath
    {"4","Ethernet"}, ### ethernet
    {"5","Fabric"}, ### fabric
    {"6","SM"}, ### sm
    {"7","Temp SM"}, ### tempSm
    {"8","Samples SM"}, ### samplesSm
    {"9","Disk"}, ### disk
    {"10","Temp Module"}, ### tempModule
    
    {"11","Samples Module"}, ### samplesModule
    {"12","Fan Tray"}, ### fanTray
    {"13","Fan Tray Speed Mismatch"}, ### fanTraySpeedMismatch
    {"14","Fan Speed Mismatch"}, ### fanSpeedMismatch
    {"15","Temp Fan"}, ### tempFan
    {"16","Samples Fan"}, ### samplesFan
    {"17","Fan RPM"}, ### fanRpm
    {"18","power"}, ### power
    {"19","Feed Power"}, ### feedPower
    {"20","System Resource"}, ### systemResource
    
    {"21","Memory"}, ### memory
    {"22","MAC"}, ### mac
    {"23","I2C"}, ### i2c
    {"24","Flash"}, ### flash
    {"25","Transceiver"}, ### transceiver
    {"26","Link"}, ### link
    {"27","IOM Status"} ### iomStatus
}
default = "Unknown"

table TceHealthStatus =
{

    {"2","Good"}, ### good
    {"3","Warning"}, ### warning
    {"4","Degraded"}, ### degraded
    {"5","Faulted"}, ### faulted
}
default = "Unknown"

table CienaCesChassisRebootReasonErrorType =
{

    {"2","User"}, ### user
    {"3","Power Failure"}, ### powerFailure 
    {"4","Upgrade"}, ### upgrade
    {"5","Reset Button"}, ### resetButton
    
    {"6","Cold Failover"}, ### coldFailover
    {"7","Fault Manager"}, ### faultManager
    {"8","Communication Failure"}, ### communicationFailure
    {"9","Auto Revert"}, ### autoRevert
    {"10","Unprotected Failure"}, ### unprotectedFailure
    
    {"11","Boot Failure"}, ### bootFailure
    {"12","Software Revert"}, ### softwareRevert
    {"13","SNMP"}, ### snmp
    {"14","App Load"}, ### appLoad
    {"15","Error Handler"}, ### errorHandler
    {"16","Watchdog"} ### watchdog
}
default = "Unknown"

table CienaCesChassisHealthSubCategory =
{

    {"2_1","Usage"}, ### Usage
    {"3_1","Status"}, ### Status
    {"4_1","Dropped Packets"}, ### DroppedPackets
    {"4_2","CRC Errors"}, ### CRCErrors
    {"4_3","Error Packets"}, ### ErrorPackets
    
    {"5_1","Status"}, ### Status
    {"9_1","Flash0"}, ### Flash0
    {"9_2","Flash1"}, ### Flash1
    {"9_3","Sys0"}, ### Sys0
    {"9_4","Sys1"}, ### Sys1
    {"9_5","Ram"}, ### Ram
    
    {"9_6","Cf0"}, ### Cf0
    {"10_1","Inlet Max"}, ### InletMax
    {"10_2","Inlet Min"}, ### InletMin
    {"10_3","Outlet Max"}, ### OutletMax
    {"10_4","Outlet Min"}, ### OutletMin
    
    {"12_1","Status"}, ### Status
    {"15_1","Max"}, ### Max
    {"15_2","Min"}, ### Min
    {"17_1","Min Speed"}, ### MinSpeed
    {"18_1","Status"}, ### Status
    
    {"19_1","Max"}, ### Max
    {"19_2","Min"}, ### Min
    {"20_1","AggTable"}, ### AggTable
    {"20_2","Meter Profile AttachmentTable"}, ### MeterProfileAttachmentTable
    {"20_3","Meter Profile Table"}, ### MeterProfileTable
    
    {"20_4","PBT Decap Table"}, ### PbtDecapTable
    {"20_5","PBT Encap Table"}, ### PbtEncapTable
    {"20_6","PBT Service Table"}, ### PbtServiceTable
    {"20_7","PBT Transit Table"}, ### PbtTransitTable
    {"20_8","PBT Tunnel Group Table"}, ### PbtTunnelGroupTable
    
    {"20_9","Port State Grp Table"}, ### PortStateGrpTable
    {"20_10","QOS Flow Table"}, ### QosFlowTable
    {"20_11","Subport Table"}, ### SubportTable
    {"20_12","VSS Table"}, ### VssTable
    {"20_13","Traffic Class Term Table"}, ### TrafficClassTermTable
    
    {"20_14","Flood Container Table"}, ### FloodContainerTable
    {"20_15","Logical Interfaces"}, ### LogicalInterfaces
    {"20_16","Shared Rate Profiles"}, ### SharedRateProfiles
    {"20_17","Shared Rate Attachments"}, ### SharedRateAttachments
    
    {"20_18","Shared Rate TCEs"}, ### SharedRateTCEs
    {"20_19","Shared Rate Ac lTCEs"}, ### SharedRateAclTCEs
    {"20_20","Shaping Profiles"}, ### ShapingProfiles
    {"20_21","Shaping Profile Attachments"}, ### ShapingProfileAttachments
    
    {"21_1","Global Heap"}, ### GlobalHeap
    {"21_2","Heap1"}, ### Heap1
    {"21_3","Heap2"}, ### Heap2
    {"21_4","Pool1"}, ### Pool1
    {"21_5","Pool2"}, ### Pool2
    
    {"22_1","Table"}, ### Table
    {"23_1","PS1"}, ### PS1
    {"23_2","PS2"}, ### PS2
    {"23_3","Alarm Card"}, ### AlarmCard
    {"23_4","Fan Tray"}, ### FanTray
    
    {"23_5","IOM3"}, ### IOM3
    {"23_6","IOM4"}, ### IOM4
    {"23_7","IOM5"}, ### IOM5
    {"23_8","IOM6"}, ### IOM6
    {"23_9","IOM7"}, ### IOM7
    
    {"24_1","Write Erase Part1"}, ### WriteErasePart1
    {"24_2","Write Erase Part2"}, ### WriteErasePart2
    {"25_1","Temp Max"}, ### TempMax
    {"25_2","Temp Min"}, ### TempMin
    {"25_3","Rx Power Max"}, ### RxPowerMax
    
    {"25_4","Rx Power Min"}, ### RxPowerMin
    {"25_5","Tx Power Max"}, ### TxPowerMax
    {"25_6","Tx Power Min"}, ### TxPowerMin
    {"26_1","State"} ### State    
}
default = "Unknown"
