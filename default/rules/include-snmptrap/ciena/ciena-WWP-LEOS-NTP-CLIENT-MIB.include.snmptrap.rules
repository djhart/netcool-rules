###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  WWP-LEOS-NTP-CLIENT-MIB
#
###############################################################################

case ".1.3.6.1.4.1.6141.2.60.18.2": ### - Notifications from WWP-LEOS-NTP-CLIENT-MIB (20085200000Z)

    log(DEBUG, "<<<<< Entering... ciena-WWP-LEOS-NTP-CLIENT-MIB.include.snmptrap.rules >>>>>")

    @Agent = "ciena-WWP-LEOS-NTP-CLIENT-MIB"
    @Class = "40506"
    
    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
     
        case "1": ### wwpLeosNtpClientSyncStatusChangeNotification
        
            ##########
            # $1 = wwpLeosNtpClientSyncState
            ##########
            
            $wwpLeosNtpClientSyncState = lookup($1, WwpLeosNtpClientSyncState)
            
            $OS_EventId = "SNMPTRAP-ciena-WWP-LEOS-NTP-CLIENT-MIB-wwpLeosNtpClientSyncStatusChangeNotification"

            @AlertGroup = "WWP LEOS NTP Client Synchronization Status"
            @Summary = "NTP Client Synchronization Status: " + $wwpLeosNtpClientSyncState
            
            switch($1)
            {
                case "1":### synchronized
                    $SEV_KEY = $OS_EventId + "_synchronized"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0               
                case "2":### not-synchronized
                    $SEV_KEY = $OS_EventId + "_not-synchronized"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                default: 
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = "NTP Client Synchronization Status: Unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }
                     
            update(@Severity)
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $wwpLeosNtpClientSyncState = $wwpLeosNtpClientSyncState + " ( " + $1 + " )"
            
             if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                 details($wwpLeosNtpClientSyncState)
             }
             @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpLeosNtpClientSyncState", $wwpLeosNtpClientSyncState)
         
        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, ciena-WWP-LEOS-NTP-CLIENT-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, ciena-WWP-LEOS-NTP-CLIENT-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-WWP-LEOS-NTP-CLIENT-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-WWP-LEOS-NTP-CLIENT-MIB.user.include.snmptrap.rules"


##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... ciena-WWP-LEOS-NTP-CLIENT-MIB.include.snmptrap.rules >>>>>")


