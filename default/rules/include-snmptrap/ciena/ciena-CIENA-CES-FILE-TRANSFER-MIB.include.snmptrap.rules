###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CIENA-CES-FILE-TRANSFER-MIB
#
###############################################################################

case ".1.3.6.1.4.1.1271.2.2.16": ###  - Notifications from CIENA-CES-FILE-TRANSFER-MIB (201102020000Z)

    log(DEBUG, "<<<<< Entering... ciena-CIENA-CES-FILE-TRANSFER-MIB.include.snmptrap.rules >>>>>")

    @Agent = "CIENA-CES-FILE-TRANSFER-MIB"
    @Class = "40506"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### cienaCesFTransferCompletion

            ##########
            # $1 = cienaGlobalSeverity 
            # $2 = cienaGlobalMacAddress 
            # $3 = cienaCesFTransferRemoteFilename 
            # $4 = cienaCesFTransferLocalFilename 
            # $5 = cienaCesFTransferNotificationStatus 
            # $6 = cienaCesFTransferNotificationInfo 
            ##########

            $cienaGlobalSeverity = lookup($1, CienaGlobalSeverity)
            $cienaGlobalMacAddress = $2
            $cienaCesFTransferRemoteFilename = $3
            $cienaCesFTransferLocalFilename = $4
            $cienaCesFTransferNotificationStatus = lookup($5, CienaCesFTransferNotificationStatus)
            $cienaCesFTransferNotificationInfo = $6

            $OS_EventId = "SNMPTRAP-ciena-CIENA-CES-FILE-TRANSFER-MIB-cienaCesFTransferCompletion"

            @AlertGroup = "File Transfer Request Status"
            @AlertKey = "Chassis MAC Addr: " + $cienaGlobalMacAddress
            @Summary = "File Transfer Request Completed" + " ( " + @AlertKey + " ) "

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800 
	    
            $cienaGlobalSeverity = $cienaGlobalSeverity + " ( " + $1 + " ) "
            
            $cienaCesFTransferNotificationStatus = $cienaCesFTransferNotificationStatus + " ( " + $5 + " ) "

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($cienaGlobalSeverity,$cienaGlobalMacAddress,$cienaCesFTransferRemoteFilename,$cienaCesFTransferLocalFilename,$cienaCesFTransferNotificationStatus,$cienaCesFTransferNotificationInfo)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cienaGlobalSeverity", $cienaGlobalSeverity, "cienaGlobalMacAddress", $cienaGlobalMacAddress, "cienaCesFTransferRemoteFilename", $cienaCesFTransferRemoteFilename,
                 "cienaCesFTransferLocalFilename", $cienaCesFTransferLocalFilename, "cienaCesFTransferNotificationStatus", $cienaCesFTransferNotificationStatus, "cienaCesFTransferNotificationInfo", $cienaCesFTransferNotificationInfo)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, ciena-CIENA-CES-FILE-TRANSFER-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, ciena-CIENA-CES-FILE-TRANSFER-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-CIENA-CES-FILE-TRANSFER-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-CIENA-CES-FILE-TRANSFER-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... ciena-CIENA-CES-FILE-TRANSFER-MIB.include.snmptrap.rules >>>>>")
