###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  WWP-LEOS-TCE-CHASSIS-MIB
#
###############################################################################

log(DEBUG, "<<<<< Entering... ciena-WWP-LEOS-TCE-CHASSIS-MIB.adv.include.snmptrap.rules >>>>>")

switch($specific-trap)
{
    case "1": ### wwpLeosTceChassisPowerSupplyStatusNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "wwpLeosTceChassisPowerSupplyStatusNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "wwpLeosTceChassisPowerEntry." + $wwpLeosTceChassisPowerSupplyIndx
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "2": ### wwpLeosTceChassisPowerSupplyHighTempNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "wwpLeosTceChassisPowerSupplyHighTempNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "wwpLeosTceChassisPowerTempSensorEntry." + $wwpLeosTceChassisPowerSupplyIndx + "." + $wwpLeosTceChassisPowerTempSensorIndx
        $OS_LocalRootObj = "wwpLeosTceChassisPowerEntry." + $wwpLeosTceChassisPowerSupplyIndx
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "3": ### wwpLeosTceChassisPowerSupplyLowTempNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "wwpLeosTceChassisPowerSupplyLowTempNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "wwpLeosTceChassisPowerTempSensorEntry." + $wwpLeosTceChassisPowerSupplyIndx + "." + $wwpLeosTceChassisPowerTempSensorIndx
        $OS_LocalRootObj = "wwpLeosTceChassisPowerEntry." + $wwpLeosTceChassisPowerSupplyIndx
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "4": ### wwpLeosTceChassisPowerSupplyNormalTempNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "wwpLeosTceChassisPowerSupplyNormalTempNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "wwpLeosTceChassisPowerTempSensorEntry." + $wwpLeosTceChassisPowerSupplyIndx + "." + $wwpLeosTceChassisPowerTempSensorIndx
        $OS_LocalRootObj = "wwpLeosTceChassisPowerEntry." + $wwpLeosTceChassisPowerSupplyIndx
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "5": ### wwpLeosTceChassisFanHiTempNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "wwpLeosTceChassisFanHiTempNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "wwpLeosTceChassisFanTempEntry." + $wwpLeosTceChassisFanTempTrayIndx + "." + $wwpLeosTceChassisFanTempId
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "6": ### wwpLeosTceChassisFanLoTempNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "wwpLeosTceChassisFanLoTempNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "wwpLeosTceChassisFanTempEntry." + $wwpLeosTceChassisFanTempTrayIndx + "." + $wwpLeosTceChassisFanTempId
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "7": ### wwpLeosTceChassisFanNormalTempNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "wwpLeosTceChassisFanNormalTempNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "wwpLeosTceChassisFanTempEntry." + $wwpLeosTceChassisFanTempTrayIndx + "." + $wwpLeosTceChassisFanTempId
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "8": ### wwpLeosTceChassisFanSpeedMinThresholdNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "wwpLeosTceChassisFanSpeedMinThresholdNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "wwpLeosTceChassisFanEntry." + $wwpLeosTceChassisFanTempTrayIndx + "." + $wwpLeosTceChassisFanTempId
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "9": ### wwpLeosTceChassisFanSpeedNormalRangeNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "wwpLeosTceChassisFanSpeedNormalRangeNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "wwpLeosTceChassisFanEntry." + $wwpLeosTceChassisFanTempTrayIndx + "." + $wwpLeosTceChassisFanTempId
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "10": ### wwpLeosTceChassisFanTrayInsertNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "wwpLeosTceChassisFanTrayInsertNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "wwpLeosTceChassisFanTrayEntry." + $wwpLeosTceChassisFanTrayIndx
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "11": ### wwpLeosTceChassisFanTrayRemoveNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "wwpLeosTceChassisFanTrayRemoveNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "wwpLeosTceChassisFanTrayEntry." + $wwpLeosTceChassisFanTrayIndx
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "12": ### wwpLeosTceChassisFanTrayStatusNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "wwpLeosTceChassisFanTrayStatusNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "wwpLeosTceChassisFanTrayEntry." + $wwpLeosTceChassisFanTrayIndx
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "13": ### wwpLeosTceChassisHealthNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "wwpLeosTceChassisHealthNotification"
        $OS_OsiLayer = 0

        
    case "14": ### wwpLeosTceChassisRebootNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "wwpLeosTceChassisRebootNotification"
        $OS_OsiLayer = 0

        
    default:
}

log(DEBUG, "<<<<< Leaving... ciena-WWP-LEOS-TCE-CHASSIS-MIB.adv.include.snmptrap.rules >>>>>")


