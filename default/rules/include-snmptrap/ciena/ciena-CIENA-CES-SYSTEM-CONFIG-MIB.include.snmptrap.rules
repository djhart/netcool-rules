###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CIENA-CES-SYSTEM-CONFIG-MIB
#
###############################################################################

case ".1.3.6.1.4.1.1271.2.2.14": ###  - Notifications from CIENA-CES-SYSTEM-CONFIG-MIB (201005100000Z)

    log(DEBUG, "<<<<< Entering... ciena-CIENA-CES-SYSTEM-CONFIG-MIB.include.snmptrap.rules >>>>>")

    @Agent = "CIENA-CES-SYSTEM-CONFIG-MIB"
    @Class = "40506"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### cienaCesImproperCmdInConfigFile

            ##########
            # $1 = cienaGlobalSeverity 
            # $2 = cienaGlobalMacAddress 
            # $3 = cienaCesSystemConfigFileName 
            # $4 = cienaCesSystemConfigErrLinesTotal 
            ##########

            $cienaGlobalSeverity = lookup($1, CienaGlobalSeverity)
            $cienaGlobalMacAddress = $2
            $cienaCesSystemConfigFileName = $3
            $cienaCesSystemConfigErrLinesTotal = $4

            $OS_EventId = "SNMPTRAP-ciena-CIENA-CES-SYSTEM-CONFIG-MIB-cienaCesImproperCmdInConfigFile"

            @AlertGroup = "Configuration File Improper Commands Status"
            @AlertKey = "Chassis MAC Addr: " + $cienaGlobalMacAddress
            @Summary = "Improper Commands were Found in the Configuration File while Processing the cienaCesSystemConfigFileName: " + $cienaCesSystemConfigFileName + " ( " + @AlertKey + " ) "

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800
	    
	    $cienaGlobalSeverity = $cienaGlobalSeverity + " ( " + $1 + " ) "
	    
	    update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($cienaGlobalSeverity,$cienaGlobalMacAddress,$cienaCesSystemConfigFileName,$cienaCesSystemConfigErrLinesTotal)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cienaGlobalSeverity", $cienaGlobalSeverity, "cienaGlobalMacAddress", $cienaGlobalMacAddress, "cienaCesSystemConfigFileName", $cienaCesSystemConfigFileName,
                 "cienaCesSystemConfigErrLinesTotal", $cienaCesSystemConfigErrLinesTotal)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, ciena-CIENA-CES-SYSTEM-CONFIG-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, ciena-CIENA-CES-SYSTEM-CONFIG-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-CIENA-CES-SYSTEM-CONFIG-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-CIENA-CES-SYSTEM-CONFIG-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... ciena-CIENA-CES-SYSTEM-CONFIG-MIB.include.snmptrap.rules >>>>>")
