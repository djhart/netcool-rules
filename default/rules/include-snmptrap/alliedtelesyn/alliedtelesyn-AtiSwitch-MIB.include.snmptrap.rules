###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  AtiSwitch-MIB
#
###############################################################################

case ".1.3.6.1.4.1.207.8.15.9": ### Allied Telesyn Layer 2 Switches - Traps from AtiSwitch-MIB 

    log(DEBUG, "<<<<< Entering... alliedtelesyn-AtiSwitch-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Allied Telesyn-Layer 2 Switches"
    @Class = "87007"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### atiswitchFanStopTrap

            $OS_EventId = "SNMPTRAP-alliedtelesyn-AtiSwitch-MIB-atiswitchFanStopTrap"

            @AlertGroup = "Switch Status"
            @AlertKey = ""
            @Summary = "Error in Fan Operation Detected"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_alliedtelesyn, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)

        case "2": ### atiswitchTemperatureAbnormalTrap

            $OS_EventId = "SNMPTRAP-alliedtelesyn-AtiSwitch-MIB-atiswitchTemperatureAbnormalTrap"

            @AlertGroup = "Switch Status"
            @AlertKey = ""
            @Summary = "Abnormal Temperature of Switch Detected"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_alliedtelesyn, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_alliedtelesyn, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, alliedtelesyn-AtiSwitch-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, alliedtelesyn-AtiSwitch-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/alliedtelesyn/alliedtelesyn-AtiSwitch-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/alliedtelesyn/alliedtelesyn-AtiSwitch-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... alliedtelesyn-AtiSwitch-MIB.include.snmptrap.rules >>>>>")
