###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 16.0 - Updated Release for Infinera R11.0
#
#       Added new attribute tcaProbableCauseDescription(21) in emsTCA trap.
#       Added new entries in infineraObjectType (BWRSRCPROFILE - LMMPTP) table.
#
#       Compatible with:
#
#         - Infinera FIS release 11.0
#
# 15.0 - Updated Release for Infinera R10.0
#
#       Added new lookup entries in alarmProbableCause (563-577),
#       infineraObjectType (FSM - XOFX) and 
#       InfnManagedObjectType (123-152) table.
#
#       Compatible with:
#
#         - Infinera DTN and DTN-X release 10.0
#         - Infinera ATN release 4.0
#         - Infinera FIS release 10.0
#
# 14.0 - Updated Release for Infinera R9.0
#
#       Added new lookup entries in alarmProbableCause, tcaProbableCause and 
#       infineraObjectType table.
#
#       Compatible with:
#
#         - Infinera DTN and DTN-X release 9.0
#         - Infinera ATN release 4.0
#         - Infinera FIS release 9.0
#
# 13.0 - Updated Release for Infinera R8.2
#
#       Added new lookup entry (degradedConfig) in alarmProbableCause table.
#
#       Compatible with:
#
#         - Infinera DTN and DTN-X release 8.2
#         - Infinera ATN release 4.0
#         - Infinera FIS release 8.2
#
# 12.0 - Updated Release for Infinera R8.1
#
#       Added new lookup entries in infineraObjectType and alarmProbableCause tables.
#
#       Compatible with:
#
#         - Infinera DTN and DTN-X release 8.1
#         - Infinera ATN release 4.0
#         - Infinera FIS release 8.1
#
# 11.0 - Updated Release for Infinera DTN 8.0
#
#       Added new lookup entries
#
#       Compatible with:
#
#         - Infinera DTN and DTN-X release 8.0
#         - Infinera ATN release 3.0
#         - Infinera FIS release 8.0
#
# 10.0 - Updated Release for Infinera DTN 7.0
#
#       Compatible with:
#
#         - Infinera DTN release 7.0
#         - Infinera ATN release 3.0
#         - Infinera FIS release 7.0
#
# 9.0 - Updated Release for Infinera DTN 6.0, Infinera ATN 3.0 and FIS 7.0
#
#       Compatible with:
#
#         - Infinera DTN release 6.0
#         - Infinera ATN release 3.0
#         - Infinera FIS release 7.0
#
# 8.0 - Updated Release for Infinera ATN 2.0 and FIS 6.1
# 
#       Compatible with:
#
#         -  Infinera DTN release 6.0
#         -  Infinera ATN release 2.0
#         -  Infinera FIS release 6.1
#
# 7.0 - Updated Release for Infinera FIS 6.0 and DTN 6.0
#
#       Compatible with:
#
#         -  Infinera DTN release 6.0
#         -  Infinera ATN release 1.0
#         -  Infinera FIS release 6.0
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  INFINERA-TRAP-MIB
#
###############################################################################

table infineraSeverity =
{
    ##########
    # Perceived or Asserted Severity.
    ##########

    {"1","Indeterminate"}, ### psIndeterminate
    {"2","Critical"},      ### psCritical
    {"3","Major"},         ### psMajor
    {"4","Minor"},         ### psMinor
    {"5","Warning"},       ### psWarning
    {"6","Cleared"}        ### psCleared
}
default = "Unknown"

table infineraX733EventType =
{
    ##########
    # Map Infinera Event Types to ITU-T X.733/ X.736 Event Type
    ##########

    {"1","0"}, ### Unknown - Not Defined (X.733)
    {"2","4"}, ### equipment - Equipment Alarm (X.733)
    {"3","4"}, ### facility - Equipment Alarm (X.733)
    {"4","1"}, ### communications - Communication Alarm (X.733)
    {"5","3"}, ### softwareProcessing - Processing Error (X.733)
    {"6","5"}, ### environmental - Environmental Alarm (X.733)
    {"7","2"}, ### qualityOfService - Quality of Service Alarm (X.733)
    {"8","1"}  ### ems - Communication Alarm (X.733)
}
default = "0"

table infineraServiceAffecting =
{
    {"1","Unknown"},              ### saUnknown
    {"2","Service Affecting"},    ### saServiceAffecting
    {"3","Non Service Affecting"} ### saNonServiceAffecting
}
default = "Unknown"

table infineraCategory =
{
    {"1","Unknown"},             ### unknown
    {"2","Equipment"},           ### equipment
    {"3","Facility"},            ### facility
    {"4","Communications"},      ### communications
    {"5","Software Processing"}, ### softwareProcessing
    {"6","Environmental"},       ### environmental
    {"7","Quality of Service"},  ### qualityOfService
    {"8","EMS"}                  ### ems
}
default = "Unknown"

table infineraObjectType =
{
    {"ACCI",       "Alarm Contact Closure Input"},
    {"ACCO",       "Alarm Contact Closure Output"},
    {"ALARM",      "Alarm"},
    {"ALARM_SAP",  "Alarm Severity Assignment Profile"},
    {"ASSOCIATION", "Association"},
    {"BANDCTP",    "Band Connection Termination Point"},
    {"BANDPTP",    "Band Physical Termination Point"},
    {"BMM",        "Band Mux Module"},
    {"BMMOCGPTP",  "BMM Optical Carrier Group Physical Termination Point"},
    {"CHANNELCTP", "Channel Connection Termination Point"},

    {"CHASSIS",             "Chassis"},
    {"CONFIG_TIMER_ALARMS", "Configurable Timer Alarm"},
    {"CtrlLink",            "Control Link"},
    {"DBCONTROL",           "Data Base Control"},
    {"DCFPTP",              "Dispersion Compensation Fiber Physical Termination Point"},
    {"DCHCTP",              "Digital Channel Connection Termination Point"},
    {"DLM",                 "Digital Line Module"},
    {"DLMOCGPTP",           "DLM Optical Carrier Group Physical Termination Point"},
    {"DTPCTP",              "Digital Transport Path Connection Termination Point"},
    {"DigitalSNCP-1-port",  "1-port Protection Group"},

    {"FAN",                     "Fan"},
    {"FANSHELF",                "Fan Shelf"},
    {"GAM",                     "Gain Amplification Module"},
    {"GAMOCGPTP",               "GAM Optical Carrier Group Physical Termination Point"},
    {"GIGECLIENTCTP",           "Gigabit Ethernet Client Channel Connection Termination Point"},
    {"GRE",                     "Generic Routing Encapsulation"},
    {"GROUPTP",                 "Group Termination Point"},
    {"INTEROPCP_NEIGHBOR",      "UNI Neighbor Information"}, 
    {"INTEROPCP_TEINTERFACE",   "UNI TE Interface Information"},
    {"IOSHELF",                 "Input/Output Shelf"},

    {"InternalLink",  "Internal Link"},
    {"LBANDPTP",      "L - Band Physical Termination Point"},
    {"LOCAL_SNC",     "Local Sub Network Connection"},
    {"LOCAL_SUB_SNC", "Local Sub SNC"},
    {"MCM",           "Management Control Module"},
    {"ME",            "Managed Element"},
    {"NCTGIGE",       "Nodal Control and Timing Gigabit Ethernet"},
    {"NTPD",          "Network Time Protocol Daemon"},
    {"OAM",           "Optical Amplification Module"},
    {"OMM",           "Optical Management Module"},

    {"OSCCTP",             "Optical Supervisory Channel Connection Termination Point"},
    {"OSCTCTP",            "Optical Supervisory Channel Transparent Connection Termination Point"},
    {"OTSPTP",             "Optical Transport Section Physical Termination Point"},
    {"OWCTP",              "Order Wire Connection Termination Point"},
    {"OWM",                "Order Wire Module"},
    {"PEM",                "Power Entry Module"},
    {"RADIUSAUTHSERVER",   "Radius Authentication Server"},
    {"RAM",                "Raman Amplification Module"},
    {"REMOTE_SNC",         "Remote Sub Network Connection"},
    {"REMOTE_SUB_SNC",      "Remote Sub SNC"},

    {"RESOURCE_OWNER",  "Resource Owner"},
    {"SDHCLIENTCTP",    "SDH Client Connection Termination Point"},
    {"SECURITYPROFILE", "Security Profile"},
    {"SESSION",         "Session"},
    {"SLOT",            "Slot"},
    {"SONETCLIENTCTP",  "SONET Client Termination Point"},
    {"STATICROUTE",     "Static Route"},
    {"SUBCLIENT",       "Sub Client"},
    {"SWCONTROL",       "Software Control"},
    {"TAM",             "Tributary Adapter Module"},

    {"TOM",             "Tributary Optical Module"},
    {"TRIBPTP",         "Tributary Physical Termination Point"},
    {"TRIBPTPYCABLEPG", "2-port Protection Group"},
    {"TeLink",          "Traffic Engineering Link"},
    {"TopoNode",        "Topology Node"},
    {"USER",            "User"},
    {"VCCTP",           "Virtual Concatenation Connection Termination Point"},
    {"VCG",             "Virtual Concatenation Group"},
    {"XCON",            "Cross-Connect"},
    {"XFR",             "File Transfer"},

    {"XLM",                   "Switching Line Module"},
    {"TEM",                   "TAM Extender Module"},
    {"COMMUNITY",             "SNMP Community"},
    {"SNMPACCESSLIST",        "SNMP Access List"},
    {"SNMPCONFIG",            "SNMP Configuration"},
    {"TRAPCONFIG",            "SNMP Trap Configuration"},
    {"DSE",                   "Dynamic Spectral Equalizer"},
    {"DSEPTP",                "Dynamic Spectral Equalizer Physical Termination Point"},
    {"GMPLS_CONTROL_CHANNEL", "GMPLS Control Channel"},
    {"ORM",                   "Optical Raman Module"},

    {"OSAPTP",               "Optical Spectral Analyzer Physical Termination Point"},
    {"SNMPCOMMUNITYTABLE",   "SNMP Community Table"},
    {"SNMPNOTIFYFP",         "SNMP Notify Filter Profile"},
    {"SNMPNOTIFYFILTER",     "SNMP Notify Filter"},
    {"SNMPNOTIFY",           "SNMP Notify"},
    {"SNMPPROXY",            "SNMP Proxy"},
    {"SNMPTARGETADDR",       "SNMP Target Address"},
    {"SNMPTARGETPARAMS",     "SNMP Target Parameters"},
    {"SNMPUSM",              "SNMP User based Security Model"},
    {"SnmpV3AdminUserTable", "SNMPV3 Administrative User"},

    {"SNMPV3CONFIG",       "SNMP V3 Configuration"},
    {"SNMPVACMACCESS",     "SNMP View Based Access Control Module Access"},
    {"SNMPVACMCONTEXT",    "SNMP View Based Access Control Module Context"},
    {"SNMPVACMSECTOGROUP", "SNMP View Based Access Control Module Security to Group"},
    {"SNMPVACMVTFAMILY",   "SNMP View Based Access Control Module View Tree Family"},
    {"TE_ENDPOINT",        "Traffic Engineering End Point"},
    {"TE_INTERFACE",       "Traffic Engineering Interface"},
    {"ODUCLIENTCTP",       "ODU Client Termination Point"},
    {"ODUKTCLIENTCTP",     "ODUk Tandem Connection Monitoring CTP"},
    {"FCCLIENTCTP",        "Fibre Channel Client Termination Point"},
    {"RemoteNe",           "Remote NE"},
    {"SCM",		   "Submarine Control Module"},
    
    {"CMM",            "Channel Mux Module"},
        
    {"AMM",                "ATN Management Module"},
    {"OFM",                "Optical Filter Module"},
    {"SIM",                "Service Interface Module"},
    
    {"PCM",            "Power Control Module"},
    {"ATNOCGPTP",      "Optical Carrier Group Physical Termination Point"},
    {"CLITERMCFG",     "CLI Terminal Configuration"},
    {"CLRCHCLIENTCTP", "Clear Channel Client Termination Point"},
    {"DCNPTP",         "DCN Physical Termination Point"},
    {"FIBERCON",       "Fiber Connection"},
    {"HOSTACCESS",     "Host Access"},
    {"NCPTP",          "Nodal Control Physical Termination Point"},
    {"OSCPTP",         "Optical Supervisory Channel Physical Termination Point"},
    {"OTUCLIENTCTP",   "OTU Client Connection Termination Point "},
    {"STPCONFIG",      "STP Configuration"},

    {"AAM",            "ATN Amplifier Module"},
    {"OPSW",           "Optical Protection Switch"},
    {"ADAPTLINK",      "Adapt Link "},
    {"OSNCPG",         "Optical SNC Protection Group"},
    {"NMSNC",          "NM SNC"},  
    
    {"EUNI",              "Ethernet User Network Interface"},
    {"EVC",               "Ethernet Virtual Connection"},
    
    {"SERVICE_OBJECT", "Service Object"},
    {"ATN_SNC",        "ATN Subnetwork Connection"},
    {"AtnProvLink",    "ATN Provisioning Link"},
    {"AtnDtnLink",     "ATN DTN Interconnection"},
    {"REM",            "Raman Extension Module"},
    {"FANA",           "Fan"},
    {"FANB",           "Fan"},
    {"EMS",            "Digital Network Administrator"},
    {"PMServer",       "Performance Management Server"},    
    {"Circuit",        "ATN Circuit"},
    {"PeripheralNode", "Peripheral Node"},
    {"PeripheralLink", "Peripheral Link"},
    
    {"NTPAUTHKEY",       "Network Time Protocol Authentication Key"},
    {"VLANASSOCIATION",  "VLAN Association"},
    {"BANDCTP_SPANLOSS", "Band CTP Span Loss"},
    {"ETHERNETSO",       "Ethernet Service object"},
    {"BANDWIDTHPROFILE", "Bandwidth Profile"},
    
    {"FDRDBGFileXFRStatus", "Field Data Recorder Debug File Transfer Status"},
    {"FDRFTPDETAILS", "Field Data Recorder File Transfer Protocol Details"},
    {"Preference", "Preference"},
    
    {"IDLERCHANNELCTP", "Idler Channel Connection Termination Point"},
    {"IDLERPTP",        "Idler Physical Termination Point"},
    {"BwLink",          "Bandwidth Link"},
    {"ELINK",           "Ethernet Link"},
    {"CMMOCHPTP",       "Channel Mux Module Optical Channel PTP"},
    
    {"CMMOCGPTP",     "Channel Mux Module Optical Carrier Group PTP"},
    {"LMOCHPTP",      "Line Module Optical Channel PTP"},
    {"IPACCESSLIST",  "IP Access List"},
    {"TopoLink",      "Topological Link"},
    
    {"ODUKICTP",     "Infinera ODUk Connection Termination Point"},
    {"FEEDPTP",      "Feed Physical Termination Point"},
    {"OTM",          "OTN Tributary Module"},
    {"XM",           "OTN Switching Module"},
    {"ODUKTICTP",    "Infinera ODUk Tandem Connection Monitoring CTP"},
    {"OTUKICTP",     "Infinera OTU Connection Termination Point"},
    {"LMOCGPTP",     "OTN Line Module Optical Carrier Group PTP"},
    {"TIM",          "Tributary Interface Module"},
    {"GFPCLIENTCTP", "Generic Framing Procedure Client CTP"},
    {"OCHCTP",       "Optical Channel Connection Termination Point"},
    {"OLM",          "Two degree OTN Line Module"},
    {"OLX",          "Multi degree OTN Line Module"},
    {"PEMSHELF",     "Power Entry Module Shelf"},
    {"XCM",          "DTN-X Management Control Module"},
    {"TSM",          "Time Synchronization Module"},
    
    {"SELECTOR",     "Selector"},
    {"SA",           "Security Association"},
    {"FIBERLINK",    "Fiber Link"},
    {"SMTPSERVER",   "Simple Mail Transfer Protocol Server"},
	
    {"IGCC",         "Infinera General Communication Channel"},
    {"LICENSE",      "License"},
    
    {"FSM",     "Flex Switch Module"},
    {"FSMSCGPTP",     "Flex Switch Module Super Channel group PTP"},
    {"FRM",     "Flex ROADM Module"},
    {"IMM",     "Infinera  Management Module"},
    {"FSE",     "Flex Switching Expansion Module"},
    {"XOFM",     "Advanced Optical Flex Module"},
    {"LOCAL_OPTICAL_SNC",     "Optical Subnetwork connection"},
    {"OEL",     "Optical Engineered Link"},
    {"FP",     "Fiber Panel"},
    {"AMPOTDRPTP",     "Amplifier OTDR PTP"},
    {"OPTICAL_XCON",     "Optical Cross Connect"},
    {"ROADMCHASSIS",     "ROADM Chassis"},
    {"EXPNSCGPTP",     "Expansion Super Channel Group PTP"},
    {"SCHCTP",     "Super Channel CTP"},
    {"ODUKCLIENTCTP",     "OCH Data Unit Client CTP"},
    {"OpticalTeLink",     "Optical TE Link"},
    {"OPTICAL_TEINTF",     "Optical TE Interface"},
    {"OFXSCGPTP",     "Optical Flex Super Channel Group PTP"},
    {"FRMSCGPTP",     "FRM Super Channel Group PTP"},
    {"IRM",     "Infinera Raman Module"},
    {"REMOTE_OPTICAL_SNC",     "Remote Optical Subnetwork Connection"},
    {"IAM",     "Infinera Amplifier Module"},
    {"CARRIERCTP",     "CARRIER CTP"},
    {"BASESCGPTP",     "Base Super Channel Group PTP"},
    {"XOFX",     "Advanced or Submarine Optical Flex Module"},

    # New in FIS R11
    {"BWRSRCPROFILE",     "Bandwidth Resource Profile"},
    {"BWPROFILE",     "Bandwidth  Profile"},
    {"SMPG",     "Shared Mesh Protection Group"},
    {"COSVALUEMAP",     "Class of Service value Map"},
    {"GFPTP",     "Generic Framing Protocol TP"},
    {"PXM",     "Packet Switching Module"},
    {"XMM",     "Cloud Express Management Module"},
    {"AUDALARMSETTING",     "Audible Alarm Setting"},
    {"ETHINTF",     "Ethernet Interface"},
    {"AC",     "Attachment Circuit"},
    {"PW",     "Pseudo Wire"},
    {"CXOCGPTP",     "Cloud Express OCG PTP"},
    {"LMM",     "Line Multiplexing Module"},
    {"COSMAPPROFILE",     "Class of Service Profile"},
    {"EULA",     "EndUser License agreeemt"},
    {"CONGESTIONMGMTPROFILE",     "Congession Management Profile"},
    {"XTIM",     "Cloud Express TIM"},
    {"NMVCMOLOCK",     "NM virtual circuit MO Lock"},
    {"VSI",     "Virtual Service Instance"},
    {"NMVC",     "Network Management Virtual circuit"},
    {"FMPSCGPTP",     "Fiber Mux Panel Super Channel Group PTP"},
    {"L2CPPROFILE",     "L2CP Profile"},
    {"NWINTF",     "Network Interface"},
    {"TMPOLICYPROFILE",     "TM Policy Profile"},
    {"LMMPTP",     "Line Multiplexing Module PTP"}

}
default = "Unknown"

table infineraTcaClearableState =
{
    {"1","Clearable"},    ### clearable
    {"2","Non Clearable"} ### nonClearable
}
default = "Unknown"

