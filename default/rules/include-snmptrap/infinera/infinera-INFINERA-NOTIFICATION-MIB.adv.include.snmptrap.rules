###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 15.0 - Updated Release for Infinera R10.0
#
#       Added new lookup entries in alarmProbableCause (563-577), 
#       infineraObjectType (FSM - XOFX) and 
#       InfnManagedObjectType (123-152) table.
#
#       Compatible with:
#
#         - Infinera DTN and DTN-X release 10.0
#         - Infinera ATN release 4.0
#         - Infinera FIS release 10.0
#
# 14.0 - Updated Release for Infinera R9.0
#
#       Added new lookup entries in alarmProbableCause, tcaProbableCause and 
#       infineraObjectType table.
#
#       Compatible with:
#
#         - Infinera DTN and DTN-X release 9.0
#         - Infinera ATN release 4.0
#         - Infinera FIS release 9.0
#
# 13.0 - Updated Release for Infinera R8.2
#
#       Added new lookup entry (degradedConfig) in alarmProbableCause table.
#
#       Compatible with:
#
#         - Infinera DTN and DTN-X release 8.2
#         - Infinera ATN release 4.0
#         - Infinera FIS release 8.2
#
# 12.0 - Updated Release for Infinera R8.1
#
#       Added new lookup entries in infineraObjectType and alarmProbableCause tables.
#
#       Compatible with:
#
#         - Infinera DTN and DTN-X release 8.1
#         - Infinera ATN release 4.0
#         - Infinera FIS release 8.1
#
# 11.0 - Updated Release for Infinera DTN 8.0
#
#       Added new lookup entries
#
#       Compatible with:
#
#         - Infinera DTN and DTN-X release 8.0
#         - Infinera ATN release 3.0
#         - Infinera FIS release 8.0
#
# 10.0 - Updated Release for Infinera DTN 7.0
#
#       Compatible with:
#
#         - Infinera DTN release 7.0
#         - Infinera ATN release 3.0
#         - Infinera FIS release 7.0
#
# 9.0 - Updated Release for Infinera DTN 6.0, Infinera ATN 3.0 and FIS 7.0
#
#       Compatible with:
#
#         - Infinera DTN release 6.0
#         - Infinera ATN release 3.0
#         - Infinera FIS release 7.0
#
# 5.0 - Updated Release for Infinera ATN 2.0 and FIS 6.1
# 
#       Compatible with:
#
#         -  Infinera DTN release 6.0
#         -  Infinera ATN release 2.0
#         -  Infinera FIS release 6.1
#
# 4.0 - Updated Release for Infinera FIS 6.0 and DTN 6.0
#
#       Compatible with:
#
#         -  Infinera DTN release 6.0
#         -  Infinera ATN release 1.0
#         -  Infinera FIS release 6.0
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  INFINERA-NOTIFICATION-MIB
#
###############################################################################

log(DEBUG, "<<<<< Entering... infinera-INFINERA-NOTIFICATION-MIB.adv.include.snmptrap.rules >>>>>")

switch($specific-trap)
{
    case "1": ### infnAlarmNotification

       if(match($9, "1"))
       {
        $OS_X733EventType = 0
       } else
       if(match($9, "2"))
       {
        $OS_X733EventType = 1
       } else
       if(match($9, "3"))
       {
        $OS_X733EventType = 2
       } else
       if(match($9, "4"))
       {
        $OS_X733EventType = 5
       } else
       if(match($9, "5"))
       {
        $OS_X733EventType = 4
       } else 
       if(match($9, "6"))
       {
        $OS_X733EventType = 3
       } else
       if(match($9, "7"))
       {
        $OS_X733EventType = 4
       } else
       if(match($9, "8"))
       {
        $OS_X733EventType = 0
       }

        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "infnAlarmNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = $short_alarmObjectType + ": " + $alarmObjectAid 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "2": ### infnTcaNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "infnTcaNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = $short_tcaObjectType + ": " + $tcaObjectAid 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "3": ### infnAdminEventNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "infnAdminEventNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = $short_infnAdminObjectType + ": " + $infnAdminObjectAid 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "4": ### infnAuditEventNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "infnAuditEventNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = $short_infnAuditObjectType + ": " + $infnAuditObjectAid 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "5": ### infnSecurityEventNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "infnSecurityEventNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Host: " + $infnSecurityHostInfo + ", " + $short_infnSecurityObjectType + ": " + $infnSecurityObjectAid
        $OS_LocalRootObj = "Host: " + $infnSecurityHostInfo
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    default:
}

log(DEBUG, "<<<<< Leaving... infinera-INFINERA-NOTIFICATION-MIB.adv.include.snmptrap.rules >>>>>")
