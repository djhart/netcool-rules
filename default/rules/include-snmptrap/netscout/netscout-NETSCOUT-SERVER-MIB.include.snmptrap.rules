###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  NETSCOUT-SERVER-MIB
#
###############################################################################

case ".1.3.6.1.4.1.141.50.2": ###  - Notifications from NETSCOUT-SERVER-MIB (200602062200Z)

    log(DEBUG, "<<<<< Entering... netscout-NETSCOUT-SERVER-MIB.include.snmptrap.rules >>>>>")

    @Agent = "NETSCOUT-SERVER-MIB"
    @Class = "40262"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### serverBasedTrap

            ##########
            # $1 = serverAlarmsDataSource 
            # $2 = serverAlarmsDeviceIpAddress 
            # $3 = serverAlarmsThreshold 
            # $4 = serverAlarmsDescription 
            # $5 = serverAlarmsValue 
            # $6 = serverAlarmsInterval 
            # $7 = serverAlarmsDataSourceName 
            # $8 = serverAlarmsURL 
            # $9 = serverAlarmsEvidence 
            # $10 = serverAlarmsAlertID 
            # $11 = serverAlarmsSeverity 
            ##########

            $serverAlarmsDataSource = $1
            $serverAlarmsDeviceIpAddress = $2
            $serverAlarmsThreshold = $3
            $serverAlarmsDescription = $4
            $serverAlarmsValue = $5
            $serverAlarmsInterval = $6
            $serverAlarmsDataSourceName = $7
            $serverAlarmsURL = $8
            $serverAlarmsEvidence = $9
            $serverAlarmsAlertID = $10
            $serverAlarmsSeverity = $11

            $OS_EventId = "SNMPTRAP-netscout-NETSCOUT-SERVER-MIB-serverBasedTrap"

            @AlertGroup = "NGenius Server Status"
            @AlertKey = $serverAlarmsEvidence
            @Summary = "An Alarm has Triggered from "+ $serverAlarmsDeviceIpAddress + ", Alarm Description: "+ $serverAlarmsDescription + " ( "+ @AlertKey + " ) "

            $serverAlarmsSeverity = lower($serverAlarmsSeverity)
            if (match($serverAlarmsSeverity, "critical"))
            {
              $SEV_KEY = $OS_EventId + "_critical"
              $DEFAULT_Severity = 5
            }
            else
            {
              $SEV_KEY = $OS_EventId + "_warning"
              $DEFAULT_Severity = 2
            }
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0
            update(@Severity)
			
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_netscout, "1")) {
                details($serverAlarmsDataSource,$serverAlarmsDeviceIpAddress,$serverAlarmsThreshold,$serverAlarmsDescription,$serverAlarmsValue,$serverAlarmsInterval,$serverAlarmsDataSourceName,$serverAlarmsURL,$serverAlarmsEvidence,$serverAlarmsAlertID,$serverAlarmsSeverity)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "serverAlarmsDataSource", $serverAlarmsDataSource, "serverAlarmsDeviceIpAddress", $serverAlarmsDeviceIpAddress, "serverAlarmsThreshold", $serverAlarmsThreshold,
                 "serverAlarmsDescription", $serverAlarmsDescription, "serverAlarmsValue", $serverAlarmsValue, "serverAlarmsInterval", $serverAlarmsInterval,
                 "serverAlarmsDataSourceName", $serverAlarmsDataSourceName, "serverAlarmsURL", $serverAlarmsURL, "serverAlarmsEvidence", $serverAlarmsEvidence,
                 "serverAlarmsAlertID", $serverAlarmsAlertID, "serverAlarmsSeverity", $serverAlarmsSeverity)

        case "3": ### serverBasedClearTrap

            ##########
            # $1 = serverAlarmsAlertID 
            # $2 = serverAlarmsEvidence 
            ##########

            $serverAlarmsAlertID = $1
            $serverAlarmsEvidence = $2

            $OS_EventId = "SNMPTRAP-netscout-NETSCOUT-SERVER-MIB-serverBasedClearTrap"

            @AlertGroup = "NGenius Server Status"
            @AlertKey = $serverAlarmsEvidence
            @Summary = "An Alarm has Been Cleared" + " ( "+ @AlertKey + " ) "

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_netscout, "1")) {
                details($serverAlarmsAlertID,$serverAlarmsEvidence)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "serverAlarmsAlertID", $serverAlarmsAlertID, "serverAlarmsEvidence", $serverAlarmsEvidence)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_netscout, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, netscout-NETSCOUT-SERVER-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, netscout-NETSCOUT-SERVER-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/netscout/netscout-NETSCOUT-SERVER-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/netscout/netscout-NETSCOUT-SERVER-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... netscout-NETSCOUT-SERVER-MIB.include.snmptrap.rules >>>>>")
