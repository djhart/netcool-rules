###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 8.0 - Updated release for JUNOS 9.4, JUNOSe 10.3 and BX-OS 4.1
#
#          - Repackaged for NIM-08
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#          -  JUNIPER-MIMSTP-MIB
#
###############################################################################

log(DEBUG, "<<<<< Entering... juniper-JUNIPER-MIMSTP-MIB.adv.include.snmptrap.rules >>>>>")

switch($specific-trap)
{
    case "1": ### jnxMIMstGenTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxMIMstGenTrap"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "jnxMIDot1sJuniperMstEntry." + $jnxMIDot1sJuniperMstContextId
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "2": ### jnxMIMstErrTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxMIMstErrTrap"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "jnxMIDot1sJuniperMstEntry." + $jnxMIDot1sJuniperMstContextId
        $OS_LocalRootObj = "jnxMIDot1sJnxMstTrapsControlEntry." + $jnxMIDot1sJuniperMstContextId
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "3": ### jnxMIMstNewRootTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxMIMstNewRootTrap"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "jnxMIMstMstiBridgeEntry." + $jnxMIDot1sJuniperMstContextId + "." + $4 
        $OS_LocalRootObj = "jnxMIDot1sJuniperMstEntry." + $jnxMIDot1sJuniperMstContextId
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "4": ### jnxMIMstTopologyChgTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxMIMstTopologyChgTrap"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "jnxMIMstMstiBridgeEntry." + $jnxMIDot1sJuniperMstContextId + "." + $2
        $OS_LocalRootObj = "jnxMIDot1sJuniperMstEntry." + $jnxMIDot1sJuniperMstContextId
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "5": ### jnxMIMstProtocolMigrationTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxMIMstProtocolMigrationTrap"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "jnxMIDot1sJuniperMstEntry." + $jnxMIDot1sJuniperMstContextId + "," + "jnxMIMstPortTrapNotificationEntry." + $2 
        $OS_LocalRootObj = "jnxMIDot1sJuniperMstEntry." + $jnxMIDot1sJuniperMstContextId
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "6": ### jnxMIMstInvalidBpduRxdTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxMIMstInvalidBpduRxdTrap"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "jnxMIDot1sJuniperMstEntry." + $jnxMIDot1sJuniperMstContextId + "," + "jnxMIMstPortTrapNotificationEntry." + $2
        $OS_LocalRootObj = "jnxMIDot1sJuniperMstEntry." + $jnxMIDot1sJuniperMstContextId
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "7": ### jnxMIMstRegionConfigChangeTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxMIMstRegionConfigChangeTrap"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "jnxMIDot1sJuniperMstEntry." + $jnxMIDot1sJuniperMstContextId 
        $OS_LocalRootObj = "jnxMIDot1sJnxMstTrapsControlEntry." + $jnxMIDot1sJuniperMstContextId
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

   case "8": ### jnxMIMstCistPortRootProtectStateChangeTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxMIMstCistPortRootProtectStateChangeTrap"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "jnxMIMstCistPortEntry." + $jnxMIMstCistPort
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "9": ### jnxMIMstMstiPortRootProtectStateChangeTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxMIMstMstiPortRootProtectStateChangeTrap"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "jnxMIMstMstiPortEntry." + $jnxMIMstMstiPort + "." + $jnxMIMstInstanceIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "10": ### jnxMIMstCistPortLoopProtectStateChangeTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxMIMstCistPortLoopProtectStateChangeTrap"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "jnxMIMstCistPortEntry." + $jnxMIMstCistPort
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "11": ### jnxMIMstMstiPortLoopProtectStateChangeTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxMIMstMstiPortLoopProtectStateChangeTrap"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "jnxMIMstMstiPortEntry." + $jnxMIMstMstiPort + "." + $jnxMIMstInstanceIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0


    default:
}

log(DEBUG, "<<<<< Leaving... juniper-JUNIPER-MIMSTP-MIB.adv.include.snmptrap.rules >>>>>")
