###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 9.0 - Updated release for JUNOS 10.4, JUNOSe 11.3
#
# 8.0 - Updated release for JUNOS 9.4, JUNOSe 10.3 and BX-OS 4.1
#
#          - Repackaged for NIM-08
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  JUNIPER-SYSTEM-MIB
#
###############################################################################

case ".1.3.6.1.4.1.4874.2.2.2": ###  - Notifications from JUNIPER-SYSTEM-MIB (200806111101Z)

    log(DEBUG, "<<<<< Entering... juniper-JUNIPER-SYSTEM-MIB.include.snmptrap.rules >>>>>")

    @Agent = "JUNIPER-SYSTEM-MIB"
    @Class = "40200"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### juniSystemHighMemUtil

            ##########
            # $1 = juniSystemMemCapacity 
            # $2 = juniSystemMemUtilPct 
            # $3 = juniSystemAbatedMemUtilThreshold 
            # $4 = juniSystemHighMemUtilThreshold 
            # $5 = juniSystemMemKBytesCapacity 
            ##########

            $juniSystemMemCapacity = $1 + "bytes"
            if(match($2, "-1"))
            {
                $juniSystemMemUtilPct = "Unknown"
            }
            else
            {
                $juniSystemMemUtilPct = $2 + "%"
            }
            $juniSystemAbatedMemUtilThreshold = $3 + "%"
            $juniSystemHighMemUtilThreshold = $4 + "%"
            $juniSystemMemKBytesCapacity = $5 + "KBytes"

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemHighMemUtil"

            @AlertGroup = "System Memory Utilization"
            @AlertKey = ""
            @Summary = "System Memory Utilization, " + $juniSystemMemUtilPct + ", Exceeds " + $juniSystemHighMemUtilThreshold

            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($juniSystemMemCapacity,$juniSystemMemUtilPct,$juniSystemAbatedMemUtilThreshold,$juniSystemHighMemUtilThreshold,$juniSystemMemKBytesCapacity)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "juniSystemMemCapacity", $juniSystemMemCapacity, "juniSystemMemUtilPct", $juniSystemMemUtilPct, "juniSystemAbatedMemUtilThreshold", $juniSystemAbatedMemUtilThreshold,
                 "juniSystemHighMemUtilThreshold", $juniSystemHighMemUtilThreshold, "juniSystemMemKBytesCapacity", $juniSystemMemKBytesCapacity)

        case "2": ### juniSystemAbatedMemUtil

            ##########
            # $1 = juniSystemMemCapacity 
            # $2 = juniSystemMemUtilPct 
            # $3 = juniSystemAbatedMemUtilThreshold 
            # $4 = juniSystemHighMemUtilThreshold 
            # $5 = juniSystemMemKBytesCapacity 
            ##########

            $juniSystemMemCapacity = $1 + "bytes"
            if(match($2, "-1"))
            {
                $juniSystemMemUtilPct = "Unknown"
            }
            else
            {
                $juniSystemMemUtilPct = $2 + "%"
            }
            $juniSystemAbatedMemUtilThreshold = $3 + "%"
            $juniSystemHighMemUtilThreshold = $4 + "%"
            $juniSystemMemKBytesCapacity = $5 + "KBytes"

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemAbatedMemUtil"

            @AlertGroup = "System Memory Utilization"
            @AlertKey = ""
            @Summary = "System Memory Utilization, " + $juniSystemMemUtilPct + ", Below " + $juniSystemAbatedMemUtilThreshold

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($juniSystemMemCapacity,$juniSystemMemUtilPct,$juniSystemAbatedMemUtilThreshold,$juniSystemHighMemUtilThreshold,$juniSystemMemKBytesCapacity)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "juniSystemMemCapacity", $juniSystemMemCapacity, "juniSystemMemUtilPct", $juniSystemMemUtilPct, "juniSystemAbatedMemUtilThreshold", $juniSystemAbatedMemUtilThreshold,
                 "juniSystemHighMemUtilThreshold", $juniSystemHighMemUtilThreshold, "juniSystemMemKBytesCapacity", $juniSystemMemKBytesCapacity)

        case "3": ### juniSystemModuleOperStatusChange

            ##########
            # $1 = juniSystemModuleCurrentType 
            # $2 = juniSystemModuleAdminStatus 
            # $3 = juniSystemModuleOperStatus 
            # $4 = juniSystemModuleDisableReason 
            # $5 = juniSystemModuleDescr 
            ##########

            $juniSystemModuleCurrentType = $1
            $juniSystemModuleAdminStatus = lookup($2, JuniEnable) + " ( " + $2 + " ) "
            $juniSystemModuleOperStatus = lookup($3, JuniSystemModuleOperStatus) + " ( " + $3 + " ) "
            $juniSystemModuleDisableReason = lookup($4, JuniSystemModuleDisableReason) + " ( " + $4 + " ) "
            $juniSystemModuleDescr = $5

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemModuleOperStatusChange"

            $juniSystemSlotNumber = extract($OID3,"\.4874\.2\.2\.2\.1\.3\.4\.1\.1\.([0-9]+)\..*$")
            $juniSystemSlotLevel = extract($OID3,"\.4874\.2\.2\.2\.1\.3\.4\.1\.1\.[0-9]+\.(.*)$")
             
            @AlertGroup = "System Module Operational Status"

            @AlertKey = "juniSystemModuleEntry." + $juniSystemSlotNumber + "." + $juniSystemSlotLevel

            switch($3)
            {
                case "1": ### notPresent
                    $SEV_KEY = $OS_EventId + "_notPresent"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Slot: " + @AlertKey + " is currently not present, but it was there previously"

                case "2": ### disabled
                    $SEV_KEY = $OS_EventId + "_disabled"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Slot: " + @AlertKey + " is disabled because of " + $juniSystemModuleDisableReason

                case "3": ### hardwareError
                    $SEV_KEY = $OS_EventId + "_hardwareError"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Hardware Error with the Slot: " + @AlertKey

                case "4": ### booting
                    $SEV_KEY = $OS_EventId + "_booting"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800
                    @Summary = "Slot: " + @AlertKey + " is booting"

                case "5": ### initializing
                    $SEV_KEY = $OS_EventId + "_initializing"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800
                    @Summary = "Slot: " + @AlertKey + " is initializing"

                case "6": ### online
                    $SEV_KEY = $OS_EventId + "_online"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Slot: " + @AlertKey + " is online"

                case "7": ### standby
                    $SEV_KEY = $OS_EventId + "_standby"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800
                    @Summary = "Slot: " + @AlertKey + " is standby"

                case "8": ### inactive
                    $SEV_KEY = $OS_EventId + "_inactive"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Slot: " + @AlertKey + " is inactive"

                case "9": ### notResponding
                    $SEV_KEY = $OS_EventId + "_notResponding"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Slot: " + @AlertKey + " is not responding"

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Slot: " + @AlertKey + " is unknown"

            }

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($juniSystemModuleCurrentType,$juniSystemModuleAdminStatus,$juniSystemModuleOperStatus,$juniSystemModuleDisableReason,$juniSystemModuleDescr,$juniSystemSlotNumber,$juniSystemSlotLevel)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "juniSystemModuleCurrentType", $juniSystemModuleCurrentType, "juniSystemModuleAdminStatus", $juniSystemModuleAdminStatus, "juniSystemModuleOperStatus", $juniSystemModuleOperStatus,
                 "juniSystemModuleDisableReason", $juniSystemModuleDisableReason, "juniSystemModuleDescr", $juniSystemModuleDescr, "juniSystemSlotNumber", $juniSystemSlotNumber,
                 "juniSystemSlotLevel", $juniSystemSlotLevel)

        case "4": ### juniSystemPowerStatusChange

            ##########
            # $1 = entPhysicalDescr 
            # $2 = juniSystemPowerStatus 
            ##########

            $entPhysicalDescr = $1
            $juniSystemPowerStatus = lookup($2, JuniSystemPowerStatus) + " ( " + $2 + " ) "
            $entPhysicalIndex = extract($OID1, "\.([0-9]+)$")
            $juniSystemPowerIndex = extract($OID2, "\.([0-9]+)$")
            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemPowerStatusChange"

            @AlertGroup = "System Power Status"

            @AlertKey = "entPhysicalEntry." + $entPhysicalIndex + " juniSystemPowerEntry." + $juniSystemPowerIndex

            switch($2)
            {
                case "0": ### notPresent
                    $SEV_KEY = $OS_EventId + "_notPresent"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Power Element: " + $juniSystemPowerIndex + " has been removed fom the chassis"

                case "1": ### inactive
                    $SEV_KEY = $OS_EventId + "_inactive"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Power is not available from the Power Element: " + $juniSystemPowerIndex 

                case "2": ### good
                    $SEV_KEY = $OS_EventId + "_good"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Power Element: " + $juniSystemPowerIndex + " Available"

                case "3": ### failed
                    $SEV_KEY = $OS_EventId + "_failed"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Power Element: " + $juniSystemPowerIndex + " is not working"

                case "4": ### sensorFailed
                    $SEV_KEY = $OS_EventId + "_sensorFailed"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Power Element: " + $juniSystemPowerIndex + " sensor has failed"

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Status of the Power Element: " + $juniSystemPowerIndex + " is not available"

            }
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($entPhysicalDescr,$juniSystemPowerStatus,$entPhysicalIndex,$juniSystemPowerIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "entPhysicalDescr", $entPhysicalDescr, "juniSystemPowerStatus", $juniSystemPowerStatus, "entPhysicalIndex", $entPhysicalIndex,
                 "juniSystemPowerIndex", $juniSystemPowerIndex)

        case "5": ### juniSystemFanStatusChange

            ##########
            # $1 = entPhysicalDescr 
            # $2 = juniSystemFanStatus 
            ##########

            $entPhysicalDescr = $1

            $juniSystemFanStatus = lookup($2, JuniSystemFanStatus) + " ( " + $2 + " ) "
            $entPhysicalIndex = extract($OID1, "\.([0-9]+)$")
            $juniSystemFanIndex = extract($OID2, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemFanStatusChange"

            @AlertGroup = "Fan Sub-System Status"

            @AlertKey = "entPhysicalEntry." + $entPhysicalIndex + " juniSystemFanEntry." + $juniSystemFanIndex

            switch($2)
            {
                case "0": ### failed
                    $SEV_KEY = $OS_EventId + "_failed"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Fan Subsystem: " + $juniSystemFanIndex + " is now non-operational, It has a critical failure or has been removed"

                case "1": ### ok
                    $SEV_KEY = $OS_EventId + "_ok"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    @Summary = "All Components are operational"

                case "2": ### warning
                    $SEV_KEY = $OS_EventId + "_warning"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Fan Subsystem: " + $juniSystemFanIndex + " has a non-critical failure"

               default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Fan Subsystem: " + $juniSystemFanIndex + " status unknown"
            }

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($entPhysicalDescr,$juniSystemFanStatus,$entPhysicalIndex,$juniSystemFanIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "entPhysicalDescr", $entPhysicalDescr, "juniSystemFanStatus", $juniSystemFanStatus, "entPhysicalIndex", $entPhysicalIndex,
                 "juniSystemFanIndex", $juniSystemFanIndex)

        case "6": ### juniSystemTempStatusChange

            ##########
            # $1 = juniSystemTempStatus 
            ##########

            $juniSystemTempStatus = lookup($1, JuniSystemTempStatus) + " ( " + $1 + " ) "

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemTempStatusChange"

            $juniSystemTempIndex = extract($OID1, "\.([0-9]+)$")

            @AlertGroup = "System Temperature Sensor Status"

            @AlertKey = "juniSystemTempEntry." + $juniSystemTempIndex

            switch($1)
            {
                case "1": ### failed
                    $SEV_KEY = $OS_EventId + "_failed"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Temperature Sensor failed for : " + @AlertKey

                case "2": ### tooLow
                    $SEV_KEY = $OS_EventId + "_tooLow"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Temperature Sensor status below nominal range for : " + @AlertKey

                case "3": ### nominal
                    $SEV_KEY = $OS_EventId + "_nominal"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Temperature Sensor status within nominal range for : " + @AlertKey

                case "4": ### tooHigh
                    $SEV_KEY = $OS_EventId + "_tooHigh"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Temperature Sensor status above nominal range for : " + @AlertKey

                case "5": ### tooLowWarning
                    $SEV_KEY = $OS_EventId + "_tooLowWarning"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Temperature Sensor status near lower limit for : " + @AlertKey

                case "6": ### tooHighWarning
                    $SEV_KEY = $OS_EventId + "_tooHighWarning"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Temperature Sensor status near upper limit for : " + @AlertKey

               default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Temperature Sensor status unknown"
            }

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($juniSystemTempStatus,$juniSystemTempIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "juniSystemTempStatus", $juniSystemTempStatus, "juniSystemTempIndex", $juniSystemTempIndex)

        case "7": ### juniSystemTempProtectionStatusChange

            ##########
            # $1 = juniSystemTempProtectionStatus 
            # $2 = juniSystemTempProtectionHoldOffTimeRemaining 
            ##########

            $juniSystemTempProtectionStatus = lookup($1, JuniSystemTempProtectionStatus) + " ( " + $1 + " ) "

            $juniSystemTempProtectionHoldOffTimeRemaining = $2

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemTempProtectionStatusChange"

            @AlertGroup = "Thermal Protection Status"
            @AlertKey = ""
            switch($1)
            {
                case "0": ### off
                    $SEV_KEY = $OS_EventId + "_monitoring"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "No Thermal Protection available"

                case "1": ### monitoring
                    $SEV_KEY = $OS_EventId + "_monitoring"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800
                    @Summary = "Thermal Protection Status is being monitored"

                case "2": ### inHoldOff
                    $SEV_KEY = $OS_EventId + "_inHoldOff"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Thermal Protection Status hold off time has begun"

                case "3": ### activatedHoldOffExpired
                    $SEV_KEY = $OS_EventId + "_activatedHoldOffExpired"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "System is in thermal protection mode, hold off time has expired"

                case "4": ### activatedTempTooHigh
                    $SEV_KEY = $OS_EventId + "_activatedTempTooHigh"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "System is in thermal protection mode, temperature too high"

               default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Thermal Protection Status unknown"
            }

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($juniSystemTempProtectionStatus,$juniSystemTempProtectionHoldOffTimeRemaining)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "juniSystemTempProtectionStatus", $juniSystemTempProtectionStatus, "juniSystemTempProtectionHoldOffTimeRemaining", $juniSystemTempProtectionHoldOffTimeRemaining)

        case "8": ### juniSystemReloadCommand

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemReloadCommand"

            if(match($OID1, ".1.3.6.1.4.1.4874.2.2.2.1.1.20"))
            {
                @AlertKey = $1
                @AlertGroup = "Slot Status"
                $SEV_KEY = $OS_EventId + "_slot"
                @Summary = "Slot: " + $1 + " is about to restart due to a system console reload command"
                $DEFAULT_Severity = 4
                $DEFAULT_Type = 1
                $DEFAULT_ExpireTime = 0
            }
            else
            {
                @AlertKey = ""
                @AlertGroup = "System Status"
                $SEV_KEY = $OS_EventId + "_system"
                @Summary = "System is about to restart due to a system console reload command"
                $DEFAULT_Severity = 5
                $DEFAULT_Type = 1
                $DEFAULT_ExpireTime = 0
            }


            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)

        case "9": ### juniSystemUtilizationThreshold

            ##########
            # $1 = juniSystemUtilizationThresholdDirection 
            # $2 = juniSystemUtilizationMaxCapacity 
            # $3 = juniSystemUtilizationCurrentValue 
            # $4 = juniSystemUtilizationThresholdRising 
            # $5 = juniSystemUtilizationThresholdFalling 
            # $6 = juniSystemUtilizationHoldDownTime 
            ##########

            $juniSystemUtilizationThresholdDirection = lookup($1, JuniSystemUtilizationThresholdDirection) + " ( " + $1 + " ) "
            $juniSystemUtilizationMaxCapacity = $2
            $juniSystemUtilizationCurrentValue = $3
            $juniSystemUtilizationThresholdRising = $4
            $juniSystemUtilizationThresholdFalling = $5
            $juniSystemUtilizationHoldDownTime = $6

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemUtilizationThreshold"

            @AlertGroup = "System Resource Utilization"
            @AlertKey = ""
            @Summary = "System Utilization: Number of units " + $3 + " exceeded " + $2 

            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($juniSystemUtilizationThresholdDirection,$juniSystemUtilizationMaxCapacity,$juniSystemUtilizationCurrentValue,$juniSystemUtilizationThresholdRising,$juniSystemUtilizationThresholdFalling,$juniSystemUtilizationHoldDownTime)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "juniSystemUtilizationThresholdDirection", $juniSystemUtilizationThresholdDirection, "juniSystemUtilizationMaxCapacity", $juniSystemUtilizationMaxCapacity, "juniSystemUtilizationCurrentValue", $juniSystemUtilizationCurrentValue,
                 "juniSystemUtilizationThresholdRising", $juniSystemUtilizationThresholdRising, "juniSystemUtilizationThresholdFalling", $juniSystemUtilizationThresholdFalling, "juniSystemUtilizationHoldDownTime", $juniSystemUtilizationHoldDownTime)

        case "10": ### juniSystemIssuStateChange

            ##########
            # $1 = juniSystemIssuState 
            ##########

            $juniSystemIssuState = lookup($1, JuniSystemIssuState) + " ( " + $1 + " ) "

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemIssuStateChange"

            @AlertGroup = "System State - ISSU Upgrade"

            @AlertKey = ""

            switch($1)
            {
                case "1": ### idle
                    $SEV_KEY = $OS_EventId + "_idle"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800
                    @Summary = "ISSU is currently idle"

                case "2": ### initializing
                    $SEV_KEY = $OS_EventId + "_initializing"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800
                    @Summary = "ISSU initialization is in-progress"

                case "3": ### initialized
                    $SEV_KEY = $OS_EventId + "_initialized"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    @Summary = "ISSU has successfully initialized"

                case "4": ### upgrading
                    $SEV_KEY = $OS_EventId + "_upgrading"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800
                    @Summary = "ISSU is currently upgrading to the new armed release"

                case "5": ### stopping
                    $SEV_KEY = $OS_EventId + "_stopping"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "ISSU is currently in the process of stopping"

               default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "ISSU Status unknown"
            }

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($juniSystemIssuState)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "juniSystemIssuState", $juniSystemIssuState)

        case "11": ### juniSystemPortSfpTxMaxPowerThreshold

            ##########
            # $1 = juniSystemPortSfpPowerThresholdIfIndex 
            # $2 = juniSystemPortSfpPowerThresholdValue 
            ##########

            $juniSystemPortSfpPowerThresholdIfIndex = $1
            $juniSystemPortSfpPowerThresholdValue = $2

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemPortSfpTxMaxPowerThreshold"

            @AlertGroup = "SFP Power Tx Max Threshold Status"
            @AlertKey = "Interface Physical-UID: " + $juniSystemPortSfpPowerThresholdIfIndex
            
            if(match($2, "1.0000"))
            {
             $SEV_KEY = $OS_EventId + "_raised"
             $DEFAULT_Severity = 3
             $DEFAULT_Type = 1
             $DEFAULT_ExpireTime = 0
	     @Summary = "Link SFP's Tx-Max Power Threshold Raised Trap" + " ( " + @AlertKey + " ) "
            }

            if(match($2, "0.0000"))
            {
             $SEV_KEY = $OS_EventId + "_cleared"
             $DEFAULT_Severity = 1
             $DEFAULT_Type = 2
             $DEFAULT_ExpireTime = 0
	     @Summary = "Link SFP's Tx-Max Power Threshold Cleared Trap" + " ( " + @AlertKey + " ) "
            }
	    
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($juniSystemPortSfpPowerThresholdIfIndex,$juniSystemPortSfpPowerThresholdValue)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "juniSystemPortSfpPowerThresholdIfIndex", $juniSystemPortSfpPowerThresholdIfIndex, "juniSystemPortSfpPowerThresholdValue", $juniSystemPortSfpPowerThresholdValue)

        case "12": ### juniSystemPortSfpTxMinPowerThreshold

            ##########
            # $1 = juniSystemPortSfpPowerThresholdIfIndex 
            # $2 = juniSystemPortSfpPowerThresholdValue 
            ##########

            $juniSystemPortSfpPowerThresholdIfIndex = $1
            $juniSystemPortSfpPowerThresholdValue = $2

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemPortSfpTxMinPowerThreshold"

            @AlertGroup = "SFP Power Tx Min Threshold Status"
            @AlertKey = "Interface Physical-UID: " + $juniSystemPortSfpPowerThresholdIfIndex

            if(match($2, "1.0000"))
            {
             $SEV_KEY = $OS_EventId + "_raised"
             $DEFAULT_Severity = 3
             $DEFAULT_Type = 1
             $DEFAULT_ExpireTime = 0
	     @Summary = "Link SFP's Tx-Min Power Threshold Raise Trap" + " ( " + @AlertKey + " ) "
            }

            if(match($2, "0.0000"))
            {
             $SEV_KEY = $OS_EventId + "_cleared"
             $DEFAULT_Severity = 1
             $DEFAULT_Type = 2
             $DEFAULT_ExpireTime = 0
	     @Summary = "Link SFP's Tx-Min Power Threshold Clear Trap" + " ( " + @AlertKey + " ) "
            } 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($juniSystemPortSfpPowerThresholdIfIndex,$juniSystemPortSfpPowerThresholdValue)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "juniSystemPortSfpPowerThresholdIfIndex", $juniSystemPortSfpPowerThresholdIfIndex, "juniSystemPortSfpPowerThresholdValue", $juniSystemPortSfpPowerThresholdValue)

        case "13": ### juniSystemPortSfpRxMaxPowerThreshold

            ##########
            # $1 = juniSystemPortSfpPowerThresholdIfIndex 
            # $2 = juniSystemPortSfpPowerThresholdValue 
            ##########

            $juniSystemPortSfpPowerThresholdIfIndex = $1
            $juniSystemPortSfpPowerThresholdValue = $2

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemPortSfpRxMaxPowerThreshold"

            @AlertGroup = "SFP Power Rx Max Threshold Status"
            @AlertKey = "Interface Physical-UID: " + $juniSystemPortSfpPowerThresholdIfIndex

            if(match($2, "1.0000"))
            {
             $SEV_KEY = $OS_EventId + "_raised"
             $DEFAULT_Severity = 3
             $DEFAULT_Type = 1
             $DEFAULT_ExpireTime = 0
	     @Summary = "Link SFP's Rx-Max Power Threshold Raise Trap" + " ( " + @AlertKey + " ) "
            }

            if(match($2, "0.0000"))
            {
             $SEV_KEY = $OS_EventId + "_cleared"
             $DEFAULT_Severity = 1
             $DEFAULT_Type = 2
             $DEFAULT_ExpireTime = 0
	     @Summary = "Link SFP's Rx-Max Power Threshold Clear Trap" + " ( " + @AlertKey + " ) "
            } 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($juniSystemPortSfpPowerThresholdIfIndex,$juniSystemPortSfpPowerThresholdValue)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "juniSystemPortSfpPowerThresholdIfIndex", $juniSystemPortSfpPowerThresholdIfIndex, "juniSystemPortSfpPowerThresholdValue", $juniSystemPortSfpPowerThresholdValue)

        case "14": ### juniSystemPortSfpRxMinPowerThreshold

            ##########
            # $1 = juniSystemPortSfpPowerThresholdIfIndex 
            # $2 = juniSystemPortSfpPowerThresholdValue 
            ##########

            $juniSystemPortSfpPowerThresholdIfIndex = $1
            $juniSystemPortSfpPowerThresholdValue = $2

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemPortSfpRxMinPowerThreshold"

            @AlertGroup = "SFP Power Rx Min Threshold Status"
            @AlertKey = "Interface Physical-UID: " + $juniSystemPortSfpPowerThresholdIfIndex

            if(match($2, "1.0000"))
            {
             $SEV_KEY = $OS_EventId + "_raised"
             $DEFAULT_Severity = 3
             $DEFAULT_Type = 1
             $DEFAULT_ExpireTime = 0
	     @Summary = "Link SFP's Rx-Min Power Threshold Raise Trap" + " ( " + @AlertKey + " ) "
            }

            if(match($2, "0.0000"))
            {
             $SEV_KEY = $OS_EventId + "_cleared"
             $DEFAULT_Severity = 1
             $DEFAULT_Type = 2
             $DEFAULT_ExpireTime = 0
	     @Summary = "Link SFP's Rx-Min Power Threshold Clear Trap" + " ( " + @AlertKey + " ) "
            }

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($juniSystemPortSfpPowerThresholdIfIndex,$juniSystemPortSfpPowerThresholdValue)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "juniSystemPortSfpPowerThresholdIfIndex", $juniSystemPortSfpPowerThresholdIfIndex, "juniSystemPortSfpPowerThresholdValue", $juniSystemPortSfpPowerThresholdValue)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, juniper-JUNIPER-SYSTEM-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, juniper-JUNIPER-SYSTEM-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-SYSTEM-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-SYSTEM-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... juniper-JUNIPER-SYSTEM-MIB.include.snmptrap.rules >>>>>")
