###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 8.0 - Updated release for JUNOS 9.4, JUNOSe 10.3 and BX-OS 4.1
#
#          - Repackaged for NIM-08
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  APS-MIB-JUNI
#
###############################################################################

case ".1.3.6.1.4.1.4874.3.2.2.1.2"|".1.3.6.1.4.1.4874.3.2.2.1.2.0": ###  - Juniper Networks SONET Linear APS - Notifications from APS-MIB-JUNI (200105242300Z)

## Note: The ".1.3.6.1.4.1.4874.3.2.2.1.2.0" enterprise ID condition has
## been added to the rules to support SNMPV1 traps sent by the SNMP agent
## that have an extra 0 in the enterprise ID. Please contact Juniper Support
## for any additional information.

    log(DEBUG, "<<<<< Entering... juniper-APS-MIB-JUNI.include.snmptrap.rules >>>>>")

    @Agent = "Juniper Networks-Sonet Linear APS"
    @Class = "40200"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### junidApsEventSwitchover

            ##########
            # $1 = junidApsChanStatusSwitchovers 
            # $2 = junidApsChanStatusCurrent 
            ##########

            $junidApsChanStatusSwitchovers = $1
            $junidApsChanStatusCurrent = lookup($2,JunidApsChanStatusCurrent) + " ( " + $2 + " ) "

            $OctetString = extract($OID1,"\.4874\.3\.2\.2\.1\.1\.6\.1\.4\.(.*)\.[0-9]+$")
            include "$NC_RULES_HOME/include-snmptrap/decodeOctetString.include.snmptrap.rules"
            $junidApsChanConfigGroupName = $String
 
            $junidApsChanConfigNumber = extract($OID1,"\.4874\.3\.2\.2\.1\.1\.6\.1\.4\.[0-9]+\..*\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-juniper-APS-MIB-JUNI-junidApsEventSwitchover"

            @AlertGroup = "Channel Switchover"

            @AlertKey = "junidApsChanStatusEntry." + extract($OID1, "\.4874\.3\.2\.2\.1\.1\.6\.1\.4\.(.*)$")

            @Summary = "Channel Switchover, APS Group: " + $junidApsChanConfigGroupName + ", Current Channel Status: " + lookup($2,JunidApsChanStatusCurrent) + "  ( " + @AlertKey + " ) "

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($junidApsChanStatusSwitchovers,$junidApsChanStatusCurrent,$junidApsChanConfigGroupName,$junidApsChanConfigNumber)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "junidApsChanStatusSwitchovers", $junidApsChanStatusSwitchovers, "junidApsChanStatusCurrent", $junidApsChanStatusCurrent, "junidApsChanConfigGroupName", $junidApsChanConfigGroupName,
                 "junidApsChanConfigNumber", $junidApsChanConfigNumber)

        case "2": ### junidApsEventModeMismatch

            ##########
            # $1 = junidApsStatusModeMismatches 
            # $2 = junidApsStatusCurrent 
            ##########

            $junidApsStatusModeMismatches = $1
            $junidApsStatusCurrent = lookup($2,JunidApsStatusCurrent) + " ( " + $2 + " ) "

            $OctetString = extract($OID1,"\.4874\.3\.2\.2\.1\.1\.2\.1\.4\.(.*)$")
            include "$NC_RULES_HOME/include-snmptrap/decodeOctetString.include.snmptrap.rules"
            $junidApsConfigName = $String


            $OS_EventId = "SNMPTRAP-juniper-APS-MIB-JUNI-junidApsEventModeMismatch"

            @AlertGroup = "Mode Mismatch"
            @AlertKey = "junidApsStatusEntry." + extract($OID1, "\.4874\.3\.2\.2\.1\.1\.2\.1\.4\.(.*)$")
            @Summary = "Received K2 Mode Information Conflict with Current Local Mode, APS Config Name: " + $junidApsConfigName + ", APS Current Status: " + lookup($2,JunidApsStatusCurrent) + "  ( " + @AlertKey + " ) "

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($junidApsStatusModeMismatches,$junidApsStatusCurrent,$junidApsConfigName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "junidApsStatusModeMismatches", $junidApsStatusModeMismatches, "junidApsStatusCurrent", $junidApsStatusCurrent, "junidApsConfigName", $junidApsConfigName)

        case "3": ### junidApsEventChannelMismatch

            ##########
            # $1 = junidApsStatusChannelMismatches 
            # $2 = junidApsStatusCurrent 
            ##########

            $junidApsStatusChannelMismatches = $1
            $junidApsStatusCurrent = lookup($2,JunidApsStatusCurrent) + " ( " + $2 + " ) "

            $OctetString = extract($OID1,"\.4874\.3\.2\.2\.1\.1\.2\.1\.5\.(.*)$")
            include "$NC_RULES_HOME/include-snmptrap/decodeOctetString.include.snmptrap.rules"
            $junidApsConfigName = $String

            $OS_EventId = "SNMPTRAP-juniper-APS-MIB-JUNI-junidApsEventChannelMismatch"

            @AlertGroup = "Channel Mismatch"
            @AlertKey = "junidApsStatusEntry." + extract($OID1, "\.4874\.3\.2\.2\.1\.1\.2\.1\.5\.(.*)$")
            @Summary = "Transmitted K1 Channel and Received K2 Channel Mismatch, APS Config Name: " + $junidApsConfigName + ", APS Current Status: " + lookup($2,JunidApsStatusCurrent)  + "  ( " + @AlertKey + " ) "

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($junidApsStatusChannelMismatches,$junidApsStatusCurrent,$junidApsConfigName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "junidApsStatusChannelMismatches", $junidApsStatusChannelMismatches, "junidApsStatusCurrent", $junidApsStatusCurrent, "junidApsConfigName", $junidApsConfigName)

        case "4": ### junidApsEventPSBF

            ##########
            # $1 = junidApsStatusPSBFs 
            # $2 = junidApsStatusCurrent 
            ##########

            $junidApsStatusPSBFs = $1
            $junidApsStatusCurrent = lookup($2,JunidApsStatusCurrent) + " ( " + $2 + " ) "

            $OctetString = extract($OID1,"\.4874\.3\.2\.2\.1\.1\.2\.1\.6\.(.*)$")
            include "$NC_RULES_HOME/include-snmptrap/decodeOctetString.include.snmptrap.rules"
            $junidApsConfigName = $String

            $OS_EventId = "SNMPTRAP-juniper-APS-MIB-JUNI-junidApsEventPSBF"

            @AlertGroup = "Protection Switch Byte"
            @AlertKey = "junidApsStatusEntry." + extract($OID1, "\.4874\.3\.2\.2\.1\.1\.2\.1\.6\.(.*)$")
            @Summary = "Protection Switch Byte Failure, APS Config Name: " + $junidApsConfigName + ", APS Current Status: " + lookup($2,JunidApsStatusCurrent)  + "  ( " + @AlertKey + " ) "

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($junidApsStatusPSBFs,$junidApsStatusCurrent,$junidApsConfigName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "junidApsStatusPSBFs", $junidApsStatusPSBFs, "junidApsStatusCurrent", $junidApsStatusCurrent, "junidApsConfigName", $junidApsConfigName)

        case "5": ### junidApsEventFEPLF

            ##########
            # $1 = junidApsStatusFEPLFs 
            # $2 = junidApsStatusCurrent 
            ##########

            $junidApsStatusFEPLFs = $1
            $junidApsStatusCurrent = lookup($2,JunidApsStatusCurrent) + " ( " + $2 + " ) "

            $OctetString = extract($OID1,"\.4874\.3\.2\.2\.1\.1\.2\.1\.7\.(.*)$")
            include "$NC_RULES_HOME/include-snmptrap/decodeOctetString.include.snmptrap.rules"
            $junidApsConfigName = $String

            $OS_EventId = "SNMPTRAP-juniper-APS-MIB-JUNI-junidApsEventFEPLF"

            @AlertGroup = "Far End Protection Line"
            @AlertKey = "junidApsStatusEntry." + extract($OID1, "\.4874\.3\.2\.2\.1\.1\.2\.1\.7\.(.*)$")
            @Summary = "Far End Protection Line Failure, APS Config Name: " + $junidApsConfigName + ", APS Current Status: " + lookup($2,JunidApsStatusCurrent)  + "  ( " + @AlertKey + " ) "

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($junidApsStatusFEPLFs,$junidApsStatusCurrent,$junidApsConfigName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "junidApsStatusFEPLFs", $junidApsStatusFEPLFs, "junidApsStatusCurrent", $junidApsStatusCurrent, "junidApsConfigName", $junidApsConfigName)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, juniper-APS-MIB-JUNI_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, juniper-APS-MIB-JUNI_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-APS-MIB-JUNI.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-APS-MIB-JUNI.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... juniper-APS-MIB-JUNI.include.snmptrap.rules >>>>>")
