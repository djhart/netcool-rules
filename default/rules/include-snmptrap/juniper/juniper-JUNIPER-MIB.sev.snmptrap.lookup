###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  JUNIPER-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table juniper-JUNIPER-MIB_sev =
{
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxPowerSupplyFailure","4","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxFanFailure","4","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxOverTemperature","4","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxRedundancySwitchover_other","3","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxRedundancySwitchover_neverSwitched","3","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxRedundancySwitchover_userSwitched","3","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxRedundancySwitchover_autoSwitched","3","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxRedundancySwitchover_unknown","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxFruRemoval","4","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxFruInsertion","1","2","0"},
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxFruPowerOff","4","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxFruPowerOn","1","2","0"},
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxFruFailed","4","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxFruOffline","4","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxFruOnline","1","2","0"},
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxFruCheck","3","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxFEBSwitchover","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxHardDiskFailed","4","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxHardDiskMissing","4","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxBootFromBackup","3","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxFmLinkErr","3","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxFmCellDropErr","4","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxExtSrcLockLost","3","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxPowerSupplyOK","1","2","0"},
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxFanOK","1","2","0"},
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxTemperatureOK","1","2","0"},
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxFruOK","1","2","0"},
    {"SNMPTRAP-juniper-JUNIPER-MIB-jnxExtSrcLockAcquired","1","2","0"}
}
default = {"Unknown","Unknown","Unknown"}

