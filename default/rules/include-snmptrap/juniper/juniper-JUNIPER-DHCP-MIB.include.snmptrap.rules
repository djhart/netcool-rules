###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 2010/15/02 Alka Girdhar
# 8.0 - Updated release for JUNOS 9.4, JUNOSe 10.3 and BX-OS 4.1
#
#          - Repackaged for NIM-08
#
###############################################################################
#
# 1.0 - Initial Release.
#
# Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
# - JUNIPER-DHCP-MIB
#
###############################################################################

case ".1.3.6.1.4.1.4874.2.2.22.1.3": ###  -  Notifications from JUNIPER-DHCP-MIB (200701312038Z)

    log(DEBUG, "<<<<< Entering... juniper-JUNIPER-DHCP-MIB.include.snmptrap.rules >>>>>")

    @Agent = "JUNIPER-DHCP-MIB"
    @Class = "40200"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### juniDhcpLocalServerPoolHighAddrUtil

            ##########
            # $1 = juniRouterName 
            # $2 = juniDhcpLocalServerPoolSize 
            # $3 = juniDhcpLocalServerPoolInUse 
            # $4 = juniDhcpLocalServerPoolUtilPct 
            ##########

            $juniRouterName = $1
            $juniDhcpLocalServerPoolSize = $2
            $juniDhcpLocalServerPoolInUse = $3
            $juniDhcpLocalServerPoolUtilPct = $4

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-DHCP-MIB-juniDhcpLocalServerPoolHighAddrUtil"

            $juniRouterIndex = extract($OID1, "\.([0-9]+)$")
            $juniDhcpLocalServerPoolName = extract($OID2, "\.4874\.2\.2\.22\.1\.3\.3\.1\.1.(.*)$")
            
            @AlertGroup = "Address Pool Utilization"
            
            @AlertKey = "juniRouterEntry." + $juniRouterIndex + " juniDhcpLocalServerPoolEntry." + $juniDhcpLocalServerPoolName
           
            @Summary = "Address Pool: " + $juniDhcpLocalServerPoolName + " has reached the configured high utilization threshold for router " + $juniRouterName 

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($juniRouterName,$juniDhcpLocalServerPoolSize,$juniDhcpLocalServerPoolInUse,$juniDhcpLocalServerPoolUtilPct,$juniRouterIndex,$juniDhcpLocalServerPoolName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "juniRouterName", $juniRouterName, "juniDhcpLocalServerPoolSize", $juniDhcpLocalServerPoolSize, "juniDhcpLocalServerPoolInUse", $juniDhcpLocalServerPoolInUse,
                 "juniDhcpLocalServerPoolUtilPct", $juniDhcpLocalServerPoolUtilPct, "juniRouterIndex", $juniRouterIndex, "juniDhcpLocalServerPoolName", $juniDhcpLocalServerPoolName)

        case "2": ### juniDhcpLocalServerPoolAbatedAddrUtil

            ##########
            # $1 = juniRouterName 
            # $2 = juniDhcpLocalServerPoolSize 
            # $3 = juniDhcpLocalServerPoolInUse 
            # $4 = juniDhcpLocalServerPoolUtilPct 
            ##########

            $juniRouterName = $1
            $juniDhcpLocalServerPoolSize = $2
            $juniDhcpLocalServerPoolInUse = $3
            $juniDhcpLocalServerPoolUtilPct = $4

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-DHCP-MIB-juniDhcpLocalServerPoolAbatedAddrUtil"

            $juniRouterIndex = extract($OID1, "\.([0-9]+)$")
            $juniDhcpLocalServerPoolName = extract($OID2, "\.4874\.2\.2\.22\.1\.3\.3\.1\.1.(.*)$")
            
            @AlertGroup = "Address Pool Utilization"
            
            @AlertKey = "juniRouterEntry." + $juniRouterIndex + " juniDhcpLocalServerPoolEntry." + $juniDhcpLocalServerPoolName
           
            @Summary = "Address Pool: " + $juniDhcpLocalServerPoolName + " utilization has fallen to the configured abated threshold level for router " + $juniRouterName 

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($juniRouterName,$juniDhcpLocalServerPoolSize,$juniDhcpLocalServerPoolInUse,$juniDhcpLocalServerPoolUtilPct,$juniRouterIndex,$juniDhcpLocalServerPoolName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "juniRouterName", $juniRouterName, "juniDhcpLocalServerPoolSize", $juniDhcpLocalServerPoolSize, "juniDhcpLocalServerPoolInUse", $juniDhcpLocalServerPoolInUse,
                 "juniDhcpLocalServerPoolUtilPct", $juniDhcpLocalServerPoolUtilPct, "juniRouterIndex", $juniRouterIndex, "juniDhcpLocalServerPoolName", $juniDhcpLocalServerPoolName)

        case "3": ### juniDhcpLocalServerPoolNoAddresses

            ##########
            # $1 = juniRouterName 
            # $2 = juniDhcpLocalServerPoolSize 
            ##########

            $juniRouterName = $1
            $juniDhcpLocalServerPoolSize = $2

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-DHCP-MIB-juniDhcpLocalServerPoolNoAddresses"

            $juniRouterIndex = extract($OID1, "\.([0-9]+)$")
            $juniDhcpLocalServerPoolName = extract($OID2, "\.4874\.2\.2\.22\.1\.3\.3\.1\.1.(.*)$")
            
            @AlertGroup = "Address Pool Status"
            
            @AlertKey = "juniRouterEntry." + $juniRouterIndex + " juniDhcpLocalServerPoolEntry." + $juniDhcpLocalServerPoolName
           
            @Summary = "Address Pool: " + $juniDhcpLocalServerPoolName + " has exhausted its supply of addresses for router " + $juniRouterName 

            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($juniRouterName,$juniDhcpLocalServerPoolSize,$juniRouterIndex,$juniDhcpLocalServerPoolName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "juniRouterName", $juniRouterName, "juniDhcpLocalServerPoolSize", $juniDhcpLocalServerPoolSize, "juniRouterIndex", $juniRouterIndex,
                 "juniDhcpLocalServerPoolName", $juniDhcpLocalServerPoolName)

        case "4": ### juniDhcpLocalServerHealth

            ##########
            # $1 = juniRouterName 
            # $2 = juniDhcpLocalServerEventSeverity 
            # $3 = juniDhcpLocalServerEventString 
            ##########

            $juniRouterName = $1
            $juniDhcpLocalServerEventSeverity = lookup($2, JuniLogSeverity)
            $juniDhcpLocalServerEventString = $3

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-DHCP-MIB-juniDhcpLocalServerHealth"
            $SEV_KEY = $OS_EventId + "-" + $juniDhcpLocalServerEventSeverity

            $juniRouterIndex = extract($OID1, "\.([0-9]+)$")
            
            @AlertGroup = "DhcpLocalServer Health Event"
            @AlertKey = "juniRouterEntry." + $juniRouterIndex

            switch($2)
            {
                        case "-1": ### off
                            $SEV_KEY = $OS_EventId + "_off"
                            @Summary = "Health event " + $juniDhcpLocalServerEventSeverity + " occurs in the DhcpLocalServer application on the router " + $juniRouterName

                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "0": ### emergency
                            $SEV_KEY = $OS_EventId + "_emergency"
                            @Summary = "Health event " + $juniDhcpLocalServerEventSeverity + " occurs in the DhcpLocalServer application on the router " + $juniRouterName

                            $DEFAULT_Severity = 5
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "1": ### alert
                            $SEV_KEY = $OS_EventId + "_alert"
                            @Summary = "Health event " + $juniDhcpLocalServerEventSeverity + " occurs in the DhcpLocalServer application on the router " + $juniRouterName

                            $DEFAULT_Severity = 4
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "2": ### critical
                            $SEV_KEY = $OS_EventId + "_critical"
                            @Summary = "Health event " + $juniDhcpLocalServerEventSeverity + " occurs in the DhcpLocalServer application on the router " + $juniRouterName

                            $DEFAULT_Severity = 4
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "3": ### error
                            $SEV_KEY = $OS_EventId + "_error"
                            @Summary = "Health event " + $juniDhcpLocalServerEventSeverity + " occurs in the DhcpLocalServer application on the router " + $juniRouterName

                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "4": ### warning
                            $SEV_KEY = $OS_EventId + "_warning"
                            @Summary = "Health event " + $juniDhcpLocalServerEventSeverity + " occurs in the DhcpLocalServer application on the router " + $juniRouterName

                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "5": ### notice
                            $SEV_KEY = $OS_EventId + "_notice"
                            @Summary = "Health event " + $juniDhcpLocalServerEventSeverity + " occurs in the DhcpLocalServer application on the router " + $juniRouterName

                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "6": ### info
                            $SEV_KEY = $OS_EventId + "_info"
                            @Summary = "Health event " + $juniDhcpLocalServerEventSeverity + " occurs in the DhcpLocalServer application on the router " + $juniRouterName

                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 13
                            $DEFAULT_ExpireTime = 1800

                        case "7": ### debug
                            $SEV_KEY = $OS_EventId + "_debug"
                            @Summary = "Health debug" + $juniDhcpLocalServerEventSeverity + " occurs in the DhcpLocalServer application on the router " + $juniRouterName

                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 13
                            $DEFAULT_ExpireTime = 1800

                        default:
                            $SEV_KEY = $OS_EventId + "_unknown"
                            @Summary = "Health Event Unknown"
                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0
            }


            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            $juniDhcpLocalServerEventSeverity = $juniDhcpLocalServerEventSeverity  + " ( " + $2 + " ) "
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($juniRouterName,$juniDhcpLocalServerEventSeverity,$juniDhcpLocalServerEventString,$juniRouterIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "juniRouterName", $juniRouterName, "juniDhcpLocalServerEventSeverity", $juniDhcpLocalServerEventSeverity, "juniDhcpLocalServerEventString", $juniDhcpLocalServerEventString,
                 "juniRouterIndex", $juniRouterIndex)

        case "5": ### juniDhcpLocalServerPoolGroupHighAddrUtil

            ##########
            # $1 = juniRouterName 
            # $2 = juniDhcpLocalServerPoolGroupSize 
            # $3 = juniDhcpLocalServerPoolGroupInUse 
            # $4 = juniDhcpLocalServerPoolGroupUtilPct 
            ##########

            $juniRouterName = $1
            $juniDhcpLocalServerPoolGroupSize = $2
            $juniDhcpLocalServerPoolGroupInUse = $3
            $juniDhcpLocalServerPoolGroupUtilPct = $4

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-DHCP-MIB-juniDhcpLocalServerPoolGroupHighAddrUtil"

            $juniRouterIndex = extract($OID1, "\.([0-9]+)$")
            $juniDhcpLocalServerPoolGroupName = extract($OID2, "\.4874\.2\.2\.22\.1\.3\.8\.1\.1.(.*)$")
            @AlertGroup = "Address Pool Group Utilization"
            
            @AlertKey = "juniRouterEntry." + $juniRouterIndex + " juniDhcpLocalServerPoolGroupEntry." + $juniDhcpLocalServerPoolGroupName
            
            @Summary = "Address Pool Group: " + $juniDhcpLocalServerPoolGroupName + " has reached the configured high utilization threshold for router " + $juniRouterName

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($juniRouterName,$juniDhcpLocalServerPoolGroupSize,$juniDhcpLocalServerPoolGroupInUse,$juniDhcpLocalServerPoolGroupUtilPct,$juniRouterIndex,$juniDhcpLocalServerPoolGroupName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "juniRouterName", $juniRouterName, "juniDhcpLocalServerPoolGroupSize", $juniDhcpLocalServerPoolGroupSize, "juniDhcpLocalServerPoolGroupInUse", $juniDhcpLocalServerPoolGroupInUse,
                 "juniDhcpLocalServerPoolGroupUtilPct", $juniDhcpLocalServerPoolGroupUtilPct, "juniRouterIndex", $juniRouterIndex, "juniDhcpLocalServerPoolGroupName", $juniDhcpLocalServerPoolGroupName)

        case "6": ### juniDhcpLocalServerPoolGroupAbatedAddrUtil

            ##########
            # $1 = juniRouterName 
            # $2 = juniDhcpLocalServerPoolGroupSize 
            # $3 = juniDhcpLocalServerPoolGroupInUse 
            # $4 = juniDhcpLocalServerPoolGroupUtilPct 
            ##########

            $juniRouterName = $1
            $juniDhcpLocalServerPoolGroupSize = $2
            $juniDhcpLocalServerPoolGroupInUse = $3
            $juniDhcpLocalServerPoolGroupUtilPct = $4

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-DHCP-MIB-juniDhcpLocalServerPoolGroupAbatedAddrUtil"

            $juniRouterIndex = extract($OID1, "\.([0-9]+)$")
            $juniDhcpLocalServerPoolGroupName = extract($OID2, "\.4874\.2\.2\.22\.1\.3\.8\.1\.1.(.*)$")
            @AlertGroup = "Address Pool Group Utilization"
            
            @AlertKey = "juniRouterEntry." + $juniRouterIndex + " juniDhcpLocalServerPoolGroupEntry." + $juniDhcpLocalServerPoolGroupName
            
            @Summary = "Address Pool Group: " + $juniDhcpLocalServerPoolGroupName + " utilization has fallen to the configured abated threshold level for router " + $juniRouterName

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($juniRouterName,$juniDhcpLocalServerPoolGroupSize,$juniDhcpLocalServerPoolGroupInUse,$juniDhcpLocalServerPoolGroupUtilPct,$juniRouterIndex,$juniDhcpLocalServerPoolGroupName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "juniRouterName", $juniRouterName, "juniDhcpLocalServerPoolGroupSize", $juniDhcpLocalServerPoolGroupSize, "juniDhcpLocalServerPoolGroupInUse", $juniDhcpLocalServerPoolGroupInUse,
                 "juniDhcpLocalServerPoolGroupUtilPct", $juniDhcpLocalServerPoolGroupUtilPct, "juniRouterIndex", $juniRouterIndex, "juniDhcpLocalServerPoolGroupName", $juniDhcpLocalServerPoolGroupName)

        case "7": ### juniDhcpLocalServerPoolGroupNoAddresses

            ##########
            # $1 = juniRouterName 
            # $2 = juniDhcpLocalServerPoolGroupSize 
            ##########

            $juniRouterName = $1
            $juniDhcpLocalServerPoolGroupSize = $2

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-DHCP-MIB-juniDhcpLocalServerPoolGroupNoAddresses"

            $juniRouterIndex = extract($OID1, "\.([0-9]+)$")
            $juniDhcpLocalServerPoolGroupName = extract($OID2, "\.4874\.2\.2\.22\.1\.3\.8\.1\.1.(.*)$")
            @AlertGroup = "Address Pool Group Status"
            
            @AlertKey = "juniRouterEntry." + $juniRouterIndex + " juniDhcpLocalServerPoolGroupEntry." + $juniDhcpLocalServerPoolGroupName
            
            @Summary = "Address Pool Group: " + $juniDhcpLocalServerPoolGroupName + " has exhausted its supply of addresses for router " + $juniRouterName

            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($juniRouterName,$juniDhcpLocalServerPoolGroupSize,$juniRouterIndex,$juniDhcpLocalServerPoolGroupName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "juniRouterName", $juniRouterName, "juniDhcpLocalServerPoolGroupSize", $juniDhcpLocalServerPoolGroupSize, "juniRouterIndex", $juniRouterIndex,
                 "juniDhcpLocalServerPoolGroupName", $juniDhcpLocalServerPoolGroupName)

        case "8": ### juniDhcpLocalServerInterfaceLimitExceeded

            ##########
            # $1 = juniRouterName 
            # $2 = juniDhcpLocalServerSubInterfaceName 
            # $3 = juniDhcpLocalServerSubInterfaceLimit 
            ##########

            $juniRouterName = $1
            $juniDhcpLocalServerSubInterfaceName = $2
            $juniDhcpLocalServerSubInterfaceLimit = $3

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-DHCP-MIB-juniDhcpLocalServerInterfaceLimitExceeded"

            $juniRouterIndex = extract($OID1, "\.([0-9]+)$")
            $juniDhcpLocalServerSubInterfaceIndex = extract($OID2, "\.([0-9]+)$")
            
            @AlertGroup = "Sub-Interface Limit"
            
            @AlertKey = "juniRouterEntry." + $juniRouterIndex + " juniDhcpLocalServerSubInterfaceEntry." + $juniDhcpLocalServerSubInterfaceIndex
            
            @Summary = "Sub-Interface: " + $juniDhcpLocalServerSubInterfaceName + "has exceeded the limit of clients allowed for router " + $juniRouterName 

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($juniRouterName,$juniDhcpLocalServerSubInterfaceName,$juniDhcpLocalServerSubInterfaceLimit,$juniRouterIndex,$juniDhcpLocalServerSubInterfaceIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "juniRouterName", $juniRouterName, "juniDhcpLocalServerSubInterfaceName", $juniDhcpLocalServerSubInterfaceName, "juniDhcpLocalServerSubInterfaceLimit", $juniDhcpLocalServerSubInterfaceLimit,
                 "juniRouterIndex", $juniRouterIndex, "juniDhcpLocalServerSubInterfaceIndex", $juniDhcpLocalServerSubInterfaceIndex)

        case "9": ### juniDhcpLocalServerInterfaceLimitAbated

            ##########
            # $1 = juniRouterName 
            # $2 = juniDhcpLocalServerSubInterfaceName 
            # $3 = juniDhcpLocalServerSubInterfaceLimit 
            ##########

            $juniRouterName = $1
            $juniDhcpLocalServerSubInterfaceName = $2
            $juniDhcpLocalServerSubInterfaceLimit = $3

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-DHCP-MIB-juniDhcpLocalServerInterfaceLimitAbated"

            $juniRouterIndex = extract($OID1, "\.([0-9]+)$")
            $juniDhcpLocalServerSubInterfaceIndex = extract($OID2, "\.([0-9]+)$")
            
            @AlertGroup = "Sub-Interface Limit"
            
            @AlertKey = "juniRouterEntry." + $juniRouterIndex + " juniDhcpLocalServerSubInterfaceEntry." + $juniDhcpLocalServerSubInterfaceIndex
            
            @Summary = "Number of clients on a Sub-Interface: " + $juniDhcpLocalServerSubInterfaceName + "has fallen below the limit of clients allowed for router " + $juniRouterName 

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($juniRouterName,$juniDhcpLocalServerSubInterfaceName,$juniDhcpLocalServerSubInterfaceLimit,$juniRouterIndex,$juniDhcpLocalServerSubInterfaceIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "juniRouterName", $juniRouterName, "juniDhcpLocalServerSubInterfaceName", $juniDhcpLocalServerSubInterfaceName, "juniDhcpLocalServerSubInterfaceLimit", $juniDhcpLocalServerSubInterfaceLimit,
                 "juniRouterIndex", $juniRouterIndex, "juniDhcpLocalServerSubInterfaceIndex", $juniDhcpLocalServerSubInterfaceIndex)

        case "10": ### juniDhcpLocalServerDuplicateClient

            ##########
            # $1 = juniRouterName 
            # $2 = juniDhcpLocalServerMacAddress 
            # $3 = juniDhcpLocalServerInterfaceString 
            # $4 = juniDhcpLocalServerLastDetected 
            ##########

            $juniRouterName = $1
            $juniDhcpLocalServerMacAddress = $2
            $juniDhcpLocalServerInterfaceString = $3
            $juniDhcpLocalServerLastDetected = $4

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-DHCP-MIB-juniDhcpLocalServerDuplicateClient"

            $juniRouterIndex = extract($OID1, "\.([0-9]+)$")
            @AlertGroup = "Duplicate DHCP Client"
            
            @AlertKey = "juniRouterEntry." + $juniRouterIndex + " DHCP Client's MAC Address: " + $juniDhcpLocalServerMacAddress
  
            @Summary = "DHCP Client: " + $juniDhcpLocalServerMacAddress + " that changed interfaces on router " + $juniRouterName 

            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($juniRouterName,$juniDhcpLocalServerMacAddress,$juniDhcpLocalServerInterfaceString,$juniDhcpLocalServerLastDetected,$juniRouterIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "juniRouterName", $juniRouterName, "juniDhcpLocalServerMacAddress", $juniDhcpLocalServerMacAddress, "juniDhcpLocalServerInterfaceString", $juniDhcpLocalServerInterfaceString,
                 "juniDhcpLocalServerLastDetected", $juniDhcpLocalServerLastDetected, "juniRouterIndex", $juniRouterIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, juniper-JUNIPER-DHCP-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, juniper-JUNIPER-DHCP-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-DHCP-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-DHCP-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... juniper-JUNIPER-DHCP-MIB.include.snmptrap.rules >>>>>")
