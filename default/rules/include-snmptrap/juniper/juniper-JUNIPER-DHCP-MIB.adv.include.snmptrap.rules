###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 8.0 - Updated release for JUNOS 9.4, JUNOSe 10.3 and BX-OS 4.1
#
#          - Repackaged for NIM-08
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  JUNIPER-DHCP-MIB
#
###############################################################################

log(DEBUG, "<<<<< Entering... juniper-JUNIPER-DHCP-MIB.adv.include.snmptrap.rules >>>>>")

switch($specific-trap)
{
    case "1": ### juniDhcpLocalServerPoolHighAddrUtil

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "juniDhcpLocalServerPoolHighAddrUtil"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "juniRouterEntry." + $juniRouterIndex + " juniDhcpLocalServerPoolEntry." + $juniDhcpLocalServerPoolName 
        $OS_LocalRootObj = "juniRouterEntry." + $juniRouterIndex
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "2": ### juniDhcpLocalServerPoolAbatedAddrUtil

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "juniDhcpLocalServerPoolAbatedAddrUtil"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "juniRouterEntry." + $juniRouterIndex + " juniDhcpLocalServerPoolEntry." + $juniDhcpLocalServerPoolName 
        $OS_LocalRootObj = "juniRouterEntry." + $juniRouterIndex
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "3": ### juniDhcpLocalServerPoolNoAddresses

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "juniDhcpLocalServerPoolNoAddresses"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "juniRouterEntry." + $juniRouterIndex + " juniDhcpLocalServerPoolEntry." + $juniDhcpLocalServerPoolName 
        $OS_LocalRootObj = "juniRouterEntry." + $juniRouterIndex
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "4": ### juniDhcpLocalServerHealth

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "juniDhcpLocalServerHealth"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "juniRouterEntry." + $juniRouterIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "5": ### juniDhcpLocalServerPoolGroupHighAddrUtil

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "juniDhcpLocalServerPoolGroupHighAddrUtil"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "juniRouterEntry." + $juniRouterIndex + " juniDhcpLocalServerPoolGroupEntry." + $juniDhcpLocalServerPoolGroupName 
        $OS_LocalRootObj = "juniRouterEntry." + $juniRouterIndex
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "6": ### juniDhcpLocalServerPoolGroupAbatedAddrUtil

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "juniDhcpLocalServerPoolGroupAbatedAddrUtil"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "juniRouterEntry." + $juniRouterIndex + " juniDhcpLocalServerPoolGroupEntry." + $juniDhcpLocalServerPoolGroupName 
        $OS_LocalRootObj = "juniRouterEntry." + $juniRouterIndex
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "7": ### juniDhcpLocalServerPoolGroupNoAddresses

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "juniDhcpLocalServerPoolGroupNoAddresses"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "juniRouterEntry." + $juniRouterIndex + " juniDhcpLocalServerPoolGroupEntry." + $juniDhcpLocalServerPoolGroupName 
        $OS_LocalRootObj = "juniRouterEntry." + $juniRouterIndex
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "8": ### juniDhcpLocalServerInterfaceLimitExceeded

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "juniDhcpLocalServerInterfaceLimitExceeded"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "juniRouterEntry." + $juniRouterIndex + " juniDhcpLocalServerSubInterfaceEntry." + $juniDhcpLocalServerSubInterfaceIndex 
        $OS_LocalRootObj = "juniRouterEntry." + $juniRouterIndex
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "9": ### juniDhcpLocalServerInterfaceLimitAbated

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "juniDhcpLocalServerInterfaceLimitAbated"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "juniRouterEntry." + $juniRouterIndex + " juniDhcpLocalServerSubInterfaceEntry." + $juniDhcpLocalServerSubInterfaceIndex 
        $OS_LocalRootObj = "juniRouterEntry." + $juniRouterIndex
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "10": ### juniDhcpLocalServerDuplicateClient

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "juniDhcpLocalServerDuplicateClient"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "juniRouterEntry." + $juniRouterIndex + " DHCP Client's MAC Address: " + $juniDhcpLocalServerMacAddress 
        $OS_LocalRootObj = "juniRouterEntry." + $juniRouterIndex
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    default:
}

log(DEBUG, "<<<<< Leaving... juniper-JUNIPER-DHCP-MIB.adv.include.snmptrap.rules >>>>>")
