###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 9.0 - Updated release for JUNOS 10.4, JUNOSe 11.3
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  JUNIPER-OTN-MIB
#
###############################################################################

case ".1.3.6.1.4.1.2636.4.15": ###  - Notifications from JUNIPER-OTN-MIB (200807100931Z)

    log(DEBUG, "<<<<< Entering... juniper-JUNIPER-OTN-MIB.include.snmptrap.rules >>>>>")

    @Agent = "JUNIPER-OTN-MIB"
    @Class = "40200"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### jnxOtnAlarmSet

            ##########
            # $1 = ifDescr 
            # $2 = jnxOtnLastAlarmId 
            # $3 = jnxOtnCurrentAlarms 
            # $4 = jnxOtnLastAlarmDate 
            ##########

            $ifDescr = $1
            $jnxOtnLastAlarmId = lookup($2, JnxOtnAlarmId)
            $jnxOtnCurrentAlarms = lookup($3, JnxOtnAlarmId)
            $HexTimeValue = $4_hex
            include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-OTN-MIB-decodeTimeValue.include.snmptrap.rules" 
            $jnxOtnLastAlarmDate = $decodedTimeValue

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-OTN-MIB-jnxOtnAlarmSet"

            @AlertGroup = "OTN Alarm Status"
            $ifIndex = extract($OID2, "\.([0-9]+)$")
            @AlertKey = "jnxOtnAlarmEntry." + $ifIndex
            @Summary = "OTN Alarm Recently Set" + " ( " + @AlertKey + " ) "

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($ifDescr,$jnxOtnLastAlarmId,$jnxOtnCurrentAlarms,$jnxOtnLastAlarmDate,$ifIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "ifDescr", $ifDescr, "jnxOtnLastAlarmId", $jnxOtnLastAlarmId, "jnxOtnCurrentAlarms", $jnxOtnCurrentAlarms,
                 "jnxOtnLastAlarmDate", $jnxOtnLastAlarmDate, "ifIndex", $ifIndex)

        case "2": ### jnxOtnAlarmCleared

            ##########
            # $1 = ifDescr 
            # $2 = jnxOtnLastAlarmId 
            # $3 = jnxOtnCurrentAlarms 
            # $4 = jnxOtnLastAlarmDate 
            ##########

            $ifDescr = $1
            $jnxOtnLastAlarmId = $2
            $jnxOtnCurrentAlarms = $3
            $HexTimeValue = $4_hex
            include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-OTN-MIB-decodeTimeValue.include.snmptrap.rules" 
            $jnxOtnLastAlarmDate = $decodedTimeValue

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-OTN-MIB-jnxOtnAlarmCleared"

            @AlertGroup = "OTN Alarm Status"
            $ifIndex = extract($OID2, "\.([0-9]+)$")
            @AlertKey = "jnxOtnAlarmEntry." + $ifIndex
            @Summary = "OTN Alarm Recently Cleared" + " ( " + @AlertKey + " ) "

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($ifDescr,$jnxOtnLastAlarmId,$jnxOtnCurrentAlarms,$jnxOtnLastAlarmDate,$ifIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "ifDescr", $ifDescr, "jnxOtnLastAlarmId", $jnxOtnLastAlarmId, "jnxOtnCurrentAlarms", $jnxOtnCurrentAlarms,
                 "jnxOtnLastAlarmDate", $jnxOtnLastAlarmDate, "ifIndex", $ifIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, juniper-JUNIPER-OTN-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, juniper-JUNIPER-OTN-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-OTN-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-OTN-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... juniper-JUNIPER-OTN-MIB.include.snmptrap.rules >>>>>")
