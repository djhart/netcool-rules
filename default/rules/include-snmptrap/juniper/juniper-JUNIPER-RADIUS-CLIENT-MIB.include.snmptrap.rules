###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 8.0 - Updated release for JUNOS 9.4, JUNOSe 10.3 and BX-OS 4.1
#
#          - Repackaged for NIM-08
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  JUNIPER-RADIUS-CLIENT-MIB
#
###############################################################################

case ".1.3.6.1.4.1.4874.2.2.19.3": ###  -  Notifications from JUNIPER-RADIUS-CLIENT-MIB (200902261641Z)

    log(DEBUG, "<<<<< Entering... juniper-JUNIPER-RADIUS-CLIENT-MIB.include.snmptrap.rules >>>>>")

    @Agent = "JUNIPER-RADIUS-CLIENT-MIB"
    @Class = "40200"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### juniRadiusAuthClientServerUnavailable

            ##########
            # $1 = juniRadiusAuthClientUnavailableServer 
            # $2 = juniRadiusAuthClientNextAvailableServer 
            ##########

            $juniRadiusAuthClientUnavailableServer = $1
            $juniRadiusAuthClientNextAvailableServer = $2

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-RADIUS-CLIENT-MIB-juniRadiusAuthClientServerUnavailable"

            @AlertGroup = "Authentication Client Server Status"

            @AlertKey = $juniRadiusAuthClientUnavailableServer

            @Summary = "Authentication Client Server: " + $juniRadiusAuthClientUnavailableServer + " is not available, It is replaced by " + $juniRadiusAuthClientNextAvailableServer

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($juniRadiusAuthClientUnavailableServer,$juniRadiusAuthClientNextAvailableServer)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "juniRadiusAuthClientUnavailableServer", $juniRadiusAuthClientUnavailableServer, "juniRadiusAuthClientNextAvailableServer", $juniRadiusAuthClientNextAvailableServer)

        case "2": ### juniRadiusAuthClientNoServerAvailable

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-RADIUS-CLIENT-MIB-juniRadiusAuthClientNoServerAvailable"

            @AlertGroup = "Authentication Client Server Status"

            @AlertKey = ""

            @Summary = "All the requested authentication servers are not available"

            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)

        case "3": ### juniRadiusAcctClientServerUnavailable

            ##########
            # $1 = juniRadiusAcctClientUnavailableServer 
            # $2 = juniRadiusAcctClientNextAvailableServer 
            ##########

            $juniRadiusAcctClientUnavailableServer = $1
            $juniRadiusAcctClientNextAvailableServer = $2

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-RADIUS-CLIENT-MIB-juniRadiusAcctClientServerUnavailable"

            @AlertGroup = "Accounting Client Server Status"

            @AlertKey = $juniRadiusAcctClientUnavailableServer

            @Summary = "Accounting Client Server: " + $juniRadiusAcctClientUnavailableServer + " is not available, It is replaced by " + $juniRadiusAcctClientNextAvailableServer

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($juniRadiusAcctClientUnavailableServer,$juniRadiusAcctClientNextAvailableServer)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "juniRadiusAcctClientUnavailableServer", $juniRadiusAcctClientUnavailableServer, "juniRadiusAcctClientNextAvailableServer", $juniRadiusAcctClientNextAvailableServer)

        case "4": ### juniRadiusAcctClientNoServerAvailable

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-RADIUS-CLIENT-MIB-juniRadiusAcctClientNoServerAvailable"

            @AlertGroup = "Accounting Client Server Status"

            @AlertKey = ""

            @Summary = "All the requested Accounting Client Servers not available"

            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)

        case "5": ### juniRadiusAuthClientServerAvailable

            ##########
            # $1 = juniRadiusAuthClientAvailableServer 
            ##########

            $juniRadiusAuthClientAvailableServer = $1

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-RADIUS-CLIENT-MIB-juniRadiusAuthClientServerAvailable"

            @AlertGroup = "Authentication Client Server Status"

            @AlertKey = $juniRadiusAuthClientAvailableServer

            @Summary = "Authentication Client Server: " + $juniRadiusAuthClientAvailableServer + " is available now"


            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($juniRadiusAuthClientAvailableServer)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "juniRadiusAuthClientAvailableServer", $juniRadiusAuthClientAvailableServer)

        case "6": ### juniRadiusAcctClientServerAvailable

            ##########
            # $1 = juniRadiusAcctClientAvailableServer 
            ##########

            $juniRadiusAcctClientAvailableServer = $1

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-RADIUS-CLIENT-MIB-juniRadiusAcctClientServerAvailable"

            @AlertGroup = "Accounting Client Server Status"
            @AlertKey = $juniRadiusAcctClientAvailableServer
            @Summary = "Accounting Client Server: " + $juniRadiusAcctClientAvailableServer + " is available now"

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($juniRadiusAcctClientAvailableServer)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "juniRadiusAcctClientAvailableServer", $juniRadiusAcctClientAvailableServer)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, juniper-JUNIPER-RADIUS-CLIENT-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, juniper-JUNIPER-RADIUS-CLIENT-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-RADIUS-CLIENT-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-RADIUS-CLIENT-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... juniper-JUNIPER-RADIUS-CLIENT-MIB.include.snmptrap.rules >>>>>")
