###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 8.0 - Updated release for JUNOS 9.4, JUNOSe 10.3 and BX-OS 4.1
#
#          - Repackaged for NIM-08
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  JUNIPER-SECURE-ACCESS-PORT-MIB
#
###############################################################################

case ".1.3.6.1.4.1.2636.3.40.1.2.1": ###  Notifications from JUNIPER-SECURE-ACCESS-PORT-MIB "200705151000Z" 

    log(DEBUG, "<<<<< Entering... juniper-JUNIPER-SECURE-ACCESS-PORT-MIB.include.snmptrap.rules >>>>>")

    @Agent = "JUNIPER-SECURE-ACCESS-PORT"
    @Class = "40200"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### jnxSecAccessdsRateLimitCrossed

            ##########
            # $1 = jnxSecAccessdsIfRateLimit 
            ##########

            $jnxSecAccessdsIfRateLimit = $1

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-SECURE-ACCESS-PORT-MIB-jnxSecAccessdsRateLimitCrossed"

            @AlertGroup = "DHCP Snooping"
            
            $ifIndex =  extract($OID1, "\.([0-9]+)$")
            @AlertKey = "ifEntry." + $ifIndex

            @Summary = "Number of DHCP Packets from an untrusted interface exceeds " + $jnxSecAccessdsIfRateLimit+ " for " + @AlertKey

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($jnxSecAccessdsIfRateLimit,$ifIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "jnxSecAccessdsIfRateLimit", $jnxSecAccessdsIfRateLimit, "ifIndex", $ifIndex)

        case "2": ### jnxSecAccessIfMacLimitExceeded

            ##########
            # $1 = jnxSecAccessIfMacLimit 
            # $2 = jnxSecAccessIfMacLimitExceed 
            ##########

            $jnxSecAccessIfMacLimit = $1
            $jnxSecAccessIfMacLimitExceed = lookup($2, JnxMacLimitExceededAction) + " ( " + $2 + " )"

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-SECURE-ACCESS-PORT-MIB-jnxSecAccessIfMacLimitExceeded"

            @AlertGroup = "MAC Limit"
            
            $ifIndex =  extract($OID1, "\.([0-9]+)$")
            @AlertKey = "ifEntry." + $ifIndex
            
            @Summary = "Number of MAC Addresses learnt by the interface - " + @AlertKey + " has crossed the limit " + $jnxSecAccessIfMacLimit

	    switch($2)
            {
                case "1": ### none
                    $SEV_KEY = $OS_EventId + "_none"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = @Summary + " But system will not take any action as Mac Limit is not enabled in the specified interface"

                case "2": ### drop
                    $SEV_KEY = $OS_EventId + "_drop"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = @Summary + " New MAC Address will not be learnt and traffic with new address will not be flooded." 

                case "3": ### alarm
                    $SEV_KEY = $OS_EventId + "_alarm"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = @Summary + " Notification has been generated for user awareness."

                case "4": ### shutdown
                    $SEV_KEY = $OS_EventId + "_shutdown"
                    $DEFAULT_Severity = 5
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = @Summary + " Interface has been moved to blocked state, no traffic will be allowed."

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }


            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($jnxSecAccessIfMacLimit,$jnxSecAccessIfMacLimitExceed,$ifIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "jnxSecAccessIfMacLimit", $jnxSecAccessIfMacLimit, "jnxSecAccessIfMacLimitExceed", $jnxSecAccessIfMacLimitExceed, "ifIndex", $ifIndex)

        case "3": ### jnxStormEventNotification

            ##########
            # $1 = jnxStormCtlRisingThreshold 
            ##########

            $jnxStormCtlRisingThreshold = $1

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-SECURE-ACCESS-PORT-MIB-jnxStormEventNotification"

	    $ifIndex = extract($OID1, "\.([0-9]+)\.[0-9]+$")
            $TrafficType = extract($OID1, "\.([0-9]+)$")

            $jnxStormCtlIfTrafficType = lookup($TrafficType, StormCtlIfTrafficType) + " ( " + $TrafficType + " ) "

            @AlertGroup = "Threshold"
            @AlertKey = "jnxStormCtlEntry." + $ifIndex + "." + $jnxStormCtlIfTrafficType
            @Summary = "Traffic for " + @AlertKey + " exceeds threshold " + $jnxStormCtlRisingThreshold 

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($jnxStormCtlRisingThreshold)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "jnxStormCtlRisingThreshold", $jnxStormCtlRisingThreshold)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, juniper-JUNIPER-SECURE-ACCESS-PORT-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, juniper-JUNIPER-SECURE-ACCESS-PORT-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-SECURE-ACCESS-PORT-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-SECURE-ACCESS-PORT-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... juniper-JUNIPER-SECURE-ACCESS-PORT-MIB.include.snmptrap.rules >>>>>")
