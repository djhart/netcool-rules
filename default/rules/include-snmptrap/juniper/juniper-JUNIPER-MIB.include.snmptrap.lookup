###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  JUNIPER-MIB
#
###############################################################################

table JnxOperatingState =
{
    {"1","Unknown"}, ### unknown
    {"2","Running"}, ### running - up and running, as a active primary
    {"3","Ready"}, ### ready - ready to run, not running yet
    {"4","Reset"}, ### reset - held in reset, not ready yet
    {"5","Running At Full Speed"}, ### runningAtFullSpeed - valid for fans only
    {"6","Down"}, ### down - down or off, for power supply
    {"7","Standby"} ### standby - running as a standby backup
}
default = "Unknown"

table JnxRedundancyConfig =
{
    {"1","Unknown"}, ### unknown
    {"2","Master"}, ### master - election priority set as a master
    {"3","Backup"}, ### backup - election priority set as a backup
    {"4","Disabled"}, ### disabled - election disabled
    {"5","Not Applicable"} ### notApplicable - any among the available can be master
}
default = "Unknown"

table JnxRedundancyState =
{
    {"1","Unknown"}, ### unknown
    {"2","Master"}, ### master - election priority set as a master
    {"3","Backup"}, ### backup - election priority set as a backup
    {"4","Disabled"} ### disabled - election disabled
}
default = "Unknown"

table JnxRedundancySwitchoverReason =
{
    {"1","Other"}, ### other
    {"2","Never Switched"}, ### neverSwitched - never switched
    {"3","User Switched"}, ### userSwitched - User-initiated switchover
    {"4","Auto Switched"} ### autoSwitched - automatic switchover
}
default = "Unknown"

table JnxFruType =
{
    {"1","Unknown FRU"}, ### other - unknown or others
    {"2","Clock Generator"}, ### clockGenerator - CG
    {"3","Flexible PIC Concentrator"}, ### flexiblePicConcentrator - FPC
    {"4","Switching And Forwarding Module"}, ### switchingAndForwardingModule - SFM
    {"5","Control Board"}, ### controlBoard - CBD, SCB
    {"6","Routing Engine"}, ### routingEngine - RE
    {"7","Power Entry Module"}, ### powerEntryModule - PEM
    {"8","Front Panel Module"}, ### frontPanelModule - FPM
    {"9","Switch Interface Board"}, ### switchInterfaceBoard - SIB
    {"10","Processor Mezzanine Board For SIB"}, ### processorMezzanineBoardForSIB - SPMB
    {"11","Port Interface Card"}, ### portInterfaceCard - PIC
    {"12","Craft Interface Panel"}, ### craftInterfacePanel - CIP
    {"13","Fan"}, ### fan
    {"14","Line Card Chassis"}, ### lineCardChassis - LCC
    {"15","Forwarding Engine Board"}, ### forwardingEngineBoard - FEB
    {"16","Protected System Domain"}, ### protectedSystemDomain - PSD
    {"17","Power Distribution Unit"}, ### powerDistributionUnit - PDU
    {"18","Power Supply Module"} ### powerSupplyModule - PSM
}
default = "Unknown"

table JnxFruOfflineReason =
{
    {"1","Unknown"}, ### unknown - unknown or other
    {"2","None"}, ### none - none
    {"3","Error"}, ### error - error
    {"4","No Power"}, ### noPower - no power
    {"5","Configured to Power Off"}, ### configPowerOff - configured to power off
    {"6","Configured Hold In Reset"}, ### configHoldInReset - configured to hold in reset
    {"7","Offlined by CLI Command"}, ### cliCommand - offlined by cli command
    {"8","Offlined by Button Press"}, ### buttonPress - offlined by button press
    {"9","Restart by CLI Command"}, ### cliRestart - restarted by cli command
    {"10","Overtemperature Shutdown"}, ### overtempShutdown - overtemperature shutdown
    {"11","Master Clock Down"}, ### masterClockDown - master clock down
    {"12","Single SFM Mode Change"}, ### singleSfmModeChange - single SFM mode change
    {"13","Packet Scheduling Mode Change"}, ### packetSchedulingModeChange - Packet Scheduling Mode Change
    {"14","Physical Removal"}, ### physicalRemoval - physical removal
    {"15","Restarted Due to Unresponsiveness"}, ### unresponsiveRestart - restarting unresponsive board
    {"16","SONET Clock Absent"}, ### sonetClockAbsent - sonet out clock absent
    {"17","RDD Power Off"}, ### rddPowerOff - RDD power off
    {"18","Major Errors"}, ### majorErrors - major errors
    {"19","Minor Errors"}, ### minorErrors - minor errors
    {"20","LCC Hard Restart"}, ### lccHardRestart - LCC hard restart
    {"21","LCC Version Mismatch"}, ### lccVersionMismatch - LCC version mismatch
    {"22","Power Cycle"}, ### powerCycle - power cycle
    {"23","Reconnect"}, ### reconnect - reconnect
    {"24","OverVoltage"}, ### overvoltage - overvoltage
    {"25","PFE Version Mismatch"}, ### pfeVersionMismatch - PFE version mismatch
    {"26","FEB Redundancy Cfg Changed"}, ### febRddCfgChange - FEB redundancy cfg changed
    {"27","FPC is Misconfigured"}, ### fpcMisconfig - FPC is misconfigured
    {"28","FRU did not Reconnect"}, ### fruReconnectFail - FRU did not reconnect
    {"29","FWDD Reset the Fru"}, ### fruFwddReset - FWDD reset the fru
    {"30","FEB got Switched"}, ### fruFebSwitch - FEB got switched
    {"31","FEB was Offlined"}, ### fruFebOffline - FEB was offlined
    {"32","In Service Software Upgrade Error"}, ### fruInServSoftUpgradeError - In Service Software Upgrade Error
    {"33","Chassis Power Rating Exceeded"}, ### fruChasdPowerRatingExceed - Chassis power rating exceeded
    {"34","Configured Offline"}, ### fruConfigOffline - Configured offline
    {"35","Restarting Request From a Service"}, ### fruServiceRestartRequest - restarting request froma service
    {"36","SPU Reset Request"}, ### spuResetRequest
    {"37","SPU Flowd Down"}, ### spuFlowdDown
    {"38","SPU SPI4 Down"}, ### spuSpi4Down
    {"39","SPU Watchdog Timeout"}, ### spuWatchdogTimeout
    {"40","SPU Core Dump"}, ### spuCoreDump
    {"41","FPGA SPI4 Link Down"}, ### fpgaSpi4LinkDown
    {"42","I3 SPI4 Link Down"}, ### i3Spi4LinkDown
    {"43","CPP Disconnect"}, ### cppDisconnect
    {"44","CPU Not Boot"}, ### cpuNotBoot
    {"45","SPU Kernel Core Dump Complete"}, ### spuCoreDumpComplete
    {"46","Reset on SPC SPU Failure"}, ### rstOnSpcSpuFailure
    {"47","Soft Reset on SPC SPU Failure"}, ### softRstOnSpcSpuFailure
    {"48","HW Authentication Failure"}, ### hwAuthenticationFailure
    {"49","Reconnect FPC Fail"}, ### reconnectFpcFail
    {"50","FPC App Failed"}, ### fpcAppFailed
    {"51","FPC Kernel Crash"}, ### fpcKernelCrash
    {"52","SPU Flowd Down, No Core Dump"}, ### spuFlowdDownNoCore
    {"53","SPU Flowd Crash with Incomplete Core Dump"}, ### spuFlowdCoreDumpIncomplete
    {"54","SPU Flowd Crash with Complete Core Dump"}, ### spuFlowdCoreDumpComplete
    {"55","SPU Idpd Down, No Core Dump"}, ### spuIdpdDownNoCore
    {"56","SPU Idpd Crash with Incomplete Core Dump"}, ### spuIdpdCoreDumpIncomplete
    {"57","SPU Idpd Crash with Complete Core Dump"}, ### spuIdpdCoreDumpComplete
    {"58","SPU Kernel Crash with Incomplete Core Dump"}, ### spuCoreDumpIncomplete
    {"59","SPU Idpd Down"}, ### spuIdpdDown
    {"60","PFE Resets"}, ### fruPfeReset
    {"61","FPC Not Ready to Reconnect"}, ### fruReconnectNotReady
    {"62","FE - Fabric Links Down"}, ### fruSfLinkDown
    {"63","Fabric Transitioned from Up to Down"}, ### fruFabricDown
    {"64","FRU Anti Counterfeit Retry"}, ### fruAntiCounterfeitRetry
    {"65","FRU FPC Chassis Cluster Disable"}, ### fruFPCChassisClusterDisable
    {"66","SPU FIPS Error"}, ### spuFipsError
    {"67","FRU FPC Fabric Down Offline"}, ### fruFPCFabricDownOffline
    {"68","FEB Cfg Change"}, ### febCfgChange
    {"69","Route Localization Role Change"}, ### routeLocalizationRoleChange
    {"70","FRU FPC Unsupported"}, ### fruFpcUnsupported
    {"71","PSD Version Mismatch"}, ### psdVersionMismatch
    {"72","FRU Reset Threshold Exceeded"}, ### fruResetThresholdExceeded
    {"73","PIC Bounce"}, ### picBounce
    {"74","Bad Voltage"}, ### badVoltage
    {"75","FRU FPC Reduced Fabric BW"}, ### fruFPCReducedFabricBW
    {"76","FrRU Auto Heal"}, ### fruAutoheal
    {"77","Built-in PIC Bounce"} ### builtinPicBounce
}
default = "Unknown"
