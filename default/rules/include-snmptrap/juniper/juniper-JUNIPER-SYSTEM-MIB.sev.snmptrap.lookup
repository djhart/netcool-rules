###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 9.0 - Updated release for JUNOS 10.4, JUNOSe 11.3
#
# 8.0 - Updated release for JUNOS 9.4, JUNOSe 10.3 and BX-OS 4.1
#
#          - Repackaged for NIM-08
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  JUNIPER-SYSTEM-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table juniper-JUNIPER-SYSTEM-MIB_sev =
{
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemHighMemUtil","4","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemAbatedMemUtil","1","2","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemModuleOperStatusChange_notPresent","4","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemModuleOperStatusChange_disabled","3","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemModuleOperStatusChange_hardwareError","4","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemModuleOperStatusChange_booting","2","13","1800"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemModuleOperStatusChange_initializing","2","13","1800"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemModuleOperStatusChange_online","1","2","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemModuleOperStatusChange_standby","2","13","1800"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemModuleOperStatusChange_inactive","3","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemModuleOperStatusChange_notResponding","3","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemModuleOperStatusChange_unknown","2","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemPowerStatusChange_notPresent","4","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemPowerStatusChange_inactive","3","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemPowerStatusChange_good","1","2","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemPowerStatusChange_failed","4","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemPowerStatusChange_sensorFailed","3","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemPowerStatusChange_unknown","2","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemFanStatusChange_failed","4","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemFanStatusChange_ok","1","2","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemFanStatusChange_warning","3","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemFanStatusChange_unknown","2","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemTempStatusChange_failed","4","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemTempStatusChange_tooLow","3","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemTempStatusChange_nominal","1","2","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemTempStatusChange_tooHigh","3","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemTempStatusChange_tooLowWarning","3","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemTempStatusChange_tooHighWarning","3","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemTempStatusChange_unknown","2","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemTempProtectionStatusChange_off","3","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemTempProtectionStatusChange_monitoring","2","13","1800"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemTempProtectionStatusChange_inHoldOff","2","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemTempProtectionStatusChange_activatedHoldOffExpired","3","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemTempProtectionStatusChange_activatedTempTooHigh","4","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemTempProtectionStatusChange_unknown","2","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemReloadCommand_slot","4","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemReloadCommand_system","5","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemUtilizationThreshold","4","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemIssuStateChange_idle","2","13","1800"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemIssuStateChange_initializing","2","13","1800"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemIssuStateChange_initialized","1","2","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemIssuStateChange_upgrading","2","13","1800"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemIssuStateChange_stopping","3","1","0"}, 
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemIssuStateChange_unknown","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemPortSfpTxMaxPowerThreshold_raised","3","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemPortSfpTxMaxPowerThreshold_cleared","1","2","0"},
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemPortSfpTxMinPowerThreshold_raised","3","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemPortSfpTxMinPowerThreshold_cleared","1","2","0"},
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemPortSfpRxMaxPowerThreshold_raised","3","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemPortSfpRxMaxPowerThreshold_cleared","1","2","0"},
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemPortSfpRxMinPowerThreshold_raised","3","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-SYSTEM-MIB-juniSystemPortSfpRxMinPowerThreshold_cleared","1","2","0"}
}
default = {"Unknown","Unknown","Unknown"}
