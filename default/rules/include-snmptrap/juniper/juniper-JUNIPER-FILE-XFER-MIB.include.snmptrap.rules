###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 8.0 - Updated release for JUNOS 9.4, JUNOSe 10.3 and BX-OS 4.1
#
#          - Repackaged for NIM-08
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  JUNIPER-FILE-XFER-MIB
#
###############################################################################

case ".1.3.6.1.4.1.4874.2.2.23.2": ###  - Notifications from JUNIPER-FILE-XFER-MIB (200209162144Z)

    log(DEBUG, "<<<<< Entering... juniper-JUNIPER-FILE-XFER-MIB.include.snmptrap.rules >>>>>")

    @Agent = "JUNIPER-FILE-XFER-MIB"
    @Class = "40200"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### juniFileXferTrap

            ##########
            # $1 = juniFileXferStatus 
            # $2 = juniFileXferTimeStamp 
            ##########

            $juniFileXferStatus = lookup($1, JuniFileXferStatus) + " ( " + $1 + " ) "
            $juniFileXferTimeStamp = $2

            $juniFileXferIndex = extract($OID1, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-FILE-XFER-MIB-juniFileXferTrap"

            @AlertGroup = "File Transfer Status"
            @AlertKey = "juniFileXferTableEntry." + $juniFileXferIndex

            switch($1)
            {
                case "1": ### success
                    $SEV_KEY = $OS_EventId + "_success"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    @Summary = "File Transfer Successful"

                case "2": ### inProgress
                    $SEV_KEY = $OS_EventId + "_inProgress"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800
                    @Summary = "File Transfer In Progress"

                case "3": ### remoteUnreachable
                    $SEV_KEY = $OS_EventId + "_remoteUnreachable"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Remote Unreachable for File Transfer"

                case "4": ### userAuthFailed
                    $SEV_KEY = $OS_EventId + "_userAuthFailed"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "File Transfer User Authentication Failed"

                case "5": ### fileNotFound
                    $SEV_KEY = $OS_EventId + "_fileNotFound"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "File Not Found for File Transfer"

                case "6": ### fileTooBig
                    $SEV_KEY = $OS_EventId + "_fileTooBig"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "File Too Big for File Transfer"

                case "7": ### fileIncompatible
                    $SEV_KEY = $OS_EventId + "_fileIncompatible"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "File Incompatible for File Transfer"

                case "8": ### pended
                    $SEV_KEY = $OS_EventId + "_pended"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "File Transfer Pended"

                case "9": ### copyRunningCfgFailed
                    $SEV_KEY = $OS_EventId + "_copyRunningCfgFailed"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "Copy of Running Configuration Failed"

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    @Summary = "File Transfer Status Unknown"

             }

            @Summary = @Summary + " ( " + @AlertKey + " ) "

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($juniFileXferStatus,$juniFileXferTimeStamp,$juniFileXferIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "juniFileXferStatus", $juniFileXferStatus, "juniFileXferTimeStamp", $juniFileXferTimeStamp, "juniFileXferIndex", $juniFileXferIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, juniper-JUNIPER-FILE-XFER-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, juniper-JUNIPER-FILE-XFER-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-FILE-XFER-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-FILE-XFER-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... juniper-JUNIPER-FILE-XFER-MIB.include.snmptrap.rules >>>>>")
