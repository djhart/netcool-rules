###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.1 - Update Release
#        Support new traps (1,6,8,10-12,701-714)
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  IBM-SYSTEM-TRAP-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table ibm-IBM-SYSTEM-TRAP-MIB_sev =
{
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapGeneric_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapGeneric_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapGeneric_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapGeneric_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapTemperature_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapTemperature_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapTemperature_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapTemperature_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapVoltage_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapVoltage_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapVoltage_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapVoltage_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapChassis_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapChassis_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapChassis_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapChassis_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapFan_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapFan_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapFan_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapFan_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapProcessor_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapProcessor_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapProcessor_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapProcessor_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapStorage_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapStorage_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapStorage_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapStorage_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapAsset_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapAsset_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapAsset_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapAsset_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapSMART_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapSMART_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapSMART_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapSMART_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapPOST_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapPOST_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapPOST_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapPOST_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapConfigurationChange_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapConfigurationChange_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapConfigurationChange_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapConfigurationChange_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapLANLeash_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapLANLeash_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapLANLeash_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapLANLeash_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapLeaseExpiration_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapLeaseExpiration_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapLeaseExpiration_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapLeaseExpiration_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapWarrantyExpiration_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapWarrantyExpiration_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapWarrantyExpiration_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapWarrantyExpiration_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapRedundantNIC_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapRedundantNIC_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapRedundantNIC_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapRedundantNIC_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapRedundantNICSwitchover_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapRedundantNICSwitchover_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapRedundantNICSwitchover_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapRedundantNICSwitchover_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapRedundantNICSwitchback_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapRedundantNICSwitchback_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapRedundantNICSwitchback_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapRedundantNICSwitchback_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapProcessorPF_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapProcessorPF_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapProcessorPF_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapProcessorPF_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapMemoryPF_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapMemoryPF_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapMemoryPF_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapMemoryPF_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapPFA_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapPFA_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapPFA_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapPFA_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapPowerSupply_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapPowerSupply_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapPowerSupply_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapPowerSupply_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapErrorLog_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapErrorLog_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapErrorLog_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapErrorLog_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapRemoteLogin","2","1","0"},
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapNetworkAdapterFailed_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapNetworkAdapterFailed_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapNetworkAdapterFailed_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapNetworkAdapterFailed_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapNetworkAdapterOffline_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapNetworkAdapterOffline_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapNetworkAdapterOffline_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapNetworkAdapterOffline_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapNetworkAdapterOnline","1","2","0"},
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapSPPowerSupply_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapSPPowerSupply_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapSPPowerSupply_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapSPPowerSupply_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapDASDBackplane","5","1","0"},
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapGenericFan","5","1","0"},
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapGenericVoltage_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapGenericVoltage_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapGenericVoltage_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapGenericVoltage_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapMgmtHwLogStatus","2","13","1800"},
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapxProcessor_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapxProcessor_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapxProcessor_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapxProcessor_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapxPhysicalMemory_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapxPhysicalMemory_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapxPhysicalMemory_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapxPhysicalMemory_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapxAutomaticServerRestart_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapxAutomaticServerRestart_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapxAutomaticServerRestart_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapxAutomaticServerRestart_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapMgmtHwLogEntry","2","13","1800"},
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapMgmtHwLogFull","3","1","0"},
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapMgmtHwLogClear","1","2","0"},
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapCSServiceEvent_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapCSServiceEvent_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapCSServiceEvent_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystemTrapCSServiceEvent_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDProcessorPFEvent_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDProcessorPFEvent_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDProcessorPFEvent_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDProcessorPFEvent_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDMemoryPFEvent_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDMemoryPFEvent_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDMemoryPFEvent_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDMemoryPFEvent_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDNetworkAdapterOnlineEvent","1","2","0"},
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDNetworkAdapterOfflineEvent","3","1","0"},
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDMgmtHwLogEntryEvent_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDMgmtHwLogEntryEvent_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDMgmtHwLogEntryEvent_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDMgmtHwLogEntryEvent_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDMgmtHwLogFullEvent_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDMgmtHwLogFullEvent_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDMgmtHwLogFullEvent_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDMgmtHwLogFullEvent_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDMgmtHwLogClearEvent_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDMgmtHwLogClearEvent_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDMgmtHwLogClearEvent_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDMgmtHwLogClearEvent_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDAutomaticServerRestartEvent_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDAutomaticServerRestartEvent_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDAutomaticServerRestartEvent_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDAutomaticServerRestartEvent_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDTemperatureEvent_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDTemperatureEvent_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDTemperatureEvent_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDTemperatureEvent_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDVoltageEvent_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDVoltageEvent_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDVoltageEvent_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDVoltageEvent_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDPowerSupplyEvent_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDPowerSupplyEvent_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDPowerSupplyEvent_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDPowerSupplyEvent_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDFanEvent_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDFanEvent_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDFanEvent_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDFanEvent_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDNetworkAdapterSwitchOverEvent_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDNetworkAdapterSwitchOverEvent_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDNetworkAdapterSwitchOverEvent_critical","5","1","0"}, 
    
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDNetworkAdapterSwitchOverEvent_unknown","2","1","0"},
        
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDNetworkAdapterSwitchBackEvent_normal","1","2","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDNetworkAdapterSwitchBackEvent_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDNetworkAdapterSwitchBackEvent_critical","5","1","0"}, 
    {"SNMPTRAP-ibm-IBM-SYSTEM-TRAP-MIB-ibmSystempTrapIBMSDNetworkAdapterSwitchBackEvent_unknown","2","1","0"}
        
}
default = {"Unknown","Unknown","Unknown"}

