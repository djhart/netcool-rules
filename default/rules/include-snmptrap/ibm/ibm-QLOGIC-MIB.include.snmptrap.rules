###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 2.0 - Updated Release.
# 
#         Update Class ID.
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  QLOGIC-MIB
#
###############################################################################

case ".1.3.6.1.4.1.1663.1.3": ### - Notifications from QLOGIC-MIB (200909290000Z)

    log(DEBUG, "<<<<< Entering... ibm-QLOGIC-MIB.include.snmptrap.rules >>>>>")

    @Agent = "ibm-QLOGIC-MIB"
    @Class = "87040"
    
    $OPTION_TypeFieldUsage = "3.6"
    

    switch($specific-trap)
    {
    
     
        case "10": ### qlSB2PortLinkDown
        
            ##########
            # $1 = fcQxPortPhysAdminStatus
            # $2 = fcQxPortPhysOperStatus
            ##########
            
            $fcQxPortPhysAdminStatus = lookup($1, FcQxPortPhysAdminStatus)
            $fcQxPortPhysOperStatus = lookup($2, FcQxPortPhysOperStatus)
            
            $fcQxPortPhysModule = extract($OID1,"\.([0-9]+)\.[0-9]+$")
            $fcQxPortPhysIndex = extract($OID1,"\.([0-9]+)$")
            
            $OS_EventId = "SNMPTRAP-ibm-QLOGIC-MIB-qlSB2PortLinkDown"

            @AlertGroup = "Port Link Status"
            @AlertKey = "fcQxPortPhysEntry" + "." + $fcQxPortPhysModule + "." + $fcQxPortPhysIndex
            @Summary = "Connection Port Link Status Has Changed to " + $fcQxPortPhysOperStatus + " ( " + @AlertKey + " ) "
            
            switch($2)
            {
                case "1":### online
                    $SEV_KEY = $OS_EventId + "_online"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0               
                case "2":### offline
                    $SEV_KEY = $OS_EventId + "_offline"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "3":### testing
                    $SEV_KEY = $OS_EventId + "_testing"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800               
                case "4":### linkFailure
                    $SEV_KEY = $OS_EventId + "_linkFailure"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                default: 
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = "Connectivity Port Link Status Unknown" + " ( " + @AlertKey + " ) "
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            update(@Severity)
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $fcQxPortPhysAdminStatus = $fcQxPortPhysAdminStatus + " ( " + $1 + " )"
            $fcQxPortPhysOperStatus = $fcQxPortPhysOperStatus + " ( " + $2 + " )"
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ibm, "1")) {
                details($fcQxPortPhysAdminStatus, $fcQxPortPhysOperStatus, $fcQxPortPhysModule,$fcQxPortPhysIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fcQxPortPhysAdminStatus", $fcQxPortPhysAdminStatus, "fcQxPortPhysOperStatus", $fcQxPortPhysOperStatus, "fcQxPortPhysModule", $fcQxPortPhysModule,
                 "fcQxPortPhysIndex", $fcQxPortPhysIndex)
         
        case "11": ### qlSB2PortLinkUp
        
            ##########
            # $1 = fcQxPortPhysAdminStatus
            # $2 = fcQxPortPhysOperStatus
            ##########
            
            $fcQxPortPhysAdminStatus = lookup($1, FcQxPortPhysAdminStatus)
            $fcQxPortPhysOperStatus = lookup($2, FcQxPortPhysOperStatus)
            
            $fcQxPortPhysModule = extract($OID1,"\.([0-9]+)\.[0-9]+$")
            $fcQxPortPhysIndex = extract($OID1,"\.([0-9]+)$")
            
            $OS_EventId = "SNMPTRAP-ibm-QLOGIC-MIB-qlSB2PortLinkUp"

            @AlertGroup = "Port Link Status"
            @AlertKey = "fcQxPortPhysEntry" + "." + $fcQxPortPhysModule + "." + $fcQxPortPhysIndex
            @Summary = "Connection Port Link Status Has Changed to " + $fcQxPortPhysOperStatus + " ( " + @AlertKey + " ) "
            
            switch($2)
            {
                case "1":### online
                    $SEV_KEY = $OS_EventId + "_online"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0               
                case "2":### offline
                    $SEV_KEY = $OS_EventId + "_offline"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "3":### testing
                    $SEV_KEY = $OS_EventId + "_testing"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800               
                case "4":### linkFailure
                    $SEV_KEY = $OS_EventId + "_linkFailure"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                default: 
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = "Connectivity Port Link Status Unknown" + " ( " + @AlertKey + " ) "
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            update(@Severity)
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $fcQxPortPhysAdminStatus = $fcQxPortPhysAdminStatus + " ( " + $1 + " )"
            $fcQxPortPhysOperStatus = $fcQxPortPhysOperStatus + " ( " + $2 + " )"
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ibm, "1")) {
                details($fcQxPortPhysAdminStatus, $fcQxPortPhysOperStatus, $fcQxPortPhysModule,$fcQxPortPhysIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fcQxPortPhysAdminStatus", $fcQxPortPhysAdminStatus, "fcQxPortPhysOperStatus", $fcQxPortPhysOperStatus, "fcQxPortPhysModule", $fcQxPortPhysModule,
                 "fcQxPortPhysIndex", $fcQxPortPhysIndex)
         
        case "12": ### qlconnUnitAddedTrap
        
            ##########
            # $1 = connUnitId
            ##########
            
            $connUnitId = $1
            
            $OS_EventId = "SNMPTRAP-ibm-QLOGIC-MIB-qlconnUnitAddedTrap"

            @AlertGroup = "Connectivity Unit Status"
            @AlertKey = "connUnitEntry" + "." + $connUnitId
            @Summary = "A Connectivity Unit Has Been Added To This Agent" + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800               
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ibm, "1")) {
                details($connUnitId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "connUnitId", $connUnitId)
         
        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ibm, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, ibm-QLOGIC-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, ibm-QLOGIC-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/ibm/ibm-QLOGIC-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/ibm/ibm-QLOGIC-MIB.user.include.snmptrap.rules"


##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... ibm-QLOGIC-MIB.include.snmptrap.rules >>>>>")


