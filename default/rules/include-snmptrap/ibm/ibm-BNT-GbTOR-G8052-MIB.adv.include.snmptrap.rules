###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  BNT-GbTOR-G8052-MIB
#
###############################################################################

log(DEBUG, "<<<<< Entering... ibm-BNT-GbTOR-G8052-MIB.adv.include.snmptrap.rules >>>>>")

switch($specific-trap)
{
    case "1": ### swPrimaryPowerSupplyFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swPrimaryPowerSupplyFailure"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "2": ### swDefGwUp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swDefGwUp"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ipCurCfgGwEntry." + $ipCurCfgGwIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "3": ### swDefGwDown

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swDefGwDown"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ipCurCfgGwEntry." + $ipCurCfgGwIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "4": ### swDefGwInService

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swDefGwInService"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ipCurCfgGwEntry." + $ipCurCfgGwIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "5": ### swDefGwNotInService

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swDefGwNotInService"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ipCurCfgGwEntry." + $ipCurCfgGwIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "19": ### swLoginFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swLoginFailure"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "22": ### swTempExceedThreshold

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swTempExceedThreshold"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "24": ### swFanFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swFanFailure"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "25": ### swValidLogin

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swValidLogin"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "27": ### swApplyComplete

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swApplyComplete"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "28": ### swSaveComplete

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swSaveComplete"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "29": ### swFwDownloadSucess

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swFwDownloadSucess"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "30": ### swFwDownloadFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swFwDownloadFailure"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "33": ### swFanFailureFixed

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swFanFailureFixed"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "42": ### swStgNewRoot

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swStgNewRoot"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "stgCurCfgTableEntry." + $stgCurCfgIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "43": ### swCistNewRoot

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swCistNewRoot"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "44": ### swStgTopologyChanged

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swStgTopologyChanged"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "stgCurCfgTableEntry." + $stgCurCfgIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "45": ### swCistTopologyChanged

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swCistTopologyChanged"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "46": ### swHotlinksMasterUp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swHotlinksMasterUp"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "47": ### swHotlinksMasterDn

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swHotlinksMasterDn"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "48": ### swHotlinksBackupUp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swHotlinksBackupUp"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "49": ### swHotlinksBackupDn

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swHotlinksBackupDn"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "50": ### swHotlinksNone

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swHotlinksNone"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "63": ### swValidLogout

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swValidLogout"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "65": ### swECMPGatewayUp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swECMPGatewayUp"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "66": ### swECMPGatewayDown

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swECMPGatewayDown"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "67": ### swTeamingCtrlUp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swTeamingCtrlUp"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "68": ### swTeamingCtrlDown

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swTeamingCtrlDown"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "69": ### swTeamingCtrlDownTearDownBlked

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swTeamingCtrlDownTearDownBlked"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "70": ### swTeamingCtrlError

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swTeamingCtrlError"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "31": ### swTempReturnThreshold

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swTempReturnThreshold"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "77": ### swVMGroupVMotion

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swVMGroupVMotion"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "78": ### swVMGroupVMOnline

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swVMGroupVMOnline"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "79": ### swVMGroupVMVlanChange

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swVMGroupVMVlanChange"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "97": ### swPrimaryPowerSupplyFixed

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "swPrimaryPowerSupplyFixed"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    default:
}

log(DEBUG, "<<<<< Leaving... ibm-BNT-GbTOR-G8052-MIB.adv.include.snmptrap.rules >>>>>")
