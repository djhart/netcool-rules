###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  MELLANOX-EFM-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table ibm-MELLANOX-EFM-MIB_sev =
{
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-internalBusError","4","1","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-procCrash","4","1","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-cpuUtilHigh","3","1","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-procUnexpectedExit","3","1","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-unexpectedShutdown","4","1","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-diskSpaceLow","4","1","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-systemHealthStatus","2","13","1800"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-lowPowerRecover","1","2","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-insufficientFans","4","1","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-insufficientFansRecover","1","2","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-insufficientPower","4","1","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-powerRedundancyMismatch","4","1","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-testTrap","2","1","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-asicChipDown","5","1","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-asicOverTempReset","4","1","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-asicOverTemp","4","1","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-lowPower","4","1","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-ibSMup","1","2","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-ibSMdown","4","1","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-ibSMrestart","2","1","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-mlxIBCAHealthStatusChange_unhealthy","4","1","0"}, 
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-mlxIBCAHealthStatusChange_healthy","1","2","0"}, 
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-mlxIBCAHealthStatusChange_unknown","2","1","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-mlxIBCAInsertion","1","2","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-mlxIBCARemoval","4","1","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-mlxIBSwitchInsertion","1","2","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-mlxIBSwitchRemoval","4","1","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-mlxIBRouterInsertion","1","2","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-mlxIBRouterRemoval","4","1","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-mlxIBPortStateChange_down","4","1","0"}, 
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-mlxIBPortStateChange_init","2","1","0"}, 
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-mlxIBPortStateChange_armed","2","13","1800"}, 
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-mlxIBPortStateChange_active","1","2","0"}, 
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-mlxIBPortStateChange_other","2","1","0"}, 
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-mlxIBPortStateChange_unknown","2","1","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-mlxIBPortPhysicalStateChange_sleep","2","1","0"}, 
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-mlxIBPortPhysicalStateChange_polling","2","13","1800"}, 
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-mlxIBPortPhysicalStateChange_disabled","3","1","0"}, 
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-mlxIBPortPhysicalStateChange_portConfigTraining","2","13","1800"}, 
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-mlxIBPortPhysicalStateChange_linkUp","1","2","0"}, 
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-mlxIBPortPhysicalStateChange_linkErrorRecovery","3","1","0"}, 
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-mlxIBPortPhysicalStateChange_phyTest","2","13","1800"}, 
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-mlxIBPortPhysicalStateChange_other","2","1","0"}, 
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-mlxIBPortPhysicalStateChange_unknown","2","1","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-mlxIBPortInsertion","1","2","0"},
    {"SNMPTRAP-ibm-MELLANOX-EFM-MIB-mlxIBPortRemoval","4","1","0"}
}
default = {"Unknown","Unknown","Unknown"}

