###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# QRadar SIEM's SNMP trap can be customized via a configuration file
# in QRadar's system.
# For any customization on top of the default configuration, this rules
# file needs to be customized as well in order to achieve correct object mapping
# and trap parsing.
# 
# QRADAR has more severity levels compared to OMNIbus, therefore,
#  the following severity mapping is applied.
# 
# Qradar Severity : OMNIbus Severity
# ----------------------------------
# 0               : Informational
# 1, 2, 3, 4      : Warning
# 5, 6            : Minor
# 7, 8            : Major
# 9, 10           : Critical
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  Q1LABS-MIB
#
###############################################################################

case ".1.3.6.1.4.1.20212.1": ### - Notifications from Q1LABS-MIB (200804110000Z)

    log(DEBUG, "<<<<< Entering... ibm-Q1LABS-MIB.include.snmptrap.rules >>>>>")

    @Agent = "ibm-Q1LABS-MIB"
    @Class = "87740"
    
    $OPTION_TypeFieldUsage = "3.6"
    

    switch($specific-trap)
    {
    
     
        case "1": ### eventCRENotification
        
            ##########
            # $1 = localHostAddress
            # $2 = timeString
            # $3 = ruleName
            # $4 = ruleDescription
            # $5 = attackerIP
            # $6 = attackerPort
            # $7 = attackersUserName
            # $8 = attackerNetworks
            # $9 = targetIP
            # $10 = targetPort
            # $11 = targetsUserName
            # $12 = targetNetworks
            # $13 = protocol
            # $14 = qid
            # $15 = eventName
            # $16 = eventDescription
            # $17 = category
            # $18 = dataSourceId
            # $19 = dataSourceName
            ##########
            
            $localHostAddress = $1
            $timeString = $2
            $ruleName = $3
            $ruleDescription = $4
            $attackerIP = $5
            $attackerPort = $6
            $attackersUserName = $7
            $attackerNetworks = $8
            $targetIP = $9
            $targetPort = $10
            $targetsUserName = $11
            $targetNetworks = $12
            $protocol = $13
            $qid = $14
            $eventName = $15
            $eventDescription = $16
            $category = $17
            $dataSourceId = $18
            $dataSourceName = $19
            
            $OS_EventId = "SNMPTRAP-ibm-Q1LABS-MIB-eventCRENotification"

            @AlertGroup = "Event CRE Notification"
            @AlertKey = "QID: " + $qid + ", Rule Name:" + $ruleName + ", Data Source ID:" + $dataSourceId
            @Summary = $ruleDescription + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800               
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            @ExtendedAttr = nvp_add(@ExtendedAttr, "localHostAddress", $localHostAddress, "timeString", $timeString, 
                "ruleName", $ruleName, "ruleDescription", $ruleDescription, "attackerIP", $attackerIP,
                "attackerPort", $attackerPort, "attackersUserName", $attackersUserName, "attackerNetworks", $attackerNetworks,
                "targetIP", $targetIP, "targetPort", $targetPort, "targetsUserName", $targetsUserName,
                "targetNetworks", $targetNetworks, "protocol", $protocol, "qid", $qid,
                "eventName", $eventName, "eventDescription", $eventDescription, "category", $category,
                "dataSourceId", $dataSourceId, "dataSourceName", $dataSourceName)
         
        case "2": ### offenseCRENotification
        
            ##########
            # $1 = localHostAddress
            # $2 = timeString
            # $3 = ruleName
            # $4 = ruleDescription
            # $5 = offenseID
            # $6 = offenseDescription
            # $7 = offenseLink
            # $8 = magnitude
            # $9 = severity
            # $10 = creditibility
            # $11 = relevance
            # $12 = eventCount
            # $13 = categoryCount
            # $14 = top5Categories
            # $15 = attackerIP
            # $16 = attackersUserName
            # $17 = attackerNetworks
            # $18 = attackerCount
            # $19 = top5AttackerIPs
            # $20 = targetIP
            # $21 = targetsUserName
            # $22 = targetNetworks
            # $23 = targetCount
            # $24 = top5TargetIPs
            # $25 = ruleCount
            # $26 = ruleNames
            # $27 = annotationCount
            # $28 = topAnnotation1
            # $29 = topAnnotation2
            # $30 = topAnnotation3
            # $31 = topAnnotation4
            # $32 = topAnnotation5
            # $33 = dataSourceId
            # $34 = dataSourceName
            ##########
            
            $localHostAddress = $1
            $timeString = $2
            $ruleName = $3
            $ruleDescription = $4
            $offenseID = $5
            $offenseDescription = $6
            $offenseLink = $7
            $magnitude = $8
            $severity = lookup($9, OffenseSeverity)
            $creditibility = $10
            $relevance = $11
            $eventCount = $12
            $categoryCount = $13
            $top5Categories = $14
            $attackerIP = $15
            $attackersUserName = $16
            $attackerNetworks = $17
            $attackerCount = $18
            $top5AttackerIPs = $19
            $targetIP = $20
            $targetsUserName = $21
            $targetNetworks = $22
            $targetCount = $23
            $top5TargetIPs = $24
            $ruleCount = $25
            $ruleNames = $26
            $annotationCount = $27
            $topAnnotation1 = $28
            $topAnnotation2 = $29
            $topAnnotation3 = $30
            $topAnnotation4 = $31
            $topAnnotation5 = $32
            $dataSourceId = $33
            $dataSourceName = $34
            
            $OS_EventId = "SNMPTRAP-ibm-Q1LABS-MIB-offenseCRENotification"

            @AlertGroup = "Offence CRE Notification"
            @AlertKey = "Offense ID:" + $offenseID + ", Rule Name:" + $ruleName + ", Data Source ID:" + $dataSourceId
            @Summary = $ruleDescription + " ( " + @AlertKey + " ) "
            
            switch($9)
            {
                case "0":### zero
                    $SEV_KEY = $OS_EventId + "_zero"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800               
                case "1":### one
                    $SEV_KEY = $OS_EventId + "_one"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "2":### two
                    $SEV_KEY = $OS_EventId + "_two"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "3":### three
                    $SEV_KEY = $OS_EventId + "_three"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "4":### four
                    $SEV_KEY = $OS_EventId + "_four"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "5":### five
                    $SEV_KEY = $OS_EventId + "_five"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "6":### six
                    $SEV_KEY = $OS_EventId + "_six"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "7":### seven
                    $SEV_KEY = $OS_EventId + "_seven"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "8":### eight
                    $SEV_KEY = $OS_EventId + "_eight"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "9":### nine
                    $SEV_KEY = $OS_EventId + "_nine"
                    $DEFAULT_Severity = 5
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "10":### ten
                    $SEV_KEY = $OS_EventId + "_ten"
                    $DEFAULT_Severity = 5
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                default: 
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }
            
            update(@Severity)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $severity = $severity + " ( " + $9 + " )"
            @ExtendedAttr = nvp_add(@ExtendedAttr, "localHostAddress", $localHostAddress, "timeString", $timeString,
                "ruleName", $ruleName, "ruleDescription", $ruleDescription, "offenseID", $offenseID,
                "offenseDescription", $offenseDescription, "offenseLink", $offenseLink, "magnitude", $magnitude,
                "severity", $severity, "creditibility", $creditibility, "relevance", $relevance,
                "eventCount", $eventCount, "categoryCount", $categoryCount, "top5Categories", $top5Categories,
                "attackerIP", $attackerIP, "attackersUserName", $attackersUserName, "attackerNetworks", $attackerNetworks,
                "attackerCount", $attackerCount, "top5AttackerIPs", $top5AttackerIPs, "targetIP", $targetIP,
                "targetsUserName", $targetsUserName, "targetNetworks", $targetNetworks, "targetCount", $targetCount,
                "top5TargetIPs", $top5TargetIPs, "ruleCount", $ruleCount, "ruleNames", $ruleNames,
                "annotationCount", $annotationCount, "topAnnotation1", $topAnnotation1, "topAnnotation2", $topAnnotation2,
                "topAnnotation3", $topAnnotation3, "topAnnotation4", $topAnnotation4, "topAnnotation5", $topAnnotation5,
                "dataSourceId", $dataSourceId, "dataSourceName", $dataSourceName)
         
        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, ibm-Q1LABS-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, ibm-Q1LABS-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########


include "$NC_RULES_HOME/include-snmptrap/ibm/ibm-Q1LABS-MIB-notifications.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/ibm/ibm-Q1LABS-MIB-notifications.user.include.snmptrap.rules"


##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... ibm-Q1LABS-MIB.include.snmptrap.rules >>>>>")


case ".1.3.6.1.4.1.20212.200": ### - Notifications from Q1LABS-MIB (200804110000Z)

    log(DEBUG, "<<<<< Entering... ibm-Q1LABS-MIB.include.snmptrap.rules >>>>>")

    @Agent = "ibm-Q1LABS-MIB"
    @Class = "87740"
    
    $OPTION_TypeFieldUsage = "3.6"
    

    switch($specific-trap)
    {
    
     
        case "1": ### q1CRENotification
        
            discard
         
        case "2": ### q1EventRuleNotification
        
            discard
         
        case "3": ### q1OffenseRuleNotification
        
            discard
         
        case "4": ### q1SentryNotification
        
            discard
         
        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, ibm-Q1LABS-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, ibm-Q1LABS-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########


include "$NC_RULES_HOME/include-snmptrap/ibm/ibm-Q1LABS-MIB-q1Notifications.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/ibm/ibm-Q1LABS-MIB-q1Notifications.user.include.snmptrap.rules"


##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... ibm-Q1LABS-MIB.include.snmptrap.rules >>>>>")


