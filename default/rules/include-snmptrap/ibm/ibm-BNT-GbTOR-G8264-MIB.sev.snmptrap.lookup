###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  BNT-GbTOR-G8264-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table ibm-BNT-GbTOR-G8264-MIB_sev =
{
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swDefGwUp","1","2","0"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swDefGwDown","4","1","0"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swDefGwInService","1","2","0"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swDefGwNotInService","3","1","0"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swVrrpNewMaster","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swVrrpNewBackup","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swVrrpAuthFailure","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swLoginFailure","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swTempExceedThreshold","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swApplyComplete","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swSaveComplete","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swFwDownloadSucess","1","2","0"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swFwDownloadFailure","3","1","0"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swStgNewRoot","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swCistNewRoot","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swStgTopologyChanged","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swCistTopologyChanged","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swHotlinksMasterUp","1","2","0"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swHotlinksMasterDn","4","1","0"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swHotlinksBackupUp","1","2","0"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swHotlinksBackupDn","4","1","0"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swHotlinksNone","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swECMPGatewayUp","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swECMPGatewayDown","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swTeamingCtrlUp","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swTeamingCtrlDown","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swTeamingCtrlDownTearDownBlked","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swTeamingCtrlError","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swTempReturnThreshold","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swVMGroupVMotion","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swVMGroupVMOnline","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-G8264-MIB-swVMGroupVMVlanChange","2","13","1800"}
}
default = {"Unknown","Unknown","Unknown"}
