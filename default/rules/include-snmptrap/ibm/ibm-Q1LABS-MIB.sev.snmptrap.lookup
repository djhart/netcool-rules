###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  Q1LABS-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table ibm-Q1LABS-MIB_sev =
{
    {"SNMPTRAP-ibm-Q1LABS-MIB-eventCRENotification","2","13","1800"},
    {"SNMPTRAP-ibm-Q1LABS-MIB-offenseCRENotification_zero","2","13","1800"}, 
    {"SNMPTRAP-ibm-Q1LABS-MIB-offenseCRENotification_one","2","1","0"}, 
    {"SNMPTRAP-ibm-Q1LABS-MIB-offenseCRENotification_two","2","1","0"}, 
    {"SNMPTRAP-ibm-Q1LABS-MIB-offenseCRENotification_three","2","1","0"}, 
    {"SNMPTRAP-ibm-Q1LABS-MIB-offenseCRENotification_four","2","1","0"}, 
    {"SNMPTRAP-ibm-Q1LABS-MIB-offenseCRENotification_five","3","1","0"}, 
    {"SNMPTRAP-ibm-Q1LABS-MIB-offenseCRENotification_six","3","1","0"}, 
    {"SNMPTRAP-ibm-Q1LABS-MIB-offenseCRENotification_seven","4","1","0"}, 
    {"SNMPTRAP-ibm-Q1LABS-MIB-offenseCRENotification_eight","4","1","0"}, 
    {"SNMPTRAP-ibm-Q1LABS-MIB-offenseCRENotification_nine","5","1","0"}, 
    {"SNMPTRAP-ibm-Q1LABS-MIB-offenseCRENotification_ten","5","1","0"}, 
    {"SNMPTRAP-ibm-Q1LABS-MIB-offenseCRENotification_unknown","2","1","0"},
    {"SNMPTRAP-ibm-Q1LABS-MIB-q1CRENotification","1","0","0"},
    {"SNMPTRAP-ibm-Q1LABS-MIB-q1EventRuleNotification","1","0","0"},
    {"SNMPTRAP-ibm-Q1LABS-MIB-q1OffenseRuleNotification","1","0","0"},
    {"SNMPTRAP-ibm-Q1LABS-MIB-q1SentryNotification","1","0","0"}
}
default = {"Unknown","Unknown","Unknown"}

