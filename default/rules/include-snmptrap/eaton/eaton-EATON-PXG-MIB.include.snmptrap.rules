###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  EATON-PXG-MIB
#
###############################################################################

case ".1.3.6.1.4.1.534.8.1": ###  Eaton's Toolkit-enabled Power Xpert Gateways - Notifications from EATON-PXG-MIB (200801300000Z)

    log(DEBUG, "<<<<< Entering... eaton-EATON-PXG-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Eaton-EATON-PXG-MIB"
    @Class = "2460"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### powerChainCriticalAlarmEvent

            ##########
            # $1 = eventID 
            # $2 = eventSequenceIndex 
            # $3 = eventDescription 
            # $4 = eventValue 
            # $5 = entPhysicalName 
            ##########

            $eventID = $1
            $eventSequenceIndex = $2
            $eventDescription = $3
            $eventValue = $4
            $entPhysicalName = $5

            $OS_EventId = "SNMPTRAP-eaton-EATON-PXG-MIB-powerChainCriticalAlarmEvent"

            @AlertGroup = "Power Chain Device Event Status"      

            $entPhysicalIndex = extract($OID5, "\.([0-9]+)$")

            @AlertKey = "entPhysicalEntry." + $entPhysicalIndex
            @Summary = $eventValue + " has caused: " + $eventDescription

            if(!match($5, ""))
            {
                @Summary = @Summary + " ( " + $5 + " ) "
            }
            else
            {
                @Summary = @Summary + " ( " + @AlertKey + " ) "
            }
                        
            $DEFAULT_Severity = 5
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_eaton, "1")) {
                details($eventID,$eventSequenceIndex,$eventDescription,$eventValue,$entPhysicalName,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "eventID", $eventID, "eventSequenceIndex", $eventSequenceIndex, "eventDescription", $eventDescription,
                 "eventValue", $eventValue, "entPhysicalName", $entPhysicalName, "entPhysicalIndex", $entPhysicalIndex)

        case "2": ### powerChainCautionaryAlarmEvent

            ##########
            # $1 = eventID 
            # $2 = eventSequenceIndex 
            # $3 = eventDescription 
            # $4 = eventValue 
            # $5 = entPhysicalName 
            ##########

            $eventID = $1
            $eventSequenceIndex = $2
            $eventDescription = $3
            $eventValue = $4
            $entPhysicalName = $5

            $OS_EventId = "SNMPTRAP-eaton-EATON-PXG-MIB-powerChainCautionaryAlarmEvent"

            @AlertGroup = "Power Chain Device Event Status"

            $entPhysicalIndex = extract($OID5, "\.([0-9]+)$")

            @AlertKey = "entPhysicalEntry." + $entPhysicalIndex
            @Summary = $eventValue + " has caused: " + $eventDescription

            if(!match($5, ""))
            {
                @Summary = @Summary + " ( " + $5 + " ) "
            }
            else
            {
                @Summary = @Summary + " ( " + @AlertKey + " ) "
            }

            $DEFAULT_Severity = 2 
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_eaton, "1")) {
                details($eventID,$eventSequenceIndex,$eventDescription,$eventValue,$entPhysicalName,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "eventID", $eventID, "eventSequenceIndex", $eventSequenceIndex, "eventDescription", $eventDescription,
                 "eventValue", $eventValue, "entPhysicalName", $entPhysicalName, "entPhysicalIndex", $entPhysicalIndex)


        case "3": ### powerChainAlarmEventAcknowledged

            ##########
            # $1 = eventID 
            # $2 = eventSequenceIndex 
            # $3 = eventDescription 
            # $4 = eventValue 
            # $5 = entPhysicalName 
            ##########

            $eventID = $1
            $eventSequenceIndex = $2
            $eventDescription = $3
            $eventValue = $4
            $entPhysicalName = $5

            $OS_EventId = "SNMPTRAP-eaton-EATON-PXG-MIB-powerChainAlarmEventAcknowledged"

            @AlertGroup = "Power Chain Device Event Status"

            $entPhysicalIndex = extract($OID5, "\.([0-9]+)$")

            @AlertKey = "entPhysicalEntry." + $entPhysicalIndex
            @Summary = $eventValue + " has caused: " + $eventDescription

            if(!match($5, ""))
            {
                @Summary = @Summary + " ( " + $5 + " ) "
            }
            else
            {
                @Summary = @Summary + " ( " + @AlertKey + " ) "
            }

            $DEFAULT_Severity = 2 
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_eaton, "1")) {
                details($eventID,$eventSequenceIndex,$eventDescription,$eventValue,$entPhysicalName,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "eventID", $eventID, "eventSequenceIndex", $eventSequenceIndex, "eventDescription", $eventDescription,
                 "eventValue", $eventValue, "entPhysicalName", $entPhysicalName, "entPhysicalIndex", $entPhysicalIndex)


        case "4": ### powerChainEventCleared

            ##########
            # $1 = eventID 
            # $2 = eventSequenceIndex 
            # $3 = eventDescription 
            # $4 = eventValue 
            # $5 = entPhysicalName 
            ##########

            $eventID = $1
            $eventSequenceIndex = $2
            $eventDescription = $3
            $eventValue = $4
            $entPhysicalName = $5

            $OS_EventId = "SNMPTRAP-eaton-EATON-PXG-MIB-powerChainEventCleared"

            @AlertGroup = "Power Chain Device Event Status"

            $entPhysicalIndex = extract($OID5, "\.([0-9]+)$")

            @AlertKey = "entPhysicalEntry." + $entPhysicalIndex
            @Summary = $eventValue + " has caused: " + $eventDescription

            if(!match($5, ""))
            {
                @Summary = @Summary + " ( " + $5 + " ) "
            }
            else
            {
                @Summary = @Summary + " ( " + @AlertKey + " ) "
            }

            $DEFAULT_Severity = 1 
            $DEFAULT_Type = 2 
            $DEFAULT_ExpireTime = 0

            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_eaton, "1")) {
                details($eventID,$eventSequenceIndex,$eventDescription,$eventValue,$entPhysicalName,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "eventID", $eventID, "eventSequenceIndex", $eventSequenceIndex, "eventDescription", $eventDescription,
                 "eventValue", $eventValue, "entPhysicalName", $entPhysicalName, "entPhysicalIndex", $entPhysicalIndex)

        case "5": ### powerChainEvent

            ##########
            # $1 = eventID 
            # $2 = eventSequenceIndex 
            # $3 = eventDescription 
            # $4 = eventValue 
            # $5 = entPhysicalName 
            ##########

            $eventID = $1
            $eventSequenceIndex = $2
            $eventDescription = $3
            $eventValue = $4
            $entPhysicalName = $5

            $OS_EventId = "SNMPTRAP-eaton-EATON-PXG-MIB-powerChainEvent"

            @AlertGroup = "Power Chain Device Event Status"

            $entPhysicalIndex = extract($OID5, "\.([0-9]+)$")

            @AlertKey = "entPhysicalEntry." + $entPhysicalIndex
            @Summary = $eventValue + " has caused: " + $eventDescription

            if(!match($5, ""))
            {
                @Summary = @Summary + " ( " + $5 + " ) "
            }
            else
            {
                @Summary = @Summary + " ( " + @AlertKey + " ) "
            }

            $DEFAULT_Severity = 2 
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_eaton, "1")) {
                details($eventID,$eventSequenceIndex,$eventDescription,$eventValue,$entPhysicalName,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "eventID", $eventID, "eventSequenceIndex", $eventSequenceIndex, "eventDescription", $eventDescription,
                 "eventValue", $eventValue, "entPhysicalName", $entPhysicalName, "entPhysicalIndex", $entPhysicalIndex)

        case "6": ### powerChainAlarmEventClosed

            ##########
            # $1 = eventID 
            # $2 = eventSequenceIndex 
            # $3 = eventDescription 
            # $4 = eventValue 
            # $5 = entPhysicalName 
            ##########

            $eventID = $1
            $eventSequenceIndex = $2
            $eventDescription = $3
            $eventValue = $4
            $entPhysicalName = $5

            $OS_EventId = "SNMPTRAP-eaton-EATON-PXG-MIB-powerChainAlarmEventClosed"

            @AlertGroup = "Power Chain Device Event Status"

            $entPhysicalIndex = extract($OID5, "\.([0-9]+)$")

            @AlertKey = "entPhysicalEntry." + $entPhysicalIndex
            @Summary = $eventValue + " has caused: " + $eventDescription

            if(!match($5, ""))
            {
                @Summary = @Summary + " ( " + $5 + " ) "
            }
            else
            {
                @Summary = @Summary + " ( " + @AlertKey + " ) "
            }

            $DEFAULT_Severity = 1 
            $DEFAULT_Type = 2 
            $DEFAULT_ExpireTime = 0

            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_eaton, "1")) {
                details($eventID,$eventSequenceIndex,$eventDescription,$eventValue,$entPhysicalName,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "eventID", $eventID, "eventSequenceIndex", $eventSequenceIndex, "eventDescription", $eventDescription,
                 "eventValue", $eventValue, "entPhysicalName", $entPhysicalName, "entPhysicalIndex", $entPhysicalIndex)


        case "7": ### powerChainCriticalAlarm

            ##########
            # $1 = alarmID 
            # $2 = alarmSequenceIndex 
            # $3 = alarmDescription 
            # $4 = alarmValue 
            # $5 = entPhysicalName 
            ##########

            $alarmID = $1
            $alarmSequenceIndex = $2
            $alarmDescription = $3
            $alarmValue = $4
            $entPhysicalName = $5

            $OS_EventId = "SNMPTRAP-eaton-EATON-PXG-MIB-powerChainCriticalAlarm"

            @AlertGroup = "Power Chain Device Alarm Status"

            $entPhysicalIndex = extract($OID5, "\.([0-9]+)$")

            @AlertKey = "entPhysicalEntry." + $entPhysicalIndex
            @Summary = $alarmValue + " has caused: " + $alarmDescription

            if(!match($5, ""))
            {
                @Summary = @Summary + " ( " + $5 + " ) "
            }
            else
            {
                @Summary = @Summary + " ( " + @AlertKey + " ) "
            }

            $DEFAULT_Severity = 5
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_eaton, "1")) {
                details($alarmID,$alarmSequenceIndex,$alarmDescription,$alarmValue,$entPhysicalName,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "alarmID", $alarmID, "alarmSequenceIndex", $alarmSequenceIndex, "alarmDescription", $alarmDescription,
                 "alarmValue", $alarmValue, "entPhysicalName", $entPhysicalName, "entPhysicalIndex", $entPhysicalIndex)

        case "8": ### powerChainCautionaryAlarm

            ##########
            # $1 = alarmID 
            # $2 = alarmSequenceIndex 
            # $3 = alarmDescription 
            # $4 = alarmValue 
            # $5 = entPhysicalName 
            ##########

            $alarmID = $1
            $alarmSequenceIndex = $2
            $alarmDescription = $3
            $alarmValue = $4
            $entPhysicalName = $5

            $OS_EventId = "SNMPTRAP-eaton-EATON-PXG-MIB-powerChainCautionaryAlarm"
            @AlertGroup = "Power Chain Device Alarm Status"

            $entPhysicalIndex = extract($OID5, "\.([0-9]+)$")

            @AlertKey = "entPhysicalEntry." + $entPhysicalIndex
            @Summary = $alarmValue + " has caused: " + $alarmDescription

            if(!match($5, ""))
            {
                @Summary = @Summary + " ( " + $5 + " ) "
            }
            else
            {
                @Summary = @Summary + " ( " + @AlertKey + " ) "
            }

            $DEFAULT_Severity = 2 
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_eaton, "1")) {
                details($alarmID,$alarmSequenceIndex,$alarmDescription,$alarmValue,$entPhysicalName,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "alarmID", $alarmID, "alarmSequenceIndex", $alarmSequenceIndex, "alarmDescription", $alarmDescription,
                 "alarmValue", $alarmValue, "entPhysicalName", $entPhysicalName, "entPhysicalIndex", $entPhysicalIndex)

        case "9": ### powerChainAlarmAcknowledged

            ##########
            # $1 = alarmID 
            # $2 = alarmSequenceIndex 
            # $3 = alarmDescription 
            # $4 = alarmValue 
            # $5 = entPhysicalName 
            ##########

            $alarmID = $1
            $alarmSequenceIndex = $2
            $alarmDescription = $3
            $alarmValue = $4
            $entPhysicalName = $5

            $OS_EventId = "SNMPTRAP-eaton-EATON-PXG-MIB-powerChainAlarmAcknowledged"

            @AlertGroup = "Power Chain Device Alarm Status"

            $entPhysicalIndex = extract($OID5, "\.([0-9]+)$")

            @AlertKey = "entPhysicalEntry." + $entPhysicalIndex
            @Summary = $alarmValue + " has caused: " + $alarmDescription

            if(!match($5, ""))
            {
                @Summary = @Summary + " ( " + $5 + " ) "
            }
            else
            {
                @Summary = @Summary + " ( " + @AlertKey + " ) "
            }

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_eaton, "1")) {
                details($alarmID,$alarmSequenceIndex,$alarmDescription,$alarmValue,$entPhysicalName,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "alarmID", $alarmID, "alarmSequenceIndex", $alarmSequenceIndex, "alarmDescription", $alarmDescription,
                 "alarmValue", $alarmValue, "entPhysicalName", $entPhysicalName, "entPhysicalIndex", $entPhysicalIndex)

        case "10": ### powerChainAlarmCleared

            ##########
            # $1 = alarmID 
            # $2 = alarmSequenceIndex 
            # $3 = alarmDescription 
            # $4 = alarmValue 
            # $5 = entPhysicalName 
            ##########

            $alarmID = $1
            $alarmSequenceIndex = $2
            $alarmDescription = $3
            $alarmValue = $4
            $entPhysicalName = $5

            $OS_EventId = "SNMPTRAP-eaton-EATON-PXG-MIB-powerChainAlarmCleared"

            @AlertGroup = "Power Chain Device Alarm Status"

            $entPhysicalIndex = extract($OID5, "\.([0-9]+)$")

            @AlertKey = "entPhysicalEntry." + $entPhysicalIndex
            @Summary = $alarmValue + " has caused: " + $alarmDescription

            if(!match($5, ""))
            {
                @Summary = @Summary + " ( " + $5 + " ) "
            }
            else
            {
                @Summary = @Summary + " ( " + @AlertKey + " ) "
            }

            $DEFAULT_Severity = 1 
            $DEFAULT_Type = 2 
            $DEFAULT_ExpireTime = 0

            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_eaton, "1")) {
                details($alarmID,$alarmSequenceIndex,$alarmDescription,$alarmValue,$entPhysicalName,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "alarmID", $alarmID, "alarmSequenceIndex", $alarmSequenceIndex, "alarmDescription", $alarmDescription,
                 "alarmValue", $alarmValue, "entPhysicalName", $entPhysicalName, "entPhysicalIndex", $entPhysicalIndex)

        case "11": ### powerChainAlarmClosed

            ##########
            # $1 = alarmID 
            # $2 = alarmSequenceIndex 
            # $3 = alarmDescription 
            # $4 = alarmValue 
            # $5 = entPhysicalName 
            ##########

            $alarmID = $1
            $alarmSequenceIndex = $2
            $alarmDescription = $3
            $alarmValue = $4
            $entPhysicalName = $5

            $OS_EventId = "SNMPTRAP-eaton-EATON-PXG-MIB-powerChainAlarmClosed"

            @AlertGroup = "Power Chain Device Alarm Status"

            $entPhysicalIndex = extract($OID5, "\.([0-9]+)$")

            @AlertKey = "entPhysicalEntry." + $entPhysicalIndex
            @Summary = $alarmValue + " has caused: " + $alarmDescription

            if(!match($5, ""))
            {
                @Summary = @Summary + " ( " + $5 + " ) "
            }
            else
            {
                @Summary = @Summary + " ( " + @AlertKey + " ) "
            }

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2 
            $DEFAULT_ExpireTime = 0

            update(@Summary) 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_eaton, "1")) {
                details($alarmID,$alarmSequenceIndex,$alarmDescription,$alarmValue,$entPhysicalName,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "alarmID", $alarmID, "alarmSequenceIndex", $alarmSequenceIndex, "alarmDescription", $alarmDescription,
                 "alarmValue", $alarmValue, "entPhysicalName", $entPhysicalName, "entPhysicalIndex", $entPhysicalIndex)

        case "12": ### powerChainAlarmUpdated

            ##########
            # $1 = alarmID 
            # $2 = alarmSequenceIndex 
            # $3 = alarmDescription 
            # $4 = alarmValue 
            # $5 = alarmLevel 
            # $6 = entPhysicalName 
            ##########

            $alarmID = $1
            $alarmSequenceIndex = $2
            $alarmDescription = $3
            $alarmValue = $4
            $alarmLevel = lookup($5, AlarmLevel)
            $entPhysicalName = $6

            $OS_EventId = "SNMPTRAP-eaton-EATON-PXG-MIB-powerChainAlarmUpdated"

            @AlertGroup = "Power Chain Device Alarm Status"

            $entPhysicalIndex = extract($OID6, "\.([0-9]+)$")

            @AlertKey = "entPhysicalEntry." + $entPhysicalIndex
            @Summary = $alarmValue + " has caused: " + $alarmDescription + ", Level: " + $alarmLevel

            switch($5)
            {
                case "1":### critical
                    $SEV_KEY = $OS_EventId + "_critical"

                    $DEFAULT_Severity = 5
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "2":### cautionary
                    $SEV_KEY = $OS_EventId + "_cautionary"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "3":### acknowledged
                    $SEV_KEY = $OS_EventId + "_acknowledged"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800
                    
                case "4":### active
                    $SEV_KEY = $OS_EventId + "_active"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800
                    
                case "5":### cleared
                    $SEV_KEY = $OS_EventId + "_cleared"

                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    
                case "6":### closed
                    $SEV_KEY = $OS_EventId + "_closed"

                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    
                default:
                    $SEV_KEY = $OS_EventId + "_unknown"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            if(!match($6, ""))
            {
                @Summary = @Summary + " ( " + $6 + " ) "
            }
            else
            {
                @Summary = @Summary + " ( " + @AlertKey + " ) "
            }

            update(@Summary)

            update(@Severity)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            $alarmLevel = $alarmLevel + " ( " + $5 + " ) "

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_eaton, "1")) {
                details($alarmID,$alarmSequenceIndex,$alarmDescription,$alarmValue,$alarmLevel,$entPhysicalName,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "alarmID", $alarmID, "alarmSequenceIndex", $alarmSequenceIndex, "alarmDescription", $alarmDescription,
                 "alarmValue", $alarmValue, "alarmLevel", $alarmLevel, "entPhysicalName", $entPhysicalName,
                 "entPhysicalIndex", $entPhysicalIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_eaton, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, eaton-EATON-PXG-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, eaton-EATON-PXG-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/eaton/eaton-EATON-PXG-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/eaton/eaton-EATON-PXG-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... eaton-EATON-PXG-MIB.include.snmptrap.rules >>>>>")
