###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 5.0 - Updated Release for HN400/HN4000 7.2.2.
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  HN-CFM-MIB
#
###############################################################################

case ".1.3.6.1.4.1.8550.2.6.4.3": ### Hatteras Networks - Notifications from HN-CFM-MIB (200701110000Z)

    log(DEBUG, "<<<<< Entering... hatteras-HN-CFM-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Hatteras-HN-CFM-MIB"
    @Class = "40535"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### hnCfmCcRdiNotification

            ##########
            # $1 = hnEventHistoryId 
            # $2 = hnEventHistoryType 
            # $3 = hnEventHistoryDescr 
            # $4 = hnEventHistorySeverity 
            # $5 = hnEventHistoryServiceAffected 
            # $6 = hnEventHistoryTimeValue 
            # $7 = hnDeviceDescription 
            ##########

            $hnEventHistoryId = $1
            $hnEventHistoryType = lookup($2,HnAlarmConditionType) + " ( " + $2 + " )"
            $hnEventHistoryDescr = $3
            $hnEventHistorySeverity = lookup($4,HnAlarmSeverity) + " ( " + $4 + " )"
            $hnEventHistoryServiceAffected = lookup($5,HnAlarmServiceAffected) + " ( " + $5 + " )"
            $HexTimeValue = $6_hex
            include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-decodeTimeValue.include.snmptrap.rules"
            $hnEventHistoryTimeValue = $decodedTimeValue
            $hnDeviceDescription = $7
            $hnDeviceId = extract($OID7,"\.([0-9]+)$")
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($hnEventHistoryId,$hnEventHistoryType,
                        $hnEventHistoryDescr,$hnEventHistorySeverity,
                        $hnEventHistoryServiceAffected,
                        $hnEventHistoryTimeValue,$hnDeviceDescription,
                        $hnDeviceId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hnEventHistoryId", $hnEventHistoryId, "hnEventHistoryType", $hnEventHistoryType, "hnEventHistoryDescr", $hnEventHistoryDescr,
                 "hnEventHistorySeverity", $hnEventHistorySeverity, "hnEventHistoryServiceAffected", $hnEventHistoryServiceAffected, "hnEventHistoryTimeValue", $hnEventHistoryTimeValue,
                 "hnDeviceDescription", $hnDeviceDescription, "hnDeviceId", $hnDeviceId)

            $OS_EventId = "SNMPTRAP-hatteras-HN-CFM-MIB-hnCfmCcRdiNotification"

            @AlertGroup = "CFM CC RDI"
            @AlertKey = "hnDeviceEntry." + $hnDeviceId
            @Summary = "CFM Continuity Check RDI  ( " + @AlertKey + " )"

            switch($4)
            {
                case "1": ### cleared
                    $SEV_KEY = $OS_EventId + "_cleared"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    @Summary = "End of: " + @Summary

                case "2": ### notAlarmed
                    $SEV_KEY = $OS_EventId + "_notAlarmed"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "3": ### notReported
                    $SEV_KEY = $OS_EventId + "_notReported"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "4": ### minor
                    $SEV_KEY = $OS_EventId + "_minor"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "5": ### major
                    $SEV_KEY = $OS_EventId + "_major"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "6": ### critical
                    $SEV_KEY = $OS_EventId + "_critical"
                    $DEFAULT_Severity = 5
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            update(@Severity)
            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        case "2": ### hnCfmCcMacStatusNotification

            ##########
            # $1 = hnEventHistoryId 
            # $2 = hnEventHistoryType 
            # $3 = hnEventHistoryDescr 
            # $4 = hnEventHistorySeverity 
            # $5 = hnEventHistoryServiceAffected 
            # $6 = hnEventHistoryTimeValue 
            # $7 = hnDeviceDescription 
            ##########

            $hnEventHistoryId = $1
            $hnEventHistoryType = lookup($2,HnAlarmConditionType) + " ( " + $2 + " )"
            $hnEventHistoryDescr = $3
            $hnEventHistorySeverity = lookup($4,HnAlarmSeverity) + " ( " + $4 + " )"
            $hnEventHistoryServiceAffected = lookup($5,HnAlarmServiceAffected) + " ( " + $5 + " )"
            $HexTimeValue = $6_hex
            include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-decodeTimeValue.include.snmptrap.rules"
            $hnEventHistoryTimeValue = $decodedTimeValue
            $hnDeviceDescription = $7
            $hnDeviceId = extract($OID7,"\.([0-9]+)$")
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($hnEventHistoryId,$hnEventHistoryType,
                        $hnEventHistoryDescr,$hnEventHistorySeverity,
                        $hnEventHistoryServiceAffected,
                        $hnEventHistoryTimeValue,$hnDeviceDescription,
                        $hnDeviceId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hnEventHistoryId", $hnEventHistoryId, "hnEventHistoryType", $hnEventHistoryType, "hnEventHistoryDescr", $hnEventHistoryDescr,
                 "hnEventHistorySeverity", $hnEventHistorySeverity, "hnEventHistoryServiceAffected", $hnEventHistoryServiceAffected, "hnEventHistoryTimeValue", $hnEventHistoryTimeValue,
                 "hnDeviceDescription", $hnDeviceDescription, "hnDeviceId", $hnDeviceId)

            $OS_EventId = "SNMPTRAP-hatteras-HN-CFM-MIB-hnCfmCcMacStatusNotification"

            @AlertGroup = "CFM CC MAC Status"
            @AlertKey = "hnDeviceEntry." + $hnDeviceId
            @Summary = "CFM Continuity Check MAC Status  ( " + @AlertKey + " )"

            switch($4)
            {
                case "1": ### cleared
                    $SEV_KEY = $OS_EventId + "_cleared"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    @Summary = "End of: " + @Summary

                case "2": ### notAlarmed
                    $SEV_KEY = $OS_EventId + "_notAlarmed"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "3": ### notReported
                    $SEV_KEY = $OS_EventId + "_notReported"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "4": ### minor
                    $SEV_KEY = $OS_EventId + "_minor"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "5": ### major
                    $SEV_KEY = $OS_EventId + "_major"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "6": ### critical
                    $SEV_KEY = $OS_EventId + "_critical"
                    $DEFAULT_Severity = 5
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            update(@Severity)
            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        case "3": ### hnCfmCcRemoteMepNotification

            ##########
            # $1 = hnEventHistoryId 
            # $2 = hnEventHistoryType 
            # $3 = hnEventHistoryDescr 
            # $4 = hnEventHistorySeverity 
            # $5 = hnEventHistoryServiceAffected 
            # $6 = hnEventHistoryTimeValue 
            # $7 = hnDeviceDescription 
            ##########

            $hnEventHistoryId = $1
            $hnEventHistoryType = lookup($2,HnAlarmConditionType) + " ( " + $2 + " )"
            $hnEventHistoryDescr = $3
            $hnEventHistorySeverity = lookup($4,HnAlarmSeverity) + " ( " + $4 + " )"
            $hnEventHistoryServiceAffected = lookup($5,HnAlarmServiceAffected) + " ( " + $5 + " )"
            $HexTimeValue = $6_hex
            include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-decodeTimeValue.include.snmptrap.rules"
            $hnEventHistoryTimeValue = $decodedTimeValue
            $hnDeviceDescription = $7
            $hnDeviceId = extract($OID7,"\.([0-9]+)$")
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($hnEventHistoryId,$hnEventHistoryType,
                        $hnEventHistoryDescr,$hnEventHistorySeverity,
                        $hnEventHistoryServiceAffected,
                        $hnEventHistoryTimeValue,$hnDeviceDescription,
                        $hnDeviceId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hnEventHistoryId", $hnEventHistoryId, "hnEventHistoryType", $hnEventHistoryType, "hnEventHistoryDescr", $hnEventHistoryDescr,
                 "hnEventHistorySeverity", $hnEventHistorySeverity, "hnEventHistoryServiceAffected", $hnEventHistoryServiceAffected, "hnEventHistoryTimeValue", $hnEventHistoryTimeValue,
                 "hnDeviceDescription", $hnDeviceDescription, "hnDeviceId", $hnDeviceId)


            $OS_EventId = "SNMPTRAP-hatteras-HN-CFM-MIB-hnCfmCcRemoteMepNotification"

            @AlertGroup = "CFM CC Remote MEP"
            @AlertKey = "hnDeviceEntry." + $hnDeviceId
            @Summary = "CFM Continuity Check Remote MEP  ( " + @AlertKey + " )"

            switch($4)
            {
                case "1": ### cleared
                    $SEV_KEY = $OS_EventId + "_cleared"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    @Summary = "End of: " + @Summary

                case "2": ### notAlarmed
                    $SEV_KEY = $OS_EventId + "_notAlarmed"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "3": ### notReported
                    $SEV_KEY = $OS_EventId + "_notReported"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "4": ### minor
                    $SEV_KEY = $OS_EventId + "_minor"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "5": ### major
                    $SEV_KEY = $OS_EventId + "_major"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "6": ### critical
                    $SEV_KEY = $OS_EventId + "_critical"
                    $DEFAULT_Severity = 5
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            update(@Severity)
            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        case "4": ### hnCfmCcErrorNotification

            ##########
            # $1 = hnEventHistoryId 
            # $2 = hnEventHistoryType 
            # $3 = hnEventHistoryDescr 
            # $4 = hnEventHistorySeverity 
            # $5 = hnEventHistoryServiceAffected 
            # $6 = hnEventHistoryTimeValue 
            # $7 = hnDeviceDescription 
            ##########

            $hnEventHistoryId = $1
            $hnEventHistoryType = lookup($2,HnAlarmConditionType) + " ( " + $2 + " )"
            $hnEventHistoryDescr = $3
            $hnEventHistorySeverity = lookup($4,HnAlarmSeverity) + " ( " + $4 + " )"
            $hnEventHistoryServiceAffected = lookup($5,HnAlarmServiceAffected) + " ( " + $5 + " )"
            $HexTimeValue = $6_hex
            include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-decodeTimeValue.include.snmptrap.rules"
            $hnEventHistoryTimeValue = $decodedTimeValue
            $hnDeviceDescription = $7
            $hnDeviceId = extract($OID7,"\.([0-9]+)$")
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($hnEventHistoryId,$hnEventHistoryType,
                        $hnEventHistoryDescr,$hnEventHistorySeverity,
                        $hnEventHistoryServiceAffected,
                        $hnEventHistoryTimeValue,$hnDeviceDescription,
                        $hnDeviceId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hnEventHistoryId", $hnEventHistoryId, "hnEventHistoryType", $hnEventHistoryType, "hnEventHistoryDescr", $hnEventHistoryDescr,
                 "hnEventHistorySeverity", $hnEventHistorySeverity, "hnEventHistoryServiceAffected", $hnEventHistoryServiceAffected, "hnEventHistoryTimeValue", $hnEventHistoryTimeValue,
                 "hnDeviceDescription", $hnDeviceDescription, "hnDeviceId", $hnDeviceId)

            $OS_EventId = "SNMPTRAP-hatteras-HN-CFM-MIB-hnCfmCcErrorNotification"

            @AlertGroup = "CFM CC Error"
            @AlertKey = "hnDeviceEntry." + $hnDeviceId
            @Summary = "CFM Continuity Check Error  ( " + @AlertKey + " )"

            switch($4)
            {
                case "1": ### cleared
                    $SEV_KEY = $OS_EventId + "_cleared"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    @Summary = "End of: " + @Summary

                case "2": ### notAlarmed
                    $SEV_KEY = $OS_EventId + "_notAlarmed"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "3": ### notReported
                    $SEV_KEY = $OS_EventId + "_notReported"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "4": ### minor
                    $SEV_KEY = $OS_EventId + "_minor"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "5": ### major
                    $SEV_KEY = $OS_EventId + "_major"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "6": ### critical
                    $SEV_KEY = $OS_EventId + "_critical"
                    $DEFAULT_Severity = 5
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            update(@Severity)
            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        case "5": ### hnCfmCcXConNotification

            ##########
            # $1 = hnEventHistoryId 
            # $2 = hnEventHistoryType 
            # $3 = hnEventHistoryDescr 
            # $4 = hnEventHistorySeverity 
            # $5 = hnEventHistoryServiceAffected 
            # $6 = hnEventHistoryTimeValue 
            # $7 = hnDeviceDescription 
            ##########

            $hnEventHistoryId = $1
            $hnEventHistoryType = lookup($2,HnAlarmConditionType) + " ( " + $2 + " )"
            $hnEventHistoryDescr = $3
            $hnEventHistorySeverity = lookup($4,HnAlarmSeverity) + " ( " + $4 + " )"
            $hnEventHistoryServiceAffected = lookup($5,HnAlarmServiceAffected) + " ( " + $5 + " )"
            $HexTimeValue = $6_hex
            include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-decodeTimeValue.include.snmptrap.rules"
            $hnEventHistoryTimeValue = $decodedTimeValue
            $hnDeviceDescription = $7
            $hnDeviceId = extract($OID7,"\.([0-9]+)$")
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($hnEventHistoryId,$hnEventHistoryType,
                        $hnEventHistoryDescr,$hnEventHistorySeverity,
                        $hnEventHistoryServiceAffected,
                        $hnEventHistoryTimeValue,$hnDeviceDescription,
                        $hnDeviceId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hnEventHistoryId", $hnEventHistoryId, "hnEventHistoryType", $hnEventHistoryType, "hnEventHistoryDescr", $hnEventHistoryDescr,
                 "hnEventHistorySeverity", $hnEventHistorySeverity, "hnEventHistoryServiceAffected", $hnEventHistoryServiceAffected, "hnEventHistoryTimeValue", $hnEventHistoryTimeValue,
                 "hnDeviceDescription", $hnDeviceDescription, "hnDeviceId", $hnDeviceId)

            $OS_EventId = "SNMPTRAP-hatteras-HN-CFM-MIB-hnCfmCcXConNotification"

            @AlertGroup = "CFM CC Cross-Connect Alarm"
            @AlertKey = "hnDeviceEntry." + $hnDeviceId
            @Summary = "CFM Continuity Check Cross-Connect  ( " + @AlertKey + " )"

            switch($4)
            {
                case "1": ### cleared
                    $SEV_KEY = $OS_EventId + "_cleared"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    @Summary = "End of: " + @Summary

                case "2": ### notAlarmed
                    $SEV_KEY = $OS_EventId + "_notAlarmed"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "3": ### notReported
                    $SEV_KEY = $OS_EventId + "_notReported"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "4": ### minor
                    $SEV_KEY = $OS_EventId + "_minor"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "5": ### major
                    $SEV_KEY = $OS_EventId + "_major"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "6": ### critical
                    $SEV_KEY = $OS_EventId + "_critical"
                    $DEFAULT_Severity = 5
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            update(@Severity)
            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, hatteras-HN-CFM-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, hatteras-HN-CFM-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-HN-CFM-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-HN-CFM-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... hatteras-HN-CFM-MIB.include.snmptrap.rules >>>>>")
