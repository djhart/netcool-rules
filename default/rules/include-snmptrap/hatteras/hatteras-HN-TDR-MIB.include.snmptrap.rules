###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 5.0 - Updated Release for HN400/HN4000 7.2.2.
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  HN-TDR-MIB
#
###############################################################################

case ".1.3.6.1.4.1.8550.2.3.1.3": ### Hatteras Networks  - Notifications from HN-TDR-MIB (200904130000Z)

    log(DEBUG, "<<<<< Entering... hatteras-HN-TDR-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Hatteras-HN-TDR-MIB"
    @Class = "40535"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### hnTdrAFDINotification

            ##########
            # $1 = ifIndex 
            # $2 = ifAlias 
            # $3 = hnTdrTestResultTime 
            # $4 = hnTdrTestResultSource 
            # $5 = hnTdrTestResultMode 
            # $6 = hnTdrTestResultGauge 
            # $7 = hnTdrTestResultVelocity 
            # $8 = hnTdrTestResultResult 
            # $9 = hnTdrTestResultInterpretation 
            # $10 = hnTdrTestResultDistance 
            # $11 = hnTdrTestResultAfdiFault 
            ##########

            $ifIndex = $1
            $ifAlias = $2

            $HexTimeValue = $3_hex
            include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-decodeTimeValue.include.snmptrap.rules"
            $hnEventHistoryTimeValue = $decodedTimeValue
            $hnTdrTestResultSource = $4
            $hnTdrTestResultMode = lookup($5,HnTdrModeValue)
            $hnTdrTestResultGauge = lookup($6,HnTdrGaugeValue)
            $hnTdrTestResultVelocity = $7
            $hnTdrTestResultResult = lookup($8,HnTdrResultValue)
            $hnTdrTestResultInterpretation = $9
            $hnTdrTestResultDistance = $10
            $hnTdrTestResultAfdiFault = lookup($11,HnTdrAfdiFaultValue)

            $OS_EventId = "SNMPTRAP-hatteras-HN-TDR-MIB-hnTdrAFDINotification"

            @AlertGroup = "TDR AFDI Test " + $hnTdrTestResultMode
            @AlertKey = "ifIndex." + $ifIndex
            @Summary = @AlertGroup + ", Gauge: " + $hnTdrTestResultGauge + ", Result: " + $hnTdrTestResultResult + "  ( " + $2 + " )"
            update(@Summary)

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $8

            $hnTdrTestResultMode = $hnTdrTestResultMode + " ( " + $5 + " )"
            $hnTdrTestResultGauge = $hnTdrTestResultGauge + " ( " +$6 + " )"
            $hnTdrTestResultResult = $hnTdrTestResultResult + " ( " + $8 + " )"
            $hnTdrTestResultAfdiFault = $hnTdrTestResultAfdiFault + " ( " + $11 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($ifIndex,$ifAlias,$hnTdrTestResultTime,$hnTdrTestResultSource,
                        $hnTdrTestResultMode,$hnTdrTestResultGauge,
                        $hnTdrTestResultVelocity,$hnTdrTestResultResult,
                        $hnTdrTestResultInterpretation,$hnTdrTestResultDistance,
                        $hnTdrTestResultAfdiFault)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "ifIndex", $ifIndex, "ifAlias", $ifAlias, "hnTdrTestResultTime", $hnTdrTestResultTime,
                 "hnTdrTestResultSource", $hnTdrTestResultSource, "hnTdrTestResultMode", $hnTdrTestResultMode, "hnTdrTestResultGauge", $hnTdrTestResultGauge,
                 "hnTdrTestResultVelocity", $hnTdrTestResultVelocity, "hnTdrTestResultResult", $hnTdrTestResultResult, "hnTdrTestResultInterpretation", $hnTdrTestResultInterpretation,
                 "hnTdrTestResultDistance", $hnTdrTestResultDistance, "hnTdrTestResultAfdiFault", $hnTdrTestResultAfdiFault)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, hatteras-HN-TDR-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, hatteras-HN-TDR-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-HN-TDR-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-HN-TDR-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... hatteras-HN-TDR-MIB.include.snmptrap.rules >>>>>")
