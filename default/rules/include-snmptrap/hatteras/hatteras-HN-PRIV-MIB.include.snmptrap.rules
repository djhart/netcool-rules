###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 5.0 - Updated Release for HN400/HN4000 7.2.2.
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  HN-PRIV-MIB
#
###############################################################################

case ".1.3.6.1.4.1.8550.2.4.1.7": ### Hatteras Networks - Notifications from HN-PRIV-MIB (200904210000Z)

    log(DEBUG, "<<<<< Entering... hatteras-HN-PRIV-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Hatteras-HN-PRIV-MIB"
    @Class = "40535"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### hnPrivFileTransferStartedNotification

            ##########
            # $1 = hnfsDeviceId 
            # $2 = hnfsIndex 
            # $3 = hnfsIpAddress 
            # $4 = hnfsPath 
            # $5 = hnfsFileIndex 
            # $6 = hnfsAction 
            # $7 = hnfsStatus 
            ##########

            $hnfsDeviceId = $1
            $hnfsIndex = lookup($2,HnfsIndex)
            $hnfsIpAddress = $3
            $hnfsPath = $4
            $hnfsFileIndex = $5
            $hnfsAction = lookup($6,HnfsAction)
            $hnfsStatus = lookup($7,HnResult)

            $OS_EventId = "SNMPTRAP-hatteras-HN-PRIV-MIB-hnPrivFileTransferStartedNotification"

            @AlertGroup = lookup($2,HnfsIndex) + " FTP Transfer Status"
            @AlertKey = "hnFileServerEntry." + $hnfsDeviceId + "." + $2

            @Summary = $hnfsIndex + " File Transfer Started on Server " + $3
            @Summary = @Summary + "  ( Device ID: " + $1 + ", File: " + $5 + ", Path: " + $4 + " )"

            switch($7)
            {
                case "1": ### noAction
                    $SEV_KEY = $OS_EventId + "_noAction"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "2": ### busy
                    $SEV_KEY = $OS_EventId + "_busy"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "3": ### complete
                    $SEV_KEY = $OS_EventId + "_complete"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0

                case "4": ### failure
                    $SEV_KEY = $OS_EventId + "_failure"
                    $DEFAULT_Severity = 5
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            update(@Severity)
            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + $1

            $hnfsIndex = $hnfsIndex + " ( " + $2 + " )"
            $hnfsAction = $hnfsAction + " ( " + $6 + " )"
            $hnfsStatus = $hnfsStatus + " ( " + $7 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($hnfsDeviceId,$hnfsIndex,
                        $hnfsIpAddress,$hnfsPath,
                        $hnfsFileIndex,$hnfsAction,
                        $hnfsStatus)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hnfsDeviceId", $hnfsDeviceId, "hnfsIndex", $hnfsIndex, "hnfsIpAddress", $hnfsIpAddress,
                 "hnfsPath", $hnfsPath, "hnfsFileIndex", $hnfsFileIndex, "hnfsAction", $hnfsAction,
                 "hnfsStatus", $hnfsStatus)

        case "2": ### hnPrivFileTransferCompleteNotification

            ##########
            # $1 = hnfsDeviceId 
            # $2 = hnfsIndex 
            # $3 = hnfsIpAddress 
            # $4 = hnfsPath 
            # $5 = hnfsFileIndex 
            # $6 = hnfsAction 
            # $7 = hnfsStatus 
            # $8 = hnfsFailureCause 
            ##########

            $hnfsDeviceId = $1
            $hnfsIndex = lookup($2,HnfsIndex)
            $hnfsIpAddress = $3
            $hnfsPath = $4
            $hnfsFileIndex = $5
            $hnfsAction = lookup($6,HnfsAction)
            $hnfsStatus = lookup($7,HnResult)
            $hnfsFailureCause = lookup($8,HnFsTransferError)

            $OS_EventId = "SNMPTRAP-hatteras-HN-PRIV-MIB-hnPrivFileTransferCompleteNotification"

            @AlertGroup = lookup($2,HnfsIndex) + " FTP Transfer Status"
            @AlertKey = "hnFileServerEntry." + $hnfsDeviceId + "." + $2

            @Summary = $hnfsIndex + " File Transfer " + $hnfsStatus + " on Server " + $3
            @Summary = @Summary + "  ( Device ID: " + $1 + ", File: " + $5 + ", Path: " + $4 + " )"

            switch($7)
            {
                case "1": ### noAction
                    $SEV_KEY = $OS_EventId + "_noAction"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "2": ### busy
                    $SEV_KEY = $OS_EventId + "_busy"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "3": ### complete
                    $SEV_KEY = $OS_EventId + "_complete"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0

                case "4": ### failure
                    $SEV_KEY = $OS_EventId + "_failure"
                    $DEFAULT_Severity = 5
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            update(@Severity)
            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $7 + $1

            $hnfsIndex = $hnfsIndex + " ( " + $2 + " )"
            $hnfsAction = $hnfsAction + " ( " + $6 + " )"
            $hnfsStatus = $hnfsStatus + " ( " + $7 + " )"
            $hnfsFailureCause = $hnfsFailureCause + " ( " + $8 + ")"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($hnfsDeviceId,$hnfsIndex,
                        $hnfsIpAddress,$hnfsPath,
                        $hnfsFileIndex,$hnfsAction,
                        $hnfsStatus,$hnfsFailureCause)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hnfsDeviceId", $hnfsDeviceId, "hnfsIndex", $hnfsIndex, "hnfsIpAddress", $hnfsIpAddress,
                 "hnfsPath", $hnfsPath, "hnfsFileIndex", $hnfsFileIndex, "hnfsAction", $hnfsAction,
                 "hnfsStatus", $hnfsStatus, "hnfsFailureCause", $hnfsFailureCause)

        case "3": ### hnPrivSaveRunningCompleteNotification

            ##########
            # $1 = hnConfigurationDeviceId 
            # $2 = hnConfigurationIndex 
            ##########

            $hnConfigurationDeviceId = $1
            $hnConfigurationIndex = $2
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($hnConfigurationDeviceId,
                        $hnConfigurationIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hnConfigurationDeviceId", $hnConfigurationDeviceId, "hnConfigurationIndex", $hnConfigurationIndex)

            $OS_EventId = "SNMPTRAP-hatteras-HN-PRIV-MIB-hnPrivSaveRunningCompleteNotification"

            @AlertGroup = "Safe Running Complete ( " + $hnSwImageAction + " )"
            @AlertKey = "hnConfigurationEntry." + $hnConfigurationDeviceId + "." + $hnConfigurationIndex
            @Summary = "Safe Running Operation Complete  ( " + @AlertKey + " )"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        case "4": ### hnPrivSwImageActionCompleteNotification

            ##########
            # $1 = hnSwImageDeviceId 
            # $2 = hnSwImageIndex 
            # $3 = hnSwImageAction 
            ##########

            $hnSwImageDeviceId = $1
            $hnSwImageIndex = $2
            $hnSwImageAction = lookup($3,HnSwImageAction)

            $OS_EventId = "SNMPTRAP-hatteras-HN-PRIV-MIB-hnPrivSwImageActionCompleteNotification"

            @AlertGroup = "SW Image Action Complete ( " + $hnSwImageAction + " )"
            @AlertKey = "hnSwImageEntry." + $hnSwImageDeviceId + "." + $hnSwImageIndex
            @Summary = "SW Image Action Complete: " + $hnSwImageAction + "  ( " + @AlertKey + " )"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            $hnSwImageAction = $hnSwImageAction + " ( " + $3 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($hnSwImageDeviceId,$hnSwImageIndex,
                        $hnSwImageAction)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hnSwImageDeviceId", $hnSwImageDeviceId, "hnSwImageIndex", $hnSwImageIndex, "hnSwImageAction", $hnSwImageAction)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, hatteras-HN-PRIV-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, hatteras-HN-PRIV-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-HN-PRIV-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-HN-PRIV-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... hatteras-HN-PRIV-MIB.include.snmptrap.rules >>>>>")
