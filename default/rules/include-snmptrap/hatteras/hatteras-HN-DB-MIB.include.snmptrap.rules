###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 5.0 - Updated Release for HN400/HN4000 7.2.2.
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  HN-DB-MIB
#
###############################################################################

case ".1.3.6.1.4.1.8550.2.1.6.3": ### Hatteras Networks - Notifications from HN-DB-MIB (200611010000Z)

    log(DEBUG, "<<<<< Entering... hatteras-HN-DB-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Hatteras-HN-DB-MIB"
    @Class = "40535"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### hnDataBaseUpdate

            ##########
            # $1 = hnDBChangeIndex 
            # $2 = hnTrapHostOriginatingIPAddr
            ##########

            $hnDBChangeIndex = $1
            $hnTrapHostOriginatingIPAddr = $2
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($hnDBChangeIndex,
                        $hnTrapHostOriginatingIPAddr)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hnDBChangeIndex", $hnDBChangeIndex, "hnTrapHostOriginatingIPAddr", $hnTrapHostOriginatingIPAddr)

            $OS_EventId = "SNMPTRAP-hatteras-HN-DB-MIB-hnDataBaseUpdate"

            @AlertGroup = "Database Update"
            @AlertKey = "hnDBChangeEntry." + $hnDBChangeIndex
            @Summary = "An Entry Has Been Added to the Event Database  ( " + @AlertKey + " )"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, hatteras-HN-DB-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, hatteras-HN-DB-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-HN-DB-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-HN-DB-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... hatteras-HN-DB-MIB.include.snmptrap.rules >>>>>")
