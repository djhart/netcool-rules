###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 5.0 - Updated Release for HN400/HN4000 7.2.2.
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  HN-ACC-MIB
#
###############################################################################

case ".1.3.6.1.4.1.8550.2.1.11.3": ### Hatteras Networks - Notifications from HN-ACC-MIB (200808250000Z)

    log(DEBUG, "<<<<< Entering... hatteras-HN-ACC-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Hatteras-HN-ACC-MIB"
    @Class = "40535"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### hnAccInNotification

            ##########
            # $1 = hnEventHistoryId 
            # $2 = hnEventHistoryType 
            # $3 = hnEventHistoryDescr 
            # $4 = hnEventHistorySeverity 
            # $5 = hnEventHistoryServiceAffected 
            # $6 = hnEventHistoryTimeValue 
            # $7 = hnDeviceDescription 
            ##########

            $hnEventHistoryId = $1
            $hnEventHistoryType = lookup($2,HnAlarmConditionType) + " ( " + $2 + " )"
            $hnEventHistoryDescr = $3
            $hnEventHistorySeverity = lookup($4,HnAlarmSeverity) + " ( " + $4 + " )"
            $hnEventHistoryServiceAffected = lookup($5,HnAlarmServiceAffected) + " ( " + $5 + " )"
            $HexTimeValue = $6_hex
            include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-decodeTimeValue.include.snmptrap.rules"
            $hnEventHistoryTimeValue = $decodedTimeValue
            $hnDeviceDescription = $7
            $hnDeviceId = extract($OID7,"\.([0-9]+)$")
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($hnEventHistoryId,$hnEventHistoryType,
                        $hnEventHistoryDescr,$hnEventHistorySeverity,
                        $hnEventHistoryServiceAffected,
                        $hnEventHistoryTimeValue,$hnDeviceDescription,
                        $hnDeviceId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hnEventHistoryId", $hnEventHistoryId, "hnEventHistoryType", $hnEventHistoryType, "hnEventHistoryDescr", $hnEventHistoryDescr,
                 "hnEventHistorySeverity", $hnEventHistorySeverity, "hnEventHistoryServiceAffected", $hnEventHistoryServiceAffected, "hnEventHistoryTimeValue", $hnEventHistoryTimeValue,
                 "hnDeviceDescription", $hnDeviceDescription, "hnDeviceId", $hnDeviceId)

            $OS_EventId = "SNMPTRAP-hatteras-HN-ACC-MIB-hnAccInNotification"

            @AlertGroup = "ACC IN Alarm"
            @AlertKey = "hnDeviceEntry." + $hnDeviceId
            @Summary = "ACC IN Alarm  ( " + @AlertKey + " )"

            switch($4)
            {
                case "1": ### cleared
                    $SEV_KEY = $OS_EventId + "_cleared"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    @Summary = "End of: " + @Summary

                case "2": ### notAlarmed
                    $SEV_KEY = $OS_EventId + "_notAlarmed"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "3": ### notReported
                    $SEV_KEY = $OS_EventId + "_notReported"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "4": ### minor
                    $SEV_KEY = $OS_EventId + "_minor"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "5": ### major
                    $SEV_KEY = $OS_EventId + "_major"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "6": ### critical
                    $SEV_KEY = $OS_EventId + "_critical"
                    $DEFAULT_Severity = 5
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            update(@Severity)
            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        case "2": ### hnAccMux1Notification

            ##########
            # $1 = hnEventHistoryId 
            # $2 = hnEventHistoryType 
            # $3 = hnEventHistoryDescr 
            # $4 = hnEventHistorySeverity 
            # $5 = hnEventHistoryServiceAffected 
            # $6 = hnEventHistoryTimeValue 
            # $7 = hnDeviceDescription 
            ##########

            $hnEventHistoryId = $1
            $hnEventHistoryType = lookup($2,HnAlarmConditionType) + " ( " + $2 + " )"
            $hnEventHistoryDescr = $3
            $hnEventHistorySeverity = lookup($4,HnAlarmSeverity) + " ( " + $4 + " )"
            $hnEventHistoryServiceAffected = lookup($5,HnAlarmServiceAffected) + " ( " + $5 + " )"
            $HexTimeValue = $6_hex
            include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-decodeTimeValue.include.snmptrap.rules"
            $hnEventHistoryTimeValue = $decodedTimeValue
            $hnDeviceDescription = $7
            $hnDeviceId = extract($OID7,"\.([0-9]+)$")
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($hnEventHistoryId,$hnEventHistoryType,
                        $hnEventHistoryDescr,$hnEventHistorySeverity,
                        $hnEventHistoryServiceAffected,
                        $hnEventHistoryTimeValue,$hnDeviceDescription,
                        $hnDeviceId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hnEventHistoryId", $hnEventHistoryId, "hnEventHistoryType", $hnEventHistoryType, "hnEventHistoryDescr", $hnEventHistoryDescr,
                 "hnEventHistorySeverity", $hnEventHistorySeverity, "hnEventHistoryServiceAffected", $hnEventHistoryServiceAffected, "hnEventHistoryTimeValue", $hnEventHistoryTimeValue,
                 "hnDeviceDescription", $hnDeviceDescription, "hnDeviceId", $hnDeviceId)

            $OS_EventId = "SNMPTRAP-hatteras-HN-ACC-MIB-hnAccMux1Notification"

            @AlertGroup = "ACC MUX1 Alarm"
            @AlertKey = "hnDeviceEntry." + $hnDeviceId
            @Summary = "ACC MUX1 Alarm  ( " + @AlertKey + " )"

            switch($4)
            {
                case "1": ### cleared
                    $SEV_KEY = $OS_EventId + "_cleared"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    @Summary = "End of: " + @Summary

                case "2": ### notAlarmed
                    $SEV_KEY = $OS_EventId + "_notAlarmed"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "3": ### notReported
                    $SEV_KEY = $OS_EventId + "_notReported"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "4": ### minor
                    $SEV_KEY = $OS_EventId + "_minor"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "5": ### major
                    $SEV_KEY = $OS_EventId + "_major"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "6": ### critical
                    $SEV_KEY = $OS_EventId + "_critical"
                    $DEFAULT_Severity = 5
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            update(@Severity)
            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        case "3": ### hnAccMux2Notification

            ##########
            # $1 = hnEventHistoryId 
            # $2 = hnEventHistoryType 
            # $3 = hnEventHistoryDescr 
            # $4 = hnEventHistorySeverity 
            # $5 = hnEventHistoryServiceAffected 
            # $6 = hnEventHistoryTimeValue 
            # $7 = hnDeviceDescription 
            ##########

            $hnEventHistoryId = $1
            $hnEventHistoryType = lookup($2,HnAlarmConditionType) + " ( " + $2 + " )"
            $hnEventHistoryDescr = $3
            $hnEventHistorySeverity = lookup($4,HnAlarmSeverity) + " ( " + $4 + " )"
            $hnEventHistoryServiceAffected = lookup($5,HnAlarmServiceAffected) + " ( " + $5 + " )"
            $HexTimeValue = $6_hex
            include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-decodeTimeValue.include.snmptrap.rules"
            $hnEventHistoryTimeValue = $decodedTimeValue
            $hnDeviceDescription = $7
            $hnDeviceId = extract($OID7,"\.([0-9]+)$")
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($hnEventHistoryId,$hnEventHistoryType,
                        $hnEventHistoryDescr,$hnEventHistorySeverity,
                        $hnEventHistoryServiceAffected,
                        $hnEventHistoryTimeValue,$hnDeviceDescription,
                        $hnDeviceId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hnEventHistoryId", $hnEventHistoryId, "hnEventHistoryType", $hnEventHistoryType, "hnEventHistoryDescr", $hnEventHistoryDescr,
                 "hnEventHistorySeverity", $hnEventHistorySeverity, "hnEventHistoryServiceAffected", $hnEventHistoryServiceAffected, "hnEventHistoryTimeValue", $hnEventHistoryTimeValue,
                 "hnDeviceDescription", $hnDeviceDescription, "hnDeviceId", $hnDeviceId)

            $OS_EventId = "SNMPTRAP-hatteras-HN-ACC-MIB-hnAccMux2Notification"

            @AlertGroup = "ACC MUX2 Alarm"
            @AlertKey = "hnDeviceEntry." + $hnDeviceId
            @Summary = "ACC MUX2 Alarm  ( " + @AlertKey + " )"

            switch($4)
            {
                case "1": ### cleared
                    $SEV_KEY = $OS_EventId + "_cleared"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    @Summary = "End of: " + @Summary

                case "2": ### notAlarmed
                    $SEV_KEY = $OS_EventId + "_notAlarmed"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "3": ### notReported
                    $SEV_KEY = $OS_EventId + "_notReported"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "4": ### minor
                    $SEV_KEY = $OS_EventId + "_minor"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "5": ### major
                    $SEV_KEY = $OS_EventId + "_major"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "6": ### critical
                    $SEV_KEY = $OS_EventId + "_critical"
                    $DEFAULT_Severity = 5
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            update(@Severity)
            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        case "4": ### hnAccMux3Notification

            ##########
            # $1 = hnEventHistoryId 
            # $2 = hnEventHistoryType 
            # $3 = hnEventHistoryDescr 
            # $4 = hnEventHistorySeverity 
            # $5 = hnEventHistoryServiceAffected 
            # $6 = hnEventHistoryTimeValue 
            # $7 = hnDeviceDescription 
            ##########

            $hnEventHistoryId = $1
            $hnEventHistoryType = lookup($2,HnAlarmConditionType) + " ( " + $2 + " )"
            $hnEventHistoryDescr = $3
            $hnEventHistorySeverity = lookup($4,HnAlarmSeverity) + " ( " + $4 + " )"
            $hnEventHistoryServiceAffected = lookup($5,HnAlarmServiceAffected) + " ( " + $5 + " )"
            $HexTimeValue = $6_hex
            include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-decodeTimeValue.include.snmptrap.rules"
            $hnEventHistoryTimeValue = $decodedTimeValue
            $hnDeviceDescription = $7
            $hnDeviceId = extract($OID7,"\.([0-9]+)$")
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($hnEventHistoryId,$hnEventHistoryType,
                        $hnEventHistoryDescr,$hnEventHistorySeverity,
                        $hnEventHistoryServiceAffected,
                        $hnEventHistoryTimeValue,$hnDeviceDescription,
                        $hnDeviceId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hnEventHistoryId", $hnEventHistoryId, "hnEventHistoryType", $hnEventHistoryType, "hnEventHistoryDescr", $hnEventHistoryDescr,
                 "hnEventHistorySeverity", $hnEventHistorySeverity, "hnEventHistoryServiceAffected", $hnEventHistoryServiceAffected, "hnEventHistoryTimeValue", $hnEventHistoryTimeValue,
                 "hnDeviceDescription", $hnDeviceDescription, "hnDeviceId", $hnDeviceId)

            $OS_EventId = "SNMPTRAP-hatteras-HN-ACC-MIB-hnAccMux3Notification"

            @AlertGroup = "ACC MUX3 Alarm"
            @AlertKey = "hnDeviceEntry." + $hnDeviceId
            @Summary = "ACC MUX3 Alarm  ( " + @AlertKey + " )"

            switch($4)
            {
                case "1": ### cleared
                    $SEV_KEY = $OS_EventId + "_cleared"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    @Summary = "End of: " + @Summary

                case "2": ### notAlarmed
                    $SEV_KEY = $OS_EventId + "_notAlarmed"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "3": ### notReported
                    $SEV_KEY = $OS_EventId + "_notReported"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "4": ### minor
                    $SEV_KEY = $OS_EventId + "_minor"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "5": ### major
                    $SEV_KEY = $OS_EventId + "_major"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "6": ### critical
                    $SEV_KEY = $OS_EventId + "_critical"
                    $DEFAULT_Severity = 5
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            update(@Severity)
            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        case "5": ### hnAccInputNotification

            ##########
            # $1 = hnDeviceId 
            # $2 = hnAccInIndex 
            # $3 = hnEventHistoryId 
            # $4 = hnEventHistoryType 
            # $5 = hnEventHistoryDescr 
            # $6 = hnEventHistorySeverity 
            # $7 = hnEventHistoryServiceAffected 
            # $8 = hnEventHistoryTimeValue 
            # $9 = hnDeviceDescription 
            ##########

            $hnDeviceId = $1
            $hnAccInIndex = $2
            $hnEventHistoryId = $3
            $hnEventHistoryType = lookup($4,HnAlarmConditionType) + " ( " + $4 + " )"
            $hnEventHistoryDescr = $5
            $hnEventHistorySeverity = lookup($6,HnAlarmSeverity) + " ( " + $6 + " )"
            $hnEventHistoryServiceAffected = lookup($7,HnAlarmServiceAffected) + " ( " + $7 + " )"
            $HexTimeValue = $8_hex
            include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-decodeTimeValue.include.snmptrap.rules"
            $hnEventHistoryTimeValue = $decodedTimeValue
            $hnDeviceDescription = $9
            $hnDeviceId = extract($OID7,"\.([0-9]+)$")
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($hnDeviceId,$hnAccInIndex,
                        $hnEventHistoryId,$hnEventHistoryType,
                        $hnEventHistoryDescr,$hnEventHistorySeverity,
                        $hnEventHistoryServiceAffected,
                        $hnEventHistoryTimeValue,$hnDeviceDescription)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hnDeviceId", $hnDeviceId, "hnAccInIndex", $hnAccInIndex, "hnEventHistoryId", $hnEventHistoryId,
                 "hnEventHistoryType", $hnEventHistoryType, "hnEventHistoryDescr", $hnEventHistoryDescr, "hnEventHistorySeverity", $hnEventHistorySeverity,
                 "hnEventHistoryServiceAffected", $hnEventHistoryServiceAffected, "hnEventHistoryTimeValue", $hnEventHistoryTimeValue, "hnDeviceDescription", $hnDeviceDescription)

            $OS_EventId = "SNMPTRAP-hatteras-HN-ACC-MIB-hnAccInputNotification"

            @AlertGroup = "ACC Input Alarm"
            @AlertKey = "hnAccInEntry." + $hnDeviceId + "." + $hnAccInIndex
            @Summary = "ACC Input Alarm  ( " + @AlertKey + " )"

            switch($6)
            {
                case "1": ### cleared
                    $SEV_KEY = $OS_EventId + "_cleared"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    @Summary = "End of: " + @Summary

                case "2": ### notAlarmed
                    $SEV_KEY = $OS_EventId + "_notAlarmed"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "3": ### notReported
                    $SEV_KEY = $OS_EventId + "_notReported"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "4": ### minor
                    $SEV_KEY = $OS_EventId + "_minor"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "5": ### major
                    $SEV_KEY = $OS_EventId + "_major"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "6": ### critical
                    $SEV_KEY = $OS_EventId + "_critical"
                    $DEFAULT_Severity = 5
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            update(@Severity)
            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, hatteras-HN-ACC-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, hatteras-HN-ACC-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-HN-ACC-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-HN-ACC-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... hatteras-HN-ACC-MIB.include.snmptrap.rules >>>>>")
