###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 5.0 - Updated Release for HN400/HN4000 7.2.2.
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  HN-PME-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table hatteras-HN-PME-MIB_sev =
{
    {"SNMPTRAP-hatteras-HN-PME-MIB-hnPmeSpanProfileMismatchNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-PME-MIB-hnPmeSpanProfileMismatchNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-PME-MIB-hnPmeSpanProfileMismatchNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-PME-MIB-hnPmeSpanProfileMismatchNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-PME-MIB-hnPmeSpanProfileMismatchNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-PME-MIB-hnPmeSpanProfileMismatchNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-PME-MIB-hnPmeSpanProfileMismatchNotification_unknown","2","1","0"}
}
default = {"Unknown","Unknown","Unknown"}
