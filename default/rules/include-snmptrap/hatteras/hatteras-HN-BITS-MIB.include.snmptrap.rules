###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 5.0 - Updated Release for HN400/HN4000 7.2.2.
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  HN-BITS-MIB
#
###############################################################################

case ".1.3.6.1.4.1.8550.2.7.8.3": ### Hatteras Networks - Notifications from HN-BITS-MIB (200808250000Z)

    log(DEBUG, "<<<<< Entering... hatteras-HN-BITS-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Hatteras-HN-BITS-MIB"
    @Class = "40535"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### hnBitsAisNotification

            ##########
            # $1 = ifIndex 
            # $2 = hnEventHistoryId 
            # $3 = hnEventHistoryType 
            # $4 = hnEventHistoryDescr 
            # $5 = hnEventHistorySeverity 
            # $6 = hnEventHistoryServiceAffected 
            # $7 = hnEventHistoryTimeValue 
            # $8 = hnDeviceDescription 
            ##########

            $ifIndex = $1 
            $hnEventHistoryId = $2 
            $hnEventHistoryType = lookup($3,HnAlarmConditionType) + " ( " + $3 + " )" 
            $hnEventHistoryDescr = $4 
            $hnEventHistorySeverity = lookup($5,HnAlarmSeverity) + " ( " + $5 + " )" 
            $hnEventHistoryServiceAffected = lookup($6,HnAlarmServiceAffected) + " ( " + $6 + " )" 
            $HexTimeValue = $7_hex
            include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-decodeTimeValue.include.snmptrap.rules"
            $hnEventHistoryTimeValue = $decodedTimeValue
            $hnDeviceDescription = $8 
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($ifIndex,$hnEventHistoryId,
                        $hnEventHistoryType,$hnEventHistoryDescr,
                        $hnEventHistorySeverity,
                        $hnEventHistoryServiceAffected,
                        $hnEventHistoryTimeValue,
                        $hnDeviceDescription)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "ifIndex", $ifIndex, "hnEventHistoryId", $hnEventHistoryId, "hnEventHistoryType", $hnEventHistoryType,
                 "hnEventHistoryDescr", $hnEventHistoryDescr, "hnEventHistorySeverity", $hnEventHistorySeverity, "hnEventHistoryServiceAffected", $hnEventHistoryServiceAffected,
                 "hnEventHistoryTimeValue", $hnEventHistoryTimeValue, "hnDeviceDescription", $hnDeviceDescription)

            $OS_EventId = "SNMPTRAP-hatteras-HN-BITS-MIB-hnBitsAisNotification"

            @AlertGroup = "BITS AIS"
            @AlertKey = "ifEntry." + $ifIndex 
            @Summary = "BITS Alarm Indication Signal  ( " + @AlertKey + " )" 

            switch($5)
            {
                case "1": ### cleared
                    $SEV_KEY = $OS_EventId + "_cleared"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    @Summary = "End of: " + @Summary

                case "2": ### notAlarmed
                    $SEV_KEY = $OS_EventId + "_notAlarmed"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "3": ### notReported
                    $SEV_KEY = $OS_EventId + "_notReported"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "4": ### minor
                    $SEV_KEY = $OS_EventId + "_minor"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "5": ### major
                    $SEV_KEY = $OS_EventId + "_major"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "6": ### critical
                    $SEV_KEY = $OS_EventId + "_critical"
                    $DEFAULT_Severity = 5
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            update(@Severity)
            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        case "2": ### hnBitsLosNotification

            ##########
            # $1 = ifIndex 
            # $2 = hnEventHistoryId 
            # $3 = hnEventHistoryType 
            # $4 = hnEventHistoryDescr 
            # $5 = hnEventHistorySeverity 
            # $6 = hnEventHistoryServiceAffected 
            # $7 = hnEventHistoryTimeValue 
            # $8 = hnDeviceDescription 
            ##########

            $ifIndex = $1 
            $hnEventHistoryId = $2 
            $hnEventHistoryType = lookup($3,HnAlarmConditionType) + " ( " + $3 + " )" 
            $hnEventHistoryDescr = $4 
            $hnEventHistorySeverity = lookup($5,HnAlarmSeverity) + " ( " + $5 + " )" 
            $hnEventHistoryServiceAffected = lookup($6,HnAlarmServiceAffected) + " ( " + $6 + " )" 
            $HexTimeValue = $7_hex
            include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-decodeTimeValue.include.snmptrap.rules"
            $hnEventHistoryTimeValue = $decodedTimeValue
            $hnDeviceDescription = $8 
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($ifIndex,$hnEventHistoryId,
                        $hnEventHistoryType,$hnEventHistoryDescr,
                        $hnEventHistorySeverity,
                        $hnEventHistoryServiceAffected,
                        $hnEventHistoryTimeValue,
                        $hnDeviceDescription)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "ifIndex", $ifIndex, "hnEventHistoryId", $hnEventHistoryId, "hnEventHistoryType", $hnEventHistoryType,
                 "hnEventHistoryDescr", $hnEventHistoryDescr, "hnEventHistorySeverity", $hnEventHistorySeverity, "hnEventHistoryServiceAffected", $hnEventHistoryServiceAffected,
                 "hnEventHistoryTimeValue", $hnEventHistoryTimeValue, "hnDeviceDescription", $hnDeviceDescription)

            $OS_EventId = "SNMPTRAP-hatteras-HN-BITS-MIB-hnBitsLosNotification"

            @AlertGroup = "BITS LOS"
            @AlertKey = "ifEntry." + $ifIndex 
            @Summary = "BITS Loss of Signal  ( " + @AlertKey + " )" 

            switch($5)
            {
                case "1": ### cleared
                    $SEV_KEY = $OS_EventId + "_cleared"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    @Summary = "End of: " + @Summary

                case "2": ### notAlarmed
                    $SEV_KEY = $OS_EventId + "_notAlarmed"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "3": ### notReported
                    $SEV_KEY = $OS_EventId + "_notReported"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "4": ### minor
                    $SEV_KEY = $OS_EventId + "_minor"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "5": ### major
                    $SEV_KEY = $OS_EventId + "_major"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "6": ### critical
                    $SEV_KEY = $OS_EventId + "_critical"
                    $DEFAULT_Severity = 5
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            update(@Severity)
            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, hatteras-HN-BITS-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, hatteras-HN-BITS-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-HN-BITS-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-HN-BITS-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... hatteras-HN-BITS-MIB.include.snmptrap.rules >>>>>")
