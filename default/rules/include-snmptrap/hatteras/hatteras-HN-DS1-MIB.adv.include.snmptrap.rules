###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 5.0 - Updated Release for HN400/HN4000 7.2.2.
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  HN-DS1-MIB
#
###############################################################################

log(DEBUG, "<<<<< Entering... hatteras-HN-DS1-MIB.adv.include.snmptrap.rules >>>>>")

switch($specific-trap)
{
    case "1": ### hnDs1AisNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "hnDs1AisNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ifEntry." + $ifIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "2": ### hnDs1LofNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "hnDs1LofNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ifEntry." + $ifIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "3": ### hnDs1LosNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "hnDs1LosNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ifEntry." + $ifIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "4": ### hnDs1RaiNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "hnDs1RaiNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ifEntry." + $ifIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "5": ### hnDs1LocmfNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "hnDs1LocmfNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ifEntry." + $ifIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "6": ### hnDs1LineLpbkNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "hnDs1LineLpbkNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ifEntry." + $ifIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "7": ### hnDs1PayloadLpbkNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "hnDs1PayloadLpbkNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ifEntry." + $ifIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "8": ### hnDs1NetworkLpbkNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "hnDs1NetworkLpbkNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ifEntry." + $ifIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "9": ### hnDs1DataUnderrunNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "hnDs1DataUnderrunNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ifEntry." + $ifIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "10": ### hnDs1FarEndDownNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "hnDs1FarEndDownNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ifEntry." + $ifIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "11": ### hnDs1FarEndDisabledNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "hnDs1FarEndDisabledNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ifEntry." + $ifIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "12": ### hnDs1CvlNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "hnDs1CvlNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ifEntry." + $ifIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "13": ### hnDs1EslNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "hnDs1EslNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ifEntry." + $ifIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "14": ### hnDs1SeslNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "hnDs1SeslNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ifEntry." + $ifIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "15": ### hnDs1CvpNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "hnDs1CvpNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ifEntry." + $ifIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "16": ### hnDs1EspNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "hnDs1EspNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ifEntry." + $ifIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "17": ### hnDs1SespNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "hnDs1SespNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ifEntry." + $ifIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "18": ### hnDs1SaspNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "hnDs1SaspNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ifEntry." + $ifIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "19": ### hnDs1CsspNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "hnDs1CsspNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ifEntry." + $ifIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "20": ### hnDs1UaspNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "hnDs1UaspNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ifEntry." + $ifIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "21": ### hnDs1EbpNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "hnDs1EbpNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ifEntry." + $ifIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "22": ### hnDs1BbepNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "hnDs1BbepNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ifEntry." + $ifIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "23": ### hnDs1BertActiveNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "hnDs1BertActiveNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ifEntry." + $ifIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    default:
}

log(DEBUG, "<<<<< Leaving... hatteras-HN-DS1-MIB.adv.include.snmptrap.rules >>>>>")
