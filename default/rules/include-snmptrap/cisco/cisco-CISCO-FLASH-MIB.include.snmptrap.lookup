##############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 2.0 - Trap(s) added
#
#         - ciscoFlashDeviceInsertedNotifRev1
#         - ciscoFlashDeviceRemovedNotifRev1
# 
#     - ciscoFlashCopyStatus table updated
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-FLASH-MIB
#
#          -  Based on lookup table definitions extracted from
#             cisco.include.snmptrap.lookup 2.8.
#
###############################################################################

table ciscoFlashCopyStatus = ### The status of the specified copy operation.
{  
    {"0","Copy Operation Pending"}, ### copyOperationPending
    {"1","Copy In Progress"}, ### specified operation is active
    {"2","Copy Operation Successful"}, ### specified operation is supported and    completed successfully
    {"3","Copy Command Invalid"}, ### command invalid or command-protocol-device combination unsupported
    {"4","Invalid Protocol"}, ### invalid protocol specified
    {"5","Invalid Source Name"}, ### invalid source file name specified For the  copy from flash to lex operation, this error code will be returned when the source file    is not a valid lex image.
    {"6","Invalid Destination Name"}, ### invalid target name (file or partition or device name) specified for the copy from flash to lex operation, this error code will be returned when no lex devices are connected to the router or when an invalid lex interface number has been specified in the destination string.
    {"7","Invalid Server Address"}, ### invalid server address specified
    {"8","Device Busy"}, ### specified device is in use and locked by another process
    {"9","Device Open Error"}, ### invalid device name
    {"10","Device Error"}, ### device read, write or erase error
    {"11","Read-Only Device"}, ### device is read-only but a write or erase operation was specified
    {"12","Device Full"}, ### device is filled to capacity
    {"13","Invalid Filename"}, ### invalid file name; file not found in partition
    {"14","File Transfer Error"}, ### file transfer was unsuccessfull; network failure
    {"15","File Checksum Error"}, ### file checksum in Flash failed
    {"16","No Memory"}, ### system running low on memory
    {"17","Unknown Copy Failure"}, ### failure unknown
    {"18","Invalid Signature"} ### copyInvalidSignature
}
default = "Unknown"

table ciscoFlashPartitioningStatus = ### The status of the specified partitioning operation.
{
    {"1","Partitioning In Progress"}, ### specified operation is active
    {"2","Partitioning Operation Successful"}, ### specified operation is supported and completed    successfully
    {"3","Partitioning Operation Invalid"}, ### command invalid or command-protocol-device combination unsupported
    {"4","Invalid Target Name"}, ### invalid target name (file or partition or    device name) specified
    {"5","Invalid Partition Count"}, ### invalid partition count specified for the partitioning command
    {"6","Invalid Partition Size"}, ### invalid partition size, or invalid count of partition sizes
    {"7","Device Busy"}, ### specified device is in use and locked by another process
    {"8","Invalid Device Name"}, ### invalid device name
    {"9","Device Error"}, ### device read, write or erase error
    {"10","No Memory"}, ### system running low on memory
    {"11","Unknown Partitioning Failure"}, ### failure unknown
}
default = "Unknown"

table ciscoFlashMiscOpStatus = ### The status of the specified operation.
{  
    {"1","Misc. Operation In Progress"}, ### specified operation is active
    {"2","Misc. Operation Successful"}, ### specified operation is supported and completed successfully
    {"3","Invalid Operation"}, ### command invalid or command-protocol-device combination unsupported
    {"4","Invalid Destination Name"}, ### invalid target name (file or partition or device name) specified
    {"5","Device Busy"}, ### specified device is in use and locked by another process
    {"6","Invalid Device Name"}, ### invalid device name
    {"7","Device Error"}, ### device read, write or erase error
    {"8","Device Not Programmable"}, ### device is read-only but a write or erase operation was specified
    {"9","Invalid File Name"}, ### invalid file name; file not found in partition
    {"10","File Delete Failure"}, ### file could not be deleted; delete count exceeded
    {"11","File Undelete Failure"}, ### file could not be undeleted; undelete count exceeded
    {"12","File Checksum Error"}, ### file has a bad checksum
    {"13","No Memory"}, ### system running low on memory
    {"14","Unknown Failure"}, ### failure unknown
    {"18","Squeeze Failure"}, ### the squeeze operation failed
    {"19","No Such File"}, ### a valid but nonexistent file name was specified
    {"20","Format Failure"}, ### the format operation failed
}
default = "Unknown"
