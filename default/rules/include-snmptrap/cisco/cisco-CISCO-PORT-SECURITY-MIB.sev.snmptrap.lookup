###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 2.0 - Trap(s) added
#
#          - cpsTrunkSecureMacAddrViolation
#          - cpsIfVlanSecureMacAddrViolation
#
# 1.1 - Changed the format
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-PORT-SECURITY-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table cisco-CISCO-PORT-SECURITY-MIB_sev =
{
    {"SNMPTRAP-cisco-CISCO-PORT-SECURITY-MIB-cpsSecureMacAddrViolation","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-PORT-SECURITY-MIB-cpsTrunkSecureMacAddrViolation","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-PORT-SECURITY-MIB-cpsIfVlanSecureMacAddrViolation","3","1","0"}
}
default = {"Unknown","Unknown","Unknown"}
