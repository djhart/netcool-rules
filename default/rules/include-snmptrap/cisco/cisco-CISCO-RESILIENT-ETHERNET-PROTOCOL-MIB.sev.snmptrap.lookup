###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-RESILIENT-ETHERNET-PROTOCOL-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table cisco-CISCO-RESILIENT-ETHERNET-PROTOCOL-MIB_sev =
{
    {"SNMPTRAP-cisco-CISCO-RESILIENT-ETHERNET-PROTOCOL-MIB-crepLinkStatus_none","4","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-RESILIENT-ETHERNET-PROTOCOL-MIB-crepLinkStatus_initDown","2","13","1800"}, 
    {"SNMPTRAP-cisco-CISCO-RESILIENT-ETHERNET-PROTOCOL-MIB-crepLinkStatus_noNeighbour","3","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-RESILIENT-ETHERNET-PROTOCOL-MIB-crepLinkStatus_oneWay","3","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-RESILIENT-ETHERNET-PROTOCOL-MIB-crepLinkStatus_twoWay","1","2","0"}, 
    {"SNMPTRAP-cisco-CISCO-RESILIENT-ETHERNET-PROTOCOL-MIB-crepLinkStatus_flapping","3","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-RESILIENT-ETHERNET-PROTOCOL-MIB-crepLinkStatus_wait","3","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-RESILIENT-ETHERNET-PROTOCOL-MIB-crepLinkStatus_unknown","2","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-RESILIENT-ETHERNET-PROTOCOL-MIB-crepPreemptionStatus_none","2","13","1800"}, 
    {"SNMPTRAP-cisco-CISCO-RESILIENT-ETHERNET-PROTOCOL-MIB-crepPreemptionStatus_preemptSuccessful","1","2","0"}, 
    {"SNMPTRAP-cisco-CISCO-RESILIENT-ETHERNET-PROTOCOL-MIB-crepPreemptionStatus_preemptFailure","3","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-RESILIENT-ETHERNET-PROTOCOL-MIB-crepPreemptionStatus_preemptTrigger","2","13","1800"}, 
    {"SNMPTRAP-cisco-CISCO-RESILIENT-ETHERNET-PROTOCOL-MIB-crepPreemptionStatus_preemptTriggerFailure","4","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-RESILIENT-ETHERNET-PROTOCOL-MIB-crepPreemptionStatus_unknown","2","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-RESILIENT-ETHERNET-PROTOCOL-MIB-crepPortRoleChange_failedPort","4","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-RESILIENT-ETHERNET-PROTOCOL-MIB-crepPortRoleChange_alternatePort","3","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-RESILIENT-ETHERNET-PROTOCOL-MIB-crepPortRoleChange_openPort","1","2","0"}, 
    {"SNMPTRAP-cisco-CISCO-RESILIENT-ETHERNET-PROTOCOL-MIB-crepPortRoleChange_unknown","2","1","0"} 
}
default = {"Unknown","Unknown","Unknown"}
