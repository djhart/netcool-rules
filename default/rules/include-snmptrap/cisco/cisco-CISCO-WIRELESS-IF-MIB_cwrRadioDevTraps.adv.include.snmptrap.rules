###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
#        1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-WIRELESS-IF-MIB
#
###############################################################################
#
# 1.1 - Added basic debug logging.
#
###############################################################################

log(DEBUG, "<<<<< Entering... cisco-CISCO-WIRELESS-IF-MIB_cwrRadioDevTraps.adv.include.snmptrap.rules >>>>>")

switch($specific-trap)
{
    case "1": ### cwrTrapConfigMismatch

        $OS_X733EventType = 3
        $OS_X733ProbableCause = 3011
        $OS_X733SpecificProb = "cwrTrapConfigMismatch"
        $OS_OsiLayer = 0
        
        $OS_LocalPriObj = "ifEntry." + $1
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "2": ### cwrTrapInitFailure

        $OS_X733EventType = 4
        $OS_X733ProbableCause = 4010
        $OS_X733SpecificProb = "cwrTrapInitFailure"
        $OS_OsiLayer = 0
        
        $OS_LocalPriObj = "ifEntry." + $1
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "3": ### cwrTrapLinkQuality

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1011
        $OS_X733SpecificProb = "cwrTrapLinkQuality"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "ifEntry." + $1
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "4": ### cwrTrapLinkSyncLost

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1011
        $OS_X733SpecificProb = "cwrTrapLinkSyncLost"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "ifEntry." + $1
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "5": ### cwrTrapLinkSyncAcquired

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1011
        $OS_X733SpecificProb = "cwrTrapLinkSyncAcquired"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "ifEntry." + $1
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "6": ### cwrTrapIfRxOsc

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1011
        $OS_X733SpecificProb = "cwrTrapIfRxOsc"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "cwrIntFreqEntry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "7": ### cwrTrapIfTxOsc

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1011
        $OS_X733SpecificProb = "cwrTrapIfTxOsc"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "cwrIntFreqEntry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "8": ### cwrTrapIfRefOsc

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1011
        $OS_X733SpecificProb = "cwrTrapIfRefOsc"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "cwrIntFreqEntry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "9": ### cwrTrapRfSupplyVoltage

        $OS_X733EventType = 4
        $OS_X733ProbableCause = 4001
        $OS_X733SpecificProb = "cwrTrapRfSupplyVoltage"
        $OS_OsiLayer = 1
        
        $OS_LocalPriObj = "ifEntry." + $1
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "10": ### cwrTrapRfRxOsc

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1011
        $OS_X733SpecificProb = "cwrTrapRfRxOsc"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "cwrRfEntry." + $1 + "." + $cwrRfIndex
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "11": ### cwrTrapRfTxOsc

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1011
        $OS_X733SpecificProb = "cwrTrapRfTxOsc"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "cwrRfEntry." + $1 + "." + $cwrRfIndex
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "12": ### cwrTrapRfTemp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1011
        $OS_X733SpecificProb = "cwrTrapRfTemp"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "cwrRfEntry." + $1 + "." + $cwrRfIndex
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "13": ### cwrTrapRfStatusChange

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1011
        $OS_X733SpecificProb = "cwrTrapRfStatusChange"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "cwrRfEntry." + $1 + "." + $cwrRfIndex
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "14": ### cwrTrapLink1HrThresh

        $OS_X733EventType = 2
        $OS_X733ProbableCause = 2005
        $OS_X733SpecificProb = "cwrTrapLink1HrThresh"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "ifEntry." + $1
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "15": ### cwrTrapLink24HrThresh

        $OS_X733EventType = 2
        $OS_X733ProbableCause = 2005
        $OS_X733SpecificProb = "cwrTrapLink24HrThresh"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "ifEntry." + $1
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    default:
}

log(DEBUG, "<<<<< Leaving... cisco-CISCO-WIRELESS-IF-MIB_cwrRadioDevTraps.adv.include.snmptrap.rules >>>>>")
