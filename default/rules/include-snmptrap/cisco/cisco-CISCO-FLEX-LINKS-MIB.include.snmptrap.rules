###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-FLEX-LINKS-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.471": ### Flex Links Feature Configuration and Status Query - Notifications from CISCO-FLEX-LINKS-MIB (200504250000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-FLEX-LINKS-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Cisco-Flex Links"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### cflIfStatusChangeNotif

            ##########
            # $1 = cflIfStatus 
            ##########

            $cflIfStatus = lookup($1, CflIfStatus)

            $OS_EventId = "SNMPTRAP-cisco-CISCO-FLEX-LINKS-MIB-cflIfStatusChangeNotif"

            @AlertGroup = "Interface Status"

            $cflIfIndex = extract($OID1, "\.([0-9]+)$")

            @AlertKey = "cflIfStatusEntry." + $cflIfIndex

            switch($1)
            {
                case "1": ### forwarding
                    @Summary = "Interface is Actively Forwarding Traffic"

                    $SEV_KEY = $OS_EventId + "_forwarding"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0

                case "2": ### blocking
                    @Summary = "Interface is Ready to Forward Traffic After Interface is Back Up From Down Operation"

                    $SEV_KEY = $OS_EventId + "_blocking"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "3": ### down
                    @Summary = "Interface Has Gone Down"

                    $SEV_KEY = $OS_EventId + "_down"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "4": ### waitingToSync
                    @Summary = "Interface is Waiting to Sync With Its Peer Interface"

                    $SEV_KEY = $OS_EventId + "_waitingToSync"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "5": ### waitingForPeerState
                    @Summary = "Interface is Waiting For Its Peer's State"

                    $SEV_KEY = $OS_EventId + "_waitingForPeerState"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                default:
                    @Summary = "Interface Has Entered An Unknown State"

                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            } 

            $cflIfStatus = $cflIfStatus + " ( " + $1 + " ) "
         
            @Summary = @Summary + " ( " + @AlertKey + " ) "

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cflIfStatus,$cflIfIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cflIfStatus", $cflIfStatus, "cflIfIndex", $cflIfIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-FLEX-LINKS-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-FLEX-LINKS-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-FLEX-LINKS-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-FLEX-LINKS-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-FLEX-LINKS-MIB.include.snmptrap.rules >>>>>")
