###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
# Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
# - CISCO-CEF-MIB
#
###############################################################################

table ciscoCefFailureReason =
{ 
    {"1","None"}, ### none - no failure. 
    {"2","Memory Allocation Failure"}, ### mallocFailure - memory allocation failed for CEF.
    {"3","Hardware Failure"}, ### hwFailure- hardware interface failure for CEF.
    {"4","Keep Alive Failure"}, ### keepaliveFailure- keepalive was not received from the CEF peer entity.
    {"5","No Msg Buffer"}, 
    ### noMsgBuffer - message buffers were exhausted while preparing IPC message to be sent to the CEF peer entity.
    {"6","Invalid Msg Size"}, ### invalidMsgSize - IPC message was received with invalid size from the CEF peer entity.
    {"7","Internal Error"} ### internalError - Some other internal error was detected for CEF.
}
default = "Unknown"


table ciscocefPeerOperState =
{ 
    {"1","Peer Disabled"}, 
    ### peerDisabled  - Cef Peer entity encounters fatal error i.e. resource allocation failure, ipc failure etc.
    ###               - When a reload/delete request is received from the Cef Peer Entity.
    {"2","Peer Up"}, ### peerUp - peer entity is up and no fatal error is encountered
    {"3","Peer Hold"} ### peerHold- Cef Peer entity is in held stage
}
default = "Unknown"


table ciscocefPeerFIBOperState =
{ 
    {"1","Peer FIB Down"}, ### peerFIBDown
    {"2","Peer FIB Up"}, ### peerFIBUp
    {"3","Peer FIB Reload Request"}, ### peerFIBReloadRequest
    {"4","Peer FIB Reloading"}, ### peerFIBReloading
    {"5","Peer FIB Synced"} ### peerFIBSynced
}
default = "Unknown"

