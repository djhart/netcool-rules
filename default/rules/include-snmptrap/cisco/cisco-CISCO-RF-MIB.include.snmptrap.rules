###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 2.0 - Trap(s) added
#         
#          - ciscoRFIssuStateNotif
#          - ciscoRFIssuStateNotifRev1
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-RF-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.176.2": ### Cisco Redundancy Framework (RF) Subsystem - Notifications from CISCO-RF-MIB (200509010000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-RF-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Cisco-Redundancy Framework"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch ($specific-trap)
    {
        case "1": ### ciscoRFSwactNotif

            ##########
            # $1 = cRFStatusUnitId
            # $2 = sysUpTime
            # $3 = cRFStatusLastSwactReasonCode 
            ##########

            $cRFStatusUnitId = $1
            $sysUpTime = $2
            $cRFStatusLastSwactReasonCode = lookup($3, ciscoRFSwactReasonType)
            
            $OS_EventId = "SNMPTRAP-cisco-CISCO-RF-MIB-ciscoRFSwactNotif"  

            @AlertGroup = "SWACT Status"
            @AlertKey = "Unit ID: " + $1
            @Summary = "SWACT: " + $1 + " Active: " + $cRFStatusLastSwactReasonCode
            switch($3)
            {
                case "1": ### unsupported - the 'reason code' is an unsupported feature
                    $SEV_KEY = $OS_EventId + "_unsupported"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "2": ### none - no SWACT has occurred
                    $SEV_KEY = $OS_EventId + "_none"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                case "3": ### notKnown - reason is unknown
                    $SEV_KEY = $OS_EventId + "_notKnown"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "4": ### userInitiated - a safe, manual SWACT was initiated by user
                    $SEV_KEY = $OS_EventId + "_userInitiated"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "5": ### userForced - a manual SWACT was forced by user; ignoring pre-conditions, warnings and safety checks
                    $SEV_KEY = $OS_EventId + "_userForced"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "6": ### activeUnitFailed - active unit failure caused an auto SWACT
                    $SEV_KEY = $OS_EventId + "_activeUnitFailed"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "7": ### activeUnitRemoved - active unit removal caused an auto SWACT
                    $SEV_KEY = $OS_EventId + "_activeUnitRemoved"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + @Type + " " + $specific-trap + " " + $3 + " " + @Agent + " " + @Manager
            
            $cRFStatusLastSwactReasonCode = $cRFStatusLastSwactReasonCode + " ( " + $3 + " )"
           
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cRFStatusUnitId,$sysUpTime,$cRFStatusLastSwactReasonCode)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cRFStatusUnitId", $cRFStatusUnitId, "sysUpTime", $sysUpTime, "cRFStatusLastSwactReasonCode", $cRFStatusLastSwactReasonCode)

        case "2": ### ciscoRFProgressionNotif

            ##########
            # $1 = cRFStatusUnitId 
            # $2 = cRFStatusUnitState 
            # $3 = cRFStatusPeerUnitId
            # $4 = cRFStatusPeerUnitState
            ##########

            $cRFStatusUnitId = $1
            $cRFStatusUnitState = lookup($2, ciscoRFState)
            $cRFStatusPeerUnitId = $3
            $cRFStatusPeerUnitState = lookup($4, ciscoRFState)
            
            $OS_EventId = "SNMPTRAP-cisco-CISCO-RF-MIB-ciscoRFProgressionNotif"

            @AlertGroup = "Redundant Unit Status"
            @AlertKey = "Unit ID: " + $1 + ", Peer Unit ID: " + $3
            @Summary = "Redundant Unit State Change, Unit: " + $cRFStatusUnitState + ", Peer Unit: " + $cRFStatusPeerUnitState
            
            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + @Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $2 + " " + $4
            
            $cRFStatusUnitState = $cRFStatusUnitState + " ( " + $2 + " )"
            $cRFStatusPeerUnitState = $cRFStatusPeerUnitState + " ( " + $4 + " )"
           
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cRFStatusUnitId,$cRFStatusUnitState,$cRFStatusPeerUnitId,$cRFStatusPeerUnitState)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cRFStatusUnitId", $cRFStatusUnitId, "cRFStatusUnitState", $cRFStatusUnitState, "cRFStatusPeerUnitId", $cRFStatusPeerUnitId,
                 "cRFStatusPeerUnitState", $cRFStatusPeerUnitState)
            
            update(@Summary)
            update(@Type)

        case "3": ### ciscoRFIssuStateNotif

            ##########
            # $1 = cRFStatusUnitId
            # $2 = cRFStatusUnitState
            # $3 = cRFStatusIssuState
            ##########

            $cRFStatusUnitId = $1
            $cRFStatusUnitState = lookup($2, ciscoRFState)
            $cRFStatusIssuState = lookup($3, RFIssuState)

            $OS_EventId = "SNMPTRAP-cisco-CISCO-RF-MIB-ciscoRFIssuStateNotif"

            @AlertGroup = "System State"

            @AlertKey = "Unit ID: " + $1

            @Summary = "System State Change, Unit: " + $cRFStatusUnitState + ", ISSU: " + $cRFStatusIssuState

            switch($3)
            {
                case "0": ### unset
                    $SEV_KEY = $OS_EventId + "_unset"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "1": ### init
                    $SEV_KEY = $OS_EventId + "_init"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "2": ### loadVersion
                    $SEV_KEY = $OS_EventId + "_loadVersion"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "3": ### runVersion
                    $SEV_KEY = $OS_EventId + "_runVersion"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "4": ### commitVersion
                    $SEV_KEY = $OS_EventId + "_commitVersion"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }


            $cRFStatusUnitState = $cRFStatusUnitState + " ( " + $2 + " ) "

            $cRFStatusIssuState = $cRFStatusIssuState + " ( " + $3 + " ) "

            @Summary = @Summary + " ( " + @AlertKey + " ) "

            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cRFStatusUnitId,$cRFStatusUnitState,$cRFStatusIssuState)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cRFStatusUnitId", $cRFStatusUnitId, "cRFStatusUnitState", $cRFStatusUnitState, "cRFStatusIssuState", $cRFStatusIssuState)

        case "4": ### ciscoRFIssuStateNotifRev1

            ##########
            # $1 = cRFStatusIssuStateRev1
            # $2 = cRFStatusIssuFromVersion
            # $3 = cRFStatusIssuToVersion
            # $4 = cRFStatusLastSwactReasonCode
            ##########

            $cRFStatusIssuStateRev1 = lookup($1, RFIssuStateRev1)
            $cRFStatusIssuFromVersion = $2
            $cRFStatusIssuToVersion = $3
            $cRFStatusLastSwactReasonCode = lookup($4, ciscoRFSwactReasonType)

            $OS_EventId = "SNMPTRAP-cisco-CISCO-RF-MIB-ciscoRFIssuStateNotifRev1"

            @AlertGroup = "System State"

            @AlertKey = ""

            @Summary = "System State Change, ISSU: " + $cRFStatusIssuStateRev1 

            switch($1)
            {
                case "0": ### init
                    $SEV_KEY = $OS_EventId + "_init"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "1": ### systemReset
                    $SEV_KEY = $OS_EventId + "_systemReset"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "3": ### loadVersion
                    $SEV_KEY = $OS_EventId + "_loadVersion"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "4": ### loadVersionSwitchover
                    $SEV_KEY = $OS_EventId + "_loadVersionSwitchover"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "6": ### runVersion
                    $SEV_KEY = $OS_EventId + "_runVersion"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "7": ### runVersionSwitchover
                    $SEV_KEY = $OS_EventId + "_runVersionSwitchover"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "9": ### commitVersion
                    $SEV_KEY = $OS_EventId + "_commitVersion"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            $cRFStatusIssuStateRev1 = $cRFStatusIssuStateRev1 + " ( " + $1 + " ) "

            $cRFStatusLastSwactReasonCode = $cRFStatusLastSwactReasonCode + " ( " + $4 + " ) "

            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cRFStatusIssuStateRev1,$cRFStatusIssuFromVersion,$cRFStatusIssuToVersion,$cRFStatusLastSwactReasonCode)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cRFStatusIssuStateRev1", $cRFStatusIssuStateRev1, "cRFStatusIssuFromVersion", $cRFStatusIssuFromVersion, "cRFStatusIssuToVersion", $cRFStatusIssuToVersion,
                 "cRFStatusLastSwactReasonCode", $cRFStatusLastSwactReasonCode)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-RF-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-RF-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-RF-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-RF-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-RF-MIB.include.snmptrap.rules >>>>>")

