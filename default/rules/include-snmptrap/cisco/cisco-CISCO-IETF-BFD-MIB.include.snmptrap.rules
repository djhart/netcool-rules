###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-IETF-BFD-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.10.137": ### Bidirectional Forwarding Detection(BFD) Protocol Management Information Base - Notifications from CISCO-IETF-BFD-MIB (200804240000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-IETF-BFD-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Cisco-BFD-MIB"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### ciscoBfdSessUp

            ##########
            # $1 = ciscoBfdSessDiag 
            # $2 = ciscoBfdSessDiag 
            ##########

            $ciscoBfdSessDiag = lookup($1, CiscoBfdDiag)
            $ciscoBfdSessDiag = lookup($2, CiscoBfdDiag)

            $OS_EventId = "SNMPTRAP-cisco-CISCO-IETF-BFD-MIB-ciscoBfdSessUp"

            @AlertGroup = "Session Status"
 
            $ciscoBfdSessIndex = extract($OID1, "\.([0-9]+)$")

            @AlertKey = "ciscoBfdSessEntry." + $ciscoBfdSessIndex

            @Summary = "Entries In ciscoBfdSessTable Are About to Enter The Up State" + " ( " + @AlertKey + " ) "

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            $ciscoBfdSessDiag = $ciscoBfdSessDiag + " ( " + $1 + " ) "

            $ciscoBfdSessDiag = $ciscoBfdSessDiag + " ( " + $2 + " ) "

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($ciscoBfdSessDiag,$ciscoBfdSessDiag,$ciscoBfdSessIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "ciscoBfdSessDiag", $ciscoBfdSessDiag, "ciscoBfdSessDiag", $ciscoBfdSessDiag, "ciscoBfdSessIndex", $ciscoBfdSessIndex)

        case "2": ### ciscoBfdSessDown

            ##########
            # $1 = ciscoBfdSessDiag 
            # $2 = ciscoBfdSessDiag 
            ##########

            $ciscoBfdSessDiag = lookup($1, CiscoBfdDiag)
            $ciscoBfdSessDiag = lookup($2, CiscoBfdDiag)

            $OS_EventId = "SNMPTRAP-cisco-CISCO-IETF-BFD-MIB-ciscoBfdSessDown"

            @AlertGroup = "Session Status"

            $ciscoBfdSessIndex = extract($OID1, "\.([0-9]+)$")

            @AlertKey = "ciscoBfdSessEntry." + $ciscoBfdSessIndex

            @Summary = "Entries In ciscoBfdSessTable Are About to Enter The Down or AdminDown State" + " ( " + @AlertKey + " ) "

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            $ciscoBfdSessDiag = $ciscoBfdSessDiag + " ( " + $1 + " ) "

            $ciscoBfdSessDiag = $ciscoBfdSessDiag + " ( " + $2 + " ) "

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($ciscoBfdSessDiag,$ciscoBfdSessDiag,$ciscoBfdSessIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "ciscoBfdSessDiag", $ciscoBfdSessDiag, "ciscoBfdSessDiag", $ciscoBfdSessDiag, "ciscoBfdSessIndex", $ciscoBfdSessIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-IETF-BFD-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-IETF-BFD-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-IETF-BFD-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-IETF-BFD-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-IETF-BFD-MIB.include.snmptrap.rules >>>>>")
