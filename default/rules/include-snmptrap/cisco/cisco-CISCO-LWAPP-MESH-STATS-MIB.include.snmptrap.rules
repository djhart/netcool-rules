###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-LWAPP-MESH-STATS-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.617": ### Cisco Light Weight Access Point Protocol Mesh Node Statistics - Notifications from CISCO-LWAPP-MESH-STATS-MIB (200703120000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-LWAPP-MESH-STATS-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Cisco-LWAPP-MESH-STATS-MIB"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### ciscoLwappMeshQueueOverflow

            ##########
            # $1 = cLApName 
            # $2 = clMeshNodePktQueuePeak 
            # $3 = clMeshNodePktQueuePktsDropped 
            ##########

            $cLApName = $1
            $clMeshNodePktQueuePeak = $2
            $clMeshNodePktQueuePktsDropped = $3

            $OS_EventId = "SNMPTRAP-cisco-CISCO-LWAPP-MESH-STATS-MIB-ciscoLwappMeshQueueOverflow"

            @AlertGroup = "Queue-Packets Status"

            $cLApSysMacAddress_Raw = extract($OID2, "3\.6\.1\.4\.1\.9\.9\.617\.1\.1\.2\.1\.3\.(.*)\.[0-9]+$")

            $MacAddrOid = $cLApSysMacAddress_Raw

            include "$NC_RULES_HOME/include-snmptrap/decodeOid2MacAddr.include.snmptrap.rules"

            $cLApSysMacAddress = $MacAddrHex

            $clMeshNodePktQueueIndex = extract($OID2, "\.([0-9]+)$")

            @AlertKey = "clMeshNodePktQueueStatsEntry." + $cLApSysMacAddress_Raw + "." + $clMeshNodePktQueueIndex

            @Summary = "Queue Overflows, Packet Dropped: " + $clMeshNodePktQueuePktsDropped

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            if(!match($1, ""))
            {
                @Summary = @Summary + " ( Access Point: " + $1 + " ) "
            }
            else
            {
                @Summary = @Summary + " ( " + @AlertKey + " ) "
            }

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cLApName,$clMeshNodePktQueuePeak,$clMeshNodePktQueuePktsDropped,$cLApSysMacAddress,$clMeshNodePktQueueIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cLApName", $cLApName, "clMeshNodePktQueuePeak", $clMeshNodePktQueuePeak, "clMeshNodePktQueuePktsDropped", $clMeshNodePktQueuePktsDropped,
                 "cLApSysMacAddress", $cLApSysMacAddress, "clMeshNodePktQueueIndex", $clMeshNodePktQueueIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-LWAPP-MESH-STATS-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-LWAPP-MESH-STATS-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-LWAPP-MESH-STATS-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-LWAPP-MESH-STATS-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-LWAPP-MESH-STATS-MIB.include.snmptrap.rules >>>>>")
