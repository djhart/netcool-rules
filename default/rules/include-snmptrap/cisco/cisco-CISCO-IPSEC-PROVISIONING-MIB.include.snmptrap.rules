###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-IPSEC-PROVISIONING-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.431": ### Cisco View and Provision IPsec-based VPNs - Notifications from CISCO-IPSEC-PROVISIONING-MIB (20050125)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-IPSEC-PROVISIONING-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Cisco-IPSec Provisioning"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### ciscoIPsecProvCryptomapAdded

            ##########
            # $1 = cipsStaticCryptomapType 
            # $2 = cipsStaticCryptomapSetSize 
            ##########

            $cipsStaticCryptomapType = lookup($1, CIPsecCryptomapType) 
            $cipsStaticCryptomapSetSize = $2
            
            $cipsStaticCryptomapSetName_Raw = extract($OID2, "\.3\.6\.1\.4\.1\.9\.9\.431\.1\.4\.1\.1\.1\.(.*)$")
            $OctetString = $cipsStaticCryptomapSetName_Raw
            include "$NC_RULES_HOME/include-snmptrap/decodeOctetString.include.snmptrap.rules"
            $cipsStaticCryptomapSetName = $String

            $OS_EventId = "SNMPTRAP-cisco-CISCO-IPSEC-PROVISIONING-MIB-ciscoIPsecProvCryptomapAdded"

            @AlertGroup = "IPsec Cryptomap Status"
            @AlertKey = "cipsStaticCryptomapSetEntry." + $cipsStaticCryptomapSetName_Raw
            @Summary = "IPsec Cryptomap Added to Cryptomap Set  ( Cryptomap Set: " + $cipsStaticCryptomapSetName + " )"
            
            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2 
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap 

            $cipsStaticCryptomapType = $cipsStaticCryptomapType + " ( " + $1 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cipsStaticCryptomapType,$cipsStaticCryptomapSetSize,$cipsStaticCryptomapSetName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cipsStaticCryptomapType", $cipsStaticCryptomapType, "cipsStaticCryptomapSetSize", $cipsStaticCryptomapSetSize, "cipsStaticCryptomapSetName", $cipsStaticCryptomapSetName)

        case "2": ### ciscoIPsecProvCryptomapDeleted

            ##########
            # $1 = cipsStaticCryptomapSetSize 
            ##########

            $cipsStaticCryptomapSetSize = $1
            
            $cipsStaticCryptomapSetName_Raw = extract($OID1, "\.3\.6\.1\.4\.1\.9\.9\.431\.1\.4\.1\.1\.1\.(.*)$")
            $OctetString = $cipsStaticCryptomapSetName_Raw
            include "$NC_RULES_HOME/include-snmptrap/decodeOctetString.include.snmptrap.rules"
            $cipsStaticCryptomapSetName = $String
            
            $OS_EventId = "SNMPTRAP-cisco-CISCO-IPSEC-PROVISIONING-MIB-ciscoIPsecProvCryptomapDeleted"

            @AlertGroup = "IPSec Cryptomap Status"
            @AlertKey = "cipsStaticCryptomapSetEntry." + $cipsStaticCryptomapSetName_Raw
            @Summary = "IPsec Cryptomap Deleted from Cryptomap Set ( Cryptomap Set: " + $cipsStaticCryptomapSetName + " )" 
            
            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap 

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cipsStaticCryptomapSetSize,$cipsStaticCryptomapSetName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cipsStaticCryptomapSetSize", $cipsStaticCryptomapSetSize, "cipsStaticCryptomapSetName", $cipsStaticCryptomapSetName)

        case "3": ### ciscoIPsecProvCryptomapAttached

            ##########
            # $1 = cipsStaticCryptomapSetSize 
            # $2 = cipsStaticCryptomapSetNumIsakmp 
            # $3 = cipsStaticCryptomapSetNumDynamic 
            ##########

            $cipsStaticCryptomapSetSize = $1
            $cipsStaticCryptomapSetNumIsakmp = $2
            $cipsStaticCryptomapSetNumDynamic = $3
            
            $cipsStaticCryptomapSetName_Raw = extract($OID1, "\.3\.6\.1\.4\.1\.9\.9\.431\.1\.4\.1\.1\.1\.(.*)$")
            $OctetString = $cipsStaticCryptomapSetName_Raw
            include "$NC_RULES_HOME/include-snmptrap/decodeOctetString.include.snmptrap.rules"
            $cipsStaticCryptomapSetName = $String
            
            $OS_EventId = "SNMPTRAP-cisco-CISCO-IPSEC-PROVISIONING-MIB-ciscoIPsecProvCryptomapAttached"

            @AlertGroup = "IPsec Cryptomap Attachment Status"
            @AlertKey = "cipsStaticCryptomapSetEntry." + $cipsStaticCryptomapSetName_Raw
            @Summary = "IPsec Cryptomap Set Attached to an Interface  ( Cryptomap Set: " + $cipsStaticCryptomapSetName + " )"
            
            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap 

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cipsStaticCryptomapSetSize,$cipsStaticCryptomapSetNumIsakmp,$cipsStaticCryptomapSetNumDynamic,$cipsStaticCryptomapSetName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cipsStaticCryptomapSetSize", $cipsStaticCryptomapSetSize, "cipsStaticCryptomapSetNumIsakmp", $cipsStaticCryptomapSetNumIsakmp, "cipsStaticCryptomapSetNumDynamic", $cipsStaticCryptomapSetNumDynamic,
                 "cipsStaticCryptomapSetName", $cipsStaticCryptomapSetName)

        case "4": ### ciscoIPsecProvCryptomapDetached

            ##########
            # $1 = cipsStaticCryptomapSetSize 
            ##########

            $cipsStaticCryptomapSetSize = $1
            
            $cipsStaticCryptomapSetName_Raw = extract($OID1, "\.3\.6\.1\.4\.1\.9\.9\.431\.1\.4\.1\.1\.1\.(.*)$")
            $OctetString = $cipsStaticCryptomapSetName_Raw
            include "$NC_RULES_HOME/include-snmptrap/decodeOctetString.include.snmptrap.rules"
            $cipsStaticCryptomapSetName = $String
            
            $OS_EventId = "SNMPTRAP-cisco-CISCO-IPSEC-PROVISIONING-MIB-ciscoIPsecProvCryptomapDetached"

            @AlertGroup = "IPsec Cryptomap Attachment Status"
            @AlertKey = "cipsStaticCryptomapSetEntry." + $cipsStaticCryptomapSetName_Raw
            @Summary = "IPsec Cryptomap Set Detached from an Interface  ( Cryptomap Set: " + $cipsStaticCryptomapSetName + " )"
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap 

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cipsStaticCryptomapSetSize,$cipsStaticCryptomapSetName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cipsStaticCryptomapSetSize", $cipsStaticCryptomapSetSize, "cipsStaticCryptomapSetName", $cipsStaticCryptomapSetName)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-IPSEC-PROVISIONING-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-IPSEC-PROVISIONING-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-IPSEC-PROVISIONING-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-IPSEC-PROVISIONING-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-IPSEC-PROVISIONING-MIB.include.snmptrap.rules >>>>>")
