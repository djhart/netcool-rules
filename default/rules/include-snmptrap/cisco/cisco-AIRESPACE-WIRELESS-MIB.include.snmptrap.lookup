###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  AIRESPACE-WIRELESS-MIB
#
###############################################################################

table BsnStationReasonCode =
{

    {"1","Unspecified"}, ### unspecified
    {"2","Previous Auth Not Valid"}, ### previousAuthNotValid
    {"3","Deauthentication Leaving"}, ### deauthenticationLeaving
    {"4","Disassociation Due To Inactivity"}, ### disassociationDueToInactivity
    {"5","Disassociation AP Busy"}, ### disassociationAPBusy
    {"6","Class2Frame From Non Auth Station"}, ### class2FrameFromNonAuthStation
    {"7","Class2Frame From Non Ass Station"}, ### class2FrameFromNonAssStation
    {"8","Disassociation Station Has Left"}, ### disassociationStaHasLeft
    {"9","Station Req Association Without Auth"}, ### staReqAssociationWithoutAuth
    {"40","Invalid Information Element"}, ### invalidInformationElement
    {"41","Group Cipher Invalid"}, ### groupCipherInvalid
    {"42","Unicast Cipher Invalid"}, ### unicastCipherInvalid
    {"43","AKMP Invalid"}, ### akmpInvalid
    {"44","Unsupported RSN Version"}, ### unsupportedRsnVersion
    {"45","Invalid RSN IE Capabilities"}, ### invalidRsnIeCapabilities
    {"46","Cipher Suite Rejected"}, ### cipherSuiteRejected     
    {"99","Missing Reason Code"} ### missingReasonCode     
}
default = "Unknown"

table BsnStationBlacklistingReasonCode =
{

    {"1","Failed 80211 Auth"}, ### failed80211Auth
    {"2","Failed Association"}, ### failedAssociation 
    {"3","IP Theft"}, ### ipTheft
    {"4","Failed 8021x Auth"}, ### failed8021xAuth
    {"5","Failed Web Auth"} ### failedWebAuth     
}
default = "Unknown"

table BsnAPIfUpDownCause =
{

    {"1","Radio Failure"}, ### radioFailure 
    {"2","Radio Low Power"}, ### radioLowPower
    {"3","Max Retransmission"}, ### maxRetransmission
    {"4","Echo Timeout"}, ### echoTimeout
    {"5","Config AP"}, ### configAP
    {"6","Config Radio"}, ### configRadio
    {"7","Config Network"}, ### configNetwork
    {"8","Admin Configured"}, ### adminConfigured
    {"9","Missed Rekey"}, ### missedRekey
    {"10","Detecting In Line Power"}, ### detectingInLinePower
    {"11","New Discovery"} ### newDiscovery   
}
default = "Unknown"

table BsnAPIfAdminStatus =
{

    {"1","Enable"}, ### enable
    {"2","Disable"} ### disable   
}
default = "Unknown"

table BsnAPReasonCodeTrapVariable =
{

    {"1","No Reason"}, ### noReason
    {"2","Signal"}, ### signal
    {"4","Noise"}, ### noise
    {"8","Interference"}, ### interference
    {"16","Load"}, ### load
    {"32","Radar"}, ### radar
    {"64","Device Aware"}, ### deviceAware 
    {"128","Major SIAQ Event"} ### majorSIAQEvent    
}
default = "Unknown"

table BsnAuthFailureUserType =
{

    {"1","Mgmt User"}, ### mgmtUser
    {"2","WLAN User"}, ### wlanUser
    {"3","MAC Filter"} ### macFilter    
}
default = "Unknown"

table BsnRogueAPOnWiredNetwork =
{

    {"0","No"}, ### no
    {"1","Yes"} ### yes    
}
default = "Unknown"

table BsnRogueAPRadioType =
{

    {"1","Dot11b"}, ### dot11b
    {"2","Dot11a"}, ### dot11a
    {"4","UWB"}, ### uwb
    {"5","Dot11g"}, ### dot11g
    {"6","Dot11n24"}, ### dot11n24
    {"7","Dot11n5"} ### dot11n5    
}
default = "Unknown"

table BsnRogueAPClassType =
{

    {"0","Pending"}, ### pending  
    {"1","Friendly"}, ### friendly
    {"2","Malicious"}, ### malicious
    {"3","Unclassified"} ### unclassified    
}
default = "Unknown"

table BsnRogueAdhocMode =
{

    {"0","No"}, ### no  
    {"1","Yes"} ### yes     
}
default = "Unknown"

table BsnDuplicateIpReportedByAP =
{

    {"0","No"}, ### no  
    {"1","Yes"} ### yes    
}
default = "Unknown"

table BsnDuplicateIpTrapClear =
{

    {"0","False"}, ### false   
    {"1","True"} ### true    
}
default = "Unknown"

table BsnAPIfType =
{

    {"1","Dot11b"}, ### dot11b    
    {"2","Dot11a"}, ### dot11a  
    {"4","UWB"} ### uwb      
}
default = "Unknown"

table BsnNetworkType =
{

    {"1","Dot11b"}, ### dot11b  
    {"2","Dot11a"} ### dot11a      
}
default = "Unknown"

table BsnNetworkState =
{

    {"0","Disable"}, ### disable
    {"1","Enable"} ### enable   
}
default = "Unknown"

table BsnSignatureType =
{

    {"0","Standard"}, ### standard  
    {"1","Custom"} ### custom     
}
default = "Unknown"

table BsnClearTrapVariable =
{

    {"0","False"}, ### false    
    {"1","True"} ### true    
}
default = "Unknown"

table BsnTxtSignatureMacInfo =
{

    {"0","BSN Signature MAC All"}, ### bsnSignatureMacAll   
    {"1","BSN Signature MAC Individual"}, ### bsnSignatureMacIndividual 
    {"2","BSN Signature MAC Both"} ### bsnSignatureMacBoth    
}
default = "Unknown"

table BsnApRegulatoryDomain =
{

    {"0","a"}, ### a   
    {"1","e"}, ### e
    {"6","i"}, ### i
    {"9","j"}, ### j 
    {"16","c"}, ### c 
    {"21","n"}, ### n  
    {"32","k"}, ### k 
    {"33","p"}, ### p 
    {"34","s"}, ### s 
    {"35","t"}, ### t  
    {"48","r"}, ### r  
    {"65535","Not Available"} ### notavailable      
}
default = "Unknown"

table BsnGlobalDot11CountryIndex =
{

    {"1","USA"}, ### usa   
    {"2","Canada"}, ### canada
    {"3","France"}, ### france
    {"4","Japan"}, ### japan
    {"5","Mexico"}, ### mexico
    {"6","Spain"}, ### spain
    {"7","USA Legacy"}, ### usalegacy
    {"8","Korea Republic"}, ### korearepublic
    {"9","Australia"}, ### australia
    {"10","Austria"}, ### austria

    {"11","Belgium"}, ### belgium
    {"12","Denmark"}, ### denmark
    {"13","Finland"}, ### finland
    {"14","Germany"}, ### germany
    {"15","Greece"}, ### greece
    {"16","Ireland"}, ### ireland
    {"17","Italy"}, ### italy
    {"18","Luxembourg"}, ### luxembourg
    {"19","Netherlands"}, ### netherlands
    {"20","Portugal"}, ### portugal

    {"21","Sweden"}, ### sweden
    {"22","United Kingdom"}, ### unitedkingdom
    {"23","None"}, ### none
    {"24","India"}, ### india
    {"25","Hong Kong"}, ### hongkong
    {"26","Switzerland"}, ### switzerland
    {"27","Iceland"}, ### iceland
    {"28","Norway"}, ### norway
    {"29","Singapore"}, ### singapore
    {"30","Thailand"}, ### thailand   

    {"31","Taiwan"}, ### taiwan
    {"33","Cyprus"}, ### cyprus
    {"34","Czechrepublic"}, ### czechrepublic
    {"35","Estonia"}, ### estonia
    {"36","Hungary"}, ### hungary
    {"37","Lithuania"}, ### lithuania
    {"38","Latvia"}, ### latvia
    {"39","Malaysia"}, ### malaysia
    {"40","New Zealand"}, ### newzealand

    {"41","Poland"}, ### poland
    {"42","Slovenia"}, ### slovenia
    {"43","Slovakrepublic"}, ### slovakrepublic
    {"44","Southafrica"}, ### southafrica
    {"45","USA Chan165"}, ### usachan165
    {"46","Israel"}, ### israel
    {"47","Israel Outdoor"}, ### israelOutdoor
    {"48","Argentina"}, ### argentina
    {"49","Brazil"}, ### brazil  

    {"51","Saudi Arabia"}, ### saudiArabia
    {"52","Turkey"}, ### turkey
    {"53","Indonesia"}, ### indonesia
    {"54","China"}, ### china
    {"55","Korea Extended"}, ### koreaExtended
    {"56","Japan2"}, ### japan2
    {"57","Gibraltar"}, ### gibraltar
    {"58","Liechtenstein"}, ### liechtenstein
    {"59","Malta"}, ### malta
    {"60","Monaco"}, ### monaco  

    {"61","Romania"}, ### romania
    {"62","Russian Federation"}, ### russianfederation
    {"63","Chile"}, ### chile
    {"64","Colombia"}, ### colombia
    {"65","Panama"}, ### panama
    {"66","Peru"}, ### peru
    {"67","Venezuela"}, ### venezuela
    {"68","Philippines"} ### philippines  
}
default = "Unknown"

table BsnTrustedApPreambleUsed =
{

    {"0","None"}, ### none    
    {"1","Short"}, ### short  
    {"2","Long"} ### long   
}
default = "Unknown"

table BsnTrustedApPreambleRequired =
{

    {"0","None"}, ### none   
    {"1","Short"}, ### short
    {"2","Long"} ### long    
}
default = "Unknown"

table BsnTrustedApEncryptionUsed =
{

    {"0","None"}, ### none    
    {"1","Open"}, ### open
    {"2","WEP"}, ### wep
    {"3","WPA"} ### wpa   
}
default = "Unknown"

table BsnTrustedApEncryptionRequired =
{

    {"0","None"}, ### none    
    {"1","Open"}, ### open
    {"2","WEP"}, ### wep
    {"3","WPA"} ### wpa   
}
default = "Unknown"

table BsnTrustedApRadioPolicyUsed =
{

    {"0","None"}, ### none    
    {"1","Dot11b"}, ### dot11b
    {"2","Dot11a"}, ### dot11a
    {"3","Dot11bg"} ### dot11bg   
}
default = "Unknown"

table BsnTrustedApRadioPolicyRequired =
{

    {"0","None"}, ### none    
    {"1","Dot11b"}, ### dot11b
    {"2","Dot11a"}, ### dot11a
    {"3","Dot11bg"} ### dot11bg   
}
default = "Unknown"

table BsnApFunctionalityDisableReasonCode =
{

    {"1","License Key Expired"}, ### licenseKeyExpired   
    {"2","License Key Deleted"}, ### licenseKeyDeleted
    {"3","License Key Feature Mismatch"} ### licenseKeyFeatureMismatch     
}
default = "Unknown"

table BsnLicenseKeyFeatureSetTrapVariable =
{

    {"1","WPS"}, ### wps    
    {"2","All"} ### all    
}
default = "Unknown"

table BsnAPIfPhyChannelNumber =
{

    {"1","ch1"}, ### ch1    
    {"2","ch2"}, ### ch2
    {"3","ch3"}, ### ch3
    {"4","ch4"}, ### ch4
    {"5","ch5"}, ### ch5
    {"6","ch6"}, ### ch6
    {"7","ch7"}, ### ch7
    {"8","ch8"}, ### ch8
    {"9","ch9"}, ### ch9
    {"10","ch10"}, ### ch10

    {"11","ch11"}, ### ch11
    {"12","ch12"}, ### ch12
    {"13","ch13"}, ### ch13
    {"14","ch14"}, ### ch14
    {"20","ch20"}, ### ch20
    {"21","ch21"}, ### ch21
    {"22","ch22"}, ### ch22
    {"23","ch23"}, ### ch23
    {"24","ch24"}, ### ch24
    {"25","ch25"}, ### ch25

    {"26","ch26"}, ### ch26
    {"34","ch34"}, ### ch34
    {"36","ch36"}, ### ch36
    {"38","ch38"}, ### ch38
    {"40","ch40"}, ### ch40
    {"42","ch42"}, ### ch42
    {"44","ch44"}, ### ch44
    {"46","ch46"}, ### ch46
    {"48","ch48"}, ### ch48
    {"52","ch52"}, ### ch52   
    
    {"56","ch56"}, ### ch56
    {"60","ch60"}, ### ch60
    {"64","ch64"}, ### ch64
    {"100","ch100"}, ### ch100
    {"104","ch104"}, ### ch104
    {"108","ch108"}, ### ch108
    {"112","ch112"}, ### ch112
    {"116","ch116"}, ### ch116
    {"120","ch120"}, ### ch120
    {"124","ch124"}, ### ch124

    {"128","ch128"}, ### ch128
    {"132","ch132"}, ### ch132
    {"136","ch136"}, ### ch136
    {"140","ch140"}, ### ch140
    {"149","ch149"}, ### ch149
    {"153","ch153"}, ### ch153
    {"157","ch157"}, ### ch157
    {"161","ch161"}, ### ch161
    {"165","ch165"}, ### ch165
    {"169","ch169"} ### ch169    
}
default = "Unknown"

table BsnAPAuthCertificateType =
{

    {"1","MIC"}, ### mic   
    {"2","SSC"}, ### ssc
    {"3","Loc MIC"}, ### locMic
    {"4","Loc SSC"}, ### locSsc
    {"5","None"} ### none    
}
default = "Unknown"

table BsnAPAuthorizationFailureCause =
{

    {"1","Key Mismatch"}, ### keymismatch    
    {"2","Entry Does Not Exist"}, ### entrydoesnotexist
    {"3","Invalid Certifcate"}, ### invalidCertifcate
    {"4","Entry Is MIC"}, ### entryIsMIC
    {"5","AAA Entry Does Not Exist"} ### aaaEntryDoesNotExist   
}
default = "Unknown"

table BsnAPInvalidRadioType =
{

    {"0","Unsupported Radio"} ### unsupportedRadio   
}
default = "Unknown"

table BsnAPAdminStatus =
{

    {"1","Enable"}, ### enable
    {"2","Disable"} ### disable
}
default = "Unknown"

