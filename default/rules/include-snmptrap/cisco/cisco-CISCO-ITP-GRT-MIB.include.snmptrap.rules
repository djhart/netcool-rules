###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
# Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
# - CISCO-ITP-GRT-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.334": ### CISCO ITP Gateway Signalling Point Routing Table - Notifications from CISCO-ITP-GRT-MIB (200805010000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-ITP-GRT-MIB.include.snmptrap.rules >>>>>")

    @Agent = "CISCO-ITP-GRT"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### ciscoGrtDestStateChange

            ##########
            # $1 = cgspEventSequenceNumber 
            # $2 = cgspCLLICode 
            # $3 = cgrtDestNotifSupFlag 
            # $4 = cgrtDestNotifChanges 
            ##########

            $cgspEventSequenceNumber = $1
            $cgspCLLICode = $2
            $cgrtDestNotifSupFlag = lookup($3, ciscocgrtDestNotifSupFlag)
            $cgrtDestNotifChanges = $4

            $OS_EventId = "SNMPTRAP-cisco-CISCO-ITP-GRT-MIB-ciscoGrtDestStateChange"

            @AlertGroup = "Cisco Grt Destination State Status"
            @AlertKey = ""
            @Summary = "Cisco Grt Destination State Status " + $cgrtDestNotifChanges + " " + $cgrtDestNotifSupFlag + " CLLICode : " + $cgspCLLICode

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            $cgrtDestNotifSupFlag = $cgrtDestNotifSupFlag + " ( " + $3 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cgspEventSequenceNumber,$cgspCLLICode,$cgrtDestNotifSupFlag,$cgrtDestNotifChanges)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cgspEventSequenceNumber", $cgspEventSequenceNumber, "cgspCLLICode", $cgspCLLICode, "cgrtDestNotifSupFlag", $cgrtDestNotifSupFlag,
                 "cgrtDestNotifChanges", $cgrtDestNotifChanges)

        case "2": ### ciscoGrtMgmtStateChange

            ##########
            # $1 = cgspEventSequenceNumber 
            # $2 = cgspCLLICode 
            # $3 = cgrtMgmtNotifSupFlag 
            # $4 = cgrtMgmtNotifChanges 
            ##########

            $cgspEventSequenceNumber = $1
            $cgspCLLICode = $2
            $cgrtMgmtNotifSupFlag = lookup($3, ciscocgrtMgmtNotifSupFlag)
            $cgrtMgmtNotifChanges = $4

            $OS_EventId = "SNMPTRAP-cisco-CISCO-ITP-GRT-MIB-ciscoGrtMgmtStateChange"

            @AlertGroup = "Cisco Grt Mgmt State Change Status"
            @AlertKey = ""
            @Summary = "Cisco Grt Mgmt State Change Status : " + $cgrtMgmtNotifChanges + " " + $cgrtMgmtNotifSupFlag + " CLLICode : " + $cgspCLLICode

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            $cgrtMgmtNotifChanges = $cgrtMgmtNotifChanges + " ( " + $3 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cgspEventSequenceNumber,$cgspCLLICode,$cgrtMgmtNotifSupFlag,$cgrtMgmtNotifChanges)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cgspEventSequenceNumber", $cgspEventSequenceNumber, "cgspCLLICode", $cgspCLLICode, "cgrtMgmtNotifSupFlag", $cgrtMgmtNotifSupFlag,
                 "cgrtMgmtNotifChanges", $cgrtMgmtNotifChanges)

        case "3": ### ciscoGrtRouteTableLoad

            ##########
            # $1 = cgspEventSequenceNumber 
            # $2 = cgspCLLICode 
            # $3 = cgrtInstLoadStatus 
            # $4 = cgrtInstTableName 
            # $5 = cgrtInstLastURL 
            ##########

            $cgspEventSequenceNumber = $1
            $cgspCLLICode = $2
            $cgrtInstLoadStatus = lookup($3, ciscoCItpTcTableLoadStatus)
            $cgrtInstTableName = $4
            $cgrtInstLastURL = $5

            $cgspInstNetwork = extract($OID3, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-cisco-CISCO-ITP-GRT-MIB-ciscoGrtRouteTableLoad"

            @AlertGroup = "Cisco Grt Route Table Load"
            @AlertKey = "cgrtInstEntry." + $cgspInstNetwork

            switch($3)
            {
                        case "1": ### loadNotRequested
                            $SEV_KEY = $OS_EventId + "_loadNotRequested"
                            @Summary = "Cisco Grt Route Table Load, status : " + $cgrtInstLoadStatus + " " + $cgspCLLICode + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "2": ### loadInProgress
                            $SEV_KEY = $OS_EventId + "_loadInProgress"
                            @Summary = "Cisco Grt Route Table Load, status : " + $cgrtInstLoadStatus + " " + $cgspCLLICode + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 1
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0

                        case "3": ### loadComplete
                            $SEV_KEY = $OS_EventId + "_loadComplete"
                            @Summary = "Cisco Grt Route Table Load, status : " + $cgrtInstLoadStatus + " " + $cgspCLLICode + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 1
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0

                        case "4": ### loadCompleteWithErrors
                            $SEV_KEY = $OS_EventId + "_loadCompleteWithErrors"
                            @Summary = "Cisco Grt Route Table Load, status : " + $cgrtInstLoadStatus + " " + $cgspCLLICode + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "5": ### loadFailed
                            $SEV_KEY = $OS_EventId + "_loadFailed"
                            @Summary = "Cisco Grt Route Table Load, status : " + $cgrtInstLoadStatus + " " + $cgspCLLICode + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        default:
                            $SEV_KEY = $OS_EventId + "_unknown"
                            @Summary = "Cisco Grt Route Table Load unknown status " + $cgspCLLICode + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0
            }

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            $cgrtInstLoadStatus = $cgrtInstLoadStatus + " ( " + $3 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cgspEventSequenceNumber,$cgspCLLICode,$cgrtInstLoadStatus,$cgrtInstTableName,$cgrtInstLastURL,$cgspInstNetwork)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cgspEventSequenceNumber", $cgspEventSequenceNumber, "cgspCLLICode", $cgspCLLICode, "cgrtInstLoadStatus", $cgrtInstLoadStatus,
                 "cgrtInstTableName", $cgrtInstTableName, "cgrtInstLastURL", $cgrtInstLastURL, "cgspInstNetwork", $cgspInstNetwork)

        case "4": ### ciscoGrtDestStateChangeRev1

            ##########
            # $1 = cgspEventSequenceNumber 
            # $2 = cgspCLLICode 
            # $3 = cgrtDestNotifSuppressed 
            # $4 = cgrtDestDisplay 
            # $5 = cgrtDestStatus 
            # $6 = cgrtDestCongestion 
            ##########

            $cgspEventSequenceNumber = $1
            $cgspCLLICode = $2
            $cgrtDestNotifSuppressed = $3
            $cgrtDestDisplay = $4
            $cgrtDestStatus = lookup($5, ciscocgrtDestStatus)
            $cgrtDestCongestion = $6

            $cgspInstNetwork = extract($OID4, "\.([0-9]+)\.[0-9]+\.[0-9]+$")
            $cgrtRouteDpc = extract($OID4, "\.([0-9]+)\.[0-9]+$")
            $cgrtRouteMask = extract($OID4, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-cisco-CISCO-ITP-GRT-MIB-ciscoGrtDestStateChangeRev1"

            @AlertGroup = "Cisco Grt Dest State Change Rev1"
            @AlertKey = "cgrtDestEntry." + $cgspInstNetwork + "." + $cgrtRouteDpc + "." + $cgrtRouteMask

            switch($5)
            {
                        case "1": ### unknown
                           $SEV_KEY = $OS_EventId + "_unknown"
                            @Summary = "Cisco Grt Dest unknown State Change Rev1 : " + $cgspCLLICode + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "2": ### accessible
                            $SEV_KEY = $OS_EventId + "_accessible"
                            @Summary = "Cisco Grt Dest State Change Rev1 : " + $cgrtDestStatus + " " + $cgspCLLICode + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 1
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0

                        case "3": ### inaccessible
                            $SEV_KEY = $OS_EventId + "_inaccessible"
                            @Summary = "Cisco Grt Dest State Change Rev1 : " + $cgrtDestStatus + " " + $cgspCLLICode + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "4": ### restricted
                            $SEV_KEY = $OS_EventId + "_restricted"
                            @Summary = "Cisco Grt Dest State Change Rev1 : " + $cgrtDestStatus + " " + $cgspCLLICode + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        default:
                            $SEV_KEY = $OS_EventId + "_unknown"
                            @Summary = "Cisco Grt Dest unknown State Change Rev1 : " + $cgspCLLICode + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0
            }


            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            $cgrtDestStatus = $cgrtDestStatus + " ( " + $5 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cgspEventSequenceNumber,$cgspCLLICode,$cgrtDestNotifSuppressed,$cgrtDestDisplay,$cgrtDestStatus,$cgrtDestCongestion,$cgspInstNetwork,$cgrtRouteDpc,$cgrtRouteMask)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cgspEventSequenceNumber", $cgspEventSequenceNumber, "cgspCLLICode", $cgspCLLICode, "cgrtDestNotifSuppressed", $cgrtDestNotifSuppressed,
                 "cgrtDestDisplay", $cgrtDestDisplay, "cgrtDestStatus", $cgrtDestStatus, "cgrtDestCongestion", $cgrtDestCongestion,
                 "cgspInstNetwork", $cgspInstNetwork, "cgrtRouteDpc", $cgrtRouteDpc, "cgrtRouteMask", $cgrtRouteMask)

        case "5": ### ciscoGrtMgmtStateChangeRev1

            ##########
            # $1 = cgspEventSequenceNumber 
            # $2 = cgspCLLICode 
            # $3 = cgrtRouteStatus 
            # $4 = cgrtRouteMgmtStatus 
            # $5 = cgrtRouteDynamic 
            # $6 = cgrtRouteAdminStatus 
            # $7 = cgrtRouteNotifSuppressed 
            # $8 = cgrtRouteDisplay 
            ##########

            $cgspEventSequenceNumber = $1
            $cgspCLLICode = $2
            $cgrtRouteStatus = lookup($3, ciscocgrtDestStatus)
            $cgrtRouteMgmtStatus = lookup($4, ciscocgrtRouteMgmtStatus) + " ( " + $4 + " )"
            $cgrtRouteDynamic = lookup($5, ciscocgrtRouteDynamic) + " ( " + $5 + " )"
            $cgrtRouteAdminStatus = lookup($5, ciscocgrtRouteAdminStatus) + " ( " + $6 + " )"
            $cgrtRouteNotifSuppressed = $7
            $cgrtRouteDisplay = $8

            $cgspInstNetwork = extract($OID4, "\.([0-9]+)\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$")
            $cgrtRouteDpc = extract($OID4, "\.([0-9]+)\.[0-9]+\.[0-9]+\.[0-9]+$")
            $cgrtRouteMask = extract($OID4, "\.([0-9]+)\.[0-9]+\.[0-9]+$")
            $cgrtRouteDestLsCost= extract($OID4, "\.([0-9]+)\.[0-9]+$")
            $cgrtRouteDestLinkset= extract($OID4, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-cisco-CISCO-ITP-GRT-MIB-ciscoGrtMgmtStateChangeRev1"
            @AlertGroup = "Cisco Grt Mgmt State Change Rev1"

            @AlertKey = "cgrtRouteEntry." + extract($OID4, "\.([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)$")



            switch($3)
            {
                        case "1": ### unknown
                            $SEV_KEY = $OS_EventId + "_unknown"
                            @Summary = "Cisco Grt Mgmt unknown State Change Rev1 : " + $cgspCLLICode + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "2": ### available
                            $SEV_KEY = $OS_EventId + "_available"
                            @Summary = "Cisco Grt Mgmt State Change Rev1 : " + $cgrtRouteStatus + " " + $cgspCLLICode + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 1
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0

                        case "3": ### restricted
                            $SEV_KEY = $OS_EventId + "_restricted"
                            @Summary = "Cisco Grt Mgmt State Change Rev1 : " + $cgrtRouteStatus + " " + $cgspCLLICode + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "4": ### unavailable
                            $SEV_KEY = $OS_EventId + "_unavailable"
                            @Summary = "Cisco Grt Dest State Change Rev1 : " + $cgrtDestStatus + " " + $cgspCLLICode + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "5": ### deleted
                            $SEV_KEY = $OS_EventId + "_deleted"
                            @Summary = "Cisco Grt Dest State Change Rev1 : " + $cgrtDestStatus + " " + $cgspCLLICode + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 1
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0

                        default:
                            $SEV_KEY = $OS_EventId + "_unknown"
                            @Summary = "Cisco Grt Mgmt unknown State Change Rev1 : " + $cgspCLLICode + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0
            }


            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            $cgrtRouteStatus = $cgrtRouteStatus + " ( " + $3 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cgspEventSequenceNumber,$cgspCLLICode,$cgrtRouteStatus,$cgrtRouteMgmtStatus,$cgrtRouteDynamic,$cgrtRouteAdminStatus,$cgrtRouteNotifSuppressed,$cgrtRouteDisplay,$cgspInstNetwork ,$cgrtRouteDpc,$cgrtRouteMask,$cgrtRouteDestLsCost,$cgrtRouteDestLinkset)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cgspEventSequenceNumber", $cgspEventSequenceNumber, "cgspCLLICode", $cgspCLLICode, "cgrtRouteStatus", $cgrtRouteStatus,
                 "cgrtRouteMgmtStatus", $cgrtRouteMgmtStatus, "cgrtRouteDynamic", $cgrtRouteDynamic, "cgrtRouteAdminStatus", $cgrtRouteAdminStatus,
                 "cgrtRouteNotifSuppressed", $cgrtRouteNotifSuppressed, "cgrtRouteDisplay", $cgrtRouteDisplay, "cgspInstNetwork", $cgspInstNetwork,
                 "cgrtRouteDpc", $cgrtRouteDpc, "cgrtRouteMask", $cgrtRouteMask, "cgrtRouteDestLsCost", $cgrtRouteDestLsCost,
                 "cgrtRouteDestLinkset", $cgrtRouteDestLinkset)

        case "6": ### ciscoGrtNoRouteMSUDiscards

            ##########
            # $1 = cgspEventSequenceNumber 
            # $2 = cgspCLLICode 
            # $3 = cgspInstDisplayName 
            # $4 = cgrtNoRouteMSUsInterval 
            # $5 = cgrtIntervalNoRouteMSUs 
            ##########

            $cgspEventSequenceNumber = $1
            $cgspCLLICode = $2
            $cgspInstDisplayName = $3
            $cgrtNoRouteMSUsInterval = $4
            $cgrtIntervalNoRouteMSUs = $5

            $cgspInstNetwork = extract($OID3, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-cisco-CISCO-ITP-GRT-MIB-ciscoGrtNoRouteMSUDiscards"

            @AlertGroup = "Cisco Grt No Route MSU Discards"
            @AlertKey = "cgspInstDisplayName." + $cgspInstNetwork
            @Summary = "Cisco Grt No Route MSU Discards : " + $cgspCLLICode 

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cgspEventSequenceNumber,$cgspCLLICode,$cgspInstDisplayName,$cgrtNoRouteMSUsInterval,$cgrtIntervalNoRouteMSUs,$cgspInstNetwork)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cgspEventSequenceNumber", $cgspEventSequenceNumber, "cgspCLLICode", $cgspCLLICode, "cgspInstDisplayName", $cgspInstDisplayName,
                 "cgrtNoRouteMSUsInterval", $cgrtNoRouteMSUsInterval, "cgrtIntervalNoRouteMSUs", $cgrtIntervalNoRouteMSUs, "cgspInstNetwork", $cgspInstNetwork)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-ITP-GRT-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-ITP-GRT-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-ITP-GRT-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-ITP-GRT-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-ITP-GRT-MIB.include.snmptrap.rules >>>>>")
