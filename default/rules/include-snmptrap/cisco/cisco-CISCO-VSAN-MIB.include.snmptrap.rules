###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 2.2 - Trap(s) added
#       - vsanPortMembershipChange
#
# 2.1 - Simplified handling of "Severity via lookup" logic.
#
#     - Added basic debug logging.
#
# 2.0 - Rewritten for improved event presentation.  Includes enhancements which
#       comply with the Netcool Rules File Standards
#       (MUSE-STD-RF-02, July 2002)
#
#     - Modified to support following features introduced in NCiL 2.0:
#         - "Advanced" and "User" include files
#         - "Severity" lookup tables
#
# 1.0 - Initial Release.
#       - Based on rules extracted from cisco.include.snmptrap.rules 3.1.
#
# Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
# - CISCO-VSAN-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.282.1.3": ### Cisco Virtual Storage Area Network - Notifications from CISCO-VSAN-MIB (200602060000Z)
 
    log(DEBUG, "<<<<< Entering... cisco-CISCO-VSAN-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Cisco-VSAN"
    @Class = "40057"
    
    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### vsanStatusChange

            ##########
            # $1 = notifyVsanIndex
            # $2 = vsanAdminState 
            # $3 = vsanOperState
            ##########

            $notifyVsanIndex = $1
            $vsanAdminState = lookup($2, ciscoVsanAdminState) + " ( " + $2 + " )"
            $vsanOperState = lookup($3, ciscoVsanOperationalState) + " ( " + $3 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($notifyVsanIndex,$vsanAdminState,$vsanOperState)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "notifyVsanIndex", $notifyVsanIndex, "vsanAdminState", $vsanAdminState, "vsanOperState", $vsanOperState)

            $OS_EventId = "SNMPTRAP-cisco-CISCO-VSAN-MIB-vsanStatusChange"

            @AlertGroup = "VSAN Status"
            @AlertKey = "vsanEntry." + $1
            switch($3)
            {
                case "1": ### up
                    switch($2)
                    {
                        case "1": ### active
                            @Summary = "VSAN Up, Administratively Active"
                            
                            $SEV_KEY = $OS_EventId + "_up_active"
                            $DEFAULT_Severity = 1
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0

                        case "2": ### suspended
                            @Summary = "VSAN Up, Administratively Suspended"
                            
                            $SEV_KEY = $OS_EventId + "_up_suspended"
                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0

                        default:
                            @Summary = "VSAN Up, Administrative Status Unknown"
                            
                            $SEV_KEY = $OS_EventId + "_up_unknown"
                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0

                    }
                case "2": ### down
                    switch($2)
                    {
                        case "1": ### active
                            @Summary = "VSAN Down, Administratively Active"
                            
                            $SEV_KEY = $OS_EventId + "_down_active"
                            $DEFAULT_Severity = 4
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "2": ### suspended
                            @Summary = "VSAN Down, Administratively Suspended"
                            
                            $SEV_KEY = $OS_EventId + "_down_suspended"
                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        default:
                            @Summary = "VSAN Down, Administrative Status Unknown"
                            
                            $SEV_KEY = $OS_EventId + "_down_unknown"
                            $DEFAULT_Severity = 4
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                    }
                default:
                    switch($2)
                    {
                        case "1": ### active
                            @Summary = "VSAN Status Unknown, Administratively Active"
                            
                            $SEV_KEY = $OS_EventId + "_unknown_active"
                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "2": ### suspended
                            @Summary = "VSAN Status Unknown, Administratively Suspended"
                            
                            $SEV_KEY = $OS_EventId + "_unknown_suspended"
                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        default:
                            @Summary = "VSAN Status Unknown, Administrative Status Unknown"
                            
                            $SEV_KEY = $OS_EventId + "_unknown_unknown"
                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                    }
            }
            if(int($1)<10)
            {
                @Summary = @Summary + "  ( VSAN: VSAN000" + $1 + " )"
            }
            else if(int($1)>9 && int($1)<100)
            {
                @Summary = @Summary + "  ( VSAN: VSAN00" + $1 + " )"
            }
            else if(int($1)>99 && int($1)<1000)
            {
                @Summary = @Summary + "  ( VSAN: VSAN0" + $1 + " )"
            }
            else if(int($1)>999 && int($1)<10000)
            {
                @Summary = @Summary + "  ( VSAN: VSAN" + $1 + " )"
            }
            else
            {
                @Summary = @Summary + "  ( " + @AlertKey + " )"
            }
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $2 + " " + $3


        case "2": ### vsanPortMembershipChange

            ##########
            # $1 = vsanFcFeElementName 
            # $2 = ifIndex 
            # $3 = notifyVsanIndex 
            ##########

            $vsanFcFeElementName = $1
            $ifIndex = $2
            $notifyVsanIndex = $3

            $OS_EventId = "SNMPTRAP-cisco-CISCO-VSAN-MIB-vsanPortMembershipChange"

            @AlertGroup = "Port-vsan membership Status"
            @AlertKey = "ifEntry." + $ifIndex
            @Summary = "Port-vsan membership Status "

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($vsanFcFeElementName,$ifIndex,$notifyVsanIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "vsanFcFeElementName", $vsanFcFeElementName, "ifIndex", $ifIndex, "notifyVsanIndex", $notifyVsanIndex)


        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-VSAN-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-VSAN-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-VSAN-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-VSAN-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-VSAN-MIB.include.snmptrap.rules >>>>>")
