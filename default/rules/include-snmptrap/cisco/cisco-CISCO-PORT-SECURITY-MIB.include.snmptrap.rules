###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 2.0 - Trap(s) added
#  
#          - cpsTrunkSecureMacAddrViolation
#          - cpsIfVlanSecureMacAddrViolation
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-PORT-SECURITY-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.315.0": ### Cisco Port Security - Notifications from CISCO-PORT-SECURITY-MIB (200905080000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-PORT-SECURITY-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Cisco-Port Security"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### cpsSecureMacAddrViolation

            ##########
            # $1 = ifIndex 
            # $2 = ifName 
            # $3 = cpsIfSecureLastMacAddress 
            ##########

            $ifIndex = $1
            $ifName = $2
            $cpsIfSecureLastMacAddress = $3

            $OS_EventId = "SNMPTRAP-cisco-CISCO-PORT-SECURITY-MIB-cpsSecureMacAddrViolation"

            @AlertGroup = "Port Security Status"
            @AlertKey = "cpsIfConfigEntry." + $1 + ", Violating MAC: " + $3
            @Summary = "Port Security MAC Address Violation  ( Interface: " + $2 + ", Violating MAC: " + $3 + " )"
 

            $DEFAULT_Severity = 2 
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($ifIndex,$ifName,$cpsIfSecureLastMacAddress)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "ifIndex", $ifIndex, "ifName", $ifName, "cpsIfSecureLastMacAddress", $cpsIfSecureLastMacAddress)

        case "2": ### cpsTrunkSecureMacAddrViolation

            ##########
            # $1 = ifName
            # $2 = vtpVlanName
            # $3 = cpsIfSecureLastMacAddress
            ##########

            $ifName = $1
            $vtpVlanName = $2
            $cpsIfSecureLastMacAddress = $3

            $OS_EventId = "SNMPTRAP-cisco-CISCO-PORT-SECURITY-MIB-cpsTrunkSecureMacAddrViolation"

            @AlertGroup = "Port Security Status"

            $managementDomainIndex = extract($OID2, "\.([0-9]+)\.[0-9]+$")

            $vtpVlanIndex = extract($OID2, "\.([0-9]+)$")

            @AlertKey = "vtpVlanEntry." + $managementDomainIndex + "." + $vtpVlanIndex + ", Violating MAC: " + $3

            @Summary = "Port Security MAC Address Violation  ( VLAN: " + $2 + ", Violating MAC: " + $3 + " ) "

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($ifName,$vtpVlanName,$cpsIfSecureLastMacAddress,$managementDomainIndex,$vtpVlanIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "ifName", $ifName, "vtpVlanName", $vtpVlanName, "cpsIfSecureLastMacAddress", $cpsIfSecureLastMacAddress,
                 "managementDomainIndex", $managementDomainIndex, "vtpVlanIndex", $vtpVlanIndex)

        case "3": ### cpsIfVlanSecureMacAddrViolation

            ##########
            # $1 = ifName
            # $2 = cpsIfSecureLastMacAddrVlanId
            # $3 = cpsIfSecureLastMacAddress
            ##########

            $ifName = $1
            $cpsIfSecureLastMacAddrVlanId = $2
            $cpsIfSecureLastMacAddress = $3

            $OS_EventId = "SNMPTRAP-cisco-CISCO-PORT-SECURITY-MIB-cpsIfVlanSecureMacAddrViolation"

            @AlertGroup = "Port Security Status"

            $ifIndex = extract($OID2, "\.([0-9]+)$")

            @AlertKey = "cpsIfConfigEntry." + $ifIndex + ", Violating MAC: " + $3

            @Summary = "Port Security MAC Address Violation  ( VLAN ID: " + $2 + ", Violating MAC: " + $3 + " ) "

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($ifName,$cpsIfSecureLastMacAddrVlanId,$cpsIfSecureLastMacAddress,$ifIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "ifName", $ifName, "cpsIfSecureLastMacAddrVlanId", $cpsIfSecureLastMacAddrVlanId, "cpsIfSecureLastMacAddress", $cpsIfSecureLastMacAddress,
                 "ifIndex", $ifIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-PORT-SECURITY-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-PORT-SECURITY-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-PORT-SECURITY-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-PORT-SECURITY-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-PORT-SECURITY-MIB.include.snmptrap.rules >>>>>")
