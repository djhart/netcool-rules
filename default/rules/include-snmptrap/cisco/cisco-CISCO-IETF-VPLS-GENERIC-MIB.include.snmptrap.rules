###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 1.0 - Initial Release.
#
# Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
# - CISCO-IETF-VPLS-GENERIC-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.10.138": ###  - Notifications from CISCO-IETF-VPLS-GENERIC-MIB (200710221200Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-IETF-VPLS-GENERIC-MIB.include.snmptrap.rules >>>>>")

    @Agent = "CISCO-IETF-VPLS-GENERIC-MIB"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### cvplsStatusChanged

            ##########
            # $1 = cvplsConfigVpnId 
            # $2 = cvplsConfigAdminStatus 
            # $3 = cvplsStatusOperStatus 
            ##########

            $cvplsConfigVpnId = $1
            $cvplsConfigAdminStatus = lookup($2, cvplsConfigAdminStatus) + " ( " + $2 + " )"
            $cvplsStatusOperStatus = lookup($3, cvplsStatusOperStatus) + " ( " + $3 + " )"

            $cvplsConfigIndex = extract($OID1, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-cisco-CISCO-IETF-VPLS-GENERIC-MIB-cvplsStatusChanged"

            @AlertGroup = "VPLS Service Status"
            @AlertKey = "cvplsConfigEntry." + $cvplsConfigIndex

            switch($2)
            {
                case "1": ### up
                    $SEV_KEY = $OS_EventId + "_up"
                    switch($3)
                    {
                        case "0": ### other
                            $SEV_KEY = $SEV_KEY + "_other"
                            @Summary = "VPLS Service Admin Status Up and Operational Status is Other"

                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0
                       
                         case "1": ### up
                            $SEV_KEY = $SEV_KEY + "_up"
                            @Summary = "VPLS Service is Operational"

                            $DEFAULT_Severity = 1
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0

                         case "2": ### down
                            $SEV_KEY = $SEV_KEY + "_down"
                            @Summary = "VPLS Service Admin Status Up and Operational Status is Down"

                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0
  
                         default:
                            $SEV_KEY = $SEV_KEY + "_unknown"
                            @Summary = "VPLS Service Admin Status Up and Operational Status is unknown"

                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                   }

                case "2": ### down
                    $SEV_KEY = $OS_EventId + "_down"
                    @Summary = "VPLS Service Status Down"

                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "3": ### testing
                    $SEV_KEY = $OS_EventId + "_testing"
                    @Summary = "VPLS Service Status is in Testing mode"

                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = "VPLS Service Status Unknown"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            @Summary = @Summary + " ( " + @AlertKey + " ) "
            update(@Summary)
            update(@Severity) 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cvplsConfigVpnId,$cvplsConfigAdminStatus,$cvplsStatusOperStatus,$cvplsConfigIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cvplsConfigVpnId", $cvplsConfigVpnId, "cvplsConfigAdminStatus", $cvplsConfigAdminStatus, "cvplsStatusOperStatus", $cvplsStatusOperStatus,
                 "cvplsConfigIndex", $cvplsConfigIndex)

        case "2": ### cvplsFwdFullAlarmRaised

            ##########
            # $1 = cvplsConfigVpnId 
            # $2 = cvplsConfigFwdFullHighWatermark 
            # $3 = cvplsConfigFwdFullLowWatermark 
            ##########

            $cvplsConfigVpnId = $1
            $cvplsConfigFwdFullHighWatermark = $2
            $cvplsConfigFwdFullLowWatermark = $3

            $cvplsConfigIndex = extract($OID1, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-cisco-CISCO-IETF-VPLS-GENERIC-MIB-cvplsFwdFullAlarmRaised"

            @AlertGroup = "Forwarding Database Utilization for VPLS"
            @AlertKey = "cvplsConfigEntry." + $cvplsConfigIndex
            @Summary = "Forwarding Database Utilization is above the value of cvplsConfigFwdFullHighWatermark : " + $cvplsConfigFwdFullHighWatermark

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cvplsConfigVpnId,$cvplsConfigFwdFullHighWatermark,$cvplsConfigFwdFullLowWatermark,$cvplsConfigIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cvplsConfigVpnId", $cvplsConfigVpnId, "cvplsConfigFwdFullHighWatermark", $cvplsConfigFwdFullHighWatermark, "cvplsConfigFwdFullLowWatermark", $cvplsConfigFwdFullLowWatermark,
                 "cvplsConfigIndex", $cvplsConfigIndex)

        case "3": ### cvplsFwdFullAlarmCleared

            ##########
            # $1 = cvplsConfigVpnId 
            # $2 = cvplsConfigFwdFullHighWatermark 
            # $3 = cvplsConfigFwdFullLowWatermark 
            ##########

            $cvplsConfigVpnId = $1
            $cvplsConfigFwdFullHighWatermark = $2
            $cvplsConfigFwdFullLowWatermark = $3

            $cvplsConfigIndex = extract($OID1, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-cisco-CISCO-IETF-VPLS-GENERIC-MIB-cvplsFwdFullAlarmCleared"

            @AlertGroup = "Forwarding Database Utilization for VPLS"
            @AlertKey = "cvplsConfigEntry." + $cvplsConfigIndex
            @Summary = "Forwarding Database Utilization is below the value of cvplsConfigFwdFullLowWatermark : " + $cvplsConfigFwdFullLowWatermark

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cvplsConfigVpnId,$cvplsConfigFwdFullHighWatermark,$cvplsConfigFwdFullLowWatermark,$cvplsConfigIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cvplsConfigVpnId", $cvplsConfigVpnId, "cvplsConfigFwdFullHighWatermark", $cvplsConfigFwdFullHighWatermark, "cvplsConfigFwdFullLowWatermark", $cvplsConfigFwdFullLowWatermark,
                 "cvplsConfigIndex", $cvplsConfigIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-IETF-VPLS-GENERIC-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-IETF-VPLS-GENERIC-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-IETF-VPLS-GENERIC-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-IETF-VPLS-GENERIC-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-IETF-VPLS-GENERIC-MIB.include.snmptrap.rules >>>>>")
