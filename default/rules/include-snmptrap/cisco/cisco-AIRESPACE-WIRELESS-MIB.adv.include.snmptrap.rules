###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  AIRESPACE-WIRELESS-MIB
#
###############################################################################

log(DEBUG, "<<<<< Entering... cisco-AIRESPACE-WIRELESS-MIB.adv.include.snmptrap.rules >>>>>")

switch($specific-trap)
{
    case "1": ### bsnDot11StationDisassociate

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnDot11StationDisassociate"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Station MAC Addr: " + $bsnStationMacAddress 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "2": ### bsnDot11StationDeauthenticate

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnDot11StationDeauthenticate"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Station MAC Addr: " + $bsnStationMacAddress 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "3": ### bsnDot11StationAuthenticateFail

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnDot11StationAuthenticateFail"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Station MAC Addr: " + $bsnStationMacAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "4": ### bsnDot11StationAssociateFail

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnDot11StationAssociateFail"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Station MAC Addr: " + $bsnStationMacAddress 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "52": ### bsnDot11StationBlacklisted

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnDot11StationBlacklisted"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Station MAC Addr: " + $bsnStationMacAddress + ", AP Slot ID: " + $bsnStationAPIfSlotId
        $OS_LocalRootObj = "Station MAC Addr: " + $bsnStationMacAddress
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "53": ### bsnDot11StationAssociate

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnDot11StationAssociate"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Station MAC Addr: " + $bsnStationMacAddress + ", AP Slot ID: " + $bsnStationAPIfSlotId
        $OS_LocalRootObj = "Station MAC Addr: " + $bsnStationMacAddress
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "5": ### bsnAPUp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAPUp"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "6": ### bsnAPDown

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAPDown"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "7": ### bsnAPAssociated

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAPAssociated"
        $OS_OsiLayer = 0

        if(regmatch($3, "^ap:"))
        {
        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress 
        }
        else
        {
        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress_raw 
        }
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "8": ### bsnAPDisassociated

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAPDisassociated"
        $OS_OsiLayer = 0

        if(regmatch($2, "^ap:"))
        {
        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress 
        }
        else
        {
        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress_raw 
        }
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "9": ### bsnAPIfUp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAPIfUp"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "10": ### bsnAPIfDown

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAPIfDown"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "11": ### bsnAPLoadProfileFailed

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAPLoadProfileFailed"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress + ", bsnAPIfEntry." + $bsnAPDot3MacAddress + "." + $bsnAPIfSlotId
        $OS_LocalRootObj = "bsnAPEntry." + $bsnAPDot3MacAddress
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "12": ### bsnAPNoiseProfileFailed

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAPNoiseProfileFailed"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress + ", bsnAPIfEntry." + $bsnAPDot3MacAddress + "." + $bsnAPIfSlotId
        $OS_LocalRootObj = "bsnAPEntry." + $bsnAPDot3MacAddress
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "13": ### bsnAPInterferenceProfileFailed

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAPInterferenceProfileFailed"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress + ", bsnAPIfEntry." + $bsnAPDot3MacAddress + "." + $bsnAPIfSlotId
        $OS_LocalRootObj = "bsnAPEntry." + $bsnAPDot3MacAddress
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "14": ### bsnAPCoverageProfileFailed

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAPCoverageProfileFailed"
        $OS_OsiLayer = 0

        if(regmatch($10, "^ap:"))
        {
        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress 
        }
        else
        {
        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress_raw 
        }
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "37": ### bsnAPLoadProfileUpdatedToPass

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAPLoadProfileUpdatedToPass"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress + ", bsnAPIfEntry." + $bsnAPDot3MacAddress + "." + $bsnAPIfSlotId
        $OS_LocalRootObj = "bsnAPEntry." + $bsnAPDot3MacAddress
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "38": ### bsnAPNoiseProfileUpdatedToPass

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAPNoiseProfileUpdatedToPass"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress + ", bsnAPIfEntry." + $bsnAPDot3MacAddress + "." + $bsnAPIfSlotId
        $OS_LocalRootObj = "bsnAPEntry." + $bsnAPDot3MacAddress
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "39": ### bsnAPInterferenceProfileUpdatedToPass

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAPInterferenceProfileUpdatedToPass"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress + ", bsnAPIfEntry." + $bsnAPDot3MacAddress + "." + $bsnAPIfSlotId 
        $OS_LocalRootObj = "bsnAPEntry." + $bsnAPDot3MacAddress
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "40": ### bsnAPCoverageProfileUpdatedToPass

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAPCoverageProfileUpdatedToPass"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "15": ### bsnAPCurrentTxPowerChanged

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAPCurrentTxPowerChanged"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress + ", bsnAPIfEntry." + $bsnAPDot3MacAddress + "." + $bsnAPIfSlotId
        $OS_LocalRootObj = "bsnAPEntry." + $bsnAPDot3MacAddress
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "16": ### bsnAPCurrentChannelChanged

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAPCurrentChannelChanged"
        $OS_OsiLayer = 0

        if(regmatch($12, "^ap:"))
        {
        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress 
        }
        else
        {
        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress_raw 
        }
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "21": ### bsnRrmDot11aGroupingDone

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnRrmDot11aGroupingDone"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Group Leader MAC Addr: " + $bsnRrmDot11aGroupLeaderMacAddr 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "22": ### bsnRrmDot11bGroupingDone

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnRrmDot11bGroupingDone"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Group Leader MAC Addr: " + $bsnRrmDot11bGroupLeaderMacAddr
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "23": ### bsnConfigSaved

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnConfigSaved"
        $OS_OsiLayer = 0

    case "24": ### bsnDot11EssCreated

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnDot11EssCreated"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnDot11EssEntry." + $bsnDot11EssIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "25": ### bsnDot11EssDeleted

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnDot11EssDeleted"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnDot11EssEntry." + $bsnDot11EssIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "26": ### bsnRADIUSServerNotResponding

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnRADIUSServerNotResponding"
        $OS_OsiLayer = 0

    case "27": ### bsnAuthenticationFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAuthenticationFailure"
        $OS_OsiLayer = 0

    case "28": ### bsnIpsecEspAuthFailureTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnIpsecEspAuthFailureTrap"
        $OS_OsiLayer = 0

        $OS_RemotePriObj = "Remote IP Addr: " + $bsnRemoteIPv4Address
        $OS_RemoteRootObj = $OS_RemotePriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "29": ### bsnIpsecEspReplayFailureTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnIpsecEspReplayFailureTrap"
        $OS_OsiLayer = 0

        $OS_RemotePriObj = "Remote IP Addr: " + $bsnRemoteIPv4Address
        $OS_RemoteRootObj = $OS_RemotePriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "31": ### bsnIpsecEspInvalidSpiTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnIpsecEspInvalidSpiTrap"
        $OS_OsiLayer = 0

        $OS_RemotePriObj = "Remote IP Addr: " + $bsnRemoteIPv4Address
        $OS_RemoteRootObj = $OS_RemotePriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "33": ### bsnIpsecIkeNegFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnIpsecIkeNegFailure"
        $OS_OsiLayer = 0

        $OS_RemotePriObj = "Remote IP Addr: " + $bsnRemoteIPv4Address
        $OS_RemoteRootObj = $OS_RemotePriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "34": ### bsnIpsecSuiteNegFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnIpsecSuiteNegFailure"
        $OS_OsiLayer = 0

        $OS_RemotePriObj = "Remote IP Addr: " + $bsnRemoteIPv4Address
        $OS_RemoteRootObj = $OS_RemotePriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "35": ### bsnIpsecInvalidCookieTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnIpsecInvalidCookieTrap"
        $OS_OsiLayer = 0

        $OS_RemotePriObj = "Remote IP Addr: " + $bsnRemoteIPv4Address
        $OS_RemoteRootObj = $OS_RemotePriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "36": ### bsnRogueAPDetected

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnRogueAPDetected"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnRogueAPEntry." + $bsnRogueAPDot11MacAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "41": ### bsnRogueAPRemoved

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnRogueAPRemoved"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnRogueAPEntry." + $bsnRogueAPDot11MacAddress 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "59": ### bsnRogueAPDetectedOnWiredNetwork

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnRogueAPDetectedOnWiredNetwork"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnRogueAPEntry." + $bsnRogueAPDot11MacAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "60": ### bsnApHasNoRadioCards

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnApHasNoRadioCards"
        $OS_OsiLayer = 0

        if(regmatch($2, "^ap:"))
        {
        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress 
        }
        else
        {
        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress_raw 
        }
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "61": ### bsnDuplicateIpAddressReported

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnDuplicateIpAddressReported"
        $OS_OsiLayer = 0

        if(regmatch($6, "^ap:"))
        {
        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress 
        }
        else
        {
        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress_raw 
        }
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "62": ### bsnAPContainedAsARogue

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAPContainedAsARogue"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "69": ### bsnNetworkStateChanged

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnNetworkStateChanged"
        $OS_OsiLayer = 0

    case "70": ### bsnSignatureAttackDetected

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnSignatureAttackDetected"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "71": ### bsnAPRadioCardTxFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAPRadioCardTxFailure"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "72": ### bsnAPRadioCardTxFailureClear

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAPRadioCardTxFailureClear"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "73": ### bsnAPRadioCardRxFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAPRadioCardRxFailure"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "74": ### bsnAPRadioCardRxFailureClear

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAPRadioCardRxFailureClear"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "75": ### bsnAPImpersonationDetected

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAPImpersonationDetected"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "79": ### bsnAPRegulatoryDomainMismatch

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAPRegulatoryDomainMismatch"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "76": ### bsnTrustedApHasInvalidPreamble

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnTrustedApHasInvalidPreamble"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnRogueAPEntry." + $bsnRogueAPDot11MacAddress 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "42": ### bsnRadiosExceedLicenseCount

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnRadiosExceedLicenseCount"
        $OS_OsiLayer = 0

    case "43": ### bsnSensedTemperatureTooHigh

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnSensedTemperatureTooHigh"
        $OS_OsiLayer = 0

    case "44": ### bsnSensedTemperatureTooLow

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnSensedTemperatureTooLow"
        $OS_OsiLayer = 0

    case "45": ### bsnTemperatureSensorFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnTemperatureSensorFailure"
        $OS_OsiLayer = 0

    case "46": ### bsnTemperatureSensorClear

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnTemperatureSensorClear"
        $OS_OsiLayer = 0

    case "47": ### bsnPOEControllerFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnPOEControllerFailure"
        $OS_OsiLayer = 0

    case "48": ### bsnMaxRogueCountExceeded

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnMaxRogueCountExceeded"
        $OS_OsiLayer = 0

    case "49": ### bsnMaxRogueCountClear

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnMaxRogueCountClear"
        $OS_OsiLayer = 0

    case "50": ### bsnApMaxRogueCountExceeded

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnApMaxRogueCountExceeded"
        $OS_OsiLayer = 0

        if(regmatch($3, "^ap:"))
        {
        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress 
        }
        else
        {
        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress_raw 
        } 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "51": ### bsnApMaxRogueCountClear

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnApMaxRogueCountClear"
        $OS_OsiLayer = 0

        if(regmatch($3, "^ap:"))
        {
        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress 
        }
        else
        {
        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress_raw 
        } 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "55": ### bsnApBigNavDosAttack

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnApBigNavDosAttack"
        $OS_OsiLayer = 0

        if(regmatch($4, "^ap:"))
        {
        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress 
        }
        else
        {
        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress_raw 
        } 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "56": ### bsnTooManyUnsuccessLoginAttempts

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnTooManyUnsuccessLoginAttempts"
        $OS_OsiLayer = 0

    case "57": ### bsnWepKeyDecryptError

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnWepKeyDecryptError"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Station: " + $bsnStationMacAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "58": ### bsnWpaMicErrorCounterActivated

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnWpaMicErrorCounterActivated"
        $OS_OsiLayer = 0

        if(regmatch($5, "^ap:"))
        {
        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress 
        }
        else
        {
        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress_raw 
        }
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "65": ### bsnAdhocRogueAutoContained

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAdhocRogueAutoContained"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnRogueAPEntry." + $bsnRogueAPDot11MacAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "66": ### bsnRogueApAutoContained

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnRogueApAutoContained"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnRogueAPEntry." + $bsnRogueAPDot11MacAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "67": ### bsnTrustedApHasInvalidEncryption

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnTrustedApHasInvalidEncryption"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnRogueAPEntry." + $bsnRogueAPDot11MacAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "68": ### bsnTrustedApHasInvalidRadioPolicy

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnTrustedApHasInvalidRadioPolicy"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnRogueAPEntry." + $bsnRogueAPDot11MacAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "63": ### bsnTrustedApHasInvalidSsid

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnTrustedApHasInvalidSsid"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnRogueAPEntry." + $bsnRogueAPDot11MacAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "64": ### bsnTrustedApIsMissing

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnTrustedApIsMissing"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnRogueAPEntry." + $bsnRogueAPDot11MacAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "77": ### bsnAPIPAddressFallback

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAPIPAddressFallback"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "78": ### bsnAPFunctionalityDisabled

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAPFunctionalityDisabled"
        $OS_OsiLayer = 0

    case "80": ### bsnRxMulticastQueueFull

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnRxMulticastQueueFull"
        $OS_OsiLayer = 0

    case "81": ### bsnRadarChannelDetected

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnRadarChannelDetected"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "82": ### bsnRadarChannelCleared

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnRadarChannelCleared"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "83": ### bsnAPAuthorizationFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "bsnAPAuthorizationFailure"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "84": ### radioCoreDumpTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "radioCoreDumpTrap"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "85": ### invalidRadioTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "invalidRadioTrap"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "86": ### countryChangeTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "countryChangeTrap"
        $OS_OsiLayer = 0

    case "87": ### unsupportedAPTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "unsupportedAPTrap"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "bsnAPEntry." + $bsnAPDot3MacAddress 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "88": ### heartbeatLossTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "heartbeatLossTrap"
        $OS_OsiLayer = 0

    case "89": ### locationNotifyTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "locationNotifyTrap"
        $OS_OsiLayer = 0

    default:
}

log(DEBUG, "<<<<< Leaving... cisco-AIRESPACE-WIRELESS-MIB.adv.include.snmptrap.rules >>>>>")
