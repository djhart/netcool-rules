###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 2.0 - Trap(s) added
#
#          - ciscoRFIssuStateNotif
#          - ciscoRFIssuStateNotifRev1
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-RF-MIB
#
#          -  Based on lookup table definitions extracted from
#             cisco.include.snmptrap.lookup 2.8.
#
###############################################################################

table ciscoRFSwactReasonType =
{
    {"1","Unsupported"}, ### unsupported - the 'reason code' is an unsupported feature
    {"2","No SWACT Occurred"}, ### none - no SWACT has occurred
    {"3","Unknown"}, ### notKnown - reason is unknown
    {"4","User Initiated"}, ### userInitiated - a safe, manual SWACT was initiated by user
    {"5","User Forced"}, ### userForced - a manual SWACT was forced by user; ignoring pre-conditions, warnings and safety checks
    {"6","Unit Failure"}, ### activeUnitFailed - active unit failure caused an auto SWACT
    {"7","Unit Removed"} ### activeUnitRemoved - active unit removal caused an auto SWACT
}
default = "Unknown"

table ciscoRFState =
{
    {"1","Unknown"}, ### notKnown - state is unknown
    {"2","Disabled"}, ### disabled - RF is not operational on this unit
    {"3","Initialization"}, ### initialization - establish necessary system services
    {"4","Negotiation"}, ### negotiation - peer unit discovery and negotiation
    {"5","Standby Cold"}, ### standbyCold - client notification on standby unit
    {"6","Standby Cold Config"}, ### standbyColdConfig - standby cfg is updated from active cfg
    {"7","Standby Cold File System"}, ### standbyColdFileSys - standby file system (FS) is updated from the active FS
    {"8","Standby Cold Bulk"}, ### standbyColdBulk - clients sync data from active to standby
    {"9","Standby Hot"}, ### standbyHot - incremental client data sync continues. This unit is ready to take over activity.
    {"10","Active Fast"}, ### activeFast - call maintenance efforts during a SWACT
    {"11","Active Drain"}, ### activeDrain - client clean-up phase
    {"12","Active Pre-Config"}, ### activePreconfig - unit is active but has not read its configuration
    {"13","Active Post-Config"}, ### activePostconfig - unit is active and is post-processing its configuration
    {"14","Active"}, ### active - unit is active and processing calls
    {"15","Active Extra Load"}, ### activeExtraload - unit is active and processing calls for all feature boards in the system
    {"16","Active Handback"} ### activeHandback - unit is active, processing calls and is in the process of handing some resources to the other unit in the system
}
default = "Unknown"

table RFIssuState =
{
    ##########
    # This object indicates the ISSU state represents the current system state
    ##########

    {"0","Unset"}, ### unset
    {"1","Init"}, ### init
    {"2","Load Version"}, ### loadVersion
    {"3","Run Version"}, ### runVersion
    {"4","Commit Version"}, ### commitVersion
}
default = "Unknown"

table RFIssuStateRev1 =
{
    ##########
    # This object indicates ISSU state represents the current system state
    ##########

    {"0","Initial"}, ### init
    {"1","System Reset"}, ### systemReset
    {"3","Load Version"}, ### loadVersion
    {"4","Load Version Switchover"}, ### loadVersionSwitchover
    {"6","Run Version"}, ### runVersion
    {"7","Run Version Switchover"}, ### runVersionSwitchover
    {"9","Commit Version"}, ### commitVersion
}
default = "Unknown"

