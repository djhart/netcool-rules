###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-LWAPP-ROGUE-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.610": ### Cisco Light Weight Access Point Protocol with Rogue Access Point Information - Notifications from CISCO-LWAPP-ROGUE-MIB (200702060000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-LWAPP-ROGUE-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Cisco-LWAPP-ROGUE-MIB"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### cLRogueAdhocRogueDetected

            ##########
            # $1 = cLApName 
            ##########

            $cLApName = $1

            $OS_EventId = "SNMPTRAP-cisco-CISCO-LWAPP-ROGUE-MIB-cLRogueAdhocRogueDetected"

            @AlertGroup = "Rogue Status"

            $cLApSysMacAddress_Raw = extract($OID1, "3\.6\.1\.4\.1\.9\.9\.513\.1\.1\.1\.1\.5\.(.*)$")

            $MacAddrOid = $cLApSysMacAddress_Raw

            include "$NC_RULES_HOME/include-snmptrap/decodeOid2MacAddr.include.snmptrap.rules"

            $cLApSysMacAddress = $MacAddrHex

            @AlertKey = "cLApEntry." + $cLApSysMacAddress_Raw

            @Summary = "Rogue: " + $cLApName + " is Detected" 

            @Summary = @Summary + " ( " + @AlertKey + " ) "

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cLApName,$cLApSysMacAddress)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cLApName", $cLApName, "cLApSysMacAddress", $cLApSysMacAddress)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-LWAPP-ROGUE-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-LWAPP-ROGUE-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-LWAPP-ROGUE-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-LWAPP-ROGUE-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-LWAPP-ROGUE-MIB.include.snmptrap.rules >>>>>")
