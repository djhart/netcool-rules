###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 2.0 - Trap(s) added
#
#          - cpscEventRev1
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-PORT-STORM-CONTROL-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.362.0.1": ### Cisco Port Storm Control - Notifications from CISCO-PORT-STORM-CONTROL-MIB (200710190000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-PORT-STORM-CONTROL-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Cisco-Port Storm Control"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### cpscEvent

            ##########
            # $1 = cpscStatus 
            ##########

            $cpscStatus = lookup($1, CPortStormControlStatusType) + " ( " + $1 + " )"
            $ifIndex = extract($OID1, "\.([0-9]+)\.[0-9]+$")
            $cpscTrafficType_Raw = extract($OID1, "\.([0-9]+)$")
            $cpscTrafficType = lookup($cpscTrafficType_Raw, CPortStormControlTrafficType)            

            $OS_EventId = "SNMPTRAP-cisco-CISCO-PORT-STORM-CONTROL-MIB-cpscEvent"

            @AlertGroup = "Port Storm Status"
            @AlertKey = "cpscStatusEntry." + $ifIndex + "." + $cpscTrafficType_Raw
            switch($1)
            {
                case "1":### inactive
                    $SEV_KEY = $OS_EventId + "_inactive"
                    @Summary = "Port Storm Control Not Enabled for  " + $cpscTrafficType

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "2":### forwarding 
                    $SEV_KEY = $OS_EventId + "_forwarding"
                    @Summary = "Port Storm Cleared for " + $cpscTrafficType 

                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                case "3":### trafficTypeFiltered 
                    $SEV_KEY = $OS_EventId + "_trafficTypeFiltered"
                    @Summary = "Port Storm for " + $cpscTrafficType + " Filtered"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "4":### allTrafficFiltered 
                    $SEV_KEY = $OS_EventId + "_allTrafficFiltered"
                    @Summary = "Port Storm, All Traffic Filtered"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "5":### shutdown 
                    $SEV_KEY = $OS_EventId + "_shutdown"
                    @Summary = "Port Storm for " + $cpscTrafficType + ", Interface Shutdown"

                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                default: 
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = "Port Storm Status Unknown"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }
            
            @Summary = @Summary + "  ( " + @AlertKey + " )"

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $1

            $cpscTrafficType = $cpscTrafficType + " ( " + $cpscTrafficType_Raw + " )"            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cpscStatus,$ifIndex,$cpscTrafficType)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cpscStatus", $cpscStatus, "ifIndex", $ifIndex, "cpscTrafficType", $cpscTrafficType)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-PORT-STORM-CONTROL-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-PORT-STORM-CONTROL-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-PORT-STORM-CONTROL-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-PORT-STORM-CONTROL-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-PORT-STORM-CONTROL-MIB.include.snmptrap.rules >>>>>")

case ".1.3.6.1.4.1.9.9.362": ### Managing Cisco Port Storm Control - Notifications from CISCO-PORT-STORM-CONTROL-MIB (200710190000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-PORT-STORM-CONTROL-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Cisco-Port Storm Control"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "2": ### cpscEventRev1

            ##########
            # $1 = cpscStatus
            ##########

            $cpscStatus = lookup($1, CPortStormControlStatusType)

            $OS_EventId = "SNMPTRAP-cisco-CISCO-PORT-STORM-CONTROL-MIB-cpscEventRev1"

            @AlertGroup = "Port Storm Status"

            $ifIndex = extract($OID1, "\.([0-9]+)\.[0-9]+$")

            $cpscTrafficType_Raw = extract($OID1, "\.([0-9]+)$")

            $cpscTrafficType = lookup($cpscTrafficType_Raw, CPortStormControlTrafficType)

            @AlertKey = "cpscStatusEntry." + $ifIndex + "." + $cpscTrafficType_Raw

            switch($1)
            {
                case "1":### inactive
                    $SEV_KEY = $OS_EventId + "_inactive"
                    @Summary = "Port Storm Control Not Enabled for  " + $cpscTrafficType

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "2":### forwarding
                    $SEV_KEY = $OS_EventId + "_forwarding"
                    @Summary = "Port Storm Cleared for " + $cpscTrafficType

                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0

                case "3":### trafficTypeFiltered
                    $SEV_KEY = $OS_EventId + "_trafficTypeFiltered"
                    @Summary = "Port Storm for " + $cpscTrafficType + " Filtered"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "4":### allTrafficFiltered
                    $SEV_KEY = $OS_EventId + "_allTrafficFiltered"
                    @Summary = "Port Storm, All Traffic Filtered"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "5":### shutdown
                    $SEV_KEY = $OS_EventId + "_shutdown"
                    @Summary = "Port Storm for " + $cpscTrafficType + ", Interface Shutdown"

                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = "Port Storm Status Unknown"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            @Summary = @Summary + "  ( " + @AlertKey + " )"

            $cpscStatus = $cpscStatus + " ( " + $1 + " ) "

            $cpscTrafficType = $cpscTrafficType + " ( " + $cpscTrafficType_Raw + " ) "

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cpscStatus,$cpscTrafficType,$ifIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cpscStatus", $cpscStatus, "cpscTrafficType", $cpscTrafficType, "ifIndex", $ifIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-PORT-STORM-CONTROL-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-PORT-STORM-CONTROL-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-PORT-STORM-CONTROL-MIB_Control.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-PORT-STORM-CONTROL-MIB_Control.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-PORT-STORM-CONTROL-MIB.include.snmptrap.rules >>>>>")


