###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################

###############################################################################
#
# 1.0 - Initial Release.
#
# Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
# -  CISCO-APPLIANCE-REDUNDANCY-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.458.2": ### Report status of High Availability (HA) functionality in Cisco network management appliance devices.  - Notifications from CISCO-APPLIANCE-REDUNDANCY-MIB (200412230000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-APPLIANCE-REDUNDANCY-MIB.include.snmptrap.rules >>>>>")

    @Agent = "CISCO-APPLIANCE-REDUNDANCY"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### carSwitchOverNotification

            ##########
            # $1 = carSWHistEventTime 
            # $2 = carSWHistEventReason 
            # $3 = carSWHistActiveNodeAddressType 
            # $4 = carSWHistActiveNodeAddress 
            # $5 = carSWHistStandbyNodeAddressType 
            # $6 = carSWHistStandbyNodeAddress 
            ##########

            $carSWHistEventTime = $1
            $carSWHistEventReason = lookup($2, ciscoCarSwitchOverReason)
            $carSWHistActiveNodeAddressType = lookup($3, InetAddressType) + " ( " + $3 + " )"
            $carSWHistActiveNodeAddress = $4
            $carSWHistStandbyNodeAddressType = lookup($5, InetAddressType) + " ( " + $5 + " )"
            $carSWHistStandbyNodeAddress = $6

            $carSWHistTableIndex = extract($OID1, "\.([0-9]+)$")
            $OS_EventId = "SNMPTRAP-cisco-CISCO-APPLIANCE-REDUNDANCY-MIB-carSwitchOverNotification"

            @AlertGroup = "Switchover event status"
            @AlertKey = "carSwitchOverHistEntry." + $carSWHistTableIndex
            @Summary = "Switch over event status " + $carSWHistEventReason + " ( " + @AlertKey + " )"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            $carSWHistEventReason = $carSWHistEventReason + " ( " + $2 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($carSWHistEventTime,$carSWHistEventReason,$carSWHistActiveNodeAddressType,$carSWHistActiveNodeAddress,$carSWHistStandbyNodeAddressType,$carSWHistStandbyNodeAddress,$carSWHistTableIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "carSWHistEventTime", $carSWHistEventTime, "carSWHistEventReason", $carSWHistEventReason, "carSWHistActiveNodeAddressType", $carSWHistActiveNodeAddressType,
                 "carSWHistActiveNodeAddress", $carSWHistActiveNodeAddress, "carSWHistStandbyNodeAddressType", $carSWHistStandbyNodeAddressType, "carSWHistStandbyNodeAddress", $carSWHistStandbyNodeAddress,
                 "carSWHistTableIndex", $carSWHistTableIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-APPLIANCE-REDUNDANCY-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-APPLIANCE-REDUNDANCY-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-APPLIANCE-REDUNDANCY-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-APPLIANCE-REDUNDANCY-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-APPLIANCE-REDUNDANCY-MIB.include.snmptrap.rules >>>>>")
