###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 1.0 - Initial Release.
#
# Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
# - CISCO-VOICE-DNIS-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.219.2": ###  -  NOTIFICATIONS from CISCO-VOICE-DNIS-MIB (200205010000Z) 

    log(DEBUG, "<<<<< Entering... cisco-CISCO-VOICE-DNIS-MIB.include.snmptrap.rules >>>>>")

    @Agent = "CISCO-VOICE-DNIS-MIB"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### cvDnisMappingUrlInaccessible

            ##########
            # $1 = cvDnisMappingUrl 
            # $2 = cvDnisMappingUrlAccessError 
            ##########

            $cvDnisMappingUrl = $1
            $cvDnisMappingUrlAccessError = $2

            $OS_EventId = "SNMPTRAP-cisco-CISCO-VOICE-DNIS-MIB-cvDnisMappingUrlInaccessible"

            @AlertGroup = "DNIS Mapping URL Access Status"
            $OctetString = extract($OID1, "\.(.*)$")
            include "$NC_RULES_HOME/include-snmptrap/decodeOctetString.include.snmptrap.rules"
            $cvDnisMappingName = $String

            @AlertKey = "cvDnisMappingEntry." + $cvDnisMappingName

            @Summary = "Specified URL : " + @AlertKey + " is not accessible because " + $cvDnisMappingUrlAccessError

            update(@Summary)

            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cvDnisMappingUrl,$cvDnisMappingUrlAccessError,$cvDnisMappingName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cvDnisMappingUrl", $cvDnisMappingUrl, "cvDnisMappingUrlAccessError", $cvDnisMappingUrlAccessError, "cvDnisMappingName", $cvDnisMappingName)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-VOICE-DNIS-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-VOICE-DNIS-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-VOICE-DNIS-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-VOICE-DNIS-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-VOICE-DNIS-MIB.include.snmptrap.rules >>>>>")
