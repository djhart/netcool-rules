###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 2.0 - Trap(s) addedd
#
#          - ciscoNetRegFreeAddrLowThreshold
#          - ciscoNetRegFreeAddrHighThreshold
#          - ciscoNetRegOtherServerNotResp
#          - ciscoNetRegOtherServerResp
#          - ciscoNetRegHaDnsPartnerDown
#          - ciscoNetRegHaDnsPartnerUp
#          - ciscoNetRegMastersNotResp
#          - ciscoNetRegMastersResp
#          - ciscoNetRegSecondaryZonesExpired
#          - ciscoNetRegDnsForwardersNotResp
#          - ciscoNetRegDnsForwardersResp
#          - ciscoNetRegHaDnsConfigErr
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  cisco-CISCO-NETWORK-REGISTRAR-MIB
#
###############################################################################

log(DEBUG, "<<<<< Entering... cisco-CISCO-NETWORK-REGISTRAR-MIB.adv.include.snmptrap.rules >>>>>")

switch($specific-trap)
{
    case "1": ### ciscoNetRegFreeAddressLow

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoNetRegFreeAddressLow"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "cnrDHCPScopeEntry." + $cnrDHCPScopeName 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "2": ### ciscoNetRegFreeAddressHigh

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoNetRegFreeAddressHigh"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "cnrDHCPScopeEntry." + $cnrDHCPScopeName 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "3": ### ciscoNetRegServerStart

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoNetRegServerStart"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = $cnrNotifServerType 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "4": ### ciscoNetRegServerStop

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoNetRegServerStop"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = $cnrNotifServerType 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "5": ### ciscoNetRegDNSQueueTooBig

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoNetRegDNSQueueTooBig"
        $OS_OsiLayer = 0

    case "6": ### ciscoNetRegOtherServerNotResponding

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoNetRegOtherServerNotResponding"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = $cnrNotifServer 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "7": ### ciscoNetRegDuplicateAddress

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoNetRegDuplicateAddress"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = $cnrNotifMACAddress 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "8": ### ciscoNetRegAddressConflict

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoNetRegAddressConflict"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = $cnrNotifContestedIpAddress 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "9": ### ciscoNetRegOtherServerResponding

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoNetRegOtherServerResponding"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = $cnrNotifServer
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "10": ### ciscoNetRegFailoverConfigMismatch

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoNetRegFailoverConfigMismatch"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = $cnrNotifServer 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "11": ### ciscoNetRegFreeAddrLowThreshold

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoNetRegFreeAddrLowThreshold"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "DHCP Scope: " + $cnrNotifDHCPScopeName
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "12": ### ciscoNetRegFreeAddrHighThreshold

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoNetRegFreeAddrHighThreshold"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "DHCP Scope: " + $cnrNotifDHCPScopeName
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "13": ### ciscoNetRegOtherServerNotResp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoNetRegOtherServerNotResp"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Server: " + $cnrNotifServer
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "14": ### ciscoNetRegOtherServerResp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoNetRegOtherServerResp"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Server: " + $cnrNotifServer
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "15": ### ciscoNetRegHaDnsPartnerDown

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoNetRegHaDnsPartnerDown"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "HA-DNS Partner Server: " + $cnrNotifServer
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "16": ### ciscoNetRegHaDnsPartnerUp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoNetRegHaDnsPartnerUp"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "HA-DNS Partner Server: " + $cnrNotifServer
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "17": ### ciscoNetRegMastersNotResp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoNetRegMastersNotResp"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "DNS Secondary Server: " + $cnrNotifDnsServerIpAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "18": ### ciscoNetRegMastersResp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoNetRegMastersResp"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "DNS Secondary Server: " + $cnrNotifDnsServerIpAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "19": ### ciscoNetRegSecondaryZonesExpired

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoNetRegSecondaryZonesExpired"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "DNS Server: " + $cnrNotifDnsServerIpAddress + ", Zone: " + $cnrNotifZoneNam
        $OS_LocalRootObj = "DNS Server: " + $cnrNotifDnsServerIpAddress
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "20": ### ciscoNetRegDnsForwardersNotResp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoNetRegDnsForwardersNotResp"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "DNS Resolving Server: " + $cnrNotifDnsServerIpAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "21": ### ciscoNetRegDnsForwardersResp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoNetRegDnsForwardersResp"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "DNS Resolving Server: " + $cnrNotifDnsServerIpAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "22": ### ciscoNetRegHaDnsConfigErr

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoNetRegHaDnsConfigErr"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Server: " + $cnrNotifServer
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    default:
}

log(DEBUG, "<<<<< Leaving... cisco-CISCO-NETWORK-REGISTRAR-MIB.adv.include.snmptrap.rules >>>>>")
