###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
##############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-SERVICE-CONTROL-RDR-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.637": ### Cisco Raw Data Record Formatter Running On A Service Control - Notifications from CISCO-SERVICE-CONTROL-RDR-MIB (200708140000Z) 

    log(DEBUG, "<<<<< Entering... cisco-CISCO-SERVICE-CONTROL-RDR-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Cisco-SERVICE-CONTROL-RDR-MIB"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### cscRdrCategoryStoppedDiscardingReportsTrap

            ##########
            # $1 = entPhysicalName 
            # $2 = cscRdrCategoryID 
            ##########

            $entPhysicalName = $1
            $cscRdrCategoryID = $2

            $entPhysicalIndex = extract($OID2, "\.([0-9]+)\.[0-9]+$")

            $cscRdrCategoryIndex = extract($OID2, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-cisco-CISCO-SERVICE-CONTROL-RDR-MIB-cscRdrCategoryStoppedDiscardingReportsTrap"

            @AlertGroup = "RDR Collector Congestion/Availablity Status"

            @AlertKey = "cscRdrCategoryEntry." + $entPhysicalIndex + "." + $cscRdrCategoryIndex

            @Summary = "System Has Recovered from the RDR Collector Congestion or Unavailablity ( " + @AlertKey + " ) "

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($entPhysicalName,$cscRdrCategoryID,$entPhysicalIndex,$cscRdrCategoryIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "entPhysicalName", $entPhysicalName, "cscRdrCategoryID", $cscRdrCategoryID, "entPhysicalIndex", $entPhysicalIndex,
                 "cscRdrCategoryIndex", $cscRdrCategoryIndex)

        case "2": ### cscRdrCategoryDiscardingReportsTrap

            ##########
            # $1 = entPhysicalName 
            # $2 = cscRdrCategoryID 
            ##########

            $entPhysicalName = $1
            $cscRdrCategoryID = $2

            $entPhysicalIndex = extract($OID2, "\.([0-9]+)\.[0-9]+$")

            $cscRdrCategoryIndex = extract($OID2, "\.([0-9]+)$")
 
            $OS_EventId = "SNMPTRAP-cisco-CISCO-SERVICE-CONTROL-RDR-MIB-cscRdrCategoryDiscardingReportsTrap"

            @AlertGroup = "RDR Collector Congestion/Availablity Status"

            @AlertKey = "cscRdrCategoryEntry." + $entPhysicalIndex + "." + $cscRdrCategoryIndex

            @Summary = "System Has Started Discarding Reports Due RDR Collector Congestion or Unavailablity ( " + @AlertKey + " ) "

            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($entPhysicalName,$cscRdrCategoryID,$entPhysicalIndex,$cscRdrCategoryIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "entPhysicalName", $entPhysicalName, "cscRdrCategoryID", $cscRdrCategoryID, "entPhysicalIndex", $entPhysicalIndex,
                 "cscRdrCategoryIndex", $cscRdrCategoryIndex)

        case "3": ### cscRdrNoActiveConnectionTrap

            ##########
            # $1 = entPhysicalName 
            ##########

            $entPhysicalName = $1

            $entPhysicalIndex = extract($OID1, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-cisco-CISCO-SERVICE-CONTROL-RDR-MIB-cscRdrNoActiveConnectionTrap"

            @AlertGroup = "RDR Formatter - Collector Connectivity Status"

            @AlertKey = "entPhysicalEntry." + $entPhysicalIndex

            @Summary = "No Active Connection Between RDR Formatter and the Collector ( " + @AlertKey + " ) "

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($entPhysicalName,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "entPhysicalName", $entPhysicalName, "entPhysicalIndex", $entPhysicalIndex)

        case "4": ### cscRdrConnectionStatusDownTrap

            ##########
            # $1 = entPhysicalName 
            # $2 = cscRdrDestStatus 
            # $3 = cscRdrDestInetAddress 
            # $4 = cscRdrDestPort 
            ##########

            $entPhysicalName = $1
            $cscRdrDestStatus = lookup($2, CscRdrDestStatus) 
            $cscRdrDestInetAddress = $3
            $cscRdrDestPort = $4

            $entPhysicalIndex = extract($OID3, "\.([0-9]+)\.[0-9]+$")

            $cscRdrDestIndex = extract($OID3, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-cisco-CISCO-SERVICE-CONTROL-RDR-MIB-cscRdrConnectionStatusDownTrap"

            @AlertGroup = "RDR Connection Status with Destination Server"

            @AlertKey = "cscRdrDestEntry." + $entPhysicalIndex + "." + $cscRdrDestIndex

            @Summary = "RDR Connection to the Destination Server: " + $cscRdrDestInetAddress + " is Down ( " + @AlertKey + " ) "

            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0 

            $cscRdrDestStatus = $cscRdrDestStatus + " ( " + $2 + " ) "

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($entPhysicalName,$cscRdrDestStatus,$cscRdrDestInetAddress,$cscRdrDestPort,$entPhysicalIndex,$cscRdrDestIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "entPhysicalName", $entPhysicalName, "cscRdrDestStatus", $cscRdrDestStatus, "cscRdrDestInetAddress", $cscRdrDestInetAddress,
                 "cscRdrDestPort", $cscRdrDestPort, "entPhysicalIndex", $entPhysicalIndex, "cscRdrDestIndex", $cscRdrDestIndex)

        case "5": ### cscRdrActiveConnectionTrap

            ##########
            # $1 = entPhysicalName 
            # $2 = cscRdrDestInetAddress 
            # $3 = cscRdrDestPort 
            ##########

            $entPhysicalName = $1
            $cscRdrDestInetAddress = $2
            $cscRdrDestPort = $3

            $entPhysicalIndex = extract($OID3, "\.([0-9]+)\.[0-9]+$")

            $cscRdrDestIndex = extract($OID3, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-cisco-CISCO-SERVICE-CONTROL-RDR-MIB-cscRdrActiveConnectionTrap"

            @AlertGroup = "RDR Connection Status with Destination Server"

            @AlertKey = "cscRdrDestEntry." + $entPhysicalIndex + "." + $cscRdrDestIndex

            @Summary = "RDR Connection to the Destination Server: " + $cscRdrDestInetAddress + " is Active ( " + @AlertKey + " ) "

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($entPhysicalName,$cscRdrDestInetAddress,$cscRdrDestPort,$entPhysicalIndex,$cscRdrDestIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "entPhysicalName", $entPhysicalName, "cscRdrDestInetAddress", $cscRdrDestInetAddress, "cscRdrDestPort", $cscRdrDestPort,
                 "entPhysicalIndex", $entPhysicalIndex, "cscRdrDestIndex", $cscRdrDestIndex)

        case "6": ### cscRdrConnectionStatusUpTrap

            ##########
            # $1 = entPhysicalName 
            # $2 = cscRdrDestStatus 
            # $3 = cscRdrDestInetAddress 
            # $4 = cscRdrDestPort 
            ##########

            $entPhysicalName = $1
            $cscRdrDestStatus = lookup($2, CscRdrDestStatus) 
            $cscRdrDestInetAddress = $3
            $cscRdrDestPort = $4

            $entPhysicalIndex = extract($OID3, "\.([0-9]+)\.[0-9]+$")

            $cscRdrDestIndex = extract($OID3, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-cisco-CISCO-SERVICE-CONTROL-RDR-MIB-cscRdrConnectionStatusUpTrap"

            @AlertGroup = "RDR Connection Status with Destination Server"

            @AlertKey = "cscRdrDestEntry." + $entPhysicalIndex + "." + $cscRdrDestIndex

            @Summary = "RDR Connection to the Destination Server: " + $cscRdrDestInetAddress + " is Up ( " + @AlertKey + " ) "

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0 

            $cscRdrDestStatus = $cscRdrDestStatus + " ( " + $2 + " ) "

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($entPhysicalName,$cscRdrDestStatus,$cscRdrDestInetAddress,$cscRdrDestPort,$entPhysicalIndex,$cscRdrDestIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "entPhysicalName", $entPhysicalName, "cscRdrDestStatus", $cscRdrDestStatus, "cscRdrDestInetAddress", $cscRdrDestInetAddress,
                 "cscRdrDestPort", $cscRdrDestPort, "entPhysicalIndex", $entPhysicalIndex, "cscRdrDestIndex", $cscRdrDestIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-SERVICE-CONTROL-RDR-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-SERVICE-CONTROL-RDR-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-SERVICE-CONTROL-RDR-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-SERVICE-CONTROL-RDR-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-SERVICE-CONTROL-RDR-MIB.include.snmptrap.rules >>>>>")
