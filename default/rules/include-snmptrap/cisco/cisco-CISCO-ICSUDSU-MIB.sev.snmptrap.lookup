###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-ICSUDSU-MIB
#
# 1.1 - Changed the format
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table cisco-CISCO-ICSUDSU-MIB_sev =
{
	    {"SNMPTRAP-cisco-CISCO-ICSUDSU-MIB-ciscoICsuDsuT1LoopStatusNotification_ok","1","2","0"},
	    {"SNMPTRAP-cisco-CISCO-ICSUDSU-MIB-ciscoICsuDsuT1LoopStatusNotification_lossofSignal","3","1","0"},
	    {"SNMPTRAP-cisco-CISCO-ICSUDSU-MIB-ciscoICsuDsuT1LoopStatusNotification_lossofFrame","3","1","0"},
	    {"SNMPTRAP-cisco-CISCO-ICSUDSU-MIB-ciscoICsuDsuT1LoopStatusNotification_lossofSignalFrame","3","1","0"},
	    {"SNMPTRAP-cisco-CISCO-ICSUDSU-MIB-ciscoICsuDsuT1LoopStatusNotification_detectedRemoteAlarmIndication","3","1","0"},
	    {"SNMPTRAP-cisco-CISCO-ICSUDSU-MIB-ciscoICsuDsuT1LoopStatusNotification_detectedAlarmIndicationSignal","3","1","0"},
	    {"SNMPTRAP-cisco-CISCO-ICSUDSU-MIB-ciscoICsuDsuT1LoopStatusNotification_placedInLoopback","2","1","0"},
	    {"SNMPTRAP-cisco-CISCO-ICSUDSU-MIB-ciscoICsuDsuT1LoopStatusNotification_unknown","2","1","0"},
	    {"SNMPTRAP-cisco-CISCO-ICSUDSU-MIB-ciscoICsuDsuSw56kLoopStatusNotification_ok","1","2","0"},
	    {"SNMPTRAP-cisco-CISCO-ICSUDSU-MIB-ciscoICsuDsuSw56kLoopStatusNotification_oosOofFromNetwork","3","1","0"},
	    {"SNMPTRAP-cisco-CISCO-ICSUDSU-MIB-ciscoICsuDsuSw56kLoopStatusNotification_noReceiveSignal","3","1","0"},
	    {"SNMPTRAP-cisco-CISCO-ICSUDSU-MIB-ciscoICsuDsuSw56kLoopStatusNotification_noSealingCurrent","3","1","0"},
	    {"SNMPTRAP-cisco-CISCO-ICSUDSU-MIB-ciscoICsuDsuSw56kLoopStatusNotification_noFrameSync","3","1","0"},
	    {"SNMPTRAP-cisco-CISCO-ICSUDSU-MIB-ciscoICsuDsuSw56kLoopStatusNotification_attemptingToRateAdap","3","1","0"},
	    {"SNMPTRAP-cisco-CISCO-ICSUDSU-MIB-ciscoICsuDsuSw56kLoopStatusNotification_rtTestFromTelco","2","1","0"},
	    {"SNMPTRAP-cisco-CISCO-ICSUDSU-MIB-ciscoICsuDsuSw56kLoopStatusNotification_llTestFromTelco","2","1","0"},
	    {"SNMPTRAP-cisco-CISCO-ICSUDSU-MIB-ciscoICsuDsuSw56kLoopStatusNotification_rdlFromRemoteDSU","2","1","0"},
	    {"SNMPTRAP-cisco-CISCO-ICSUDSU-MIB-ciscoICsuDsuSw56kLoopStatusNotification_unknown","2","1","0"}
}
default = {"Unknown","Unknown","Unknown"}
