###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
# Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
# - CISCO-IF-EXTENSION
#
###############################################################################

table ciscoifAdminStatus =
{ 
    {"1","Up"}, ### up - ready to pass packets. If ifAdminStatus is changed to up(1) then ifOperStatus should change to up(1).
    {"2","Down"}, ### down -  When a managed system initializes, all interfaces start with ifAdminStatus in the down(2) state.  
    {"3","Testing"} ### testing - in some test mode, indicates that no operational packets can be passed.
}
default = "Unknown"


table ciscoifOperStatus =
{ 
    {"1","Up"}, ### up - ready to pass packets. If ifAdminStatus is changed to up(1) then ifOperStatus should change to up(1).
    {"2","Down"}, ### down - If ifAdminStatus is down(2) then ifOperStatus should be down(2). 
                  ###      - If the interface is waiting for external actions (such as a serial line waiting for an incoming connection), it should remain in the down(2) state. 
    {"3","Testing"}, ### testing - in some test mode, indicates that no operational packets can be passed.
    {"4","Unknown"}, ### unknown - status can not be determined for some reason.
    {"5","Dormant"}, ### dormant - if the interface is ready to transmit and receive network traffic, it should change to dormant(5).
    {"6","Not Present"}, ### notPresent - some component is missing.
                         ###            - if there is a fault that prevents it from going to the up(1) state, 
                         ###              it should remain in the notPresent(6) state if the interface has missing (typically, hardware) components.
    {"7","Lower Layer Down"} ### lowerLayerDown - lower-layer interface(s).
}
default = "Unknown"

