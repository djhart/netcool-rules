##############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-CALL-TRACKER-MIB
#
###############################################################################
#
# 2.1 - Simplified handling of "Severity via lookup" logic.
#
#     - Added basic debug logging.
#
# 2.0 - Rewritten for improved event presentation.  Includes enhancements which
#       comply with the Netcool Rules File Standards
#       (MUSE-STD-RF-02, July 2002)
#
#     - Modified to support following features introduced in NCiL 2.0:
#         - "Advanced" and "User" include files
#         - "Severity" lookup tables
#
# 1.0 - Based on rules extracted from cisco.include.snmptrap.rules 3.1.
#
###############################################################################

case ".1.3.6.1.4.1.9.9.163.2": ### Cisco Call Progress and Status Tracking - Notifications from CISCO-CALL-TRACKER-MIB

    log(DEBUG, "<<<<< Entering... cisco-CISCO-CALL-TRACKER-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Cisco-Call Tracker"
    @Class = "40057"
    
    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### cctCallSetupNotification

            ##########
            # $1 = cctActiveSetupTime
            # $2 = cctActiveCalledPartyId 
            # $3 = cctActiveCallingPartyId
            # $4 = cctActiveCallCategory
            ##########

            $cctActiveSetupTime = $1
            $cctActiveCalledPartyId = $2
            $cctActiveCallingPartyId = $3
            $cctActiveCallCategory = lookup($4, CctCallCategory)
            $cctActiveCallId = extract($OID1, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-cisco-CISCO-CALL-TRACKER-MIB-cctCallSetupNotification"

            @AlertGroup = "Call Status"
            @AlertKey = "cctActiveEntry." + $cctActiveCallId
            @Summary = $cctActiveCallCategory + " Call Setup  ( Called Party: " + $2 + ", Calling Party: " + $3 + " )"
            
            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $cctActiveCallCategory = $cctActiveCallCategory + " ( " + $4 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cctActiveSetupTime,$cctActiveCalledPartyId,$cctActiveCallingPartyId,$cctActiveCallCategory)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cctActiveSetupTime", $cctActiveSetupTime, "cctActiveCalledPartyId", $cctActiveCalledPartyId, "cctActiveCallingPartyId", $cctActiveCallingPartyId,
                 "cctActiveCallCategory", $cctActiveCallCategory)

        case "2": ### cctCallTerminateNotification

            ##########
            # $1 = cctHistoryCallId
            ##########

            $cctHistoryCallId = $1
            $cctHistoryIndex = extract($OID1, "\.([0-9]+)$")
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cctHistoryCallId,$cctHistoryIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cctHistoryCallId", $cctHistoryCallId, "cctHistoryIndex", $cctHistoryIndex)

            $OS_EventId = "SNMPTRAP-cisco-CISCO-CALL-TRACKER-MIB-cctCallTerminateNotification"

            @AlertGroup = "Call Status"
            @AlertKey = "cctActiveEntry." + $1
            @AlertKey = "cctHistoryEntry." + $cctHistoryIndex
            @Summary = "Call Terminated  ( " + @AlertKey + " )"
            
            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-CALL-TRACKER-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-CALL-TRACKER-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-CALL-TRACKER-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-CALL-TRACKER-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-CALL-TRACKER-MIB.include.snmptrap.rules >>>>>")
