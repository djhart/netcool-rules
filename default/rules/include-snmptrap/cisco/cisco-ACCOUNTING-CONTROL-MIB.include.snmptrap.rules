###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  ACCOUNTING-CONTROL-MIB
#
###############################################################################
#
# 2.1 - Simplified handling of "Severity via lookup" logic.
#
#     - Added basic debug logging.
#
# 2.0 - Rewritten for improved event presentation.  Includes enhancements which
#       comply with the Netcool Rules File Standards
#       (MUSE-STD-RF-02, July 2002)
#
#     - Modified to support following features introduced in NCiL 2.0:
#         - "Advanced" and "User" include files
#         - "Severity" lookup tables
#
# 1.0 - Based on rules extracted from cisco.include.snmptrap.rules 3.1.
#
###############################################################################

case ".1.3.6.1.4.1.9.10.17.3": ### Cisco Accounting Control - Notifications from ACCOUNTING-CONTROL-MIB

    log(DEBUG, "<<<<< Entering... cisco-ACCOUNTING-CONTROL-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Cisco-Accounting Control"
    @Class = "40057"
    
    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### acctngFileNearlyFull

            ##########
            # $1 = acctngFileName 
            # $2 = acctngFileMaximumSize 
            # $3 = acctngControlTrapThreshold
            ##########

            $acctngFileName = $1
            $acctngFileMaximumSize = $2 + " bytes"
            $acctngControlTrapThreshold = $3 + "%"
            $acctngFileIndex = extract($OID1, "\.([0-9]+)$")
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($acctngFileName,$acctngFileMaximumSize,$acctngControlTrapThreshold,$acctngFileIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "acctngFileName", $acctngFileName, "acctngFileMaximumSize", $acctngFileMaximumSize, "acctngControlTrapThreshold", $acctngControlTrapThreshold,
                 "acctngFileIndex", $acctngFileIndex)

            $OS_EventId = "SNMPTRAP-cisco-ACCOUNTING-CONTROL-MIB-acctngFileNearlyFull"

            @AlertGroup = "Accounting File Size"
            @AlertKey = "acctngFileEntry." + $acctngFileIndex
            @Summary = "Accounting File Over " + $3 + "% Full, Max. Size " + $2 + " bytes  ( " + $1 + " )"
            
            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        case "2": ### acctngFileFull
        
            ##########
            # $1 = acctngFileName 
            # $2 = acctngFileMaximumSize 
            ##########

            $acctngFileName = $1
            $acctngFileMaximumSize = $2
            $acctngFileIndex = extract($OID1, "\.([0-9]+)$")
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($acctngFileName,$acctngFileMaximumSize,$acctngFileIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "acctngFileName", $acctngFileName, "acctngFileMaximumSize", $acctngFileMaximumSize, "acctngFileIndex", $acctngFileIndex)

            $OS_EventId = "SNMPTRAP-cisco-ACCOUNTING-CONTROL-MIB-acctngFileFull"

            @AlertGroup = "Accounting File Size"
            @AlertKey = "acctngFileEntry." + $acctngFileIndex
            @Summary = "Accounting File Full, Max. Size " + $2 + " bytes  ( " + $1 + " )"
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0
                       
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        default:
        
            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-ACCOUNTING-CONTROL-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-ACCOUNTING-CONTROL-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-ACCOUNTING-CONTROL-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-ACCOUNTING-CONTROL-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-ACCOUNTING-CONTROL-MIB.include.snmptrap.rules >>>>>")
