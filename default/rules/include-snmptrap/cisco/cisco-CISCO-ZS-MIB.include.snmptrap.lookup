##############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
# Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
# - CISCO-ZS-MIB
#
# -  Based on lookup table definitions extracted from
#    cisco.include.snmptrap.lookup 2.8.
#
###############################################################################
#
# 2.0 - Added in lookup element
#       - Elements 5-10 for table ciscoZoneMemberType
#     - Added in tables 
#       - ciscoFcZoneServerRejReasonExpl 
#       - ciscoZoneMergeFailCause 
#       - ciscozoneSetActivateResult 
#
###############################################################################

table ciscoZoneMemberType =
{
    {"1","Port WWN"}, ### wwn - The Port World Wide Name (PWWN).
    {"2","Domain And Port Number Format"}, ### domainPort - Domain and Port number format.
    {"3","FC-ID"}, ### fcAddr - FC-ID.
    {"4","Fabric Port WWN"}, ### fwwn - Fabric Port World Wide Name. This is used for Port based zoning.
    {"5","Symbolic Node Name"}, ### symNodeName - Symbolic Node Name used for iSCSI zoning.
    {"6","Interface Format"}, ### intf - interface format.
    {"7","Domain And Interface Format"}, ### domainIntf - Domain and Interface format.
    {"8","IPv4 Address Format"}, ### ipAddr - IPv4 address format.
    {"9","IPv6 Address Format"}, ### ipAddrv6 - IPv6 address format.
    {"10","Device Alias"} 
    ### deviceAlias - Device Alias. This is a human readable string used to alias a World Wide Name (WWN). 
    ### It is defined in CISCO-FC-DEVICE-ALIAS-MIB."
}
default = "Unknown"

table ciscoFcZoneServerRejReasonExpl =
{
    {"1","No Additional Explanation"}, ### noAdditionalExplanation - there is no additional explanation.
    {"2","Zones Not Supported"}, ### zonesNotSupported- zones are not supported.
    {"3","Zone Set Name Unknown"}, ### zoneSetNameUnknown - zone set name is not known.
    {"4","No Zone Set Active"}, ### noZoneSetActive - no zone set is currently active.
    {"5","Zone Name Unknown"}, ### zoneNameUnknown - zone name is unknown.
    {"6","Zone State Unknown"}, ### zoneStateUnknown - state of the zone is not known.
    {"7","Incorrect Pay load Len"}, ### incorrectPayloadLen - payload len is not correct.
    {"8","Too Large Zone Set"}, ### tooLargeZoneSet - zone set is larger than permitted size.
    {"9","Deactivate Zone Set Failed"}, ### deactivateZoneSetFailed - deactivation of zone set failed.
    {"10","Req Not Supported Alias"}, ### reqNotSupportedAlias - request is not supported.
    {"11","Capability Not Supported"}, ### capabilityNotSupported - capability is not supported.
    {"12","Zone MemberID Type Not Supp"}, ### zoneMemberIDTypeNotSupp - type of zone member is not supported.
    {"13","Invalid Zone Set Definition"} ### invalidZoneSetDefinition - zone set definition is invalid.
}
default = "Unknown"


table ciscoZoneMemberTypeIndex =
{
    
    {"1","Zone"}, ### zone - member belongs to a zone
    {"2","Alias"} ### alias - member belongs to an alias
}
default = "Unknown"

table ciscoZoneMergeFailCause =
{
    {"1","Other"}, ### other - reason other than those specified below.
    {"2","Zoning Mode Mismatch"}, ### zoningModeMismatch - mismatch in zoning modes between the 2 switches.
    {"3","qosNotEnabled"}, 
    ### qosNotEnabled - qos is not enabled on one of the switches and qos attribute is present in one of the zones.
    {"4","Qos Conflict"}, ### qosConflict - conflict between VSAN based Qos and zone attribute qos.
    {"5","Broadcast Not Enabled"},
    ### bcastNotEnabled - broadcast service has not been enabled on one of the switches and broadcast attribute 
    ### is present in one of the zones.
    {"6","Merge Control Flag Mismatch"}, ### mergeCtrlMismatch - there is a mismatch in the merge control flag value. 
    {"7","Default Zone Mismatch"}, ### defZoneMismatch - there is a mismatch in the default zone policy.
    {"8","Qos Attribute Missing"}, ### qosAttrMissing - qos attribute is missing.
    {"9","Broadcast Attribute Missing"}, ### bcastAttrMissing - broadcast attribute is missing.
    {"10","Read Only Attribute Missing"}, ### rdonlyAttrMissing - read only attribute is missing.
    {"11","Member Mismatch"}, ### memberMismatch - there is a mismatch in the members.
    {"12","Invalid Payload Format"}, ### invPayloadFormat - the payload format is invalid.
    {"13","Size Exceeded"}, ### Size Exceeded - the payload exceeded the maximum size.
    {"14","Unlicensed Member"}, ### unlicensedMember - a member is unlicensed.
    {"15","Non Interop Zone Set "} 
    ### nonInteropZoneset - a zone attribute/member which cannot be supported in the current interop mode of the switch."
}
default = "Unknown"
                 
table ciscoZoneDefaultZoneBehaviour =
{
    {"1","Permit"}, ### permit - allow communication
    {"2","Deny"} ### deny - deny communication
}
default = "Unknown"

table ciscozoneSetActivateResult =
{
    {"1","Activation Success"}, ### activateSuccess - activation success
    {"2","Activation Failure"}, ### activateFailure - activation failure
    {"3","Deactivation Success"}, ### deactivateSuccess - deactivation success
    {"4","Fabric Port WWN"}, ### deactivateFailure - deactivation failure
    {"5","In Progress"}, ### inProgress - in progress
    {"6","Created New Entry"} ### newEntry - entry just created
}
default = "Unknown"


