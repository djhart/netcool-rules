###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
#        1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-PSA-MICROCODE-MIB
#
###############################################################################
#
# 1.1 - Simplified handling of "Severity via lookup" logic.
#
#     - Added basic debug logging.
#
###############################################################################

case ".1.3.6.1.4.1.9.9.259": ### Cisco PSA Microcode - Notifications from CISCO-PSA-MICROCODE-MIB

    log(DEBUG, "<<<<< Entering... cisco-CISCO-PSA-MICROCODE-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Cisco-PSA Microcode"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### ciscoPsaMicrocodeReload

            ##########
            # $1 = entPhysicalName
            # $2 = entPhysicalDescr 
            # $3 = cpmcModulePsaCurrBundleId
            # $4 = cpmcModulePsaPrevBundleId
            # $5 = cpmcModulePsaFeaturesEnabled 
            # $6 = cpmcModulePsaFeaturesDisabled 
            ##########

            $entPhysicalName = $1
            $entPhysicalDescr = $2
            $cpmcModulePsaCurrBundleId = lookup($3, PsaMicrocodeBundleId)
            $cpmcModulePsaPrevBundleId = lookup($4, PsaMicrocodeBundleId) + " ( " + $4 + " )"
            $cpmcModulePsaFeaturesEnabled = $5
            $cpmcModulePsaFeaturesDisabled = $6
            $entPhysicalIndex = extract($OID1, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-cisco-CISCO-PSA-MICROCODE-MIB-ciscoPsaMicrocodeReload"

            @AlertGroup = "PSA Microcode Status"
            @AlertKey = "cpmcModulePsaEntry." + $entPhysicalIndex
            @Summary = "PSA Microcode Reloaded, Current Microcode Bundle: " + $cpmcModulePsaCurrBundleId + "  ( " + @AlertKey + " )"
            
            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $cpmcModulePsaCurrBundleId = $cpmcModulePsaCurrBundleId + " ( " + $3 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($entPhysicalName,$entPhysicalDescr,$cpmcModulePsaCurrBundleId,$cpmcModulePsaPrevBundleId,$cpmcModulePsaFeaturesEnabled,$cpmcModulePsaFeaturesDisabled,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "entPhysicalName", $entPhysicalName, "entPhysicalDescr", $entPhysicalDescr, "cpmcModulePsaCurrBundleId", $cpmcModulePsaCurrBundleId,
                 "cpmcModulePsaPrevBundleId", $cpmcModulePsaPrevBundleId, "cpmcModulePsaFeaturesEnabled", $cpmcModulePsaFeaturesEnabled, "cpmcModulePsaFeaturesDisabled", $cpmcModulePsaFeaturesDisabled,
                 "entPhysicalIndex", $entPhysicalIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-PSA-MICROCODE-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-PSA-MICROCODE-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
#########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-PSA-MICROCODE-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-PSA-MICROCODE-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-PSA-MICROCODE-MIB.include.snmptrap.rules >>>>>")
