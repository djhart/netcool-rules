###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  ONS15501-MIB
#
###############################################################################

table Ons15501AlarmStatus =
{
    ##########
    # This object represents a alarm level
    ##########

    {"1","Critical"}, ### critical
    {"2","Major"}, ### major
    {"3","Minor"}, ### minor
    {"4","Info"}, ### info
    {"5","No Alarm"} ### noAlarm
}
default = "Unknown"

table Ons15501TrapTypeEnumeration =
{
    ##########
    # This object provides the details on type of alarm
    ##########

    {"1","Unacceptable Ambient Temperature"}, ### unacceptableAmbientTemperature
    {"2","Unacceptable Electrical Power"}, ### unacceptableElectricalPower
    {"3","Input Signal Power Too Low"}, ### inputSignalPowerTooLow
    {"4","Unacceptable Output Signal Power"}, ### unacceptableOutputSignalPower
    {"5","Embedded Controller Communication Failed"}, ### embeddedControllerCommFailure
    {"6","Software Upgrade Initiated"}, ### softwareUpgradeInitiated
    {"7","Software Upgrade Failed"}, ### softwareUpgradeFailed
    {"8","Software Upgrade Completed"}, ### softwareUpgradeCompleted
    {"9","Software Reboot Initiated"}, ### softwareRebootInitiated
    {"10","Software Rolled Back"}, ### softwareRolledBack
    {"11","Configuration Changed"}, ### configurationChanged
    {"12","Unacceptable Gain"}, ### unacceptableGain
    {"13","Laser Pump Bad"}, ### laserPumpBad
    {"14","eEPROM Bad"}, ### eEPROMBad
    {"32767","Unknown"} ### unknown
}
default = "Unknown"

table Ons15501TrapDirectionEnumeration =
{
    ##########
    # This object represents a alarm level
    ##########

    {"1","Don't Care"}, ### dontCare
    {"2","Asserted"}, ### asserted
    {"3","Cleared"} ### cleared
}
default = "Unknown"

table Ons15501TrapTypeEnumeration_Raw =
{
    ##########
    # This object provides the details on type of alarm
    ##########

    {"1","Ambient Temperature Status"}, ### unacceptableAmbientTemperature
    {"2","Electrical Power Status"}, ### unacceptableElectricalPower
    {"3","Input Signal Power Status"}, ### inputSignalPowerTooLow
    {"4","Output Signal Power Status"}, ### unacceptableOutputSignalPower
    {"5","Embedded Controller Communication Status"}, ### embeddedControllerCommFailure
    {"6","Software Upgrade Status"}, ### softwareUpgradeInitiated
    {"7","Software Upgrade Status"}, ### softwareUpgradeFailed
    {"8","Software Upgrade Status"}, ### softwareUpgradeCompleted
    {"9","Software Reboot Status"}, ### softwareRebootInitiated
    {"10","Software Rolled Status"}, ### softwareRolledBack
    {"11","Configuration Status"}, ### configurationChanged
    {"12","Gain Status"}, ### unacceptableGain
    {"13","Laser Pump Status"}, ### laserPumpBad
    {"14","eEPROM Status"}, ### eEPROMBad
    {"32767","Unknown"} ### unknown
}
default = "Unknown"

