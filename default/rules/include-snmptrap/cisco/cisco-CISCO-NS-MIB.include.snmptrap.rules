##############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-NS-MIB
#
###############################################################################
#
# 2.2 - Trap(s) added 
# 
#         - fcNameServerEntryAdd
#         - fcNameServerEntryDelete
#
# 2.1 - Simplified handling of "Severity via lookup" logic.
#
#     - Added basic debug logging.
#
# 2.0 - Rewritten for improved event presentation.  Includes enhancements which
#       comply with the Netcool Rules File Standards
#       (MUSE-STD-RF-02, July 2002)
#
#     - Modified to support following features introduced in NCiL 2.0:
#         - "Advanced" and "User" include files
#         - "Severity" lookup tables
#
# 1.0 - Based on rules extracted from cisco.include.snmptrap.rules 3.1.
#
###############################################################################

case ".1.3.6.1.4.1.9.9.293.1.4": ### Cisco Name Server - Notifications from CISCO-NS-MIB (200408300000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-NS-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Cisco-Name Server"
    @Class = "40057"
    
    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### fcNameServerRejectRegNotify

            ##########
            # $1 = fcNameServerPortName
            # $2 = fcNameServerRejectReasonCode 
            # $3 = fcNameServerRejReasonCodeExp
            ##########

            $fcNameServerPortName = $1
            $fcNameServerRejectReasonCode = lookup($2, FcGs3RejectReasonCode)
            $fcNameServerRejReasonCodeExp = $3
            $vsanIndex = extract($OID1, "3\.6\.1\.4\.1\.9\.9\.293\.1\.1\.4\.1\.2\.([0-9]+)\..*$")
            
            $fcNameServerPortIdentifier_OctetString = extract($OID1, "3\.6\.1\.4\.1\.9\.9\.293\.1\.1\.4\.1\.2\.[0-9]+\.(.*)$")
            $OctetString = $fcNameServerPortIdentifier_OctetString
            include "$NC_RULES_HOME/include-snmptrap/decodeOctetString.include.snmptrap.rules"
            $fcNameServerPortIdentifier = $String

            $OS_EventId = "SNMPTRAP-cisco-CISCO-NS-MIB-fcNameServerRejectRegNotify"

            @AlertGroup = "Name Server Registration Status"
            @AlertKey = "fcNameServerEntry." + $vsanIndex + "." + $fcNameServerPortIdentifier_OctetString
            @Summary = "Name Server Rejected Registration Request, " + $fcNameServerRejectReasonCode + "  ( NS Port: " + $1 + " )"
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $2
            
            $fcNameServerRejectReasonCode = $fcNameServerRejectReasonCode + " ( " + $2 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($fcNameServerPortName,$fcNameServerRejectReasonCode,$fcNameServerRejReasonCodeExp,$vsanIndex,$fcNameServerPortIdentifier)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fcNameServerPortName", $fcNameServerPortName, "fcNameServerRejectReasonCode", $fcNameServerRejectReasonCode, "fcNameServerRejReasonCodeExp", $fcNameServerRejReasonCodeExp,
                 "vsanIndex", $vsanIndex, "fcNameServerPortIdentifier", $fcNameServerPortIdentifier)

        case "2": ### fcNameServerDatabaseFull

            ##########
            # $1 = notifyVsanIndex
            ##########

            $notifyVsanIndex = $1
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($notifyVsanIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "notifyVsanIndex", $notifyVsanIndex)

            $OS_EventId = "SNMPTRAP-cisco-CISCO-NS-MIB-fcNameServerDatabaseFull"

            @AlertGroup = "Name Server Database Status"
            @AlertKey = "vsanEntry." + $1
            @Summary = "Name Server Database Full"
            if(int($1)<10)
            {
                @Summary = @Summary + "  ( VSAN: VSAN000" + $1 + " )"
            }
            else if(int($1)>9 && int($1)<100)
            {
                @Summary = @Summary + "  ( VSAN: VSAN00" + $1 + " )"
            }
            else if(int($1)>99 && int($1)<1000)
            {
                @Summary = @Summary + "  ( VSAN: VSAN0" + $1 + " )"
            }
            else if(int($1)>999 && int($1)<10000)
            {
                @Summary = @Summary + "  ( VSAN: VSAN" + $1 + " )"
            }
            else
            {
                @Summary = @Summary + "  ( " + @AlertKey + " )"
            }
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap


        case "3": ### fcNameServerEntryAdd

            ##########
            # $1 = fcNameServerPortName
            # $2 = fcNameServerPortType
            ##########

            $fcNameServerPortName = $1
            $fcNameServerPortType = lookup($2, FcNameServerPortType)

            $OS_EventId = "SNMPTRAP-cisco-CISCO-NS-MIB-fcNameServerEntryAdd"

            @AlertGroup = "Name Server Entry Status"

            $vsanIndex = extract($OID1, "3\.6\.1\.4\.1\.9\.9\.293\.1\.1\.4\.1\.2\.([0-9]+)\..*$")
        
            $fcNameServerPortIdentifier_OctetString = extract($OID1, "3\.6\.1\.4\.1\.9\.9\.293\.1\.1\.4\.1\.2\.[0-9]+\.(.*)$")
            $OctetString = $fcNameServerPortIdentifier_OctetString
            include "$NC_RULES_HOME/include-snmptrap/decodeOctetString.include.snmptrap.rules"
            $fcNameServerPortIdentifier = $String

            @AlertKey = "fcNameServerEntry." + $vsanIndex + "." + $fcNameServerPortIdentifier_OctetString

            @Summary = "A New Entry Added to the Name Server Database" + " ( " + @AlertKey + " ) " 

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            $fcNameServerPortType = $fcNameServerPortType + " ( " + $2 + " ) "

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($fcNameServerPortName,$fcNameServerPortType,$vsanIndex,$fcNameServerPortIdentifier)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fcNameServerPortName", $fcNameServerPortName, "fcNameServerPortType", $fcNameServerPortType, "vsanIndex", $vsanIndex,
                 "fcNameServerPortIdentifier", $fcNameServerPortIdentifier)

        case "4": ### fcNameServerEntryDelete

            ##########
            # $1 = fcNameServerPortName
            # $2 = fcNameServerPortType
            ##########

            $fcNameServerPortName = $1
            $fcNameServerPortType = lookup($2, FcNameServerPortType)

            $OS_EventId = "SNMPTRAP-cisco-CISCO-NS-MIB-fcNameServerEntryDelete"

            @AlertGroup = "Name Server Entry Status"

            $vsanIndex = extract($OID1, "3\.6\.1\.4\.1\.9\.9\.293\.1\.1\.4\.1\.2\.([0-9]+)\..*$")

            $fcNameServerPortIdentifier_OctetString = extract($OID1, "3\.6\.1\.4\.1\.9\.9\.293\.1\.1\.4\.1\.2\.[0-9]+\.(.*)$")
            $OctetString = $fcNameServerPortIdentifier_OctetString
            include "$NC_RULES_HOME/include-snmptrap/decodeOctetString.include.snmptrap.rules"
            $fcNameServerPortIdentifier = $String

            @AlertKey = "fcNameServerEntry." + $vsanIndex + "." + $fcNameServerPortIdentifier_OctetString

            @Summary = "An Existing Entry Deleted from the Name Server Database" + " ( " + @AlertKey + " ) "

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($fcNameServerPortName,$fcNameServerPortType,$vsanIndex,$fcNameServerPortIdentifier)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fcNameServerPortName", $fcNameServerPortName, "fcNameServerPortType", $fcNameServerPortType, "vsanIndex", $vsanIndex,
                 "fcNameServerPortIdentifier", $fcNameServerPortIdentifier)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-NS-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-NS-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-NS-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-NS-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-NS-MIB.include.snmptrap.rules >>>>>")
