###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
# Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
# - CISCO-IETF-PIM-EXT-MIB
#
###############################################################################


table ciscocpimExtRPSetType =          
{
    {"1","Other"}, ### other
    {"2","Static"}, ### static - Indicates entry was learnt from local configuration.
    {"3","BSR"}, ### bsr - Indicates entry was learnt from BSR RP discovery protocol. 
    {"4","Embedded "}, 
    ### embedded - Indicates entry was learnt from Embedded-RP mechanism where the RP-Address is embedded in the Multicast Group address.
    {"5","Auto-Rendezvous-Point"} 
    ### autorp - Indicates entry was learnt from Auto-Rendezvous-Point protocol, a Cisco proprietary protocol
    ###          to dynamically distribute Group to RP mappings within an autonomous system."
}
default = "Unknown"


table ciscocpimRPMappingChangeType =          
{   
    {"1","NewMapping"}, ### newMapping - Indicates that a new mapping has been added into the pimRPSetTable.
    {"2","DeletedMapping"}, ### deletedMapping - Indicates that a mapping has been deleted from the pimRPSetTable.
    {"3","ModifiedOldMapping"}, ### modifiedOldMapping
    {"4","ModifiedNewMapping"}  ### modifiedNewMapping 
    ### modifiedXXXMapping indicates that an RP mapping (which already existed in the table) has been modified.
    ### The two modifications types i.e. modifiedOldMapping and modifiedNewMapping, are defined to differentiate
    ### the notification generated before modification from that generated after modification.

}
default = "Unknown"
