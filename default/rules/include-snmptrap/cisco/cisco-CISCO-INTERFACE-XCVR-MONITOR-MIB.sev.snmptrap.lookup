###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-INTERFACE-XCVR-MONITOR-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB_sev =
{
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TemperatureWarning_highSet","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TemperatureWarning_lowSet","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TemperatureWarning_highClear","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TemperatureWarning_lowClear","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TemperatureWarning_normal","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TemperatureWarning_unknown","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TemperatureAlarm_highSet","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TemperatureAlarm_lowSet","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TemperatureAlarm_highClear","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TemperatureAlarm_lowClear","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TemperatureAlarm_normal","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TemperatureAlarm_unknown","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_VoltageWarning_highSet","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_VoltageWarning_lowSet","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_VoltageWarning_highClear","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_VoltageWarning_lowClear","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_VoltageWarning_normal","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_VoltageWarning_unknown","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_VoltageAlarm_highSet","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_VoltageAlarm_lowSet","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_VoltageAlarm_highClear","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_VoltageAlarm_lowClear","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_VoltageAlarm_normal","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_VoltageAlarm_unknown","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_CurrentWarning_highSet","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_CurrentWarning_lowSet","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_CurrentWarning_highClear","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_CurrentWarning_lowClear","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_CurrentWarning_normal","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_CurrentWarning_unknown","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_CurrentAlarm_highSet","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_CurrentAlarm_lowSet","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_CurrentAlarm_highClear","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_CurrentAlarm_lowClear","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_CurrentAlarm_normal","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_CurrentAlarm_unknown","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_RxPowerWarning_highSet","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_RxPowerWarning_lowSet","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_RxPowerWarning_highClear","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_RxPowerWarning_lowClear","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_RxPowerWarning_normal","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_RxPowerWarning_unknown","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_RxPowerAlarm_highSet","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_RxPowerAlarm_lowSet","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_RxPowerAlarm_highClear","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_RxPowerAlarm_lowClear","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_RxPowerAlarm_normal","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_RxPowerAlarm_unknown","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TxPowerWarning_highSet","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TxPowerWarning_lowSet","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TxPowerWarning_highClear","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TxPowerWarning_lowClear","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TxPowerWarning_normal","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TxPowerWarning_unknown","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TxPowerAlarm_highSet","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TxPowerAlarm_lowSet","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TxPowerAlarm_highClear","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TxPowerAlarm_lowClear","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TxPowerAlarm_normal","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TxPowerAlarm_unknown","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TxFaultAlarm_highSet","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TxFaultAlarm_lowSet","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TxFaultAlarm_highClear","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TxFaultAlarm_lowClear","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TxFaultAlarm_normal","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-INTERFACE-XCVR-MONITOR-MIB-cIfXcvrMonStatusChangeNotif_TxFaultAlarm_unknown","2","1","0"}
}
default = {"Unknown","Unknown","Unknown"}
