###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
# Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
# - CISCO-PAE-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table cisco-CISCO-PAE-MIB_sev =
{
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeNoGuestVlanNotif_initialize","2","13","1800"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeNoGuestVlanNotif_disconnected","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeNoGuestVlanNotif_connecting","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeNoGuestVlanNotif_authenticating","2","13","1800"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeNoGuestVlanNotif_authenticated","2","13","1800"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeNoGuestVlanNotif_aborting","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeNoGuestVlanNotif_held","2","13","1800"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeNoGuestVlanNotif_forceAuth","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeNoGuestVlanNotif_forceUnauth","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeNoGuestVlanNotif_restart","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeNoGuestVlanNotif_unknown","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeNoAuthFailVlanNotif_initialize","2","13","1800"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeNoAuthFailVlanNotif_disconnected","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeNoAuthFailVlanNotif_connecting","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeNoAuthFailVlanNotif_authenticating","2","13","1800"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeNoAuthFailVlanNotif_authenticated","2","13","1800"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeNoAuthFailVlanNotif_aborting","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeNoAuthFailVlanNotif_held","2","13","1800"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeNoAuthFailVlanNotif_forceAuth","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeNoAuthFailVlanNotif_forceUnauth","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeNoAuthFailVlanNotif_restart","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeNoAuthFailVlanNotif_unknown","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeGuestVlanNotif_initialize","2","13","1800"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeGuestVlanNotif_disconnected","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeGuestVlanNotif_connecting","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeGuestVlanNotif_authenticating","2","13","1800"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeGuestVlanNotif_authenticated","2","13","1800"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeGuestVlanNotif_aborting","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeGuestVlanNotif_held","2","13","1800"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeGuestVlanNotif_forceAuth","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeGuestVlanNotif_forceUnauth","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeGuestVlanNotif_restart","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeGuestVlanNotif_unknown","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeAuthFailVlanNotif_initialize","2","13","1800"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeAuthFailVlanNotif_disconnected","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeAuthFailVlanNotif_connecting","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeAuthFailVlanNotif_authenticating","2","13","1800"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeAuthFailVlanNotif_authenticated","2","13","1800"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeAuthFailVlanNotif_aborting","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeAuthFailVlanNotif_held","2","13","1800"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeAuthFailVlanNotif_forceAuth","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeAuthFailVlanNotif_forceUnauth","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeAuthFailVlanNotif_restart","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-PAE-MIB-cpaeAuthFailVlanNotif_unknown","2","1","0"}
}
default = {"Unknown","Unknown","Unknown"}
