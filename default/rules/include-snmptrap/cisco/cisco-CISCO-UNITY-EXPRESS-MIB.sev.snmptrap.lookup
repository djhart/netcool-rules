###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 1.0 - Initial Release.
#
# Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
# - CISCO-UNITY-EXPRESS-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table cisco-CISCO-UNITY-EXPRESS-MIB_sev =
{
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressApplAlert_error","4","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressApplAlert_warning","2","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressApplAlert_informational","2","13","1800"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressApplAlert_unknown","2","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressStorageAlert_error","4","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressStorageAlert_warning","2","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressStorageAlert_informational","2","13","1800"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressStorageAlert_unknown","2","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressSecurityAlert_error","4","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressSecurityAlert_warning","2","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressSecurityAlert_informational","2","13","1800"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressSecurityAlert_unknown","2","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressCallMgrAlert_error","4","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressCallMgrAlert_warning","2","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressCallMgrAlert_informational","2","13","1800"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressCallMgrAlert_unknown","2","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressRescExhausted_error","4","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressRescExhausted_warning","2","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressRescExhausted_informational","2","13","1800"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressRescExhausted_unknown","2","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressBackupAlert_error","4","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressBackupAlert_warning","2","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressBackupAlert_informational","2","13","1800"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressBackupAlert_unknown","2","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressNTPAlert_error","4","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressNTPAlert_warning","2","1","0"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressNTPAlert_informational","2","13","1800"}, 
    {"SNMPTRAP-cisco-CISCO-UNITY-EXPRESS-MIB-ciscoUnityExpressNTPAlert_unknown","2","1","0"} 
}
default = {"Unknown","Unknown","Unknown"}

