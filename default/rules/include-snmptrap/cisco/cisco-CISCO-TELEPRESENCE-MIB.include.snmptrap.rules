###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-TELEPRESENCE-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.643": ### Cisco Telepresence System - Notifications from CISCO-TELEPRESENCE-MIB (200802130000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-TELEPRESENCE-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Cisco-Telepresence System"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### ctpPeripheralErrorNotification

            ##########
            # $1 = ctpPeripheralErrorIndex 
            # $2 = ctpPeripheralErrorStatus 
            ##########

            $ctpPeripheralErrorIndex = $1
            $ctpPeripheralErrorStatus = lookup($2, CtpPeripheralStatusCode)

            $OS_EventId = "SNMPTRAP-cisco-CISCO-TELEPRESENCE-MIB-ctpPeripheralErrorNotification"

            @AlertGroup = "Peripheral Error Status"

            $ctpPeripheralErrorHistoryIndex = extract($OID2, "\.([0-9]+)$")

            @AlertKey = "ctpPeripheralErrorHistoryEntry." + $ctpPeripheralErrorHistoryIndex 

            @Summary = "Peripheral Device: " + $ctpPeripheralErrorIndex

            switch($2)
            {
                case "0":### noError
                    $SEV_KEY = $OS_EventId + "_noError"
                    @Summary = @Summary + " is Functioning Through the Attached Port"

                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
					
                case "1":### other
                    $SEV_KEY = $OS_EventId + "_other"
                    @Summary = @Summary + " Has Other Issue"

                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
					
                case "2":### cableError
                    $SEV_KEY = $OS_EventId + "_cableError"
                    @Summary = @Summary + " Has Cabling Issue"

                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
					
                case "3":### powerError
                    $SEV_KEY = $OS_EventId + "_powerError"
                    @Summary = @Summary + " Has Power Issue"

                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
					
                case "4":### mgmtSysConfigError
                    $SEV_KEY = $OS_EventId + "_mgmtSysConfigError"
                    @Summary = @Summary + " Has Communications Management System Configuration Issue"

                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "5":### systemError
                    $SEV_KEY = $OS_EventId + "_systemError"
                    @Summary = "Telepresence System Error"

                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "6":### deviceError
                    $SEV_KEY = $OS_EventId + "_deviceError"
                    @Summary = @Summary + " is Attached But Not Fully Functional"

                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "7":### linkError
                    $SEV_KEY = $OS_EventId + "_linkError"
                    @Summary = @Summary + " Has Port Level Link Issue"

                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = @Summary + " Has Unknown Error"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            $ctpPeripheralErrorStatus = $ctpPeripheralErrorStatus + " ( " + $2 + " ) "

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($ctpPeripheralErrorIndex,$ctpPeripheralErrorStatus,$ctpPeripheralErrorHistoryIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "ctpPeripheralErrorIndex", $ctpPeripheralErrorIndex, "ctpPeripheralErrorStatus", $ctpPeripheralErrorStatus, "ctpPeripheralErrorHistoryIndex", $ctpPeripheralErrorHistoryIndex)

        case "2": ### ctpSysUserAuthFailNotification

            ##########
            # $1 = ctpSysUserAuthFailSourceAddrType 
            # $2 = ctpSysUserAuthFailSourceAddr 
            # $3 = ctpSysUserAuthFailSourcePort 
            # $4 = ctpSysUserAuthFailUserName 
            # $5 = ctpSysUserAuthFailAccessProtocol 
            ##########

            $ctpSysUserAuthFailSourceAddrType = lookup($1, InetAddressType)
            $ctpSysUserAuthFailSourceAddr = $2
            $ctpSysUserAuthFailSourcePort = $3
            $ctpSysUserAuthFailUserName = $4
            $ctpSysUserAuthFailAccessProtocol = lookup($5, CtpSystemAccessProtocol)

            $OS_EventId = "SNMPTRAP-cisco-CISCO-TELEPRESENCE-MIB-ctpSysUserAuthFailNotification"

            @AlertGroup = "User Authentication Status"

            $ctpSysUserAuthFailHistoryIndex = extract($OID2, "\.([0-9]+)$")

            @AlertKey = "ctpSysUserAuthFailHistoryEntry." + $ctpSysUserAuthFailHistoryIndex

            @Summary = "User Authentication: " + $ctpSysUserAuthFailUserName + " Failure via A Telepresence Supported Access Protocol: " + $ctpSysUserAuthFailAccessProtocol + " is Detected"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            $ctpSysUserAuthFailSourceAddrType = $ctpSysUserAuthFailSourceAddrType + " ( " + $1 + " ) "

            $ctpSysUserAuthFailAccessProtocol = $ctpSysUserAuthFailAccessProtocol + " ( " + $5 + " ) "

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($ctpSysUserAuthFailSourceAddrType,$ctpSysUserAuthFailSourceAddr,$ctpSysUserAuthFailSourcePort,$ctpSysUserAuthFailUserName,$ctpSysUserAuthFailAccessProtocol,$ctpSysUserAuthFailHistoryIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "ctpSysUserAuthFailSourceAddrType", $ctpSysUserAuthFailSourceAddrType, "ctpSysUserAuthFailSourceAddr", $ctpSysUserAuthFailSourceAddr, "ctpSysUserAuthFailSourcePort", $ctpSysUserAuthFailSourcePort,
                 "ctpSysUserAuthFailUserName", $ctpSysUserAuthFailUserName, "ctpSysUserAuthFailAccessProtocol", $ctpSysUserAuthFailAccessProtocol, "ctpSysUserAuthFailHistoryIndex", $ctpSysUserAuthFailHistoryIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-TELEPRESENCE-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-TELEPRESENCE-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-TELEPRESENCE-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-TELEPRESENCE-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-TELEPRESENCE-MIB.include.snmptrap.rules >>>>>")
