###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CHIPCOM-MIB
#
###############################################################################

case ".1.3.6.1.4.1.49": ### Notifications from CHIPCOM-MIB 

    log(DEBUG, "<<<<< Entering... chipcom-CHIPCOM-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Chipcom-CHIPCOM-MIB"
    @Class = "87005"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### chipHello

            ##########
            # $1 = sysObjectID
            ##########

            $sysObjectID = $1
            $OS_EventId = "SNMPTRAP-chipcom-CHIPCOM-MIB-chipHello"

            @AlertGroup = "Chip Status"
            @AlertKey = $sysObjectID
            @Summary = "ChipHello Message Sent"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_chipcom, "1")) {
                details($sysObjectID)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "sysObjectID", $sysObjectID)

        case "2": ### chipSlotDown

            ##########
            # $1 = olModSlotIndex
            ##########

            $olModSlotIndex = $1

            $OS_EventId = "SNMPTRAP-chipcom-CHIPCOM-MIB-chipSlotDown"

            @AlertGroup = "Chip Status"
            @AlertKey = "olModEntry." + $olModSlotIndex
            @Summary = "Module in the Indicated Slot Down"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_chipcom, "1")) {
                details($olModSlotIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "olModSlotIndex", $olModSlotIndex)

        case "3": ### chipSlotUp

            ##########
            # $1 = olModSlotIndex
            ##########

            $olModSlotIndex = $1

            $OS_EventId = "SNMPTRAP-chipcom-CHIPCOM-MIB-chipSlotUp"

            @AlertGroup = "Chip Status"
            @AlertKey = "olModEntry." + $olModSlotIndex
            @Summary = "Module in the Indicated Slot Up"

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_chipcom, "1")) {
                details($olModSlotIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "olModSlotIndex", $olModSlotIndex)

        case "4": ### chipEnvironment


            @AlertGroup = "Chip Environment Status"
            @AlertKey = ""
            @Summary = "Concentrator's Environment Changed"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap


        case "5": ### chipHardware


            $OS_EventId = "SNMPTRAP-chipcom-CHIPCOM-MIB-chipHardware"

            @AlertGroup = "Chip Module Status"
            @AlertKey = ""
            @Summary = "Soft Hardware Failure in Module Detected"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap


        case "6": ### chipSoftware


            $OS_EventId = "SNMPTRAP-chipcom-CHIPCOM-MIB-chipSoftware"

            @AlertGroup = "Chip Module Status"
            @AlertKey = ""
            @Summary = "Soft Software Failure Detected"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap


        case "7": ### chipChange


            $OS_EventId = "SNMPTRAP-chipcom-CHIPCOM-MIB-chipChange"

            @AlertGroup = "Chip Module Status"
            @AlertKey = ""
            @Summary = "Module Configuration Changed"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap


        case "8": ### chipFatal


            $OS_EventId = "SNMPTRAP-chipcom-CHIPCOM-MIB-chipFatal"

            @AlertGroup = "Chip Module Status"
            @AlertKey = ""
            @Summary = "Fatal Error in Module Detected"

            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap


        case "9": ### chipTrunkDown

            ##########
            # $1 = olTrunkSlotIndex
            # $2 = olTrunkIndex
            # $3 = olTrunkAdminState
            # $4 = olTrunkStatus
            ##########

            $olTrunkSlotIndex = $1
            $olTrunkIndex = $2
            $olTrunkAdminState = lookup($3, olTrunkAdminState) + " ( " + $3 + " )"
            $olTrunkStatus = $4

            $OS_EventId = "SNMPTRAP-chipcom-CHIPCOM-MIB-chipTrunkDown"

            @AlertGroup = "ChipTrunk Status"
            @AlertKey = "olTrunkEntry." + $olTrunkSlotIndex + "." + $olTrunkIndex
            @Summary = "Trunk's Status Changed to Error Condition"

            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_chipcom, "1")) {
                details($olTrunkSlotIndex,$olTrunkIndex,$olTrunkAdminState,$olTrunkStatus)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "olTrunkSlotIndex", $olTrunkSlotIndex, "olTrunkIndex", $olTrunkIndex, "olTrunkAdminState", $olTrunkAdminState,
                 "olTrunkStatus", $olTrunkStatus)

        case "10": ### chipTrunkUp

            ##########
            # $1 = olTrunkSlotIndex
            # $2 = olTrunkIndex
            # $3 = olTrunkAdminState
            # $4 = olTrunkStatus
            ##########

            $olTrunkSlotIndex = $1
            $olTrunkIndex = $2
            $olTrunkAdminState = lookup($3, olTrunkAdminState) + " ( " + $3 + " )"
            $olTrunkStatus = $4

            $OS_EventIid = "SNMPTRAP-chipcom-CHIPCOM-MIB-chipTrunkUp"

            @AlertGroup = "ChipTrunk Status"
            @AlertKey = "olTrunkEntry." + $olTrunkSlotIndex + "." + $olTrunkIndex
            @Summary = "Trunk's Status Changed to Non-Error Condition"

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_chipcom, "1")) {
                details($olTrunkSlotIndex,$olTrunkIndex,$olTrunkAdminState,$olTrunkStatus)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "olTrunkSlotIndex", $olTrunkSlotIndex, "olTrunkIndex", $olTrunkIndex, "olTrunkAdminState", $olTrunkAdminState,
                 "olTrunkStatus", $olTrunkStatus)

        case "11": ### chipPortDown

            ##########
            # $1 = olPortSlotIndex
            # $2 = olPortIndex
            # $3 = olPortAdminState
            # $4 = olPortStatus
            ##########

            $olPortSlotIndex = $1
            $olPortIndex = $2
            $olPortAdminState = lookup($3, olPortAdminState) + " ( " + $3 + " )"
            $olPortStatus = $4

            $OS_EventId = "SNMPTRAP-chipcom-CHIPCOM-MIB-chipPortDown"

            @AlertGroup = "ChipPort Status"
            @AlertKey = "olPortEntry." + $olPortSlotIndex + "." +$olPortIndex
            @Summary = "Port's Status Changed to Error Condition"

            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_chipcom, "1")) {
                details($olPortSlotIndex,$olPortIndex,$olPortAdminState,$olPortStatus)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "olPortSlotIndex", $olPortSlotIndex, "olPortIndex", $olPortIndex, "olPortAdminState", $olPortAdminState,
                 "olPortStatus", $olPortStatus)

        case "12": ### chipPortUp

            ##########
            # $1 = olPortSlotIndex
            # $2 = olPortIndex
            # $3 = olPortAdminState
            # $4 = olPortStatus
            ##########

            $olPortSlotIndex = $1
            $olPortIndex = $2
            $olPortAdminState = lookup($3, olPortAdminState) + " ( " + $3 + " )"
            $olPortStatus = $4

            $OS_EventId = "SNMPTRAP-chipcom-CHIPCOM-MIB-chipPortUp"

            @AlertGroup = "ChipPort Status"
            @AlertKey = "olPortEntry." + $olPortSlotIndex + "." +$olPortIndex
            @Summary = "Port's Status Changed to Non-Error Condition"

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_chipcom, "1")) {
                details($olPortSlotIndex,$olPortIndex,$olPortAdminState,$olPortStatus)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "olPortSlotIndex", $olPortSlotIndex, "olPortIndex", $olPortIndex, "olPortAdminState", $olPortAdminState,
                 "olPortStatus", $olPortStatus)

        case "13": ### chipPing

            ##########
            # $1 = chipEchoAddr
            # $2 = chipEchoNumber
            # $3 = chipEchoResponseCounts
            ##########

            $chipEchoAddr = $1
            $chipEchoNumber = $2
            $chipEchoResponseCounts = $3

            $OS_EventId = "SNMPTRAP-chipcom-CHIPCOM-MIB-chipPing"

            @AlertGroup = "Ping Status"
            @AlertKey = "Addr : " + $chipEchoAddr
            @Summary = "SNMP Initiated PING Command Details"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_chipcom, "1")) {
                details($chipEchoAddr,$chipEchoNumber,$chipEchoResponseCounts)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "chipEchoAddr", $chipEchoAddr, "chipEchoNumber", $chipEchoNumber, "chipEchoResponseCounts", $chipEchoResponseCounts)

        case "14": ### chipAboveThreshd


            $OS_EventId = "SNMPTRAP-chipcom-CHIPCOM-MIB-chipAboveThreshd"

            @AlertGroup = "Gauge Variable Threshold"
            @AlertKey = ""
            @Summary = "Gauge Variable Exceeded its threshold"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap


        case "15": ### chipBelowThreshd


            $OS_EventId = "SNMPTRAP-chipcom-CHIPCOM-MIB-chipBelowThreshd"

            @AlertGroup = "Gauge Variable Threshold"
            @AlertKey = ""
            @Summary = "Gauge Variable Exceeded its threshold"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap


        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_chipcom, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, chipcom-CHIPCOM-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, chipcom-CHIPCOM-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/chipcom/chipcom-CHIPCOM-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/chipcom/chipcom-CHIPCOM-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... chipcom-CHIPCOM-MIB.include.snmptrap.rules >>>>>")
