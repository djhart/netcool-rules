###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 1.2 - Updated Release.  
#             Added support of additional Foundry Products
#
#        Compatible with: 
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  Foundry Networks  NetIron XMR/MLX 3.1.00
#          -  Foundry Networks  NetIron IMR 3.0.00
#          -  Foundry Networks  BigIron RX 2.2.01
#          -  Foundry Networks  FastIron SuperX, FESX, FWSX 2.5.00
#          -  Foundry Networks  FastIron GS 2.4.00
#          -  Foundry Networks  ServerIron 450/850 9.5.00
#          -  Foundry Networks  ServerIron 100/400/800/GTE 9.4.00
#          -  Foundry Networks  IronPoint 1.4.01
#
#        Supports following features introduced in NCiL 2.0:
#
#          - "Advanced" and "User" include files
#          - "Severity" lookup tables
#
# 1.1 - Fixed a bug which caused generic traps from any enterprise whose
#       enterprise ID started with 1991 being handled as traps from Foundry.
#
# 1.0 - Initial Release compatible with snmp.rules 2.0.
#
###############################################################################

else if (nmatch($enterprise, ".1.3.6.1.4.1.1991."))
{
    @Agent = "Generic-Foundry Networks"
    @Class = "40660"
    
    switch($generic-trap)
    {
        case "2"|"3": ### linkDown, linkUp
                            
            ##########
            # $1 = ifIndex - A unique value for each interface. Its value
            #        ranges between 1 and the value of ifNumber. The value for
            #        each interface must remain constant at least from one
            #        re-initialization of the entity's network management
            #        system to the next re-initialization.
            # $2 = ifDescr - A textual string containing information about the
            #        interface. This string should include the name of the
            #        manufacturer, the product name and the version of the
            #        hardware interface.
            ##########
                            
            $ifIndex = $1
            $ifDescr = $2
            if(match($OPTION_EnableDetails, "1")) {
                details($ifIndex,$ifDescr)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "ifIndex", $ifIndex, "ifDescr", $ifDescr)
                
            @Summary = @Summary + "  ( " + $2 + " )"
        
        default:
    }
    
    ### The following statement adds the Foundry Model information to the Agent Field
    
    @Agent = @Agent + " " + lookup(extract($enterprise, "\.1\.3\.6\.1\.4\.1\.1991\.1\.3\.(.*)"), foundryProducts)
}
