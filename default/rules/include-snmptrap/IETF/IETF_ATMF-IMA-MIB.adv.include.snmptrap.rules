###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  IETF_ATMF-IMA-MIB
#
###############################################################################

log(DEBUG, "<<<<< Entering... IETF_ATMF-IMA-MIB.adv.include.snmptrap.rules >>>>>")

switch($specific-trap)
{
    case "1": ### imaFailureAlarm

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "imaFailureAlarm"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "ifEntry." + $1
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "2": ### imaGroupUnavailSecsCrossing

        $OS_X733EventType = 2
        $OS_X733ProbableCause = 2005
        $OS_X733SpecificProb = "imaGroupUnavailSecsCrossing"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "imaGroupCurrentEntry." + $imaGroupIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "3": ### imaGroupNeNumFailuresCrossing

        $OS_X733EventType = 2
        $OS_X733ProbableCause = 2005
        $OS_X733SpecificProb = "imaGroupNeNumFailuresCrossing"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "imaGroupCurrentEntry." + $imaGroupIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "4": ### imaGroupFeNumFailuresCrossing

        $OS_X733EventType = 2
        $OS_X733ProbableCause = 2005
        $OS_X733SpecificProb = "imaGroupFeNumFailuresCrossing"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "imaGroupCurrentEntry." + $imaGroupIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "5": ### imaLinkImaViolationsCrossing

        $OS_X733EventType = 2
        $OS_X733ProbableCause = 2005
        $OS_X733SpecificProb = "imaLinkImaViolationsCrossing"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "imaLinkCurrentEntry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "6": ### imaLinkOifAnomaliesCrossing

        $OS_X733EventType = 2
        $OS_X733ProbableCause = 2005
        $OS_X733SpecificProb = "imaLinkOifAnomaliesCrossing"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "imaLinkCurrentEntry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "7": ### imaLinkNeSevErroredSecsCrossing

        $OS_X733EventType = 2
        $OS_X733ProbableCause = 2005
        $OS_X733SpecificProb = "imaLinkNeSevErroredSecsCrossing"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "imaLinkCurrentEntry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "8": ### imaLinkFeSevErroredSecsCrossing

        $OS_X733EventType = 2
        $OS_X733ProbableCause = 2005
        $OS_X733SpecificProb = "imaLinkFeSevErroredSecsCrossing"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "imaLinkCurrentEntry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "9": ### imaLinkNeUnavailSecsCrossing

        $OS_X733EventType = 2
        $OS_X733ProbableCause = 2005
        $OS_X733SpecificProb = "imaLinkNeUnavailSecsCrossing"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "imaLinkCurrentEntry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "10": ### imaLinkFeUnavailSecsCrossing

        $OS_X733EventType = 2
        $OS_X733ProbableCause = 2005
        $OS_X733SpecificProb = "imaLinkFeUnavailSecsCrossing"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "imaLinkCurrentEntry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "11": ### imaLinkNeTxUnusableSecsCrossing

        $OS_X733EventType = 2
        $OS_X733ProbableCause = 2005
        $OS_X733SpecificProb = "imaLinkNeTxUnusableSecsCrossing"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "imaLinkCurrentEntry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "12": ### imaLinkNeRxUnusableSecsCrossing

        $OS_X733EventType = 2
        $OS_X733ProbableCause = 2005
        $OS_X733SpecificProb = "imaLinkNeRxUnusableSecsCrossing"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "imaLinkCurrentEntry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "13": ### imaLinkFeTxUnusableSecsCrossing

        $OS_X733EventType = 2
        $OS_X733ProbableCause = 2005
        $OS_X733SpecificProb = "imaLinkFeTxUnusableSecsCrossing"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "imaLinkCurrentEntry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "14": ### imaLinkFeRxUnusableSecsCrossing

        $OS_X733EventType = 2
        $OS_X733ProbableCause = 2005
        $OS_X733SpecificProb = "imaLinkFeRxUnusableSecsCrossing"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "imaLinkCurrentEntry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "15": ### imaLinkNeTxNumFailuresCrossing

        $OS_X733EventType = 2
        $OS_X733ProbableCause = 2005
        $OS_X733SpecificProb = "imaLinkNeTxNumFailuresCrossing"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "imaLinkCurrentEntry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "16": ### imaLinkNeRxNumFailuresCrossing

        $OS_X733EventType = 2
        $OS_X733ProbableCause = 2005
        $OS_X733SpecificProb = "imaLinkNeRxNumFailuresCrossing"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "imaLinkCurrentEntry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "17": ### imaLinkFeTxNumFailuresCrossing

        $OS_X733EventType = 2
        $OS_X733ProbableCause = 2005
        $OS_X733SpecificProb = "imaLinkFeTxNumFailuresCrossing"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "imaLinkCurrentEntry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "18": ### imaLinkFeRxNumFailuresCrossing

        $OS_X733EventType = 2
        $OS_X733ProbableCause = 2005
        $OS_X733SpecificProb = "imaLinkFeRxNumFailuresCrossing"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "imaLinkCurrentEntry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    default:
}

log(DEBUG, "<<<<< Leaving... IETF_ATMF-IMA-MIB.adv.include.snmptrap.rules >>>>>")
