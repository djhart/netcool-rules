###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 2.0 - Trap(s) added
#
#         - ospfNssaTranslatorStatusChange
#         - ospfRestartStatusChange
#         - ospfNbrRestartHelperStatusChange
#         - ospfVirtNbrRestartHelperStatusChange
#
# 1.1 - Added basic debug logging.
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  OSPF-TRAP-MIB
#
###############################################################################

log(DEBUG, "<<<<< Entering... IETF-OSPF-TRAP-MIB.adv.include.snmptrap.rules >>>>>")

switch($specific-trap)
{
    case "1": ### ospfVirtIfStateChange

        $OS_X733SpecificProb = "ospfVirtIfStateChange"
        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1011
        $OS_OsiLayer = 3
        
        $OS_LocalPriObj = "ospfVirtIfEntry." + $2 + "." + $3
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
        $OS_RemoteNodeAlias = $3
            
    case "2": ### ospfNbrStateChange

        $OS_X733SpecificProb = "ospfNbrStateChange"
        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_OsiLayer = 3
        
        $OS_LocalPriObj = "ospfNbrEntry." + $2 + "." + $3
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
        $OS_RemoteNodeAlias = $2
        if(!match($3, "0"))
        {
            $OS_RemotePriObj = "ifEntry." + $3
            $OS_RemoteRootObj = $OS_LocalPriObj
        }
        
    case "3": ### ospfVirtNbrStateChange

        $OS_X733SpecificProb = "ospfVirtNbrStateChange"
        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_OsiLayer = 3
        
        $OS_LocalPriObj = "ospfVirtNbrEntry." + $2 + "." + $3
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
        $OS_RemoteNodeAlias = $3
        
    case "4": ### ospfIfConfigError

        $OS_X733SpecificProb = "ospfIfConfigError"
        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_OsiLayer = 3
        
        $OS_LocalPriObj = "ospfIfEntry." + $2 + "." + $3
        if(match($3, "0"))
        {
            $OS_LocalNodeAlias = $2
            $OS_LocalRootObj = "ospfIfEntry." + $2 + "." + $3
            $VAR_RelateLRO2LPO = 1
            $VAR_RelateLRO2LSO = 0
            $VAR_RelateLSO2LPO = 0
        }
        else
        {
            $OS_LocalRootObj = "ifEntry." + $3
            $VAR_RelateLRO2LPO = 2
            $VAR_RelateLRO2LSO = 0
            $VAR_RelateLSO2LPO = 0
        }
            
    case "5": ### ospfVirtIfConfigError

        $OS_X733SpecificProb = "ospfVirtIfConfigError"
        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_OsiLayer = 3
        
        $OS_LocalPriObj = "ospfVirtIfEntry." + $2 + "." + $3
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
        $OS_RemoteNodeAlias = $3
            
    case "6": ### ospfIfAuthFailure

        $OS_X733SpecificProb = "ospfIfAuthFailure"
        $OS_X733EventType = 9
        $OS_X733ProbableCause = 100168
        $OS_OsiLayer = 3
        
        $OS_LocalPriObj = "ospfIfEntry." + $2 + "." + $3
        
        if(match($3, "0"))
        {
            $OS_LocalNodeAlias = $2
            $OS_LocalRootObj = "ospfIfEntry." + $2 + "." + $3
            $VAR_RelateLRO2LPO = 1
            $VAR_RelateLRO2LSO = 0
            $VAR_RelateLSO2LPO = 0
        }
        else
        {
            $OS_LocalRootObj = "ifEntry." + $3
            $VAR_RelateLRO2LPO = 2
            $VAR_RelateLRO2LSO = 0
            $VAR_RelateLSO2LPO = 0
        }
            
    case "7": ### ospfVirtIfAuthFailure

        $OS_X733SpecificProb = "ospfVirtIfAuthFailure"
        $OS_X733EventType = 9
        $OS_X733ProbableCause = 100168
        $OS_OsiLayer = 3
        
        $OS_LocalPriObj = "ospfVirtIfEntry." + $2 + "." + $3
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
        $OS_RemoteNodeAlias = $3
            
    case "8": ### ospfIfRxBadPacket

        $OS_X733SpecificProb = "ospfIfRxBadPacket"
        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1013
        $OS_OsiLayer = 3
        
        $OS_LocalPriObj = "ospfIfEntry." + $2 + "." + $3
        if(match($3, "0"))
        {
            $OS_LocalNodeAlias = $2
            $OS_LocalRootObj = "ospfIfEntry." + $2 + "." + $3
            $VAR_RelateLRO2LPO = 1
            $VAR_RelateLRO2LSO = 0
            $VAR_RelateLSO2LPO = 0
        }
        else
        {
            $OS_LocalRootObj = "ifEntry." + $3
            $VAR_RelateLRO2LPO = 2
            $VAR_RelateLRO2LSO = 0
            $VAR_RelateLSO2LPO = 0
        }
            
    case "9": ### ospfVirtIfRxBadPacket

        $OS_X733SpecificProb = "ospfVirtIfRxBadPacket"
        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1013
        $OS_OsiLayer = 3
        
        $OS_LocalPriObj = "ospfVirtIfEntry." + $2 + "." + $3
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
        $OS_RemoteNodeAlias = $3
            
    case "10": ### ospfTxRetransmit

        $OS_X733SpecificProb = "ospfTxRetransmit"
        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1014
        $OS_OsiLayer = 3
        
        $OS_LocalPriObj = "ospfIfEntry." + $2 + "." + $3
        if(match($3, "0"))
        {
            $OS_LocalNodeAlias = $2
            $OS_LocalRootObj = "ospfIfEntry." + $2 + "." + $3
            $VAR_RelateLRO2LPO = 1
            $VAR_RelateLRO2LSO = 0
            $VAR_RelateLSO2LPO = 0
        }
        else
        {
            $OS_LocalRootObj = "ifEntry." + $3
            $VAR_RelateLRO2LPO = 2
            $VAR_RelateLRO2LSO = 0
            $VAR_RelateLSO2LPO = 0
        }
        $OS_RemoteNodeAlias = $4
            
    case "11": ### ospfVirtIfTxRetransmit

        $OS_X733SpecificProb = "ospfVirtIfTxRetransmit"
        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1014
        $OS_OsiLayer = 3
        
        $OS_LocalPriObj = "ospfVirtIfEntry." + $2 + "." + $3
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
        $OS_RemoteNodeAlias = $3
            
    case "12": ### ospfOriginateLsa
    
        $OS_X733SpecificProb = "ospfOriginateLsa"
        $OS_X733EventType = 0
        $OS_X733ProbableCause = 0
        $OS_OsiLayer = 3
        
        $OS_LocalPriObj = "ospfLsdbEntry." + $2 + "." + $3 + "." + $4 + "." + $5
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
            
    case "13": ### ospfMaxAgeLsa

        $OS_X733SpecificProb = "ospfMaxAgeLsa"
        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_OsiLayer = 3
        
        $OS_LocalPriObj = "ospfLsdbEntry." + $2 + "." + $3 + "." + $4 + "." + $5
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
            
    case "14": ### ospfLsdbOverflow

        $OS_X733SpecificProb = "ospfLsdbOverflow"
        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_OsiLayer = 3
        
    case "15": ### ospfLsdbApproachingOverflow

        $OS_X733SpecificProb = "ospfLsdbApproachingOverflow"
        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_OsiLayer = 3
        
    case "16": ### ospfIfStateChange

        $OS_X733SpecificProb = "ospfIfStateChange"
        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_OsiLayer = 3
        
        $OS_LocalPriObj = "ospfIfEntry." + $2 + "." + $3
        if(match($3, "0"))
        {
            $OS_LocalNodeAlias = $2
            $OS_LocalRootObj = "ospfIfEntry." + $2 + "." + $3
            $VAR_RelateLRO2LPO = 1
            $VAR_RelateLRO2LSO = 0
            $VAR_RelateLSO2LPO = 0
        }
        else
        {
            $OS_LocalRootObj = "ifEntry." + $3
            $VAR_RelateLRO2LPO = 2
            $VAR_RelateLRO2LSO = 0
            $VAR_RelateLSO2LPO = 0
        }
            
    case "17": ### ospfNssaTranslatorStatusChange

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ospfNssaTranslatorStatusChange"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ospfAreaEntry." + $2
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "18": ### ospfRestartStatusChange

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ospfRestartStatusChange"
        $OS_OsiLayer = 0

    case "19": ### ospfNbrRestartHelperStatusChange

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ospfNbrRestartHelperStatusChange"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ospfNbrEntry." + $2 + "." + $3
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

        $OS_RemoteNodeAlias = $2
        if(!match($3, "0"))
        {
            $OS_RemotePriObj = "ifEntry." + $3
            $OS_RemoteRootObj = $OS_LocalPriObj
        }

    case "20": ### ospfVirtNbrRestartHelperStatusChange

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ospfVirtNbrRestartHelperStatusChange"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ospfVirtNbrEntry." + $2 + "." + $3
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

        $OS_RemoteNodeAlias = $3

    default:
}

log(DEBUG, "<<<<< Leaving... IETF-OSPF-TRAP-MIB.adv.include.snmptrap.rules >>>>>")
