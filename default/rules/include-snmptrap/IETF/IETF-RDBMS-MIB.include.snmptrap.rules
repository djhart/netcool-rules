###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  IETF-RDBMS-MIB
#
###############################################################################

case ".1.3.6.1.2.1.39.2": ### Relational Databases - Notifications from RDBMS-MIB (RFC1697)

    log(DEBUG, "<<<<< Entering... IETF-RDBMS-MIB.include.snmptrap.rules >>>>>")

    @Agent = "IETF-RDBMS-MIB"
    @Class = "40086"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### rdbmsStateChange

            ##########
            # $1 = rdbmsRelState
            ##########

            $rdbmsRelState = lookup($1, rdbmsRelState) + " ( " + $1 + " )"
            $rdbmsDbIndex = extract($OID1, "\.([0-9]+)\.[0-9]+$")
            $applIndex = extract($OID1, "\.([0-9]+)$")
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($rdbmsRelState,$rdbmsDbIndex,$applIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "rdbmsRelState", $rdbmsRelState, "rdbmsDbIndex", $rdbmsDbIndex, "applIndex", $applIndex)

            $OS_EventId = "SNMPTRAP-IETF-RDBMS-MIB-rdbmsStateChange"

            @AlertGroup = "Database Status"
            @AlertKey = "rdbmsRelEntry." + $rdbmsDbIndex + "." + $applIndex
            switch($1)
            {
                case "1": ### other - The database/server is in some other condition, possibly described in the vendor private MIB.
                    @Summary = "Database Status: Other"
                    
                    $SEV_KEY = $OS_EventId + "_other"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "2": ### active - The server is actively using the database.
                    @Summary = "Database Active"
                    
                    $SEV_KEY = $OS_EventId + "_active"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0

                case "3": ### available - The server could use the database if necessary.
                    @Summary = "Database Available"
                    
                    $SEV_KEY = $OS_EventId + "_available"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0

                case "4": ### restricted - The database is in some administratively determined state of less-than-complete availability.
                    @Summary = "Database Restricted, Administratively Unavailable"
                    
                    $SEV_KEY = $OS_EventId + "_restricted"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "5": ### unavailable - The database is not available through this server.
                    @Summary = "Database Unavailable"
                    
                    $SEV_KEY = $OS_EventId + "_unavailable"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    @Summary = "Database Status Unknown"
                    
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

            }
            @Summary = @Summary + "  ( " + @AlertKey + " )"
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $1

        case "2": ### rdbmsOutOfSpace

            ##########
            # $1 = rdbmsSrvInfoDiskOutOfSpaces
            ##########

            $rdbmsSrvInfoDiskOutOfSpaces = $1
            $applIndex = extract($OID1, "\.([0-9]+)$")
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($rdbmsSrvInfoDiskOutOfSpaces,$applIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "rdbmsSrvInfoDiskOutOfSpaces", $rdbmsSrvInfoDiskOutOfSpaces, "applIndex", $applIndex)

            $OS_EventId = "SNMPTRAP-IETF-RDBMS-MIB-rdbmsOutOfSpace"

            @AlertGroup = "Disk Space Availability"
            @AlertKey = "rdbmsSrvInfoEntry." + $applIndex
            @Summary = "Database Server Unable to Allocate Disk Space"
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, IETF-RDBMS-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, IETF-RDBMS-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/IETF/IETF-RDBMS-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/IETF/IETF-RDBMS-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... IETF-RDBMS-MIB.include.snmptrap.rules >>>>>")
