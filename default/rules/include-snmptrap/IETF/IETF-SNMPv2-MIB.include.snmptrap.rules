###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  SNMPv2-MIB
#
###############################################################################

# Commented out line below - main entry point is IETF-IF-MIB.include.snmptrap.rules
#case ".1.3.6.1.6.3.1.1.5": ### - Notifications from SNMPv2-MIB (200210160000Z)

log(DEBUG, "<<<<< Entering... IETF-SNMPv2-MIB.include.snmptrap.rules >>>>>")

    @Agent = "IETF-SNMPv2-MIB"
    @Class = "40086"
    
    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
     
        case "1": ### coldStart
        
            ##########
            ##########
            
            
            $OS_EventId = "SNMPTRAP-IETF-SNMPv2-MIB-coldStart"

            @AlertGroup = "Initialize-Configuration Status"
            @Summary = "SNMP entity, Supporting a Notification Originator application, is Reinitializing Itself and that Its Configuration May have been Altered" 
            
            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            @Identifier = @Node + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
         
        case "2": ### warmStart
        
            ##########
            ##########
            
            
            $OS_EventId = "SNMPTRAP-IETF-SNMPv2-MIB-warmStart"

            @AlertGroup = "Initialize-Configuration Status"
            @Summary = "SNMP Entity, Supporting a Notification Originator Application, is Reinitializing Itself such that its Configuration is Unaltered"
            
            $DEFAULT_Severity = 5
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            @Identifier = @Node + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
         
        case "5": ### authenticationFailure
        
            ##########
            ##########
            
            
            $OS_EventId = "SNMPTRAP-IETF-SNMPv2-MIB-authenticationFailure"

            @AlertGroup = "Authentication Status"
            @Summary = "SNMP Entity has Received a Protocol Message that is Not Properly Authenticated" 
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            @Identifier = @Node + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
         
        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, IETF-SNMPv2-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, IETF-SNMPv2-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/IETF/IETF-SNMPv2-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/IETF/IETF-SNMPv2-MIB.user.include.snmptrap.rules"


##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... IETF-SNMPv2-MIB.include.snmptrap.rules >>>>>")


