###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  MPLS-VPN-MIB
#
###############################################################################
#
# 1.1 - Added basic debug logging.
#
###############################################################################

log(DEBUG, "<<<<< Entering... IETF-MPLS-VPN-MIB.adv.include.snmptrap.rules >>>>>")

switch($specific-trap)
{
    case "1": ### mplsVrfIfUp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1011
        $OS_X733SpecificProb = "mplsVrfIfUp"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "mplsVpnInterfaceConfEntry." + extract($OID1, "3\.118\.1\.2\.1\.1\.1\.(.*)$")
        $OS_LocalSecObj = "mplsVpnVrfEntry." + extract($OID1, "3\.118\.1\.2\.1\.1\.1\.(.*)\.[0-9]+$")
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 3
        $VAR_RelateLSO2LPO = 3

    case "2": ### mplsVrfIfDown

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1011
        $OS_X733SpecificProb = "mplsVrfIfDown"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "mplsVpnInterfaceConfEntry." + extract($OID1, "3\.118\.1\.2\.1\.1\.1\.(.*)$")
        $OS_LocalSecObj = "mplsVpnVrfEntry." + extract($OID1, "3\.118\.1\.2\.1\.1\.1\.(.*)\.[0-9]+$")
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 3
        $VAR_RelateLSO2LPO = 3

    case "3": ### mplsNumVrfRouteMidThreshExceeded

        $OS_X733EventType = 2
        $OS_X733ProbableCause = 2005
        $OS_X733SpecificProb = "mplsNumVrfRouteMidThreshExceeded"
        $OS_OsiLayer = 3
        
        $OS_LocalPriObj = "mplsVpnVrfEntry." + extract($OID1, "3\.118\.1\.2\.2\.1\.1\.(.*)$")
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "4": ### mplsNumVrfRouteMaxThreshExceeded

        $OS_X733EventType = 2
        $OS_X733ProbableCause = 2005
        $OS_X733SpecificProb = "mplsNumVrfRouteMaxThreshExceeded"
        $OS_OsiLayer = 3
        
        $OS_LocalPriObj = "mplsVpnVrfEntry." + extract($OID1, "3\.118\.1\.2\.2\.1\.1\.(.*)$")
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "5": ### mplsNumVrfSecIllegalLabelThreshExceeded

        $OS_X733EventType = 2
        $OS_X733ProbableCause = 2005
        $OS_X733SpecificProb = "mplsNumVrfSecIllegalLabelThreshExceeded"
        $OS_OsiLayer = 3
        
        $OS_LocalPriObj = "mplsVpnVrfEntry." + extract($OID1, "3\.118\.1\.2\.2\.1\.1\.(.*)$")
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    default:
}

log(DEBUG, "<<<<< Leaving... IETF-MPLS-VPN-MIB.adv.include.snmptrap.rules >>>>>")
