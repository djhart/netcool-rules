###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  POWER-ETHERNET-MIB
#
# 1.1 - Changed the format
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table IETF-POWER-ETHERNET-MIB_sev =
{
	    {"SNMPTRAP-IETF-POWER-ETHERNET-MIB-pethPsePortOnOffNotification_disabled","3","1","0"},
	    {"SNMPTRAP-IETF-POWER-ETHERNET-MIB-pethPsePortOnOffNotification_searching","2","1","0"},
	    {"SNMPTRAP-IETF-POWER-ETHERNET-MIB-pethPsePortOnOffNotification_deliveringPower","1","2","0"},
	    {"SNMPTRAP-IETF-POWER-ETHERNET-MIB-pethPsePortOnOffNotification_fault","2","1","0"},
	    {"SNMPTRAP-IETF-POWER-ETHERNET-MIB-pethPsePortOnOffNotification_test","2","12","0"},
	    {"SNMPTRAP-IETF-POWER-ETHERNET-MIB-pethPsePortOnOffNotification_otherFault","2","1","0"},
	    {"SNMPTRAP-IETF-POWER-ETHERNET-MIB-pethPsePortOnOffNotification_unknown","2","1","0"},
	    {"SNMPTRAP-IETF-POWER-ETHERNET-MIB-pethMainPowerUsageOnNotification","2","1","0"},
	    {"SNMPTRAP-IETF-POWER-ETHERNET-MIB-pethMainPowerUsageOffNotification","1","2","0"}
}
default = {"Unknown","Unknown","Unknown"}
