###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  IETF-IPV6-MIB
#
###############################################################################
table ipv6IfOperStatus =
{
    ##########
    # The current operational state of the interface. The noIfIdentifier(3)
    # state indicates that no valid Interface Identifier is assigned to the
    # interface. This state usually indicates that the link-local interface
    # address failed Duplicate Address Detection. If ipv6IfAdminStatus is
    # down(2) then ipv6IfOperStatus should be down(2). If ipv6IfAdminStatus is
    # changed to up(1) then ipv6IfOperStatus should change to up(1) if the
    # interface is ready to transmit and receive network traffic; it should
    # remain in the down(2) or noIfIdentifier(3) state if and only if there is
    # a fault that prevents it from going to the up(1) state; it should remain
    # in the notPresent(5) state if the interface has missing (typically,
    # lower layer) components.
    ##########
    
    {"1","Up"}, ### up - ready to pass packets
    {"2","Down"}, ### - down
    {"3","No Interface Identifier"}, ### noIfIdentifier - no interface identifier
    {"4","Unknown"}, ### unknown - status can not be determined for some reason
    {"5","Not Present"} ### notPresent - some component is missing
}
default = "Unknown"
