###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  UPS-MIB
#
# 1.1 - Changed the format
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table IETF-UPS-MIB_sev =
{
	    {"SNMPTRAP-IETF-UPS-MIB-upsTrapOnBattery","4","1","0"},
	    {"SNMPTRAP-IETF-UPS-MIB-upsTrapTestCompleted_donePass","1","2","0"},
	    {"SNMPTRAP-IETF-UPS-MIB-upsTrapTestCompleted_doneWarning","2","1","0"},
	    {"SNMPTRAP-IETF-UPS-MIB-upsTrapTestCompleted_doneError","3","1","0"},
	    {"SNMPTRAP-IETF-UPS-MIB-upsTrapTestCompleted_aborted","2","1","0"},
	    {"SNMPTRAP-IETF-UPS-MIB-upsTrapTestCompleted_inProgress","2","1","0"},
	    {"SNMPTRAP-IETF-UPS-MIB-upsTrapTestCompleted_noTestsInitiated","2","1","0"},
            {"SNMPTRAP-IETF-UPS-MIB-upsTrapTestCompleted_unknown","2","1","0"},
	    {"SNMPTRAP-IETF-UPS-MIB-upsTrapAlarmEntryAdded","3","1","0"},
	    {"SNMPTRAP-IETF-UPS-MIB-upsTrapAlarmEntryRemoved","1","2","0"}
}
default = {"Unknown","Unknown","Unknown"}
