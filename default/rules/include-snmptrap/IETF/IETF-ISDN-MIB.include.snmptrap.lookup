###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  ISDN-MIB
#
###############################################################################
#
# 1.2 - Improved readability of lookup table values.
#
# 1.1 - Added isdnLapdOperStatus table.
#
###############################################################################

table isdnBearerOperStatus =
{
    {"1","Idle"}, ### idle - The B channel is idle. No call or call attempt is going on.
    {"2","Connecting"}, ### connecting - A connection attempt (outgoing call) is being made on this interface.
    {"3","Connected"}, ### connected - An incoming call is in the process of validation.
    {"4","Active"} ### active - A call is active on this interface.
}
default = "Unknown"

table isdnBearerInfoType =
{
    {"1","Unknown"}, ### unknown
    {"2","Speech"}, ### speech
    {"3","Q.931 Unrestricted Digital"}, ### unrestrictedDigital - as defined in Q.931
    {"4","Unrestricted Digital w/56k Rate Adaption"}, ### unrestrictedDigital56 - with 56k rate adaption
    {"5","Restricted Digital"}, ### restrictedDigital
    {"6","3.1kHz Audio"}, ### audio31 - 3.1 kHz audio
    {"7","7kHz Audio"}, ### audio7 - 7 kHz audio
    {"8","Video"}, ### video
    {"9","Packet Switched"} ### packetSwitched
}
default = "Unknown"

table isdnBearerCallOrigin =
{
    {"1","Unknown"}, ### unknown
    {"2","Outbound"}, ### originate
    {"3","Inbound"}, ### answer
    {"4","Callback"} ### callback
}
default = "Unknown"

table isdnLapdOperStatus =
{
    {"1","Inactive"}, ### inactive - all layers are inactive
    {"2","Layer-1 Active, Layer-2 Datalink Not Established"}, ### l1Active - layer 1 is activated, layer 2 datalink not established.
    {"3","Layer-1 Active, Layer-2 Datalink Established"} ### l2Active - layer 1 is activated, layer 2 datalink established.
}
default = "Unknown"
