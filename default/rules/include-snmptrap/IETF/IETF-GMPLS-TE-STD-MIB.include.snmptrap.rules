###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  GMPLS-TE-STD-MIB
#
###############################################################################

case ".1.3.6.1.2.1.10.166.13": ###  - Notifications from GMPLS-TE-STD-MIB (200702270000Z)

    log(DEBUG, "<<<<< Entering... IETF-GMPLS-TE-STD-MIB.include.snmptrap.rules >>>>>")

    @Agent = "IETF-GMPLS-TE-STD-MIB"
    @Class = "40086"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### gmplsTunnelDown

            ##########
            # $1 = mplsTunnelAdminStatus 
            # $2 = mplsTunnelOperStatus 
            # $3 = gmplsTunnelErrorLastErrorType 
            # $4 = gmplsTunnelErrorReporterType 
            # $5 = gmplsTunnelErrorReporter 
            # $6 = gmplsTunnelErrorCode 
            # $7 = gmplsTunnelErrorSubcode 
            ##########

            $mplsTunnelAdminStatus = lookup($1, mplsTunnelAdminStatus)
            $mplsTunnelOperStatus = lookup($2, mplsTunnelOperStatus)
            $gmplsTunnelErrorLastErrorType = lookup($3, gmplsTunnelErrorLastErrorType)
            $gmplsTunnelErrorReporterType = lookup($4, InetAddressType)
            $gmplsTunnelErrorReporter = $5
            $gmplsTunnelErrorCode = $6
            $gmplsTunnelErrorSubcode = $7

            $OS_EventId = "SNMPTRAP-IETF-GMPLS-TE-STD-MIB-gmplsTunnelDown"

            @AlertGroup = "GMPLS Tunnel Status"
            $mplsTunnelIndex = extract($OID1, "3\.6\.1\.2\.1\.10\.166\.3\.2\.2\.1\.34\.([0-9]+)\.")
            $mplsTunnelInstance = extract($OID1, "3\.6\.1\.2\.1\.10\.166\.3\.2\.2\.1\.34\.[0-9]+\.([0-9]+)\.")
            if(regmatch($OID1, "3\.6\.1\.2\.1\.10\.166\.3\.2\.2\.1\.34\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$"))
            {
                $mplsTunnelIngressLSRId = extract($OID1, "3\.6\.1\.2\.1\.10\.166\.3\.2\.2\.1\.34\.[0-9]+\.[0-9]+\.([0-9]+)\.[0-9]+$")

                $Integer = $mplsTunnelIngressLSRId
                include "$NC_RULES_HOME/include-snmptrap/decodeInteger2Ip.include.snmptrap.rules"
                $mplsTunnelIngressLSRId = $IPv4addr

                $mplsTunnelEgressLSRId = extract($OID1, "3\.6\.1\.2\.1\.10\.166\.3\.2\.2\.1\.34\.[0-9]+\.[0-9]+\.[0-9]+\.([0-9]+)$")

                $Integer = $mplsTunnelEgressLSRId
                include "$NC_RULES_HOME/include-snmptrap/decodeInteger2Ip.include.snmptrap.rules"
                $mplsTunnelEgressLSRId = $IPv4addr
            }
            else
            {
                $mplsTunnelIngressLSRId = extract($OID1, "3\.6\.1\.2\.1\.10\.166\.3\.2\.2\.1\.34\.[0-9]+\.[0-9]+\.([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$")
                $mplsTunnelEgressLSRId = extract($OID1, "3\.6\.1\.2\.1\.10\.166\.3\.2\.2\.1\.34\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)$")
            }

            @AlertKey = "mplsTunnelEntry." + $mplsTunnelIndex + "." + $mplsTunnelInstance + "." + $mplsTunnelIngressLSRId + "." + $mplsTunnelEgressLSRId
            @Summary = "A Tunnel is About to Enter the Down State" + " ( " + @AlertKey + " ) "

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            $mplsTunnelAdminStatus = $mplsTunnelAdminStatus + " ( " + $1 + " )"
            $mplsTunnelOperStatus = $mplsTunnelOperStatus + " ( " + $2 + " )"
            $gmplsTunnelErrorLastErrorType = $gmplsTunnelErrorLastErrorType + " ( " + $3 + " )"
            $gmplsTunnelErrorReporterType = $gmplsTunnelErrorReporterType + " ( " + $4 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($mplsTunnelAdminStatus,$mplsTunnelOperStatus,$gmplsTunnelErrorLastErrorType,$gmplsTunnelErrorReporterType,$gmplsTunnelErrorReporter,$gmplsTunnelErrorCode,$gmplsTunnelErrorSubcode,$mplsTunnelIndex,$mplsTunnelInstance,$mplsTunnelIngressLSRId,$mplsTunnelEgressLSRId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "mplsTunnelAdminStatus", $mplsTunnelAdminStatus, "mplsTunnelOperStatus", $mplsTunnelOperStatus, "gmplsTunnelErrorLastErrorType", $gmplsTunnelErrorLastErrorType,
                 "gmplsTunnelErrorReporterType", $gmplsTunnelErrorReporterType, "gmplsTunnelErrorReporter", $gmplsTunnelErrorReporter, "gmplsTunnelErrorCode", $gmplsTunnelErrorCode,
                 "gmplsTunnelErrorSubcode", $gmplsTunnelErrorSubcode, "mplsTunnelIndex", $mplsTunnelIndex, "mplsTunnelInstance", $mplsTunnelInstance,
                 "mplsTunnelIngressLSRId", $mplsTunnelIngressLSRId, "mplsTunnelEgressLSRId", $mplsTunnelEgressLSRId)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, IETF-GMPLS-TE-STD-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, IETF-GMPLS-TE-STD-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/IETF/IETF-GMPLS-TE-STD-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/IETF/IETF-GMPLS-TE-STD-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... IETF-GMPLS-TE-STD-MIB.include.snmptrap.rules >>>>>")
