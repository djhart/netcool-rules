###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  IETF-HC-ALARM-MIB
#
###############################################################################

case ".1.3.6.1.2.1.16.29.2": ###  - Notifications from HC-ALARM-MIB

    log(DEBUG, "<<<<< Entering... IETF-HC-ALARM-MIB.include.snmptrap.rules >>>>>")

    @Agent = "IETF-HC-ALARM-MIB"
    @Class = "40086"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### hcRisingAlarm

            ##########
            # $1 = hcAlarmVariable
            # $2 = hcAlarmSampleType
            # $3 = hcAlarmAbsValue
            # $4 = hcAlarmValueStatus
            # $5 = hcAlarmRisingThreshAbsValueLo
            # $6 = hcAlarmRisingThreshAbsValueHi
            # $7 = hcAlarmRisingThresholdValStatus
            # $8 = hcAlarmRisingEventIndex
            ##########

            $hcAlarmVariable = $1
            $hcAlarmSampleType = lookup($2, hcAlarmSampleType)
            $hcAlarmAbsValue = $3
            $hcAlarmValueStatus = lookup($4, HcValueStatus) + " ( " + $4 + " )"
            $hcAlarmRisingThreshAbsValueLo = $5
            $hcAlarmRisingThreshAbsValueHi = $6
            $hcAlarmRisingThresholdValStatus = lookup($7, HcValueStatus) + " ( " + $7 + " )"
            $hcAlarmRisingEventIndex = $8
            $hcAlarmIndex = extract($OID1, "\.([0-9]+)$")

            switch($4)
            {
                case "3": ### valueNegative
                    $AlarmValue = "-" + $3
                default: ### valueNotAvailable|valuePositive
                    $AlarmValue = $3
            }
            $ThresholdValue = int($5) + (int($6) * 4294967296)
            switch($7)
            {
                case "3": ### valueNegative
                    $ThresholdValue = "-" + $ThresholdValue
                default: ### valueNotAvailable|valuePositive
            }

            $OS_EventId = "SNMPTRAP-IETF-HC-ALARM-MIB-hcRisingAlarm"

            @AlertGroup = "RMON HC Alarm"
            @AlertKey = "hcAlarmEntry." + $hcAlarmIndex
            @Summary = "RMON HC Alarm: " + $hcAlarmSampleType + " of " + $1 + ", " + $AlarmValue + ", Crossed Threshold  ( " + @AlertKey + " )"
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + @Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $hcAlarmSampleType = $hcAlarmSampleType + " ( " + $2 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($hcAlarmVariable,$hcAlarmSampleType,$hcAlarmAbsValue,$hcAlarmValueStatus,$hcAlarmRisingThreshAbsValueLo,$hcAlarmRisingThreshAbsValueHi,$hcAlarmRisingThresholdValStatus,$hcAlarmRisingEventIndex,$hcAlarmIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hcAlarmVariable", $hcAlarmVariable, "hcAlarmSampleType", $hcAlarmSampleType, "hcAlarmAbsValue", $hcAlarmAbsValue,
                 "hcAlarmValueStatus", $hcAlarmValueStatus, "hcAlarmRisingThreshAbsValueLo", $hcAlarmRisingThreshAbsValueLo, "hcAlarmRisingThreshAbsValueHi", $hcAlarmRisingThreshAbsValueHi,
                 "hcAlarmRisingThresholdValStatus", $hcAlarmRisingThresholdValStatus, "hcAlarmRisingEventIndex", $hcAlarmRisingEventIndex, "hcAlarmIndex", $hcAlarmIndex)

        case "2": ### hcFallingAlarm

            ##########
            # $1 = hcAlarmVariable
            # $2 = hcAlarmSampleType
            # $3 = hcAlarmAbsValue
            # $4 = hcAlarmValueStatus
            # $5 = hcAlarmFallingThreshAbsValueLo
            # $6 = hcAlarmFallingThreshAbsValueHi
            # $7 = hcAlarmFallingThresholdValStatus
            # $8 = hcAlarmFallingEventIndex
            ##########

            $hcAlarmVariable = $1
            $hcAlarmSampleType = lookup($2, hcAlarmSampleType)
            $hcAlarmAbsValue = $3
            $hcAlarmValueStatus = lookup($4, HcValueStatus) + " ( " + $4 + " )"
            $hcAlarmFallingThreshAbsValueLo = $5
            $hcAlarmFallingThreshAbsValueHi = $6
            $hcAlarmFallingThresholdValStatus = lookup($7, HcValueStatus) + " ( " + $7 + " )"
            $hcAlarmFallingEventIndex = $8
            $hcAlarmIndex = extract($OID1, "\.([0-9]+)$")
            
            switch($4)
            {
                case "3": ### valueNegative
                    $AlarmValue = "-" + $3
                default: ### valueNotAvailable|valuePositive
                    $AlarmValue = $3
            }
            $ThresholdValue = int($5) + (int($6) * 4294967296)
            switch($7)
            {
                case "3": ### valueNegative
                    $ThresholdValue = "-" + $ThresholdValue
                default: ### valueNotAvailable|valuePositive
            }

            $OS_EventId = "SNMPTRAP-IETF-HC-ALARM-MIB-hcFallingAlarm"

            @AlertGroup = "RMON HC Alarm"
            @AlertKey = "hcAlarmEntry." + $hcAlarmIndex
            @Summary = "RMON HC Alarm Cleared: " + $hcAlarmSampleType + " of " + $1 + ", " + $AlarmValue + ", Crossed Threshold  ( " + @AlertKey + " )"
            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + @Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $hcAlarmSampleType = $hcAlarmSampleType + " ( " + $2 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($hcAlarmVariable,$hcAlarmSampleType,$hcAlarmAbsValue,$hcAlarmValueStatus,$hcAlarmFallingThreshAbsValueLo,$hcAlarmFallingThreshAbsValueHi,$hcAlarmFallingThresholdValStatus,$hcAlarmFallingEventIndex,$hcAlarmIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hcAlarmVariable", $hcAlarmVariable, "hcAlarmSampleType", $hcAlarmSampleType, "hcAlarmAbsValue", $hcAlarmAbsValue,
                 "hcAlarmValueStatus", $hcAlarmValueStatus, "hcAlarmFallingThreshAbsValueLo", $hcAlarmFallingThreshAbsValueLo, "hcAlarmFallingThreshAbsValueHi", $hcAlarmFallingThreshAbsValueHi,
                 "hcAlarmFallingThresholdValStatus", $hcAlarmFallingThresholdValStatus, "hcAlarmFallingEventIndex", $hcAlarmFallingEventIndex, "hcAlarmIndex", $hcAlarmIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, IETF-HC-ALARM-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, IETF-HC-ALARM-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/IETF/IETF-HC-ALARM-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/IETF/IETF-HC-ALARM-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... IETF-HC-ALARM-MIB.include.snmptrap.rules >>>>>")

