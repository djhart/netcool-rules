###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  IETF-SONET-MIB
#
###############################################################################
table sonetSectionCurrentStatus =
{
    ##########
    # This variable indicates the status of the interface. The
    # sonetSectionCurrentStatus is a bit map represented as a sum, therefore,
    # it can represent multiple defects simultaneously. The
    # sonetSectionNoDefect should be set if and only if no other flag is set.
    # The various bit positions are:
    #   1 - sonetSectionNoDefect
    #   2 - sonetSectionLOS
    #   4 - sonetSectionLOF
    ##########

    {"1","No Defect"}, ### sonetSectionNoDefect
    {"2","Loss of Signal"}, ### sonetSectionLOS
    {"4","Loss of Frame"}, ### sonetSectionLOF
    {"6","Loss of Signal and Frame"}
}
default = "Unknown"

table sonetLineCurrentStatus =
{
    ##########
    # This variable indicates the status of the interface. The
    # sonetLineCurrentStatus is a bit map represented as a sum, therefore, it
    # can represent multiple defects simultaneously. The sonetLineNoDefect
    # should be set if and only if no other flag is set. The various bit
    # positions are:
    #   1 - sonetLineNoDefect
    #   2 - sonetLineAIS
    #   4 - sonetLineRDI
    ##########
    
    {"1","No Defect"}, ### sonetLineNoDefect
    {"2","AIS"}, ### sonetLineAIS
    {"4","RDI"}, ### sonetLineRDI
    {"6","AIS and RDI"}
}
default = "Unknown"

table sonetPathCurrentStatus =
{
    ##########
    # This variable indicates the status of the interface. The
    # sonetPathCurrentStatus is a bit map represented as a sum, therefore, it
    # can represent multiple defects simultaneously. The sonetPathNoDefect
    # should be set if and only if no other flag is set. The various bit
    # positions are:
    #   1 - sonetPathNoDefect
    #   2 - sonetPathSTSLOP
    #   4 - sonetPathSTSAIS
    #   8 - sonetPathSTSRDI
    #  16 - sonetPathUnequipped
    #  32 - sonetPathSignalLabelMismatch
    ##########
    
    {"1","No Defect"}, ### sonetPathNoDefect
    {"2","STS LOP"}, ### sonetPathSTSLOP
    {"4","STS AIS"}, ### sonetPathSTSAIS
    {"8","STS RDI"}, ### sonetPathSTSRDI
    {"16","Unequipped"}, ### sonetPathUnequipped
    {"32","Signal Label Mismatch"} ### sonetPathSignalLabelMismatch
}
default = "Unknown"

table sonetVTCurrentStatus =
{
    ##########
    # This variable indicates the status of the interface. The
    # sonetVTCurrentStatus is a bit map represented as a sum, therefore, it can
    # represent multiple defects and failures simultaneously. The
    # sonetVTNoDefect should be set if and only if no other flag is set. The
    # various bit positions are:
    #   1 - sonetVTNoDefect
    #   2 - sonetVTLOP
    #   4 - sonetVTPathAIS
    #   8 - sonetVTPathRDI
    #  16 - sonetVTPathRFI
    #  32 - sonetVTUnequipped
    #  64 - sonetVTSignalLabelMismatch
    ##########
    
    {"1","No Defect"}, ### sonetVTNoDefect
    {"2","LOP"}, ### sonetVTLOP
    {"4","Path AIS"}, ### sonetVTPathAIS
    {"8","Path RDI"}, ### sonetVTPathRDI
    {"16","Path RFI"}, ### sonetVTPathRFI
    {"32","Unequipped"}, ### sonetVTUnequipped
    {"64","Signal Label Mismatch"} ### sonetVTSignalLabelMismatch
}
default = "Unknown"
