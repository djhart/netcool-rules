###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  FR-MFR-MIB
#
###############################################################################

case ".1.3.6.1.2.1.10.47.4": ###  - Notifications from FR-MFR-MIB (200011300000Z)

    log(DEBUG, "<<<<< Entering... IETF-FR-MFR-MIB.include.snmptrap.rules >>>>>")

    @Agent = "IETF-FR-MFR-MIB"
    @Class = "40086"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### mfrMibTrapBundleLinkMismatch

            ##########
            # $1 = mfrBundleNearEndName 
            # $2 = mfrBundleFarEndName 
            # $3 = mfrBundleLinkNearEndName 
            # $4 = mfrBundleLinkFarEndName 
            # $5 = mfrBundleLinkFarEndBundleName 
            ##########

            $mfrBundleNearEndName = $1
            $mfrBundleFarEndName = $2
            $mfrBundleLinkNearEndName = $3
            $mfrBundleLinkFarEndName = $4
            $mfrBundleLinkFarEndBundleName = $5

            $OS_EventId = "SNMPTRAP-IETF-FR-MFR-MIB-mfrMibTrapBundleLinkMismatch"

            @AlertGroup = "Bundle Link Status"
            $mfrBundleIndex = extract($OID1, "\.([0-9]+)$")
            $ifIndex = extract($OID3, "\.([0-9]+)$")
            @AlertKey = "mfrBundleEntry." + $mfrBundleIndex + ", mfrBundleLinkEntry." + $ifIndex
            @Summary = "A Bundle Link Mismatch has been Detected" + " ( " + @AlertKey + " ) "

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($mfrBundleNearEndName,$mfrBundleFarEndName,$mfrBundleLinkNearEndName,$mfrBundleLinkFarEndName,$mfrBundleLinkFarEndBundleName,$mfrBundleIndex,$ifIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "mfrBundleNearEndName", $mfrBundleNearEndName, "mfrBundleFarEndName", $mfrBundleFarEndName, "mfrBundleLinkNearEndName", $mfrBundleLinkNearEndName,
                 "mfrBundleLinkFarEndName", $mfrBundleLinkFarEndName, "mfrBundleLinkFarEndBundleName", $mfrBundleLinkFarEndBundleName, "mfrBundleIndex", $mfrBundleIndex,
                 "ifIndex", $ifIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, IETF-FR-MFR-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, IETF-FR-MFR-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/IETF/IETF-FR-MFR-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/IETF/IETF-FR-MFR-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... IETF-FR-MFR-MIB.include.snmptrap.rules >>>>>")
