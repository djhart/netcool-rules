###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  HA-MIB
#
###############################################################################

case ".1.3.6.1.4.1.1588.2.1.2.2": ### - Notifications from HA-MIB (200208160000Z)

	log(DEBUG, "<<<<< Entering... brocade-HA-MIB.include.snmptrap.rules >>>>>")

    @Agent = "brocade-HA-MIB"
    @Class = "40059"
    
    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
    
     
        case "1": ### fruStatusChanged
        
            ##########
            # $1 = entPhysicalName
            # $2 = fruStatus
            # $3 = fruClass
            # $4 = fruObjectNum
            ##########
            
            $entPhysicalName = $1
            $fruStatus = lookup($2, FruStatus)
            $fruClass = lookup($3, FruClass)
            $fruObjectNum = $4
            
            $entPhysicalIndex = extract($OID1, "\.([0-9]+)$")
            
            $OS_EventId = "SNMPTRAP-brocade-HA-MIB-fruStatusChanged"

            @AlertGroup = "FRU Status"
            @AlertKey = "entPhysicalEntry" + "." + $entPhysicalIndex + ", " + "fRUEntry" + "." + $entPhysicalIndex
            @Summary = "FRU status has changed, Status: " + $fruStatus + " ( " + @AlertKey + " ) "
            
            switch($2)
            {
                case "1":### other
                    $SEV_KEY = $OS_EventId + "_other"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "2":### unknown
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "3":### on
                    $SEV_KEY = $OS_EventId + "_on"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0               
                case "4":### off
                    $SEV_KEY = $OS_EventId + "_off"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "5":### faulty
                    $SEV_KEY = $OS_EventId + "_faulty"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                default: 
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = "FRU Status Has Changed, Status: " + $fruStatus + " ( " + @AlertKey + " ) "
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }
                     
			update(@Severity)
			
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $fruStatus = $fruStatus + " ( " + $2 + " )"
            $fruClass = $fruClass + " ( " + $3 + " )"
 			if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_brocade, "1")) {
     			details($entPhysicalName, $fruStatus, $fruClass, $fruObjectNum, $entPhysicalIndex)
 			}
 			@ExtendedAttr = nvp_add(@ExtendedAttr, "entPhysicalName", $entPhysicalName, "fruStatus", $fruStatus, "fruClass", $fruClass,
 			     "fruObjectNum", $fruObjectNum, "entPhysicalIndex", $entPhysicalIndex)
         
        case "2": ### cpStatusChanged
        
            ##########
            # $1 = cpStatus
            # $2 = cpLastEvent
            # $3 = swID
            # $4 = swSsn
            ##########
            
            $cpStatus = lookup($1, CpStatus)
            $cpLastEvent = lookup($2, CpLastEvent)
            $swID = $3
            $swSsn = $4
            
            $entPhysicalIndex = extract($OID1, "\.([0-9]+)$")
            
            $OS_EventId = "SNMPTRAP-brocade-HA-MIB-cpStatusChanged"

            @AlertGroup = "CP Status"
            @AlertKey = "cpEntry" + "." + $entPhysicalIndex
            @Summary = "CP Object status has changed, Status: " + $cpStatus + " ( " + @AlertKey + " ) "
            
            switch($1)
            {
                case "1":### other
                    $SEV_KEY = $OS_EventId + "_other"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "2":### unknown
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "3":### active
                    $SEV_KEY = $OS_EventId + "_active"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0               
                case "4":### standby
                    $SEV_KEY = $OS_EventId + "_standby"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800               
                case "5":### failed
                    $SEV_KEY = $OS_EventId + "_failed"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                default: 
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = "CP Object Status Has Changed, Status: " + $cpStatus + " ( " + @AlertKey + " ) "
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }
                     
			update(@Severity)
			
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $cpStatus = $cpStatus + " ( " + $1 + " )"
            $cpLastEvent = $cpLastEvent + " ( " + $2 + " )"
 			if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_brocade, "1")) {
     			details($cpStatus, $cpLastEvent, $swID, $swSsn, $entPhysicalIndex)
 			}
 			@ExtendedAttr = nvp_add(@ExtendedAttr, "cpStatus", $cpStatus, "cpLastEvent", $cpLastEvent, "swID", $swID,
 			     "swSsn", $swSsn, "entPhysicalIndex", $entPhysicalIndex)
         
        case "3": ### fruHistoryTrap
        
            ##########
            # $1 = fruHistoryClass
            # $2 = fruHistoryObjectNum
            # $3 = fruHistoryEvent
            # $4 = fruHistoryTime
            # $5 = fruHistoryFactoryPartNum
            # $6 = fruHistoryFactorySerialNum
            ##########
            
            $fruHistoryClass = lookup($1, FruClass)
            $fruHistoryObjectNum = $2
            $fruHistoryEvent = lookup($3, FruHistoryEvent)
            $fruHistoryTime = $4
            $fruHistoryFactoryPartNum = $5
            $fruHistoryFactorySerialNum = $6
            
            $fruHistoryIndex = extract($OID1, "\.([0-9]+)$")
            
            $OS_EventId = "SNMPTRAP-brocade-HA-MIB-fruHistoryTrap"

            @AlertGroup = "FRU History"
            @AlertKey = "fruHistoryEntry" + "." + $fruHistoryIndex
            
            switch($3)
            {
                case "1":### added
                    $SEV_KEY = $OS_EventId + "_added"
                    @Summary = "FRU added, Object: " + $fruHistoryClass + " ( " + @AlertKey + " ) "
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800               
                case "2":### removed
                    $SEV_KEY = $OS_EventId + "_removed"
                    @Summary = "FRU removed, Object: " + $fruHistoryClass + " ( " + @AlertKey + " ) "
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800               
                case "3":### invalid
                    $SEV_KEY = $OS_EventId + "_invalid"
                    @Summary = "FRU invalid, Object: " + $fruHistoryClass + " ( " + @AlertKey + " ) "
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800               
                default: 
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = "Unknown FRU event, Object: " + $fruHistoryClass + " ( " + @AlertKey + " ) "
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }
                     
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $fruHistoryClass = $fruHistoryClass + " ( " + $1 + " )"
            $fruHistoryEvent = $fruHistoryEvent + " ( " + $3 + " )"
 			if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_brocade, "1")) {
     			details($fruHistoryClass, $fruHistoryObjectNum, $fruHistoryEvent, $fruHistoryTime, $fruHistoryFactoryPartNum, $fruHistoryFactorySerialNum, $fruHistoryIndex)
 			}
 			@ExtendedAttr = nvp_add(@ExtendedAttr, "fruHistoryClass", $fruHistoryClass, "fruHistoryObjectNum", $fruHistoryObjectNum, "fruHistoryEvent", $fruHistoryEvent,
 			     "fruHistoryTime", $fruHistoryTime, "fruHistoryFactoryPartNum", $fruHistoryFactoryPartNum, "fruHistoryFactorySerialNum", $fruHistoryFactorySerialNum,
 			     "fruHistoryIndex", $fruHistoryIndex)
         
        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_brocade, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, brocade-HA-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, brocade-HA-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/brocade/brocade-HA-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/brocade/brocade-HA-MIB.user.include.snmptrap.rules"


##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... brocade-HA-MIB.include.snmptrap.rules >>>>>")


