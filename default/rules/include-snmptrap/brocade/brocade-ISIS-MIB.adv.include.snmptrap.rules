###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  ISIS-MIB
#
###############################################################################

log(DEBUG, "<<<<< Entering... brocade-ISIS-MIB.adv.include.snmptrap.rules >>>>>")

switch($specific-trap)
{
    case "1": ### isisDatabaseOverload

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "isisDatabaseOverload"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "isisSysLevelEntry." + $isisSysLevelIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "2": ### isisManualAddressDrops

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "isisManualAddressDrops"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Area Address: " + $isisNotificationAreaAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "3": ### isisCorruptedLSPDetected

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "isisCorruptedLSPDetected"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "LSP ID: " + $isisPduLspId
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "4": ### isisAttemptToExceedMaxSequence

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "isisAttemptToExceedMaxSequence"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "LSP ID: " + $isisPduLspId
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "5": ### isisIDLenMismatch

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "isisIDLenMismatch"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Circuit ID: " + $isisNotificationCircIfIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "6": ### isisMaxAreaAddressesMismatch

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "isisMaxAreaAddressesMismatch"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Circuit ID: " + $isisNotificationCircIfIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "7": ### isisOwnLSPPurge

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "isisOwnLSPPurge"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Circuit ID: " + $isisNotificationCircIfIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "8": ### isisSequenceNumberSkip

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "isisSequenceNumberSkip"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Circuit ID: " + $isisNotificationCircIfIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "9": ### isisAuthenticationTypeFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "isisAuthenticationTypeFailure"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Circuit ID: " + $isisNotificationCircIfIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "10": ### isisAuthenticationFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "isisAuthenticationFailure"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Circuit ID: " + $isisNotificationCircIfIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "11": ### isisVersionSkew

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "isisVersionSkew"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Circuit ID: " + $isisNotificationCircIfIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "12": ### isisAreaMismatch

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "isisAreaMismatch"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Circuit ID: " + $isisNotificationCircIfIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "13": ### isisRejectedAdjacency

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "isisRejectedAdjacency"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Circuit ID: " + $isisNotificationCircIfIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "14": ### isisLSPTooLargeToPropagate

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "isisLSPTooLargeToPropagate"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Circuit ID: " + $isisNotificationCircIfIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "15": ### isisOrigLSPBuffSizeMismatch

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "isisOrigLSPBuffSizeMismatch"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Circuit ID: " + $isisNotificationCircIfIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "16": ### isisProtocolsSupportedMismatch

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "isisProtocolsSupportedMismatch"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Circuit ID: " + $isisNotificationCircIfIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "17": ### isisAdjacencyChange

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "isisAdjacencyChange"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Circuit ID: " + $isisNotificationCircIfIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "18": ### isisLSPErrorDetected

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "isisLSPErrorDetected"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Circuit ID: " + $isisNotificationCircIfIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    default:
}

log(DEBUG, "<<<<< Leaving... brocade-ISIS-MIB.adv.include.snmptrap.rules >>>>>")


