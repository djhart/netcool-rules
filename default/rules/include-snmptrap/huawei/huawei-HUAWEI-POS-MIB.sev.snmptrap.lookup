###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  HUAWEI-POS-MIB
#
# 1.1 - Changed the format
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table huawei-HUAWEI-POS-MIB_sev =
{
	    {"SNMPTRAP-huawei-HUAWEI-POS-MIB-posAppNotReadyTrap","3","1","0"},
	    {"SNMPTRAP-huawei-HUAWEI-POS-MIB-posAppConnectFailTrap","3","1","0"},
	    {"SNMPTRAP-huawei-HUAWEI-POS-MIB-posAppStateChangeTrap_noset","2","1","0"},
	    {"SNMPTRAP-huawei-HUAWEI-POS-MIB-posAppStateChangeTrap_down","3","1","0"},
	    {"SNMPTRAP-huawei-HUAWEI-POS-MIB-posAppStateChangeTrap_up","1","2","0"},
	    {"SNMPTRAP-huawei-HUAWEI-POS-MIB-posAppStateChangeTrap_ok","1","2","0"},
	    {"SNMPTRAP-huawei-HUAWEI-POS-MIB-posAppStateChangeTrap_kept","2","1","0"},
	    {"SNMPTRAP-huawei-HUAWEI-POS-MIB-posAppStateChangeTrap_linking","2","1","0"},
	    {"SNMPTRAP-huawei-HUAWEI-POS-MIB-posAppStateChangeTrap_linked","1","2","0"},
	    {"SNMPTRAP-huawei-HUAWEI-POS-MIB-posAppStateChangeTrap_default","2","1","0"},
	    {"SNMPTRAP-huawei-HUAWEI-POS-MIB-posAppNotConfigedTrap","3","1","0"},
	    {"SNMPTRAP-huawei-HUAWEI-POS-MIB-posAppBuffOverFlowTrap","4","1","0"},
	    {"SNMPTRAP-huawei-HUAWEI-POS-MIB-posAppDebugOpenTrap","3","1","0"},
	    {"SNMPTRAP-huawei-HUAWEI-POS-MIB-posAppDebugAllOpenTrap","4","1","0"},
	    {"SNMPTRAP-huawei-HUAWEI-POS-MIB-posInterBuffOverFlowTrap","3","1","0"},
	    {"SNMPTRAP-huawei-HUAWEI-POS-MIB-posInterStateChangeTrap_noset","2","1","0"},
	    {"SNMPTRAP-huawei-HUAWEI-POS-MIB-posInterStateChangeTrap_down","3","1","0"},
	    {"SNMPTRAP-huawei-HUAWEI-POS-MIB-posInterStateChangeTrap_up","1","2","0"},
	    {"SNMPTRAP-huawei-HUAWEI-POS-MIB-posInterStateChangeTrap_ok","1","2","0"},
	    {"SNMPTRAP-huawei-HUAWEI-POS-MIB-posInterStateChangeTrap_default","2","1","0"},
	    {"SNMPTRAP-huawei-HUAWEI-POS-MIB-posInterDebugOpenTrap","3","1","0"},
	    {"SNMPTRAP-huawei-HUAWEI-POS-MIB-posInterDebugAllOpenTrap","4","1","0"},
	    {"SNMPTRAP-huawei-HUAWEI-POS-MIB-posFCMTimeoutTrap","3","1","0"},
	    {"SNMPTRAP-huawei-HUAWEI-POS-MIB-posFCMConnectFailTrap","3","1","0"},
	    {"SNMPTRAP-huawei-HUAWEI-POS-MIB-posClearPacketCounter","2","13","1800"},
	    {"SNMPTRAP-huawei-HUAWEI-POS-MIB-posClearFcmCounter","2","13","1800"}
}
default = {"Unknown","Unknown","Unknown"}
