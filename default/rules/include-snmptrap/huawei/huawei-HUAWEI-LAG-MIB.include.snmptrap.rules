###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 3.0 - Updated release for NIM-03
#
#          -  Supports "Advanced" and "User" include files
#          -  Supports "Severity" lookup tables
#
###############################################################################
#
# 2.0 - Updated release
#
#          -  Repackage for NIM-02
#          -  HUAWEI-LAG-MIB release V1.3
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#          -  HUAWEI-LAG-MIB release V1.2
#
###############################################################################

case ".1.3.6.1.4.1.2011.5.25.25.2": ### Huawei - Notifications from HUAWEI-LAG-MIB

    log(DEBUG, "<<<<< Entering... huawei-HUAWEI-LAG-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Huawei AR- and S-Series"
    @Class = "40579"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### hwAggSpeedChangedNotification

            ##########
            # This event will be triggered whenever an
            # aggregation changes its speed.
            #
            # $1 = hwAggLinkNumber - The serial number of aggregation group.
            ##########

            $hwAggLinkNumber = $1
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_huawei, "1")) {
                details($hwAggLinkNumber)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hwAggLinkNumber", $hwAggLinkNumber)

            $OS_EventId = "SNMPTRAP-huawei-HUAWEI-LAG-MIB-hwAggSpeedChangedNotification"

            @AlertGroup = "Agg Speed Change"
            @AlertKey = "hwAggLinkEntry." + $1
            @Summary = "Aggregation Speed has Changed  ( Link Number: " + $1 + " )"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        case "2": ### hwAggPortInactiveNotification

            ##########
            # This event will be triggered whenever any port
            # in aggrerator is made inactive
            #
            # $1 = hwAggLinkNumber - The serial number of aggregation group.
            ##########

            $hwAggLinkNumber = $1
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_huawei, "1")) {
                details($hwAggLinkNumber)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hwAggLinkNumber", $hwAggLinkNumber)

            $OS_EventId = "SNMPTRAP-huawei-HUAWEI-LAG-MIB-hwAggPortInactiveNotification"

            @AlertGroup = "Agg Port Inactive"
            @AlertKey = "hwAggLinkEntry." + $1
            @Summary = "Aggregation Port Inactive  ( Link Number: " + $1 + " )"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_huawei, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

#########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, huawei-HUAWEI-LAG-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, huawei-HUAWEI-LAG-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/huawei/huawei-HUAWEI-LAG-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/huawei/huawei-HUAWEI-LAG-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

log(DEBUG, "<<<<< Leaving... huawei-HUAWEI-LAG-MIB.include.snmptrap.rules >>>>>")
