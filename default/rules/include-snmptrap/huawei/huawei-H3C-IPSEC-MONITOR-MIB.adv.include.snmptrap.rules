###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 3.0 - Initial Release.
#
#       Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#         -  H3C-IPSEC-MONITOR-MIB release V1.3
#
###############################################################################

log(DEBUG, "<<<<< Entering... huawei-H3C-IPSEC-MONITOR-MIB.adv.include.snmptrap.rules >>>>>")

switch($specific-trap)
{
    case "1": ### h3cIPSecTunnelStart

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "h3cIPSecTunnelStart"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "h3cIPSecTunnelEntry." + $h3cIPSecTunIfIndex + "." + $h3cIPSecTunEntryIndex + "." + $h3cIPSecTunIndex
        $OS_LocalRootObj = "ifEntry." + $h3cIPSecTunIfIndex
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "2": ### h3cIPSecTunnelStop

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "h3cIPSecTunnelStop"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "h3cIPSecTunnelEntry." + $h3cIPSecTunIfIndex + "." + $h3cIPSecTunEntryIndex + "." + $h3cIPSecTunIndex
        $OS_LocalRootObj = "ifEntry." + $h3cIPSecTunIfIndex
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "3": ### h3cIPSecNoSaFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "h3cIPSecNoSaFailure"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "h3cIPSecTunnelEntry." + $h3cIPSecTunIfIndex + "." + $h3cIPSecTunEntryIndex + "." + $h3cIPSecTunIndex
        $OS_LocalRootObj = "ifEntry." + $h3cIPSecTunIfIndex
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "4": ### h3cIPSecAuthFailFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "h3cIPSecAuthFailFailure"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "h3cIPSecTunnelEntry." + $h3cIPSecTunIfIndex + "." + $h3cIPSecTunEntryIndex + "." + $h3cIPSecTunIndex
        $OS_LocalRootObj = "ifEntry." + $h3cIPSecTunIfIndex
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "5": ### h3cIPSecEncryFailFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "h3cIPSecEncryFailFailure"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "h3cIPSecTunnelEntry." + $h3cIPSecTunIfIndex + "." + $h3cIPSecTunEntryIndex + "." + $h3cIPSecTunIndex
        $OS_LocalRootObj = "ifEntry." + $h3cIPSecTunIfIndex
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "6": ### h3cIPSecDecryFailFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "h3cIPSecDecryFailFailure"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "h3cIPSecTunnelEntry." + $h3cIPSecTunIfIndex + "." + $h3cIPSecTunEntryIndex + "." + $h3cIPSecTunIndex
        $OS_LocalRootObj = "ifEntry." + $h3cIPSecTunIfIndex
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "7": ### h3cIPSecInvalidSaFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "h3cIPSecInvalidSaFailure"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "h3cIPSecTunnelEntry." + $h3cIPSecTunIfIndex + "." + $h3cIPSecTunEntryIndex + "." + $h3cIPSecTunIndex
        $OS_LocalRootObj = "ifEntry." + $h3cIPSecTunIfIndex
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "8": ### h3cIPSecPolicyAdd

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "h3cIPSecPolicyAdd"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Name: " + $1
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "9": ### h3cIPSecPolicyDel

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "h3cIPSecPolicyDel"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Name: " + $1
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "10": ### h3cIPSecPolicyAttach

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "h3cIPSecPolicyAttach"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Name: " + $1 + ", Interface: " + $3
        $OS_LocalRootObj = "ifEntry." + $3
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "11": ### h3cIPSecPolicyDetach

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "h3cIPSecPolicyDetach"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Name: " + $1 + ", Interface: " + $3
        $OS_LocalRootObj = "ifEntry." + $3
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    default:
}

log(DEBUG, "<<<<< Leaving... huawei-H3C-IPSEC-MONITOR-MIB.adv.include.snmptrap.rules >>>>>")
