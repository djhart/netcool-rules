###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  Compliant with JUNOS 12.2. For further information, refer to 
#              Juniper JUNOS 12.2 Syslog Reference Guide.
#
###############################################################################

case "ALARMD":

    log(DEBUG, "<<<<< Entering... juniper-junos-ALARMD.include.syslog.rules >>>>>")

    switch($message-tag)
    {

        case "ALARMD_CONFIG_ACCESS_ERROR":


            ###############################
            #
            # System Log Message: 
            # function-name: error-message
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-ALARMD_CONFIG_ACCESS_ERROR"
            
            $functionname = ltrim(rtrim(extract($Details, ":(.*):")))
            $errormsg = ltrim(rtrim(extract($Details, ":.*: (.*)")))

            @AlertGroup = "ALARMD_CONFIG_ACCESS_ERROR"
            @AlertKey = "Function name:" + $functionname

            @Summary = "Function Name:" + $functionname + ", Error Message:" + $errormsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($functionname,$errormsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "functionname", $functionname, "errormsg", $errormsg)


        case "ALARMD_CONFIG_CLOSE_ERROR":


            ###############################
            #
            # System Log Message: 
            # Unable to close configuration database
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-ALARMD_CONFIG_CLOSE_ERROR"

            @AlertGroup = "ALARMD_CONFIG_CLOSE_ERROR"
            @AlertKey = ""

            @Summary = "Unable to close configuration database."

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            


        case "ALARMD_CONFIG_PARSE_ERROR":


            ###############################
            #
            # System Log Message: 
            # Unable to parse configuration; using defaults
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-ALARMD_CONFIG_PARSE_ERROR"

            @AlertGroup = "ALARMD_CONFIG_PARSE_ERROR"
            @AlertKey = ""

            @Summary = "Unable to parse configuration; using defaults."

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            


        case "ALARMD_CONFIG_RECONFIG_ERROR":


            ###############################
            #
            # System Log Message: 
            # Re-configuration failed, using previous values
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-ALARMD_CONFIG_RECONFIG_ERROR"

            @AlertGroup = "ALARMD_CONFIG_RECONFIG_ERROR"
            @AlertKey = ""

            @Summary = "Re-configuration failed, using previous values."

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            


        case "ALARMD_CONNECTION_FAILURE":


            ###############################
            #
            # System Log Message: 
            # after [count] attempts [process-name] connect returned error: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-ALARMD_CONNECTION_FAILURE"
            
            $count = rtrim(extract($Details, "after (.*) attempts"))
            $processname = ltrim(rtrim(extract($Details, "attempts(.*)connect")))
            $errormsg = rtrim(extract($Details, "error: (.*)"))

            @AlertGroup = "ALARMD_CONNECTION_FAILURE"
            @AlertKey = "Process Name:" + $processname

            @Summary = "After " + $count + " attempts " + $processname + " connect returned error: " + $errormsg

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($count,$processname,$errormsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "count", $count, "processname", $processname, "errormsg", $errormsg)


        case "ALARMD_DECODE_ALARM_OBJECT_ERROR":


            ###############################
            #
            # System Log Message: 
            # Cannot decode error test from object [alarm-object], reason [alarm-reason]
            # for if_name [interface-type]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-ALARMD_DECODE_ALARM_OBJECT_ERROR"
            
            $alarmobject = ltrim(rtrim(extract($Details, "from object(.*),?\s+reason")))
            $alarmreason = rtrim(extract($Details, "reason (.*) for"))
            $iftype = rtrim(extract($Details, "if_name (.*)"))

            @AlertGroup = "ALARMD_DECODE_ALARM_OBJECT_ERROR"
            @AlertKey = "Object:" + $alarmobject

            @Summary = "Cannot decode error test from " + $alarmobject + ", reason:" + $alarmreason + " for if_name " + $iftype

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($alarmobject,$alarmreason,$iftype)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "alarmobject", $alarmobject, "alarmreason", $alarmreason, "iftype", $iftype)


        case "ALARMD_EXISTS":


            ###############################
            #
            # System Log Message: 
            # alarmd already running; exiting
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-ALARMD_EXISTS"

            @AlertGroup = "ALARMD_EXISTS"
            @AlertKey = ""

            @Summary = "alarmd already running; exiting."

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            


        case "ALARMD_EXISTS_TERM_OTHER":


            ###############################
            #
            # System Log Message: 
            # Killing existing alarmd and exiting
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-ALARMD_EXISTS_TERM_OTHER"

            @AlertGroup = "ALARMD_EXISTS_TERM_OTHER"
            @AlertKey = ""

            @Summary = "Killing existing alarmd and exiting."

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            


        case "ALARMD_IFDALARM_TYPE_ERROR":


            ###############################
            #
            # System Log Message: 
            # Unknown interface alarm type: [alarm-type]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-ALARMD_IFDALARM_TYPE_ERROR"
            
            $alarmtype = rtrim(extract($Details, "type: (.*)"))

            @AlertGroup = "ALARMD_IFDALARM_TYPE_ERROR"
            @AlertKey = "Interface alarm type:" + $alarmtype

            @Summary = "Unknown interface alarm type: " + $alarmtype

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($alarmtype)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "alarmtype", $alarmtype)


        case "ALARMD_IFDEV_RTSLIB_FAILURE":


            ###############################
            #
            # System Log Message: 
            # [function-name]: [library-function-name] failed on [error-message] socket
            # ([socket-option])
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-ALARMD_IFDEV_RTSLIB_FAILURE"
            
            $functionname = ltrim(rtrim(extract($Details, "^.*:(.*): ")))
            $libfuncname = rtrim(extract($Details, ":.*: (.*) failed"))
            $errormsg = rtrim(extract($Details, "on (.*) socket"))
            $socketopt = rtrim(extract($Details, "socket \((.*)\)"))

            @AlertGroup = "ALARMD_IFDEV_RTSLIB_FAILURE"
            @AlertKey = "Function name:" + $functionname + ", Library function name:" + $libfuncname

            @Summary = $functionname + ": " + $libfuncname + " failed on " + $errormsg 
                        + " socket (" + $socketopt + ")"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($functionname,$libfuncname,$errormsg,$socketopt)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "functionname", $functionname, "libfuncname", $libfuncname, "errormsg", $errormsg,
                 "socketopt", $socketopt)


        case "ALARMD_IPC_MSG_ERROR":


            ###############################
            #
            # System Log Message: 
            # [function-name]: error code [error-code], type [message-type], subtype
            # [message-subtype], opcode [message-opcode]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-ALARMD_IPC_MSG_ERROR"
            
            $functionname = ltrim(rtrim(extract($Details, "^.*:(.*): ")))
            $errorcode = rtrim(extract($Details, "error code (.*),?\s+type"))
            $msgtype = rtrim(extract($Details, "type (.*),?\s+subtype"))
            $subtype = rtrim(extract($Details, "subtype (.*),?\s+opcode"))
            $opcode = rtrim(extract($Details, "opcode (.*)"))

            @AlertGroup = "ALARMD_IPC_MSG_ERROR"
            @AlertKey = "Function name:" + $functionname + ", Error code:" + $errorcode

            @Summary = $functionname + ": " + "error code " + $errorcode + ", " + "type " 
                       + $msgtype + ", " + "subtype " + $subtype + ", " + "opcode " + $opcode

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($functionname,$errorcode,$msgtype,$subtype,$opcode)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "functionname", $functionname, "errorcode", $errorcode, "msgtype", $msgtype,
                 "subtype", $subtype, "opcode", $opcode)


        case "ALARMD_IPC_MSG_WRITE_ERROR":


            ###############################
            #
            # System Log Message: 
            # alarmd_send_msg: ipc_msg_write failed: [error-message].
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-ALARMD_IPC_MSG_WRITE_ERROR"
            
            $errmsg = rtrim(extract($Details, "failed: (.*)"))

            @AlertGroup = "ALARMD_IPC_MSG_WRITE_ERROR"
            @AlertKey = "Error:" + $errmsg

            @Summary = "alarmd_send_msg: ipc_msg_write failed: " + $errmsg

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "errmsg", $errmsg)


        case "ALARMD_IPC_UNEXPECTED_CONN":


            ###############################
            #
            # System Log Message: 
            # Invalid connection id: [connection-who]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-ALARMD_IPC_UNEXPECTED_CONN"
            
            $connwho = rtrim(extract($Details, "id: (.*)"))

            @AlertGroup = "ALARMD_IPC_UNEXPECTED_CONN"
            @AlertKey = "Connection ID:" + $connwho

            @Summary = "Invalid connection id: " + $connwho

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($connwho)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "connwho", $connwho)


        case "ALARMD_IPC_UNEXPECTED_MSG":


            ###############################
            #
            # System Log Message: 
            # [function-name]: invalid message received: [message] (message type
            # [message-type], subtype [message-subtype])
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-ALARMD_IPC_UNEXPECTED_MSG"
            
            $functionname = ltrim(rtrim(extract($Details, "^.*:(.*):.*:")))
            $message = rtrim(extract($Details, "received: (.*) \(message"))
            $msgtype = rtrim(extract($Details, "message type (.*),?\s+subtype"))
            $subtype = rtrim(extract($Details, "subtype (.*)\)"))

            @AlertGroup = "ALARMD_IPC_UNEXPECTED_MSG"
            @AlertKey = "Function name:" + $functionname + ", message:" + $message

            @Summary = $functionname + ": " + "invalid message received: " + $message
                       + "(message type " + $msgtype + ", subtype " + $subtype + ")"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($functionname,$message,$msgtype,$subtype)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "functionname", $functionname, "message", $message, "msgtype", $msgtype,
                 "subtype", $subtype)


        case "ALARMD_MEM_ALLOC_FAILURE":


            ###############################
            #
            # System Log Message: 
            # [function-name]: alarmd memory allocation failed
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-ALARMD_MEM_ALLOC_FAILURE"
            
            $functionname = ltrim(rtrim(extract($Details, "^.*:(.*): alarmd")))

            @AlertGroup = "ALARMD_MEM_ALLOC_FAILURE"
            @AlertKey = "Function Name:" + $functionname

            @Summary = $functionname + ": alarmd memory allocation failed."

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($functionname)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "functionname", $functionname)


        case "ALARMD_MGR_CONNECT":


            ###############################
            #
            # System Log Message: 
            # [function-name] evSelectFD: initial pipe create aborted (errno
            # [error-code])
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-ALARMD_MGR_CONNECT"
            
            $functionname = ltrim(rtrim(extract($Details, "^.*:(.*) evSelectFD:")))
            $errcode = rtrim(extract($Details, "\(errno (.*)\)"))

            @AlertGroup = "ALARMD_MGR_CONNECT"
            @AlertKey = "Function Name:" + $functionname + ", Error code:" + $errcode

            @Summary = $functionname + " evSelectFD: initial pipe create aborted" + "(errno "
                       + $errcode + ")"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($functionname,$errcode)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "functionname", $functionname, "errcode", $errcode)


        case "ALARMD_MULTIPLE_ALARM_BIT_ERROR":


            ###############################
            #
            # System Log Message: 
            # Multiple alarm bits: 0x[alarm-bit]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-ALARMD_MULTIPLE_ALARM_BIT_ERROR"
            
            $alarmbit = rtrim(extract($Details, "0x(.*)"))

            @AlertGroup = "ALARMD_MULTIPLE_ALARM_BIT_ERROR"
            @AlertKey = "Alarm Bit:" + "0x" + $alarmbit

            @Summary = "Multiple alarm bits: " + "0x" + $alarmbit

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($alarmbit)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "alarmbit", $alarmbit)


        case "ALARMD_PIDFILE_OPEN":


            ###############################
            #
            # System Log Message: 
            # Unable to open PID file '[filename]': errno [error-code]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-ALARMD_PIDFILE_OPEN"
            
            $filename = rtrim(extract($Details, "file (.*):"))
            $errorcode = rtrim(extract($Details, "errno (.*)"))

            @AlertGroup = "ALARMD_PIDFILE_OPEN"
            @AlertKey = "PID file:" + $filename

            @Summary = "Unable to open PID file " + $filename + ": errno" + $errorcode

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($filename,$errorcode)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "filename", $filename, "errorcode", $errorcode)


        case "ALARMD_PIPE_WRITE_ERROR":


            ###############################
            #
            # System Log Message: 
            # Pipe write error: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-ALARMD_PIPE_WRITE_ERROR"
            
            $errormsg = rtrim(extract($Details, "error: (.*)"))

            @AlertGroup = "ALARMD_PIPE_WRITE_ERROR"
            @AlertKey = "Error:" + $errormsg

            @Summary = "Pipe write error: " + $errormsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($errormsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "errormsg", $errormsg)


        case "ALARMD_SOCKET_CREATE":


            ###############################
            #
            # System Log Message: 
            # Unable to create socket for [process-name] connection: errno [error-code]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-ALARMD_SOCKET_CREATE"
            
            $processname = rtrim(extract($Details, "for (.*) connection"))
            $errorcode = rtrim(extract($Details, ": errno (.*)"))

            @AlertGroup = "ALARMD_SOCKET_CREATE"
            @AlertKey = "Process Name:" + $processname

            @Summary = " Unable to create socket for " + $processname + " connection: errno "
                       + $errorcode

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($processname,$errorcode)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "processname", $processname, "errorcode", $errorcode)


        case "ALARMD_UNEXPECTED_EXIT":


            ###############################
            #
            # System Log Message: 
            # evMainLoop returned [return-value] (errno [error-code])
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-ALARMD_UNEXPECTED_EXIT"
            
            $returnval = rtrim(extract($Details, "returned (.*) \(errno"))
            $errorcode = rtrim(extract($Details, "\(errno (.*)\)"))

            @AlertGroup = "ALARMD_UNEXPECTED_EXIT"
            @AlertKey = "Error code:" + $errorcode

            @Summary = "evMainLoop returned " + $returnval + "(errno " + $errorcode + ")"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($returnval,$errorcode)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "returnval", $returnval, "errorcode", $errorcode)



        default:

            $UseJuniperJunosDefaults = 1
            @Summary = "Unknown System Log Message (" + $message-tag + ") Received "
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $prefix + " " + $message-tag
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

