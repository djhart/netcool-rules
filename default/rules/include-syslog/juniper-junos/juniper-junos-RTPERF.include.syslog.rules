###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  Compliant with JUNOS 12.2. For further information, refer to 
#              Juniper JUNOS 12.2 Syslog Reference Guide.
#
###############################################################################

case "RTPERF":

    log(DEBUG, "<<<<< Entering... juniper-junos-RTPERF.include.syslog.rules >>>>>")

    switch($message-tag)
    {

        case "RTPERF_CPU_THRESHOLD_EXCEEDED":


            ###############################
            #
            # System Log Message: 
            # FPC [fpc-slot] PIC [pic-slot] CPU utilization exceeds threshold,
            # current value=[current-value]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-RTPERF_CPU_THRESHOLD_EXCEEDED"

            $fpc-slot = extract($Details,"FPC (.*) PIC")
            $pic-slot = extract($Details,"PIC (.*) CPU")
            $current-value = extract($Details,",? current value (.*)$")

            @AlertGroup = "RTPERF_CPU_THRESHOLD_[EXCEEDED|OK]"
            @AlertKey = "FPC Slot:" + $fpc-slot + ", PIC Slot:" + $pic-slot

            @Summary = "FPC " + $fpc-slot + " PIC " + $pic-slot + " CPU utilization " +
                       "exceeds threshold, current value=" + $current-value

            $DEFAULT_Severity = 5
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($fpc-slot,$pic-slot,$current-value)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fpc-slot", $fpc-slot, "pic-slot", $pic-slot, "current-value", $current-value)


        case "RTPERF_CPU_USAGE_OK":


            ###############################
            #
            # System Log Message: 
            # FPC [fpc-slot] PIC [pic-slot] CPU utilization returns to normal,
            # current value=[current-value]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-RTPERF_CPU_USAGE_OK"

            $fpc-slot = extract($Details,"FPC (.*) PIC")
            $pic-slot = extract($Details,"PIC (.*) CPU")
            $current-value = extract($Details,",? current value (.*)$")

            @AlertGroup = "RTPERF_CPU_THRESHOLD_[EXCEEDED|OK]"
            @AlertKey = "FPC Slot:" + $fpc-slot + ", PIC Slot:" + $pic-slot

            @Summary = "FPC " + $fpc-slot + " PIC " + $pic-slot + " CPU utilization " +
                       "returns to normal, current value=" + $current-value

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($fpc-slot,$pic-slot,$current-value)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fpc-slot", $fpc-slot, "pic-slot", $pic-slot, "current-value", $current-value)



        default:

            $UseJuniperJunosDefaults = 1
            @Summary = "Unknown System Log Message (" + $message-tag + ") Received "
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $prefix + " " + $message-tag
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

