###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  Compliant with JUNOS 12.2. For further information, refer to 
#              Juniper JUNOS 12.2 Syslog Reference Guide.
#
###############################################################################

case "SSHD":

    log(DEBUG, "<<<<< Entering... juniper-junos-SSHD.include.syslog.rules >>>>>")

    switch($message-tag)
    {

        case "SSHD_LOGIN_ATTEMPTS_THRESHOLD":


            ###############################
            #
            # System Log Message: 
            # Threshold for unsuccessful authentication attempts ([limit]) reached by
            # user '[username]'
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SSHD_LOGIN_ATTEMPTS_THRESHOLD"

            $limit = extract($Details,"attempts \((.*)\) reached")
            $username = extract($Details,"by user (.*)$")

            @AlertGroup = "SSHD_LOGIN_ATTEMPTS_THRESHOLD"
            @AlertKey = "User:" + $username

            @Summary = "Threshold for unsuccessful authentication attempts (" + $limit + ")" + " reached by user " +
                       $username

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($limit,$username)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "limit", $limit, "username", $username)


        case "SSHD_LOGIN_FAILED":


            ###############################
            #
            # System Log Message: 
            # Login failed for user '[username]' from host '[source-address]'
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SSHD_LOGIN_FAILED"

            $username = extract($Details,"user (.*) from host")
            $source-address = extract($Details,"from host (.*)$")

            @AlertGroup = "SSHD_LOGIN_FAILED"
            @AlertKey = "User:" + $username + ", Source Address:" + $source-address

            @Summary = "Login failed for user " + $username + " from host " + $source-address

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($username,$source-address)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "username", $username, "source-address", $source-address)


        case "SSHD_LOGIN_FAILED_LIMIT":


            ###############################
            #
            # System Log Message: 
            # Specified number of login failures ([limit]) for user '[username]'
            # reached from '[remote-address]'
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SSHD_LOGIN_FAILED_LIMIT"

            $limit = extract($Details,"failures \((.*)\) for user")
            $username = extract($Details,"for user (.*) reached")
            $remote-address = extract($Details,"reached from (.*)$")

            @AlertGroup = "SSHD_LOGIN_FAILED_LIMIT"
            @AlertKey = "User:" + $username + ", Remote address:" + $remote-address

            @Summary = "Specified number of login failures (" + $limit + ") for user " +
                       $username + " reached from " + $remote-address

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($limit,$username,$remote-address)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "limit", $limit, "username", $username, "remote-address", $remote-address)



        default:

            $UseJuniperJunosDefaults = 1
            @Summary = "Unknown System Log Message (" + $message-tag + ") Received "
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $prefix + " " + $message-tag
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

