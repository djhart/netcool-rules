###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 1.1 - Updated Release.
#
#     - Set NmosEventMap field with event map name and precedence value.
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  Compliant with JUNOS 12.2. For further information, refer to 
#              Juniper JUNOS 12.2 Syslog Reference Guide.
#
###############################################################################

case "SNMP":

    log(DEBUG, "<<<<< Entering... juniper-junos-SNMP.include.syslog.rules >>>>>")

    switch($message-tag)
    {

        case "SNMP_GET_ERROR1":


            ###############################
            #
            # System Log Message: 
            # [function-name] [operation] failed for [object-name]: index1 [index1]
            # ([error-message])
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SNMP_GET_ERROR1"
            
            $function-operation = extract($Details, "SNMP_GET_ERROR1:(.*) failed ")
            $object = rtrim(extract($Details, "failed for (.*): index1"))
            $index1 = rtrim(extract($Details, "SNMP_GET_ERROR1:.*: index1 (.*) \(.*\)"))
            $reason = rtrim(extract($Details, "SNMP_GET_ERROR1:.*: index1 .* \((.*)\)"))

			@AlertGroup = "SNMP_GET_ERROR1 (" + $function-operation + ")"
            @AlertKey = "Object: " + $object + ", Index1: " + $index1
                
            @Summary = $function-operation + " failed for " + $object + ": index1 " + $index1 + " (" + $reason + ")"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($function-operation, $object, $index1, $reason)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "function-operation", $function-operation, "object", $object, "index1", $index1,
                 "reason", $reason)


        case "SNMP_GET_ERROR2":


            ###############################
            #
            # System Log Message: 
            # [function-name] [operation] failed for [object-name]: index1 [index1]
            # index2 [index2] ([error-message])
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SNMP_GET_ERROR2"

            $function-operation = extract($Details, "SNMP_GET_ERROR2:(.*) failed ")
            $object = rtrim(extract($Details, "failed for (.*): index1"))
            $index1 = rtrim(extract($Details, "SNMP_GET_ERROR2:.*: index1 (.*) index2"))
            $index2 = rtrim(extract($Details, "SNMP_GET_ERROR2:.*: index1 .* index2 (.*) \(.*\)"))
            $reason = rtrim(extract($Details, "SNMP_GET_ERROR2:.*: index1 .* index2 .* \((.*)\)"))

            @AlertGroup = "SNMP_GET_ERROR2 (" + $function-operation + ")"
            @AlertKey = "Object: " + $object + ", Index1: " + $index1 + ", Index2: " + $index2

            @Summary = $function-operation + " failed for " + $object + ": Index1 " 
                       + $index1 + " Index2 " + $index2 + " (" + $reason + ")"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($function-operation, $object, $index1, $index2, $reason)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "function-operation", $function-operation, "object", $object, "index1", $index1,
                 "index2", $index2, "reason", $reason)


        case "SNMP_GET_ERROR3":


            ###############################
            #
            # System Log Message: 
            # [function-name] [operation] failed for [object-name]: index1 [index1]
            # index2 [index2] index3 [index3] ([error-message])
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SNMP_GET_ERROR3"

            $function-operation = extract($Details, "SNMP_GET_ERROR3:(.*) failed ")
            $object = rtrim(extract($Details, "failed for (.*): index1"))
            $index1 = rtrim(extract($Details, "SNMP_GET_ERROR3:.*: index1 (.*) index2"))
            $index2 = rtrim(extract($Details, "SNMP_GET_ERROR3:.*: .* index2 (.*) index3"))
            $index3 = rtrim(extract($Details, "SNMP_GET_ERROR3:.*: index1 .* index3 (.*) \(.*\)"))
            $reason = rtrim(extract($Details, "SNMP_GET_ERROR3:.*: index1 .* index3 .* \((.*)\)"))

            @AlertGroup = "SNMP_GET_ERROR3 (" + $function-operation + ")"
            @AlertKey = "Object: " + $object + ", Index1: " + $index1 
                        + ", Index2: " + $index2 + ", Index3: " + $index3

            @Summary = $function-operation + " failed for " + $object + ": Index1 " 
                       + $index1 + " Index2 " + $index2  + " Index3 " 
                       + $index3 + " (" + $reason + ")"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($function-operation, $object, $index1, $index2, $index3, $reason)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "function-operation", $function-operation, "object", $object, "index1", $index1,
                 "index2", $index2, "index3", $index3, "reason", $reason)


        case "SNMP_GET_ERROR4":


            ###############################
            #
            # System Log Message: 
            # [function-name] [operation] failed for [object-name]: index1 [index1]
            # index2 [index2] index3 [index3] index4 [index4] ([error-message])
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SNMP_GET_ERROR4"

            $function-operation = extract($Details, "SNMP_GET_ERROR4:(.*) failed ")
            $object = rtrim(extract($Details, "failed for (.*): index1"))
            $index1 = rtrim(extract($Details, "SNMP_GET_ERROR4:.*: index1 (.*) index2"))
            $index2 = rtrim(extract($Details, "SNMP_GET_ERROR4:.*: .* index2 (.*) index3"))
            $index3 = rtrim(extract($Details, "SNMP_GET_ERROR4:.*: index1 .* index3 (.*) index4 .* \(.*\)"))
            $index4 = rtrim(extract($Details, "SNMP_GET_ERROR4:.*: index1 .* index4 (.*) \(.*\)"))
            $reason = rtrim(extract($Details, "SNMP_GET_ERROR4:.*: index1 .* index4 .* \((.*)\)"))

            @AlertGroup = "SNMP_GET_ERROR4 (" + $function-operation + ")"
            @AlertKey = "Object: " + $object + ", Index1: " + $index1 
                        + ", Index2: " + $index2 + ", Index3: " + $index3
                        + ", Index4: " + $index4

            @Summary = $function-operation + " failed for " + $object + ": Index1 " 
                       + $index1 + " Index2 " + $index2  + " Index3 " 
                       + $index3 + " Index4 " + $index4 + " (" + $reason + ")"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($function-operation, $object, $index1, $index2, $index3,$index4, $reason)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "function-operation", $function-operation, "object", $object, "index1", $index1,
                 "index2", $index2, "index3", $index3, "index4", $index4,
                 "reason", $reason)


        case "SNMP_RTSLIB_FAILURE":


            ###############################
            #
            # System Log Message: 
            # [function-name]: [reason]: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SNMP_RTSLIB_FAILURE"

            $function = extract($Details, "SNMP_RTSLIB_FAILURE: (.*): .*: .*")
            $reason = rtrim(extract($Details, "SNMP_RTSLIB_FAILURE: .*: (.*):"))
            $errmsg = rtrim(extract($Details, "SNMP_RTSLIB_FAILURE: .*: .*: (.*)"))

            @AlertGroup = "SNMP_RTSLIB_FAILURE (" + $function + ")"
            @AlertKey = "Function:" + $function + ", Reason: " + $reason

            @Summary = $function + ": " + $reason + ": " + $errmsg

            $DEFAULT_Severity = 5
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($function, $reason, $errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "function", $function, "reason", $reason, "errmsg", $errmsg)


        case "SNMP_TRAP_LINK_DOWN":


            ###############################
            #
            # System Log Message: 
            # ifIndex [snmp-interface-index], ifAdminStatus [admin-status],
            # ifOperStatus [operational-status], ifName [interface-name]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SNMP_TRAP_LINK_DOWN"
            @NmosEventMap = "LinkDownIfDescr.910"

            $ifIndex = extract($Details, "ifIndex (.*),? ifAdminStatus")
            $ifAdminStatus = rtrim(extract($Details, "ifAdminStatus (.*),? ifOperStatus"))
            $ifOperStatus = rtrim(extract($Details, "ifOperStatus (.*),? ifName"))                
            $ifName = rtrim(extract($Details, "ifName (.*)"))

            @AlertGroup = "SNMP_TRAP_LINK[UP|DOWN]"
            @AlertKey = "Interface index: " + $ifIndex + ", Interface name: " + $ifName

            @Summary = "Link Down: ifIndex " + $ifIndex 
                        + ", ifAdminStatus " + $ifAdminStatus 
                        + ", ifOperStatus " + $ifOperStatus 
                        + ", ifName " + $ifName

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($ifIndex, $ifAdminStatus, $ifOperStatus, $ifName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "ifIndex", $ifIndex, "ifAdminStatus", $ifAdminStatus, "ifOperStatus", $ifOperStatus,
                 "ifName", $ifName)


        case "SNMP_TRAP_LINK_UP":


            ###############################
            #
            # System Log Message: 
            # ifIndex [snmp-interface-index], ifAdminStatus [admin-status],
            # ifOperStatus [operational-status], ifName [interface-name]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SNMP_TRAP_LINK_UP"

            $ifIndex = extract($Details, "ifIndex (.*),? ifAdminStatus")
            $ifAdminStatus = rtrim(extract($Details, "ifAdminStatus (.*),? ifOperStatus"))
            $ifOperStatus = rtrim(extract($Details, "ifOperStatus (.*),? ifName"))                
            $ifName = rtrim(extract($Details, "ifName (.*)"))

            @AlertGroup = "SNMP_TRAP_LINK[UP|DOWN]"
            @AlertKey = "Interface index: " + $ifIndex + ", Interface name: " + $ifName

            @Summary = "Link Up: ifIndex " + $ifIndex 
                        + ", ifAdminStatus " + $ifAdminStatus 
                        + ", ifOperStatus " + $ifOperStatus 
                        + ", ifName " + $ifName

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($ifIndex, $ifAdminStatus, $ifOperStatus, $ifName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "ifIndex", $ifIndex, "ifAdminStatus", $ifAdminStatus, "ifOperStatus", $ifOperStatus,
                 "ifName", $ifName)


        case "SNMP_TRAP_TRACERT_PATH_CHANGE":


            ###############################
            #
            # System Log Message: 
            # traceRouteCtlOwnerIndex = [test-owner], traceRouteCtlTestName =
            # [test-name]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SNMP_TRAP_TRACERT_PATH_CHANGE"

            $traceRouteCtlOwnerIndex = ltrim(rtrim(extract($Details,"traceRouteCtlOwnerIndex (.*),? traceRouteCtlTestName")))
            $traceRouteCtlTestName = ltrim(rtrim(extract($Details,"traceRouteCtlTestName (.*)$")))

            @AlertGroup = "SNMP_TRAP_TRACERT_PATH_CHANGE"
            @AlertKey = "traceRouteCtlOwnerIndex." + $traceRouteCtlOwnerIndex
                        + ", traceRouteCtlTestName." + $traceRouteCtlTestName 

            @Summary = "traceRouteCtlOwnerIndex = " + $traceRouteCtlOwnerIndex
                        + " traceRouteCtlTestName = " + $traceRouteCtlTestName 

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($traceRouteCtlOwnerIndex,$traceRouteCtlTestName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "traceRouteCtlOwnerIndex", $traceRouteCtlOwnerIndex, "traceRouteCtlTestName", $traceRouteCtlTestName)


        case "SNMP_TRAP_TRACERT_TEST_COMPLETED":


            ###############################
            #
            # System Log Message: 
            # traceRouteCtlOwnerIndex = [test-owner], traceRouteCtlTestName =
            # [test-name]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SNMP_TRAP_TRACERT_TEST_COMPLETED"

            $traceRouteCtlOwnerIndex = ltrim(rtrim(extract($Details,"traceRouteCtlOwnerIndex (.*),? traceRouteCtlTestName")))
            $traceRouteCtlTestName = ltrim(rtrim(extract($Details,"traceRouteCtlTestName (.*)$")))

            @AlertGroup = "SNMP_TRAP_TRACERT_TEST_[COMPLETED|FAILED]"
            @AlertKey = "traceRouteCtlOwnerIndex." + $traceRouteCtlOwnerIndex
                        + ", traceRouteCtlTestName." + $traceRouteCtlTestName 

            @Summary = "traceRouteCtlOwnerIndex = " + $traceRouteCtlOwnerIndex
                        + " traceRouteCtlTestName = " + $traceRouteCtlTestName 

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($traceRouteCtlOwnerIndex,$traceRouteCtlTestName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "traceRouteCtlOwnerIndex", $traceRouteCtlOwnerIndex, "traceRouteCtlTestName", $traceRouteCtlTestName)


        case "SNMP_TRAP_TRACERT_TEST_FAILED":


            ###############################
            #
            # System Log Message: 
            # traceRouteCtlOwnerIndex = [test-owner], traceRouteCtlTestName =
            # [test-name]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SNMP_TRAP_TRACERT_TEST_FAILED"

            $traceRouteCtlOwnerIndex = ltrim(rtrim(extract($Details,"traceRouteCtlOwnerIndex (.*),? traceRouteCtlTestName")))
            $traceRouteCtlTestName = ltrim(rtrim(extract($Details,"traceRouteCtlTestName (.*)$")))

            @AlertGroup = "SNMP_TRAP_TRACERT_TEST_[COMPLETED|FAILED]"
            @AlertKey = "traceRouteCtlOwnerIndex." + $traceRouteCtlOwnerIndex
                        + ", traceRouteCtlTestName." + $traceRouteCtlTestName 

            @Summary = "traceRouteCtlOwnerIndex = " + $traceRouteCtlOwnerIndex
                        + " traceRouteCtlTestName = " + $traceRouteCtlTestName 

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($traceRouteCtlOwnerIndex,$traceRouteCtlTestName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "traceRouteCtlOwnerIndex", $traceRouteCtlOwnerIndex, "traceRouteCtlTestName", $traceRouteCtlTestName)



        default:

            $UseJuniperJunosDefaults = 1
            @Summary = "Unknown System Log Message (" + $message-tag + ") Received "
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $prefix + " " + $message-tag
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

