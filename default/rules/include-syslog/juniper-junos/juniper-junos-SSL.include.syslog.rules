###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  Compliant with JUNOS 12.2. For further information, refer to 
#              Juniper JUNOS 12.2 Syslog Reference Guide.
#
###############################################################################

case "SSL":

    log(DEBUG, "<<<<< Entering... juniper-junos-SSL.include.syslog.rules >>>>>")

    switch($message-tag)
    {

        case "SSL_PROXY_ERROR":


            ###############################
            #
            # System Log Message: 
            # lsys:[logical-system-name] [session-id]
            # [<source-address/source-port]->[destination-address/destination-port>]
            # NAT:[<nat-source-address/nat-source-port]->[nat-destination-address/nat-destination-port>]
            # [profile-name]
            # [<source-zone-name:source-interface-name]->[destination-zone-name:destination-interface-name>]
            # message: [message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SSL_PROXY_ERROR"

            $lsys-name = extract($Details,"SSL_PROXY_ERROR: lsys:(.*) .* .*->.*->.*->.*")
            $session-id = extract($Details,"SSL_PROXY_ERROR: lsys:.* (.*) .*->.*->.*->.*")
            $source-address = extract($Details,"SSL_PROXY_ERROR: lsys:.* .* (.*)->.*->.*->.*")
            $destination-address = extract($Details,"->(.*) NAT:.*->.* .* .*->.*")
            $nat-source-address = extract($Details,"->.* NAT:(.*)->.* .* .*->.*")
            $nat-destination-address = extract($Details,"->.* NAT:.*->(.*) .* .*->.*")
            $profile-name = extract($Details,"->.* NAT:.*->.* (.*) .*->.*")
            $source-zone-interface-name = extract($Details,"->.* NAT:.*->.* .* (.*)->.*")
            $destination-zone-interface-name = extract($Details,"->.* NAT:.*->.* .* .*->(.*) message: .*")
            $message = extract($Details, ".* message: (.*)$")

            @AlertGroup = "SSL_PROXY_ERROR"
            @AlertKey = "LSYS:" + $lsys-name + ", Source: " + $source-address + ", Profile Name:" + $profile-name

            $Summary = "lsys:" + $lsys-name + " " + $session-id + " " + $source-address + "->" + $destination-address +
 " NAT:" + $nat-source-address + "->" + $nat-destination-address + " " + $profile-name + " " + $source-zone-interface-name + "->" + $destination-zone-interface-name + " message: " + $message
 
            if ( length($Summary) > 255 ) 
            {
            	@Summary = substr($Summary,1,255)
            }
            else
            {
            	@Summary = $Summary
            }

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($lsys-name,$session-id,$source-address,$destination-address,$nat-source-address,$nat-destination-address,
                        $profile-name,$source-zone-interface-name,$destination-zone-interface-name,$message)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "lsys-name", $lsys-name, "session-id", $session-id, "source-address", $source-address,
                 "destination-address", $destination-address, "nat-source-address", $nat-source-address, "nat-destination-address", $nat-destination-address,
                 "profile-name", $profile-name, "source-zone-interface-name", $source-zone-interface-name, "destination-zone-interface-name", $destination-zone-interface-name,
                 "message", $message)


        case "SSL_PROXY_INFO":


            ###############################
            #
            # System Log Message: 
            # lsys:[logical-system-name] [session-id]
            # [<source-address/source-port]->[destination-address/destination-port>]
            # NAT:[<nat-source-address/nat-source-port]->[nat-destination-address/nat-destination-port>]
            # [profile-name]
            # [<source-zone-name:source-interface-name]->[destination-zone-name:destination-interface-name>]
            # message: [message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SSL_PROXY_INFO"

            $lsys-name = extract($Details,"SSL_PROXY_INFO: lsys:(.*) .* .*->.*->.*->.*")
            $session-id = extract($Details,"SSL_PROXY_INFO: lsys:.* (.*) .*->.*->.*->.*")
            $source-address = extract($Details,"SSL_PROXY_INFO: lsys:.* .* (.*)->.*->.*->.*")
            $destination-address = extract($Details,"->(.*) NAT:.*->.* .* .*->.*")
            $nat-source-address = extract($Details,"->.* NAT:(.*)->.* .* .*->.*")
            $nat-destination-address = extract($Details,"->.* NAT:.*->(.*) .* .*->.*")
            $profile-name = extract($Details,"->.* NAT:.*->.* (.*) .*->.*")
            $source-zone-interface-name = extract($Details,"->.* NAT:.*->.* .* (.*)->.*")
            $destination-zone-interface-name = extract($Details,"->.* NAT:.*->.* .* .*->(.*) message: .*")
            $message = extract($Details, ".* message: (.*)$")

            @AlertGroup = "SSL_PROXY_INFO"
            @AlertKey = "LSYS:" + $lsys-name + ", Source: " + $source-address + ", Profile Name:" + $profile-name

            $Summary = "lsys:" + $lsys-name + " " + $session-id + " " + $source-address + "->" + $destination-address +
 " NAT:" + $nat-source-address + "->" + $nat-destination-address + " " + $profile-name + " " + $source-zone-interface-name + "->" + $destination-zone-interface-name + " message: " + $message
 
            if ( length($Summary) > 255 ) 
            {
            	@Summary = substr($Summary,1,255)
            }
            else
            {
            	@Summary = $Summary
            }

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($lsys-name,$session-id,$source-address,$destination-address,$nat-source-address,$nat-destination-address,
                        $profile-name,$source-zone-interface-name,$destination-zone-interface-name,$message)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "lsys-name", $lsys-name, "session-id", $session-id, "source-address", $source-address,
                 "destination-address", $destination-address, "nat-source-address", $nat-source-address, "nat-destination-address", $nat-destination-address,
                 "profile-name", $profile-name, "source-zone-interface-name", $source-zone-interface-name, "destination-zone-interface-name", $destination-zone-interface-name,
                 "message", $message)


        case "SSL_PROXY_SESSION_IGNORE":


            ###############################
            #
            # System Log Message: 
            # lsys:[logical-system-name] [session-id]
            # [<source-address/source-port]->[destination-address/destination-port>]
            # NAT:[<nat-source-address/nat-source-port]->[nat-destination-address/nat-destination-port>]
            # [profile-name]
            # [<source-zone-name:source-interface-name]->[destination-zone-name:destination-interface-name>]
            # message: [message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SSL_PROXY_SESSION_IGNORE"

            $lsys-name = extract($Details,"SSL_PROXY_SESSION_IGNORE: lsys:(.*) .* .*->.*->.*->.*")
            $session-id = extract($Details,"SSL_PROXY_SESSION_IGNORE: lsys:.* (.*) .*->.*->.*->.*")
            $source-address = extract($Details,"SSL_PROXY_SESSION_IGNORE: lsys:.* .* (.*)->.*->.*->.*")
            $destination-address = extract($Details,"->(.*) NAT:.*->.* .* .*->.*")
            $nat-source-address = extract($Details,"->.* NAT:(.*)->.* .* .*->.*")
            $nat-destination-address = extract($Details,"->.* NAT:.*->(.*) .* .*->.*")
            $profile-name = extract($Details,"->.* NAT:.*->.* (.*) .*->.*")
            $source-zone-interface-name = extract($Details,"->.* NAT:.*->.* .* (.*)->.*")
            $destination-zone-interface-name = extract($Details,"->.* NAT:.*->.* .* .*->(.*) message: .*")
            $message = extract($Details, ".* message: (.*)$")

            @AlertGroup = "SSL_PROXY_SESSION_IGNORE"
            @AlertKey = "LSYS:" + $lsys-name + ", Source: " + $source-address + ", Profile Name:" + $profile-name

            $Summary = "lsys:" + $lsys-name + " " + $session-id + " " + $source-address + "->" + $destination-address +
 " NAT:" + $nat-source-address + "->" + $nat-destination-address + " " + $profile-name + " " + $source-zone-interface-name + "->" + $destination-zone-interface-name + " message: " + $message

            if ( length($Summary) > 255 ) 
            {
            	@Summary = substr($Summary,1,255)
            }
            else
            {
            	@Summary = $Summary
            }

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($lsys-name,$session-id,$source-address,$destination-address,$nat-source-address,$nat-destination-address,
                        $profile-name,$source-zone-interface-name,$destination-zone-interface-name,$message)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "lsys-name", $lsys-name, "session-id", $session-id, "source-address", $source-address,
                 "destination-address", $destination-address, "nat-source-address", $nat-source-address, "nat-destination-address", $nat-destination-address,
                 "profile-name", $profile-name, "source-zone-interface-name", $source-zone-interface-name, "destination-zone-interface-name", $destination-zone-interface-name,
                 "message", $message)


        case "SSL_PROXY_SESSION_WHITELIST":


            ###############################
            #
            # System Log Message: 
            # lsys:[logical-system-name] [session-id] host:[url]
            # [<source-address/source-port]->[destination-address/destination-port>]
            # NAT:[<nat-source-address/nat-source-port]->[nat-destination-address/nat-destination-port>]
            # [profile-name]
            # [<source-zone-name:source-interface-name]->[destination-zone-name:destination-interface-name>]
            # message: [message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SSL_PROXY_SESSION_WHITELIST"

            $lsys-name = extract($Details,"SSL_PROXY_SESSION_WHITELIST: lsys:(.*) .* host:.* .*->.*->.*->.*")
            $session-id = extract($Details,"SSL_PROXY_SESSION_WHITELIST: lsys:.* (.*) host:.* .*->.*->.*->.*")
            $url = extract($Details,"SSL_PROXY_SESSION_WHITELIST: lsys:.* .* host:(.*) .*->.*->.*->.*")
            $source-address = extract($Details,"SSL_PROXY_SESSION_WHITELIST: lsys:.* .* host:.* (.*)->.*->.*->.*")
            $destination-address = extract($Details,"->(.*) NAT:.*->.* .* .*->.*")
            $nat-source-address = extract($Details,"->.* NAT:(.*)->.* .* .*->.*")
            $nat-destination-address = extract($Details,"->.* NAT:.*->(.*) .* .*->.*")
            $profile-name = extract($Details,"->.* NAT:.*->.* (.*) .*->.*")
            $source-zone-interface-name = extract($Details,"->.* NAT:.*->.* .* (.*)->.*")
            $destination-zone-interface-name = extract($Details,"->.* NAT:.*->.* .* .*->(.*) message: .*")
            $message = extract($Details, ".* message: (.*)$")

            @AlertGroup = "SSL_PROXY_SESSION_WHITELIST"
            @AlertKey = "LSYS:" + $lsys-name + ", Source: " + $source-address + ", Profile Name:" + $profile-name

            $Summary = "lsys:" + $lsys-name + " " + $session-id + " " + $source-address + "->" + $destination-address +
 " NAT:" + $nat-source-address + "->" + $nat-destination-address + " " + $profile-name + " " + $source-zone-interface-name + "->" + $destination-zone-interface-name + " message: " + $message
 
            if ( length($Summary) > 255 ) 
            {
            	@Summary = substr($Summary,1,255)
            }
            else
            {
            	@Summary = $Summary
            }

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($lsys-name,$session-id,$url,$source-address,$destination-address,$nat-source-address,$nat-destination-address,
                        $profile-name,$source-zone-interface-name,$destination-zone-interface-name,$message)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "lsys-name", $lsys-name, "session-id", $session-id, "url", $url,
                 "source-address", $source-address, "destination-address", $destination-address, "nat-source-address", $nat-source-address,
                 "nat-destination-address", $nat-destination-address, "profile-name", $profile-name, "source-zone-interface-name", $source-zone-interface-name,
                 "destination-zone-interface-name", $destination-zone-interface-name, "message", $message)


        case "SSL_PROXY_SSL_SESSION_ALLOW":


            ###############################
            #
            # System Log Message: 
            # lsys:[logical-system-name] [session-id]
            # [<source-address/source-port]->[destination-address/destination-port>]
            # NAT:[<nat-source-address/nat-source-port]->[nat-destination-address/nat-destination-port>]
            # [profile-name]
            # [<source-zone-name:source-interface-name]->[destination-zone-name:destination-interface-name>]
            # message: [message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SSL_PROXY_SSL_SESSION_ALLOW"

            $lsys-name = extract($Details,"SSL_PROXY_SSL_SESSION_ALLOW: lsys:(.*) .* .*->.*->.*->.*")
            $session-id = extract($Details,"SSL_PROXY_SSL_SESSION_ALLOW: lsys:.* (.*) .*->.*->.*->.*")
            $source-address = extract($Details,"SSL_PROXY_SSL_SESSION_ALLOW: lsys:.* .* (.*)->.*->.*->.*")
            $destination-address = extract($Details,"->(.*) NAT:.*->.* .* .*->.*")
            $nat-source-address = extract($Details,"->.* NAT:(.*)->.* .* .*->.*")
            $nat-destination-address = extract($Details,"->.* NAT:.*->(.*) .* .*->.*")
            $profile-name = extract($Details,"->.* NAT:.*->.* (.*) .*->.*")
            $source-zone-interface-name = extract($Details,"->.* NAT:.*->.* .* (.*)->.*")
            $destination-zone-interface-name = extract($Details,"->.* NAT:.*->.* .* .*->(.*) message: .*")
            $message = extract($Details, ".* message: (.*)$")

            @AlertGroup = "SSL_PROXY_SSL_SESSION_ALLOW"
            @AlertKey = "LSYS:" + $lsys-name + ", Source: " + $source-address + ", Profile Name:" + $profile-name

            $Summary = "lsys:" + $lsys-name + " " + $session-id + " " + $source-address + "->" + $destination-address +
 " NAT:" + $nat-source-address + "->" + $nat-destination-address + " " + $profile-name + " " + $source-zone-interface-name + "->" + $destination-zone-interface-name + " message: " + $message

            if ( length($Summary) > 255 ) 
            {
            	@Summary = substr($Summary,1,255)
            }
            else
            {
            	@Summary = $Summary
            }

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($lsys-name,$session-id,$source-address,$destination-address,$nat-source-address,$nat-destination-address,
                        $profile-name,$source-zone-interface-name,$destination-zone-interface-name,$message)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "lsys-name", $lsys-name, "session-id", $session-id, "source-address", $source-address,
                 "destination-address", $destination-address, "nat-source-address", $nat-source-address, "nat-destination-address", $nat-destination-address,
                 "profile-name", $profile-name, "source-zone-interface-name", $source-zone-interface-name, "destination-zone-interface-name", $destination-zone-interface-name,
                 "message", $message)


        case "SSL_PROXY_SSL_SESSION_DROP":


            ###############################
            #
            # System Log Message: 
            # lsys:[logical-system-name] [session-id]
            # [<source-address/source-port]->[destination-address/destination-port>]
            # NAT:[<nat-source-address/nat-source-port]->[nat-destination-address/nat-destination-port>]
            # [profile-name]
            # [<source-zone-name:source-interface-name]->[destination-zone-name:destination-interface-name>]
            # message: [message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SSL_PROXY_SSL_SESSION_DROP"

            $lsys-name = extract($Details,"SSL_PROXY_SSL_SESSION_DROP: lsys:(.*) .* .*->.*->.*->.*")
            $session-id = extract($Details,"SSL_PROXY_SSL_SESSION_DROP: lsys:.* (.*) .*->.*->.*->.*")
            $source-address = extract($Details,"SSL_PROXY_SSL_SESSION_DROP: lsys:.* .* (.*)->.*->.*->.*")
            $destination-address = extract($Details,"->(.*) NAT:.*->.* .* .*->.*")
            $nat-source-address = extract($Details,"->.* NAT:(.*)->.* .* .*->.*")
            $nat-destination-address = extract($Details,"->.* NAT:.*->(.*) .* .*->.*")
            $profile-name = extract($Details,"->.* NAT:.*->.* (.*) .*->.*")
            $source-zone-interface-name = extract($Details,"->.* NAT:.*->.* .* (.*)->.*")
            $destination-zone-interface-name = extract($Details,"->.* NAT:.*->.* .* .*->(.*) message: .*")
            $message = extract($Details, ".* message: (.*)$")

            @AlertGroup = "SSL_PROXY_SSL_SESSION_DROP"
            @AlertKey = "LSYS:" + $lsys-name + ", Source: " + $source-address + ", Profile Name:" + $profile-name

            $Summary = "lsys:" + $lsys-name + " " + $session-id + " " + $source-address + "->" + $destination-address +
 " NAT:" + $nat-source-address + "->" + $nat-destination-address + " " + $profile-name + " " + $source-zone-interface-name + "->" + $destination-zone-interface-name + " message: " + $message

            if ( length($Summary) > 255 ) 
            {
            	@Summary = substr($Summary,1,255)
            }
            else
            {
            	@Summary = $Summary
            }

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($lsys-name,$session-id,$source-address,$destination-address,$nat-source-address,$nat-destination-address,
                        $profile-name,$source-zone-interface-name,$destination-zone-interface-name,$message)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "lsys-name", $lsys-name, "session-id", $session-id, "source-address", $source-address,
                 "destination-address", $destination-address, "nat-source-address", $nat-source-address, "nat-destination-address", $nat-destination-address,
                 "profile-name", $profile-name, "source-zone-interface-name", $source-zone-interface-name, "destination-zone-interface-name", $destination-zone-interface-name,
                 "message", $message)


        case "SSL_PROXY_WARNING":


            ###############################
            #
            # System Log Message: 
            # lsys:[logical-system-name] [session-id]
            # [<source-address/source-port]->[destination-address/destination-port>]
            # NAT:[<nat-source-address/nat-source-port]->[nat-destination-address/nat-destination-port>]
            # [profile-name]
            # [<source-zone-name:source-interface-name]->[destination-zone-name:destination-interface-name>]
            # message: [message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SSL_PROXY_WARNING"

            $lsys-name = extract($Details,"SSL_PROXY_WARNING: lsys:(.*) .* .*->.*->.*->.*")
            $session-id = extract($Details,"SSL_PROXY_WARNING: lsys:.* (.*) .*->.*->.*->.*")
            $source-address = extract($Details,"SSL_PROXY_WARNING: lsys:.* .* (.*)->.*->.*->.*")
            $destination-address = extract($Details,"->(.*) NAT:.*->.* .* .*->.*")
            $nat-source-address = extract($Details,"->.* NAT:(.*)->.* .* .*->.*")
            $nat-destination-address = extract($Details,"->.* NAT:.*->(.*) .* .*->.*")
            $profile-name = extract($Details,"->.* NAT:.*->.* (.*) .*->.*")
            $source-zone-interface-name = extract($Details,"->.* NAT:.*->.* .* (.*)->.*")
            $destination-zone-interface-name = extract($Details,"->.* NAT:.*->.* .* .*->(.*) message: .*")
            $message = extract($Details, ".* message: (.*)$")

            @AlertGroup = "SSL_PROXY_WARNING"
            @AlertKey = "LSYS:" + $lsys-name + ", Source: " + $source-address + ", Profile Name:" + $profile-name

            $Summary = "lsys:" + $lsys-name + " " + $session-id + " " + $source-address + "->" + $destination-address +
 " NAT:" + $nat-source-address + "->" + $nat-destination-address + " " + $profile-name + " " + $source-zone-interface-name + "->" + $destination-zone-interface-name + " message: " + $message

            if ( length($Summary) > 255 ) 
            {
            	@Summary = substr($Summary,1,255)
            }
            else
            {
            	@Summary = $Summary
            }

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($lsys-name,$session-id,$source-address,$destination-address,$nat-source-address,$nat-destination-address,
                        $profile-name,$source-zone-interface-name,$destination-zone-interface-name,$message)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "lsys-name", $lsys-name, "session-id", $session-id, "source-address", $source-address,
                 "destination-address", $destination-address, "nat-source-address", $nat-source-address, "nat-destination-address", $nat-destination-address,
                 "profile-name", $profile-name, "source-zone-interface-name", $source-zone-interface-name, "destination-zone-interface-name", $destination-zone-interface-name,
                 "message", $message)



        default:

            $UseJuniperJunosDefaults = 1
            @Summary = "Unknown System Log Message (" + $message-tag + ") Received "
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $prefix + " " + $message-tag
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

