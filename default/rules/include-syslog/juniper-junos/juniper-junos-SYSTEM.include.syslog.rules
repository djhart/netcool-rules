###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  Compliant with JUNOS 12.2. For further information, refer to 
#              Juniper JUNOS 12.2 Syslog Reference Guide.
#
###############################################################################

case "SYSTEM":

    log(DEBUG, "<<<<< Entering... juniper-junos-SYSTEM.include.syslog.rules >>>>>")

    switch($message-tag)
    {

        case "SYSTEM_ABNORMAL_SHUTDOWN":


            ###############################
            #
            # System Log Message: 
            # System abnormally shut down
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SYSTEM_ABNORMAL_SHUTDOWN"

            @AlertGroup = "SYSTEM_STATUS"
            @AlertKey = ""

            @Summary = "System abnormally shut down"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            


        case "SYSTEM_OPERATIONAL":


            ###############################
            #
            # System Log Message: 
            # System is operational
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SYSTEM_OPERATIONAL"

            @AlertGroup = "SYSTEM_STATUS"
            @AlertKey = ""

            @Summary = "System is operational"

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            


        case "SYSTEM_SHUTDOWN":


            ###############################
            #
            # System Log Message: 
            # System [type] by [username] at [time]: [message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SYSTEM_SHUTDOWN"

            $type = ltrim(rtrim(extract($Details,"System (.*) by")))
            $user = ltrim(rtrim(extract($Details,"type by (.*) at")))
            $time = ltrim(rtrim(extract($Details,"at (.*):")))
            $message = ltrim(rtrim(extract($Details,":.*:(.*)$")))

            @AlertGroup = "SYSTEM_SHUTDOWN"
            @AlertKey = "Type:" + $type + ", User:" + $user + ", Time:" + $time

            @Summary = "System " + $type + " by " + $user + " at " + $time + ": " + $message

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($type,$user,$time,$message)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "type", $type, "user", $user, "time", $time,
                 "message", $message)



        default:

            $UseJuniperJunosDefaults = 1
            @Summary = "Unknown System Log Message (" + $message-tag + ") Received "
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $prefix + " " + $message-tag
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

