###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  Compliant with JUNOS 12.2. For further information, refer to 
#              Juniper JUNOS 12.2 Syslog Reference Guide.
#
###############################################################################

case "CFMD":

    log(DEBUG, "<<<<< Entering... juniper-junos-CFMD.include.syslog.rules >>>>>")

    switch($message-tag)
    {

        case "CFMD_CCM_DEFECT_CROSS_CONNECT":


            ###############################
            #
            # System Log Message: 
            # CFM defect: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-CFMD_CCM_DEFECT_CROSS_CONNECT"

            $errmsg = ltrim(rtrim(extract($Details,"CFM defect: (.*)")))

            @AlertGroup = "CFMD_CCM_DEFECT_CROSS_CONNECT"
            @AlertKey = "Error:" + $errmsg

            @Summary = "CFM defect: " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "errmsg", $errmsg)


        case "CFMD_CCM_DEFECT_ERROR":


            ###############################
            #
            # System Log Message: 
            # CFM defect: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-CFMD_CCM_DEFECT_ERROR"

            $errmsg = ltrim(rtrim(extract($Details,"CFM defect: (.*)")))

            @AlertGroup = "CFMD_CCM_DEFECT_ERROR"
            @AlertKey = "Error:" + $errmsg

            @Summary = "CFM defect: " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "errmsg", $errmsg)


        case "CFMD_CCM_DEFECT_MAC_STATUS":


            ###############################
            #
            # System Log Message: 
            # CFM defect: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-CFMD_CCM_DEFECT_MAC_STATUS"

            $errmsg = ltrim(rtrim(extract($Details,"CFM defect: (.*)")))

            @AlertGroup = "CFMD_CCM_DEFECT_MAC_STATUS"
            @AlertKey = "Error:" + $errmsg

            @Summary = "CFM defect: " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "errmsg", $errmsg)


        case "CFMD_CCM_DEFECT_NONE":


            ###############################
            #
            # System Log Message: 
            # CFM defect: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-CFMD_CCM_DEFECT_NONE"

            $errmsg = ltrim(rtrim(extract($Details,"CFM defect: (.*)")))

            @AlertGroup = "CFMD_CCM_DEFECT_NONE"
            @AlertKey = "Error:" + $errmsg

            @Summary = "CFM defect: " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "errmsg", $errmsg)


        case "CFMD_CCM_DEFECT_RDI":


            ###############################
            #
            # System Log Message: 
            # CFM defect: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-CFMD_CCM_DEFECT_RDI"

            $errmsg = ltrim(rtrim(extract($Details,"CFM defect: (.*)")))

            @AlertGroup = "CFMD_CCM_DEFECT_RDI"
            @AlertKey = "Error:" + $errmsg

            @Summary = "CFM defect: " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "errmsg", $errmsg)

        case "CFMD_CCM_DEFECT_RMEP":


            ###############################
            #
            # System Log Message: 
            # CFM defect: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-CFMD_CCM_DEFECT_RMEP"

            $errmsg = ltrim(rtrim(extract($Details,"CFM defect: (.*)")))

            @AlertGroup = "CFMD_CCM_DEFECT_RMEP"
            @AlertKey = "Error:" + $errmsg

            @Summary = "CFM defect: " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "errmsg", $errmsg)


        case "CFMD_CCM_DEFECT_UNKNOWN":


            ###############################
            #
            # System Log Message: 
            # CFM defect: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-CFMD_CCM_DEFECT_UNKNOWN"

            $errmsg = ltrim(rtrim(extract($Details,"CFM defect: (.*)")))

            @AlertGroup = "CFMD_CCM_DEFECT_UNKNOWN"
            @AlertKey = "Error:" + $errmsg

            @Summary = "CFM defect: " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "errmsg", $errmsg)


        case "CFMD_PPM_READ_ERROR":


            ###############################
            #
            # System Log Message: 
            # Read error on pipe from ppmd: [reason] ([error-message])
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-CFMD_PPM_READ_ERROR"

            $reason = ltrim(rtrim(extract($Details,"ppmd: (.*) \(.*\)")))
            $errmsg = ltrim(rtrim(extract($Details,"ppmd: .* \((.*)\)")))

            @AlertGroup = "CFMD_PPM_READ_ERROR"
            @AlertKey = "Reason:" + $reason + ",Error:" + $errmsg

            @Summary = "Read error on pipe from ppmd:" + $reason + "(" + $errmsg + ")"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($reason,$errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "reason", $reason, "errmsg", $errmsg)


        case "CFMD_PPM_WRITE_ERROR":


            ###############################
            #
            # System Log Message: 
            # [function-name]: write error on pipe to ppmd ([error-message])
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-CFMD_PPM_WRITE_ERROR"

            $function-name = ltrim(rtrim(extract($Details,"CFMD_PPM_WRITE_ERROR:(.*): write")))
            $errmsg = ltrim(rtrim(extract($Details,"to ppmd \((.*)\)")))

            @AlertGroup = "CFMD_PPM_WRITE_ERROR"
            @AlertKey = "Function:" + $function-name

            @Summary = $function-name + ": write error on pipe to ppmd " + "(" + $errmsg + ")"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($function-name,$errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "function-name", $function-name, "errmsg", $errmsg)



        default:

            $UseJuniperJunosDefaults = 1
            @Summary = "Unknown System Log Message (" + $message-tag + ") Received "
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $prefix + " " + $message-tag
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

