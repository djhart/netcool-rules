###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  Compliant with JUNOS 12.2. For further information, refer to 
#              Juniper JUNOS 12.2 Syslog Reference Guide.
#
###############################################################################

case "WEBFILTER":

    log(DEBUG, "<<<<< Entering... juniper-junos-WEBFILTER.include.syslog.rules >>>>>")

    switch($message-tag)
    {

        case "WEBFILTER_CACHE_NOT_ENABLED":


            ###############################
            #
            # System Log Message: 
            # Failed to enable the web-filtering category cache
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEBFILTER_CACHE_NOT_ENABLED"

            @AlertGroup = "WEBFILTER_CACHE_NOT_ENABLED"
            @AlertKey = ""

            @Summary = "Failed to enable the web-filtering category cache"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            


        case "WEBFILTER_INTERNAL_ERROR":


            ###############################
            #
            # System Log Message: 
            # Error encountered: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEBFILTER_INTERNAL_ERROR"

            $errmsg = extract($Details,"encountered: (.*)$")

            @AlertGroup = "WEBFILTER_INTERNAL_ERROR"
            @AlertKey = "Error:" + $errmsg

            @Summary = "Error encountered: " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "errmsg", $errmsg)


        case "WEBFILTER_REQUEST_NOT_CHECKED":


            ###############################
            #
            # System Log Message: 
            # Error encountered: server down, failed to check request [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEBFILTER_REQUEST_NOT_CHECKED"

            $errmsg = extract($Details,"check request (.*)$")

            @AlertGroup = "WEBFILTER_REQUEST_NOT_CHECKED"
            @AlertKey = "Error:" + $errmsg

            @Summary = "Error encountered: server down, failed to check request " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "errmsg", $errmsg)


        case "WEBFILTER_SERVER_CONNECTED":


            ###############################
            #
            # System Log Message: 
            # Successfully connected to webfilter server [name]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEBFILTER_SERVER_CONNECTED"

            $name = ltrim(rtrim(extract($Details,"server (.*)$")))

            @AlertGroup = "WEBFILTER_SERVER_[CONNECTED|DISCONNECTED]"
            @AlertKey = "Name:" + $name

            @Summary = "Successfully connected to webfilter server " + $name

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($name)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "name", $name)


        case "WEBFILTER_SERVER_DISCONNECTED":


            ###############################
            #
            # System Log Message: 
            # Webfilter: server [name] disconnected
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEBFILTER_SERVER_DISCONNECTED"

            $name = ltrim(rtrim(extract($Details,"server (.*) disconnected")))

            @AlertGroup = "WEBFILTER_SERVER_[CONNECTED|DISCONNECTED]"
            @AlertKey = "Name:" + $name

            @Summary = "Webfilter: server " + $name + " disconnected"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($name)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "name", $name)


        case "WEBFILTER_SERVER_ERROR":


            ###############################
            #
            # System Log Message: 
            # WebFilter: An error is received from server [name] (0x[status-code]):
            # [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEBFILTER_SERVER_ERROR"

            $name = extract($Details,"server (.*) \(")
            $status-code = extract($Details,"server .* \((.*)\)")
            $errmsg = extract($Details,"\(.*\): (.*)$")

            @AlertGroup = "WEBFILTER_SERVER_ERROR"
            @AlertKey = "Name:" + $name + ", Status Code:" + $status-code

            @Summary = "WebFilter: An error is received from server " + $name +
                       " (" + $status-code + "):" + $errmsg

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($name,$status-code,$errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "name", $name, "status-code", $status-code, "errmsg", $errmsg)


        case "WEBFILTER_URL_BLOCKED":


            ###############################
            #
            # System Log Message: 
            # WebFilter: ACTION="URL Blocked"
            # [source-address]([source-port])->[destination-address]([destination-port])
            # CATEGORY="[name]" REASON="[error-message]"
            # PROFILE="[profile-name]" URL=[object-name] OBJ=[pathname]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEBFILTER_URL_BLOCKED"

            $source-address = ltrim(extract($Details,"URL Blocked (.*)\(.*\)->"))
            $source-port = extract($Details,"URL Blocked .*\((.*)\)->")
            $destination-address = extract($Details,"URL Blocked .*\(.*\)->(.*)\(.*\)")
            $destination-port = extract($Details,"URL Blocked .*\(.*\)->.*\((.*)\)")
            $category = extract($Details,"CATEGORY=?(.*) REASON")
            $reason = ltrim(extract($Details,"REASON=?(.*) PROFILE"))
            $profile = ltrim(extract($Details,"PROFILE=?(.*) URL"))
            $url = extract($Details,"PROFILE=?.*URL=?(.*) OBJ")
            $pathname = extract($Details,"OBJ=?(.*)$")

            @AlertGroup = "WEBFILTER_URL_[BLOCKED|PERMITTED]"
            @AlertKey = "Source:" + $source-address + ":" + $source-port + ", Destination: " +
                        $destination-address + ":" + $destination-port

            @Summary = "WebFilter: ACTION=URL Blocked " + $source-address + "(" + $source-port + ")" +
                       "->" + $destination-address + "(" + $destination-port + ")" + 
                       " CATEGORY=" + $category + " REASON=" + $reason + " PROFILE=" +
                       $profile + " URL=" + $url + " OBJ=" + $pathname

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($source-address,$source-port,$destination-address,$destination-port,
                $category,$reason,$profile,$url,$pathname)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "source-address", $source-address, "source-port", $source-port, "destination-address", $destination-address,
                 "destination-port", $destination-port, "category", $category, "reason", $reason,
                 "profile", $profile, "url", $url, "pathname", $pathname)


        case "WEBFILTER_URL_PERMITTED":


            ###############################
            #
            # System Log Message: 
            # WebFilter: ACTION="URL Permitted"
            # [source-address]([source-port])->[destination-address]([destination-port])
            # CATEGORY="[name]" REASON="[error-message]"
            # PROFILE="[profile-name]" URL=[object-name] OBJ=[pathname]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEBFILTER_URL_PERMITTED"

            $source-address = ltrim(extract($Details,"Permitted (.*)\(.*\)->"))
            $source-port = extract($Details,"Permitted .*\((.*)\)->")
            $destination-address = extract($Details,"Permitted .*\(.*\)->(.*)\(.*\)")
            $destination-port = extract($Details,"Permitted .*\(.*\)->.*\((.*)\)")
            $category = extract($Details,"CATEGORY=?(.*) REASON")
            $reason = ltrim(extract($Details,"REASON=?(.*) PROFILE"))
            $profile = ltrim(extract($Details,"PROFILE=?(.*) URL"))
            $url = extract($Details,"PROFILE=?.*URL=?(.*) OBJ")
            $pathname = extract($Details,"OBJ=?(.*)$")

            @AlertGroup = "WEBFILTER_URL_[BLOCKED|PERMITTED]"
            @AlertKey = "Source:" + $source-address + ":" + $source-port + ", Destination: " +
                        $destination-address + ":" + $destination-port

            @Summary = "WebFilter: ACTION=URL Permitted " + $source-address + "(" + $source-port + ")" +
                       "->" + $destination-address + "(" + $destination-port + ")" + 
                       " CATEGORY=" + $category + " REASON=" + $reason + " PROFILE=" +
                       $profile + " URL=" + $url + " OBJ=" + $pathname

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($source-address,$source-port,$destination-address,$destination-port,
                $category,$reason,$profile,$url,$pathname)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "source-address", $source-address, "source-port", $source-port, "destination-address", $destination-address,
                 "destination-port", $destination-port, "category", $category, "reason", $reason,
                 "profile", $profile, "url", $url, "pathname", $pathname)

        case "WEBFILTER_URL_REDIRECTED":


            ###############################
            #
            # System Log Message: 
            # WebFilter: ACTION="URL Redirected"
            # [source-address]([source-port])->[destination-address]([destination-port])
            # REASON="[error-message]"
            # PROFILE="[profile-name]" URL=[object-name] OBJ=[pathname]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEBFILTER_URL_REDIRECTED"

            $source-address = ltrim(extract($Details,"Redirected (.*)\(.*\)->"))
            $source-port = extract($Details,"Redirected .*\((.*)\)->")
            $destination-address = extract($Details,"Redirected .*\(.*\)->(.*)\(.*\)")
            $destination-port = extract($Details,"Redirected .*\(.*\)->.*\((.*)\)")
            $reason = ltrim(extract($Details,"REASON=?(.*) PROFILE"))
            $profile = ltrim(extract($Details,"PROFILE=?(.*) URL"))
            $url = extract($Details,"PROFILE=?.*URL=?(.*) OBJ")
            $pathname = extract($Details,"OBJ=?(.*)$")

            @AlertGroup = "WEBFILTER_URL_REDIRECTED"
            @AlertKey = "Source:" + $source-address + ":" + $source-port + ", Destination: " +
                        $destination-address + ":" + $destination-port

            @Summary = "WebFilter: ACTION=URL Redirected " + $source-address + "(" + $source-port + ")" +
                       "->" + $destination-address + "(" + $destination-port + ")" + 
                       " REASON=" + $reason + " PROFILE=" +
                       $profile + " URL=" + $url + " OBJ=" + $pathname

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($source-address,$source-port,$destination-address,$destination-port,
                $reason,$profile,$url,$pathname)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "source-address", $source-address, "source-port", $source-port, "destination-address", $destination-address,
                 "destination-port", $destination-port, "reason", $reason, "profile", $profile,
                 "url", $url, "pathname", $pathname)


        default:

            $UseJuniperJunosDefaults = 1
            @Summary = "Unknown System Log Message (" + $message-tag + ") Received "
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $prefix + " " + $message-tag
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

