###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.1 - Updated Release.
#
#        Remove extended character "-" in some trap comments.
#        Correct FC_PROXY_NP_PORT_LOGIN_FAILED AlertGroup value.
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  Compliant with JUNOS 12.2. For further information, refer to 
#              Juniper JUNOS 12.2 Syslog Reference Guide.
#
###############################################################################

case "FC":

    log(DEBUG, "<<<<< Entering... juniper-junos-FC.include.syslog.rules >>>>>")

    switch($message-tag)
    {

        case "FC_FABRIC_CREATED":


            ###############################
            #
            # System Log Message: 
            # Fabric [fc-fabric-name] fabric-id [fc-fabric-id] fabric-type
            # [fc-fabric-type] created
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-FC_FABRIC_CREATED"

            $fc-fabric-name = extract($Details,"Fabric (.*) fabric-id .*")
            $fc-fabric-id = extract($Details,"fabric-id (.*) fabric-type")
            $fc-fabric-type = extract($Details,"fabric-type (.*) created")

            @AlertGroup = "FC_FABRIC_CREATED"
            @AlertKey = "Fabric Name:" + $fc-fabric-name + ", ID:" + $fc-fabric-id +
                        ", Type:" + $fc-fabric-type

            @Summary = "Fabric " + $fc-fabric-name + " fabric-id " + $fc-fabric-id + " fabric-type " +
                       $fc-fabric-type + " created"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($fc-fabric-name,$fc-fabric-id,$fc-fabric-type)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fc-fabric-name", $fc-fabric-name, "fc-fabric-id", $fc-fabric-id, "fc-fabric-type", $fc-fabric-type)


        case "FC_FABRIC_DELETED":


            ###############################
            #
            # System Log Message: 
            # Fabric [fc-fabric-name] fabric-id [fc-fabric-id] fabric-type
            # [fc-fabric-type] deleted
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-FC_FABRIC_DELETED"

            $fc-fabric-name = extract($Details,"Fabric (.*) fabric-id .*")
            $fc-fabric-id = extract($Details,"fabric-id (.*) fabric-type")
            $fc-fabric-type = extract($Details,"fabric-type (.*) deleted")

            @AlertGroup = "FC_FABRIC_DELETED"
            @AlertKey = "Fabric Name:" + $fc-fabric-name + ", ID:" + $fc-fabric-id +
                        ", Type:" + $fc-fabric-type

            @Summary = "Fabric " + $fc-fabric-name + " fabric-id " + $fc-fabric-id + " fabric-type " +
                       $fc-fabric-type + " deleted"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($fc-fabric-name,$fc-fabric-id,$fc-fabric-type)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fc-fabric-name", $fc-fabric-name, "fc-fabric-id", $fc-fabric-id, "fc-fabric-type", $fc-fabric-type)


        case "FC_FABRIC_WWN_ASSIGNED":


            ###############################
            #
            # System Log Message: 
            # Fabric [fc-fabric-name] fabric-wwn [wwn] is set
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-FC_FABRIC_WWN_ASSIGNED"

            $fc-fabric-name = extract($Details,"Fabric (.*) fabric-wwn .*")
            $wwn = extract($Details,"Fabric .* fabric-wwn (.*) is .*")

            @AlertGroup = "FC_FABRIC_WWN_ASSIGNED"
            @AlertKey = "Fabric Name:" + $fc-fabric-name + ", WWN:" + $wwn

            @Summary = "Fabric " + $fc-fabric-name + " fabric-wwn " + $wwn + " is set"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($fc-fabric-name,$wwn)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fc-fabric-name", $fc-fabric-name, "wwn", $wwn)


        case "FC_FABRIC_WWN_CLEARED":


            ###############################
            #
            # System Log Message: 
            # Fabric [fc-fabric-name] fabric-wwn is cleared
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-FC_FABRIC_WWN_CLEARED"

            $fc-fabric-name = extract($Details,"Fabric (.*) fabric-wwn .*")

            @AlertGroup = "FC_FABRIC_WWN_CLEARED"
            @AlertKey = "Fabric Name:" + $fc-fabric-name

            @Summary = "Fabric " + $fc-fabric-name + " fabric-wwn is cleared"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($fc-fabric-name)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fc-fabric-name", $fc-fabric-name)


        case "FC_FLOGI_VN_PORT_LOGIN_FAILED":


            ###############################
            #
            # System Log Message: 
            # Fabric [fc-fabric-name] interface [interface-name] enode-mac
            # [enode-mac-address] wwn [wwn] - [reason]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-FC_FLOGI_VN_PORT_LOGIN_FAILED"

            $fc-fabric-name = extract($Details,"Fabric (.*) interface .* enode-mac .*")
            $interface-name = extract($Details,".* interface (.*) enode-mac .*")
            $enode-mac-address = extract($Details,".* enode-mac (.*) wwn .*")
            $wwn = extract($Details,"enode-mac .* wwn (.*) - .*")
            $reason = extract($Details,"enode-mac .* wwn .* - (.*)")

            @AlertGroup = "FC_FLOGI_VN_PORT_LOGIN_FAILED"
            @AlertKey = "Fabric Name:" + $fc-fabric-name + ", Interface:" + $interface-name + ", End Node MAC:" + $enode-mac-address

            @Summary = "Fabric " + $fc-fabric-name + " interface " + $interface-name + " enode-mac " + $enode-mac-address +
                       " wwn " + $wwn + " - " + $reason

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($fc-fabric-name,$interface-name,$enode-mac-address,$wwn,$reason)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fc-fabric-name", $fc-fabric-name, "interface-name", $interface-name, "enode-mac-address", $enode-mac-address,
                 "wwn", $wwn, "reason", $reason)


        case "FC_LOGICAL_INTERFACE_CREATED":


            ###############################
            #
            # System Log Message: 
            # Fabric [fc-fabric-name] interface [interface-name] created
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-FC_LOGICAL_INTERFACE_CREATED"

            $fc-fabric-name = extract($Details,"Fabric (.*) interface .*")
            $interface-name = extract($Details,"Fabric .* interface (.*) created")

            @AlertGroup = "FC_LOGICAL_INTERFACE_CREATED"
            @AlertKey = "Fabric Name:" + $fc-fabric-name + ", Interface:" + $interface-name

            @Summary = "Fabric " + $fc-fabric-name + " interface " + $interface-name + " created"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($fc-fabric-name,$interface-name)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fc-fabric-name", $fc-fabric-name, "interface-name", $interface-name)


        case "FC_LOGICAL_INTERFACE_DELETED":


            ###############################
            #
            # System Log Message: 
            # Fabric [fc-fabric-name] interface [interface-name] deleted
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-FC_LOGICAL_INTERFACE_DELETED"

            $fc-fabric-name = extract($Details,"Fabric (.*) interface .*")
            $interface-name = extract($Details,"Fabric .* interface (.*) deleted")

            @AlertGroup = "FC_LOGICAL_INTERFACE_DELETED"
            @AlertKey = "Fabric Name:" + $fc-fabric-name + ", Interface:" + $interface-name

            @Summary = "Fabric " + $fc-fabric-name + " interface " + $interface-name + " deleted"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($fc-fabric-name,$interface-name)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fc-fabric-name", $fc-fabric-name, "interface-name", $interface-name)


        case "FC_LOGICAL_INTERFACE_ISOLATED":


            ###############################
            #
            # System Log Message: 
            # Fabric [fc-fabric-name] interface [interface-name] reason - [reason]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-FC_LOGICAL_INTERFACE_ISOLATED"

            $fc-fabric-name = extract($Details,"Fabric (.*) interface .*")
            $interface-name = extract($Details,"Fabric .* interface (.*) reason .*")
            $reason = extract($Details,"interface .* reason - (.*)")

            @AlertGroup = "FC_LOGICAL_INTERFACE_ISOLATED"
            @AlertKey = "Fabric Name:" + $fc-fabric-name + ", Interface:" + $interface-name

            @Summary = "Fabric " + $fc-fabric-name + " interface " + $interface-name + " reason - " + $reason

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($fc-fabric-name,$interface-name,$reason)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fc-fabric-name", $fc-fabric-name, "interface-name", $interface-name, "reason", $reason)


        case "FC_PROXY_NP_PORT_LB_ADDED":


            ###############################
            #
            # System Log Message: 
            # Fabric [fc-fabric-name] NP_Port [interface-name] added to fabric load
            # balance list
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-FC_PROXY_NP_PORT_LB_ADDED"

            $fc-fabric-name = extract($Details,"Fabric (.*) NP_Port .* added")
            $interface-name = extract($Details,"Fabric .* NP_Port (.*) added")

            @AlertGroup = "FC_PROXY_NP_PORT_LB_ADDED"
            @AlertKey = "Fabric Name:" + $fc-fabric-name + ", Interface:" + $interface-name

            @Summary = "Fabric " + $fc-fabric-name + " NP_Port " + $interface-name + " added to fabric load balance list"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($fc-fabric-name,$interface-name)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fc-fabric-name", $fc-fabric-name, "interface-name", $interface-name)


        case "FC_PROXY_NP_PORT_LB_REMOVED":


            ###############################
            #
            # System Log Message: 
            # Fabric [fc-fabric-name] NP_Port [interface-name] removed from fabric
            # load balance list
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-FC_PROXY_NP_PORT_LB_REMOVED"

            $fc-fabric-name = extract($Details,"Fabric (.*) NP_Port .* removed")
            $interface-name = extract($Details,"Fabric .* NP_Port (.*) removed")

            @AlertGroup = "FC_PROXY_NP_PORT_LB_REMOVED"
            @AlertKey = "Fabric Name:" + $fc-fabric-name + ", Interface:" + $interface-name

            @Summary = "Fabric " + $fc-fabric-name + " NP_Port " + $interface-name + " removed to fabric load balance list"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($fc-fabric-name,$interface-name)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fc-fabric-name", $fc-fabric-name, "interface-name", $interface-name)


        case "FC_PROXY_NP_PORT_LOGIN_ABORTED":


            ###############################
            #
            # System Log Message: 
            # Fabric [fc-fabric-name] NP_Port [interface-name] login aborted
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-FC_PROXY_NP_PORT_LOGIN_ABORTED"

            $fc-fabric-name = extract($Details,"Fabric (.*) NP_Port .* login")
            $interface-name = extract($Details,"Fabric .* NP_Port (.*) login")

            @AlertGroup = "FC_PROXY_NP_PORT_LOGIN_ABORTED"
            @AlertKey = "Fabric Name:" + $fc-fabric-name + ", Interface:" + $interface-name

            @Summary = "Fabric " + $fc-fabric-name + " NP_Port " + $interface-name + " login aborted"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($fc-fabric-name,$interface-name)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fc-fabric-name", $fc-fabric-name, "interface-name", $interface-name)


        case "FC_PROXY_NP_PORT_LOGIN_FAILED":


            ###############################
            #
            # System Log Message: 
            # Fabric [fc-fabric-name] NP_Port [interface-name] login failed -
            # [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-FC_PROXY_NP_PORT_LOGIN_FAILED"

            $fc-fabric-name = extract($Details,"Fabric (.*) NP_Port .* login")
            $interface-name = extract($Details,"Fabric .* NP_Port (.*) login")
            $errmsg = extract($Details,".* login failed - (.*)")

            @AlertGroup = "FC_PROXY_NP_PORT_LOGIN_FAILED"
            @AlertKey = "Fabric Name:" + $fc-fabric-name + ", Interface:" + $interface-name

            @Summary = "Fabric " + $fc-fabric-name + " NP_Port " + $interface-name + " login failed - " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($fc-fabric-name,$interface-name,$errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fc-fabric-name", $fc-fabric-name, "interface-name", $interface-name, "errmsg", $errmsg)


        case "FC_PROXY_NP_PORT_LOGOUT":


            ###############################
            #
            # System Log Message: 
            # Fabric [fc-fabric-name] NP_Port [interface-name] logged out by core
            # switch
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-FC_PROXY_NP_PORT_LOGOUT"

            $fc-fabric-name = extract($Details,"Fabric (.*) NP_Port .* logged")
            $interface-name = extract($Details,"Fabric .* NP_Port (.*) logged")

            @AlertGroup = "FC_PROXY_NP_PORT_LOGOUT"
            @AlertKey = "Fabric Name:" + $fc-fabric-name + ", Interface:" + $interface-name

            @Summary = "Fabric " + $fc-fabric-name + " NP_Port " + $interface-name + " logged out by core switch "

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($fc-fabric-name,$interface-name)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fc-fabric-name", $fc-fabric-name, "interface-name", $interface-name)


        case "FC_PROXY_NP_PORT_NPIV_FAILED":


            ###############################
            #
            # System Log Message: 
            # Fabric [fc-fabric-name] NP_Port [interface-name] attached fabric port
            # does not support NPIV
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-FC_PROXY_NP_PORT_NPIV_FAILED"

            $fc-fabric-name = extract($Details,"Fabric (.*) NP_Port .* attached")
            $interface-name = extract($Details,"Fabric .* NP_Port (.*) attached")

            @AlertGroup = "FC_PROXY_NP_PORT_NPIV_FAILED"
            @AlertKey = "Fabric Name:" + $fc-fabric-name + ", Interface:" + $interface-name

            @Summary = "Fabric " + $fc-fabric-name + " NP_Port " + $interface-name + " attached fabric port does not " +
                       "support NPIV"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($fc-fabric-name,$interface-name)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fc-fabric-name", $fc-fabric-name, "interface-name", $interface-name)


        case "FC_PROXY_NP_PORT_OFFLINE":


            ###############################
            #
            # System Log Message: 
            # Fabric [fc-fabric-name] NP_Port [interface-name] is offline
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-FC_PROXY_NP_PORT_OFFLINE"

            $fc-fabric-name = extract($Details,"Fabric (.*) NP_Port .* is offline")
            $interface-name = extract($Details,"Fabric .* NP_Port (.*) is offline")

            @AlertGroup = "FC_PROXY_NP_PORT_OFFLINE"
            @AlertKey = "Fabric Name:" + $fc-fabric-name + ", Interface:" + $interface-name

            @Summary = "Fabric " + $fc-fabric-name + " NP_Port " + $interface-name + " is offline"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($fc-fabric-name,$interface-name)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fc-fabric-name", $fc-fabric-name, "interface-name", $interface-name)


        case "FC_PROXY_NP_PORT_ONLINE":


            ###############################
            #
            # System Log Message: 
            # Fabric [fc-fabric-name] NP_Port [interface-name] fcid: [fcid] online
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-FC_PROXY_NP_PORT_ONLINE"


            $fc-fabric-name = extract($Details,"Fabric (.*) NP_Port .* fcid:")
            $interface-name = extract($Details,"Fabric .* NP_Port (.*) fcid:")
            $fcid = extract($Details,"fcid: (.*) online")

            @AlertGroup = "FC_PROXY_NP_PORT_ONLINE"
            @AlertKey = "Fabric Name:" + $fc-fabric-name + ", Interface:" + $interface-name

            @Summary = "Fabric " + $fc-fabric-name + " NP_Port " + $interface-name + " fcid:" + $fcid + " online"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($fc-fabric-name,$interface-name,$fcid)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fc-fabric-name", $fc-fabric-name, "interface-name", $interface-name, "fcid", $fcid)


        case "FC_PROXY_VN_PORT_LOGIN_ABORTED":


            ###############################
            #
            # System Log Message: 
            # Fabric [fc-fabric-name] NP_Port [interface-name] FDISC is aborted for
            # wwn [wwn]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-FC_PROXY_VN_PORT_LOGIN_ABORTED"

            $fc-fabric-name = extract($Details,"Fabric (.*) NP_Port .* FDISC")
            $interface-name = extract($Details,"Fabric .* NP_Port (.*) FDISC")
            $wwn = extract($Details,"aborted for wwn (.*)")

            @AlertGroup = "FC_PROXY_VN_PORT_LOGIN_ABORTED"
            @AlertKey = "Fabric Name:" + $fc-fabric-name + ", Interface:" + $interface-name

            @Summary = "Fabric " + $fc-fabric-name + " NP_Port " + $interface-name + "  FDISC is aborted for wwn " + $wwn

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($fc-fabric-name,$interface-name,$wwn)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fc-fabric-name", $fc-fabric-name, "interface-name", $interface-name, "wwn", $wwn)


        case "FC_PROXY_VN_PORT_LOGIN_FAILED":


            ###############################
            #
            # System Log Message: 
            # Fabric [fc-fabric-name] NP_Port [interface-name] FDISC rejected for wwn
            # [wwn] - [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-FC_PROXY_VN_PORT_LOGIN_FAILED"

            $fc-fabric-name = extract($Details,"Fabric (.*) NP_Port .* FDISC")
            $interface-name = extract($Details,"Fabric .* NP_Port (.*) FDISC")
            $wwn = extract($Details,"rejected for wwn (.*) - .*")
            $errmsg = extract($Details,"rejected for wwn .* - (.*)")

            @AlertGroup = "FC_PROXY_VN_PORT_LOGIN_FAILED"
            @AlertKey = "Fabric Name:" + $fc-fabric-name + ", Interface:" + $interface-name

            @Summary = "Fabric " + $fc-fabric-name + " NP_Port " + $interface-name + "  FDISC is rejected for wwn " + $wwn + 
                       " - " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($fc-fabric-name,$interface-name,$wwn,$errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fc-fabric-name", $fc-fabric-name, "interface-name", $interface-name, "wwn", $wwn,
                 "errmsg", $errmsg)



        default:

            $UseJuniperJunosDefaults = 1
            @Summary = "Unknown System Log Message (" + $message-tag + ") Received "
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $prefix + " " + $message-tag
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

