###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  Compliant with JUNOS 12.2. For further information, refer to 
#              Juniper JUNOS 12.2 Syslog Reference Guide.
#
###############################################################################

case "DHCPD":

    log(DEBUG, "<<<<< Entering... juniper-junos-DHCPD.include.syslog.rules >>>>>")

    switch($message-tag)
    {

        case "DHCPD_BIND_FAILURE":


            ###############################
            #
            # System Log Message: 
            # bind() to port [port] failed: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-DHCPD_BIND_FAILURE"

            $port = extract($Details,".* to port (.*) failed")
            $errmsg = extract($Details,".* failed: (.*)")

            @AlertGroup = "DHCPD_BIND_FAILURE"
            @AlertKey = "Port:" + $port

            @Summary = "bind() to port " + $port + " failed: " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($port,$errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "port", $port, "errmsg", $errmsg)


        case "DHCPD_DEGRADED_MODE":


            ###############################
            #
            # System Log Message: 
            # Running in degraded mode because of [reason]: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-DHCPD_DEGRADED_MODE"

            $reason = extract($Details,".* because of (.*): .*")
            $errmsg = extract($Details,".* because of .*: (.*)$")

            @AlertGroup = "DHCPD_DEGRADED_MODE"
            @AlertKey = "Reason:" + $reason

            @Summary = "Running in degraded mode because of " + $reason + ": " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($reason,$errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "reason", $reason, "errmsg", $errmsg)


        case "DHCPD_MEMORY_ALLOCATION_FAILURE":


            ###############################
            #
            # System Log Message: 
            # Unable to allocate memory for [object-name]: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-DHCPD_MEMORY_ALLOCATION_FAILURE"

            $object-name = extract($Details,".* memory for (.*): .*")
            $errmsg = extract($Details,".* memory for .*: (.*)$")

            @AlertGroup = "DHCPD_MEMORY_ALLOCATION_FAILURE"
            @AlertKey = "Object Name:" + $object-name

            @Summary = "Unable to allocate memory for " + $object-name + ": " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($object-name,$errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "object-name", $object-name, "errmsg", $errmsg)


        case "DHCPD_OVERLAY_CONFIG_FAILURE":


            ###############################
            #
            # System Log Message: 
            # Unable to open overlay configuration: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-DHCPD_OVERLAY_CONFIG_FAILURE"

            $errmsg = extract($Details,".* overlay configuration: (.*)$")

            @AlertGroup = "DHCPD_OVERLAY_CONFIG_FAILURE"
            @AlertKey = "Error:" + $errmsg

            @Summary = "Unable to open overlay configuration: " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "errmsg", $errmsg)


        case "DHCPD_OVERLAY_PARSE_FAILURE":


            ###############################
            #
            # System Log Message: 
            # Unable to parse overlay configuration because of [count] syntax errors
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-DHCPD_OVERLAY_PARSE_FAILURE"

            $count = extract($Details,".* because of (.*) syntax .*")

            @AlertGroup = "DHCPD_OVERLAY_PARSE_FAILURE"
            @AlertKey = "Error Count:" + $count

            @Summary = "Unable to parse overlay configuration because of " + $count + " syntax errors"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($count)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "count", $count)


        case "DHCPD_RECVMSG_FAILURE":


            ###############################
            #
            # System Log Message: 
            # Exiting due to too many recvmsg() failures
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-DHCPD_RECVMSG_FAILURE"

            @AlertGroup = "DHCPD_RECVMSG_FAILURE"
            @AlertKey = ""

            @Summary = "Exiting due to too many recvmsg() failures"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager




        case "DHCPD_RTSOCK_FAILURE":


            ###############################
            #
            # System Log Message: 
            # Error with rtsock: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-DHCPD_RTSOCK_FAILURE"

            $errmsg = extract($Details,".* rtsock: (.*)")

            @AlertGroup = "DHCPD_RTSOCK_FAILURE"
            @AlertKey = "Error:" + $errmsg

            @Summary = "Error with rtsock: " + $errmsg

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "errmsg", $errmsg)


        case "DHCPD_SENDMSG_FAILURE":


            ###############################
            #
            # System Log Message: 
            # sendmsg() from [source-address] to port [destination-port] at
            # [destination-address] via interface [interface-name] and routing instance
            # [routing-instance] failed: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-DHCPD_SENDMSG_FAILURE"

            $source-address = extract($Details,"sendmsg\(\) from (.*) to port .*")
            $destination-port = extract($Details,"sendmsg\(\) from .* to port (.*) at .* via interface .*")
            $destination-address = extract($Details,"sendmsg\(\) from .* to port .* at (.*) via interface .*")
            $interface-name = extract($Details,".* via interface (.*) and routing .*")
            $routing-instance = extract($Details,".* and routing instance (.*) failed: .*")
            $errmsg = extract($Details,".* and routing instance .* failed: (.*)")

            @AlertGroup = "DHCPD_SENDMSG_FAILURE"
            @AlertKey = "Destination Port:" + $destination-port + ", Address:" + $destination-address + 
                        ", Interface:" + $interface-name

            @Summary = "sendmsg() from " + $source-address + " to port " + $destination-port + " at " + 
                       $destination-address + " via interface " + $interface-name + " and routing instance " + 
                       $routing-instance + " failed: " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($source-address,$destination-port,$destination-address,$interface-name,$routing-instance,$errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "source-address", $source-address, "destination-port", $destination-port, "destination-address", $destination-address,
                 "interface-name", $interface-name, "routing-instance", $routing-instance, "errmsg", $errmsg)


        case "DHCPD_SENDMSG_NOINT_FAILURE":


            ###############################
            #
            # System Log Message: 
            # sendmsg() from [source-address] to port [destination-port] at
            # [destination-address] via routing instance [routing-instance] failed:
            # [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-DHCPD_SENDMSG_NOINT_FAILURE"

            $source-address = extract($Details,"sendmsg\(\) from (.*) to port .*")
            $destination-port = extract($Details,"sendmsg\(\) from .* to port (.*) at .* via routing instance .*")
            $destination-address = extract($Details,"sendmsg\(\) from .* to port .* at (.*) via routing instance .*")
            $routing-instance = extract($Details,".* via routing instance (.*) failed: .*")
            $errmsg = extract($Details,".* via routing instance .* failed: (.*)")

            @AlertGroup = "DHCPD_SENDMSG_NOINT_FAILURE"
            @AlertKey = "Destination Port:" + $destination-port + ", Address:" + $destination-address + 
                        ", Routing Instance:" + $routing-instance

            @Summary = "sendmsg() from " + $source-address + "to port " + $destination-port + " at " + 
                       $destination-address + " via routing instance " + $routing-instance + " failed: " + 
                       $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($source-address,$destination-port,$destination-address,$routing-instance,$errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "source-address", $source-address, "destination-port", $destination-port, "destination-address", $destination-address,
                 "routing-instance", $routing-instance, "errmsg", $errmsg)


        case "DHCPD_SETSOCKOPT_FAILURE":


            ###############################
            #
            # System Log Message: 
            # setsockopt([socket-option]) failed: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-DHCPD_SETSOCKOPT_FAILURE"

            $socket-option = extract($Details,"setsockopt\((.*)\) failed: .*")
            $errmsg = extract($Details,"setsockopt\(.*\) failed: (.*)")

            @AlertGroup = "DHCPD_SETSOCKOPT_FAILURE"
            @AlertKey = "Socket Option:" + $socket-option

            @Summary = "setsockopt(" + $socket-option + ") failed: " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($socket-option,$errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "socket-option", $socket-option, "errmsg", $errmsg)


        case "DHCPD_SOCKET_FAILURE":


            ###############################
            #
            # System Log Message: 
            # socket([arguments]) failed: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-DHCPD_SOCKET_FAILURE"

            $arguments = extract($Details,"socket\((.*)\) failed: .*")
            $errmsg = extract($Details,"socket\(.*\) failed: (.*)")

            @AlertGroup = "DHCPD_SOCKET_FAILURE"
            @AlertKey = "Arguments:" + $arguments

            @Summary = "socket(" + $arguments + ") failed: " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($arguments,$errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "arguments", $arguments, "errmsg", $errmsg)



        default:

            $UseJuniperJunosDefaults = 1
            @Summary = "Unknown System Log Message (" + $message-tag + ") Received "
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $prefix + " " + $message-tag
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

