###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.1 - Updated Release.
#
#        Remove extended character "-" in some trap comments.
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  Compliant with JUNOS 12.2. For further information, refer to 
#              Juniper JUNOS 12.2 Syslog Reference Guide.
#
###############################################################################

case "FCOE":

    log(DEBUG, "<<<<< Entering... juniper-junos-FCOE.include.syslog.rules >>>>>")

    switch($message-tag)
    {

        case "FCOE_LOGICAL_INTERFACE_CREATED":


            ###############################
            #
            # System Log Message: 
            # Fabric [fc-fabric-name] interface [interface-name] mac [mac-address]
            # created
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-FCOE_LOGICAL_INTERFACE_CREATED"

            $fc-fabric-name = extract($Details,"Fabric (.*) interface .*")
            $interface-name = extract($Details,"Fabric .* interface (.*) mac .*")
            $mac-address = extract($Details,"interface .* mac (.*) created")

            @AlertGroup = "FCOE_LOGICAL_INTERFACE_CREATED"
            @AlertKey = "Fabric Name:" + $fc-fabric-name + ", Interface:" + $interface-name + ", MAC:" + $mac-address

            @Summary = "Fabric " + $fc-fabric-name + " interface " + $interface-name + " mac " + $mac-address +
                       " created"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($fc-fabric-name,$interface-name,$mac-address)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fc-fabric-name", $fc-fabric-name, "interface-name", $interface-name, "mac-address", $mac-address)


        case "FCOE_LOGICAL_INTERFACE_DELETED":


            ###############################
            #
            # System Log Message: 
            # Fabric [fc-fabric-name] interface [interface-name] mac [mac-address]
            # deleted
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-FCOE_LOGICAL_INTERFACE_DELETED"

            $fc-fabric-name = extract($Details,"Fabric (.*) interface .*")
            $interface-name = extract($Details,"Fabric .* interface (.*) mac .*")
            $mac-address = extract($Details,"interface .* mac (.*) deleted")

            @AlertGroup = "FCOE_LOGICAL_INTERFACE_DELETED"
            @AlertKey = "Fabric Name:" + $fc-fabric-name + ", Interface:" + $interface-name + ", MAC:" + $mac-address

            @Summary = "Fabric " + $fc-fabric-name + " interface " + $interface-name + " mac " + $mac-address +
                       " deleted"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($fc-fabric-name,$interface-name,$mac-address)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fc-fabric-name", $fc-fabric-name, "interface-name", $interface-name, "mac-address", $mac-address)


        case "FCOE_LOGICAL_INTERFACE_ISOLATED":


            ###############################
            #
            # System Log Message: 
            # Fabric [fc-fabric-name] interface [interface-name] mac [mac-address]
            # reason - [reason]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-FCOE_LOGICAL_INTERFACE_ISOLATED"

            $fc-fabric-name = extract($Details,"Fabric (.*) interface .*")
            $interface-name = extract($Details,"Fabric .* interface (.*) mac .*")
            $mac-address = extract($Details,"interface .* mac (.*) reason - .*")
            $reason = extract($Details,"mac .* reason - (.*)")

            @AlertGroup = "FCOE_LOGICAL_INTERFACE_ISOLATED"
            @AlertKey = "Fabric Name:" + $fc-fabric-name + ", Interface:" + $interface-name + ", MAC:" + $mac-address

            @Summary = "Fabric " + $fc-fabric-name + " interface " + $interface-name + " mac " + $mac-address +
                       " reason - " + $reason

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($fc-fabric-name,$interface-name,$mac-address,$reason)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "fc-fabric-name", $fc-fabric-name, "interface-name", $interface-name, "mac-address", $mac-address,
                 "reason", $reason)



        default:

            $UseJuniperJunosDefaults = 1
            @Summary = "Unknown System Log Message (" + $message-tag + ") Received "
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $prefix + " " + $message-tag
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

