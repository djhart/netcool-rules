###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
#
# 1.0 - Initial Release  
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  Juniper JUNOS 12.2 Syslog (Partial)
#       
###############################################################################

##########################
#
# Severity mapping, between JUNOS messages and Netcool Events
#
###########################
	
#######################################################################
#                  JUNOS messages                    # Netcool Events #
#######################################################################
# emergency  Panic or other conditions that cause    #       5        #
#            the system to become unusable.          #                #
#######################################################################
# alert      Conditions that should be corrected     #       5        #
#            immediately, such as a corrupted        #                #
#            system database.                        #                #
#######################################################################
# critical   Critical conditions, such as hard       #       4        #
#            drive errors.                           #                #
#######################################################################
# error      Standard error conditions.              #       3        #
#######################################################################
# warning    System warning messages.                #       2        #
#######################################################################
# notice     Conditions that are not error           #       2        #
#            but that might warrant special handling #                #
#######################################################################
# info       Informational messages.                 #       2        #
#            This is the default.                    #                #
#######################################################################
# debug      Software debugging messages.            #       2        #
#######################################################################

case "Juniper-JUNOS":

    log(DEBUG, "<<<<< Entering... juniper-junos.include.syslog.rules >>>>>")

    @Agent = "Juniper-JUNOS"
    @Class = "40371"
    
    if (regmatch ($Details, "^[A-Z|_|0-9]+: "))
    { 
       $message-tag = extract($Details, "^([A-Z|_|0-9]+): ")
	}
	else
	{
		if (regmatch ($Details, ": [A-Z|_|0-9]+: "))
		{
			$message-tag = extract($Details, ": ([A-Z|_|0-9]+): ")
		}
		else
		{
			$message-tag = $Token8
		}
	}
    
    
    if(regmatch($Token5, "[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+"))
    {
        @Node = $Token5
        @NodeAlias = $Token5
    }
    
    $prefix = ltrim(rtrim(extract($message-tag, "^([A-Z|0-9]+)_")))
    $Message = ltrim(rtrim(extract($Details, "[A-Z|_|0-9]+:(.*)$")))
    
    # Initialize default values
    @AlertGroup = $prefix
    @Summary = $Message
    $OS_EventId = "SYSLOG-juniper-junos-" + $message-tag
    
    switch($prefix)
    {
        case "dummy case statement": ### This will prevent syntax errors in case no includes are added below.
            
        #######################################################################
        # Enter rules file Includes below with the following syntax:
        #
        # include "<$NCHOME>/etc/rules/include-syslog/juniper-junos/
        # juniper-junos-<prefix>.include.syslog.rules"
        #######################################################################
    
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-ACCT.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-ALARMD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-ANALYZER.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-ANCPD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-ANTISPAM.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-APPID.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-APPIDD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-APPPXY.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-APPTRACK.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-ASP.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-AUDITD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-AUTHD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-AUTOCONFD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-AUTOD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-AV.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-BFDD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-BOOTPD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-CFMD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-CHASSISM.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-COSD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-DCBX.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-DCD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-DDOS.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-DFCD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-DFWD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-DHCPD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-DOT1XD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-DYNAMIC.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-ESWD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-EVENTD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-FABOAMD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-FC.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-FCOE.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-FIP.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-FIPS.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-RTLOGD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-RTPERF.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-SAVAL.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-SDXD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-SFW.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-SMTPD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-SNMP.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-SNMPD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-SPD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-SSH.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-SSHD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-SSL.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-SYSTEM.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-TFTPD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-UFDD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-UI.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-UTMD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-VCCPD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-VM.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-VRRPD.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-WEB.include.syslog.rules"
        include "$NC_RULES_HOME/include-syslog/juniper-junos/juniper-junos-WEBFILTER.include.syslog.rules"
        
        #######################################################################
        # End of rules file Includes
        #######################################################################
               
        default:
        
            $UseJuniperJunosDefaults = 1
    }
    
    switch($UseJuniperJunosDefaults)
    {
        case "1":            
            if(match(@AlertKey, ""))
            {
                $CLARIFIER = $Message
            }
        default:
    }
    
	##########
	# Handle Severity via Lookup.
	##########
	
#	if(exists($SEV_KEY))
#	{
#	    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, juniper-junos_sev)
#	}
#	else
#	{
#	    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, juniper-junos_sev)
#	}
#	include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"
	
	##########
	# End of Severity via Lookup.
	##########
    
    if(exists($CLARIFIER))
    {
        @Identifier = @Identifier + " " + $CLARIFIER
    }

log(DEBUG, "<<<<< Leaving... juniper-junos.include.syslog.rules >>>>>")
