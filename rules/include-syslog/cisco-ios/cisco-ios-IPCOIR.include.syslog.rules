###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.2 - Added basic debug logging.
#
# 1.1 - Modified to support default Cisco IOS message handling.
#
#     - Modified to support MOI fields in OMNIbus 3.6
#
# 1.0 - Initial Release based on logic extracted from
#       cisco-ios.include.syslog.rules
# 
#       
###############################################################################

case "IPCOIR":

    log(DEBUG, "<<<<< Entering... cisco-ios-IPCOIR.include.syslog.rules >>>>>")

    switch($Mnemonic)
    {
        case "CARD_LOADING":
            
            ##########
            # %IPCOIR-5-CARD_LOADING: Loading card [dec]
            ##########
            
            $MOI_Local = rtrim(extract($Message, "[Cc]ard ([0-9]+)"))
            $MOIType_Local = "slot"
            include "$NC_RULES_HOME/include-syslog/cisco-ios/cisco-ios-AssignMOI.include.syslog.rules"
            
            @AlertGroup = "IPCOIR-Card Loading Status"
            @AlertKey = "Slot: " + $MOI_Local
            @Severity = 1
            @Type = 2
        
        case "LOADER_ENTER_BOOT":
            
            ##########
            # %IPCOIR-5-LOADER_ENTER_BOOT: Linecard entering boot image,
            # download will restart, slot [dec]
            ##########
            
            $MOI_Local = rtrim(extract($Message, "[Ss]lot ([0-9]+)"))
            $MOIType_Local = "slot"
            include "$NC_RULES_HOME/include-syslog/cisco-ios/cisco-ios-AssignMOI.include.syslog.rules"
            
            @AlertGroup = "IPCOIR-Card Loading Status"
            @AlertKey = "Slot: " + $MOI_Local
        
        case "LOADER_IPC_FAIL":
            
            ##########
            # %IPCOIR-3-LOADER_IPC_FAIL: IPC failed, slot [dec]
            ##########
            
            $MOI_Local = rtrim(extract($Message, "[Ss]lot ([0-9]+)"))
            $MOIType_Local = "slot"
            include "$NC_RULES_HOME/include-syslog/cisco-ios/cisco-ios-AssignMOI.include.syslog.rules"
            
            @AlertGroup = "IPCOIR-Card Loading Status"
            @AlertKey = "Slot: " + $MOI_Local
        
        default:
            
            $UseCiscoIosDefaults = 1
    }

log(DEBUG, "<<<<< Leaving... cisco-ios-IPCOIR.include.syslog.rules >>>>>")
