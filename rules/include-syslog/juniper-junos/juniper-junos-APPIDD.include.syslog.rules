###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  Compliant with JUNOS 12.2. For further information, refer to 
#              Juniper JUNOS 12.2 Syslog Reference Guide.
#
###############################################################################

case "APPIDD":

    log(DEBUG, "<<<<< Entering... juniper-junos-APPIDD.include.syslog.rules >>>>>")

    switch($message-tag)
    {

        case "APPIDD_APPPACK_DOWNLOAD_RESULT":


            ###############################
            #
            # System Log Message: 
            # Application package download result([status])
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-APPIDD_APPPACK_DOWNLOAD_RESULT"

            $status = extract($Details,"result\s*\((.*)\)$")

            @AlertGroup = "APPIDD_APPPACK_DOWNLOAD_RESULT"
            @AlertKey = "Status:" + $status

            @Summary = "Application package download result " + $status

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($status)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "status", $status)


        case "APPIDD_APPPACK_INSTALL_RESULT":


            ###############################
            #
            # System Log Message: 
            # Application package install result([status])
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-APPIDD_APPPACK_INSTALL_RESULT"

            $status = extract($Details,"result\s*\((.*)\)$")

            @AlertGroup = "APPIDD_APPPACK_INSTALL_RESULT"
            @AlertKey = "Status:" + $status

            @Summary = "Application package install result " + $status

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($status)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "status", $status)


        case "APPIDD_APPPACK_UNINSTALL_RESULT":


            ###############################
            #
            # System Log Message: 
            # Application package uninstall result([status])
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-APPIDD_APPPACK_UNINSTALL_RESULT"

            $status = extract($Details,"result\s*\((.*)\)$")

            @AlertGroup = "APPIDD_APPPACK_UNINSTALL_RESULT"
            @AlertKey = "Status:" + $status

            @Summary = "Application package uninstall result " + $status

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($status)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "status", $status)


        case "APPIDD_DAEMON_INIT_FAILED":


            ###############################
            #
            # System Log Message: 
            # Aborted. A failure was encountered [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-APPIDD_DAEMON_INIT_FAILED"

            $errmsg = extract($Details,"encountered (.*)$")

            @AlertGroup = "APPIDD_DAEMON_INIT_FAILED"
            @AlertKey = "Error:" + $errmsg

            @Summary = "Aborted. A failure was encountered " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "errmsg", $errmsg)


        case "APPIDD_INTERNAL_ERROR":


            ###############################
            #
            # System Log Message: 
            # Internal Error([error-message])
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-APPIDD_INTERNAL_ERROR"

            $errmsg = extract($Details,"[Ii]nternal [Ee]rror\s*\((.*)\)")

            @AlertGroup = "APPIDD_INTERNAL_ERROR"
            @AlertKey = "Error:" + $errmsg

            @Summary = "Internal Error" + " ( " + $errmsg + " )"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "errmsg", $errmsg)


        case "APPIDD_SCHEDULED_UPDATE_FAILED":


            ###############################
            #
            # System Log Message: 
            # Failed to update application packge [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-APPIDD_SCHEDULED_UPDATE_FAILED"

            $errmsg = extract($Details,"application \w+\s*(.*)$")

            @AlertGroup = "APPIDD_SCHEDULED_UPDATE_FAILED"
            @AlertKey = "Error:" + $errmsg

            @Summary = "Failed to update application package, error:" + $errmsg

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "errmsg", $errmsg)



        default:

            $UseJuniperJunosDefaults = 1
            @Summary = "Unknown System Log Message (" + $message-tag + ") Received "
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $prefix + " " + $message-tag
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

