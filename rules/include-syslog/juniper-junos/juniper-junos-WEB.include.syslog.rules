###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  Compliant with JUNOS 12.2. For further information, refer to 
#              Juniper JUNOS 12.2 Syslog Reference Guide.
#
###############################################################################

case "WEB":

    log(DEBUG, "<<<<< Entering... juniper-junos-WEB.include.syslog.rules >>>>>")

    switch($message-tag)
    {

        case "WEB_AUTH_FAIL":


            ###############################
            #
            # System Log Message: 
            # Unable to authenticate httpd client (username [username])
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEB_AUTH_FAIL"

            $username = ltrim(rtrim(extract($Details,"\(username (.*)\)")))

            @AlertGroup = "WEB_AUTH_[FAIL|SUCCESS]"
            @AlertKey = "User:" + $username

            @Summary = "Unable to authenticate httpd client (username " + $username + ")"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($username)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "username", $username)


        case "WEB_AUTH_SUCCESS":


            ###############################
            #
            # System Log Message: 
            # Authenticated httpd client (username [username])
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEB_AUTH_SUCCESS"

            $username = ltrim(rtrim(extract($Details,"\(username (.*)\)")))

            @AlertGroup = "WEB_AUTH_[FAIL|SUCCESS]"
            @AlertKey = "User:" + $username

            @Summary = "Authenticated httpd client (username " + $username + ")"

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($username)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "username", $username)


        case "WEB_AUTH_TIME_EXCEEDED":


            ###############################
            #
            # System Log Message: 
            # Login attempt exceeded maximum processing time
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEB_AUTH_TIME_EXCEEDED"

            @AlertGroup = "WEB_AUTH_TIME_EXCEEDED"
            @AlertKey = ""

            @Summary = "Login attempt exceeded maximum processing time"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager



        case "WEB_CERT_FILE_NOT_FOUND":


            ###############################
            #
            # System Log Message: 
            # Could not find certificate file '[filename]', disabling HTTPS
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEB_CERT_FILE_NOT_FOUND"

            $filename = extract($Details,"file (.*),? disabling")

            @AlertGroup = "WEB_CERT_FILE_NOT_FOUND"
            @AlertKey = "Filename:" + $filename

            @Summary = "Could not find certificate file " + $filename + ", disabling HTTPS"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($filename)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "filename", $filename)


        case "WEB_CHILD_STATE":


            ###############################
            #
            # System Log Message: 
            # Unable to retrieve child state: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEB_CHILD_STATE"

            $errmsg = extract($Details,"child state: (.*)$")

            @AlertGroup = "WEB_CHILD_STATE"
            @AlertKey = "Error:" + $errmsg

            @Summary = "Unable to retrieve child state: " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "errmsg", $errmsg)


        case "WEB_CONFIG_OPEN_ERROR":


            ###############################
            #
            # System Log Message: 
            # Could not open '[filename]' for writing
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEB_CONFIG_OPEN_ERROR"

            $filename = extract($Details,"open (.*),? for writing")

            @AlertGroup = "WEB_CONFIG_OPEN_ERROR"
            @AlertKey = "Filename:" + $filename

            @Summary = "Could not open " + $filename + " for writing"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($filename)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "filename", $filename)


        case "WEB_CONFIG_WRITE_ERROR":


            ###############################
            #
            # System Log Message: 
            # Could not write '[filename]' configuration. Disk full?
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEB_CONFIG_WRITE_ERROR"

            $filename = extract($Details,"write (.*),? configuration")

            @AlertGroup = "WEB_CONFIG_WRITE_ERROR"
            @AlertKey = "Filename:" + $filename

            @Summary = "Could not write " + $filename + " configuration. Disk full?"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($filename)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "filename", $filename)


        case "WEB_COULDNT_START_HTTPD":


            ###############################
            #
            # System Log Message: 
            # Could not fork httpd process!
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEB_COULDNT_START_HTTPD"

            @AlertGroup = "WEB_COULDNT_START_HTTPD"
            @AlertKey = ""

            @Summary = "Could not fork httpd process!"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            


        case "WEB_EVENTLIB_INIT":


            ###############################
            #
            # System Log Message: 
            # Unable to initialize event library: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEB_EVENTLIB_INIT"

            $errmsg = extract($Details,"library: (.*)$")

            @AlertGroup = "WEB_EVENTLIB_INIT"
            @AlertKey = "Error:" + $errmsg

            @Summary = "Unable to initialize event library: " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "errmsg", $errmsg)


        case "WEB_KEYPAIR_FILE_NOT_FOUND":


            ###############################
            #
            # System Log Message: 
            # Could not find key pair file '[filename]', disabling HTTPS
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEB_KEYPAIR_FILE_NOT_FOUND"

            $filename = extract($Details,"pair file (.*),? disabling")

            @AlertGroup = "WEB_KEYPAIR_FILE_NOT_FOUND"
            @AlertKey = "Filename:" + $filename

            @Summary = "Could not find key pair file " + $filename + ", disabling HTTPS"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($filename)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "filename", $filename)


        case "WEB_MGD_BIND_ERROR":


            ###############################
            #
            # System Log Message: 
            # Could not bind mgd listener socket: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEB_MGD_BIND_ERROR"

            $errmsg = extract($Details,"socket: (.*)$")

            @AlertGroup = "WEB_MGD_BIND_ERROR"
            @AlertKey = "Error:" + $errmsg

            @Summary = "Could not bind mgd listener socket: " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "errmsg", $errmsg)


        case "WEB_MGD_CHMOD_ERROR":


            ###############################
            #
            # System Log Message: 
            # Could not chmod mgd listener socket
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEB_MGD_CHMOD_ERROR"

            @AlertGroup = "WEB_MGD_CHMOD_ERROR"
            @AlertKey = ""

            @Summary = "Could not chmod mgd listener socket"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager



        case "WEB_MGD_CONNECT_ERROR":


            ###############################
            #
            # System Log Message: 
            # Could not connect to mgd management socket
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEB_MGD_CONNECT_ERROR"

            @AlertGroup = "WEB_MGD_CONNECT_ERROR"
            @AlertKey = ""

            @Summary = "Could not connect to mgd management socket"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            


        case "WEB_MGD_FCNTL_ERROR":


            ###############################
            #
            # System Log Message: 
            # Could not set incoming mgd request to nonblocking
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEB_MGD_FCNTL_ERROR"

            @AlertGroup = "WEB_MGD_FCNTL_ERROR"
            @AlertKey = ""

            @Summary = "Could not set incoming mgd request to nonblocking"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            


        case "WEB_MGD_LISTEN_ERROR":


            ###############################
            #
            # System Log Message: 
            # Could not listen mgd listener socket: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEB_MGD_LISTEN_ERROR"

            $errmsg = extract($Details,"socket: (.*)$")

            @AlertGroup = "WEB_MGD_LISTEN_ERROR"
            @AlertKey = "Error:" + $errmsg

            @Summary = "Could not listen mgd listener socket: " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "errmsg", $errmsg)


        case "WEB_MGD_RECVMSG_PEEK_ERROR":


            ###############################
            #
            # System Log Message: 
            # Could not peek recvmsg() mgd connection
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEB_MGD_RECVMSG_PEEK_ERROR"

            @AlertGroup = "WEB_MGD_RECVMSG_PEEK_ERROR"
            @AlertKey = ""

            @Summary = "Could not peek recvmsg() mgd connection"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            


        case "WEB_MGD_SOCKET_ERROR":


            ###############################
            #
            # System Log Message: 
            # Could not create mgd listener socket
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEB_MGD_SOCKET_ERROR"

            @AlertGroup = "WEB_MGD_SOCKET_ERROR"
            @AlertKey = ""

            @Summary = "Could not create mgd listener socket"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager



        case "WEB_PIDFILE_LOCK":


            ###############################
            #
            # System Log Message: 
            # Unable to lock PID file [pathname]: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEB_PIDFILE_LOCK"

            $pathname = extract($Details,"[Pp][Ii][Dd] file (.*): .*$")
            $errmsg = extract($Details,"[Pp][Ii][Dd] file .*: (.*)$")

            @AlertGroup = "WEB_PIDFILE_LOCK"
            @AlertKey = "Pathname:" + $pathname + ", Error:" + $errmsg

            @Summary = "Unable to lock PID file " + $pathname + ": " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($pathname,$errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "pathname", $pathname, "errmsg", $errmsg)


        case "WEB_PIDFILE_UPDATE":


            ###############################
            #
            # System Log Message: 
            # Unable to update PID file [pathname]: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEB_PIDFILE_UPDATE"

            $pathname = extract($Details,"[Pp][Ii][Dd] file (.*): .*$")
            $errmsg = extract($Details,"[Pp][Ii][Dd] file .*: (.*)$")

            @AlertGroup = "WEB_PIDFILE_UPDATE"
            @AlertKey = "Pathname:" + $pathname + ", Error:" + $errmsg

            @Summary = "Unable to update PID file " + $pathname + ": " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($pathname,$errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "pathname", $pathname, "errmsg", $errmsg)


        case "WEB_UNAME_FAILED":


            ###############################
            #
            # System Log Message: 
            # Unable to retrieve system hostname: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEB_UNAME_FAILED"

            $errmsg = extract($Details,"hostname: (.*)$")

            @AlertGroup = "WEB_UNAME_FAILED"
            @AlertKey = "Error:" + $errmsg

            @Summary = "Unable to retrieve system hostname: " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "errmsg", $errmsg)


        case "WEB_WEBAUTH_AUTH_FAIL":


            ###############################
            #
            # System Log Message: 
            # Web-authentication of user [username] with fwauthd failed
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEB_WEBAUTH_AUTH_FAIL"

            $username = extract($Details,"of user (.*) with")

            @AlertGroup = "WEB_WEBAUTH_AUTH_[FAIL|OK]"
            @AlertKey = "User:" + $username

            @Summary = "Web-authentication of user " + $username + " with fwauthd failed"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($username)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "username", $username)


        case "WEB_WEBAUTH_AUTH_OK":


            ###############################
            #
            # System Log Message: 
            # Web-authentication of user [username] with fwauthd successful
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEB_WEBAUTH_AUTH_OK"

            $username = extract($Details,"of user (.*) with")

            @AlertGroup = "WEB_WEBAUTH_AUTH_[FAIL|OK]"
            @AlertKey = "User:" + $username

            @Summary = "Web-authentication of user " + $username + " with fwauthd successful"

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($username)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "username", $username)


        case "WEB_WEBAUTH_CONNECT_FAIL":


            ###############################
            #
            # System Log Message: 
            # Unable to connect to fwauthd on socket [file-descriptor]:
            # [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-WEB_WEBAUTH_CONNECT_FAIL"

            $file-descriptor = extract($Details,"socket (.*): .*$")
            $errmsg = extract($Details,"socket .*: (.*)$")

            @AlertGroup = "WEB_WEBAUTH_CONNECT_FAIL"
            @AlertKey = "File Descriptor:" + $file-descriptor

            @Summary = "Unable to connect to fwauthd on socket " + $file-descriptor + ": " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($file-descriptor,$errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "file-descriptor", $file-descriptor, "errmsg", $errmsg)



        default:

            $UseJuniperJunosDefaults = 1
            @Summary = "Unknown System Log Message (" + $message-tag + ") Received "
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $prefix + " " + $message-tag
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

