###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  Compliant with JUNOS 12.2. For further information, refer to 
#              Juniper JUNOS 12.2 Syslog Reference Guide.
#
###############################################################################

case "SDXD":

    log(DEBUG, "<<<<< Entering... juniper-junos-SDXD.include.syslog.rules >>>>>")

    switch($message-tag)
    {

        case "SDXD_BEEP_FIN_FAIL":


            ###############################
            #
            # System Log Message: 
            # Graceful close of BEEP session failed
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SDXD_BEEP_FIN_FAIL"

            @AlertGroup = "SDXD_BEEP_FIN_FAIL"
            @AlertKey = ""

            @Summary = "Graceful close of BEEP session failed"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            


        case "SDXD_BEEP_INIT_FAIL":


            ###############################
            #
            # System Log Message: 
            # BEEP initialization failed
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SDXD_BEEP_INIT_FAIL"

            @AlertGroup = "SDXD_BEEP_INIT_FAIL"
            @AlertKey = ""

            @Summary = "BEEP initialization failed"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            


        case "SDXD_CHANNEL_START_FAIL":


            ###############################
            #
            # System Log Message: 
            # Notification channel could not be started ([error-code]): [message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SDXD_CHANNEL_START_FAIL"

            $errcode = extract($Details,"started \((.*)\):")
            $message = extract($Details,"started \(.*\): (.*)")

            @AlertGroup = "SDXD_CHANNEL_START_FAIL"
            @AlertKey = "Error Code:" + $errcode

            @Summary = "Notification channel could not be started (" + $errcode + "): " +
                       $message

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($errcode,$message)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "errcode", $errcode, "message", $message)


        case "SDXD_CONNECT_FAIL":


            ###############################
            #
            # System Log Message: 
            # Connection to SDX server failed: [message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SDXD_CONNECT_FAIL"

            $message = extract($Details,"failed: (.*)")

            @AlertGroup = "SDXD_CONNECT_FAIL"
            @AlertKey = "Message:" + $message

            @Summary = "Connection to SDX server failed: " + $message

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($message)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "message", $message)


        case "SDXD_DAEMONIZE_FAIL":


            ###############################
            #
            # System Log Message: 
            # Aborting, unable to run in the background as a daemon: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SDXD_DAEMONIZE_FAIL"

            $errmsg = extract($Details,"daemon: (.*)")

            @AlertGroup = "SDXD_DAEMONIZE_FAIL"
            @AlertKey = "Error:" + $errmsg

            @Summary = "Aborting, unable to run in the background as a daemon: " + $errmsg

            $DEFAULT_Severity = 5
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "errmsg", $errmsg)


        case "SDXD_EVLIB_FAILURE":


            ###############################
            #
            # System Log Message: 
            # [function-name]: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SDXD_EVLIB_FAILURE"

            $function-name = extract($Details,"SDXD_EVLIB_FAILURE: (.*):.*")
            $errmsg = extract($Details,"SDXD_EVLIB_FAILURE: .*: (.*)")

            @AlertGroup = "SDXD_EVLIB_FAILURE"
            @AlertKey = "Function:" + $function-name + ",Error:" + $errmsg

            @Summary = $function-name + ": " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($function-name,$errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "function-name", $function-name, "errmsg", $errmsg)


        case "SDXD_KEEPALIVES_MISSED":


            ###############################
            #
            # System Log Message: 
            # No server keepalives received for three intervals
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SDXD_KEEPALIVES_MISSED"

            @AlertGroup = "SDXD_KEEPALIVES_MISSED"
            @AlertKey = ""

            @Summary = "No server keepalives received for three intervals"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            


        case "SDXD_KEEPALIVE_SEND_FAIL":


            ###############################
            #
            # System Log Message: 
            # Unable to send keepalive
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SDXD_KEEPALIVE_SEND_FAIL"

            @AlertGroup = "SDXD_KEEPALIVE_SEND_FAIL"
            @AlertKey = ""

            @Summary = "Unable to send keepalive"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            


        case "SDXD_MGMT_SOCKET_IO":


            ###############################
            #
            # System Log Message: 
            # management daemon I/O failure: [reason]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SDXD_MGMT_SOCKET_IO"

            $reason = extract($Details,"failure: (.*)$")

            @AlertGroup = "SDXD_MGMT_SOCKET_IO"
            @AlertKey = "Reason:" + $reason

            @Summary = "management daemon I/O failure: " + $reason

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($reason)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "reason", $reason)


        case "SDXD_OUT_OF_MEMORY":


            ###############################
            #
            # System Log Message: 
            # Insufficient memory during [operation]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SDXD_OUT_OF_MEMORY"

            $operation = extract($Details,"during (.*)")

            @AlertGroup = "SDXD_OUT_OF_MEMORY"
            @AlertKey = "Operation:" + $operation

            @Summary = "Insufficient memory during " + $operation

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($operation)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "operation", $operation)


        case "SDXD_PID_FILE_UPDATE":


            ###############################
            #
            # System Log Message: 
            # Unable to update process PID file: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SDXD_PID_FILE_UPDATE"

            $errmsg = extract($Details,"[Pp][Ii][Dd] file: (.*)")

            @AlertGroup = "SDXD_PID_FILE_UPDATE"
            @AlertKey = "Error:" + $errmsg

            @Summary = "Unable to update process PID file: " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "errmsg", $errmsg)


        case "SDXD_SOCKET_FAILURE":


            ###############################
            #
            # System Log Message: 
            # [operation] failed: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-SDXD_SOCKET_FAILURE"

            $operation = extract($Details,"SDXD_SOCKET_FAILURE: (.*) failed:.*")
            $errmsg = extract($Details,"SDXD_SOCKET_FAILURE:.*: (.*)$")

            @AlertGroup = "SDXD_SOCKET_FAILURE"
            @AlertKey = "Operation:" + $operation

            @Summary = $operation + " failed: " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($operation,$errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "operation", $operation, "errmsg", $errmsg)



        default:

            $UseJuniperJunosDefaults = 1
            @Summary = "Unknown System Log Message (" + $message-tag + ") Received "
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $prefix + " " + $message-tag
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

