###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  Compliant with JUNOS 12.2. For further information, refer to 
#              Juniper JUNOS 12.2 Syslog Reference Guide.
#
###############################################################################

case "BOOTPD":

    log(DEBUG, "<<<<< Entering... juniper-junos-BOOTPD.include.syslog.rules >>>>>")

    switch($message-tag)
    {

        case "BOOTPD_ARG_ERR":


            ###############################
            #
            # System Log Message: 
            # Ignoring unknown option [-option]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-BOOTPD_ARG_ERR"

            $option = extract($Details,"unknown option (.*)$")

            @AlertGroup = "BOOTPD_ARG_ERR"
            @AlertKey = "Option:" + $option

            @Summary = "Ignoring unknown option " + $option

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($option)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "option", $option)


        case "BOOTPD_BAD_ID":


            ###############################
            #
            # System Log Message: 
            # Unexpected ID 0x[assembly-id]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-BOOTPD_BAD_ID"

            $assemblyID = extract($Details,"0x(.*)")

            @AlertGroup = "BOOTPD_BAD_ID"
            @AlertKey = "Assembly ID: 0x" + $assemblyID

            @Summary = "Unexpected ID 0x" + $assemblyID

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($assemblyID)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "assemblyID", $assemblyID)


        case "BOOTPD_BOOTSTRING":


            ###############################
            #
            # System Log Message: 
            # Boot string: [boot-string]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-BOOTPD_BOOTSTRING"

            $boot-string = extract($Details,"Boot string: (.*)")

            @AlertGroup = "BOOTPD_BOOTSTRING"
            @AlertKey = "Boot String:" + $boot-string

            @Summary = "Boot String:" + $boot-string

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($boot-string)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "boot-string", $boot-string)


        case "BOOTPD_CONFIG_ERR":


            ###############################
            #
            # System Log Message: 
            # Problems with configuration file '[filename]', using defaults
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-BOOTPD_CONFIG_ERR"

            $filename = extract($Details,"configuration file (.*),? using")

            @AlertGroup = "BOOTPD_CONFIG_ERR"
            @AlertKey = "Filename:" + $filename

            @Summary = "Problems with configuration file " + $filename +
                       ", using defaults"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($filename)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "filename", $filename)


        case "BOOTPD_CONF_OPEN":


            ###############################
            #
            # System Log Message: 
            # Unable to open configuration file '[filename]'
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-BOOTPD_CONF_OPEN"

            $filename = extract($Details,"configuration file (.*)")

            @AlertGroup = "BOOTPD_CONF_OPEN"
            @AlertKey = "Filename:" + $filename

            @Summary = "Unable to open configuration file " + $filename

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($filename)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "filename", $filename)


        case "BOOTPD_DUP_PIC_SLOT":


            ###############################
            #
            # System Log Message: 
            # Duplicate default value defined for PIC [pic-slot] in FPC [fpc-slot]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-BOOTPD_DUP_PIC_SLOT"

            $pic-slot = extract($Details,"for [Pp][Ii][Cc] (.*) in")
            $fpc-slot = extract($Details,"in [Ff][Pp][Cc] (.*)")

            @AlertGroup = "BOOTPD_DUP_PIC_SLOT"
            @AlertKey = "PIC slot:" + $pic-slot + ", FPC Slot:" + $fpc-slot

            @Summary = "Duplicate default value defined for PIC " + $pic-slot +
                       " in FPC " + $fpc-slot

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($pic-slot,$fpc-slot)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "pic-slot", $pic-slot, "fpc-slot", $fpc-slot)


        case "BOOTPD_DUP_REV":


            ###############################
            #
            # System Log Message: 
            # Duplicate revision: [major-version].[minor-version]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-BOOTPD_DUP_REV"

            $revision = ltrim(rtrim(extract($Details,"revision: (.*)")))

            @AlertGroup = "BOOTPD_DUP_REV"
            @AlertKey = "Revision:" + $revision

            @Summary = "Duplicate revision: " + $revision

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($revision)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "revision", $revision)


        case "BOOTPD_DUP_SLOT":


            ###############################
            #
            # System Log Message: 
            # Duplicate slot default: [slot]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-BOOTPD_DUP_SLOT"

            $slot = extract($Details,"slot default: (.*)")

            @AlertGroup = "BOOTPD_DUP_SLOT"
            @AlertKey = "Slot:" + $slot

            @Summary = "Duplicate slot default: " + $slot

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($slot)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "slot", $slot)


        case "BOOTPD_HWDB_ERROR":


            ###############################
            #
            # System Log Message: 
            # Operation in chassis hardware database failed: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-BOOTPD_HWDB_ERROR"

            $errmsg = extract($Details,"failed: (.*)")

            @AlertGroup = "BOOTPD_HWDB_ERROR"
            @AlertKey = "Error:" + $errmsg

            @Summary = "Operation in chassis hardware database failed: " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "errmsg", $errmsg)


        case "BOOTPD_MODEL_CHK":


            ###############################
            #
            # System Log Message: 
            # Unexpected ID 0x[identifier] for model [model]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-BOOTPD_MODEL_CHK"

            $identifier = extract($Details,"0x(.*) for")
            $model = extract($Details,"for model (.*)")

            @AlertGroup = "BOOTPD_MODEL_CHK"
            @AlertKey = "Identifier:" + $identifier + ", Model:" + $model

            @Summary = "Unexpected ID 0x" + $identifier + " for model " + $model

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($identifier,$model)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "identifier", $identifier, "model", $model)


        case "BOOTPD_NEW_CONF":


            ###############################
            #
            # System Log Message: 
            # New configuration installed
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-BOOTPD_NEW_CONF"

            @AlertGroup = "BOOTPD_NEW_CONF"
            @AlertKey = ""

            @Summary = "New configuration installed"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager




        case "BOOTPD_NO_BOOTSTRING":


            ###############################
            #
            # System Log Message: 
            # No boot string found for type [board-type]
            # [major-version].[minor-version] [command-type]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-BOOTPD_NO_BOOTSTRING"

            $hw-component = extract($Details,"for type (.*)")

            @AlertGroup = "BOOTPD_NO_BOOTSTRING"
            @AlertKey = "HW Component:" + $hw-component

            @Summary = "No boot string found for type " + $hw-component

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($hw-component)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hw-component", $hw-component)


        case "BOOTPD_NO_CONFIG":


            ###############################
            #
            # System Log Message: 
            # No configuration file '[filename]', using defaults
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-BOOTPD_NO_CONFIG"

            $filename = extract($Details,"file (.*),? using")

            @AlertGroup = "BOOTPD_NO_CONFIG"
            @AlertKey = "Filename:" + $filename

            @Summary = "No configuration file " + $filename + ", using defaults"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($filename)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "filename", $filename)


        case "BOOTPD_PARSE_ERR":


            ###############################
            #
            # System Log Message: 
            # [filename]: [return-value] parse errors on SIGHUP
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-BOOTPD_PARSE_ERR"

            $filename = ltrim(rtrim(extract($Details,"BOOTPD_PARSE_ERR:(.*):.*")))
            $return-value = ltrim(rtrim(extract($Details,"BOOTPD_PARSE_ERR:.*: (.*) parse errors")))

            @AlertGroup = "BOOTPD_PARSE_ERR"
            @AlertKey = "Filename:" + $filename + ", Return value:" + $return-value

            @Summary = $filename + ": " + $return-value + " parse errors on SIGHUP"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($filename,$return-value)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "filename", $filename, "return-value", $return-value)


        case "BOOTPD_REPARSE":


            ###############################
            #
            # System Log Message: 
            # Reparsing configuration file '[filename]'
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-BOOTPD_REPARSE"

            $filename = extract($Details,"configuration file (.*)")

            @AlertGroup = "BOOTPD_REPARSE"
            @AlertKey = "Filename:" + $filename

            @Summary = "Reparsing configuration file " + $filename

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($filename)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "filename", $filename)


        case "BOOTPD_SELECT_ERR":


            ###############################
            #
            # System Log Message: 
            # select: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-BOOTPD_SELECT_ERR"

            $errmsg = extract($Details,"select: (.*)")

            @AlertGroup = "BOOTPD_SELECT_ERR"
            @AlertKey = "Error:" + $errmsg

            @Summary = "select: " + $errmsg

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "errmsg", $errmsg)


        case "BOOTPD_TIMEOUT":


            ###############################
            #
            # System Log Message: 
            # Timeout [duration] unreasonable
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-BOOTPD_TIMEOUT"

            $duration = extract($Details,"Timeout (.*) unreasonable")

            @AlertGroup = "BOOTPD_TIMEOUT"
            @AlertKey = "Duration:" + $duration

            @Summary = "Timeout " + $duration + " unreasonable"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($duration)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "duration", $duration)



        default:

            $UseJuniperJunosDefaults = 1
            @Summary = "Unknown System Log Message (" + $message-tag + ") Received "
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $prefix + " " + $message-tag
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

