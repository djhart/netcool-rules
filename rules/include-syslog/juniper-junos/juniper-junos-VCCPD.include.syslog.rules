###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  Compliant with JUNOS 12.2. For further information, refer to 
#              Juniper JUNOS 12.2 Syslog Reference Guide.
#
###############################################################################

case "VCCPD":

    log(DEBUG, "<<<<< Entering... juniper-junos-VCCPD.include.syslog.rules >>>>>")

    switch($message-tag)
    {

        case "VCCPD_KNL_VERSION":


            ###############################
            #
            # System Log Message: 
            # Routing socket version mismatch (kernel [kernel-version] != vccpd
            # [version]) -- kernel upgrade required
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-VCCPD_KNL_VERSION"

            $kernel-version = extract($Details,"\(kernel (.*) !=? .*\)")
            $vccpd-version = extract($Details,"\(kernel .* !=? vccpd (.*)\)")

            @AlertGroup = "VCCPD_KNL_VERSION"
            @AlertKey = "Kernel version:" + $kernel-version + ", VCCPD version:" + $vccpd-version

            @Summary = "Routing socket version mismatch (kernel " + $kernel-version
                        + " != " + "vccpd" + $vccpd-version + ")" + "-- kernel upgrade required"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($kernel-version,$vccpd-version)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "kernel-version", $kernel-version, "vccpd-version", $vccpd-version)


        case "VCCPD_KNL_VERSIONNONE":


            ###############################
            #
            # System Log Message: 
            # Routing socket message type [message-type]'s version is not supported
            # by kernel, expected [version] -- kernel upgrade required
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-VCCPD_KNL_VERSIONNONE"

            $msgtype = extract($Details,"type (.*)'?\s+s? version")
            $version = extract($Details,"expected (.*) --")

            @AlertGroup = "VCCPD_KNL_VERSIONNONE"
            @AlertKey = "Message type:" + $msgtype + ", Version:" + $version

            @Summary = "Routing socket message type " + $msgtype + "'s version is not supported"
                       + " by kernel, expected " + $version + " -- kernel upgrade required"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($msgtype,$version)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "msgtype", $msgtype, "version", $version)


        case "VCCPD_KNL_VERSIONOLD":


            ###############################
            #
            # System Log Message: 
            # Routing socket message type [message-type]'s version is older than
            # expected ([kernel-version] <[version]) -- consider upgrading the kernel
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-VCCPD_KNL_VERSIONOLD"

            $msgtype = extract($Details,"type (.*)'?\s+s? version")
            $kernel-version = extract($Details,"\((.*)<.*\)")
            $version = extract($Details,"\(.*<(.*)\)")

            @AlertGroup = "VCCPD_KNL_VERSIONOLD"
            @AlertKey = "Type:" + $msgtype + ", Kernel version:" + $kernel-version + ", Version:" + $version

            @Summary = "Routing socket message type " + $msgtype + "'s version is older than" 
                       + " expected " + "(" + $kernel-version + " < " + $version + ")"
                       + " -- consider upgrading the kernel"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($msgtype,$kernel-version,$version)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "msgtype", $msgtype, "kernel-version", $kernel-version, "version", $version)


        case "VCCPD_PROTOCOL_ADJDOWN":


            ###############################
            #
            # System Log Message: 
            # Lost adjacency to [neighbor-system-ids] on [interface-name],
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-VCCPD_PROTOCOL_ADJDOWN"

            $neighbour-id = extract($Details,"adjacency to (.*) on")
            $interface-name = extract($Details,"on (.*)$")

            @AlertGroup = "VCCPD_PROTOCOL_ADJ[DOWN|UP]"
            @AlertKey = "Neighbour System ID:" + $neighbour-id + ", Interface name:" + $interface-name

            @Summary = "Lost adjacency to " + $neighbour-id + " on " + $interface-name

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($neighbour-id,$interface-name)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "neighbour-id", $neighbour-id, "interface-name", $interface-name)


        case "VCCPD_PROTOCOL_ADJUP":


            ###############################
            #
            # System Log Message: 
            # New adjacency to [neighbor-system-ids] on [interface-name]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-VCCPD_PROTOCOL_ADJUP"

            $neighbour-id = extract($Details,"adjacency to (.*) on")
            $interface-name = extract($Details,"on (.*)$")

            @AlertGroup = "VCCPD_PROTOCOL_ADJ[DOWN|UP]"
            @AlertKey = "Neighbour System ID:" + $neighbour-id + ", Interface name:" + $interface-name

            @Summary = "New adjacency to " + $neighbour-id + " on " + $interface-name

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($neighbour-id,$interface-name)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "neighbour-id", $neighbour-id, "interface-name", $interface-name)


        case "VCCPD_PROTOCOL_LSPCKSUM":


            ###############################
            #
            # System Log Message: 
            # LSP checksum error, interface [interface-name], LSP id [lspl], sequence
            # [sequence-number],checksum [checksum], lifetime [duration]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-VCCPD_PROTOCOL_LSPCKSUM"

            $interface-name = extract($Details,"interface (.*),? [Ll][Ss][Pp]")
            $lsps = extract($Details,"[Ll][Ss][Pp] id (.*),? sequence")
            $seqnum = extract($Details,"sequence (.*),?checksum .*,? lifetime")
            $chksum = extract($Details,"sequence .*,?checksum (.*),? lifetime")
            $duration = extract($Details,"lifetime (.*)$")

            @AlertGroup = "VCCPD_PROTOCOL_LSPCKSUM"
            @AlertKey = "Interface:" + $interface-name + ", LSP id:" + $lsps

            @Summary = "LSP checksum error, interface " + $interface-name
                       + ", LSP id " + $lsps + ", sequence " + $seqnum 
                       + ", checksum " + $chksum + ", lifetime " + $duration 

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($interface-name,$lsps,$seqnum,$chksum,$duration)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "interface-name", $interface-name, "lsps", $lsps, "seqnum", $seqnum,
                 "chksum", $chksum, "duration", $duration)


        case "VCCPD_PROTOCOL_OVERLOAD":


            ###############################
            #
            # System Log Message: 
            # vccpd database overload
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-VCCPD_PROTOCOL_OVERLOAD"

            @AlertGroup = "VCCPD_PROTOCOL_OVERLOAD"
            @AlertKey = ""

            @Summary = "vccpd database overload"

            $DEFAULT_Severity = 5
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager



        case "VCCPD_SYSTEM":


            ###############################
            #
            # System Log Message: 
            # [reason]: [error-message]
            #
            ###############################

            $OS_EventId = "SYSLOG-juniper-junos-VCCPD_SYSTEM"

            $reason = ltrim(rtrim(extract($Details,".*:(.*):.*")))
            $errmsg = ltrim(rtrim(extract($Details,".*:.*:(.*)$")))

            @AlertGroup = "VCCPD_SYSTEM"
            @AlertKey = "Reason:" +$reason + ", Error:" + $errmsg

            @Summary = $reason + ": " + $errmsg

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($reason,$errmsg)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "reason", $reason, "errmsg", $errmsg)



        default:

            $UseJuniperJunosDefaults = 1
            @Summary = "Unknown System Log Message (" + $message-tag + ") Received "
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $prefix + " " + $message-tag
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

