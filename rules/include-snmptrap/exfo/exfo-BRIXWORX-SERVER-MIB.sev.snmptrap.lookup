###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  BRIXWORX-SERVER-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table exfo-BRIXWORX-SERVER-MIB_sev =
{
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvUnknownVfrTrap","3","1","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvVfrStatusReportTrap_sev3","3","1","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvVfrStatusReportTrap_sev1","1","2","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvVfrStatusReportTrap_default","2","1","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvProxyVfrTestTrap","2","13","1800"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvSlaTrap_okay","1","2","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvSlaTrap_warning","3","1","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvSlaTrap_critical","4","1","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvSlaTrap_unknown","2","1","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvSlaStatusPassTrap","1","2","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvSlaStatusWarnTrap","2","1","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvSlaStatusFailTrap","4","1","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvInactiveVfrTrap","3","1","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvAlertTrap_licence","4","1","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvAlertTrap_purgingdata","2","13","1800"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvAlertTrap_tcfconfigaccept","1","2","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvAlertTrap_tcfconfigreject","3","1","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvAlertTrap_preflightok","1","2","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvAlertTrap_preflightfailure","4","1","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvAlertTrap_preflightwarning","2","1","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvAlertTrap_sldsucceeded","1","2","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvAlertTrap_sldfailure","3","1","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvAlertTrap_ipconflict","3","1","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvAlertTrap_linkloss","3","1","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvAlertTrap_dataloss","3","1","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvAlertTrap_stackcb","3","1","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvAlertTrap_fanspeed","2","1","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvAlertTrap_verifierspeed","2","1","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvAlertTrap_traceroute","3","1","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvAlertTrap_verifieroffline","3","1","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvAlertTrap_unknown","5","1","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvTestTrapThresholdPassingTrap","1","2","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvTestTrapThresholdWarningTrap","2","1","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvTestTrapThresholdFailingTrap","4","1","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvTestLevelTrapThresholdPassingTrap","1","2","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvTestLevelTrapThresholdWarningTrap","2","1","0"},
    {"SNMPTRAP-exfo-BRIXWORX-SERVER-MIB-bxWxSrvTestLevelTrapThresholdFailingTrap","4","1","0"}
}
default = {"Unknown","Unknown","Unknown"}
