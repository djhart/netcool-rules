###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  LLDP-EXT-DCBX-MIB
#
###############################################################################

log(DEBUG, "<<<<< Entering... ibm-LLDP-EXT-DCBX-MIB.adv.include.snmptrap.rules >>>>>")

switch($specific-trap)
{
    case "1": ### lldpXdcbxMiscControlError

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "lldpXdcbxMiscControlError"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "lldpXdcbxPortEntry." + $lldpXdcbxPortNumber
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "2": ### lldpXdcbxMiscFeatureError

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "lldpXdcbxMiscFeatureError"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "lldpXdcbxFeatEntry." + $lldpXdcbxPortNumber + "." + $lldpXdcbxFeatType_Raw + "." + $lldpXdcbxFeatSubType
        $OS_LocalRootObj = "lldpXdcbxPortEntry." + $lldpXdcbxPortNumber
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "3": ### lldpXdcbxMultiplePeers

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "lldpXdcbxMultiplePeers"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "lldpXdcbxPortEntry." + $lldpXdcbxPortNumber 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "4": ### lldpXdcbxLldpTxDisabled

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "lldpXdcbxLldpTxDisabled"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "lldpXdcbxPortEntry." + $lldpXdcbxPortNumber 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "5": ### lldpXdcbxLldpRxDisabled

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "lldpXdcbxLldpRxDisabled"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "lldpXdcbxPortEntry." + $lldpXdcbxPortNumber 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "6": ### lldpXdcbxDupControlTlv

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "lldpXdcbxDupControlTlv"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "lldpXdcbxPortEntry." + $lldpXdcbxPortNumber
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "7": ### lldpXdcbxDupFeatureTlv

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "lldpXdcbxDupFeatureTlv"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "lldpXdcbxFeatEntry." + $lldpXdcbxPortNumber + "." + $lldpXdcbxFeatType + "." + $lldpXdcbxFeatSubType
        $OS_LocalRootObj = "lldpXdcbxPortEntry." + $lldpXdcbxPortNumber
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "8": ### lldpXdcbxPeerNoFeat

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "lldpXdcbxPeerNoFeat"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "lldpXdcbxFeatEntry." + $lldpXdcbxPortNumber + "." + $lldpXdcbxFeatType + "." + $lldpXdcbxFeatSubType
        $OS_LocalRootObj = "lldpXdcbxPortEntry." + $lldpXdcbxPortNumber
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "9": ### lldpXdcbxPeerNoResp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "lldpXdcbxPeerNoResp"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "lldpXdcbxPortEntry." + $lldpXdcbxPortNumber 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "10": ### lldpXdcbxPeerConfigMismatch

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "lldpXdcbxPeerConfigMismatch"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "lldpXdcbxPortEntry." + $lldpXdcbxPortNumber
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    default:
}

log(DEBUG, "<<<<< Leaving... ibm-LLDP-EXT-DCBX-MIB.adv.include.snmptrap.rules >>>>>")
