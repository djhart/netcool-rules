###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  ISS-MIB
#
###############################################################################

case ".1.3.6.1.4.1.2499": ###  - Notifications from ISS-MIB (200105210000Z)

    log(DEBUG, "<<<<< Entering... ibm-ISS-MIB.include.snmptrap.rules >>>>>")

    @Agent = "IBM-ISS-MIB"
    @Class = "87726"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### eventinfo

            discard

        case "2": ### logdatatrap

            discard

        case "3": ### highpriorityevent

            ##########
            # $1 = eventEntryName25 
            # $2 = eventEntryTime25 
            # $3 = eventEntryProtocol25 
            # $4 = eventEntrySourceIpAddress25 
            # $5 = eventEntryDestinationIpAddress25 
            # $6 = eventEntryIcmpType25 
            # $7 = eventEntryIcmpCode25 
            # $8 = eventEntrySourcePort25 
            # $9 = eventEntryDestinationPort25 
            # $10 = eventEntryUserActionList25 
            # $11 = eventEntryEventSpecificInfo25 
            ##########

            $eventEntryName25 = $1
            $eventEntryTime25 = $2
            $eventEntryProtocol25 = $3
            $eventEntrySourceIpAddress25 = $4
            $eventEntryDestinationIpAddress25 = $5
            $eventEntryIcmpType25 = $6
            $eventEntryIcmpCode25 = $7
            $eventEntrySourcePort25 = $8
            $eventEntryDestinationPort25 = $9
            $eventEntryUserActionList25 = $10
            $eventEntryEventSpecificInfo25 = $11

            $OS_EventId = "SNMPTRAP-ibm-ISS-MIB-highpriorityevent"

            @AlertGroup = "Priority Status" 
            @AlertKey = "event25Entry." + $eventEntryName25
	    
            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Summary = "High priority event [" + $eventEntryEventSpecificInfo25  + " ] from source " +  $eventEntrySourceIpAddress25 + ":" + $eventEntrySourcePort25 + " for target " + $eventEntryDestinationIpAddress25 + ":" + $eventEntryDestinationPort25

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ibm, "1")) {
                details($eventEntryName25,$eventEntryTime25,$eventEntryProtocol25,$eventEntrySourceIpAddress25,$eventEntryDestinationIpAddress25,$eventEntryIcmpType25,$eventEntryIcmpCode25,$eventEntrySourcePort25,$eventEntryDestinationPort25,$eventEntryUserActionList25,$eventEntryEventSpecificInfo25)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "eventEntryName25", $eventEntryName25, "eventEntryTime25", $eventEntryTime25, "eventEntryProtocol25", $eventEntryProtocol25,
                 "eventEntrySourceIpAddress25", $eventEntrySourceIpAddress25, "eventEntryDestinationIpAddress25", $eventEntryDestinationIpAddress25, "eventEntryIcmpType25", $eventEntryIcmpType25,
                 "eventEntryIcmpCode25", $eventEntryIcmpCode25, "eventEntrySourcePort25", $eventEntrySourcePort25, "eventEntryDestinationPort25", $eventEntryDestinationPort25,
                 "eventEntryUserActionList25", $eventEntryUserActionList25, "eventEntryEventSpecificInfo25", $eventEntryEventSpecificInfo25)

        case "4": ### mediumpriorityevent

            ##########
            # $1 = eventEntryName25 
            # $2 = eventEntryTime25 
            # $3 = eventEntryProtocol25 
            # $4 = eventEntrySourceIpAddress25 
            # $5 = eventEntryDestinationIpAddress25 
            # $6 = eventEntryIcmpType25 
            # $7 = eventEntryIcmpCode25 
            # $8 = eventEntrySourcePort25 
            # $9 = eventEntryDestinationPort25 
            # $10 = eventEntryUserActionList25 
            # $11 = eventEntryEventSpecificInfo25 
            ##########

            $eventEntryName25 = $1
            $eventEntryTime25 = $2
            $eventEntryProtocol25 = $3
            $eventEntrySourceIpAddress25 = $4
            $eventEntryDestinationIpAddress25 = $5
            $eventEntryIcmpType25 = $6
            $eventEntryIcmpCode25 = $7
            $eventEntrySourcePort25 = $8
            $eventEntryDestinationPort25 = $9
            $eventEntryUserActionList25 = $10
            $eventEntryEventSpecificInfo25 = $11

            $OS_EventId = "SNMPTRAP-ibm-ISS-MIB-mediumpriorityevent"

            @AlertGroup = "Priority Status" 
            @AlertKey = "event25Entry." + $eventEntryName25

	    $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Summary = "Medium priority event [" + $eventEntryEventSpecificInfo25  + " ] from source " +  $eventEntrySourceIpAddress25 + ":" + $eventEntrySourcePort25 + " for target " + $eventEntryDestinationIpAddress25 + ":" + $eventEntryDestinationPort25

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ibm, "1")) {
                details($eventEntryName25,$eventEntryTime25,$eventEntryProtocol25,$eventEntrySourceIpAddress25,$eventEntryDestinationIpAddress25,$eventEntryIcmpType25,$eventEntryIcmpCode25,$eventEntrySourcePort25,$eventEntryDestinationPort25,$eventEntryUserActionList25,$eventEntryEventSpecificInfo25)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "eventEntryName25", $eventEntryName25, "eventEntryTime25", $eventEntryTime25, "eventEntryProtocol25", $eventEntryProtocol25,
                 "eventEntrySourceIpAddress25", $eventEntrySourceIpAddress25, "eventEntryDestinationIpAddress25", $eventEntryDestinationIpAddress25, "eventEntryIcmpType25", $eventEntryIcmpType25,
                 "eventEntryIcmpCode25", $eventEntryIcmpCode25, "eventEntrySourcePort25", $eventEntrySourcePort25, "eventEntryDestinationPort25", $eventEntryDestinationPort25,
                 "eventEntryUserActionList25", $eventEntryUserActionList25, "eventEntryEventSpecificInfo25", $eventEntryEventSpecificInfo25)

        case "5": ### lowpriorityevent

            ##########
            # $1 = eventEntryName25 
            # $2 = eventEntryTime25 
            # $3 = eventEntryProtocol25 
            # $4 = eventEntrySourceIpAddress25 
            # $5 = eventEntryDestinationIpAddress25 
            # $6 = eventEntryIcmpType25 
            # $7 = eventEntryIcmpCode25 
            # $8 = eventEntrySourcePort25 
            # $9 = eventEntryDestinationPort25 
            # $10 = eventEntryUserActionList25 
            # $11 = eventEntryEventSpecificInfo25 
            ##########

            $eventEntryName25 = $1
            $eventEntryTime25 = $2
            $eventEntryProtocol25 = $3
            $eventEntrySourceIpAddress25 = $4
            $eventEntryDestinationIpAddress25 = $5
            $eventEntryIcmpType25 = $6
            $eventEntryIcmpCode25 = $7
            $eventEntrySourcePort25 = $8
            $eventEntryDestinationPort25 = $9
            $eventEntryUserActionList25 = $10
            $eventEntryEventSpecificInfo25 = $11

            $OS_EventId = "SNMPTRAP-ibm-ISS-MIB-lowpriorityevent"

            @AlertGroup = "Priority Status" 
            @AlertKey = "event25Entry." + $eventEntryName25

	    $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Summary = "Low priority event [" + $eventEntryEventSpecificInfo25  + " ] from source " +  $eventEntrySourceIpAddress25 + ":" + $eventEntrySourcePort25 + " for target " + $eventEntryDestinationIpAddress25 + ":" + $eventEntryDestinationPort25

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ibm, "1")) {
                details($eventEntryName25,$eventEntryTime25,$eventEntryProtocol25,$eventEntrySourceIpAddress25,$eventEntryDestinationIpAddress25,$eventEntryIcmpType25,$eventEntryIcmpCode25,$eventEntrySourcePort25,$eventEntryDestinationPort25,$eventEntryUserActionList25,$eventEntryEventSpecificInfo25)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "eventEntryName25", $eventEntryName25, "eventEntryTime25", $eventEntryTime25, "eventEntryProtocol25", $eventEntryProtocol25,
                 "eventEntrySourceIpAddress25", $eventEntrySourceIpAddress25, "eventEntryDestinationIpAddress25", $eventEntryDestinationIpAddress25, "eventEntryIcmpType25", $eventEntryIcmpType25,
                 "eventEntryIcmpCode25", $eventEntryIcmpCode25, "eventEntrySourcePort25", $eventEntrySourcePort25, "eventEntryDestinationPort25", $eventEntryDestinationPort25,
                 "eventEntryUserActionList25", $eventEntryUserActionList25, "eventEntryEventSpecificInfo25", $eventEntryEventSpecificInfo25)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ibm, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, ibm-ISS-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, ibm-ISS-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/ibm/ibm-ISS-MIB_Notifications.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/ibm/ibm-ISS-MIB_Notifications.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... ibm-ISS-MIB.include.snmptrap.rules >>>>>")

case ".1.3.6.1.4.1.2499.1.6.1": ###  - Notifications from ISS-MIB (200105210000Z)	   

log(DEBUG, "<<<<< Entering... ibm-ISS-MIB.include.snmptrap.rules >>>>>")

    @Agent = "IBM-ISS-MIB"
    @Class = "87726"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### bypassInOut

            discard
	    
        case "2": ### bypassDiagnostics

            discard
	    
        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ibm, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
	}	    

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, ibm-ISS-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, ibm-ISS-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/ibm/ibm-ISS-MIB_Traps.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/ibm/ibm-ISS-MIB_Traps.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... ibm-ISS-MIB.include.snmptrap.rules >>>>>")  
