###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  BNT-GbESM-1G-L2L3-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table ibm-BNT-GbESM-1G-L2L3-MIB_sev =
{
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwDefGwUp","1","2","0"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwDefGwDown","4","1","0"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwDefGwInService","1","2","0"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwDefGwNotInService","3","1","0"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwVrrpNewMaster","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwVrrpNewBackup","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwVrrpAuthFailure","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwLoginFailure","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwTempExceedThreshold","3","1","0"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwValidLogin","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwApplyComplete","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwSaveComplete","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwFwDownloadSucess","1","2","0"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwFwDownloadFailure","3","1","0"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwStgNewRoot","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwCistNewRoot","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwStgTopologyChanged","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwCistTopologyChanged","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwHotlinksMasterUp","1","2","0"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwHotlinksMasterDn","4","1","0"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwHotlinksBackupUp","1","2","0"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwHotlinksBackupDn","4","1","0"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwHotlinksNone","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwNtpNotServer","3","1","0"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwNtpUpdateClock","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwValidLogout","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwStgBlockingState","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbESM-1G-L2L3-MIB-altSwTempReturnThreshold","1","2","0"}
}
default = {"Unknown","Unknown","Unknown"}

