###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  ITNCM-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table ibm-ITNCM-MIB_sev =
{
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmPolicyCompliantTrap","1","2","0"},
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmPolicyViolationTrap_critical","5","1","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmPolicyViolationTrap_major","4","1","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmPolicyViolationTrap_minor","3","1","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmPolicyViolationTrap_warning","2","1","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmPolicyViolationTrap_indeterminate","1","1","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmPolicyViolationTrap_unknown","2","1","0"},
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmPolicyExemptTrap","2","13","0"},
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmPolicyNotAssessedTrap","2","1","0"},
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmResourceDeviceActivityTrap","2","13","0"},
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmResourceRealmActivityTrap","1","13","0"},
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmResourceCommandActivityTrap","1","13","0"},
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWOSUpgrade_readyForExecution","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWOSUpgrade_executing","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWOSUpgrade_finished","1","2","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWOSUpgrade_cancelled","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWOSUpgrade_expired","5","1","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWOSUpgrade_pendingApproval","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWOSUpgrade_resumed","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWOSUpgrade_sleeping","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWOSUpgrade_unknown","2","1","0"},
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWConfigurationActivity_readyForExecution","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWConfigurationActivity_executing","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWConfigurationActivity_finished","1","2","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWConfigurationActivity_cancelled","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWConfigurationActivity_expired","5","1","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWConfigurationActivity_pendingApproval","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWConfigurationActivity_resumed","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWConfigurationActivity_sleeping","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWConfigurationActivity_unknown","2","1","0"},
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWConfigurationRetrieval_readyForExecution","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWConfigurationRetrieval_executing","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWConfigurationRetrieval_finished","1","2","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWConfigurationRetrieval_cancelled","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWConfigurationRetrieval_expired","5","1","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWConfigurationRetrieval_pendingApproval","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWConfigurationRetrieval_resumed","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWConfigurationRetrieval_sleeping","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWConfigurationRetrieval_unknown","2","1","0"},
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWDeviceAccessTrap_readyForExecution","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWDeviceAccessTrap_executing","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWDeviceAccessTrap_finished","1","2","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWDeviceAccessTrap_cancelled","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWDeviceAccessTrap_expired","5","1","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWDeviceAccessTrap_pendingApproval","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWDeviceAccessTrap_resumed","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWDeviceAccessTrap_sleeping","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWDeviceAccessTrap_unknown","2","1","0"},
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWDriverUpdateTrap_readyForExecution","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWDriverUpdateTrap_executing","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWDriverUpdateTrap_finished","1","2","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWDriverUpdateTrap_cancelled","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWDriverUpdateTrap_expired","5","1","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWDriverUpdateTrap_pendingApproval","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWDriverUpdateTrap_resumed","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWDriverUpdateTrap_sleeping","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWDriverUpdateTrap_unknown","2","1","0"},
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWInternalOperationTrap_readyForExecution","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWInternalOperationTrap_executing","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWInternalOperationTrap_finished","1","2","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWInternalOperationTrap_cancelled","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWInternalOperationTrap_expired","5","1","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWInternalOperationTrap_pendingApproval","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWInternalOperationTrap_resumed","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWInternalOperationTrap_sleeping","2","13","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmUOWInternalOperationTrap_unknown","2","1","0"},
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmTaskAuthenticationFailureTrap","2","1","0"},
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmTaskDeviceCommsFailureTrap","4","1","0"},
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmTaskDeviceModelChangeTrap","5","1","0"},
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmTaskFailureTrap_success","1","2","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmTaskFailureTrap_failure","5","1","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmTaskFailureTrap_notAttempted","4","1","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmTaskFailureTrap_unknown","2","1","0"},
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmTaskSuccessTrap","1","2","0"},
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmAutodiscoveryTaskFailureTrap_oobc","1","0","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmAutodiscoveryTaskFailureTrap_idt","1","0","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmAutodiscoveryTaskFailureTrap_compliance","1","0","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmAutodiscoveryTaskFailureTrap_itnm","1","0","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmAutodiscoveryTaskFailureTrap_gui","1","0","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmAutodiscoveryTaskFailureTrap_api","1","0","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmAutodiscoveryTaskFailureTrap_services","1","0","0"}, 
    {"SNMPTRAP-ibm-ITNCM-MIB-itncmAutodiscoveryTaskFailureTrap_unknown","2","1","0"}
}
default = {"Unknown","Unknown","Unknown"}

