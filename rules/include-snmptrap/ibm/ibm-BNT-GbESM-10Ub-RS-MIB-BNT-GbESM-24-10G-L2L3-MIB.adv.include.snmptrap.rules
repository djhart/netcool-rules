###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  BNT-GbESM-10Ub-RS-MIB
#          -  BNT-GbESM-24-10G-L2L3-MIB
#
###############################################################################

log(DEBUG, "<<<<< Entering... ibm-BNT-GbESM-10Ub-RS-MIB-BNT-GbESM-24-10G-L2L3-MIB.adv.include.snmptrap.rules >>>>>")

switch($specific-trap)
{
    case "2": ### altSwDefGwUp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwDefGwUp"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ipCurCfgGwEntry." + $ipCurCfgGwIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "3": ### altSwDefGwDown

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwDefGwDown"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ipCurCfgGwEntry." + $ipCurCfgGwIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "4": ### altSwDefGwInService

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwDefGwInService"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ipCurCfgGwEntry." + $ipCurCfgGwIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "5": ### altSwDefGwNotInService

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwDefGwNotInService"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ipCurCfgGwEntry." + $ipCurCfgGwIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "16": ### altSwVrrpNewMaster

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwVrrpNewMaster"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "vrrpCurCfgVirtRtrTableEntry." + $vrrpCurCfgVirtRtrIndx 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "17": ### altSwVrrpNewBackup

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwVrrpNewBackup"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "vrrpCurCfgVirtRtrTableEntry." + $vrrpCurCfgVirtRtrIndx 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "18": ### altSwVrrpAuthFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwVrrpAuthFailure"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "vrrpCurCfgIfTableEntry." + $vrrpCurCfgIfIndx 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "19": ### altSwLoginFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwLoginFailure"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "22": ### altSwTempExceedThreshold

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwTempExceedThreshold"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "25": ### altSwValidLogin

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwValidLogin"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "27": ### altSwApplyComplete

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwApplyComplete"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "28": ### altSwSaveComplete

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwSaveComplete"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "29": ### altSwFwDownloadSucess

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwFwDownloadSucess"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "30": ### altSwFwDownloadFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwFwDownloadFailure"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "42": ### altSwStgNewRoot

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwStgNewRoot"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "stgCurCfgTableEntry." + $stgCurCfgIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "43": ### altSwCistNewRoot

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwCistNewRoot"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "44": ### altSwStgTopologyChanged

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwStgTopologyChanged"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "stgCurCfgTableEntry." + $stgCurCfgIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "45": ### altSwCistTopologyChanged

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwCistTopologyChanged"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "46": ### altSwHotlinksMasterUp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwHotlinksMasterUp"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "47": ### altSwHotlinksMasterDn

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwHotlinksMasterDn"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "48": ### altSwHotlinksBackupUp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwHotlinksBackupUp"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "49": ### altSwHotlinksBackupDn

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwHotlinksBackupDn"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "50": ### altSwHotlinksNone

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwHotlinksNone"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "61": ### altSwNtpNotServer

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwNtpNotServer"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "62": ### altSwNtpUpdateClock

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwNtpUpdateClock"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "63": ### altSwValidLogout

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwValidLogout"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "64": ### altSwStgBlockingState

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwStgBlockingState"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "65": ### altSwECMPGatewayUp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwECMPGatewayUp"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "66": ### altSwECMPGatewayDown

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwECMPGatewayDown"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "71": ### altSwLACPPortBlocked

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwLACPPortBlocked"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "72": ### altSwLACPPortUnblocked

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwLACPPortUnblocked"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "73": ### altSwStackSwitchAttached

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwStackSwitchAttached"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "74": ### altSwStackSwitchDettached

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwStackSwitchDettached"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "75": ### altSwStackBackupPresent

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwStackBackupPresent"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "76": ### altSwStackBackupGone

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwStackBackupGone"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "77": ### altVMGroupVMotion

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altVMGroupVMotion"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "78": ### altVMGroupVMOnline

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altVMGroupVMOnline"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "79": ### altVMGroupVMVlanChange

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altVMGroupVMVlanChange"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "80": ### altSwStackMasterAfterInit

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwStackMasterAfterInit"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "81": ### altSwStackMasterFromBackup

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwStackMasterFromBackup"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "82": ### altSwStackDuplicateJoinAttempt

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwStackDuplicateJoinAttempt"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "83": ### altSwStackLinkUp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwStackLinkUp"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "84": ### altSwStackLinkDown

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwStackLinkDown"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "85": ### altSwStackXferError

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwStackXferError"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "86": ### altSwStackXferSuccess

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwStackXferSuccess"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "87": ### altSwStackSwitchTypeMismatch

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwStackSwitchTypeMismatch"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "88": ### altSwStackImageSlotMismatch

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwStackImageSlotMismatch"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "89": ### altSwStackImageVersMismatch

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwStackImageVersMismatch"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "90": ### altSwStackChassisTypeMismatch

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwStackChassisTypeMismatch"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "91": ### altSwStackBcsBayMismatch

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwStackBcsBayMismatch"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "92": ### altSwStackBootCfgMismatch

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwStackBootCfgMismatch"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "93": ### altSwStackNvramMasterJoin

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwStackNvramMasterJoin"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "94": ### altSwStackForceDetach

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwStackForceDetach"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "95": ### altSwTempReturnThreshold

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwTempReturnThreshold"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "96": ### altSwAmpTopologyUp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwAmpTopologyUp"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "97": ### altSwAmpTopologyDown

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "altSwAmpTopologyDown"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Sys Name: " + $sysName + ", Location: " + $sysLocation 
        $OS_LocalRootObj = "Sys Name: " + $sysName
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    default:
}

log(DEBUG, "<<<<< Leaving... ibm-BNT-GbESM-10Ub-RS-MIB-BNT-GbESM-24-10G-L2L3-MIB.adv.include.snmptrap.rules >>>>>")
