###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  IBM-SVC-MIB
#
###############################################################################

case ".1.3.6.1.4.1.2.6.190": ### - Notifications from IBM-SVC-MIB (201517040000Z)

    log(DEBUG, "<<<<< Entering... ibm-IBM-SVC-MIB.include.snmptrap.rules >>>>>")

    @Agent = "ibm-IBM-SVC-MIB"
    @Class = "87055"
    
    $OPTION_TypeFieldUsage = "3.6"
    

    switch($specific-trap)
    {
    
     
        case "1": ### tsveETrap
        
            ##########
            # $1 = tsveMACH
            # $2 = tsveSERI
            # $3 = tsveERRI
            # $4 = tsveERRC
            # $5 = tsveSWVE
            # $6 = tsveFRUP
            # $7 = tsveCLUS
            # $8 = tsveNODE
            # $9 = tsveERRS
            # $10 = tsveTIME
            # $11 = tsveOBJT
            # $12 = tsveOBJI
            # $13 = tsveOBJN
            # $14 = tsveCOPY
            # $15 = tsveMPNO
            # $16 = tsveADD1
            # $17 = tsveADD2
            ##########
            
            $tsveMACH = $1
            $tsveSERI = $2
            $tsveERRI = $3
            $tsveERRC = $4
            $tsveSWVE = $5
            $tsveFRUP = $6
            $tsveCLUS = $7
            $tsveNODE = $8
            $tsveERRS = $9
            $tsveTIME = $10
            $tsveOBJT = $11
            $tsveOBJI = $12
            $tsveOBJN = $13
            $tsveCOPY = $14
            $tsveMPNO = $15
            $tsveADD1 = $16
            $tsveADD2 = $17
            
            $mach = extract($tsveMACH, "#\s*(.*)")
            $seri = extract($tsveSERI, "#\s*(.*)")
            $erri = extract($tsveERRI, "#\s*(.*)")
            $errc = extract($tsveERRC, "#\s*(.*)")
            $swve = extract($tsveSWVE, "#\s*(.*)")
            $frup = extract($tsveFRUP, "#\s*(.*)")
            $clus = extract($tsveCLUS, "#\s*(.*)")
            $node = extract($tsveNODE, "#\s*(.*)")
            $errs = extract($tsveERRS, "#\s*(.*)")
            $time = extract($tsveTIME, "#\s*(.*)")
            $objt = extract($tsveOBJT, "#\s*(.*)")
            $obji = extract($tsveOBJI, "#\s*(.*)")
            $objn = extract($tsveOBJN, "#\s*(.*)")
            $copy = extract($tsveCOPY, "#\s*(.*)")
            $mpno = extract($tsveMPNO, "#\s*(.*)")
            $add1 = extract($tsveADD1, "#\s*(.*)")
            $add2 = extract($tsveADD2, "#\s*(.*)")
            
            $nodeValue = extract($node, "=\s*([0-9]+)")
            $objtValue = extract($objt, "=\s*(.*)")
            $objiValue = extract($obji, "=\s*([0-9]+)")
            
            $ecDescription = extract($errc, "=\s*[0-9]+\s+:\s+(.*)")
            $ecNumber = extract($errc, "=\s*([0-9]+)\s+:\s+.*")
            
            $eiDescription = extract($erri, "=\s+[0-9]+\s+:\s+(.*)")
            $eiNumber = extract($erri, "=\s*([0-9]+)\s+:\s+.*")
            
            $eSequenceNum = extract($errs, "=\s*([0-9]+)")
            
            
            $OS_EventId = "SNMPTRAP-ibm-IBM-SVC-MIB-tsveETrap"

            @AlertGroup = "Error Trap"
            @AlertKey = "NodeID: " + $nodeValue + ", ObjType: " + $objtValue + ", ObjID: " + $objiValue
            @Summary = " ( ErrorSeqNum: " + $eSequenceNum + ", " + @AlertKey + " ) "
            
            if (!match($ecNumber, "")) {
            	@AlertGroup = "ErrorCode: " + $ecNumber
            	if(!match($ecDescription, "")) {
            		@Summary = $ecDescription + @Summary
            	}
            }else {
            	@AlertGroup = "ErrorID: " + $eiNumber
            	@Summary = $eiDescription + @Summary
            }
            
            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            @ExtendedAttr = nvp_add(@ExtendedAttr, "tsveMACH", $tsveMACH, "tsveSERI", $tsveSERI, 
                "tsveERRI", $tsveERRI, "tsveERRC", $tsveERRC, "tsveSWVE", $tsveSWVE, 
                "tsveFRUP", $tsveFRUP, "tsveCLUS", $tsveCLUS, "tsveNODE", $tsveNODE, 
                "tsveERRS", $tsveERRS, "tsveTIME", $tsveTIME, "tsveOBJT", $tsveOBJT, 
                "tsveOBJI", $tsveOBJI, "tsveOBJN", $tsveOBJN, "tsveCOPY", $tsveCOPY, 
                "tsveMPNO", $tsveMPNO, "tsveADD1", $tsveADD1, "tsveADD2", $tsveADD2, 
                "mach", $mach, "seri", $seri, "erri", $erri, 
                "errc", $errc, "swve", $swve, "frup", $frup, 
                "clus", $clus, "node", $node, "errs", $errs, 
                "time", $time, "objt", $objt, "obji", $obji, 
                "objn", $objn, "copy", $copy, "mpno", $mpno, 
                "add1", $add1, "add2", $add2, "nodeValue", $nodeValue, 
                "objtValue", $objtValue, "objiValue", $objiValue, "ecDescription", $ecDescription, 
                "ecNumber", $ecNumber, "eiDescription", $eiDescription, "eiNumber", $eiNumber, 
                "eSequenceNum", $eSequenceNum)
         
        case "2": ### tsveWTrap
        
            ##########
            # $1 = tsveMACH
            # $2 = tsveSERI
            # $3 = tsveERRI
            # $4 = tsveERRC
            # $5 = tsveSWVE
            # $6 = tsveFRUP
            # $7 = tsveCLUS
            # $8 = tsveNODE
            # $9 = tsveERRS
            # $10 = tsveTIME
            # $11 = tsveOBJT
            # $12 = tsveOBJI
            # $13 = tsveOBJN
            # $14 = tsveCOPY
            # $15 = tsveMPNO
            # $16 = tsveADD1
            # $17 = tsveADD2
            ##########
            
            $tsveMACH = $1
            $tsveSERI = $2
            $tsveERRI = $3
            $tsveERRC = $4
            $tsveSWVE = $5
            $tsveFRUP = $6
            $tsveCLUS = $7
            $tsveNODE = $8
            $tsveERRS = $9
            $tsveTIME = $10
            $tsveOBJT = $11
            $tsveOBJI = $12
            $tsveOBJN = $13
            $tsveCOPY = $14
            $tsveMPNO = $15
            $tsveADD1 = $16
            $tsveADD2 = $17
            
            $mach = extract($tsveMACH, "#\s*(.*)")
            $seri = extract($tsveSERI, "#\s*(.*)")
            $erri = extract($tsveERRI, "#\s*(.*)")
            $errc = extract($tsveERRC, "#\s*(.*)")
            $swve = extract($tsveSWVE, "#\s*(.*)")
            $frup = extract($tsveFRUP, "#\s*(.*)")
            $clus = extract($tsveCLUS, "#\s*(.*)")
            $node = extract($tsveNODE, "#\s*(.*)")
            $errs = extract($tsveERRS, "#\s*(.*)")
            $time = extract($tsveTIME, "#\s*(.*)")
            $objt = extract($tsveOBJT, "#\s*(.*)")
            $obji = extract($tsveOBJI, "#\s*(.*)")
            $objn = extract($tsveOBJN, "#\s*(.*)")
            $copy = extract($tsveCOPY, "#\s*(.*)")
            $mpno = extract($tsveMPNO, "#\s*(.*)")
            $add1 = extract($tsveADD1, "#\s*(.*)")
            $add2 = extract($tsveADD2, "#\s*(.*)")
            
            $nodeValue = extract($node, "=\s*([0-9]+)")
            $objtValue = extract($objt, "=\s*(.*)")
            $objiValue = extract($obji, "=\s*([0-9]+)")
            
            $ecDescription = extract($errc, "=\s*[0-9]+\s+:\s+(.*)")
            $ecNumber = extract($errc, "=\s*([0-9]+)\s+:\s+.*")
            
            $eiDescription = extract($erri, "=\s+[0-9]+\s+:\s+(.*)")
            $eiNumber = extract($erri, "=\s*([0-9]+)\s+:\s+.*")
            
            $eSequenceNum = extract($errs, "=\s*([0-9]+)")
            
            
            $OS_EventId = "SNMPTRAP-ibm-IBM-SVC-MIB-tsveWTrap"

            @AlertGroup = "Warning Trap"
            @AlertKey = "NodeID: " + $nodeValue + ", ObjType: " + $objtValue + ", ObjID: " + $objiValue
            @Summary = " ( ErrorSeqNum: " + $eSequenceNum + ", " + @AlertKey + " ) "
            
            if (!match($ecNumber, "")) {
            	@AlertGroup = "ErrorCode: " + $ecNumber
            	if(!match($ecDescription, "")) {
            		@Summary = $ecDescription + @Summary
            	}
            }else {
            	@AlertGroup = "ErrorID: " + $eiNumber
            	@Summary = $eiDescription + @Summary
            }
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            @ExtendedAttr = nvp_add(@ExtendedAttr, "tsveMACH", $tsveMACH, "tsveSERI", $tsveSERI, 
                "tsveERRI", $tsveERRI, "tsveERRC", $tsveERRC, "tsveSWVE", $tsveSWVE, 
                "tsveFRUP", $tsveFRUP, "tsveCLUS", $tsveCLUS, "tsveNODE", $tsveNODE, 
                "tsveERRS", $tsveERRS, "tsveTIME", $tsveTIME, "tsveOBJT", $tsveOBJT, 
                "tsveOBJI", $tsveOBJI, "tsveOBJN", $tsveOBJN, "tsveCOPY", $tsveCOPY, 
                "tsveMPNO", $tsveMPNO, "tsveADD1", $tsveADD1, "tsveADD2", $tsveADD2, 
                "mach", $mach, "seri", $seri, "erri", $erri, 
                "errc", $errc, "swve", $swve, "frup", $frup, 
                "clus", $clus, "node", $node, "errs", $errs, 
                "time", $time, "objt", $objt, "obji", $obji, 
                "objn", $objn, "copy", $copy, "mpno", $mpno, 
                "add1", $add1, "add2", $add2, "nodeValue", $nodeValue, 
                "objtValue", $objtValue, "objiValue", $objiValue, "ecDescription", $ecDescription, 
                "ecNumber", $ecNumber, "eiDescription", $eiDescription, "eiNumber", $eiNumber, 
                "eSequenceNum", $eSequenceNum)
         
        case "3": ### tsveITrap
        
            ##########
            # $1 = tsveMACH
            # $2 = tsveSERI
            # $3 = tsveERRI
            # $4 = tsveERRC
            # $5 = tsveSWVE
            # $6 = tsveFRUP
            # $7 = tsveCLUS
            # $8 = tsveNODE
            # $9 = tsveERRS
            # $10 = tsveTIME
            # $11 = tsveOBJT
            # $12 = tsveOBJI
            # $13 = tsveOBJN
            # $14 = tsveCOPY
            # $15 = tsveMPNO
            # $16 = tsveADD1
            # $17 = tsveADD2
            ##########
            
            $tsveMACH = $1
            $tsveSERI = $2
            $tsveERRI = $3
            $tsveERRC = $4
            $tsveSWVE = $5
            $tsveFRUP = $6
            $tsveCLUS = $7
            $tsveNODE = $8
            $tsveERRS = $9
            $tsveTIME = $10
            $tsveOBJT = $11
            $tsveOBJI = $12
            $tsveOBJN = $13
            $tsveCOPY = $14
            $tsveMPNO = $15
            $tsveADD1 = $16
            $tsveADD2 = $17
            
            $mach = extract($tsveMACH, "#\s*(.*)")
            $seri = extract($tsveSERI, "#\s*(.*)")
            $erri = extract($tsveERRI, "#\s*(.*)")
            $errc = extract($tsveERRC, "#\s*(.*)")
            $swve = extract($tsveSWVE, "#\s*(.*)")
            $frup = extract($tsveFRUP, "#\s*(.*)")
            $clus = extract($tsveCLUS, "#\s*(.*)")
            $node = extract($tsveNODE, "#\s*(.*)")
            $errs = extract($tsveERRS, "#\s*(.*)")
            $time = extract($tsveTIME, "#\s*(.*)")
            $objt = extract($tsveOBJT, "#\s*(.*)")
            $obji = extract($tsveOBJI, "#\s*(.*)")
            $objn = extract($tsveOBJN, "#\s*(.*)")
            $copy = extract($tsveCOPY, "#\s*(.*)")
            $mpno = extract($tsveMPNO, "#\s*(.*)")
            $add1 = extract($tsveADD1, "#\s*(.*)")
            $add2 = extract($tsveADD2, "#\s*(.*)")
            
            $nodeValue = extract($node, "=\s*([0-9]+)")
            $objtValue = extract($objt, "=\s*(.*)")
            $objiValue = extract($obji, "=\s*([0-9]+)")
            
            $ecDescription = extract($errc, "=\s*[0-9]+\s+:\s+(.*)")
            $ecNumber = extract($errc, "=\s*([0-9]+)\s+:\s+.*")
            
            $eiDescription = extract($erri, "=\s+[0-9]+\s+:\s+(.*)")
            $eiNumber = extract($erri, "=\s*([0-9]+)\s+:\s+.*")
            
            $eSequenceNum = extract($errs, "=\s*([0-9]+)")
            
            
            $OS_EventId = "SNMPTRAP-ibm-IBM-SVC-MIB-tsveITrap"

            @AlertGroup = "Information Trap"
            @AlertKey = "NodeID: " + $nodeValue + ", ObjType: " + $objtValue + ", ObjID: " + $objiValue
            @Summary = " ( ErrorSeqNum: " + $eSequenceNum + ", " + @AlertKey + " ) "
            
            if (!match($ecNumber, "")) {
            	@AlertGroup = "ErrorCode: " + $ecNumber
            	if(!match($ecDescription, "")) {
            		@Summary = $ecDescription + @Summary
            	}
            }else {
            	@AlertGroup = "ErrorID: " + $eiNumber
            	@Summary = $eiDescription + @Summary
            }
            
            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 0               
            
            update(@Severity)
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            @ExtendedAttr = nvp_add(@ExtendedAttr, "tsveMACH", $tsveMACH, "tsveSERI", $tsveSERI, 
                "tsveERRI", $tsveERRI, "tsveERRC", $tsveERRC, "tsveSWVE", $tsveSWVE, 
                "tsveFRUP", $tsveFRUP, "tsveCLUS", $tsveCLUS, "tsveNODE", $tsveNODE, 
                "tsveERRS", $tsveERRS, "tsveTIME", $tsveTIME, "tsveOBJT", $tsveOBJT, 
                "tsveOBJI", $tsveOBJI, "tsveOBJN", $tsveOBJN, "tsveCOPY", $tsveCOPY, 
                "tsveMPNO", $tsveMPNO, "tsveADD1", $tsveADD1, "tsveADD2", $tsveADD2, 
                "mach", $mach, "seri", $seri, "erri", $erri, 
                "errc", $errc, "swve", $swve, "frup", $frup, 
                "clus", $clus, "node", $node, "errs", $errs, 
                "time", $time, "objt", $objt, "obji", $obji, 
                "objn", $objn, "copy", $copy, "mpno", $mpno, 
                "add1", $add1, "add2", $add2, "nodeValue", $nodeValue, 
                "objtValue", $objtValue, "objiValue", $objiValue, "ecDescription", $ecDescription, 
                "ecNumber", $ecNumber, "eiDescription", $eiDescription, "eiNumber", $eiNumber, 
                "eSequenceNum", $eSequenceNum)
         
        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, ibm-IBM-SVC-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, ibm-IBM-SVC-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/ibm/ibm-IBM-SVC-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/ibm/ibm-IBM-SVC-MIB.user.include.snmptrap.rules"


##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... ibm-IBM-SVC-MIB.include.snmptrap.rules >>>>>")


