###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  BNT-GbTOR-1-10G-RS-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table ibm-BNT-GbTOR-1-10G-RS-MIB_sev =
{
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swDefGwUp","1","2","0"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swDefGwDown","4","1","0"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swDefGwInService","1","2","0"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swDefGwNotInService","3","1","0"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swLoginFailure","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swTempExceedThreshold","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swValidLogin","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swApplyComplete","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swSaveComplete","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swFwDownloadSucess","1","2","0"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swFwDownloadFailure","3","1","0"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swStgNewRoot","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swCistNewRoot","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swStgTopologyChanged","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swCistTopologyChanged","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swHotlinksMasterUp","1","2","0"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swHotlinksMasterDn","4","1","0"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swHotlinksBackupUp","1","2","0"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swHotlinksBackupDn","4","1","0"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swHotlinksNone","4","1","0"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swValidLogout","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swECMPGatewayUp","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swECMPGatewayDown","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swTeamingCtrlUp","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swTeamingCtrlDown","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swTeamingCtrlDownTearDownBlked","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swTeamingCtrlError","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swTempReturnThreshold","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swAmpTopologyUp","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swAmpTopologyDown","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swVMGroupVMotion","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swVMGroupVMOnline","2","13","1800"},
    {"SNMPTRAP-ibm-BNT-GbTOR-1-10G-RS-MIB-swVMGroupVMVlanChange","2","13","1800"}
}
default = {"Unknown","Unknown","Unknown"}
