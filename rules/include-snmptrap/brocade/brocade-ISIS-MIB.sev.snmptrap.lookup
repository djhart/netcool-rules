###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  ISIS-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table brocade-ISIS-MIB_sev =
{
    {"SNMPTRAP-brocade-ISIS-MIB-isisDatabaseOverload_off","2","1","0"}, 
    {"SNMPTRAP-brocade-ISIS-MIB-isisDatabaseOverload_on","1","2","0"}, 
    {"SNMPTRAP-brocade-ISIS-MIB-isisDatabaseOverload_waiting","2","13","1800"}, 
    {"SNMPTRAP-brocade-ISIS-MIB-isisDatabaseOverload_overloaded","3","1","0"}, 
    
    {"SNMPTRAP-brocade-ISIS-MIB-isisDatabaseOverload_unknown","2","1","0"},
        
    {"SNMPTRAP-brocade-ISIS-MIB-isisManualAddressDrops","3","1","0"},
    {"SNMPTRAP-brocade-ISIS-MIB-isisCorruptedLSPDetected","3","1","0"},
    {"SNMPTRAP-brocade-ISIS-MIB-isisAttemptToExceedMaxSequence","3","1","0"},
    {"SNMPTRAP-brocade-ISIS-MIB-isisIDLenMismatch","3","1","0"},
    {"SNMPTRAP-brocade-ISIS-MIB-isisMaxAreaAddressesMismatch","3","1","0"},
    {"SNMPTRAP-brocade-ISIS-MIB-isisOwnLSPPurge","3","1","0"},
    {"SNMPTRAP-brocade-ISIS-MIB-isisSequenceNumberSkip","3","1","0"},
    {"SNMPTRAP-brocade-ISIS-MIB-isisAuthenticationTypeFailure","3","1","0"},
    {"SNMPTRAP-brocade-ISIS-MIB-isisAuthenticationFailure","3","1","0"},
    {"SNMPTRAP-brocade-ISIS-MIB-isisVersionSkew","2","1","0"},
    {"SNMPTRAP-brocade-ISIS-MIB-isisAreaMismatch","2","13","1800"},
    {"SNMPTRAP-brocade-ISIS-MIB-isisRejectedAdjacency","2","1","0"},
    {"SNMPTRAP-brocade-ISIS-MIB-isisLSPTooLargeToPropagate","2","1","0"},
    {"SNMPTRAP-brocade-ISIS-MIB-isisOrigLSPBuffSizeMismatch","2","1","0"},
    {"SNMPTRAP-brocade-ISIS-MIB-isisProtocolsSupportedMismatch","2","1","0"},
    {"SNMPTRAP-brocade-ISIS-MIB-isisAdjacencyChange_down","3","1","0"}, 
    {"SNMPTRAP-brocade-ISIS-MIB-isisAdjacencyChange_initializing","2","13","1800"}, 
    {"SNMPTRAP-brocade-ISIS-MIB-isisAdjacencyChange_up","1","2","0"}, 
    {"SNMPTRAP-brocade-ISIS-MIB-isisAdjacencyChange_failed","3","1","0"}, 
    
    {"SNMPTRAP-brocade-ISIS-MIB-isisAdjacencyChange_unknown","2","1","0"},
        
    {"SNMPTRAP-brocade-ISIS-MIB-isisLSPErrorDetected","2","1","0"}
}
default = {"Unknown","Unknown","Unknown"}

