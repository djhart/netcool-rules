###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  OS-RFC2544-MIB
#
###############################################################################

case ".1.3.6.1.4.1.6926.2.7": ### - Notifications from OS-RFC2544-MIB (200901090000Z)

    log(DEBUG, "<<<<< Entering... mrv-OS-RFC2544-MIB.include.snmptrap.rules >>>>>")

    @Agent = "mrv-OS-RFC2544-MIB"
    @Class = "4020"
    
    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
     
        case "1": ### osRfc2544BurstComplete
        
            ##########
            # $1 = osRfc2544TestType
            ##########
            
            $osRfc2544TestType = lookup($1, TestType)
            
            $osRfc2544Owner = extract($OID1, "\.([0-9]+)\.[0-9]+$")
            $osRfc2544Test = extract($OID1, "\.[0-9]+\.([0-9]+)$")
            
            $OS_EventId = "SNMPTRAP-mrv-OS-RFC2544-MIB-osRfc2544BurstComplete"

            @AlertGroup = "Burst Test Status"
            @AlertKey = "osRfc2544CtlEntry." + $osRfc2544Owner + "." + $osRfc2544Test
            @Summary = "Burst Complete, Test Type : " + $osRfc2544TestType + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0               
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $osRfc2544TestType = $osRfc2544TestType + " ( " + $1 + " )"
            
             if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_mrv, "1")) {
                 details($osRfc2544TestType, $osRfc2544Owner, $osRfc2544Test)
             }
             @ExtendedAttr = nvp_add(@ExtendedAttr, "osRfc2544TestType", $osRfc2544TestType, "osRfc2544Owner", $osRfc2544Owner, "osRfc2544Test", $osRfc2544Test)
         
        case "2": ### osRfc2544BurstFail
        
            ##########
            # $1 = osRfc2544TestType
            ##########
            
            $osRfc2544TestType = lookup($1, TestType)
            
            $osRfc2544Owner = extract($OID1, "\.([0-9]+)\.[0-9]+$")
            $osRfc2544Test = extract($OID1, "\.[0-9]+\.([0-9]+)$")
            
            $OS_EventId = "SNMPTRAP-mrv-OS-RFC2544-MIB-osRfc2544BurstFail"

            @AlertGroup = "Burst Test Status"
            @AlertKey = "osRfc2544CtlEntry." + $osRfc2544Owner + "." + $osRfc2544Test
            @Summary = "Burst Failed, Test Type : " + $osRfc2544TestType + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $osRfc2544TestType = $osRfc2544TestType + " ( " + $1 + " )"
            
             if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_mrv, "1")) {
                 details($osRfc2544TestType, $osRfc2544Owner, $osRfc2544Test)
             }
             @ExtendedAttr = nvp_add(@ExtendedAttr, "osRfc2544TestType", $osRfc2544TestType, "osRfc2544Owner", $osRfc2544Owner, "osRfc2544Test", $osRfc2544Test)
         
        case "3": ### osRfc2544TestComplete
        
            ##########
            # $1 = osRfc2544TestType
            ##########
            
            $osRfc2544TestType = lookup($1, TestType)
            
            $osRfc2544Owner = extract($OID1, "\.([0-9]+)\.[0-9]+$")
            $osRfc2544Test = extract($OID1, "\.[0-9]+\.([0-9]+)$")
            
            $OS_EventId = "SNMPTRAP-mrv-OS-RFC2544-MIB-osRfc2544TestComplete"

            @AlertGroup = "Test Status"
            @AlertKey = "osRfc2544CtlEntry." + $osRfc2544Owner + "." + $osRfc2544Test
            @Summary = "Test Complete, Test Type : " + $osRfc2544TestType + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0               
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $osRfc2544TestType = $osRfc2544TestType + " ( " + $1 + " )"
            
             if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_mrv, "1")) {
                 details($osRfc2544TestType, $osRfc2544Owner, $osRfc2544Test)
             }
             @ExtendedAttr = nvp_add(@ExtendedAttr, "osRfc2544TestType", $osRfc2544TestType, "osRfc2544Owner", $osRfc2544Owner, "osRfc2544Test", $osRfc2544Test)
         
        case "5": ### osRfc2544FdAlarm
        
            ##########
            # $1 = osRfc2544ResultsRttAverage
            # $2 = osRfc2544TholdReason
            ##########
            
            $osRfc2544ResultsRttAverage = $1
            $osRfc2544TholdReason = lookup($2, OsRfc2544TholdReason)
            
            $osRfc2544Owner = extract($OID1, "\.([0-9]+)\.[0-9]+$")
            $osRfc2544Test = extract($OID1, "\.[0-9]+\.([0-9]+)$")
            
            $OS_EventId = "SNMPTRAP-mrv-OS-RFC2544-MIB-osRfc2544FdAlarm"

            @AlertGroup = "Frame Delay Trap"
            @AlertKey = "osRfc2544ResultsEntry." + $osRfc2544Owner + "." + $osRfc2544Test
            @Summary = "Frame Delay Alarm, Average Round-Trip-Time Received: " + $osRfc2544ResultsRttAverage + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $osRfc2544TholdReason = $osRfc2544TholdReason + " ( " + $2 + " )"
            
             if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_mrv, "1")) {
                 details($osRfc2544ResultsRttAverage, $osRfc2544TholdReason, $osRfc2544Owner, $osRfc2544Test)
             }
             @ExtendedAttr = nvp_add(@ExtendedAttr, "osRfc2544ResultsRttAverage", $osRfc2544ResultsRttAverage, "osRfc2544TholdReason", $osRfc2544TholdReason, "osRfc2544Owner", $osRfc2544Owner,
                  "osRfc2544Test", $osRfc2544Test)
         
        case "6": ### osRfc2544JittAlarm
        
            ##########
            # $1 = osRfc2544ResultsJittAverage
            # $2 = osRfc2544TholdReason
            ##########
            
            $osRfc2544ResultsJittAverage = $1
            $osRfc2544TholdReason = lookup($2, OsRfc2544TholdReason)
            
            $osRfc2544Owner = extract($OID1, "\.([0-9]+)\.[0-9]+$")
            $osRfc2544Test = extract($OID1, "\.[0-9]+\.([0-9]+)$")
            
            $OS_EventId = "SNMPTRAP-mrv-OS-RFC2544-MIB-osRfc2544JittAlarm"

            @AlertGroup = "Jitter Trap"
            @AlertKey = "osRfc2544ResultsEntry." + $osRfc2544Owner + "." + $osRfc2544Test
            @Summary = "Jitter Alarm, Average Jitter Value: " + $osRfc2544ResultsJittAverage + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $osRfc2544TholdReason = $osRfc2544TholdReason + " ( " + $2 + " )"
            
             if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_mrv, "1")) {
                 details($osRfc2544ResultsJittAverage, $osRfc2544TholdReason, $osRfc2544Owner, $osRfc2544Test)
             }
             @ExtendedAttr = nvp_add(@ExtendedAttr, "osRfc2544ResultsJittAverage", $osRfc2544ResultsJittAverage, "osRfc2544TholdReason", $osRfc2544TholdReason, "osRfc2544Owner", $osRfc2544Owner,
                  "osRfc2544Test", $osRfc2544Test)
         
        case "7": ### osRfc2544PcktLossAlarm
        
            ##########
            # $1 = osRfc2544ResultsPcktLoss
            # $2 = osRfc2544TholdReason
            ##########
            
            $osRfc2544ResultsPcktLoss = $1
            $osRfc2544TholdReason = lookup($2, OsRfc2544TholdReason)
            
            $osRfc2544Owner = extract($OID1, "\.([0-9]+)\.[0-9]+$")
            $osRfc2544Test = extract($OID1, "\.[0-9]+\.([0-9]+)$")
            
            $OS_EventId = "SNMPTRAP-mrv-OS-RFC2544-MIB-osRfc2544PcktLossAlarm"

            @AlertGroup = "Packet Loss Trap"
            @AlertKey = "osRfc2544ResultsEntry." + $osRfc2544Owner + "." + $osRfc2544Test
            @Summary = "Packet Loss Alarm, Packet Loss Value: " + $osRfc2544ResultsPcktLoss + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $osRfc2544TholdReason = $osRfc2544TholdReason + " ( " + $2 + " )"
            
             if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_mrv, "1")) {
                 details($osRfc2544ResultsPcktLoss, $osRfc2544TholdReason, $osRfc2544Owner, $osRfc2544Test)
             }
             @ExtendedAttr = nvp_add(@ExtendedAttr, "osRfc2544ResultsPcktLoss", $osRfc2544ResultsPcktLoss, "osRfc2544TholdReason", $osRfc2544TholdReason, "osRfc2544Owner", $osRfc2544Owner,
                  "osRfc2544Test", $osRfc2544Test)
         
        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_mrv, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, mrv-OS-RFC2544-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, mrv-OS-RFC2544-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/mrv/mrv-OS-RFC2544-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/mrv/mrv-OS-RFC2544-MIB.user.include.snmptrap.rules"


##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... mrv-OS-RFC2544-MIB.include.snmptrap.rules >>>>>")


