###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  SNA-NAU-MIB
#
# 1.1 - Changed the format
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table IETF-SNA-NAU-MIB_sev =
{
	    {"SNMPTRAP-IETF-SNA-NAU-MIB-snaNodeStateChangeTrap_inactive","4","1","0"},
	    {"SNMPTRAP-IETF-SNA-NAU-MIB-snaNodeStateChangeTrap_active","1","2","0"},
	    {"SNMPTRAP-IETF-SNA-NAU-MIB-snaNodeStateChangeTrap_waiting","2","1","0"},
	    {"SNMPTRAP-IETF-SNA-NAU-MIB-snaNodeStateChangeTrap_stopping","3","1","0"},
	    {"SNMPTRAP-IETF-SNA-NAU-MIB-snaNodeStateChangeTrap_unknown","2","1","0"},
	    {"SNMPTRAP-IETF-SNA-NAU-MIB-snaNodeActFailTrap_inactive","4","1","0"},
	    {"SNMPTRAP-IETF-SNA-NAU-MIB-snaNodeActFailTrap_active","1","2","0"},
	    {"SNMPTRAP-IETF-SNA-NAU-MIB-snaNodeActFailTrap_waiting","2","1","0"},
	    {"SNMPTRAP-IETF-SNA-NAU-MIB-snaNodeActFailTrap_stopping","3","1","0"},
	    {"SNMPTRAP-IETF-SNA-NAU-MIB-snaNodeActFailTrap_unknown","2","1","0"},
	    {"SNMPTRAP-IETF-SNA-NAU-MIB-snaLuStateChangeTrap_inactive","4","1","0"},
	    {"SNMPTRAP-IETF-SNA-NAU-MIB-snaLuStateChangeTrap_active","1","2","0"},
	    {"SNMPTRAP-IETF-SNA-NAU-MIB-snaLuStateChangeTrap_unknown","2","1","0"},
	    {"SNMPTRAP-IETF-SNA-NAU-MIB-snaLuSessnBindFailTrap_unbound","4","1","0"},
	    {"SNMPTRAP-IETF-SNA-NAU-MIB-snaLuSessnBindFailTrap_pendingBind","2","1","0"},
	    {"SNMPTRAP-IETF-SNA-NAU-MIB-snaLuSessnBindFailTrap_bound","1","2","0"},
	    {"SNMPTRAP-IETF-SNA-NAU-MIB-snaLuSessnBindFailTrap_pendingUnbind","2","1","0"},
	    {"SNMPTRAP-IETF-SNA-NAU-MIB-snaLuSessnBindFailTrap_unknown","2","1","0"}
}
default = {"Unknown","Unknown","Unknown"}
