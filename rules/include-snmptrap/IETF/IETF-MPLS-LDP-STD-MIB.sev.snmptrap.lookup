###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  MPLS-LDP-STD-MIB
#
# 1.1 - Changed the format
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table IETF-MPLS-LDP-STD-MIB_sev =
{
	    {"SNMPTRAP-IETF-MPLS-LDP-STD-MIB-mplsLdpInitSessionThresholdExceeded","3","1","0"},
	    {"SNMPTRAP-IETF-MPLS-LDP-STD-MIB-mplsLdpPathVectorLimitMismatch","3","1","0"},
	    {"SNMPTRAP-cisco-MPLS-LDP-MIB-mplsLdpSessionUp_nonexistent","2","1","0"},
	    {"SNMPTRAP-cisco-MPLS-LDP-MIB-mplsLdpSessionUp_Initialized","2","12","0"},
	    {"SNMPTRAP-cisco-MPLS-LDP-MIB-mplsLdpSessionUp_openrec","2","12","0"},
	    {"SNMPTRAP-cisco-MPLS-LDP-MIB-mplsLdpSessionUp_opensent","2","12","0"},
	    {"SNMPTRAP-cisco-MPLS-LDP-MIB-mplsLdpSessionUp_operational","1","2","0"},
	    {"SNMPTRAP-cisco-MPLS-LDP-MIB-mplsLdpSessionUp_unknown","2","1","0"},
	    {"SNMPTRAP-cisco-MPLS-LDP-MIB-mplsLdpSessionDown_nonexistent","2","1","0"},
	    {"SNMPTRAP-cisco-MPLS-LDP-MIB-mplsLdpSessionDown_Initialized","2","12","0"},
	    {"SNMPTRAP-cisco-MPLS-LDP-MIB-mplsLdpSessionDown_openrec","2","12","0"},
	    {"SNMPTRAP-cisco-MPLS-LDP-MIB-mplsLdpSessionDown_opensent","2","12","0"},
	    {"SNMPTRAP-cisco-MPLS-LDP-MIB-mplsLdpSessionDown_operational","1","2","0"},
	    {"SNMPTRAP-cisco-MPLS-LDP-MIB-mplsLdpSessionDown_unknown","2","1","0"}
}
default = {"Unknown","Unknown","Unknown"}
