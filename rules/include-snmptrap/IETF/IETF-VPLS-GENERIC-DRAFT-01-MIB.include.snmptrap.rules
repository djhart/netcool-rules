###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  VPLS-GENERIC-DRAFT-01-MIB
#
###############################################################################

case ".1.3.6.1.4.1.1991.3.4.1": ### - Notifications from VPLS-GENERIC-DRAFT-01-MIB (200608301200Z)

log(DEBUG, "<<<<< Entering... IETF-VPLS-GENERIC-DRAFT-01-MIB.include.snmptrap.rules >>>>>")

    @Agent = "IETF-VPLS-GENERIC-DRAFT-01-MIB"
    @Class = "40086"
    
    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
     
        case "1": ### vplsStatusChanged
        
            ##########
            # $1 = vplsConfigVpnId
            # $2 = vplsConfigAdminStatus
            # $3 = vplsStatusOperStatus
            ##########
            
            $vplsConfigVpnId = $1
            $vplsConfigAdminStatus = lookup($2, VplsConfigAdminStatus)
            $vplsStatusOperStatus = lookup($3, VplsStatusOperStatus)
            
            $vplsConfigIndex = extract($OID1, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-IETF-VPLS-GENERIC-DRAFT-01-MIB-vplsStatusChanged"

            @AlertGroup = "VPLS Status"
            @AlertKey = "vplsConfigEntry." + $vplsConfigIndex + ", vplsStatusEntry." + $vplsConfigIndex
            switch($2)
                {
                case "1": ### up
                    switch($3)
                    {
                        case "0": ### other
                            $SEV_KEY = $OS_EventId + "_up_other"
                            @Summary = "VPLS Tunnel Up, Operationally Other"

                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 13
                            $DEFAULT_ExpireTime = 1800                    
                        case "1": ### up
                            $SEV_KEY = $OS_EventId + "_up_up"
                            @Summary = "VPLS Tunnel Up, Operationally Up"

                            $DEFAULT_Severity = 1
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0
                        case "2": ### down
                            $SEV_KEY = $OS_EventId + "_up_down"
                            @Summary = "VPLS Tunnel Up, Operationally Down"

                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0
                        default:
                            $SEV_KEY = $OS_EventId + "_up_unknown"
                            @Summary = "VPLS Tunnel Up, Operationally Unknown"

                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0
                    }
                case "2": ### down
                    switch($3)
                     {
                        case "0": ### other
                            $SEV_KEY = $OS_EventId + "_down_other"
                            @Summary = "VPLS Tunnel Down, Operationally Other"

                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 13
                            $DEFAULT_ExpireTime = 1800                  
                        case "1": ### up
                            $SEV_KEY = $OS_EventId + "_down_up"
                            @Summary = "VPLS Tunnel Down, Operationally Up"

                            $DEFAULT_Severity = 1
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0
                        case "2": ### down
                            $SEV_KEY = $OS_EventId + "_down_down"
                            @Summary = "VPLS Tunnel Down, Operationally Down"

                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0
                        default:
                            $SEV_KEY = $OS_EventId + "_down_unknown"
                            @Summary = "VPLS Tunnel Down, Operationally Unknown"

                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0
                     }
                case "3": ### testing
                    switch($3)
                     {
                        case "0": ### other
                            $SEV_KEY = $OS_EventId + "_testing_other"
                            @Summary = "VPLS Tunnel Testing, Operationally Other"

                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 13
                            $DEFAULT_ExpireTime = 1800                    
                        case "1": ### up
                            $SEV_KEY = $OS_EventId + "_testing_up"
                            @Summary = "VPLS Tunnel Testing, Operationally Up"

                            $DEFAULT_Severity = 1
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0
                        case "2": ### down
                            $SEV_KEY = $OS_EventId + "_testing_down"
                            @Summary = "VPLS Tunnel Testing, Operationally Down"

                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0
                        default:
                            $SEV_KEY = $OS_EventId + "_testing_unknown"
                            @Summary = "VPLS Tunnel Testing, Operationally Unknown"

                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0
                     }
                default:
                    switch($3)
                    {
                        case "0": ### other
                            $SEV_KEY = $OS_EventId + "_unknown_other"
                            @Summary = "VPLS Tunnel Unknown, Operationally Other"

                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 13
                            $DEFAULT_ExpireTime = 1800                    
                        case "1": ### up
                            $SEV_KEY = $OS_EventId + "_unknown_up"
                            @Summary = "VPLS Tunnel Unknown, Operationally Up"

                            $DEFAULT_Severity = 1
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0
                        case "2": ### down
                            $SEV_KEY = $OS_EventId + "_unknown_down"
                            @Summary = "VPLS Tunnel Unknown, Operationally Down"

                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0
                        default:
                            $SEV_KEY = $OS_EventId + "_unknown_unknown"
                            @Summary = "VPLS Tunnel Status Unknown, Operationally Unknown"

                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0
                    }
                }

            
            @Summary = @Summary + " ( " + @AlertKey + " ) "

            update(@Severity)
			
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $vplsConfigAdminStatus = $vplsConfigAdminStatus + " ( " + $2 + " )"
            $vplsStatusOperStatus = $vplsStatusOperStatus + " ( " + $3 + " )"
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($vplsConfigVpnId, $vplsConfigAdminStatus, $vplsStatusOperStatus, $vplsConfigIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "vplsConfigVpnId", $vplsConfigVpnId, "vplsConfigAdminStatus", $vplsConfigAdminStatus, "vplsStatusOperStatus", $vplsStatusOperStatus,
                 "vplsConfigIndex", $vplsConfigIndex)
         
        case "2": ### vplsFwdFullAlarmRaised
        
            ##########
            # $1 = vplsConfigVpnId
            # $2 = vplsConfigFwdFullHighWatermark
            # $3 = vplsConfigFwdFullLowWatermark
            ##########
            
            $vplsConfigVpnId = $1
            $vplsConfigFwdFullHighWatermark = $2
            $vplsConfigFwdFullLowWatermark = $3
            
            $vplsConfigIndex = extract($OID1, "\.([0-9]+)$")

            
            $OS_EventId = "SNMPTRAP-IETF-VPLS-GENERIC-DRAFT-01-MIB-vplsFwdFullAlarmRaised"

            @AlertGroup = "VPLS Forward Full Alarm Status"
            @AlertKey = "vplsConfigEntry." + $vplsConfigIndex
            @Summary = "Utilization of the Forwarding Database is Above the Value Specified by vplsConfigFwdFullHighWatermark: " + $vplsConfigFwdFullHighWatermark + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($vplsConfigVpnId, $vplsConfigFwdFullHighWatermark, $vplsConfigFwdFullLowWatermark, $vplsConfigIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "vplsConfigVpnId", $vplsConfigVpnId, "vplsConfigFwdFullHighWatermark", $vplsConfigFwdFullHighWatermark, "vplsConfigFwdFullLowWatermark", $vplsConfigFwdFullLowWatermark,
                 "vplsConfigIndex", $vplsConfigIndex)
         
        case "3": ### vplsFwdFullAlarmCleared
        
            ##########
            # $1 = vplsConfigVpnId
            # $2 = vplsConfigFwdFullHighWatermark
            # $3 = vplsConfigFwdFullLowWatermark
            ##########
            
            $vplsConfigVpnId = $1
            $vplsConfigFwdFullHighWatermark = $2
            $vplsConfigFwdFullLowWatermark = $3
            
            $vplsConfigIndex = extract($OID1, "\.([0-9]+)$")
            
            $OS_EventId = "SNMPTRAP-IETF-VPLS-GENERIC-DRAFT-01-MIB-vplsFwdFullAlarmCleared"

            @AlertGroup = "VPLS Forward Full Alarm Status"
            @AlertKey = "vplsConfigEntry." + $vplsConfigIndex
            @Summary = "Utilization of the Forwarding Database is Below the Value Specified by vplsConfigFwdFullLowWatermark: " + $vplsConfigFwdFullLowWatermark + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($vplsConfigVpnId, $vplsConfigFwdFullHighWatermark, $vplsConfigFwdFullLowWatermark, $vplsConfigIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "vplsConfigVpnId", $vplsConfigVpnId, "vplsConfigFwdFullHighWatermark", $vplsConfigFwdFullHighWatermark, "vplsConfigFwdFullLowWatermark", $vplsConfigFwdFullLowWatermark,
                 "vplsConfigIndex", $vplsConfigIndex)
         
        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, IETF-VPLS-GENERIC-DRAFT-01-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, IETF-VPLS-GENERIC-DRAFT-01-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/IETF/IETF-VPLS-GENERIC-DRAFT-01-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/IETF/IETF-VPLS-GENERIC-DRAFT-01-MIB.user.include.snmptrap.rules"


##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... IETF-VPLS-GENERIC-DRAFT-01-MIB.include.snmptrap.rules >>>>>")


