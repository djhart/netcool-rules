###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  ENTITY-STATE-MIB
#
###############################################################################

case ".1.3.6.1.2.1.131": ### IETF Entity State - Notifications from ENTITY-STATE-MIB(20040215) 

    log(DEBUG, "<<<<< Entering... IETF-ENTITY-STATE-MIB.include.snmptrap.rules >>>>>")

    @Agent = "IETF-Entity State"
    @Class = "40086"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### entStateOperEnabled

            ##########
            # $1 = entStateAdmin 
            # $2 = entStateAlarm 
            ##########

            $entStateAdmin = lookup($1, AdminState) + " ( " + $1 + " )"
            $entStateAlarm = lookup($2, AlarmStatus) + " ( " + $2 + " )"

            $entPhysicalIndex = extract($OID1, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-IETF-ENTITY-STATE-MIB-entStateOperEnabled"

            @AlertGroup = "Entity Operational Status"
            @AlertKey = "entStateEntry." + $entPhysicalIndex
            switch($1)
            {
                case "1": ### locked
                    $SEV_KEY = $OS_EventId + "_locked"
                    @Summary = "Resource Administratively Prohibited from Use"

                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "2": ### shuttingDown
                    $SEV_KEY = $OS_EventId + "_shuttingDown"
                    @Summary = "Resource Usage Administratively Limited to Current Instances of Use"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "3": ### unlocked
                    $SEV_KEY = $OS_EventId + "_unlocked"
                    @Summary = "Resource not Administratively Prohibited from Use"

                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = "Resource Status Unknown"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $1

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($entStateAdmin,$entStateAlarm,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "entStateAdmin", $entStateAdmin, "entStateAlarm", $entStateAlarm, "entPhysicalIndex", $entPhysicalIndex)

        case "2": ### entStateOperDisabled

            ##########
            # $1 = entStateAdmin 
            # $2 = entStateAlarm 
            ##########

            $entStateAdmin = lookup($1, AdminState) + " ( " + $1 + " )"
            $entStateAlarm = lookup($2, AlarmStatus) + " ( " + $2 + " )"

            $entPhysicalIndex = extract($OID1, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-IETF-ENTITY-STATE-MIB-entStateOperDisabled"

            @AlertGroup = "Entity Operational Status"
            @AlertKey = "entStateEntry." + $entPhysicalIndex
            switch($1)
            {
                case "1": ### locked
                    $SEV_KEY = $OS_EventId + "_locked"
                    @Summary = "Resource Administratively Prohibited from Use"

                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "2": ### shuttingDown
                    $SEV_KEY = $OS_EventId + "_shuttingDown"
                    @Summary = "Resource Usage Administratively Limited to Current Instances of Use"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "3": ### unlocked
                    $SEV_KEY = $OS_EventId + "_unlocked"
                    @Summary = "Resource not Administratively Prohibited from Use"

                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = "Resource Status Unknown"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $1

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($entStateAdmin,$entStateAlarm,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "entStateAdmin", $entStateAdmin, "entStateAlarm", $entStateAlarm, "entPhysicalIndex", $entPhysicalIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, IETF-ENTITY-STATE-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, IETF-ENTITY-STATE-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/IETF/IETF-ENTITY-STATE-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/IETF/IETF-ENTITY-STATE-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... IETF-ENTITY-STATE-MIB.include.snmptrap.rules >>>>>")
