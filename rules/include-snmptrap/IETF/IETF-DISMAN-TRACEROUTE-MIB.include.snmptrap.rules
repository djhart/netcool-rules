###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  IETF-DISMAN-TRACEROUTE-MIB
#
###############################################################################

case ".1.3.6.1.2.1.81": ### Distributed Management, Traceroute - Notifications from DISMAN-TRACEROUTE-MIB

    log(DEBUG, "<<<<< Entering... IETF-DISMAN-TRACEROUTE-MIB.include.snmptrap.rules >>>>>")

    @Agent = "IETF-DISMAN-TRACEROUTE-MIB"
    @Class = "40086"

    $OPTION_TypeFieldUsage = "3.6"

    ##########
    # $1 = traceRouteCtlTargetAddressType
    # $2 = traceRouteCtlTargetAddress
    # $3 = traceRouteResultsIpTgtAddrType
    # $4 = traceRouteResultsIpTgtAddr
    ##########
    
    switch($specific-trap)
    {
        case "1": ### traceRoutePathChange

            $traceRouteCtlTargetAddressType = lookup($1, InetAddressType) + " ( " + $1 + " )"
            $traceRouteCtlTargetAddress = $2
            $traceRouteResultsIpTgtAddrType = lookup($3, InetAddressType) + " ( " + $3 + " )"
            $traceRouteResultsIpTgtAddr = $4
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($traceRouteCtlTargetAddressType,$traceRouteCtlTargetAddress,$traceRouteResultsIpTgtAddrType,$traceRouteResultsIpTgtAddr)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "traceRouteCtlTargetAddressType", $traceRouteCtlTargetAddressType, "traceRouteCtlTargetAddress", $traceRouteCtlTargetAddress, "traceRouteResultsIpTgtAddrType", $traceRouteResultsIpTgtAddrType,
                 "traceRouteResultsIpTgtAddr", $traceRouteResultsIpTgtAddr)

            $OS_EventId = "SNMPTRAP-IETF-DISMAN-TRACEROUTE-MIB-traceRoutePathChange"
            
            @Node = $2
            if(match($4, ""))
            {
                @NodeAlias = $2
            }
            else
            {
                @NodeAlias = $4
            }
            
            @AlertGroup = "Traceroute Path Status"
            @AlertKey = "traceRouteCtlEntry." + extract($OID1, "3\.6\.1\.2\.1\.81\.1\.2\.1\.3\.(.*)$") + " ( on " + $Node + " )"
            @Summary = "Traceroute Path Changed  ( from " + $Node + " )"
            
            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        case "2": ### traceRouteTestFailed

            $traceRouteCtlTargetAddressType = lookup($1, InetAddressType) + " ( " + $1 + " )"
            $traceRouteCtlTargetAddress = $2
            $traceRouteResultsIpTgtAddrType = lookup($3, InetAddressType) + " ( " + $3 + " )"
            $traceRouteResultsIpTgtAddr = $4
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($traceRouteCtlTargetAddressType,$traceRouteCtlTargetAddress,$traceRouteResultsIpTgtAddrType,$traceRouteResultsIpTgtAddr)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "traceRouteCtlTargetAddressType", $traceRouteCtlTargetAddressType, "traceRouteCtlTargetAddress", $traceRouteCtlTargetAddress, "traceRouteResultsIpTgtAddrType", $traceRouteResultsIpTgtAddrType,
                 "traceRouteResultsIpTgtAddr", $traceRouteResultsIpTgtAddr)

            $OS_EventId = "SNMPTRAP-IETF-DISMAN-TRACEROUTE-MIB-traceRouteTestFailed"
            
            @Node = $2
            if(match($4, ""))
            {
                @NodeAlias = $2
            }
            else
            {
                @NodeAlias = $4
            }
            
            @AlertGroup = "Traceroute Path Status"
            @AlertKey = "traceRouteCtlEntry." + extract($OID1, "\.[0-9]+\.(.*)$") + " ( on " + $Node + " )"
            @Summary = "Traceroute Path Not Determined  ( from " + $Node + " )"
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        case "3": ### traceRouteTestCompleted

            $traceRouteCtlTargetAddressType = lookup($1, InetAddressType) + " ( " + $1 + " )"
            $traceRouteCtlTargetAddress = $2
            $traceRouteResultsIpTgtAddrType = lookup($3, InetAddressType) + " ( " + $3 + " )"
            $traceRouteResultsIpTgtAddr = $4
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($traceRouteCtlTargetAddressType,$traceRouteCtlTargetAddress,$traceRouteResultsIpTgtAddrType,$traceRouteResultsIpTgtAddr)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "traceRouteCtlTargetAddressType", $traceRouteCtlTargetAddressType, "traceRouteCtlTargetAddress", $traceRouteCtlTargetAddress, "traceRouteResultsIpTgtAddrType", $traceRouteResultsIpTgtAddrType,
                 "traceRouteResultsIpTgtAddr", $traceRouteResultsIpTgtAddr)

            $OS_EventId = "SNMPTRAP-IETF-DISMAN-TRACEROUTE-MIB-traceRouteTestCompleted"
            
            @Node = $2
            if(match($4, ""))
            {
                @NodeAlias = $2
            }
            else
            {
                @NodeAlias = $4
            }
            
            @AlertGroup = "Traceroute Path Status"
            @AlertKey = "traceRouteCtlEntry." + extract($OID1, "\.[0-9]+\.(.*)$") + " ( on " + $Node + " )"
            @Summary = "Traceroute Path Determined  ( from " + $Node + " )"
            
            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, IETF-DISMAN-TRACEROUTE-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, IETF-DISMAN-TRACEROUTE-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/IETF/IETF-DISMAN-TRACEROUTE-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/IETF/IETF-DISMAN-TRACEROUTE-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... IETF-DISMAN-TRACEROUTE-MIB.include.snmptrap.rules >>>>>")
