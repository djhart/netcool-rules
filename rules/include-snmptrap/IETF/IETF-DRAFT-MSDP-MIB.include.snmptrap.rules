###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  DRAFT-MSDP-MIB
#
###############################################################################

case ".1.3.6.1.3.92.1.1.7": ### Multicast Source Discovery Protocol (MSDP) Management - Notifications from DRAFT-MSDP-MIB (991216)

    log(DEBUG, "<<<<< Entering... IETF-DRAFT-MSDP-MIB.include.snmptrap.rules >>>>>")

    @Agent = "IETF-DRAFT-MSDP-MIB"
    @Class = "40086"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### msdpEstablished

            ##########
            # $1 = msdpPeerFsmEstablishedTransitions
            ##########

            $msdpPeerFsmEstablishedTransitions = $1
            $msdpPeerRemoteAddress = extract($OID1, "\.([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)$")

            $OS_EventId = "SNMPTRAP-IETF-DRAFT-MSDP-MIB-msdpEstablished"

            @AlertGroup = "MSDP Peer Status"
            @AlertKey = "msdpPeerEntry." + $msdpPeerRemoteAddress
            @Summary = "MSDP Peer Established  ( MSDP Peer: " + $msdpPeerRemoteAddress + " )"

            $DEFAULT_Severity = 1 
            $DEFAULT_Type = 2 
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($msdpPeerFsmEstablishedTransitions,$msdpPeerRemoteAddress)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "msdpPeerFsmEstablishedTransitions", $msdpPeerFsmEstablishedTransitions, "msdpPeerRemoteAddress", $msdpPeerRemoteAddress)

        case "2": ### msdpBackwardTransition

            ##########
            # $1 = msdpPeerState
            ##########

            $msdpPeerState = lookup($1, msdpPeerState) + " ( " + $1 + " )"
            $msdpPeerRemoteAddress = extract($OID1, "\.([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)$")

            $OS_EventId = "SNMPTRAP-IETF-DRAFT-MSDP-MIB-msdpBackwardTransition"

            @AlertGroup = "MSDP Peer Status"
            @AlertKey = "msdpPeerEntry." + $msdpPeerRemoteAddress
            switch($1)
            {
                case "1": ### inactive 
                    $SEV_KEY = $OS_EventId + "_inactive"
                    @Summary = "MSDP TCP Peer Connection Inactive"

                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "2": ### listen 
                    $SEV_KEY = $OS_EventId + "_listen"
                    @Summary = "MSDP TCP Peer Connection Listening" 

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 12
                    $DEFAULT_ExpireTime = 0

                case "3": ### connecting 
                    $SEV_KEY = $OS_EventId + "_connecting"
                    @Summary = "MSDP TCP Peer Connection Connecting"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 12
                    $DEFAULT_ExpireTime = 0

                case "4": ### established 
                    $SEV_KEY = $OS_EventId + "_established"
                    @Summary = "MSDP TCP Peer Connection Established"

                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0

                case "5": ### disabled 
                    $SEV_KEY = $OS_EventId + "_disabled"
                    @Summary = "MSDP TCP Peer Connection Disabled"

                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                default:  
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = "MSDP TCP Peer Connection Status Unknown"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }
            @Summary = @Summary + "  ( MSDP Peer: " + $msdpPeerRemoteAddress + " )"
          
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $1

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($msdpPeerState,$msdpPeerRemoteAddress)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "msdpPeerState", $msdpPeerState, "msdpPeerRemoteAddress", $msdpPeerRemoteAddress)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, IETF-DRAFT-MSDP-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, IETF-DRAFT-MSDP-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/IETF/IETF-DRAFT-MSDP-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/IETF/IETF-DRAFT-MSDP-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... IETF-DRAFT-MSDP-MIB.include.snmptrap.rules >>>>>")
