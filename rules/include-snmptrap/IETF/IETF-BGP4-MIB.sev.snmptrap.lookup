###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  BGP4-MIB
#
# 1.1 - Changed the format
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table IETF-BGP4-MIB_sev =
{
	    {"SNMPTRAP-IETF-BGP4-MIB-bgpEstablished_idle","4","1","0"},
	    {"SNMPTRAP-IETF-BGP4-MIB-bgpEstablished_connect","2","12","0"},
	    {"SNMPTRAP-IETF-BGP4-MIB-bgpEstablished_active","2","12","0"},
	    {"SNMPTRAP-IETF-BGP4-MIB-bgpEstablished_opensent","2","12","0"},
	    {"SNMPTRAP-IETF-BGP4-MIB-bgpEstablished_openconfirm","2","12","0"},
	    {"SNMPTRAP-IETF-BGP4-MIB-bgpEstablished_established","1","2","0"},
	    {"SNMPTRAP-IETF-BGP4-MIB-bgpEstablished_unknown","2","1","0"},
	    {"SNMPTRAP-IETF-BGP4-MIB-bgpBackwardTransition_idle","4","1","0"},
	    {"SNMPTRAP-IETF-BGP4-MIB-bgpBackwardTransition_connect","2","12","0"},
	    {"SNMPTRAP-IETF-BGP4-MIB-bgpBackwardTransition_active","2","12","0"},
	    {"SNMPTRAP-IETF-BGP4-MIB-bgpBackwardTransition_opensent","2","12","0"},
	    {"SNMPTRAP-IETF-BGP4-MIB-bgpBackwardTransition_openconfirm","2","12","0"},
	    {"SNMPTRAP-IETF-BGP4-MIB-bgpBackwardTransition_established","1","2","0"},
	    {"SNMPTRAP-IETF-BGP4-MIB-bgpBackwardTransition_unknown","2","1","0"}
}
default = {"Unknown","Unknown","Unknown"}
