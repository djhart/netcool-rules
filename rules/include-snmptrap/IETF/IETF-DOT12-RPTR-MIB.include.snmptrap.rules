###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  IETF-DOT12-RPTR-MIB
#
###############################################################################

case ".1.3.6.1.2.1.53.2": ### IEEE 802.12 Repeaters - Notifcations from DOT12-RPTR-MIB (RFC2266)

    log(DEBUG, "<<<<< Entering... IETF-DOT12-RPTR-MIB.include.snmptrap.rules >>>>>")

    @Agent = "IETF-DOT12-RPTR-MIB"
    @Class = "40086"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### vgRptrHealth

            ##########
            # $1 = vgRptrInfoOperStatus
            ##########

            $vgRptrInfoOperStatus = $1
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($vgRptrInfoOperStatus)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "vgRptrInfoOperStatus", $vgRptrInfoOperStatus)

            $OS_EventId = "SNMPTRAP-IETF-DOT12-RPTR-MIB-vgRptrHealth"

            @AlertGroup = "802.12 Repeater Status"
            @AlertKey = "vgRptrInfoEntry." + extract($OID1, "\.([0-9]+)$")
            switch($1)
            {
                case "1": ### other
                    @Summary = "802.12 Repeater Status Unknown"

                    $SEV_KEY = $OS_EventId + "_other"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "2": ### ok
                    @Summary = "802.12 Repeater OK"

                    $SEV_KEY = $OS_EventId + "_ok"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0

                case "3": ### generalFailure
                    @Summary = "802.12 Repeater General Failure"

                    $SEV_KEY = $OS_EventId + "_generalFailure"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    @Summary = "802.12 Repeater Status Unknown"

                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

            }
            @Summary = @Summary + "  ( " + @AlertKey + " )"
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $1

        case "2": ### vgRptrResetEvent

            ##########
            # $1 = vgRptrInfoOperStatus
            ##########

            $vgRptrInfoOperStatus = $1
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($vgRptrInfoOperStatus)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "vgRptrInfoOperStatus", $vgRptrInfoOperStatus)

            $OS_EventId = "SNMPTRAP-IETF-DOT12-RPTR-MIB-vgRptrResetEvent"

            @AlertGroup = "802.12 Repeater Status"
            @AlertKey = "vgRptrInfoEntry." + extract($OID1, "\.([0-9]+)$")
            switch($1)
            {
                case "1": ### other
                    @Summary = "802.12 Repeater Reset, Status Unknown"

                    $SEV_KEY = $OS_EventId + "_other"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "2": ### ok
                    @Summary = "802.12 Repeater Reset, OK"

                    $SEV_KEY = $OS_EventId + "_ok"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0

                case "3": ### generalFailure
                    @Summary = "802.12 Repeater Reset, General Failure"

                    $SEV_KEY = $OS_EventId + "_generalFailure"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    @Summary = "802.12 Repeater Status Unknown"

                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

            }
            @Summary = @Summary + "  ( " + @AlertKey + " )"
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $1

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, IETF-DOT12-RPTR-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, IETF-DOT12-RPTR-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/IETF/IETF-DOT12-RPTR-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/IETF/IETF-DOT12-RPTR-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... IETF-DOT12-RPTR-MIB.include.snmptrap.rules >>>>>")
