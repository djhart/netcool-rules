###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  PTOPO-MIB
#
###############################################################################

case ".1.3.6.1.2.1.79.2": ### Physical Topology Information - Notifications from PTOPO-MIB (20000921)

    log(DEBUG, "<<<<< Entering... IETF-PTOPO-MIB.include.snmptrap.rules >>>>>")

    @Agent = "IETF-PTOPO-MIB"
    @Class = "40086"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### ptopoConfigChange

            ##########
            # $1 = ptopoConnTabInserts 
            # $2 = ptopoConnTabDeletes 
            # $3 = ptopoConnTabDrops 
            # $4 = ptopoConnTabAgeouts 
            ##########

            $ptopoConnTabInserts = $1
            $ptopoConnTabDeletes = $2
            $ptopoConnTabDrops = $3
            $ptopoConnTabAgeouts = $4

            $OS_EventId = "SNMPTRAP-IETF-PTOPO-MIB-ptopoConfigChange"

            @AlertGroup = "Physical Topology Configuration Status"
            @AlertKey = ""
            @Summary = "Physical Topology Configuration Changed"

            $DEFAULT_Severity = 2 
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($ptopoConnTabInserts,$ptopoConnTabDeletes,$ptopoConnTabDrops,$ptopoConnTabAgeouts)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "ptopoConnTabInserts", $ptopoConnTabInserts, "ptopoConnTabDeletes", $ptopoConnTabDeletes, "ptopoConnTabDrops", $ptopoConnTabDrops,
                 "ptopoConnTabAgeouts", $ptopoConnTabAgeouts)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ietf, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, IETF-PTOPO-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, IETF-PTOPO-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/IETF/IETF-PTOPO-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/IETF/IETF-PTOPO-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... IETF-PTOPO-MIB.include.snmptrap.rules >>>>>")
