###############################################################################
# extrahop-EXTRAHOP-MIB
# Aug 4, 2016 7:32:07 PM
###############################################################################

case ".1.3.6.1.4.1.32015.2":  ### extrahopTraps - Traps from EXTRAHOP-MIB (201505080000Z)

$OPTION_EnableDetails = "1"

	log(DEBUG, "<<<<< Entering... extrahop-EXTRAHOP-MIB.include.snmptrap.rules >>>>>")

	@Agent = "EXTRAHOP-MIB"

	switch($specific-trap) {
		case "1": ### - extrahopAlertTrap
			#######################
			#
			# Alert notification
			#
			# $1 = extrahopAlertName           # Name of the alert
			# $2 = extrahopAlertComment        # Alert comment
			# $3 = extrahopAlertObjectType     # Type of object alert applies to
			# $4 = extrahopAlertObjectName     # Name of object alert applies to
			# $5 = extrahopAlertExpr           # Alert expression
			# $6 = extrahopAlertValue          # Value that triggered alert
			# $7 = extrahopAlertTime           # Time of the alert trigger
			# $8 = extrahopAlertObjectId       # Decimal representation of numeric object id
			# $9 = extrahopAlertObjectStrId    # String object id
			# $10 = extrahopAlertObjectMACAddr # Object MAC Address
			# $11 = extrahopAlertObjectIPAddr  # Object IP Address
			# $12 = extrahopAlertObjectTags    # Object tags
			# $13 = extrahopAlertObjectURL     # URL
			# $14 = extrahopAlertStatName      # Stat name
			# $15 = extrahopAlertStatFieldName # Stat field name
			# $16 = extrahopAlertSeverity      # Severity
			#######################

			$extrahopAlertName = $1
			$extrahopAlertComment = $2
			$extrahopAlertObjectType = $3
			$extrahopAlertObjectName = $4
			$extrahopAlertExpr = $5
			$extrahopAlertValue = $6
			$extrahopAlertTime = $7
			$extrahopAlertObjectId = $8
			$extrahopAlertObjectStrId = $9
			$extrahopAlertObjectMACAddr = $10
			$extrahopAlertObjectIPAddr = $11
			$extrahopAlertObjectTags = $12
			$extrahopAlertObjectURL = $13
			$extrahopAlertStatName = $14
			$extrahopAlertStatFieldName = $15
			$extrahopAlertSeverity = lookup($16,extrahop-EXTRAHOP-MIB-extrahopAlertSeverity)
			$OS_EventId = "SNMPTRAP-EXTRAHOP-MIB-extrahopAlertTrap"

			# Rename @Service to "Alert Expression" in view
			$alertExpression = regreplace( $extrahopAlertExpr, "extrahop\.[a-z]+\.", "" )
			@Service = $alertExpression # Rename this to "Alert Expression" in view

			@Node = $extrahopAlertObjectIPAddr
      @NodeAlias = $extrahopAlertObjectIPAddr
      @LocalNodeAlias = $extrahopAlertObjectIPAddr

			@AlertGroup = "extrahopAlertTrap"
			@AlertKey = $extrahopAlertName+$extrahopAlertObjectType+$extrahopAlertObjectName+$extrahopAlertObjectMACAddr+$extrahopAlertObjectIPAddr
			@Summary = $extrahopAlertName + ": " + $alertExpression + " :value: " + $extrahopAlertValue
			@PhysicalCard = $extrahopAlertObjectMACAddr
			@URL = $extrahopAlertObjectURL
			@Customer = $extrahopAlertExpr
			@Location = $extrahopAlertObjectType # Rename this to "Object Type" in view.  It will be: capture/application/device

			@Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager

      # ExtraHop Sevs
      # Emergency :0
      # Alert     :1
      # Critical  :2
      # Error     :3
      # Warning   :4
      # Notice    :5
      # Info      :6
      # Debug     :7
      #
      # I have both @Severity and $DEFAULT_Severity below for now but just using
      # @Severity until rules needs to be more detailed and then we'll switch to lookups
      #
			$netcoolSev = extract($extrahopAlertSeverity, "[a-z ]+\(([0-9])\)+")

      switch($netcoolSev)
      {
        case "0" | "1" | "2": ### Critical

          @Severity = 5
          $DEFAULT_Severity = 5
          $DEFAULT_Type = 1
          $DEFAULT_ExpireTime = 0

        case "3": ### Major

          @Severity = 4
          $DEFAULT_Severity = 3
          $DEFAULT_Type = 1
          $DEFAULT_ExpireTime = 0

        case "4": ### Minor

          @Severity = 3
          $DEFAULT_Severity = 3
          $DEFAULT_Type = 1
          $DEFAULT_ExpireTime = 0

        case "5" | "6": ### Warning

          @Severity = 2
          $DEFAULT_Severity = 2
          $DEFAULT_Type = 1
          $DEFAULT_ExpireTime = 0

        default: ### Indeterminate

          @Severity = 1
          $DEFAULT_Severity = 1
          $DEFAULT_Type = 1
          $DEFAULT_ExpireTime = 0
      } 


		case "2": ### - extrahopStorageAlertTrap
			#########################################################################
			#
			# Storage notification
			#
			# $1 = extrahopStorageAlertRole     # Role of the storage device
			# $2 = extrahopStorageAlertDevice   # Storage device issuing the alert
			# $3 = extrahopStorageAlertStatus   # Status of the device
			# $4 = extrahopStorageAlertDetails  # Details about the notification
			# $5 = extrahopStorageAlertSeverity # Severity
			# $6 = extrahopStorageAlertMachine  # Machine sending alert
      #
			#########################################################################

			$extrahopStorageAlertRole = $1
			$extrahopStorageAlertDevice = $2
			$extrahopStorageAlertStatus = $3
			$extrahopStorageAlertDetails = $4
			$extrahopStorageAlertSeverity = lookup($5,extrahop-EXTRAHOP-MIB-extrahopStorageAlertSeverity)
			$extrahopStorageAlertMachine = $6
			$OS_EventId = "SNMPTRAP-EXTRAHOP-MIB-extrahopStorageAlertTrap"

			@AlertGroup = "extrahopStorageAlertTrap"
			@AlertKey = $extrahopStorageAlertRole+$extrahopStorageAlertDevice+$extrahopStorageAlertStatus+$extrahopStorageAlertDetails+$extrahopStorageAlertSeverity+$extrahopStorageAlertMachine
			@Summary = "extrahopStorageAlertTrap: Storage notification"

			$DEFAULT_Severity = 3
			$DEFAULT_Type = 1
			$DEFAULT_ExpireTime = 0

			@Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

			details($extrahopStorageAlertRole,$extrahopStorageAlertDevice,$extrahopStorageAlertStatus,$extrahopStorageAlertDetails,$extrahopStorageAlertSeverity,$extrahopStorageAlertMachine)

		default:
			@Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
			@Severity = 1
			@Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
			details($*)
	}

##########
# Handle Severity via Lookup.
##########

# This is not used at the moment since there are only 2 types of events

#if(exists($SEV_KEY))
#{
#    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, extrahop-EXTRAHOP-MIB_sev)
#}
#else
#{
#    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, extrahop-EXTRAHOP-MIB_sev)
#}
#include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

### DJH enabled all details to create rules file.  Turn these off later
details($*)

##########
# Enter "Advanced" and "User" includes
##########

include "$NC_RULES_HOME/include-snmptrap/extrahop/extrahop-EXTRAHOP-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/extrahop/extrahop-EXTRAHOP-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

# Putting this here to overide some of the 'intelligent' severity rules that do not work
# unless using SevKey lookups
$OS_Severity = @Severity
@Type = 1

log(DEBUG, "<<<<< Leaving... extrahop-EXTRAHOP-MIB.include.snmptrap.rules >>>>>")
