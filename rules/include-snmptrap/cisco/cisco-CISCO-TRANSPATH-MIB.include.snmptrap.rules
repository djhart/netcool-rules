###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 1.0 - Initial Release.
#
# Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
# - CISCO-TRANSPATH-MIB
#
###############################################################################

case ".1.3.6.1.4.1.2496.1.1.4": ###  - Notifications from CISCO-TRANSPATH-MIB (9808180000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-TRANSPATH-MIB.include.snmptrap.rules >>>>>")

    @Agent = "CISCO-TRANSPATH-MIB"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### commAlarm

            ##########
            # $1 = tpAlarmId 
            # $2 = tpAlarmCatId 
            # $3 = tpAlarmCatName 
            # $4 = tpAlarmCatDesc 
            # $5 = tpAlarmSet 
            # $6 = tpAlarmNotify 
            # $7 = tpAlarmSeverity 
            # $8 = tpAlarmReported 
            # $9 = tpComponentId 
            # $10 = tpComponentType 
            # $11 = tpCompMMLName 
            # $12 = tpCompDesc 
            # $13 = tpCompParentId 
            # $14 = tpAlarmTime 
            ##########

            $tpAlarmId = $1
            $tpAlarmCatId = $2
            $tpAlarmCatName = $3
            $tpAlarmCatDesc = $4
            $tpAlarmSet = lookup($5, tpAlarmSet) + " ( " + $5 + " )"
            $tpAlarmNotify = lookup($6, tpAlarmNotify) + " ( " + $6 + " )"
            $tpAlarmSeverity = lookup($7, tpAlarmSeverity) + " ( " + $7 + " )"
            $tpAlarmReported = lookup($8, tpAlarmReported) + " ( " + $8 + " )"
            $tpComponentId = $9
            $tpComponentType = $10
            $tpCompMMLName = $11
            $tpCompDesc = $12
            $tpCompParentId = $13
            $tpAlarmTime = $14

            $OS_EventId = "SNMPTRAP-cisco-CISCO-TRANSPATH-MIB-commAlarm"

            @AlertGroup = $tpAlarmCatId + "-" + $tpAlarmCatName
            @AlertKey = "tpCompTableEntry." + $tpComponentId
            @Summary = "Communication Error ( " + @AlertKey + " )"

            switch($7)
            {
                case "1": ### informational
                    $SEV_KEY = $OS_EventId + "_informational"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "2": ### minor
                    $SEV_KEY = $OS_EventId + "_minor"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "3": ### major
                    $SEV_KEY = $OS_EventId + "_major"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "4": ### critical
                    $SEV_KEY = $OS_EventId + "_critical"
                    $DEFAULT_Severity = 5
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            if(match($5, "1"))
            {
                $SEV_KEY = $OS_EventId + "_clear"
                $DEFAULT_Severity = 1
                $DEFAULT_Type = 2
                $DEFAULT_ExpireTime = 0
            }

            update(@Summary)
            update(@Severity)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($tpAlarmId,$tpAlarmCatId,$tpAlarmCatName,$tpAlarmCatDesc,$tpAlarmSet,$tpAlarmNotify,$tpAlarmSeverity,$tpAlarmReported,$tpComponentId,$tpComponentType,$tpCompMMLName,$tpCompDesc,$tpCompParentId,$tpAlarmTime)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "tpAlarmId", $tpAlarmId, "tpAlarmCatId", $tpAlarmCatId, "tpAlarmCatName", $tpAlarmCatName,
                 "tpAlarmCatDesc", $tpAlarmCatDesc, "tpAlarmSet", $tpAlarmSet, "tpAlarmNotify", $tpAlarmNotify,
                 "tpAlarmSeverity", $tpAlarmSeverity, "tpAlarmReported", $tpAlarmReported, "tpComponentId", $tpComponentId,
                 "tpComponentType", $tpComponentType, "tpCompMMLName", $tpCompMMLName, "tpCompDesc", $tpCompDesc,
                 "tpCompParentId", $tpCompParentId, "tpAlarmTime", $tpAlarmTime)

        case "2": ### qualityOfService

            ##########
            # $1 = tpAlarmId 
            # $2 = tpAlarmCatId 
            # $3 = tpAlarmCatName 
            # $4 = tpAlarmCatDesc 
            # $5 = tpAlarmSet 
            # $6 = tpAlarmNotify 
            # $7 = tpAlarmSeverity 
            # $8 = tpAlarmReported 
            # $9 = tpComponentId 
            # $10 = tpComponentType 
            # $11 = tpCompMMLName 
            # $12 = tpCompDesc 
            # $13 = tpCompParentId 
            # $14 = tpAlarmTime 
            ##########

            $tpAlarmId = $1
            $tpAlarmCatId = $2
            $tpAlarmCatName = $3
            $tpAlarmCatDesc = $4
            $tpAlarmSet = lookup($5, tpAlarmSet) + " ( " + $5 + " )"
            $tpAlarmNotify = lookup($6, tpAlarmNotify) + " ( " + $6 + " )"
            $tpAlarmSeverity = lookup($7, tpAlarmSeverity) + " ( " + $7 + " )"
            $tpAlarmReported = lookup($8, tpAlarmReported) + " ( " + $8 + " )"
            $tpComponentId = $9
            $tpComponentType = $10
            $tpCompMMLName = $11
            $tpCompDesc = $12
            $tpCompParentId = $13
            $tpAlarmTime = $14

            $OS_EventId = "SNMPTRAP-cisco-CISCO-TRANSPATH-MIB-qualityOfService"

            @AlertGroup = $tpAlarmCatId + "-" + $tpAlarmCatName
            @AlertKey = "tpCompTableEntry." + $tpComponentId
            @Summary = "Failure in the Quality of Service ( " + @AlertKey + " )"

            switch($7)
            {
                case "1": ### informational
                    $SEV_KEY = $OS_EventId + "_informational"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "2": ### minor
                    $SEV_KEY = $OS_EventId + "_minor"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "3": ### major
                    $SEV_KEY = $OS_EventId + "_major"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "4": ### critical
                    $SEV_KEY = $OS_EventId + "_critical"
                    $DEFAULT_Severity = 5
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            if(match($5, "1"))
            {
                $SEV_KEY = $OS_EventId + "_clear"
                $DEFAULT_Severity = 1
                $DEFAULT_Type = 2
                $DEFAULT_ExpireTime = 0
            }

            update(@Summary)
            update(@Severity)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($tpAlarmId,$tpAlarmCatId,$tpAlarmCatName,$tpAlarmCatDesc,$tpAlarmSet,$tpAlarmNotify,$tpAlarmSeverity,$tpAlarmReported,$tpComponentId,$tpComponentType,$tpCompMMLName,$tpCompDesc,$tpCompParentId,$tpAlarmTime)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "tpAlarmId", $tpAlarmId, "tpAlarmCatId", $tpAlarmCatId, "tpAlarmCatName", $tpAlarmCatName,
                 "tpAlarmCatDesc", $tpAlarmCatDesc, "tpAlarmSet", $tpAlarmSet, "tpAlarmNotify", $tpAlarmNotify,
                 "tpAlarmSeverity", $tpAlarmSeverity, "tpAlarmReported", $tpAlarmReported, "tpComponentId", $tpComponentId,
                 "tpComponentType", $tpComponentType, "tpCompMMLName", $tpCompMMLName, "tpCompDesc", $tpCompDesc,
                 "tpCompParentId", $tpCompParentId, "tpAlarmTime", $tpAlarmTime)

        case "3": ### processingError

            ##########
            # $1 = tpAlarmId 
            # $2 = tpAlarmCatId 
            # $3 = tpAlarmCatName 
            # $4 = tpAlarmCatDesc 
            # $5 = tpAlarmSet 
            # $6 = tpAlarmNotify 
            # $7 = tpAlarmSeverity 
            # $8 = tpAlarmReported 
            # $9 = tpComponentId 
            # $10 = tpComponentType 
            # $11 = tpCompMMLName 
            # $12 = tpCompDesc 
            # $13 = tpCompParentId 
            # $14 = tpAlarmTime 
            ##########

            $tpAlarmId = $1
            $tpAlarmCatId = $2
            $tpAlarmCatName = $3
            $tpAlarmCatDesc = $4
            $tpAlarmSet = lookup($5, tpAlarmSet) + " ( " + $5 + " )"
            $tpAlarmNotify = lookup($6, tpAlarmNotify) + " ( " + $6 + " )"
            $tpAlarmSeverity = lookup($7, tpAlarmSeverity) + " ( " + $7 + " )"
            $tpAlarmReported = lookup($8, tpAlarmReported) + " ( " + $8 + " )"
            $tpComponentId = $9
            $tpComponentType = $10
            $tpCompMMLName = $11
            $tpCompDesc = $12
            $tpCompParentId = $13
            $tpAlarmTime = $14

            $OS_EventId = "SNMPTRAP-cisco-CISCO-TRANSPATH-MIB-processingError"

            @AlertGroup = $tpAlarmCatId + "-" + $tpAlarmCatName
            @AlertKey = "tpCompTableEntry." + $tpComponentId
            @Summary = "Process Fault Notification ( " + @AlertKey + " )"

            switch($7)
            {
                case "1": ### informational
                    $SEV_KEY = $OS_EventId + "_informational"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "2": ### minor
                    $SEV_KEY = $OS_EventId + "_minor"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "3": ### major
                    $SEV_KEY = $OS_EventId + "_major"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "4": ### critical
                    $SEV_KEY = $OS_EventId + "_critical"
                    $DEFAULT_Severity = 5
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            if(match($5, "1"))
            {
                $SEV_KEY = $OS_EventId + "_clear"
                $DEFAULT_Severity = 1
                $DEFAULT_Type = 2
                $DEFAULT_ExpireTime = 0
            }

            update(@Summary)
            update(@Severity)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($tpAlarmId,$tpAlarmCatId,$tpAlarmCatName,$tpAlarmCatDesc,$tpAlarmSet,$tpAlarmNotify,$tpAlarmSeverity,$tpAlarmReported,$tpComponentId,$tpComponentType,$tpCompMMLName,$tpCompDesc,$tpCompParentId,$tpAlarmTime)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "tpAlarmId", $tpAlarmId, "tpAlarmCatId", $tpAlarmCatId, "tpAlarmCatName", $tpAlarmCatName,
                 "tpAlarmCatDesc", $tpAlarmCatDesc, "tpAlarmSet", $tpAlarmSet, "tpAlarmNotify", $tpAlarmNotify,
                 "tpAlarmSeverity", $tpAlarmSeverity, "tpAlarmReported", $tpAlarmReported, "tpComponentId", $tpComponentId,
                 "tpComponentType", $tpComponentType, "tpCompMMLName", $tpCompMMLName, "tpCompDesc", $tpCompDesc,
                 "tpCompParentId", $tpCompParentId, "tpAlarmTime", $tpAlarmTime)

        case "4": ### equipmentError

            ##########
            # $1 = tpAlarmId 
            # $2 = tpAlarmCatId 
            # $3 = tpAlarmCatName 
            # $4 = tpAlarmCatDesc 
            # $5 = tpAlarmSet 
            # $6 = tpAlarmNotify 
            # $7 = tpAlarmSeverity 
            # $8 = tpAlarmReported 
            # $9 = tpComponentId 
            # $10 = tpComponentType 
            # $11 = tpCompMMLName 
            # $12 = tpCompDesc 
            # $13 = tpCompParentId 
            # $14 = tpAlarmTime 
            ##########

            $tpAlarmId = $1
            $tpAlarmCatId = $2
            $tpAlarmCatName = $3
            $tpAlarmCatDesc = $4
            $tpAlarmSet = lookup($5, tpAlarmSet) + " ( " + $5 + " )"
            $tpAlarmNotify = lookup($6, tpAlarmNotify) + " ( " + $6 + " )"
            $tpAlarmSeverity = lookup($7, tpAlarmSeverity) + " ( " + $7 + " )"
            $tpAlarmReported = lookup($8, tpAlarmReported) + " ( " + $8 + " )"
            $tpComponentId = $9
            $tpComponentType = $10
            $tpCompMMLName = $11
            $tpCompDesc = $12
            $tpCompParentId = $13
            $tpAlarmTime = $14

            $OS_EventId = "SNMPTRAP-cisco-CISCO-TRANSPATH-MIB-equipmentError"

            @AlertGroup = $tpAlarmCatId + "-" + $tpAlarmCatName
            @AlertKey = "tpCompTableEntry." + $tpComponentId
            @Summary = "Equipment Error Notification ( " + @AlertKey + " )"

            switch($7)
            {
                case "1": ### informational
                    $SEV_KEY = $OS_EventId + "_informational"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "2": ### minor
                    $SEV_KEY = $OS_EventId + "_minor"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "3": ### major
                    $SEV_KEY = $OS_EventId + "_major"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "4": ### critical
                    $SEV_KEY = $OS_EventId + "_critical"
                    $DEFAULT_Severity = 5
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            if(match($5, "1"))
            {
                $SEV_KEY = $OS_EventId + "_clear"
                $DEFAULT_Severity = 1
                $DEFAULT_Type = 2
                $DEFAULT_ExpireTime = 0
            }

            update(@Summary)
            update(@Severity)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($tpAlarmId,$tpAlarmCatId,$tpAlarmCatName,$tpAlarmCatDesc,$tpAlarmSet,$tpAlarmNotify,$tpAlarmSeverity,$tpAlarmReported,$tpComponentId,$tpComponentType,$tpCompMMLName,$tpCompDesc,$tpCompParentId,$tpAlarmTime)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "tpAlarmId", $tpAlarmId, "tpAlarmCatId", $tpAlarmCatId, "tpAlarmCatName", $tpAlarmCatName,
                 "tpAlarmCatDesc", $tpAlarmCatDesc, "tpAlarmSet", $tpAlarmSet, "tpAlarmNotify", $tpAlarmNotify,
                 "tpAlarmSeverity", $tpAlarmSeverity, "tpAlarmReported", $tpAlarmReported, "tpComponentId", $tpComponentId,
                 "tpComponentType", $tpComponentType, "tpCompMMLName", $tpCompMMLName, "tpCompDesc", $tpCompDesc,
                 "tpCompParentId", $tpCompParentId, "tpAlarmTime", $tpAlarmTime)

        case "5": ### environmentError

            ##########
            # $1 = tpAlarmId 
            # $2 = tpAlarmCatId 
            # $3 = tpAlarmCatName 
            # $4 = tpAlarmCatDesc 
            # $5 = tpAlarmSet 
            # $6 = tpAlarmNotify 
            # $7 = tpAlarmSeverity 
            # $8 = tpAlarmReported 
            # $9 = tpComponentId 
            # $10 = tpComponentType 
            # $11 = tpCompMMLName 
            # $12 = tpCompDesc 
            # $13 = tpCompParentId 
            # $14 = tpAlarmTime 
            ##########

            $tpAlarmId = $1
            $tpAlarmCatId = $2
            $tpAlarmCatName = $3
            $tpAlarmCatDesc = $4
            $tpAlarmSet = lookup($5, tpAlarmSet) + " ( " + $5 + " )"
            $tpAlarmNotify = lookup($6, tpAlarmNotify) + " ( " + $6 + " )"
            $tpAlarmSeverity = lookup($7, tpAlarmSeverity) + " ( " + $7 + " )"
            $tpAlarmReported = lookup($8, tpAlarmReported) + " ( " + $8 + " )"
            $tpComponentId = $9
            $tpComponentType = $10
            $tpCompMMLName = $11
            $tpCompDesc = $12
            $tpCompParentId = $13
            $tpAlarmTime = $14

            $OS_EventId = "SNMPTRAP-cisco-CISCO-TRANSPATH-MIB-environmentError"

            @AlertGroup = $tpAlarmCatId + "-" + $tpAlarmCatName
            @AlertKey = "tpCompTableEntry." + $tpComponentId
            @Summary = "Environment Error Notification ( " + @AlertKey + " )"

            switch($7)
            {
                case "1": ### informational
                    $SEV_KEY = $OS_EventId + "_informational"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "2": ### minor
                    $SEV_KEY = $OS_EventId + "_minor"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "3": ### major
                    $SEV_KEY = $OS_EventId + "_major"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "4": ### critical
                    $SEV_KEY = $OS_EventId + "_critical"
                    $DEFAULT_Severity = 5
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            if(match($5, "1"))
            {
                $SEV_KEY = $OS_EventId + "_clear"
                $DEFAULT_Severity = 1
                $DEFAULT_Type = 2
                $DEFAULT_ExpireTime = 0
            }

            update(@Summary)
            update(@Severity)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($tpAlarmId,$tpAlarmCatId,$tpAlarmCatName,$tpAlarmCatDesc,$tpAlarmSet,$tpAlarmNotify,$tpAlarmSeverity,$tpAlarmReported,$tpComponentId,$tpComponentType,$tpCompMMLName,$tpCompDesc,$tpCompParentId,$tpAlarmTime)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "tpAlarmId", $tpAlarmId, "tpAlarmCatId", $tpAlarmCatId, "tpAlarmCatName", $tpAlarmCatName,
                 "tpAlarmCatDesc", $tpAlarmCatDesc, "tpAlarmSet", $tpAlarmSet, "tpAlarmNotify", $tpAlarmNotify,
                 "tpAlarmSeverity", $tpAlarmSeverity, "tpAlarmReported", $tpAlarmReported, "tpComponentId", $tpComponentId,
                 "tpComponentType", $tpComponentType, "tpCompMMLName", $tpCompMMLName, "tpCompDesc", $tpCompDesc,
                 "tpCompParentId", $tpCompParentId, "tpAlarmTime", $tpAlarmTime)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-TRANSPATH-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-TRANSPATH-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-TRANSPATH-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-TRANSPATH-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-TRANSPATH-MIB.include.snmptrap.rules >>>>>")
