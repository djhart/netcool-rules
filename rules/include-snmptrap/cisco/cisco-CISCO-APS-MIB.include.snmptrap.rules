###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-APS-MIB
#
###############################################################################
#
# 2.1 - Simplified handling of "Severity via lookup" logic.
#
#     - Added basic debug logging.
#
# 2.0 - Rewritten for improved event presentation.  Includes enhancements which
#       comply with the Netcool Rules File Standards
#       (MUSE-STD-RF-02, July 2002)
#
#     - Modified to support following features introduced in NCiL 2.0:
#         - "Advanced" and "User" include files
#         - "Severity" lookup tables
#
# 1.0 - Based on rules extracted from cisco.include.snmptrap.rules 3.1.
#
###############################################################################

case ".1.3.6.1.4.1.9.10.71.2": ### Cisco Automatic Protection Switching (APS) - Notifications from CISCO-APS-MIB

    log(DEBUG, "<<<<< Entering... cisco-CISCO-APS-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Cisco-APS"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### cApsEventSwitchover

            ##########
            # $1 = cApsChanStatusSwitchovers
            # $2 = cApsChanStatusCurrent
            ##########

            $cApsChanStatusSwitchovers = $1
            $cApsChanStatusCurrent = lookup($2, cApsChanStatusCurrent) + " ( " + $2 + " )"
            
            $cApsChanConfigGroupName_OctetString = extract($OID2, "3\.6\.1\.4\.1\.9\.10\.71\.1\.6\.1\.1\.(.*)\.[0-9]+$")
            $OctetString = $cApsChanConfigGroupName_OctetString
            include "$NC_RULES_HOME/include-snmptrap/decodeOctetString.include.snmptrap.rules"
            $cApsChanConfigGroupName = $String
            
            $cApsChanConfigNumber = extract($OID2, "\.([0-9]+)$")
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cApsChanStatusSwitchovers,$cApsChanStatusCurrent,$cApsChanConfigGroupName,$cApsChanConfigNumber)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cApsChanStatusSwitchovers", $cApsChanStatusSwitchovers, "cApsChanStatusCurrent", $cApsChanStatusCurrent, "cApsChanConfigGroupName", $cApsChanConfigGroupName,
                 "cApsChanConfigNumber", $cApsChanConfigNumber)

            $OS_EventId = "SNMPTRAP-cisco-CISCO-APS-MIB-cApsEventSwitchover"

            @AlertGroup = "APS Channel Status"
            @AlertKey = "cApsChanStatusEntry." + $cApsChanConfigGroupName_OctetString + "." + $cApsChanConfigNumber
            switch($2)
            {
                case "0": ### ok
                    @Summary = "APS Channel OK"
                    
                    $SEV_KEY = $OS_EventId + "_ok"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    
                case "1": ### lockedOut
                    @Summary = "APS Channel Locked Out"
                    
                    $SEV_KEY = $OS_EventId + "_lockedOut"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "2": ### sd
                    @Summary = "APS Channel Signal Degraded"
                    
                    $SEV_KEY = $OS_EventId + "_sd"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "4": ### sf
                    @Summary = "APS Channel Signal Failed"
                    
                    $SEV_KEY = $OS_EventId + "_sf"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "8": ### switched
                    @Summary = "APS Channel Switched Over"
                    
                    $SEV_KEY = $OS_EventId + "_switched"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                default:
                    @Summary = "APS Channel Status Unknown"
                    
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
            }
            @Summary = @Summary + "  ( APS Group: " + $cApsChanConfigGroupName + ", Channel: " + $cApsChanConfigNumber + " )"
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $2

        case "2": ### cApsEventModeMismatch

            ##########
            # $1 = cApsStatusModeMismatches
            # $2 = cApsStatusCurrent
            ##########

            $cApsStatusModeMismatches = $1
            $cApsStatusCurrent = lookup($2, cApsStatusCurrent) + " ( " + $2 + " )"
            
            $cApsConfigName_OctetString = extract($OID2, "3\.6\.1\.4\.1\.9\.10\.71\.1\.2\.1\.3\.(.*)$")
            $OctetString = $cApsConfigName_OctetString
            include "$NC_RULES_HOME/include-snmptrap/decodeOctetString.include.snmptrap.rules"
            $cApsConfigName = $String
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cApsStatusModeMismatches,$cApsStatusCurrent,$cApsConfigName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cApsStatusModeMismatches", $cApsStatusModeMismatches, "cApsStatusCurrent", $cApsStatusCurrent, "cApsConfigName", $cApsConfigName)

            $OS_EventId = "SNMPTRAP-cisco-CISCO-APS-MIB-cApsEventModeMismatch"

            @AlertGroup = "APS Group Status"
            @AlertKey = "cApsStatusEntry." + $cApsConfigName_OctetString
            @Summary = "APS Group Mode Mismatch  ( APS Group: " + $cApsConfigName + " )"
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $2

        case "3": ### cApsEventChannelMismatch

            ##########
            # $1 = cApsStatusChannelMismatches
            # $2 = cApsStatusCurrent
            ##########

            $cApsStatusChannelMismatches = $1
            $cApsStatusCurrent = lookup($2, cApsStatusCurrent) + " ( " + $2 + " )"
            
            $cApsConfigName_OctetString = extract($OID2, "3\.6\.1\.4\.1\.9\.10\.71\.1\.2\.1\.3\.(.*)$")
            $OctetString = $cApsConfigName_OctetString
            include "$NC_RULES_HOME/include-snmptrap/decodeOctetString.include.snmptrap.rules"
            $cApsConfigName = $String
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cApsStatusChannelMismatches,$cApsStatusCurrent,$cApsConfigName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cApsStatusChannelMismatches", $cApsStatusChannelMismatches, "cApsStatusCurrent", $cApsStatusCurrent, "cApsConfigName", $cApsConfigName)

            $OS_EventId = "SNMPTRAP-cisco-CISCO-APS-MIB-cApsEventChannelMismatch"

            @AlertGroup = "APS Group Status"
            @AlertKey = "cApsStatusEntry." + $cApsConfigName_OctetString
            @Summary = "APS Group Channel Mismatch  ( APS Group: " + $cApsConfigName + " )"
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $2

        case "4": ### cApsEventPSBF

            ##########
            # $1 = cApsStatusPSBFs 
            # $2 = cApsStatusCurrent 
            ##########

            $cApsStatusPSBFs = $1
            $cApsStatusCurrent = lookup($2, cApsStatusCurrent) + " ( " + $2 + " )"
            
            $cApsConfigName_OctetString = extract($OID2, "3\.6\.1\.4\.1\.9\.10\.71\.1\.2\.1\.3\.(.*)$")
            $OctetString = $cApsConfigName_OctetString
            include "$NC_RULES_HOME/include-snmptrap/decodeOctetString.include.snmptrap.rules"
            $cApsConfigName = $String
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cApsStatusPSBFs,$cApsStatusCurrent,$cApsConfigName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cApsStatusPSBFs", $cApsStatusPSBFs, "cApsStatusCurrent", $cApsStatusCurrent, "cApsConfigName", $cApsConfigName)

            $OS_EventId = "SNMPTRAP-cisco-CISCO-APS-MIB-cApsEventPSBF"

            @AlertGroup = "APS Group Status"
            @AlertKey = "cApsStatusEntry." + $cApsConfigName_OctetString
            @Summary = "APS Group Protection Switch Byte Failure  ( APS Group: " + $cApsConfigName + " )"
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $2

        case "5": ### cApsEventFEPLF

            ##########
            # $1 = cApsStatusFEPLFs
            # $2 = cApsStatusCurrent
            ##########

            $cApsStatusFEPLFs = $1
            $cApsStatusCurrent = lookup($2, cApsStatusCurrent) + " ( " + $2 + " )"
            
            $cApsConfigName_OctetString = extract($OID2, "3\.6\.1\.4\.1\.9\.10\.71\.1\.2\.1\.3\.(.*)$")
            $OctetString = $cApsConfigName_OctetString
            include "$NC_RULES_HOME/include-snmptrap/decodeOctetString.include.snmptrap.rules"
            $cApsConfigName = $String
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cApsStatusFEPLFs,$cApsStatusCurrent,$cApsConfigName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cApsStatusFEPLFs", $cApsStatusFEPLFs, "cApsStatusCurrent", $cApsStatusCurrent, "cApsConfigName", $cApsConfigName)

            $OS_EventId = "SNMPTRAP-cisco-CISCO-APS-MIB-cApsEventFEPLF"

            @AlertGroup = "APS Group Status"
            @AlertKey = "cApsStatusEntry." + $cApsConfigName_OctetString
            @Summary = "APS Group Far-End Protection-Line Failure  ( APS Group: " + $cApsConfigName + " )"
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $2

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-APS-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-APS-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-APS-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-APS-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-APS-MIB.include.snmptrap.rules >>>>>")
