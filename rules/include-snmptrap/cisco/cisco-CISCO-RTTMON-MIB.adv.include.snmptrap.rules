###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 2.0 - Trap(s) added
#
#         - rttMonNotification
#         - rttMonLpdDiscoveryNotification
#         - rttMonLpdGrpStatusNotification
#
# 1.1 - Added basic debug logging.
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-RTTMON-MIB
#
###############################################################################

log(DEBUG, "<<<<< Entering... cisco-CISCO-RTTMON-MIB.adv.include.snmptrap.rules >>>>>")

switch($specific-trap)
{
    case "1": ### rttMonConnectionChangeNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 100015
        $OS_X733SpecificProb = "rttMonConnectionChangeNotification"
        $OS_OsiLayer = 3
        
        $OS_LocalPriObj = "rttMonCtrlOperEntry." + $rttMonCtrlAdminIndex
        $OS_LocalRootObj = "rttMonCtrlAdminEntry." + $rttMonCtrlAdminIndex
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "2": ### rttMonTimeoutNotification

        $OS_X733EventType = 2
        $OS_X733ProbableCause = 2001
        $OS_X733SpecificProb = "rttMonTimeoutNotification"
        $OS_OsiLayer = 3
        
        $OS_LocalPriObj = "rttMonCtrlOperEntry." + $rttMonCtrlAdminIndex
        $OS_LocalRootObj = "rttMonCtrlAdminEntry." + $rttMonCtrlAdminIndex
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "3": ### rttMonThresholdNotification

        $OS_X733EventType = 2
        $OS_X733ProbableCause = 2001
        $OS_X733SpecificProb = "rttMonThresholdNotification"
        $OS_OsiLayer = 3
        
        $OS_LocalPriObj = "rttMonCtrlOperEntry." + $rttMonCtrlAdminIndex
        $OS_LocalRootObj = "rttMonCtrlAdminEntry." + $rttMonCtrlAdminIndex
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "4": ### rttMonVerifyErrorNotification

        $OS_X733EventType = 3
        $OS_X733ProbableCause = 100072
        $OS_X733SpecificProb = "rttMonVerifyErrorNotification"
        $OS_OsiLayer = 3
        
        $OS_LocalPriObj = "rttMonCtrlOperEntry." + $rttMonCtrlAdminIndex
        $OS_LocalRootObj = "rttMonCtrlAdminEntry." + $rttMonCtrlAdminIndex
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "5": ### rttMonNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "rttMonNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "rttMonReactEntry." + $rttMonCtrlAdminIndex + "." + $rttMonReactConfigIndex
        $OS_LocalRootObj = "rttMonCtrlAdminEntry." + $rttMonCtrlAdminIndex
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "6": ### rttMonLpdDiscoveryNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "rttMonLpdDiscoveryNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "rttMonLpdGrpStatsEntry." + $rttMonLpdGrpStatsGroupIndex + "." + $rttMonLpdGrpStatsStartTimeIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "7": ### rttMonLpdGrpStatusNotification

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "rttMonLpdGrpStatusNotification"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "rttMonLpdGrpStatsEntry." + $rttMonLpdGrpStatsGroupIndex + "." + $rttMonLpdGrpStatsStartTimeIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    default:
}

log(DEBUG, "<<<<< Leaving... cisco-CISCO-RTTMON-MIB.adv.include.snmptrap.rules >>>>>")
