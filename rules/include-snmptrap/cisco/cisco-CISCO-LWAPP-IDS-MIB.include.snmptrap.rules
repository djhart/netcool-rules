###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-LWAPP-IDS-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.519": ### Cisco Light Weight Access Point Protocol with IDS/IPS Application Integration - Notifications from CISCO-LWAPP-IDS-MIB (200604100000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-LWAPP-IDS-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Cisco-LWAPP-IDS-MIB"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### ciscoLwappIdsShunClientUpdate

            ##########
            # $1 = cLIdsClientTimeRemaining 
            ##########

            $cLIdsClientTimeRemaining = $1

            $OS_EventId = "SNMPTRAP-cisco-CISCO-LWAPP-IDS-MIB-ciscoLwappIdsShunClientUpdate"

            @AlertGroup = "cLIdsClientExclTable Row Status"

            $cLIdsIpsSensorAddressType_Raw = extract($OID1, "3\.6\.1\.4\.1\.9\.9\.519\.1\.2\.1\.1\.3\.([0-9]+)\..*$")

            if(match($cLIdsIpsSensorAddressType_Raw, "1"))
            {
                $cLIdsIpsSensorAddress = extract($OID1, "3\.6\.1\.4\.1\.9\.9\.519\.1\.2\.1\.1\.3\.1\.([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)\..*$")
                $cLIdsClientAddressType_Raw = extract($OID1, "3\.6\.1\.4\.1\.9\.9\.519\.1\.2\.1\.1\.3\.1\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.([0-9]+)\..*$")
                $cLIdsClientAddress = extract($OID1, "3\.6\.1\.4\.1\.9\.9\.519\.1\.2\.1\.1\.3\.1\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.(.*)$")
            }

            else

            if(match($cLIdsIpsSensorAddressType_Raw, "2"))
            {
                $cLIdsIpsSensorAddress = extract($OID1, "3\.6\.1\.4\.1\.9\.9\.519\.1\.2\.1\.1\.3\.2\.([0-9]+\:[0-9]+\:[0-9]+\:[0-9]+\:[0-9]+\:[0-9]+\:[0-9]+\:[0-9]+)\..$")
                $cLIdsClientAddressType_Raw = extract($OID1, "3\.6\.1\.4\.1\.9\.9\.519\.1\.2\.1\.1\.3\.2\.[0-9]+\:[0-9]+\:[0-9]+\:[0-9]+\:[0-9]+\:[0-9]+\:[0-9]+\:[0-9]+\.([0-9]+)\..*$")
                $cLIdsClientAddress = extract($OID1, "3\.6\.1\.4\.1\.9\.9\.519\.1\.2\.1\.1\.3\.2\.[0-9]+\:[0-9]+\:[0-9]+\:[0-9]+\:[0-9]+\:[0-9]+\:[0-9]+\:[0-9]+\.[0-9]+\.(.*)$")
            }

            else

            if((match($cLIdsIpsSensorAddressType_Raw, "2"))&&(regmatch($OID1, ".*\.519\.1\.2\.1\.1\.3\.2\.0\:0\:0\:0\:0\..*")))
            {
               $cLIdsIpsSensorAddress = extract($OID1, "3\.6\.1\.4\.1\.9\.9\.519\.1\.2\.1\.1\.3\.2\.([0-9]+\:[0-9]+\:[0-9]+\:[0-9]+\:[0-9]+)\.([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)\..$")
               $cLIdsClientAddressType_Raw = extract ($OID1, "3\.6\.1\.4\.1\.9\.9\.519\.1\.2\.1\.1\.3\.2\.[0-9]+\:[0-9]+\:[0-9]+\:[0-9]+\:[0-9]+)\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.([0-9]+)\..*$")
               $cLIdsClientAddress = extract ($OID1, "3\.6\.1\.4\.1\.9\.9\.519\.1\.2\.1\.1\.3\.2\.[0-9]+\:[0-9]+\:[0-9]+\:[0-9]+\:[0-9]+)\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.(.*)$")
            }

            else

            {
                $cLIdsIpsSensorAddress = extract($OID1, "3\.6\.1\.4\.1\.9\.9\.519\.1\.2\.1\.1\.3\.[0-9]+\.([0-9]+\.[0-9]+)\..*$")
                $cLIdsClientAddressType_Raw = extract($OID1, "3\.6\.1\.4\.1\.9\.9\.519\.1\.2\.1\.1\.3\.[0-9]+\.[0-9]+\.[0-9]+\.([0-9]+)\..*$")
                $cLIdsClientAddress = extract($OID1, "3\.6\.1\.4\.1\.9\.9\.519\.1\.2\.1\.1\.3\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.(.*)$")
            }

            $cLIdsIpsSensorAddressType = lookup($cLIdsIpsSensorAddressType_Raw, InetAddressType)

            $cLIdsClientAddressType = lookup($cLIdsClientAddressType_Raw, InetAddressType)

            @AlertKey = "cLIdsClientExclEntry." + $cLIdsIpsSensorAddressType_Raw + "." + $cLIdsIpsSensorAddress + "." + $cLIdsClientAddressType_Raw + "." + $cLIdsClientAddress

            if(match($cLIdsClientTimeRemaining, "0"))
            {
              @Summary = "A Row is Removed From cLIdsClientExclTable"
            }
            else
            {
              @Summary = "A Row is Added to cLIdsClientExclTable"
            }

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            $cLIdsIpsSensorAddressType = $cLIdsIpsSensorAddressType + " ( " + $cLIdsIpsSensorAddressType_Raw + " ) "

            $cLIdsClientAddressType = $cLIdsClientAddressType + " ( " + $cLIdsClientAddressType_Raw + " ) "

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cLIdsClientTimeRemaining,$cLIdsIpsSensorAddressType,$cLIdsIpsSensorAddress,$cLIdsClientAddressType,$cLIdsClientAddress)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cLIdsClientTimeRemaining", $cLIdsClientTimeRemaining, "cLIdsIpsSensorAddressType", $cLIdsIpsSensorAddressType, "cLIdsIpsSensorAddress", $cLIdsIpsSensorAddress,
                 "cLIdsClientAddressType", $cLIdsClientAddressType, "cLIdsClientAddress", $cLIdsClientAddress)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-LWAPP-IDS-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-LWAPP-IDS-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-LWAPP-IDS-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-LWAPP-IDS-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-LWAPP-IDS-MIB.include.snmptrap.rules >>>>>")
