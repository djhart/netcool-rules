###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-IP-LOCAL-POOL-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.326": ### Cisco IP Local Pool - Notifications from CISCO-IP-LOCAL-POOL-MIB (20030403)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-IP-LOCAL-POOL-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Cisco-IP Local Pool"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### ciscoIpLocalPoolInUseAddrNoti

            ##########
            # $1 = cIpLocalPoolStatFreeAddrs 
            # $2 = cIpLocalPoolStatInUseAddrs 
            ##########

            $cIpLocalPoolStatFreeAddrs = $1
            $cIpLocalPoolStatInUseAddrs = $2 
            
            $cIpLocalPoolName_Raw = extract($OID1, "\.3\.6\.1\.4\.1\.9\.9\.326\.1\.3\.1\.1\.(.*)$")
            $OctetString = $cIpLocalPoolName_Raw
            include "$NC_RULES_HOME/include-snmptrap/decodeOctetString.include.snmptrap.rules"
            $cIpLocalPoolName = $String
            
            $OS_EventId = "SNMPTRAP-cisco-CISCO-IP-LOCAL-POOL-MIB-ciscoIpLocalPoolInUseAddrNoti"

            @AlertGroup = "IP Local Pool Threshold"
            @AlertKey = "cIpLocalPoolStatsEntry." + $cIpLocalPoolName_Raw
            @Summary = "IP Addresses Used: " + $2 + ", Exceeds Threshold: " + $1 + "  ( IP Pool: " + $cIpLocalPoolName + " )" 
            
            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cIpLocalPoolStatFreeAddrs,$cIpLocalPoolStatInUseAddrs,$cIpLocalPoolName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cIpLocalPoolStatFreeAddrs", $cIpLocalPoolStatFreeAddrs, "cIpLocalPoolStatInUseAddrs", $cIpLocalPoolStatInUseAddrs, "cIpLocalPoolName", $cIpLocalPoolName)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-IP-LOCAL-POOL-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-IP-LOCAL-POOL-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-IP-LOCAL-POOL-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-IP-LOCAL-POOL-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-IP-LOCAL-POOL-MIB.include.snmptrap.rules >>>>>")
