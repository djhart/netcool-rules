###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-RTTMON-MIB
#
###############################################################################

table RttMonReactVar =
{
    ##########
    #  This object indicates specific Reaction variables for a particular probe type
    ##########

    {"1","Round Trip Time"}, ### rtt
    {"2","Jitter Source to Destination Average"}, ### jitterSDAvg
    {"3","Jitter Destionation to Source Average"}, ### jitterDSAvg
    {"4","Packet Loss from Source to Destination"}, ### packetLossSD
    {"5","Packet Loss from Destination to Source"}, ### packetLossDS
    {"6","Mean Opinion Score"}, ### mos
    {"7","Operation Timeout"}, ### timeout
    {"8","Connection Failed"}, ### connectionLoss
    {"9","Data Corruption"}, ### verifyError
    {"10","Jitter Average"}, ### jitterAvg
    {"11","Calculated Planning Impairment Factor"}, ### icpif
    {"12","Missing In Action"}, ### packetMIA
    {"13","Packets Arriving Late"}, ### packetLateArrival
    {"14","Packets Arriving out of Sequence"}, ### packetOutOfSequence
    {"15","Maximum Positive Jitter from Source to Destination"}, ### maxOfPositiveSD
    {"16","Maximum Negative Jitter from Source to Destination"}, ### maxOfNegativeSD
    {"17","Maximum Positive Jitter from Destination to Source"}, ### maxOfPositiveDS
    {"18","Maximum Negative Jitter from Destination to Source"}, ### maxOfNegativeDS
    {"19","Inter Arrival Jitter from Destination to Source"}, ### iaJitterDS
    {"20","Frame Loss Recorded at Source DSP"}, ### frameLossDS
    {"21","Listener Quality MOS at Source"}, ### mosLQDS
    {"22","Conversational Quality MOS at Source"}, ### mosCQDS
    {"23","R-Factor Value at Destination"}, ### rFactorDS
    {"24","Successive Dropped Packet"}, ### successivePacketLoss
    {"25","Maximum Latency from Destination to Source"}, ### maxOfLatencyDS
    {"26","Maximum Latency from Source to Destination"}, ### maxOfLatencySD
    {"27","Latency Average from Destination to Source"}, ### latencyDSAvg
    {"28","Latency Average from Source to Destination"}, ### latencySDAvg
    {"29","Packets Loss in Both Directions"}, ### packetLoss
    {"30","Inter Arrival Jitter from Source to Destination"}, ### iaJitterSD
    {"31","Conversational quality MOS at Destination"}, ### mosCQSD
    {"32","R-Factor value at Destination"}, ### rFactorSD
}
default = "Unknown"


table RttMplsVpnMonLpdFailureSense =
{
    ##########
    # This object indicates the causes of failure in LSP Path Discovery
    ##########

    {"1","Unknown"}, ### unknown
    {"2","No Path"}, ### noPath
    {"3","All Paths Broken"}, ### allPathsBroken
    {"4","All Paths Unexplorable"}, ### allPathsUnexplorable
    {"5","All Paths Broken or Unexplorable"}, ### allPathsBrokenOrUnexplorable
    {"6","Timeout"}, ### timeout
    {"7","Error"}, ### Error
}
default = "Unknown"


table RttMplsVpnMonLpdGrpStatus =
{
    ##########
    # This object indicates the status of the LPD Group
    ##########

    {"1","Unknown"}, ### unknown
    {"2","Up"}, ### up
    {"3","Partial"}, ### partial
    {"4","Down"}, ### down
}
default = "Unknown"

