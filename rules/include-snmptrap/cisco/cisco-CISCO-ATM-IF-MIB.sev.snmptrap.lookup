###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-ATM-IF-MIB
#
# 1.1 - Changed the format
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table cisco-CISCO-ATM-IF-MIB_sev =
{
	    {"SNMPTRAP-cisco-CISCO-ATM-IF-MIB-ciscoAtmIfEvent_down","3","1","0"},
	    {"SNMPTRAP-cisco-CISCO-ATM-IF-MIB-ciscoAtmIfEvent_restarting","2","12","0"},
	    {"SNMPTRAP-cisco-CISCO-ATM-IF-MIB-ciscoAtmIfEvent_waitDevType","2","12","0"},
	    {"SNMPTRAP-cisco-CISCO-ATM-IF-MIB-ciscoAtmIfEvent_deviceAndPortTypeComplete","2","12","0"},
	    {"SNMPTRAP-cisco-CISCO-ATM-IF-MIB-ciscoAtmIfEvent_awaitPnniConfig","2","12","0"},
	    {"SNMPTRAP-cisco-CISCO-ATM-IF-MIB-ciscoAtmIfEvent_pnniConfigComplete","2","12","0"},
	    {"SNMPTRAP-cisco-CISCO-ATM-IF-MIB-ciscoAtmIfEvent_awaitRestartAck","2","12","0"},
	    {"SNMPTRAP-cisco-CISCO-ATM-IF-MIB-ciscoAtmIfEvent_upAndNormal","1","2","0"},
	    {"SNMPTRAP-cisco-CISCO-ATM-IF-MIB-ciscoAtmIfEvent_unknown","2","1","0"}
}
default = {"Unknown","Unknown","Unknown"}
