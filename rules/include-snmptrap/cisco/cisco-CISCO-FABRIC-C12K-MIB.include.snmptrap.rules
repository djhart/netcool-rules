###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-FABRIC-C12K-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.281": ### Cisco Fabric c12000 Series Routers - Notifications from CISCO-FABRIC-C12K-MIB (20020920)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-FABRIC-C12K-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Cisco-Fabric c12000 Routers"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### ciscoFabricC12kMIBFabMasterSchCh

            ##########
            # $1 = cfcGenericGlobalFabMasterSched 
            # $2 = entPhysicalName 
            ##########

            $cfcGenericGlobalFabMasterSched = $1
            $entPhysicalName = $2

            $OS_EventId = "SNMPTRAP-cisco-CISCO-FABRIC-C12K-MIB-ciscoFabricC12kMIBFabMasterSchCh"

            @AlertGroup = "Master Scheduler Status"
            @AlertKey = "entPhysicalEntry." + $1                                   
            @Summary = "Master Scheduler Changed on System  ( Master Scheduler: " + $2 + " )"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager +  " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cfcGenericGlobalFabMasterSched,$entPhysicalName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cfcGenericGlobalFabMasterSched", $cfcGenericGlobalFabMasterSched, "entPhysicalName", $entPhysicalName)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-FABRIC-C12K-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-FABRIC-C12K-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-FABRIC-C12K-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-FABRIC-C12K-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-FABRIC-C12K-MIB.include.snmptrap.rules >>>>>")
