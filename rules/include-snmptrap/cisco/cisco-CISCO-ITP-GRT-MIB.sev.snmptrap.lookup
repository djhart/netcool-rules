###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
# Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
# - CISCO-ITP-GRT-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table cisco-CISCO-ITP-GRT-MIB_sev =
{
    {"SNMPTRAP-cisco-CISCO-ITP-GRT-MIB-ciscoGrtDestStateChange","2","13","1800"},
    {"SNMPTRAP-cisco-CISCO-ITP-GRT-MIB-ciscoGrtMgmtStateChange","2","13","1800"},
    {"SNMPTRAP-cisco-CISCO-ITP-GRT-MIB-ciscoGrtRouteTableLoad_loadNotRequested","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-ITP-GRT-MIB-ciscoGrtRouteTableLoad_loadInProgress","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-ITP-GRT-MIB-ciscoGrtRouteTableLoad_loadComplete","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-ITP-GRT-MIB-ciscoGrtRouteTableLoad_loadCompleteWithErrors","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-ITP-GRT-MIB-ciscoGrtRouteTableLoad_loadFailed","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-ITP-GRT-MIB-ciscoGrtRouteTableLoad_unknown","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-ITP-GRT-MIB-ciscoGrtDestStateChangeRev1_unknown","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-ITP-GRT-MIB-ciscoGrtDestStateChangeRev1_accessible","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-ITP-GRT-MIB-ciscoGrtDestStateChangeRev1_inaccessible","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-ITP-GRT-MIB-ciscoGrtDestStateChangeRev1_restricted","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-ITP-GRT-MIB-ciscoGrtMgmtStateChangeRev1_unknown","2","1","0"},
    {"SNMPTRAP-cisco-CISCO-ITP-GRT-MIB-ciscoGrtMgmtStateChangeRev1_available","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-ITP-GRT-MIB-ciscoGrtMgmtStateChangeRev1_restricted","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-ITP-GRT-MIB-ciscoGrtMgmtStateChangeRev1_unavailable","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-ITP-GRT-MIB-ciscoGrtMgmtStateChangeRev1_deleted","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-ITP-GRT-MIB-ciscoGrtNoRouteMSUDiscards","2","13","1800"}
}
default = {"Unknown","Unknown","Unknown"}
