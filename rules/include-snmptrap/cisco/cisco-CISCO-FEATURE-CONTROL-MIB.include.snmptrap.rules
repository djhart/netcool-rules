###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 1.1 - Updated Release for MIB (201404090000Z)
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-FEATURE-CONTROL-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.377": ### - Notifications from CISCO-FEATURE-CONTROL-MIB (201404090000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-FEATURE-CONTROL-MIB.include.snmptrap.rules >>>>>")

    @Agent = "cisco-CISCO-FEATURE-CONTROL-MIB"
    @Class = "40057"
    
    $OPTION_TypeFieldUsage = "3.6"
    

    switch($specific-trap)
    {
    
     
        case "1": ### ciscoFeatureOpStatusChange
        
            discard
         
        case "2": ### ciscoFeatOpStatusChange
        
            ##########
            # $1 = cfcFeatureCtrlOpStatus2
            ##########
            
            $cfcFeatureCtrlOpStatus2 = lookup($1, CiscoFeatureStatus)
            
            $cfcFeatureCtrlIndex2_Raw = extract($OID1,"\.([0-9]+)\.[0-9]+$")
            $cfcFeatureCtrlInstanceNum2 = extract($OID1,"\.[0-9]+\.([0-9]+)$")
            $cfcFeatureCtrlIndex2 = lookup($cfcFeatureCtrlIndex2_Raw,CiscoOptionalFeature)
            
            $OS_EventId = "SNMPTRAP-cisco-CISCO-FEATURE-CONTROL-MIB-ciscoFeatOpStatusChange"

            @AlertGroup = $cfcFeatureCtrlIndex2 + " " + "Feature Status"
            @AlertKey = "cfcFeatureCtrl2Entry" + "." + $cfcFeatureCtrlIndex2_Raw + "." + $cfcFeatureCtrlInstanceNum2
            @Summary = $cfcFeatureCtrlIndex2 + " " + $cfcFeatureCtrlOpStatus2 + " ( " + @AlertKey + " ) "
            
            switch($1)
            {
                case "1":### unknown
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "2":### enabled
                    $SEV_KEY = $OS_EventId + "_enabled"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0               
                case "3":### disabled
                    $SEV_KEY = $OS_EventId + "_disabled"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "4":### installed
                    $SEV_KEY = $OS_EventId + "_installed"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0               
                case "5":### uninstalled
                    $SEV_KEY = $OS_EventId + "_uninstalled"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                default: 
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = $cfcFeatureCtrlOpStatus2 + " " + "Status Unknown."
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $cfcFeatureCtrlOpStatus2 = $cfcFeatureCtrlOpStatus2 + " ( " + $1 + " )"
            
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cfcFeatureCtrlOpStatus2", $cfcFeatureCtrlOpStatus2, "cfcFeatureCtrlIndex2_Raw", $cfcFeatureCtrlIndex2_Raw, 
                "cfcFeatureCtrlIndex2", $cfcFeatureCtrlIndex2, "cfcFeatureCtrlInstanceNum2", $cfcFeatureCtrlInstanceNum2)
         
        case "3": ### ciscoFeatureSetOpStatusChange
        
            ##########
            # $1 = cfcFeatureSetOpStatus
            ##########
            
            $cfcFeatureSetOpStatus = lookup($1, CiscoFeatureStatus)
            
            $cfcFeatureSetIndex_Raw = extract($OID1,"\.([0-9]+)$")
            $cfcFeatureSetIndex = lookup($cfcFeatureSetIndex_Raw,CiscoOptionalFeatureSet)
            
            $OS_EventId = "SNMPTRAP-cisco-CISCO-FEATURE-CONTROL-MIB-ciscoFeatureSetOpStatusChange"

            @AlertGroup = $cfcFeatureSetIndex + " " + "Feature Set Status"
            @AlertKey = "cfcFeatureSetEntry" + "." + $cfcFeatureSetIndex_Raw
            @Summary = $cfcFeatureSetIndex + " " + $cfcFeatureSetOpStatus + " ( " + @AlertKey + " ) "
            
            switch($1)
            {
                case "1":### unknown
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "2":### enabled
                    $SEV_KEY = $OS_EventId + "_enabled"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0               
                case "3":### disabled
                    $SEV_KEY = $OS_EventId + "_disabled"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "4":### installed
                    $SEV_KEY = $OS_EventId + "_installed"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0               
                case "5":### uninstalled
                    $SEV_KEY = $OS_EventId + "_uninstalled"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                default: 
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $cfcFeatureSetOpStatus = $cfcFeatureSetOpStatus + " ( " + $1 + " )"
            
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cfcFeatureSetOpStatus", $cfcFeatureSetOpStatus, "cfcFeatureSetIndex_Raw", $cfcFeatureSetIndex_Raw, 
                "cfcFeatureSetIndex", $cfcFeatureSetIndex)
         
        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-FEATURE-CONTROL-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-FEATURE-CONTROL-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-FEATURE-CONTROL-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-FEATURE-CONTROL-MIB.user.include.snmptrap.rules"


##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-FEATURE-CONTROL-MIB.include.snmptrap.rules >>>>>")


