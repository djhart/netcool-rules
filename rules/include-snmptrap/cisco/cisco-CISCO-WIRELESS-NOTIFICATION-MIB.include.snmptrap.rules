###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 2.0 - Updated Release.
#
#          -  CISCO-WIRELESS-NOTIFICATION-MIB revision 201105140000Z
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-WIRELESS-NOTIFICATION-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.199991"|".1.3.6.1.4.1.9.9.712": ### Cisco Wireless Notification - Notifications from CISCO-WIRELESS-NOTIFICATION-MIB (201105140000Z and 200910280000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-WIRELESS-NOTIFICATION-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Cisco Prime Network Control System (NCS)"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### ciscoWirelessMOStatusNotification

            ##########
            # $1 = cWNotificationTimestamp 
            # $2 = cWNotificationUpdatedTimestamp 
            # $3 = cWNotificationKey 
            # $4 = cWNotificationCategory 
            # $5 = cWNotificationSubCategory 
            # $6 = cWNotificationManagedObjectAddressType 
            # $7 = cWNotificationManagedObjectAddress 
            # $8 = cWNotificationSourceDisplayName 
            # $9 = cWNotificationDescription 
            # $10 = cWNotificationSeverity 
            # $11 = cWNotificationSpecialAttributes 
            # $12 = cWNotificationVirtualDomains 
            ##########

            $HexTimeValue = $1_hex
            include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-WIRELESS-NOTIFICATION-MIB-decodeTimeValue.include.snmptrap.rules"
            $cWNotificationTimestamp = $decodedTimeValue	
            $HexTimeValue = $2_hex
            include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-WIRELESS-NOTIFICATION-MIB-decodeTimeValue.include.snmptrap.rules"
            $cWNotificationUpdatedTimestamp = $decodedTimeValue
            $cWNotificationKey = $3
            $cWNotificationCategory = lookup($4, CWirelessNotificationCategory)
            $cWNotificationSubCategory = $5
            $cWNotificationManagedObjectAddressType = lookup($6, InetAddressType)
            $cWNotificationManagedObjectAddress = $7
            $cWNotificationSourceDisplayName = $8
            $cWNotificationDescription = $9
            $cWNotificationSeverity = lookup($10, CiscoAlarmSeverity) + " ( " + $10 + " ) "
            $cWNotificationSpecialAttributes = $11
            $cWNotificationVirtualDomains = $12

            $OS_EventId = "SNMPTRAP-cisco-CISCO-WIRELESS-NOTIFICATION-MIB-ciscoWirelessMOStatusNotification"
            
            @AlertGroup = $cWNotificationCategory

            if(!match($cWNotificationSubCategory, ""))
            {
                @AlertGroup = @AlertGroup + " ( " + $cWNotificationSubCategory + " )"
            }

            if(regmatch($cWNotificationSubCategory, "[Ll]ink [Dd]own") || regmatch($cWNotificationSubCategory, "[Ll]ink [Uu]p"))
            {
               @AlertGroup = $cWNotificationCategory + " ( " + "Link Status" + " )"
            }

            $cWNotificationIndex = extract($OID1, "\.([0-9]+)$")

            @AlertKey = "Src Name: " + $cWNotificationSourceDisplayName 
             
            if(!match($cWNotificationManagedObjectAddress, ""))
            {
                @AlertKey = @AlertKey +  ", Src " + $cWNotificationManagedObjectAddressType + ": " + $cWNotificationManagedObjectAddress
            }

            @Summary = $cWNotificationDescription

            switch($10)
            {
                case "1": ### cleared
                    $SEV_KEY = $OS_EventId + "_cleared"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                case "2": ### indeterminate
                    $SEV_KEY = $OS_EventId + "_indeterminate"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "3": ### critical
                    $SEV_KEY = $OS_EventId + "_critical"
                    $DEFAULT_Severity = 5
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "4": ### major
                    $SEV_KEY = $OS_EventId + "_major"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "5": ### minor
                    $SEV_KEY = $OS_EventId + "_minor"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "6": ### warning
                    $SEV_KEY = $OS_EventId + "_warning"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "7": ### info
                    $SEV_KEY = $OS_EventId + "_info"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800
                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                }

            update(@Severity) 

            $cWNotificationCategory = $cWNotificationCategory + " ( " + $4 + " ) "
            $cWNotificationManagedObjectAddressType = $cWNotificationManagedObjectAddressType + " ( " + $6 + " ) "

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cWNotificationTimestamp,$cWNotificationUpdatedTimestamp,$cWNotificationKey,$cWNotificationCategory,$cWNotificationSubCategory,$cWNotificationManagedObjectAddressType,$cWNotificationManagedObjectAddress,$cWNotificationSourceDisplayName,$cWNotificationDescription,$cWNotificationSeverity,$cWNotificationSpecialAttributes,$cWNotificationVirtualDomains)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cWNotificationTimestamp", $cWNotificationTimestamp, "cWNotificationUpdatedTimestamp", $cWNotificationUpdatedTimestamp, "cWNotificationKey", $cWNotificationKey,
                 "cWNotificationCategory", $cWNotificationCategory, "cWNotificationSubCategory", $cWNotificationSubCategory, "cWNotificationManagedObjectAddressType", $cWNotificationManagedObjectAddressType,
                 "cWNotificationManagedObjectAddress", $cWNotificationManagedObjectAddress, "cWNotificationSourceDisplayName", $cWNotificationSourceDisplayName, "cWNotificationDescription", $cWNotificationDescription,
                 "cWNotificationSeverity", $cWNotificationSeverity, "cWNotificationSpecialAttributes", $cWNotificationSpecialAttributes, "cWNotificationVirtualDomains", $cWNotificationVirtualDomains)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-WIRELESS-NOTIFICATION-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-WIRELESS-NOTIFICATION-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-WIRELESS-NOTIFICATION-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-WIRELESS-NOTIFICATION-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-WIRELESS-NOTIFICATION-MIB.include.snmptrap.rules >>>>>")
