###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
# Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
# - CISCO-TELEPRESENCE-CALL-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.644": ### Cisco MIB to manage a Telepresence call - Notifications from CISCO-TELEPRESENCE-CALL-MIB (200809170000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-TELEPRESENCE-CALL-MIB.include.snmptrap.rules >>>>>")

    @Agent = "CISCO-TELEPRESENCE-CALL"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### ctpcMgmtSysConnFailNotification

            ##########
            # $1 = ctpcMgmtSysAddrType 
            # $2 = ctpcMgmtSysAddr 
            ##########

            $ctpcMgmtSysAddrType = lookup($1, InetAddressType)
            $ctpcMgmtSysAddr = $2
            $ctpcMgmtSysIndex = extract($OID1, "\.([0-9]+)$")
            $OS_EventId = "SNMPTRAP-cisco-CISCO-TELEPRESENCE-CALL-MIB-ctpcMgmtSysConnFailNotification"

            @AlertGroup = "Management system connection failure"
            @AlertKey = "ctpcMgmtSysEntry." + $ctpcMgmtSysIndex 
            @Summary = "Management system connection failure occurs " +  $ctpcMgmtSysAddr + " ( " + @AlertKey + " )"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            $ctpcMgmtSysAddrType = $ctpcMgmtSysAddrType + " ( " + $1 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($ctpcMgmtSysAddrType,$ctpcMgmtSysAddr,$ctpcMgmtSysIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "ctpcMgmtSysAddrType", $ctpcMgmtSysAddrType, "ctpcMgmtSysAddr", $ctpcMgmtSysAddr, "ctpcMgmtSysIndex", $ctpcMgmtSysIndex)

        case "2": ### ctpcStatNotificaion

            ##########
            # $1 = ctpcStatEventMonObjectInst 
            # $2 = ctpcStatEventCrossedValue 
            # $3 = ctpcStatEventCrossedType 
            ##########

            $ctpcStatEventMonObjectInst = $1
            $ctpcStatEventCrossedValue = $2
            $ctpcStatEventCrossedType = lookup($3, ciscoCtpcStatThreshCrossedType)
            $ctpcStatEventHistoryIndex = extract($OID1, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-cisco-CISCO-TELEPRESENCE-CALL-MIB-ctpcStatNotificaion"

            @AlertGroup = "Call monitoring threshold is crossed"
            @AlertKey = "ctpcStatEventHistoryEntry." + $ctpcStatEventHistoryIndex 

            switch($3)
            {
                        case "1": ### risingThreshold
                            $SEV_KEY = $OS_EventId + "_risingThreshold"
                            @Summary = "Call monitoring threshold is crossed " + $ctpcStatEventMonObjectInst + " " + $ctpcStatEventCrossedValue + " " + $ctpcStatEventCrossedType + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 1
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0

                        case "2": ### fallingThreshold
                            $SEV_KEY = $OS_EventId + "_fallingThreshold"
                            @Summary = "Call monitoring threshold is crossed " + $ctpcStatEventMonObjectInst + " " + $ctpcStatEventCrossedValue + " " + $ctpcStatEventCrossedType + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        default:
                            $SEV_KEY = $OS_EventId + "_unknown"
                            @Summary = "Call monitoring threshold is crossed unknown" + $ctpcStatEventMonObjectInst + " " + " ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0
            }

            update(@Summary)
            update(@Severity)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            $ctpcStatEventCrossedType = $ctpcStatEventCrossedType + " ( " + $3 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($ctpcStatEventMonObjectInst,$ctpcStatEventCrossedValue,$ctpcStatEventCrossedType,$ctpcStatEventHistoryIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "ctpcStatEventMonObjectInst", $ctpcStatEventMonObjectInst, "ctpcStatEventCrossedValue", $ctpcStatEventCrossedValue, "ctpcStatEventCrossedType", $ctpcStatEventCrossedType,
                 "ctpcStatEventHistoryIndex", $ctpcStatEventHistoryIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-TELEPRESENCE-CALL-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-TELEPRESENCE-CALL-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-TELEPRESENCE-CALL-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-TELEPRESENCE-CALL-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-TELEPRESENCE-CALL-MIB.include.snmptrap.rules >>>>>")
