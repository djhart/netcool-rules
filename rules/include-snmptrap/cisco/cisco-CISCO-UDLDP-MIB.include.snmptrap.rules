###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-UDLDP-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.118": ###  - Notifications from CISCO-UDLDP-MIB (200911090900Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-UDLDP-MIB.include.snmptrap.rules >>>>>")

    @Agent = "CISCO-UDLDP-MIB"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### cudldpFastHelloLinkFailRptNotification

            ##########
            # $1 = ifName 
            ##########

            $ifName = $1

            $OS_EventId = "SNMPTRAP-cisco-CISCO-UDLDP-MIB-cudldpFastHelloLinkFailRptNotification"

            @AlertGroup = "Fast Hello UDLD Link Status"
            $ifIndex = extract($OID1, "\.([0-9]+)$")
            @AlertKey = "ifEntry." + $ifIndex
            @Summary = "UDLDP Detects a Link Failure On a Fast Hello UDLD Link" + " ( " + @AlertKey + " ) "  

            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($ifName,$ifIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "ifName", $ifName, "ifIndex", $ifIndex)

        case "2": ### cudldpFastHelloStatusChangeNotification

            ##########
            # $1 = cudldpHelloInterval 
            # $2 = cudldpIfFastHelloInterval 
            # $3 = cudldpIfOperHelloInterval 
            # $4 = cudldpIfFastHelloOperStatus 
            # $5 = ifName 
            ##########

            $cudldpHelloInterval = $1 + " seconds"
            $cudldpIfFastHelloInterval = $2 + " milliseconds"
            $cudldpIfOperHelloInterval = $3 + " milliseconds"
            $cudldpIfFastHelloOperStatus = lookup($4, CudldpIfFastHelloOperStatus)
            $ifName = $5

            $OS_EventId = "SNMPTRAP-cisco-CISCO-UDLDP-MIB-cudldpFastHelloStatusChangeNotification"

            @AlertGroup = "UDLD Fast Hello Operational Status"
            $ifIndex = extract($OID5, "\.([0-9]+)$")	    
            @AlertKey = "cudldpInterfaceEntry." + $ifIndex + ", ifEntry." + $ifIndex
            switch($4)
            {
                case "1":### operational
                    $SEV_KEY = $OS_EventId + "_operational"
                    @Summary = "UDLD Fast Hello Operational Status has Changed to Operational"

                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
 
                case "2":### notOperational
                    $SEV_KEY = $OS_EventId + "_notOperational"
                    @Summary = "UDLD Fast Hello Operational Status has Changed to Not Operational"

                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = "UDLD Fast Hello Operational Status Unknown"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            @Summary = @Summary + " ( " + @AlertKey + " ) "

            update(@Severity)
	    
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            $cudldpIfFastHelloOperStatus = $cudldpIfFastHelloOperStatus + " ( " + $4 + " ) " 
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cudldpHelloInterval,$cudldpIfFastHelloInterval,$cudldpIfOperHelloInterval,$cudldpIfFastHelloOperStatus,$ifName,$ifIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cudldpHelloInterval", $cudldpHelloInterval, "cudldpIfFastHelloInterval", $cudldpIfFastHelloInterval, "cudldpIfOperHelloInterval", $cudldpIfOperHelloInterval,
                 "cudldpIfFastHelloOperStatus", $cudldpIfFastHelloOperStatus, "ifName", $ifName, "ifIndex", $ifIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-UDLDP-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-UDLDP-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-UDLDP-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-UDLDP-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-UDLDP-MIB.include.snmptrap.rules >>>>>")
