################################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
# Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
# - CISCO-IF-EXTENSION
#
################################################################################

case ".1.3.6.1.4.1.9.9.276":  ### - ciscoIfExtensionMIB : Cisco Extension to the IF MIB - Notifications from CISCO-IF-EXTENSION-MIB(200707230000Z)


	log(DEBUG, "<<<<< Entering... cisco-CISCO-IF-EXTENSION-MIB.include.snmptrap.rules >>>>>")

	@Agent = "CISCO-IF EXTENSION"
	@Class = "40057"

	$OPTION_TypeFieldUsage = "3.6"

	switch($specific-trap) {
		case "1": ### - cieLinkDown
			#######################
			# $1 = ifIndex
			# $2 = ifAdminStatus
			# $3 = ifOperStatus
			# $4 = ifName
			# $5 = ifType
			#######################
			
			$ifIndex = $1
			$ifAdminStatus = lookup($2, ifAdminStatus) + " ( " + $2 + " )"
			$ifOperStatus = lookup ($3, ifOperStatus) + " ( " + $3 + " )"
			$ifName = $4
			$ifType = lookup($5, IANAifType) + " ( " + $5 + " )"


			$OS_EventId = "SNMPTRAP-cisco-CISCO-IF-EXTENSION-MIB-cieLinkDown"

			@AlertGroup = "CISCO IF EXTENSION Link Status"
			@AlertKey = "ifEntry." + $1

			switch ($3)
			{
				case "1": ### up
					@Summary = "Network Interface ( ifIndex = " + $1 + " ) Up"+   "  (  " + @AlertKey + " )"
					$SEV_KEY = $OS_EventId + "_up"
					$DEFAULT_Severity = 1
					$DEFAULT_Type = 2
					$DEFAULT_ExpireTime = 0
				case "2": ### down
					switch ($2)
					{
						case "1": ### up
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Down, should be Up"+   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operDown-adminUp"
							$DEFAULT_Severity = 4
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						case "2": ### down
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Administratively Down" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operDown-adminDown"
							$DEFAULT_Severity = 3
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						case "3": ### testing
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Down, should be Testing" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operDown-adminTesting"
							$DEFAULT_Severity = 2
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0					
						default:
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Down, admin status unknown  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operDown-adminUnknown"
							$DEFAULT_Severity = 2
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0	
					}
				case "3": ### testing
					switch ($2)
					{
						case "1": ### up
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Testing, should be Up" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operTesting-adminUp"
							$DEFAULT_Severity = 4
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						case "2": ### down
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Testing, should be Down" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operTesting-adminDown"
							$DEFAULT_Severity = 3
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						case "3": ### testing
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Administratively Testing" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operTesting-adminTesting"
							$DEFAULT_Severity = 2
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						default:
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Testing, admin status unknown  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operTesting-adminUnknown"
							$DEFAULT_Severity = 2
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0							
					}
				case "4": ### unknown
					switch ($2)
					{
						case "1": ### up
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Unknown, should be Up" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operUnknown-adminUp"
							$DEFAULT_Severity = 4
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						case "2": ### down
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Unknown, should be Down" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operUnknown-adminDown"
							$DEFAULT_Severity = 3
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						case "3": ### testing
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Administratively Testing" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operUnknown-adminTesting"
							$DEFAULT_Severity = 2
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						default:
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) operational and admin status unknown  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operUnknown-adminUnknown"
							$DEFAULT_Severity = 2
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0							
					}
				case "5": ### dormant
					switch ($2)
					{
						case "1": ### up
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Dormant, should be Up" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operDormant-adminUp"
							$DEFAULT_Severity = 4
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						case "2": ### down
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Dormant, should be Down" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operDormant-adminDown"
							$DEFAULT_Severity = 3
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						case "3": ### testing
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Administratively Testing" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operDormant-adminTesting"
							$DEFAULT_Severity = 2
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						default:
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Dormant, admin status unknown  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operDormant-adminUnknown"
							$DEFAULT_Severity = 2
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0							
					}		
				case "6": ### notPresent
					switch ($2)
					{
						case "1": ### up
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Not Present, should be Up" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operNotPresent-adminUp"
							$DEFAULT_Severity = 4
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						case "2": ### down
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Not Present, should be Down" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operNotPresent-adminDown"
							$DEFAULT_Severity = 3
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						case "3": ### testing
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Administratively Testing" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operNotPresent-adminTesting"
							$DEFAULT_Severity = 2
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						default:
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Not Present, admin status unknown  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operNotPresent-adminUnknown"
							$DEFAULT_Severity = 2
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0							
                    }	
                case "7": ### lowerLayerDown
                    switch ($2)
                    {
						case "1": ### up
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Lower-Layer Interface down, should be Up" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operLowerLayerDown-adminUp"
							$DEFAULT_Severity = 4
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						case "2": ### down
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Lower-Layer Interface down, should be Down" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operLowerLayerDown-adminDown"
							$DEFAULT_Severity = 3
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						case "3": ### testing
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Administratively Testing" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operLowerLayerDown-adminTesting"
							$DEFAULT_Severity = 2
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						default:
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Lower-layer Interface down, admin status unknown  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operLowerLayerDown-adminUnknown"
							$DEFAULT_Severity = 2
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0							
					}						
				default:
					@Summary = "Network Interface ( ifIndex = " + $1 + " ) operational status unknown  (  " + @AlertKey + " )"
					$SEV_KEY = $OS_EventId + "_operUnknown"
					$DEFAULT_Severity = 2
					$DEFAULT_Type = 1
					$DEFAULT_ExpireTime = 0
			}      


			@Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + $ifName + $ifType
			
			if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
    			details($ifIndex,$ifAdminStatus,$ifOperStatus,$ifName,$ifType)
			}
			@ExtendedAttr = nvp_add(@ExtendedAttr, "ifIndex", $ifIndex, "ifAdminStatus", $ifAdminStatus, "ifOperStatus", $ifOperStatus,
			     "ifName", $ifName, "ifType", $ifType)

		case "2": ### - cieLinkUp
			#######################
			# $1 = ifIndex
			# $2 = ifAdminStatus
			# $3 = ifOperStatus
			# $4 = ifName
			# $5 = ifType
			##########################################
			$ifIndex = $1
			$ifAdminStatus = lookup($2, ifAdminStatus) + " ( " + $2 + " )"
			$ifOperStatus = lookup ($3, ifOperStatus) + " ( " + $3 + " )"
			$ifName = $4
			$ifType = lookup($5, IANAifType) + " ( " + $5 + " )"

			$OS_EventId = "SNMPTRAP-cisco-CISCO-IF-EXTENSION-MIB-cieLinkUp"

			@AlertGroup = "CISCO IF EXTENSION Link Status"

			@AlertKey = "ifEntry." + $1
			
			switch ($3)
			{
				case "1": ### up
						@Summary = "Network Interface ( ifIndex = " + $1 + " ) Up" +   "  (  " + @AlertKey + " )"
						$SEV_KEY = $OS_EventId + "_up"
						$DEFAULT_Severity = 1
						$DEFAULT_Type = 2
						$DEFAULT_ExpireTime = 0
				case "2": ### down
					switch ($2)
					{
						case "1": ### up
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Down, should be Up" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operDown-adminUp"
							$DEFAULT_Severity = 4
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						case "2": ### down
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Administratively Down" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operDown-adminDown"
							$DEFAULT_Severity = 3
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						case "3": ### testing
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Down, should be Testing" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operDown-adminTesting"
							$DEFAULT_Severity = 2
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						default:
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Down, admin status unknown  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operDown-adminUnknown"
							$DEFAULT_Severity = 2
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0							
					}
				case "3": ### testing
					switch ($2)
					{
						case "1": ### up
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Testing, should be Up" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operTesting-adminUp"
							$DEFAULT_Severity = 4
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						case "2": ### down
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Testing, should be Down" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operTesting-adminDown"
							$DEFAULT_Severity = 3
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						case "3": ### testing
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Administratively Testing" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operTesting-adminTesting"
							$DEFAULT_Severity = 2
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0                            
						default:
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Testing, admin status unknown  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operTesting-adminUnknown"
							$DEFAULT_Severity = 2
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0							
					}
				case "4": ### unknown
					switch ($2)
					{
						case "1": ### up
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Unknown, should be Up" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operUnknown-adminUp"
							$DEFAULT_Severity = 4
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						case "2": ### down
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Unknown, should be Down" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operUnknown-adminDown"
							$DEFAULT_Severity = 3
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						case "3": ### testing
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Administratively Testing" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operUnknown-adminTesting"
							$DEFAULT_Severity = 2
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						default:
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) operational and admin status unknown  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operUnknown-adminUnknown"
							$DEFAULT_Severity = 2
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0							
					}
				case "5": ### dormant
					switch ($2)
					{
						case "1": ### up
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Dormant, should be Up" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operDormant-adminUp"
							$DEFAULT_Severity = 4
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						case "2": ### down
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Dormant, should be Down" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operDormant-adminDown"
							$DEFAULT_Severity = 3
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						case "3": ### testing
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Administratively Testing" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operDormant-adminTesting"
							$DEFAULT_Severity = 2
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						default:
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Dormant, admin status unknown  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operDormant-adminUnknown"
							$DEFAULT_Severity = 2
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0							
					}		
				case "6": ### notPresent
					switch ($2)
					{
						case "1": ### up
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Not Present, should be Up" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operNotPresent-adminUp"
							$DEFAULT_Severity = 4
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						case "2": ### down
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Not Present, should be Down" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operNotPresent-adminDown"
							$DEFAULT_Severity = 3
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						case "3": ### testing
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Administratively Testing" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operNotPresent-adminTesting"
							$DEFAULT_Severity = 2
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						default:
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Not Present, admin status unknown  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operNotPresent-adminUnknown"
							$DEFAULT_Severity = 2
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0							
					}	
				case "7": ### lowerLayerDown
					switch ($2)
					{
						case "1": ### up
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Lower-Layer Interface down, should be Up" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operLowerLayerDown-adminUp"
							$DEFAULT_Severity = 4
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						case "2": ### down
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Lower-Layer Interface down, should be Down" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operLowerLayerDown-adminDown"
							$DEFAULT_Severity = 3
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						case "3": ### testing
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Administratively Testing" +   "  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operLowerLayerDown-adminTesting"
							$DEFAULT_Severity = 2
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0
						default:
							@Summary = "Network Interface ( ifIndex = " + $1 + " ) Lower-Layer Interface Down, admin status unknown  (  " + @AlertKey + " )"
							$SEV_KEY = $OS_EventId + "_operLowerLayerDown-adminUnknown"
							$DEFAULT_Severity = 2
							$DEFAULT_Type = 1
							$DEFAULT_ExpireTime = 0							
					}											
				default:
					@Summary = "Network Interface ( ifIndex = " + $1 + " ) operational status unknown  (  " + @AlertKey + " )"
					$SEV_KEY = $OS_EventId + "_operUnknown"
					$DEFAULT_Severity = 2
					$DEFAULT_Type = 1
					$DEFAULT_ExpireTime = 0				
			}


			@Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + $ifName + $ifType
			
			if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
    			details($ifIndex,$ifAdminStatus,$ifOperStatus,$ifName,$ifType)
			}
			@ExtendedAttr = nvp_add(@ExtendedAttr, "ifIndex", $ifIndex, "ifAdminStatus", $ifAdminStatus, "ifOperStatus", $ifOperStatus,
			     "ifName", $ifName, "ifType", $ifType)

		default:
			@Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
			@Severity = 1
			@Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
			if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
    			details($*)
			}
			@ExtendedAttr = nvp_add($*)
	}

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
	[$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-IF-EXTENSION-MIB_sev)
}
else
{
	[$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-IF-EXTENSION-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-IF-EXTENSION-MIB.adv.include.snmptrap.rules"

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-IF-EXTENSION-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-IF-EXTENSION-MIB.include.snmptrap.rules >>>>>")



