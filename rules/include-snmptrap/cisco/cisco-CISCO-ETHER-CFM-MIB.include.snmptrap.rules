###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 1.0 - Initial Release.
#
# Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
# - CISCO-ETHER-CFM-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.461": ###  - Notifications from CISCO-ETHER-CFM-MIB (200412280000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-ETHER-CFM-MIB.include.snmptrap.rules >>>>>")

    @Agent = "CISCO-ETHER-CFM-MIB"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### cEtherCfmCcMepUp

            ##########
            # $1 = cEtherCfmEventServiceId 
            # $2 = cEtherCfmEventLclMacAddress 
            # $3 = cEtherCfmEventLclMepCount 
            # $4 = cEtherCfmEventLclIfCount 
            # $5 = cEtherCfmEventRmtMepid 
            # $6 = cEtherCfmEventRmtMacAddress 
            # $7 = cEtherCfmEventCode 
            # $8 = cEtherCfmEventRmtPortState 
            ##########

            $cEtherCfmEventServiceId = $1
            $cEtherCfmEventLclMacAddress = $2
            $cEtherCfmEventLclMepCount = $3
            $cEtherCfmEventLclIfCount = $4
            $cEtherCfmEventRmtMepid = $5
            $cEtherCfmEventRmtMacAddress = $6
            $cEtherCfmEventCode = lookup($7, cEtherCfmEventCode) + " ( " + $7 + " )"
            $cEtherCfmEventRmtPortState = lookup($8, cEtherCfmEventRmtPortState) + " ( " + $8 + " )"

            $cEtherCfmEventDomainIndex = extract($OID1, "\.([0-9]+)\.[0-9]+\.[0-9]+$")
            $cEtherCfmEventSvlan = extract($OID1, "\.([0-9]+)\.[0-9]+$")
            $cEtherCfmEventIndex = extract($OID1, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-cisco-CISCO-ETHER-CFM-MIB-cEtherCfmCcMepUp"

            @AlertGroup = "MEP Status"
            @AlertKey = "cEtherCfmEventEntry." + $cEtherCfmEventDomainIndex + "." + $cEtherCfmEventSvlan + "." + $cEtherCfmEventIndex

            switch($7)
            {
                case "1": ### new
                    $SEV_KEY = $OS_EventId + "_new"
                    @Summary = "Device: " + $cEtherCfmEventLclMacAddress + " receives a CC message from the Remote MEP: " + $cEtherCfmEventRmtMacAddress + " for the first time ( " + @AlertKey + " )"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "2": ### returning
                    $SEV_KEY = $OS_EventId + "_returning"
                    @Summary = "Device: " + $cEtherCfmEventLclMacAddress + " received a CC message from the Remote MEP: " + $cEtherCfmEventRmtMacAddress + " for which it had expired CCDB Entry ( " + @AlertKey + " )"

                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0

                case "3": ### portState
                    $SEV_KEY = $OS_EventId + "_portState"
                    @Summary = "Device: " + $cEtherCfmEventLclMacAddress + " received a CC message from the Remote MEP: " + $cEtherCfmEventRmtMacAddress + " for which it had valid CCDB Entry and it indicates the port state has changed to : " 

                    @AlertGroup = @AlertGroup + "-Port State Change"
                    switch($8)
                    {
                        case "1": ### up
                            $SEV_KEY = $SEV_KEY + "_up"
                            @Summary = @Summary + "up ( " + @AlertKey + " )"

                            $DEFAULT_Severity = 1
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0

                        case "2": ### down
                            $SEV_KEY = $SEV_KEY + "_down"
                            @Summary = @Summary + "down ( " + @AlertKey + " )"

                            $DEFAULT_Severity = 4
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "3": ### adminDown
                            $SEV_KEY = $SEV_KEY + "_adminDown"
                            @Summary = @Summary + "administratively down ( " + @AlertKey + " )"

                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "4": ### test
                            $SEV_KEY = $SEV_KEY + "_test"
                            @Summary = @Summary + "test mode ( " + @AlertKey + " )"

                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "5": ### remoteExcessiveErrors
                            $SEV_KEY = $SEV_KEY + "_remoteExcessiveErrors"
                            @Summary = @Summary + "remoteExcessiveErrors, that is other end of the link is receiving an excessive number of invalid frames ( " + @AlertKey + " )"

                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                       case "6": ### localExcessiveErrors
                            $SEV_KEY = $SEV_KEY + "_localExcessiveErrors"
                            @Summary = @Summary + "localExcessiveErrors, that is this end of the link is receiving an excessive number of invalid frames ( " + @AlertKey + " )"

                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                      case "7": ### localNoData
                            $SEV_KEY = $SEV_KEY + "_localNoData"
                            @Summary = @Summary + "localNoData, that is No Data and CFM messages have been received for an excessive length of time ( " + @AlertKey + " )"

                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                      default: ### unknown
                            $SEV_KEY = $SEV_KEY + "_unknown"
                            @Summary = @Summary + "unknown ( " + @AlertKey + " )"

                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                   }

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = "MEP Status unknown"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            update(@Summary)
            update(@Severity)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cEtherCfmEventServiceId,$cEtherCfmEventLclMacAddress,$cEtherCfmEventLclMepCount,$cEtherCfmEventLclIfCount,$cEtherCfmEventRmtMepid,$cEtherCfmEventRmtMacAddress,$cEtherCfmEventCode,$cEtherCfmEventRmtPortState,$cEtherCfmEventDomainIndex,$cEtherCfmEventSvlan,$cEtherCfmEventIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cEtherCfmEventServiceId", $cEtherCfmEventServiceId, "cEtherCfmEventLclMacAddress", $cEtherCfmEventLclMacAddress, "cEtherCfmEventLclMepCount", $cEtherCfmEventLclMepCount,
                 "cEtherCfmEventLclIfCount", $cEtherCfmEventLclIfCount, "cEtherCfmEventRmtMepid", $cEtherCfmEventRmtMepid, "cEtherCfmEventRmtMacAddress", $cEtherCfmEventRmtMacAddress,
                 "cEtherCfmEventCode", $cEtherCfmEventCode, "cEtherCfmEventRmtPortState", $cEtherCfmEventRmtPortState, "cEtherCfmEventDomainIndex", $cEtherCfmEventDomainIndex,
                 "cEtherCfmEventSvlan", $cEtherCfmEventSvlan, "cEtherCfmEventIndex", $cEtherCfmEventIndex)

        case "2": ### cEtherCfmCcMepDown

            ##########
            # $1 = cEtherCfmEventServiceId 
            # $2 = cEtherCfmEventLclMacAddress 
            # $3 = cEtherCfmEventLclMepCount 
            # $4 = cEtherCfmEventLclIfCount 
            # $5 = cEtherCfmEventRmtMepid 
            # $6 = cEtherCfmEventRmtMacAddress 
            # $7 = cEtherCfmEventCode 
            ##########

            $cEtherCfmEventServiceId = $1
            $cEtherCfmEventLclMacAddress = $2
            $cEtherCfmEventLclMepCount = $3
            $cEtherCfmEventLclIfCount = $4
            $cEtherCfmEventRmtMepid = $5
            $cEtherCfmEventRmtMacAddress = $6
            $cEtherCfmEventCode = lookup($7, cEtherCfmEventCode) + " ( " + $7 + " )"

            $cEtherCfmEventDomainIndex = extract($OID1, "\.([0-9]+)\.[0-9]+\.[0-9]+$")
            $cEtherCfmEventSvlan = extract($OID1, "\.([0-9]+)\.[0-9]+$")
            $cEtherCfmEventIndex = extract($OID1, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-cisco-CISCO-ETHER-CFM-MIB-cEtherCfmCcMepDown"

            @AlertGroup = "MEP Status"
            @AlertKey = "cEtherCfmEventEntry." + $cEtherCfmEventDomainIndex + "." + $cEtherCfmEventSvlan + "." + $cEtherCfmEventIndex

            switch($7)
            {
                case "4": ### lastGasp
                    $SEV_KEY = $OS_EventId + "_lastGasp"
                    @Summary = "Device: " + $cEtherCfmEventLclMacAddress + " received a CC message from the Remote MEP: " + $cEtherCfmEventRmtMacAddress + " with zero lifetime ( " + @AlertKey + " )"

                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "5": ### timeout
                    $SEV_KEY = $OS_EventId + "_timeout"
                    @Summary = "Local CCDB entry for the remote MEP : " + $cEtherCfmEventRmtMacAddress + " has expired ( " + @AlertKey + " )"

                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "6": ### configClear
                    $SEV_KEY = $OS_EventId + "_configClear"
                    @Summary = "Configuration Error CC Message has been cleared ( " + @AlertKey + " )"

                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0

                case "7": ### loopClear
                    $SEV_KEY = $OS_EventId + "_loopClear"
                    @AlertGroup = "MEP Loop Error Status"
                    @Summary = "Loop Error CC Message has been cleared ( " + @AlertKey + " )"

                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0

                case "8": ### xconnectClear
                    $SEV_KEY = $OS_EventId + "_xconnectClear"
                    @AlertGroup = "MEP Crossconnect Error Status"
                    @Summary = "Cross Connect Error CC Message has been cleared ( " + @AlertKey + " )"

                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0

                case "9": ### unknownClear
                    $SEV_KEY = $OS_EventId + "_unknownClear"
                    @Summary = "Unknown Error CC Message has been cleared ( " + @AlertKey + " )"

                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0

                default: 
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = "MEP Status unknown"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

              }

            update(@Summary)
 
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cEtherCfmEventServiceId,$cEtherCfmEventLclMacAddress,$cEtherCfmEventLclMepCount,$cEtherCfmEventLclIfCount,$cEtherCfmEventRmtMepid,$cEtherCfmEventRmtMacAddress,$cEtherCfmEventCode,$cEtherCfmEventDomainIndex,$cEtherCfmEventSvlan,$cEtherCfmEventIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cEtherCfmEventServiceId", $cEtherCfmEventServiceId, "cEtherCfmEventLclMacAddress", $cEtherCfmEventLclMacAddress, "cEtherCfmEventLclMepCount", $cEtherCfmEventLclMepCount,
                 "cEtherCfmEventLclIfCount", $cEtherCfmEventLclIfCount, "cEtherCfmEventRmtMepid", $cEtherCfmEventRmtMepid, "cEtherCfmEventRmtMacAddress", $cEtherCfmEventRmtMacAddress,
                 "cEtherCfmEventCode", $cEtherCfmEventCode, "cEtherCfmEventDomainIndex", $cEtherCfmEventDomainIndex, "cEtherCfmEventSvlan", $cEtherCfmEventSvlan,
                 "cEtherCfmEventIndex", $cEtherCfmEventIndex)

        case "3": ### cEtherCfmCcCrossconnect

            ##########
            # $1 = cEtherCfmEventServiceId 
            # $2 = cEtherCfmEventLclMacAddress 
            # $3 = cEtherCfmEventRmtMepid 
            # $4 = cEtherCfmEventRmtMacAddress 
            # $5 = cEtherCfmEventRmtServiceId 
            ##########

            $cEtherCfmEventServiceId = $1
            $cEtherCfmEventLclMacAddress = $2
            $cEtherCfmEventRmtMepid = $3
            $cEtherCfmEventRmtMacAddress = $4
            $cEtherCfmEventRmtServiceId = $5

            $OS_EventId = "SNMPTRAP-cisco-CISCO-ETHER-CFM-MIB-cEtherCfmCcCrossconnect"

            $cEtherCfmEventDomainIndex = extract($OID1, "\.([0-9]+)\.[0-9]+\.[0-9]+$")
            $cEtherCfmEventSvlan = extract($OID1, "\.([0-9]+)\.[0-9]+$")
            $cEtherCfmEventIndex = extract($OID1, "\.([0-9]+)$")

            @AlertGroup = "MEP Crossconnect Error Status"
            @AlertKey = "cEtherCfmEventEntry." + $cEtherCfmEventDomainIndex + "." + $cEtherCfmEventSvlan + "." + $cEtherCfmEventIndex
            @Summary = "Cross Connect Error CC Message ( " + @AlertKey + " )"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cEtherCfmEventServiceId,$cEtherCfmEventLclMacAddress,$cEtherCfmEventRmtMepid,$cEtherCfmEventRmtMacAddress,$cEtherCfmEventRmtServiceId,$cEtherCfmEventDomainIndex,$cEtherCfmEventSvlan,$cEtherCfmEventIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cEtherCfmEventServiceId", $cEtherCfmEventServiceId, "cEtherCfmEventLclMacAddress", $cEtherCfmEventLclMacAddress, "cEtherCfmEventRmtMepid", $cEtherCfmEventRmtMepid,
                 "cEtherCfmEventRmtMacAddress", $cEtherCfmEventRmtMacAddress, "cEtherCfmEventRmtServiceId", $cEtherCfmEventRmtServiceId, "cEtherCfmEventDomainIndex", $cEtherCfmEventDomainIndex,
                 "cEtherCfmEventSvlan", $cEtherCfmEventSvlan, "cEtherCfmEventIndex", $cEtherCfmEventIndex)

        case "4": ### cEtherCfmCcLoop

            ##########
            # $1 = cEtherCfmEventServiceId 
            # $2 = cEtherCfmEventLclMacAddress 
            # $3 = cEtherCfmEventLclMepid 
            ##########

            $cEtherCfmEventServiceId = $1
            $cEtherCfmEventLclMacAddress = $2
            $cEtherCfmEventLclMepid = $3

            $OS_EventId = "SNMPTRAP-cisco-CISCO-ETHER-CFM-MIB-cEtherCfmCcLoop"

            $cEtherCfmEventDomainIndex = extract($OID1, "\.([0-9]+)\.[0-9]+\.[0-9]+$")
            $cEtherCfmEventSvlan = extract($OID1, "\.([0-9]+)\.[0-9]+$")
            $cEtherCfmEventIndex = extract($OID1, "\.([0-9]+)$")

            @AlertGroup = "MEP Loop Error Status"
            @AlertKey = "cEtherCfmEventEntry." + $cEtherCfmEventDomainIndex + "." + $cEtherCfmEventSvlan + "." + $cEtherCfmEventIndex
            @Summary = "Loop Error CC Message, Device receives a CC message with the same MEPID: " + $cEtherCfmEventLclMepid + " and MAC Address: " + $cEtherCfmEventLclMacAddres + " ( " + @AlertKey + " )"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cEtherCfmEventServiceId,$cEtherCfmEventLclMacAddress,$cEtherCfmEventLclMepid,$cEtherCfmEventDomainIndex,$cEtherCfmEventSvlan,$cEtherCfmEventIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cEtherCfmEventServiceId", $cEtherCfmEventServiceId, "cEtherCfmEventLclMacAddress", $cEtherCfmEventLclMacAddress, "cEtherCfmEventLclMepid", $cEtherCfmEventLclMepid,
                 "cEtherCfmEventDomainIndex", $cEtherCfmEventDomainIndex, "cEtherCfmEventSvlan", $cEtherCfmEventSvlan, "cEtherCfmEventIndex", $cEtherCfmEventIndex)

        case "5": ### cEtherCfmCcConfigError

            ##########
            # $1 = cEtherCfmEventServiceId 
            # $2 = cEtherCfmEventLclMacAddress 
            # $3 = cEtherCfmEventLclMepid 
            # $4 = cEtherCfmEventRmtMacAddress 
            ##########

            $cEtherCfmEventServiceId = $1
            $cEtherCfmEventLclMacAddress = $2
            $cEtherCfmEventLclMepid = $3
            $cEtherCfmEventRmtMacAddress = $4

            $OS_EventId = "SNMPTRAP-cisco-CISCO-ETHER-CFM-MIB-cEtherCfmCcConfigError"

            $cEtherCfmEventDomainIndex = extract($OID1, "\.([0-9]+)\.[0-9]+\.[0-9]+$")
            $cEtherCfmEventSvlan = extract($OID1, "\.([0-9]+)\.[0-9]+$")
            $cEtherCfmEventIndex = extract($OID1, "\.([0-9]+)$")

            @AlertGroup = "MEP Config Error Status"
            @AlertKey = "cEtherCfmEventEntry." + $cEtherCfmEventDomainIndex + "." + $cEtherCfmEventSvlan + "." + $cEtherCfmEventIndex
            @Summary = "Configuration Error CC Message, Device receives a CC message with the same MEPID: " + $cEtherCfmEventLclMepid + " but different MAC Address: " + $cEtherCfmEventLclMacAddres + " ( " + @AlertKey + " )"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cEtherCfmEventServiceId,$cEtherCfmEventLclMacAddress,$cEtherCfmEventLclMepid,$cEtherCfmEventRmtMacAddress,$cEtherCfmEventDomainIndex,$cEtherCfmEventSvlan,$cEtherCfmEventIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cEtherCfmEventServiceId", $cEtherCfmEventServiceId, "cEtherCfmEventLclMacAddress", $cEtherCfmEventLclMacAddress, "cEtherCfmEventLclMepid", $cEtherCfmEventLclMepid,
                 "cEtherCfmEventRmtMacAddress", $cEtherCfmEventRmtMacAddress, "cEtherCfmEventDomainIndex", $cEtherCfmEventDomainIndex, "cEtherCfmEventSvlan", $cEtherCfmEventSvlan,
                 "cEtherCfmEventIndex", $cEtherCfmEventIndex)

        case "6": ### cEtherCfmXCheckMissing

            ##########
            # $1 = cEtherCfmEventServiceId 
            # $2 = cEtherCfmEventLclMacAddress 
            # $3 = cEtherCfmEventRmtMepid 
            # $4 = cEtherCfmEventRmtMacAddress 
            ##########

            $cEtherCfmEventServiceId = $1
            $cEtherCfmEventLclMacAddress = $2
            $cEtherCfmEventRmtMepid = $3
            $cEtherCfmEventRmtMacAddress = $4

            $OS_EventId = "SNMPTRAP-cisco-CISCO-ETHER-CFM-MIB-cEtherCfmXCheckMissing"

            $cEtherCfmEventDomainIndex = extract($OID1, "\.([0-9]+)\.[0-9]+\.[0-9]+$")
            $cEtherCfmEventSvlan = extract($OID1, "\.([0-9]+)\.[0-9]+$")
            $cEtherCfmEventIndex = extract($OID1, "\.([0-9]+)$")

            @AlertGroup = "MEP Cross-check Missing Error Status"
            @AlertKey = "cEtherCfmEventEntry." + $cEtherCfmEventDomainIndex + "." + $cEtherCfmEventSvlan + "." + $cEtherCfmEventIndex
            @Summary = "Configured MEP: " + $cEtherCfmEventLclMacAddress + " does not come up during the Cross-check start timeout interval ( " + @AlertKey + " )"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cEtherCfmEventServiceId,$cEtherCfmEventLclMacAddress,$cEtherCfmEventRmtMepid,$cEtherCfmEventRmtMacAddress,$cEtherCfmEventDomainIndex,$cEtherCfmEventSvlan,$cEtherCfmEventIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cEtherCfmEventServiceId", $cEtherCfmEventServiceId, "cEtherCfmEventLclMacAddress", $cEtherCfmEventLclMacAddress, "cEtherCfmEventRmtMepid", $cEtherCfmEventRmtMepid,
                 "cEtherCfmEventRmtMacAddress", $cEtherCfmEventRmtMacAddress, "cEtherCfmEventDomainIndex", $cEtherCfmEventDomainIndex, "cEtherCfmEventSvlan", $cEtherCfmEventSvlan,
                 "cEtherCfmEventIndex", $cEtherCfmEventIndex)

        case "7": ### cEtherCfmXCheckUnknown

            ##########
            # $1 = cEtherCfmEventServiceId 
            # $2 = cEtherCfmEventLclMacAddress 
            # $3 = cEtherCfmEventRmtMepid 
            # $4 = cEtherCfmEventRmtMacAddress 
            ##########

            $cEtherCfmEventServiceId = $1
            $cEtherCfmEventLclMacAddress = $2
            $cEtherCfmEventRmtMepid = $3
            $cEtherCfmEventRmtMacAddress = $4

            $OS_EventId = "SNMPTRAP-cisco-CISCO-ETHER-CFM-MIB-cEtherCfmXCheckUnknown"

            $cEtherCfmEventDomainIndex = extract($OID1, "\.([0-9]+)\.[0-9]+\.[0-9]+$")
            $cEtherCfmEventSvlan = extract($OID1, "\.([0-9]+)\.[0-9]+$")
            $cEtherCfmEventIndex = extract($OID1, "\.([0-9]+)$")

            @AlertGroup = "MEP Status"
            @AlertKey = "cEtherCfmEventEntry." + $cEtherCfmEventDomainIndex + "." + $cEtherCfmEventSvlan + "." + $cEtherCfmEventIndex
            @Summary = "Unexpected MEP: " + $cEtherCfmEventLclMacAddress + " comes up ( " + @AlertKey + " )"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cEtherCfmEventServiceId,$cEtherCfmEventLclMacAddress,$cEtherCfmEventRmtMepid,$cEtherCfmEventRmtMacAddress,$cEtherCfmEventDomainIndex,$cEtherCfmEventSvlan,$cEtherCfmEventIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cEtherCfmEventServiceId", $cEtherCfmEventServiceId, "cEtherCfmEventLclMacAddress", $cEtherCfmEventLclMacAddress, "cEtherCfmEventRmtMepid", $cEtherCfmEventRmtMepid,
                 "cEtherCfmEventRmtMacAddress", $cEtherCfmEventRmtMacAddress, "cEtherCfmEventDomainIndex", $cEtherCfmEventDomainIndex, "cEtherCfmEventSvlan", $cEtherCfmEventSvlan,
                 "cEtherCfmEventIndex", $cEtherCfmEventIndex)

        case "8": ### cEtherCfmXCheckServiceUp

            ##########
            # $1 = cEtherCfmEventServiceId 
            # $2 = cEtherCfmEventLclMacAddress 
            ##########

            $cEtherCfmEventServiceId = $1
            $cEtherCfmEventLclMacAddress = $2

            $OS_EventId = "SNMPTRAP-cisco-CISCO-ETHER-CFM-MIB-cEtherCfmXCheckServiceUp"

            $cEtherCfmEventDomainIndex = extract($OID1, "\.([0-9]+)\.[0-9]+\.[0-9]+$")
            $cEtherCfmEventSvlan = extract($OID1, "\.([0-9]+)\.[0-9]+$")
            $cEtherCfmEventIndex = extract($OID1, "\.([0-9]+)$")

            @AlertGroup = "MEP Cross-check Missing Error Status"
            @AlertKey = "cEtherCfmEventEntry." + $cEtherCfmEventDomainIndex + "." + $cEtherCfmEventSvlan + "." + $cEtherCfmEventIndex
            @Summary = "Configured MEP: " + $cEtherCfmEventLclMacAddress + " come up before the expiration of the Cross-check start timeout interval ( " + @AlertKey + " )"

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cEtherCfmEventServiceId,$cEtherCfmEventLclMacAddress,$cEtherCfmEventDomainIndex,$cEtherCfmEventSvlan,$cEtherCfmEventIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cEtherCfmEventServiceId", $cEtherCfmEventServiceId, "cEtherCfmEventLclMacAddress", $cEtherCfmEventLclMacAddress, "cEtherCfmEventDomainIndex", $cEtherCfmEventDomainIndex,
                 "cEtherCfmEventSvlan", $cEtherCfmEventSvlan, "cEtherCfmEventIndex", $cEtherCfmEventIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-ETHER-CFM-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-ETHER-CFM-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-ETHER-CFM-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-ETHER-CFM-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-ETHER-CFM-MIB.include.snmptrap.rules >>>>>")
