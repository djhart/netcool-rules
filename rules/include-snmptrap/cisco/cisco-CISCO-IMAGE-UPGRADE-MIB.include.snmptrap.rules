###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 1.1 - Updated Release for MIB Version 201103280000Z
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-IMAGE-UPGRADE-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.360": ### - Notifications from CISCO-IMAGE-UPGRADE-MIB (201103280000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-IMAGE-UPGRADE-MIB.include.snmptrap.rules >>>>>")

    @Agent = "cisco-CISCO-IMAGE-UPGRADE-MIB"
    @Class = "40057"
    
    $OPTION_TypeFieldUsage = "3.6"
    

    switch($specific-trap)
    {
    
     
        case "1": ### ciuUpgradeOpCompletionNotify
        
            ##########
            # $1 = ciuUpgradeOpCommand
            # $2 = ciuUpgradeOpStatus
            # $3 = ciuUpgradeOpTimeCompleted
            # $4 = ciuUpgradeOpLastCommand
            # $5 = ciuUpgradeOpLastStatus
            ##########
            
            $ciuUpgradeOpCommand = lookup($1, CiuUpgradeOpCommand)
            $ciuUpgradeOpStatus = lookup($2, CiuUpgradeOpStatus)
            $ciuUpgradeOpTimeCompleted = $3
            $ciuUpgradeOpLastCommand = lookup($4, CiuUpgradeOpLastCommand)
            $ciuUpgradeOpLastStatus = lookup($5, CiuUpgradeOpLastStatus)
            
            $OS_EventId = "SNMPTRAP-cisco-CISCO-IMAGE-UPGRADE-MIB-ciuUpgradeOpCompletionNotify"

            @AlertGroup = "Image " + $ciuUpgradeOpCommand + " " + "Status"
            @Summary = "Image " + $ciuUpgradeOpCommand + " " + "Operation" + " " + $ciuUpgradeOpStatus
            
            switch($2)
            {
                case "1":### none
                    $SEV_KEY = $OS_EventId + "_none"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "2":### invalidOperation
                    $SEV_KEY = $OS_EventId + "_invalidOperation"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "3":### failure
                    $SEV_KEY = $OS_EventId + "_failure"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "4":### inProgress
                    $SEV_KEY = $OS_EventId + "_inProgress"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 12
                    $DEFAULT_ExpireTime = 0               
                case "5":### success
                    $SEV_KEY = $OS_EventId + "_success"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0               
                case "6":### abortInProgress
                    $SEV_KEY = $OS_EventId + "_abortInProgress"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 12
                    $DEFAULT_ExpireTime = 0               
                case "7":### abortSuccess
                    $SEV_KEY = $OS_EventId + "_abortSuccess"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 12
                    $DEFAULT_ExpireTime = 0               
                case "8":### abortFailed
                    $SEV_KEY = $OS_EventId + "_abortFailed"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "9":### successReset
                    $SEV_KEY = $OS_EventId + "_successReset"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0               
                case "10":### fsUpgReset
                    $SEV_KEY = $OS_EventId + "_fsUpgReset"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800               
                default: 
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = "Image " + $ciuUpgradeOpCommand + " " + "Operation Status Unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            @Identifier = @Node + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $ciuUpgradeOpCommand = $ciuUpgradeOpCommand + " ( " + $1 + " )"
            $ciuUpgradeOpStatus = $ciuUpgradeOpStatus + " ( " + $2 + " )"
            $ciuUpgradeOpLastCommand = $ciuUpgradeOpLastCommand + " ( " + $4 + " )"
            $ciuUpgradeOpLastStatus = $ciuUpgradeOpLastStatus + " ( " + $5 + " )"
            
            @ExtendedAttr = nvp_add(@ExtendedAttr, "ciuUpgradeOpCommand", $ciuUpgradeOpCommand, "ciuUpgradeOpStatus", $ciuUpgradeOpStatus, 
                "ciuUpgradeOpTimeCompleted", $ciuUpgradeOpTimeCompleted, "ciuUpgradeOpLastCommand", $ciuUpgradeOpLastCommand, "ciuUpgradeOpLastStatus", $ciuUpgradeOpLastStatus)
         
        case "2": ### ciuUpgradeJobStatusNotify
        
            ##########
            # $1 = ciuUpgradeOpStatusOperation
            # $2 = ciuUpgradeOpStatusModule
            # $3 = ciuUpgradeOpStatusSrcImageLoc
            # $4 = ciuUpgradeOpStatusDestImageLoc
            # $5 = ciuUpgradeOpStatusJobStatus
            # $6 = ciuUpgradeOpStatusPercentCompl
            # $7 = ciuUpgradeOpStatusJobStatusReas
            # $8 = ciuUpgradeOpStatus
            # $9 = ciuUpgradeOpStatusReason
            ##########
            
            $ciuUpgradeOpStatusOperation = lookup($1, CiuUpgradeOpStatusOperation)
            $ciuUpgradeOpStatusModule = $2
            $ciuUpgradeOpStatusSrcImageLoc = $3
            $ciuUpgradeOpStatusDestImageLoc = $4
            $ciuUpgradeOpStatusJobStatus = lookup($5, CiuUpgradeOpStatusJobStatus)
            $ciuUpgradeOpStatusPercentCompl = $6
            $ciuUpgradeOpStatusJobStatusReas = $7
            $ciuUpgradeOpStatus = lookup($8, CiuUpgradeOpStatus)
            $ciuUpgradeOpStatusReason = $9
            
            $ciuUpgradeOpStatusOperIndex = extract($OID1,"\.([0-9]+)$")
            $ciuUpgradeOpStatusModuleLookup = lookup($ciuUpgradeOpStatusModule, PhysicalClass)
            
            $OS_EventId = "SNMPTRAP-cisco-CISCO-IMAGE-UPGRADE-MIB-ciuUpgradeJobStatusNotify"

            @AlertGroup = $ciuUpgradeOpStatusModuleLookup + " " + "Upgrade Status"
            @AlertKey = "ciuUpgradeOpStatusEntry" + "." + $ciuUpgradeOpStatusOperIndex
            @Summary = "Image" + " " + $ciuUpgradeOpStatusOperation + " " + "Operation Status" + " " + $ciuUpgradeOpStatusJobStatus + " ( " + @AlertKey + " ) "
            
            $ciuUpgradeOpStatusModuleLookup = $ciuUpgradeOpStatusModuleLookup + " ( " + $ciuUpgradeOpStatusModule + " )"
            
            switch($5)
            {
                case "1":### unknown
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "2":### other
                    $SEV_KEY = $OS_EventId + "_other"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "3":### failed
                    $SEV_KEY = $OS_EventId + "_failed"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "4":### inProgress
                    $SEV_KEY = $OS_EventId + "_inProgress"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 0               
                case "5":### success
                    $SEV_KEY = $OS_EventId + "_success"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0               
                case "6":### planned
                    $SEV_KEY = $OS_EventId + "_planned"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800               
                default: 
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = "Image" + " " + $ciuUpgradeOpStatusOperation + " " + "Operation Status Unknown" + " ( " + @AlertKey + " ) "
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $ciuUpgradeOpStatusOperation = $ciuUpgradeOpStatusOperation + " ( " + $1 + " )"
            $ciuUpgradeOpStatusJobStatus = $ciuUpgradeOpStatusJobStatus + " ( " + $5 + " )"
            $ciuUpgradeOpStatus = $ciuUpgradeOpStatus + " ( " + $8 + " )"
            @ExtendedAttr = nvp_add(@ExtendedAttr, "ciuUpgradeOpStatusOperation", $ciuUpgradeOpStatusOperation, "ciuUpgradeOpStatusModule", $ciuUpgradeOpStatusModule, 
                "ciuUpgradeOpStatusSrcImageLoc", $ciuUpgradeOpStatusSrcImageLoc, "ciuUpgradeOpStatusDestImageLoc", $ciuUpgradeOpStatusDestImageLoc, "ciuUpgradeOpStatusJobStatus", $ciuUpgradeOpStatusJobStatus, 
                "ciuUpgradeOpStatusPercentCompl", $ciuUpgradeOpStatusPercentCompl, "ciuUpgradeOpStatusJobStatusReas", $ciuUpgradeOpStatusJobStatusReas, "ciuUpgradeOpStatus", $ciuUpgradeOpStatus, 
                "ciuUpgradeOpStatusReason", $ciuUpgradeOpStatusReason, "ciuUpgradeOpStatusOperIndex", $ciuUpgradeOpStatusOperIndex, "ciuUpgradeOpStatusModuleLookup", $ciuUpgradeOpStatusModuleLookup)
         
        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-IMAGE-UPGRADE-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-IMAGE-UPGRADE-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-IMAGE-UPGRADE-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-IMAGE-UPGRADE-MIB.user.include.snmptrap.rules"


##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-IMAGE-UPGRADE-MIB.include.snmptrap.rules >>>>>")


