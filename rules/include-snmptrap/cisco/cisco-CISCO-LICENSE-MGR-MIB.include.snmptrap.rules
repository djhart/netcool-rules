###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-LICENSE-MGR-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.369.3": ### Cisco managing License Files on the System - Notifications from CISCO-LICENSE-MGR-MIB (20040720)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-LICENSE-MGR-MIB.include.snmptrap.rules >>>>>")
 
    @Agent = "Cisco-License Manager"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### clmLicenseExpiryNotify

            ##########
            # $1 = clmLicenseExpiryDate 
            ##########

            $clmLicenseExpiryDate = $1
            
            $clmLicenseFeatureName_Raw = extract($OID1, "3\.6\.1\.4\.1\.9\.9\.369\.1\.3\.4\.1\.6\.(.*)$")
            $OctetString = $clmLicenseFeatureName_Raw
            include "$NC_RULES_HOME/include-snmptrap/decodeOctetString.include.snmptrap.rules"
            $clmLicenseFeatureName = $String
            

            $OS_EventId = "SNMPTRAP-cisco-CISCO-LICENSE-MGR-MIB-clmLicenseExpiryNotify"

            @AlertGroup = "Feature License Status"
            @AlertKey = "clmLicenseFeatureUsageEntry." + $clmLicenseFeatureName_Raw
            @Summary = "Feature License Expired  ( Feature: " + $clmLicenseFeatureName + " )"
                    
            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap 
           
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($clmLicenseExpiryDate,$clmLicenseFeatureName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "clmLicenseExpiryDate", $clmLicenseExpiryDate, "clmLicenseFeatureName", $clmLicenseFeatureName)

        case "2": ### clmNoLicenseForFeatureNotify

            ##########
            # $1 = clmLicenseGracePeriod 
            ##########

            $clmLicenseGracePeriod = $1 + " Seconds"            

            $clmLicenseFeatureName_Raw = extract($OID1, "3\.6\.1\.4\.1\.9\.9\.369\.1\.3\.4\.1\.7\.(.*)$")
            $OctetString = $clmLicenseFeatureName_Raw
            include "$NC_RULES_HOME/include-snmptrap/decodeOctetString.include.snmptrap.rules"
            $clmLicenseFeatureName = $String
            

            $OS_EventId = "SNMPTRAP-cisco-CISCO-LICENSE-MGR-MIB-clmNoLicenseForFeatureNotify"

            @AlertGroup = "Feature License Status"
            @AlertKey = "clmLicenseFeatureUsageEntry." + $clmLicenseFeatureName_Raw
            @Summary = "Feature License Issued  ( Feature: " + $clmLicenseFeatureName + " )"
                    
            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap 

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($clmLicenseGracePeriod,$clmLicenseFeatureName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "clmLicenseGracePeriod", $clmLicenseGracePeriod, "clmLicenseFeatureName", $clmLicenseFeatureName)

        case "3": ### clmLicenseFileMissingNotify

            ##########
            # $1 = clmNoOfMissingUsageLicenses 
            ##########

            $clmNoOfMissingUsageLicenses = $1
      
            $clmLicenseFeatureName_Raw = extract($OID1, "3\.6\.1\.4\.1\.9\.9\.369\.1\.3\.4\.1\.4\.(.*)$")
            $OctetString = $clmLicenseFeatureName_Raw
            include "$NC_RULES_HOME/include-snmptrap/decodeOctetString.include.snmptrap.rules"
            $clmLicenseFeatureName = $String
            
            $OS_EventId = "SNMPTRAP-cisco-CISCO-LICENSE-MGR-MIB-clmLicenseFileMissingNotify"

            @AlertGroup = "Feature License Status"
            @AlertKey = "clmLicenseFeatureUsageEntry." + $clmLicenseFeatureName_Raw
            @Summary = $1 + " Installed Licenses Missing  ( Feature: " + $clmLicenseFeatureName + " )"
                    
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap 

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($clmNoOfMissingUsageLicenses,$clmLicenseFeatureName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "clmNoOfMissingUsageLicenses", $clmNoOfMissingUsageLicenses, "clmLicenseFeatureName", $clmLicenseFeatureName)

        case "4": ### clmLicenseExpiryWarningNotify

            ##########
            # $1 = clmLicenseExpiryDate 
            ##########

            $clmLicenseExpiryDate = $1 
           
            $clmLicenseFeatureName_Raw = extract($OID1, "3\.6\.1\.4\.1\.9\.9\.369\.1\.3\.4\.1\.6\.(.*)$")
            $OctetString = $clmLicenseFeatureName_Raw
            include "$NC_RULES_HOME/include-snmptrap/decodeOctetString.include.snmptrap.rules"
            $clmLicenseFeatureName = $String
            

            $OS_EventId = "SNMPTRAP-cisco-CISCO-LICENSE-MGR-MIB-clmLicenseExpiryWarningNotify"

            @AlertGroup = "Feature License Status"
            @AlertKey = "clmLicenseFeatureUsageEntry." + $clmLicenseFeatureName_Raw
            @Summary = "Feature License Expiry Warning  ( Feature: " + $clmLicenseFeatureName + " )"
                    
            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap 

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($clmLicenseExpiryDate,$clmLicenseFeatureName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "clmLicenseExpiryDate", $clmLicenseExpiryDate, "clmLicenseFeatureName", $clmLicenseFeatureName)

         default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-LICENSE-MGR-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-LICENSE-MGR-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-LICENSE-MGR-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-LICENSE-MGR-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-LICENSE-MGR-MIB.include.snmptrap.rules >>>>>")
