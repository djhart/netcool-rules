###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-LWAPP-MESH-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table cisco-CISCO-LWAPP-MESH-MIB_sev =
{
    {"SNMPTRAP-cisco-CISCO-LWAPP-MESH-MIB-ciscoLwappMeshAuthFailure","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-LWAPP-MESH-MIB-ciscoLwappMeshChildExcludedParent","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-LWAPP-MESH-MIB-ciscoLwappMeshParentChange","2","13","1800"},
    {"SNMPTRAP-cisco-CISCO-LWAPP-MESH-MIB-ciscoLwappMeshChildMoved","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-LWAPP-MESH-MIB-ciscoLwappMeshExcessiveParentChange","2","13","1800"},
    {"SNMPTRAP-cisco-CISCO-LWAPP-MESH-MIB-ciscoLwappMeshOnsetSNR","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-LWAPP-MESH-MIB-ciscoLwappMeshAbateSNR","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-LWAPP-MESH-MIB-ciscoLwappMeshConsoleLogin_success","1","2","0"},
    {"SNMPTRAP-cisco-CISCO-LWAPP-MESH-MIB-ciscoLwappMeshConsoleLogin_failure","3","1","0"},
    {"SNMPTRAP-cisco-CISCO-LWAPP-MESH-MIB-ciscoLwappMeshConsoleLogin_unknown","2","1","0"}
}
default = {"Unknown","Unknown","Unknown"}
