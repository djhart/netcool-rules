###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
# Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
# - CISCO-NBAR-PROTOCOL-DISCOVERY-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.244": ### Cisco NBAR Protocol Discovery MIB - Notifications from CISCO-NBAR-PROTOCOL-DISCOVERY-MIB (200208160000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-NBAR-PROTOCOL-DISCOVERY-MIB.include.snmptrap.rules >>>>>")

    @Agent = "CISCO-NBAR-PROTOCOL-DISCOVERY"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### cnpdThresholdRisingEvent

            ##########
            # $1 = cnpdThresholdConfigIfIndex 
            # $2 = cnpdThresholdConfigStatsSelect 
            # $3 = cnpdThresholdHistoryValue 
            # $4 = cnpdThresholdConfigRising 
            # $5 = cnpdThresholdConfigProtocol 
            # $6 = cnpdThresholdHistoryTime 
            ##########

            $cnpdThresholdConfigIfIndex = $1
            $cnpdThresholdConfigStatsSelect = lookup($2, CiscoPdDataType) + " ( " + $2 + " )"
            $cnpdThresholdHistoryValue = $3
            $cnpdThresholdConfigRising = $4
            $cnpdThresholdConfigProtocol = $5
            $cnpdThresholdHistoryTime = $6

            $cnpdThresholdConfigIndex = extract($OID1, "\.([0-9]+)$")
            $cnpdThresholdHistoryIndex = extract($OID3, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-cisco-CISCO-NBAR-PROTOCOL-DISCOVERY-MIB-cnpdThresholdRisingEvent"

            @AlertGroup = "Notification entry crosses its rising threshold"
            @AlertKey = "cnpdThresholdConfigEntry." + $cnpdThresholdConfigIndex + ", cnpdThresholdHistoryEntry." + $cnpdThresholdHistoryIndex 
            @Summary = "Notification entry crosses its rising threshold and generates an event that added to the cnpdThresholdHistoryTable ( " + @AlertKey + " )"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cnpdThresholdConfigIfIndex,$cnpdThresholdConfigStatsSelect,$cnpdThresholdHistoryValue,$cnpdThresholdConfigRising,$cnpdThresholdConfigProtocol,$cnpdThresholdHistoryTime,$cnpdThresholdConfigIndex,$cnpdThresholdHistoryIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cnpdThresholdConfigIfIndex", $cnpdThresholdConfigIfIndex, "cnpdThresholdConfigStatsSelect", $cnpdThresholdConfigStatsSelect, "cnpdThresholdHistoryValue", $cnpdThresholdHistoryValue,
                 "cnpdThresholdConfigRising", $cnpdThresholdConfigRising, "cnpdThresholdConfigProtocol", $cnpdThresholdConfigProtocol, "cnpdThresholdHistoryTime", $cnpdThresholdHistoryTime,
                 "cnpdThresholdConfigIndex", $cnpdThresholdConfigIndex, "cnpdThresholdHistoryIndex", $cnpdThresholdHistoryIndex)

        case "2": ### cnpdThresholdFallingEvent

            ##########
            # $1 = cnpdThresholdConfigIfIndex 
            # $2 = cnpdThresholdConfigStatsSelect 
            # $3 = cnpdThresholdHistoryValue 
            # $4 = cnpdThresholdConfigFalling 
            # $5 = cnpdThresholdConfigProtocol 
            # $6 = cnpdThresholdHistoryTime 
            ##########

            $cnpdThresholdConfigIfIndex = $1
            $cnpdThresholdConfigStatsSelect = lookup($2, CiscoPdDataType) + " ( " + $2 + " )"
            $cnpdThresholdHistoryValue = $3
            $cnpdThresholdConfigFalling = $4
            $cnpdThresholdConfigProtocol = $5
            $cnpdThresholdHistoryTime = $6

            $cnpdThresholdConfigIndex = extract($OID1, "\.([0-9]+)$")
            $cnpdThresholdHistoryIndex = extract($OID3, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-cisco-CISCO-NBAR-PROTOCOL-DISCOVERY-MIB-cnpdThresholdFallingEvent"

            @AlertGroup = "Notification entry crosses its falling threshold"
            @AlertKey = "cnpdThresholdConfigEntry." + $cnpdThresholdConfigIndex + ", cnpdThresholdHistoryEntry." + $cnpdThresholdHistoryIndex 
            @Summary = "Notification entry crosses its falling threshold and generates event thatadded to the cnpdThresholdHistoryTable ( " + @AlertKey + " )"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cnpdThresholdConfigIfIndex,$cnpdThresholdConfigStatsSelect,$cnpdThresholdHistoryValue,$cnpdThresholdConfigFalling,$cnpdThresholdConfigProtocol,$cnpdThresholdHistoryTime,$cnpdThresholdConfigIndex,$cnpdThresholdHistoryIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cnpdThresholdConfigIfIndex", $cnpdThresholdConfigIfIndex, "cnpdThresholdConfigStatsSelect", $cnpdThresholdConfigStatsSelect, "cnpdThresholdHistoryValue", $cnpdThresholdHistoryValue,
                 "cnpdThresholdConfigFalling", $cnpdThresholdConfigFalling, "cnpdThresholdConfigProtocol", $cnpdThresholdConfigProtocol, "cnpdThresholdHistoryTime", $cnpdThresholdHistoryTime,
                 "cnpdThresholdConfigIndex", $cnpdThresholdConfigIndex, "cnpdThresholdHistoryIndex", $cnpdThresholdHistoryIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-NBAR-PROTOCOL-DISCOVERY-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-NBAR-PROTOCOL-DISCOVERY-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-NBAR-PROTOCOL-DISCOVERY-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-NBAR-PROTOCOL-DISCOVERY-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-NBAR-PROTOCOL-DISCOVERY-MIB.include.snmptrap.rules >>>>>")
