###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
# Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
# - CISCO-MODULE-AUTO-SHUTDOWN-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.386": ### Cisco MIB to configure the module automatic shutdown feature - Notifications from CISCO-MODULE-AUTO-SHUTDOWN-MIB (200803120000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-MODULE-AUTO-SHUTDOWN-MIB.include.snmptrap.rules >>>>>")

    @Agent = "CISCO-MODULE-AUTO-SHUTDOWN"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### cmasModuleAutoShutdown

            ##########
            # $1 = entPhysicalName 
            # $2 = entPhysicalModelName 
            # $3 = cmasModuleNumResets 
            # $4 = cmasModuleLastResetReason 
            ##########

            $entPhysicalName = $1
            $entPhysicalModelName = $2
            $cmasModuleNumResets = $3
            $cmasModuleLastResetReason = $4

            $entPhysicalIndex = extract($OID1, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-cisco-CISCO-MODULE-AUTO-SHUTDOWN-MIB-cmasModuleAutoShutdown"

            @AlertGroup = "Module auto shutdown feature"
            @AlertKey = "entPhysicalEntry." + $entPhysicalIndex
            @Summary = "Module auto shutdown feature shuts down a module ( " + @AlertKey + " )"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($entPhysicalName,$entPhysicalModelName,$cmasModuleNumResets,$cmasModuleLastResetReason,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "entPhysicalName", $entPhysicalName, "entPhysicalModelName", $entPhysicalModelName, "cmasModuleNumResets", $cmasModuleNumResets,
                 "cmasModuleLastResetReason", $cmasModuleLastResetReason, "entPhysicalIndex", $entPhysicalIndex)

        case "2": ### cmasModuleSysActionNotif

            ##########
            # $1 = entPhysicalName 
            # $2 = entPhysicalModelName 
            # $3 = cmasModuleSysAction 
            # $4 = cmasModuleSysActionReason 
            ##########

            $entPhysicalName = $1
            $entPhysicalModelName = $2
            $cmasModuleSysAction = lookup($3, CiscoModuleAutoShutSysAction)
            $cmasModuleSysActionReason = $4

            $OS_EventId = "SNMPTRAP-cisco-CISCO-MODULE-AUTO-SHUTDOWN-MIB-cmasModuleSysActionNotif"

            @AlertGroup = "System initiated action occurs"
            @AlertKey = ""

            switch($3)
            {
                        case "1": ### other
                            $SEV_KEY = $OS_EventId + "_other"
                            @Summary = "System initiated other action occurs on a module"
                            $DEFAULT_Severity = 1
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0

                        case "2": ### reset
                            $SEV_KEY = $OS_EventId + "_reset"
                            @Summary = "System initiated reset action occurs on a module"
                            $DEFAULT_Severity = 1
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0

                        case "3": ### powerCycle
                            $SEV_KEY = $OS_EventId + "_powerCycle"
                            @Summary = "System initiated powerCycle action occurs on a module"
                            $DEFAULT_Severity = 1
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0

                        case "4": ### powerDown
                            $SEV_KEY = $OS_EventId + "_powerDown"
                            @Summary = "System initiated powerDown action occurs on a module"
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        default:
                            $SEV_KEY = $OS_EventId + "_unknown"
                            @Summary = "System initiated unknown action occurs on a module"
                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0
            }


            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            $cmasModuleSysAction = $cmasModuleSysAction + " ( " + $3 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($entPhysicalName,$entPhysicalModelName,$cmasModuleSysAction,$cmasModuleSysActionReason)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "entPhysicalName", $entPhysicalName, "entPhysicalModelName", $entPhysicalModelName, "cmasModuleSysAction", $cmasModuleSysAction,
                 "cmasModuleSysActionReason", $cmasModuleSysActionReason)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-MODULE-AUTO-SHUTDOWN-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-MODULE-AUTO-SHUTDOWN-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-MODULE-AUTO-SHUTDOWN-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-MODULE-AUTO-SHUTDOWN-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-MODULE-AUTO-SHUTDOWN-MIB.include.snmptrap.rules >>>>>")
