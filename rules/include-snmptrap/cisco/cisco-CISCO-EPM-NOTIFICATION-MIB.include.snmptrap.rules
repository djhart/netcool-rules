###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
# Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
# - CISCO-EPM-NOTIFICATION-MIB
#
###############################################################################
#
# 2.0 - Customized to Cisco standard format
#
# Part of VAMS 1.5 - Customized by Tivoli OEM Engineering Team for AMS-Video 
#                    2008/04/10 D. Johnson (johnsond@us.ibm.com)
#
###############################################################################

case ".1.3.6.1.4.1.9.9.311":  ### - Notifications from CISCO-EPM-NOTIFICATION-MIB (200406070000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-EPM-NOTIFICATION-MIB.include.snmptrap.rules >>>>>")

    @Agent = "CISCO-EPM-NOTIFICATION-MIB"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

	switch($specific-trap) 
      {
		case "1": ### - ciscoEpmNotificationAlarm

			#######################
                        # $1 = cenAlarmVersion
                        # $2 = cenAlarmTimestamp
                        # $3 = cenAlarmUpdatedTimestamp
                        # $4 = cenAlarmInstanceID
                        # $5 = cenAlarmStatus
                        # $6 = cenAlarmStatusDefinition
                        # $7 = cenAlarmType
                        # $8 = cenAlarmCategory
                        # $9 = cenAlarmCategoryDefinition
                        # $10 = cenAlarmServerAddressType
                        # $11 = cenAlarmServerAddress
                        # $12 = cenAlarmManagedObjectClass
                        # $13 = cenAlarmManagedObjectAddressType
                        # $14 = cenAlarmManagedObjectAddress
                        # $15 = cenAlarmDescription
                        # $16 = cenAlarmSeverity
                        # $17 = cenAlarmSeverityDefinition
                        # $18 = cenAlarmTriageValue
                        # $19 = cenEventIDList
                        # $20 = cenUserMessage1
                        # $21 = cenUserMessage2
                        # $22 = cenUserMessage3
			#######################

			$cenAlarmVersion = $1
			$cenAlarmTimestamp = $2
			$cenAlarmUpdatedTimestamp = $3
			$cenAlarmInstanceID = $4
			$cenAlarmStatus = $5
			$cenAlarmStatusDefinition = $6
			$cenAlarmType = lookup($7,cisco-CISCO-EPM-NOTIFICATION-MIB-cenAlarmType)
			$cenAlarmCategory = $8
			$cenAlarmCategoryDefinition = $9
			$cenAlarmServerAddressType = lookup($10,InetAddressType) + " ( " + $10 + " )"
			$cenAlarmServerAddress = $11
			$cenAlarmManagedObjectClass = $12
			$cenAlarmManagedObjectAddressType = lookup($13,InetAddressType) + " ( " + $13 + " )"
			$cenAlarmManagedObjectAddress = $14
			$cenAlarmDescription = $15
			$cenAlarmSeverity = $16
			$cenAlarmSeverityDefinition = $17
			$cenAlarmTriageValue = $18
			$cenEventIDList = $19
			$cenUserMessage1 = $20
			$cenUserMessage2 = $21
			$cenUserMessage3 = $22

                  $cenAlarmIndex = extract($OID1, "\.([0-9]+)$")

			$OS_EventId = "SNMPTRAP-cisco-CISCO-EPM-NOTIFICATION-MIB-ciscoEpmNotificationAlarm"

			@AlertGroup = "ciscoEpmNotificationAlarm"
			@AlertKey = "cenAlarmEntry." + $cenAlarmIndex
			@Summary = "ciscoEpmNotificationAlarm : Notification status of the managed object : " + $cenAlarmStatus + ", Alarm type : " + $cenAlarmType + " ( " + @AlertKey + " )"

                  $DEFAULT_Severity = 2
                  $DEFAULT_Type = 13
                  $DEFAULT_ExpireTime = 1800

			@Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

			$cenAlarmType = $cenAlarmType + " ( " + $7 + " )"
			if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
    			details($cenAlarmVersion,$cenAlarmTimestamp,$cenAlarmUpdatedTimestamp,$cenAlarmInstanceID,$cenAlarmStatus,$cenAlarmStatusDefinition,$cenAlarmType,$cenAlarmCategory,$cenAlarmCategoryDefinition,$cenAlarmServerAddressType,$cenAlarmServerAddress,$cenAlarmManagedObjectClass,$cenAlarmManagedObjectAddressType,$cenAlarmManagedObjectAddress,$cenAlarmDescription,$cenAlarmSeverity,$cenAlarmSeverityDefinition,$cenAlarmTriageValue,$cenEventIDList,$cenUserMessage1,$cenUserMessage2,$cenUserMessage3,$cenAlarmIndex)
			}
			@ExtendedAttr = nvp_add(@ExtendedAttr, "cenAlarmVersion", $cenAlarmVersion, "cenAlarmTimestamp", $cenAlarmTimestamp, "cenAlarmUpdatedTimestamp", $cenAlarmUpdatedTimestamp,
			     "cenAlarmInstanceID", $cenAlarmInstanceID, "cenAlarmStatus", $cenAlarmStatus, "cenAlarmStatusDefinition", $cenAlarmStatusDefinition,
			     "cenAlarmType", $cenAlarmType, "cenAlarmCategory", $cenAlarmCategory, "cenAlarmCategoryDefinition", $cenAlarmCategoryDefinition,
			     "cenAlarmServerAddressType", $cenAlarmServerAddressType, "cenAlarmServerAddress", $cenAlarmServerAddress, "cenAlarmManagedObjectClass", $cenAlarmManagedObjectClass,
			     "cenAlarmManagedObjectAddressType", $cenAlarmManagedObjectAddressType, "cenAlarmManagedObjectAddress", $cenAlarmManagedObjectAddress, "cenAlarmDescription", $cenAlarmDescription,
			     "cenAlarmSeverity", $cenAlarmSeverity, "cenAlarmSeverityDefinition", $cenAlarmSeverityDefinition, "cenAlarmTriageValue", $cenAlarmTriageValue,
			     "cenEventIDList", $cenEventIDList, "cenUserMessage1", $cenUserMessage1, "cenUserMessage2", $cenUserMessage2,
			     "cenUserMessage3", $cenUserMessage3, "cenAlarmIndex", $cenAlarmIndex)


		case "2": ### - ciscoEpmNotificationAlarmRev1
			#######################
			# $1 = cenAlarmVersion
                        # $2 = cenAlarmTimestamp
                        # $3 = cenAlarmUpdatedTimestamp
                        # $4 = cenAlarmInstanceID
                        # $5 = cenAlarmStatus
                        # $6 = cenAlarmStatusDefinition
                        # $7 = cenAlarmType
                        # $8 = cenAlarmCategory
                        # $9 = cenAlarmCategoryDefinition
                        # $10 = cenAlarmServerAddressType
                        # $11 = cenAlarmServerAddress
                        # $12 = cenAlarmManagedObjectClass
                        # $13 = cenAlarmManagedObjectAddressType
                        # $14 = cenAlarmManagedObjectAddress
                        # $15 = cenAlarmDescription
                        # $16 = cenAlarmSeverity
                        # $17 = cenAlarmSeverityDefinition
                        # $18 = cenAlarmTriageValue
                        # $19 = cenEventIDList
                        # $20 = cenUserMessage1
                        # $21 = cenUserMessage2
                        # $22 = cenUserMessage3
                        # $23 = cenAlarmMode
                        # $24 = cenPartitionNumber
                        # $25 = cenPartitionName
                        # $26 = cenCustomerIdentification
                        # $27 = cenCustomerRevision
                        # $28 = cenAlertID
                        #######################

			$cenAlarmVersion = $1
			$cenAlarmTimestamp = $2
			$cenAlarmUpdatedTimestamp = $3
			$cenAlarmInstanceID = $4
			$cenAlarmStatus = $5
			$cenAlarmStatusDefinition = $6
			$cenAlarmType = lookup($7,cisco-CISCO-EPM-NOTIFICATION-MIB-cenAlarmType)
			$cenAlarmCategory = $8
			$cenAlarmCategoryDefinition = $9
			$cenAlarmServerAddressType = lookup($10,InetAddressType) + " ( " + $10 + " )"
			$cenAlarmServerAddress = $11
			$cenAlarmManagedObjectClass = $12
			$cenAlarmManagedObjectAddressType = lookup($13,InetAddressType) + " ( " + $13 + " )"
			$cenAlarmManagedObjectAddress = $14
			$cenAlarmDescription = $15
			$cenAlarmSeverity = $16
			$cenAlarmSeverityDefinition = $17
			$cenAlarmTriageValue = $18
			$cenEventIDList = $19
			$cenUserMessage1 = $20
			$cenUserMessage2 = $21
			$cenUserMessage3 = $22
			$cenAlarmMode = lookup($23,cisco-CISCO-EPM-NOTIFICATION-MIB-cenAlarmMode)
			$cenPartitionNumber = $24
			$cenPartitionName = $25
			$cenCustomerIdentification = $26
			$cenCustomerRevision = $27
			$cenAlertID = $28

                  $cenAlarmIndex = extract($OID1, "\.([0-9]+)$")

			$OS_EventId = "SNMPTRAP-cisco-CISCO-EPM-NOTIFICATION-MIB-ciscoEpmNotificationAlarmRev1"

			@Agent = "CISCO-EPM-NOTIFICATION-MIB"
			@AlertGroup = "ciscoEpmNotificationAlarmRev1"
			@AlertKey = "cenAlarmEntry." + $cenAlarmIndex
			@Summary = "ciscoEpmNotificationAlarmRev1 : Notification status of the managed object : " + $cenAlarmStatus + " , Alarm type : " + $cenAlarmType + " , Alarm Mode : " + $cenAlarmMode + " ( " + @AlertKey + " )"

                  switch($23)
                  { 
                    case "1": ### unknown
                        $SEV_KEY = $OS_EventId + "_unknown"
                        $DEFAULT_Severity = 2
                        $DEFAULT_Type = 1
                        $DEFAULT_ExpireTime = 0

                    case "2": ### alert
                        $SEV_KEY = $OS_EventId + "_alert"
                        $DEFAULT_Severity = 5
                        $DEFAULT_Type = 1
                        $DEFAULT_ExpireTime = 0

                    case "3": ### event
                        $SEV_KEY = $OS_EventId + "_event"
                        $DEFAULT_Severity = 1
                        $DEFAULT_Type = 2
                        $DEFAULT_ExpireTime = 0

                    default:
                        $SEV_KEY = $OS_EventId + "_unknown"
                        $DEFAULT_Severity = 2
                        $DEFAULT_Type = 1
                       $DEFAULT_ExpireTime = 0
                   }

                   update(@Summary)
                   update(@Severity)

			@Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

			$cenAlarmType = $cenAlarmType + " ( " + $7 + " )"
			$cenAlarmMode = $cenAlarmMode + " ( " + $23 + " )"

			if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
    			details($cenAlarmVersion,$cenAlarmTimestamp,$cenAlarmUpdatedTimestamp,$cenAlarmInstanceID,$cenAlarmStatus,$cenAlarmStatusDefinition,$cenAlarmType,$cenAlarmCategory,$cenAlarmCategoryDefinition,$cenAlarmServerAddressType,$cenAlarmServerAddress,$cenAlarmManagedObjectClass,$cenAlarmManagedObjectAddressType,$cenAlarmManagedObjectAddress,$cenAlarmDescription,$cenAlarmSeverity,$cenAlarmSeverityDefinition,$cenAlarmTriageValue,$cenEventIDList,$cenUserMessage1,$cenUserMessage2,$cenUserMessage3,$cenAlarmMode,$cenPartitionNumber,$cenPartitionName,$cenCustomerIdentification,$cenCustomerRevision,$cenAlertID,$cenAlarmIndex)
			}
			@ExtendedAttr = nvp_add(@ExtendedAttr, "cenAlarmVersion", $cenAlarmVersion, "cenAlarmTimestamp", $cenAlarmTimestamp, "cenAlarmUpdatedTimestamp", $cenAlarmUpdatedTimestamp,
			     "cenAlarmInstanceID", $cenAlarmInstanceID, "cenAlarmStatus", $cenAlarmStatus, "cenAlarmStatusDefinition", $cenAlarmStatusDefinition,
			     "cenAlarmType", $cenAlarmType, "cenAlarmCategory", $cenAlarmCategory, "cenAlarmCategoryDefinition", $cenAlarmCategoryDefinition,
			     "cenAlarmServerAddressType", $cenAlarmServerAddressType, "cenAlarmServerAddress", $cenAlarmServerAddress, "cenAlarmManagedObjectClass", $cenAlarmManagedObjectClass,
			     "cenAlarmManagedObjectAddressType", $cenAlarmManagedObjectAddressType, "cenAlarmManagedObjectAddress", $cenAlarmManagedObjectAddress, "cenAlarmDescription", $cenAlarmDescription,
			     "cenAlarmSeverity", $cenAlarmSeverity, "cenAlarmSeverityDefinition", $cenAlarmSeverityDefinition, "cenAlarmTriageValue", $cenAlarmTriageValue,
			     "cenEventIDList", $cenEventIDList, "cenUserMessage1", $cenUserMessage1, "cenUserMessage2", $cenUserMessage2,
			     "cenUserMessage3", $cenUserMessage3, "cenAlarmMode", $cenAlarmMode, "cenPartitionNumber", $cenPartitionNumber,
			     "cenPartitionName", $cenPartitionName, "cenCustomerIdentification", $cenCustomerIdentification, "cenCustomerRevision", $cenCustomerRevision,
			     "cenAlertID", $cenAlertID, "cenAlarmIndex", $cenAlarmIndex)

		default:
			@Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
			@Severity = 1
			@Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
			if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
    			details($*)
			}
			@ExtendedAttr = nvp_add($*)
	}

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-EPM-NOTIFICATION-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-EPM-NOTIFICATION-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-EPM-NOTIFICATION-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-EPM-NOTIFICATION-MIB.user.include.snmptrap.rules"

#include "$NC_RULES_HOME/rules/include-snmptrap/vams/cisco-CISCO-EPM-NOTIFICATION-MIB.adv.include.snmptrap.rules"
#include "$NC_RULES_HOME/rules/include-snmptrap/vams/cisco-CISCO-EPM-NOTIFICATION-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

log(DEBUG, "<<<<< Leaving... cisco-CISCO-EPM-NOTIFICATION-MIB.include.snmptrap.rules >>>>>")
