##############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-ENTITY-FRU-CONTROL-MIB
#
###############################################################################
#
# 2.3 - Updated Release.
#
#        Set NmosEventMap field with event map name and precedence value.
#
###############################################################################
#
# 2.2 - Trap(s) added
#
#         - cefcPowerSupplyOutputChange
#
# 2.1 - Simplified handling of "Severity via lookup" logic.
#
#     - Added basic debug logging.
#
# 2.0 - Rewritten for improved event presentation.  Includes enhancements which
#       comply with the Netcool Rules File Standards
#       (MUSE-STD-RF-02, July 2002)
#
#     - Modified to support following features introduced in NCiL 2.0:
#         - "Advanced" and "User" include files
#         - "Severity" lookup tables
#
# 1.0 - Based on rules extracted from cisco.include.snmptrap.rules 3.1.
#
###############################################################################

case ".1.3.6.1.4.1.9.9.117.2": ### Cisco Field Replaceable Units (FRUs) - Notifications from CISCO-ENTITY-FRU-CONTROL-MIB (200810080000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-ENTITY-FRU-CONTROL-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Cisco-Field Replaceable Units"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### cefcModuleStatusChange

            ##########
            # $1 = cefcModuleOperStatus
            # $2 = cefcModuleStatusLastChangeTime
            ##########

            $cefcModuleOperStatus = lookup($1, ModuleOperType) + " ( " + $1 + " )"
            $cefcModuleStatusLastChangeTime = $2
            $entPhysicalIndex = extract($OID1, "\.([0-9]+)$")
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cefcModuleOperStatus,$cefcModuleStatusLastChangeTime,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cefcModuleOperStatus", $cefcModuleOperStatus, "cefcModuleStatusLastChangeTime", $cefcModuleStatusLastChangeTime, "entPhysicalIndex", $entPhysicalIndex)

            $OS_EventId = "SNMPTRAP-cisco-CISCO-ENTITY-FRU-CONTROL-MIB-cefcModuleStatusChange"
            @NmosEventMap = "EntityMibTrap.0"
            
            @AlertGroup = "Module Status"
            @AlertKey = "cefcModuleEntry." + $entPhysicalIndex
            switch($1)
            {
                case "1": ### unknown
                    @Summary = "Module Status Unknown"
                    
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "2": ### ok
                    @Summary = "Module Operational"
                    
                    $SEV_KEY = $OS_EventId + "_ok"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    
                case "3": ### disabled
                    @Summary = "Module Administratively Disabled"
                    
                    $SEV_KEY = $OS_EventId + "_disabled"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "4": ### okButDiagFailed
                    @Summary = "Module Operational, but Failed Diagnostics"
                    
                    $SEV_KEY = $OS_EventId + "_okButDiagFailed"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "5": ### boot
                    @Summary = "Module Booting, Bringing Up Image"
                    
                    $SEV_KEY = $OS_EventId + "_boot"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "6": ### selfTest
                    @Summary = "Module Performing Self-Test"
                    
                    $SEV_KEY = $OS_EventId + "_selfTest"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "7": ### failed
                    @Summary = "Module Failed"
                    
                    $SEV_KEY = $OS_EventId + "_failed"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "8": ### missing
                    @Summary = "Module Provisioned, but Missing"
                    
                    $SEV_KEY = $OS_EventId + "_missing"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "9": ### mismatchWithParent
                    @Summary = "Module Not Compatible with Parent"
                    
                    $SEV_KEY = $OS_EventId + "_mismatchWithParent"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "10": ### mismatchConfig
                    @Summary = "Module Not Compatible with Current Configuration"
                    
                    $SEV_KEY = $OS_EventId + "_mismatchConfig"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "11": ### diagFailed
                    @Summary = "Module Diagnostic Test Failed, Hardware Failure"
                    
                    $SEV_KEY = $OS_EventId + "_diagFailed"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "12": ### dormant
                    @Summary = "Module Dormant, Waiting for External or Internal Event"
                    
                    $SEV_KEY = $OS_EventId + "_dormant"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "13": ### outOfServiceAdmin
                    @Summary = "Module Out of Service, Administratively"
                    
                    $SEV_KEY = $OS_EventId + "_outOfServiceAdmin"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "14": ### outOfServiceEnvTemp
                    @Summary = "Module Out Of Service, Environmental Temperature Problem"
                    
                    $SEV_KEY = $OS_EventId + "_outOfServiceEnvTemp"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "15": ### poweredDown
                    @Summary = "Module Powered Down"
                    
                    $SEV_KEY = $OS_EventId + "_poweredDown"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "16": ### poweredUp
                    @Summary = "Module Powered Up"
                    
                    $SEV_KEY = $OS_EventId + "_poweredUp"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    
                case "17": ### powerDenied
                    @Summary = "Module Power Denied"
                    
                    $SEV_KEY = $OS_EventId + "_powerDenied"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "18": ### powerCycled
                    @Summary = "Module Power Cycled"
                    
                    $SEV_KEY = $OS_EventId + "_powerCycled"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "19": ### okButPowerOverWarning
                    @Summary = "Module OK, but Power over Warning"
                    
                    $SEV_KEY = $OS_EventId + "_okButPowerOverWarning"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "20": ### okButPowerOverCritical
                    @Summary = "Module OK, but Power over Critical"
                    
                    $SEV_KEY = $OS_EventId + "_okButPowerOverCritical"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "21": ### syncInProgress
                    @Summary = "Synchronization in progress"

                    $SEV_KEY = $OS_EventId + "_syncInProgress"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "22": ### upgrading
                    @Summary = "Module upgrading"

                    $SEV_KEY = $OS_EventId + "_upgrading"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "23": ### okButAuthFailed
                    @Summary = "Module operational but failed hardware integrity verification"

                    $SEV_KEY = $OS_EventId + "_okButAuthFailed"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                default:
                    @Summary = "Module Status Unknown"
                    
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
            }
            @Summary = @Summary + "  ( " + @AlertKey + " )"
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $1

        case "2": ### cefcPowerStatusChange

            ##########
            # $1 = cefcFRUPowerOperStatus
            # $2 = cefcFRUPowerAdminStatus 
            ##########

            $cefcFRUPowerOperStatus = lookup($1, PowerOperType) + " ( " + $1 + " )"
            $cefcFRUPowerAdminStatus = lookup($2, PowerAdminType) + " ( " + $2 + " )"
            $entPhysicalIndex = extract($OID1, "\.([0-9]+)$")
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cefcFRUPowerOperStatus,$cefcFRUPowerAdminStatus,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cefcFRUPowerOperStatus", $cefcFRUPowerOperStatus, "cefcFRUPowerAdminStatus", $cefcFRUPowerAdminStatus, "entPhysicalIndex", $entPhysicalIndex)

            $OS_EventId = "SNMPTRAP-cisco-CISCO-ENTITY-FRU-CONTROL-MIB-cefcPowerStatusChange"
            @NmosEventMap = "EntityMibTrap.0"
            
            @AlertGroup = "FRU Power Status"
            @AlertKey = "cefcFRUPowerStatusEntry." + $entPhysicalIndex
            switch($1)
            {
                case "1": ### offEnvOther
                    @Summary = "FRU Powered Off"
                    
                    $SEV_KEY = $OS_EventId + "_offEnvOther"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "2": ### on
                    @Summary = "FRU Powered On"
                    
                    $SEV_KEY = $OS_EventId + "_on"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    
                case "3": ### offAdmin
                    @Summary = "FRU Administratively Powered Off"
                    
                    $SEV_KEY = $OS_EventId + "_offAdmin"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "4": ### offDenied
                    @Summary = "FRU Powered Off, Insufficient System Power"
                    
                    $SEV_KEY = $OS_EventId + "_offDenied"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "5": ### offEnvPower
                    @Summary = "FRU Powered Off, Internal Power Problem"
                    
                    $SEV_KEY = $OS_EventId + "_offEnvPower"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "6": ### offEnvTemp
                    @Summary = "FRU Powered Off, Temperature Problem"
                    
                    $SEV_KEY = $OS_EventId + "_offEnvTemp"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "7": ### offEnvFan
                    @Summary = "FRU Powered Off, Fan Problem"
                    
                    $SEV_KEY = $OS_EventId + "_offEnvFan"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "8": ### failed
                    @Summary = "FRU Failed"
                    
                    $SEV_KEY = $OS_EventId + "_failed"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "9": ### onButFanFail
                    @Summary = "FRU Powered On, but Fan Failed"
                    
                    $SEV_KEY = $OS_EventId + "_onButFanFail"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
 
                case "10": ### offCooling
                    @Summary = "FRU powered off due to system's insufficient cooling capasity"

                    $SEV_KEY = $OS_EventId + "_offCooling"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "11": ### offConnectorRating
                    @Summary = "FRU powered off due to system's connector rating exceeded"

                    $SEV_KEY = $OS_EventId + "_offConnectorRating"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "12": ### onButInlinePowerFail
                    @Summary = "FRU on but no inline power delivered"

                   $SEV_KEY = $OS_EventId + "_onButInlinePowerFail"
                   $DEFAULT_Severity = 3
                   $DEFAULT_Type = 1
                   $DEFAULT_ExpireTime = 0

                default:
                    @Summary = "FRU Power Status Unknown"
                    
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
            }
            @Summary = @Summary + "  ( " + @AlertKey + " )"
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $1

        case "3": ### cefcFRUInserted

            ##########
            # $1 = entPhysicalContainedIn 
            ##########

            $entPhysicalContainedIn = $1
            $entPhysicalIndex = extract($OID1, "\.([0-9]+)$")
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($entPhysicalContainedIn,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "entPhysicalContainedIn", $entPhysicalContainedIn, "entPhysicalIndex", $entPhysicalIndex)

            $OS_EventId = "SNMPTRAP-cisco-CISCO-ENTITY-FRU-CONTROL-MIB-cefcFRUInserted"
            @NmosEventMap = "EntityMibTrap.0"
            
            @AlertGroup = "FRU Insert/Remove"
            @AlertKey = "entPhysicalEntry." + $entPhysicalIndex
            @Summary = "FRU Inserted  ( " + @AlertKey + " )"
            
            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $1

        case "4": ### cefcFRURemoved

            ##########
            # $1 = entPhysicalContainedIn 
            ##########

            $entPhysicalContainedIn = $1
            $entPhysicalIndex = extract($OID1, "\.([0-9]+)$")
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($entPhysicalContainedIn,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "entPhysicalContainedIn", $entPhysicalContainedIn, "entPhysicalIndex", $entPhysicalIndex)

            $OS_EventId = "SNMPTRAP-cisco-CISCO-ENTITY-FRU-CONTROL-MIB-cefcFRURemoved"
            @NmosEventMap = "EntityMibTrap.10000"
            
            @AlertGroup = "FRU Insert/Remove"
            @AlertKey = "entPhysicalEntry." + $entPhysicalIndex
            @Summary = "FRU Removed  ( " + @AlertKey + " )"
            
            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $1

        case "5": ### cefcUnrecognizedFRU

            ##########
            # $1 = entPhysicalClass
            # $2 = entPhysicalVendorType
            # $3 = entPhysicalName
            # $4 = entPhysicalModelName
            # $5 = cefcPhysicalStatus
            ##########

            $entPhysicalClass = lookup($1, PhysicalClass)
            $entPhysicalVendorType = $2
            $entPhysicalName = $3
            $entPhysicalModelName = $4
            $cefcPhysicalStatus = lookup($5, cefcPhysicalStatus) + " ( " + $5 + " )"
            $entPhysicalIndex = extract($OID1, "\.([0-9]+)$")
            
            $OS_EventId = "SNMPTRAP-cisco-CISCO-ENTITY-FRU-CONTROL-MIB-cefcUnrecognizedFRU"
            
            @AlertGroup = "FRU Insert/Remove"
            @AlertKey = "cefcPhysicalEntry." + $entPhysicalIndex
            @Summary = $entPhysicalClass + " FRU Inserted, Product ID Not Supported"
            if(!match($3, ""))
            {
                @Summary = @Summary + "  ( " + $3 + " )"
            }
            else
            {
                @Summary = @Summary + "  ( " + @AlertKey + " )"
            }
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $entPhysicalClass = $entPhysicalClass + " ( " + $1 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($entPhysicalClass,$entPhysicalVendorType,$entPhysicalName,$entPhysicalModelName,$cefcPhysicalStatus,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "entPhysicalClass", $entPhysicalClass, "entPhysicalVendorType", $entPhysicalVendorType, "entPhysicalName", $entPhysicalName,
                 "entPhysicalModelName", $entPhysicalModelName, "cefcPhysicalStatus", $cefcPhysicalStatus, "entPhysicalIndex", $entPhysicalIndex)

        case "6": ### cefcFanTrayStatusChange

            ##########
            # $1 = cefcFanTrayOperStatus
            ##########

            $cefcFanTrayOperStatus = lookup($1, cefcFanTrayOperStatus) + " ( " + $1 + " )"
            $entPhysicalIndex = extract($OID1, "\.([0-9]+)$")
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cefcFanTrayOperStatus,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cefcFanTrayOperStatus", $cefcFanTrayOperStatus, "entPhysicalIndex", $entPhysicalIndex)

            $OS_EventId = "SNMPTRAP-cisco-CISCO-ENTITY-FRU-CONTROL-MIB-cefcFanTrayStatusChange"
            
            @AlertGroup = "Fan Tray Status"
            @AlertKey = "cefcFanTrayStatusEntry." + $entPhysicalIndex
            switch($1)
            {
                case "1": ### unknown
                    @Summary = "Fan Tray Status Unknown"
                    
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "2": ### up
                    @Summary = "Fan Tray Powered On"
                    
                    $SEV_KEY = $OS_EventId + "_up"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    
                case "3": ### down
                    @Summary = "Fan Tray Powered Down"
                    
                    $SEV_KEY = $OS_EventId + "_down"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                case "4": ### warning
                    @Summary = "Fan Tray Partial Failure, Needs Replacement ASAP"
                    
                    $SEV_KEY = $OS_EventId + "_warning"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
                default:
                    @Summary = "Fan Tray Status Unknown"
                    
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                    
            }
            @Summary = @Summary + "  ( " + @AlertKey + " )"
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $1

        case "7": ### cefcPowerSupplyOutputChange

            ##########
            # $1 = entPhysicalName
            # $2 = entPhysicalModelName
            # $3 = cefcPSOutputModeCurrent
            ##########

            $entPhysicalName = $1
            $entPhysicalModelName = $2
            $cefcPSOutputModeCurrent = $3

            $OS_EventId = "SNMPTRAP-cisco-CISCO-ENTITY-FRU-CONTROL-MIB-cefcPowerSupplyOutputChange"

            @AlertGroup = "Power Supply Output Status"
   
            $entPhysicalIndex = extract($OID1, "\.([0-9]+)$")

            @AlertKey = "cefcPhysicalEntry." + $entPhysicalIndex

            @Summary = "Power supply output capacity has changed" 

            if(!match($1, ""))
            {
                @Summary = @Summary + " ( " + $1 + " ) "
            }
            else
            {
                @Summary = @Summary + " ( " + @AlertKey + " ) "
            }

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($entPhysicalName,$entPhysicalModelName,$cefcPSOutputModeCurrent,$entPhysicalIndex )
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "entPhysicalName", $entPhysicalName, "entPhysicalModelName", $entPhysicalModelName, "cefcPSOutputModeCurrent", $cefcPSOutputModeCurrent,
                 "entPhysicalIndex", $entPhysicalIndex)


        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-ENTITY-FRU-CONTROL-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-ENTITY-FRU-CONTROL-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-ENTITY-FRU-CONTROL-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-ENTITY-FRU-CONTROL-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-ENTITY-FRU-CONTROL-MIB.include.snmptrap.rules >>>>>")
