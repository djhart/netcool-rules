###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 2.0 - Trap(s) added
#
#          -  ccsSpecMgmtNotification
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-CABLE-SPECTRUM-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.114.2": ### Cable Spectrum Management for DOCSIS-compliant Cable Modem Termination Systems (CMTS) - Notifications from CISCO-CABLE-SPECTRUM-MIB (200610100000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-CABLE-SPECTRUM-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Cisco-CMTS Cable Spectrum Management"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### ccsHoppingNotification

            ##########
            # $1 = ccsUpSpecMgmtHopCondition 
            # $2 = ccsUpSpecMgmtFromCenterFreq 
            # $3 = ccsUpSpecMgmtToCenterFreq 
            # $4 = ccsUpSpecMgmtFromBandWidth 
            # $5 = ccsUpSpecMgmtToBandWidth 
            # $6 = ccsUpSpecMgmtFromModProfile 
            # $7 = ccsUpSpecMgmtToModProfile 
            ##########

            $ccsUpSpecMgmtHopCondition = lookup($1, ccsUpSpecMgmtHopCondition)
            $ccsUpSpecMgmtFromCenterFreq = $2 + " KHz"
            $ccsUpSpecMgmtToCenterFreq = $3 + " KHz"
            $ccsUpSpecMgmtFromBandWidth = $4 + " KHz"
            $ccsUpSpecMgmtToBandWidth = $5 + " KHz"
            $ccsUpSpecMgmtFromModProfile = $6
            $ccsUpSpecMgmtToModProfile = $7
            $ifIndex = extract($OID1, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-cisco-CISCO-CABLE-SPECTRUM-MIB-ccsHoppingNotification"

            @AlertGroup = "Cable Spectrum Frequency Hopping Status"
            @AlertKey = "ccsUpSpecMgmtEntry." + $ifIndex
            @Summary = "Cable Spectrum Frequency Hopping, " + $ccsUpSpecMgmtHopCondition + "  ( " + @AlertKey + " )"
            switch($1)
            {
                case "0": ### snr
                    $SEV_KEY = $OS_EventId + "_snr"
                   
                    $DEFAULT_severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "1": ### stationMaintainenceMiss 
                    $SEV_KEY = $OS_EventId + "_stationMaintainenceMiss"
                   
                    $DEFAULT_severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "2": ### others 
                    $SEV_KEY = $OS_EventId + "_others"
                   
                    $DEFAULT_severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                default: ### unknown 
                    $SEV_KEY = $OS_EventId + "_unknown"
                   
                    $DEFAULT_severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $1

	    $ccsUpSpecMgmtHopCondition = $ccsUpSpecMgmtHopCondition + " ( " + $1 + " ) "

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($ccsUpSpecMgmtHopCondition,$ccsUpSpecMgmtFromCenterFreq,$ccsUpSpecMgmtToCenterFreq,$ccsUpSpecMgmtFromBandWidth,$ccsUpSpecMgmtToBandWidth,$ccsUpSpecMgmtFromModProfile,$ccsUpSpecMgmtToModProfile,$ifIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "ccsUpSpecMgmtHopCondition", $ccsUpSpecMgmtHopCondition, "ccsUpSpecMgmtFromCenterFreq", $ccsUpSpecMgmtFromCenterFreq, "ccsUpSpecMgmtToCenterFreq", $ccsUpSpecMgmtToCenterFreq,
                 "ccsUpSpecMgmtFromBandWidth", $ccsUpSpecMgmtFromBandWidth, "ccsUpSpecMgmtToBandWidth", $ccsUpSpecMgmtToBandWidth, "ccsUpSpecMgmtFromModProfile", $ccsUpSpecMgmtFromModProfile,
                 "ccsUpSpecMgmtToModProfile", $ccsUpSpecMgmtToModProfile, "ifIndex", $ifIndex)

        case "2": ### ccsSpecMgmtNotification

            ##########
            # $1 = ccsUpSpecMgmtCriteria
            # $2 = ccsUpSpecMgmtFromCenterFreq
            # $3 = ccsUpSpecMgmtToCenterFreq
            # $4 = ccsUpSpecMgmtFromBandWidth
            # $5 = ccsUpSpecMgmtToBandWidth
            # $6 = ccsUpSpecMgmtFromModProfile
            # $7 = ccsUpSpecMgmtToModProfile
            ##########

            $ccsUpSpecMgmtCriteria = lookup($1,CcsUpSpecMgmtCriteria) 
            $ccsUpSpecMgmtFromCenterFreq = $2
            $ccsUpSpecMgmtToCenterFreq = $3
            $ccsUpSpecMgmtFromBandWidth = $4
            $ccsUpSpecMgmtToBandWidth = $5
            $ccsUpSpecMgmtFromModProfile = $6
            $ccsUpSpecMgmtToModProfile = $7

            $OS_EventId = "SNMPTRAP-cisco-CISCO-CABLE-SPECTRUM-MIB-ccsSpecMgmtNotification"

            @AlertGroup = "Spectrum Management Frequency Hopping Status"

            $ifIndex = extract($OID1, "\.([0-9]+)$")

            @AlertKey = "ccsUpSpecMgmtEntry." + $ifIndex

            @Summary = "Frequency, modulation or profile changed, " + $ccsUpSpecMgmtCriteria + " ( " + @AlertKey + " ) " 

            switch($1)
            {
             case "1": ### snrBelowThres

                $SEV_KEY = $OS_EventId + "_snrBelowThres"
                $DEFAULT_Severity = 3
                $DEFAULT_Type = 1
                $DEFAULT_ExpireTime = 0

             case "2": ### cnrBelowThres

                $SEV_KEY = $OS_EventId + "_cnrBelowThres"
                $DEFAULT_Severity = 3
                $DEFAULT_Type = 1
                $DEFAULT_ExpireTime = 0

             case "4": ### corrFecAboveThres

                $SEV_KEY = $OS_EventId + "_corrFecAboveThres"
                $DEFAULT_Severity = 3
                $DEFAULT_Type = 1
                $DEFAULT_ExpireTime = 0

             case "8": ### uncorrFecAboveThres

                $SEV_KEY = $OS_EventId + "_uncorrFecAboveThres"
                $DEFAULT_Severity = 3
                $DEFAULT_Type = 1
                $DEFAULT_ExpireTime = 0

             case "16": ### snrAboveThres

                $SEV_KEY = $OS_EventId + "_snrAboveThres"
                $DEFAULT_Severity = 3
                $DEFAULT_Type = 1
                $DEFAULT_ExpireTime = 0

             case "32": ### cnrAboveThres

                $SEV_KEY = $OS_EventId + "_cnrAboveThres"
                $DEFAULT_Severity = 3
                $DEFAULT_Type = 1
                $DEFAULT_ExpireTime = 0

             case "64": ### corrFecBelowThres

                $SEV_KEY = $OS_EventId + "_corrFecBelowThres"
                $DEFAULT_Severity = 3
                $DEFAULT_Type = 1
                $DEFAULT_ExpireTime = 0

             case "128": ### uncorrFecBelowThres

                $SEV_KEY = $OS_EventId + "_uncorrFecBelowThres"
                $DEFAULT_Severity = 3
                $DEFAULT_Type = 1
                $DEFAULT_ExpireTime = 0

             case "256": ### noActiveModem

                $SEV_KEY = $OS_EventId + "_noActiveModem"
                $DEFAULT_Severity = 3
                $DEFAULT_Type = 1
                $DEFAULT_ExpireTime = 0

             case "512": ### uncorrFecAboveSecondThres

                $SEV_KEY = $OS_EventId + "_uncorrFecAboveSecondThres"
                $DEFAULT_Severity = 3
                $DEFAULT_Type = 1
                $DEFAULT_ExpireTime = 0

             case "1024": ###  others

                $SEV_KEY = $OS_EventId + "_others"
                $DEFAULT_Severity = 2
                $DEFAULT_Type = 1
                $DEFAULT_ExpireTime = 0

             default:

                $SEV_KEY = $OS_EventId + "_unknown"
                $DEFAULT_Severity = 2
                $DEFAULT_Type = 1
                $DEFAULT_ExpireTime = 0
            }

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
 
            $ccsUpSpecMgmtCriteria = $ccsUpSpecMgmtCriteria + " ( " + $1 + " ) "

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($ccsUpSpecMgmtCriteria,$ccsUpSpecMgmtFromCenterFreq,$ccsUpSpecMgmtToCenterFreq,$ccsUpSpecMgmtFromBandWidth,$ccsUpSpecMgmtToBandWidth,$ccsUpSpecMgmtFromModProfile,$ccsUpSpecMgmtToModProfile,$ifIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "ccsUpSpecMgmtCriteria", $ccsUpSpecMgmtCriteria, "ccsUpSpecMgmtFromCenterFreq", $ccsUpSpecMgmtFromCenterFreq, "ccsUpSpecMgmtToCenterFreq", $ccsUpSpecMgmtToCenterFreq,
                 "ccsUpSpecMgmtFromBandWidth", $ccsUpSpecMgmtFromBandWidth, "ccsUpSpecMgmtToBandWidth", $ccsUpSpecMgmtToBandWidth, "ccsUpSpecMgmtFromModProfile", $ccsUpSpecMgmtFromModProfile,
                 "ccsUpSpecMgmtToModProfile", $ccsUpSpecMgmtToModProfile, "ifIndex", $ifIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-CABLE-SPECTRUM-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-CABLE-SPECTRUM-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-CABLE-SPECTRUM-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-CABLE-SPECTRUM-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-CABLE-SPECTRUM-MIB.include.snmptrap.rules >>>>>")
