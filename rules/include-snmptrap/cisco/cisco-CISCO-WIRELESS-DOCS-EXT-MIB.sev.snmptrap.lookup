###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-WIRELESS-DOCS-EXT-MIB
#
# 1.1 - Changed the format
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table cisco-CISCO-WIRELESS-DOCS-EXT-MIB_sev =
{
	    {"SNMPTRAP-cisco-CISCO-WIRELESS-DOCS-EXT-MIB-cwdxHeSuOnOffNotification_offline","3","1","0"},
	    {"SNMPTRAP-cisco-CISCO-WIRELESS-DOCS-EXT-MIB-cwdxHeSuOnOffNotification_others","2","1","0"},
	    {"SNMPTRAP-cisco-CISCO-WIRELESS-DOCS-EXT-MIB-cwdxHeSuOnOffNotification_initRangingRcvd","2","1","0"},
	    {"SNMPTRAP-cisco-CISCO-WIRELESS-DOCS-EXT-MIB-cwdxHeSuOnOffNotification_initDhcpReqRcvd","1","2","0"},
	    {"SNMPTRAP-cisco-CISCO-WIRELESS-DOCS-EXT-MIB-cwdxHeSuOnOffNotification_onlineNetAccessDisabled","2","1","0"},
	    {"SNMPTRAP-cisco-CISCO-WIRELESS-DOCS-EXT-MIB-cwdxHeSuOnOffNotification_onlineKekAssigned","1","2","0"},
	    {"SNMPTRAP-cisco-CISCO-WIRELESS-DOCS-EXT-MIB-cwdxHeSuOnOffNotification_onlineTekAssigned","1","2","0"},
	    {"SNMPTRAP-cisco-CISCO-WIRELESS-DOCS-EXT-MIB-cwdxHeSuOnOffNotification_rejectBadMic","3","1","0"},
	    {"SNMPTRAP-cisco-CISCO-WIRELESS-DOCS-EXT-MIB-cwdxHeSuOnOffNotification_rejectBadCos","3","1","0"},
	    {"SNMPTRAP-cisco-CISCO-WIRELESS-DOCS-EXT-MIB-cwdxHeSuOnOffNotification_kekRejected","3","1","0"},
	    {"SNMPTRAP-cisco-CISCO-WIRELESS-DOCS-EXT-MIB-cwdxHeSuOnOffNotification_tekRejected","3","1","0"},
	    {"SNMPTRAP-cisco-CISCO-WIRELESS-DOCS-EXT-MIB-cwdxHeSuOnOffNotification_online","1","2","0"},
	    {"SNMPTRAP-cisco-CISCO-WIRELESS-DOCS-EXT-MIB-cwdxHeSuOnOffNotification_initTftpPacketRcvd","1","2","0"},
	    {"SNMPTRAP-cisco-CISCO-WIRELESS-DOCS-EXT-MIB-cwdxHeSuOnOffNotification_initTodRquestRcvd","2","1","0"},
	    {"SNMPTRAP-cisco-CISCO-WIRELESS-DOCS-EXT-MIB-cwdxHeSuOnOffNotification_unknown","2","1","0"},
	    {"SNMPTRAP-cisco-CISCO-WIRELESS-DOCS-EXT-MIB-cwdxHeSuChOverNotification_messageSent","1","2","0"},
	    {"SNMPTRAP-cisco-CISCO-WIRELESS-DOCS-EXT-MIB-cwdxHeSuChOverNotification_commandNotActive","2","1","0"},
	    {"SNMPTRAP-cisco-CISCO-WIRELESS-DOCS-EXT-MIB-cwdxHeSuChOverNotification_noOpNeeded","1","2","0"},
	    {"SNMPTRAP-cisco-CISCO-WIRELESS-DOCS-EXT-MIB-cwdxHeSuChOverNotification_suNotFound","3","1","0"},
	    {"SNMPTRAP-cisco-CISCO-WIRELESS-DOCS-EXT-MIB-cwdxHeSuChOverNotification_waitToSendMessage","2","1","0"},
	    {"SNMPTRAP-cisco-CISCO-WIRELESS-DOCS-EXT-MIB-cwdxHeSuChOverNotification_timeOut","3","1","0"},
	    {"SNMPTRAP-cisco-CISCO-WIRELESS-DOCS-EXT-MIB-cwdxHeSuChOverNotification_unknown","2","1","0"}
}
default = {"Unknown","Unknown","Unknown"}
