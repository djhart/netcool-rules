###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
# Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
# - CISCO-LWAPP-WEBAUTH-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.515": ### Cisco Light Weight Access Point Web Authentication MIB - Notifications from CISCO-LWAPP-WEBAUTH-MIB(200703040000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-LWAPP-WEBAUTH-MIB.include.snmptrap.rules >>>>>")

    @Agent = "CISCO-LWAPP-WEBAUTH"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### cLWAGuestUserRemoved

            ##########
            # $1 = cLWAGuestUserName 
            ##########

            $cLWAGuestUserName = $1

            $OS_EventId = "SNMPTRAP-cisco-CISCO-LWAPP-WEBAUTH-MIB-cLWAGuestUserRemoved"

            @AlertGroup = "Lifetime of the guest-user expires and the guest-user's accounts are removed"
            @AlertKey = "cLWAGuestUserName." + $cLWAGuestUserName 
            @Summary = "Lifetime of the guest-user expires and the guest-user's accounts are removed ( " + @AlertKey + " )"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cLWAGuestUserName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cLWAGuestUserName", $cLWAGuestUserName)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-LWAPP-WEBAUTH-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-LWAPP-WEBAUTH-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-LWAPP-WEBAUTH-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-LWAPP-WEBAUTH-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-LWAPP-WEBAUTH-MIB.include.snmptrap.rules >>>>>")
