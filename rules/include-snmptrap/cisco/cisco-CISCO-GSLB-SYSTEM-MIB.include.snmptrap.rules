###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
# Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
# - CISCO-GSLB-SYSTEM-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.589": ### Network and system information of Global Server Load Balancer(GSLB) as a network device - Notifications from CISCO-GSLB-SYSTEM-MIB (200612040000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-GSLB-SYSTEM-MIB.include.snmptrap.rules >>>>>")

    @Agent = "CISCO-GSLB-SYSTEM"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### ciscoGslbSystemPeerEventStatus

            ##########
            # $1 = sysName 
            # $2 = cgsPeerDnsName 
            # $3 = cgsPeerService 
            # $4 = cgsPeerPrevStatus 
            # $5 = cgsPeerStatus 
            ##########

            $sysName = $1
            $cgsPeerDnsName = $2
            $cgsPeerService = lookup($3, ciscoGslbNodeServices)
            $cgsPeerPrevStatus = lookup($4, ciscoGslbPeerStatus)
            $cgsPeerStatus = lookup($5, ciscoGslbPeerStatus)

            $cgsPeerAddressType = extract($OID2, "\.([0-9]+)\.[0-9]+$")
            $cgsPeerAddress = extract($OID2, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-cisco-CISCO-GSLB-SYSTEM-MIB-ciscoGslbSystemPeerEventStatus"

            @AlertGroup = "GSLB peer device status"
            @AlertKey = "cgsPeerEntry." + $cgsPeerAddressType + "." + $cgsPeerAddress

            switch($5)
            {
                        case "1": ### inactive
                            $SEV_KEY = $OS_EventId + "_inactive"
                            @Summary = $sysName + " " + $cgsPeerDnsName + " " + $cgsPeerService + " GSLB peer device is waiting to be activated by the primary GSLB device registered with ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "2": ### offline
                            $SEV_KEY = $OS_EventId + "_offline"
                            @Summary = $sysName + " " + $cgsPeerDnsName + " " + $cgsPeerService + " GSLB device which is out-of-service ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "3": ### online
                            $SEV_KEY = $OS_EventId + "_online"
                            @Summary = $sysName + " " + $cgsPeerDnsName + " " + $cgsPeerService + " GSLB device which is in service ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 1
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0

                        default:
                            $SEV_KEY = $OS_EventId + "_unknown"
                            @Summary = $sysName + " " + $cgsPeerDnsName + " " + $cgsPeerService + " GSLB device status is unknown ( " + @AlertKey + " )"
                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0
            }


            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            $cgsPeerService = $cgsPeerService + " ( " + $3 + " )"
            $cgsPeerPrevStatus = $cgsPeerPrevStatus + " ( " + $4 + " )"
            $cgsPeerStatus = $cgsPeerStatus  + " ( " + $5 + " )"

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($sysName,$cgsPeerDnsName,$cgsPeerService,$cgsPeerPrevStatus,$cgsPeerStatus,$cgsPeerAddressType,$cgsPeerAddress)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "sysName", $sysName, "cgsPeerDnsName", $cgsPeerDnsName, "cgsPeerService", $cgsPeerService,
                 "cgsPeerPrevStatus", $cgsPeerPrevStatus, "cgsPeerStatus", $cgsPeerStatus, "cgsPeerAddressType", $cgsPeerAddressType,
                 "cgsPeerAddress", $cgsPeerAddress)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-GSLB-SYSTEM-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-GSLB-SYSTEM-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-GSLB-SYSTEM-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-GSLB-SYSTEM-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-GSLB-SYSTEM-MIB.include.snmptrap.rules >>>>>")
