###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-ATM-PVCTRAP-EXTN-MIB
#
###############################################################################
#
# 1.1 - Added basic debug logging.
#
###############################################################################

log(DEBUG, "<<<<< Entering... cisco-CISCO-ATM-PVCTRAP-EXTN-MIB.adv.include.snmptrap.rules >>>>>")

switch($specific-trap)
{
    case "1": ### catmIntfPvcUpTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1011
        $OS_X733SpecificProb = "catmIntfPvcUpTrap"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "atmInterfaceConfEntry." + $1
        $OS_LocalSecObj = "catmInterfaceExt2Entry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 2
        $VAR_RelateLSO2LPO = 2

    case "2": ### catmIntfPvcOAMFailureTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1011
        $OS_X733SpecificProb = "catmIntfPvcOAMFailureTrap"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "atmInterfaceConfEntry." + $1
        $OS_LocalSecObj = "catmInterfaceExt2Entry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 2
        $VAR_RelateLSO2LPO = 2

    case "3": ### catmIntfPvcSegCCOAMFailureTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1011
        $OS_X733SpecificProb = "catmIntfPvcSegCCOAMFailureTrap"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "atmInterfaceConfEntry." + $1
        $OS_LocalSecObj = "catmInterfaceExt2Entry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 2
        $VAR_RelateLSO2LPO = 2

    case "4": ### catmIntfPvcEndCCOAMFailureTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1011
        $OS_X733SpecificProb = "catmIntfPvcEndCCOAMFailureTrap"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "atmInterfaceConfEntry." + $1
        $OS_LocalSecObj = "catmInterfaceExt2Entry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 2
        $VAR_RelateLSO2LPO = 2

    case "5": ### catmIntfPvcAISRDIOAMFailureTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1011
        $OS_X733SpecificProb = "catmIntfPvcAISRDIOAMFailureTrap"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "atmInterfaceConfEntry." + $1
        $OS_LocalSecObj = "catmInterfaceExt2Entry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 2
        $VAR_RelateLSO2LPO = 2

    case "6": ### catmIntfPvcAnyOAMFailureTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1011
        $OS_X733SpecificProb = "catmIntfPvcAnyOAMFailureTrap"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "atmInterfaceConfEntry." + $1
        $OS_LocalSecObj = "catmInterfaceExt2Entry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 2
        $VAR_RelateLSO2LPO = 2

    case "7": ### catmIntfPvcOAMRecoverTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1011
        $OS_X733SpecificProb = "catmIntfPvcOAMRecoverTrap"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "atmInterfaceConfEntry." + $1
        $OS_LocalSecObj = "catmInterfaceExt2Entry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 2
        $VAR_RelateLSO2LPO = 2

    case "8": ### catmIntfPvcSegCCOAMRecoverTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1011
        $OS_X733SpecificProb = "catmIntfPvcSegCCOAMRecoverTrap"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "atmInterfaceConfEntry." + $1
        $OS_LocalSecObj = "catmInterfaceExt2Entry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 2
        $VAR_RelateLSO2LPO = 2

    case "9": ### catmIntfPvcEndCCOAMRecoverTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1011
        $OS_X733SpecificProb = "catmIntfPvcEndCCOAMRecoverTrap"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "atmInterfaceConfEntry." + $1
        $OS_LocalSecObj = "catmInterfaceExt2Entry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 2
        $VAR_RelateLSO2LPO = 2

    case "10": ### catmIntfPvcAISRDIOAMRecoverTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1011
        $OS_X733SpecificProb = "catmIntfPvcAISRDIOAMRecoverTrap"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "atmInterfaceConfEntry." + $1
        $OS_LocalSecObj = "catmInterfaceExt2Entry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 2
        $VAR_RelateLSO2LPO = 2

    case "11": ### catmIntfPvcAnyOAMRecoverTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1011
        $OS_X733SpecificProb = "catmIntfPvcAnyOAMRecoverTrap"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "atmInterfaceConfEntry." + $1
        $OS_LocalSecObj = "catmInterfaceExt2Entry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 2
        $VAR_RelateLSO2LPO = 2

    case "12": ### catmIntfPvcUp2Trap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1011
        $OS_X733SpecificProb = "catmIntfPvcUp2Trap"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "atmInterfaceConfEntry." + $1
        $OS_LocalSecObj = "catmInterfaceExt2Entry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 2
        $VAR_RelateLSO2LPO = 2

    case "13": ### catmIntfPvcDownTrap

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1011
        $OS_X733SpecificProb = "catmIntfPvcDownTrap"
        $OS_OsiLayer = 2
        
        $OS_LocalPriObj = "atmInterfaceConfEntry." + $1
        $OS_LocalSecObj = "atmInterfaceExtEntry." + $1
        $OS_LocalRootObj = "ifEntry." + $1
        $VAR_RelateLRO2LPO = 2
        $VAR_RelateLRO2LSO = 2
        $VAR_RelateLSO2LPO = 2

    default:
}

log(DEBUG, "<<<<< Leaving... cisco-CISCO-ATM-PVCTRAP-EXTN-MIB.adv.include.snmptrap.rules >>>>>")
