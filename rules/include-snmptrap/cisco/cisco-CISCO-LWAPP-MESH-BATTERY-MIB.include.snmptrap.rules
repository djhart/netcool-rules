###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-LWAPP-MESH-BATTERY-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.620": ### Cisco Light Weight Access Point Protocol Mesh Battery - Notifications from CISCO-LWAPP-MESH-BATTERY-MIB (200703130000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-LWAPP-MESH-BATTERY-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Cisco-LWAPP-MESH-BATTERY-MIB"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### ciscoLwappMeshBatteryAlarm

            ##########
            # $1 = cLApName 
            # $2 = clMeshNodeBatteryStatus 
            ##########

            $cLApName = $1
            $clMeshNodeBatteryStatus = lookup($2, ClMeshNodeBatteryStatus)

            $OS_EventId = "SNMPTRAP-cisco-CISCO-LWAPP-MESH-BATTERY-MIB-ciscoLwappMeshBatteryAlarm"

            @AlertGroup = "Battery Status"

            $cLApSysMacAddress_Raw = extract($OID2, "3\.6\.1\.4\.1\.9\.9\.620\.1\.1\.1\.1\.2\.(.*)$")

            $MacAddrOid = $cLApSysMacAddress_Raw

            include "$NC_RULES_HOME/include-snmptrap/decodeOid2MacAddr.include.snmptrap.rules"

            $cLApSysMacAddress = $MacAddrHex

            @AlertKey = "clMeshNodeBatteryEntry." + $cLApSysMacAddress_Raw

            switch($2)
            {
                case "2":### normal
                    $SEV_KEY = $OS_EventId + "_normal"
                    @Summary = "Battery is Operating Normally"

                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
					
                case "3":### overloaded
                    $SEV_KEY = $OS_EventId + "_overloaded"
                    @Summary = "Battery is Overloaded"

                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
					
                case "4":### low
                    $SEV_KEY = $OS_EventId + "_low"
                    @Summary = "Battery Has Low Voltage"

                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
					
                case "5":### acfail
                    $SEV_KEY = $OS_EventId + "_acfail"
                    @Summary = "External AC Power Supply Has Failed"

                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "6":### replace
                    $SEV_KEY = $OS_EventId + "_replace"
                    @Summary = "Battery Must Be Replaced"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800
					
                case "7":### missing
                    $SEV_KEY = $OS_EventId + "_missing"
                    @Summary = "One or More Batteries in Battery String is Missing"

                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
					
                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = "Battery Status Unknown"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            if(!match($1, ""))
            {
                @Summary = @Summary + " ( Access Point: " + $1 + " ) "
            }
            else
            {
                @Summary = @Summary + " ( " + @AlertKey + " ) "
            }

            $clMeshNodeBatteryStatus = $clMeshNodeBatteryStatus + " ( " + $2 + " ) "

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cLApName,$clMeshNodeBatteryStatus,$cLApSysMacAddress)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cLApName", $cLApName, "clMeshNodeBatteryStatus", $clMeshNodeBatteryStatus, "cLApSysMacAddress", $cLApSysMacAddress)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-LWAPP-MESH-BATTERY-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-LWAPP-MESH-BATTERY-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-LWAPP-MESH-BATTERY-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-LWAPP-MESH-BATTERY-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-LWAPP-MESH-BATTERY-MIB.include.snmptrap.rules >>>>>")
