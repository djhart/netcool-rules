###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
# Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
# - CISCO-LWAPP-MOBILITY-MIB
#
###############################################################################

case ".1.3.6.1.4.1.9.9.576": ### CISCO MIB implemented on devices operating as Central Controllers (CC) that terminate the Light Weight Access Point Protocol tunnel from Light-weight LWAPP Access Points - Notifications from CISCO-LWAPP-MOBILITY-MIB (200607190000Z)

    log(DEBUG, "<<<<< Entering... cisco-CISCO-LWAPP-MOBILITY-MIB.include.snmptrap.rules >>>>>")

    @Agent = "CISCO-LWAPP-MOBILITY"
    @Class = "40057"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### ciscoLwappMobilityAnchorControlPathDown

            ##########
            # $1 = cLMobilityAnchorAddressType 
            # $2 = cLMobilityAnchorAddress 
            ##########

            $cLMobilityAnchorAddressType = lookup($1, InetAddressType)
            $cLMobilityAnchorAddress = $2

            $OS_EventId = "SNMPTRAP-cisco-CISCO-LWAPP-MOBILITY-MIB-ciscoLwappMobilityAnchorControlPathDown"

            @AlertGroup = "Cisco Lwapp Mobility Anchor Control Path Status"
            @AlertKey = ""
            @Summary = "Cisco Lwapp Mobility Anchor Control Path Path Down, address : " + $cLMobilityAnchorAddressType + " " + $cLMobilityAnchorAddress

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            $cLMobilityAnchorAddressType = $cLMobilityAnchorAddressType + " ( " + $1 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cLMobilityAnchorAddressType,$cLMobilityAnchorAddress)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cLMobilityAnchorAddressType", $cLMobilityAnchorAddressType, "cLMobilityAnchorAddress", $cLMobilityAnchorAddress)

        case "2": ### ciscoLwappMobilityAnchorControlPathUp

            ##########
            # $1 = cLMobilityAnchorAddressType 
            # $2 = cLMobilityAnchorAddress 
            ##########

            $cLMobilityAnchorAddressType = lookup($1, InetAddressType)
            $cLMobilityAnchorAddress = $2

            $OS_EventId = "SNMPTRAP-cisco-CISCO-LWAPP-MOBILITY-MIB-ciscoLwappMobilityAnchorControlPathUp"

            @AlertGroup = "Cisco Lwapp Mobility Anchor Control Path Status"
            @AlertKey = ""
            @Summary = "Cisco Lwapp Mobility Anchor Control Path Path Up, address : " + $cLMobilityAnchorAddressType + " " + $cLMobilityAnchorAddress

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            $cLMobilityAnchorAddressType = $cLMobilityAnchorAddressType + " ( " + $1 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cLMobilityAnchorAddressType,$cLMobilityAnchorAddress)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cLMobilityAnchorAddressType", $cLMobilityAnchorAddressType, "cLMobilityAnchorAddress", $cLMobilityAnchorAddress)

        case "3": ### ciscoLwappMobilityAnchorDataPathDown

            ##########
            # $1 = cLMobilityAnchorAddressType 
            # $2 = cLMobilityAnchorAddress 
            ##########

            $cLMobilityAnchorAddressType = lookup($1, InetAddressType)
            $cLMobilityAnchorAddress = $2

            $OS_EventId = "SNMPTRAP-cisco-CISCO-LWAPP-MOBILITY-MIB-ciscoLwappMobilityAnchorDataPathDown"

            @AlertGroup = "Cisco Lwapp Mobility Anchor Data Path Status"
            @AlertKey = ""
            @Summary = "Cisco Lwapp Mobility Anchor Data Path Path Down, address : " + $cLMobilityAnchorAddressType + " " + $cLMobilityAnchorAddress

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            $cLMobilityAnchorAddressType = $cLMobilityAnchorAddressType + " ( " + $1 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cLMobilityAnchorAddressType,$cLMobilityAnchorAddress)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cLMobilityAnchorAddressType", $cLMobilityAnchorAddressType, "cLMobilityAnchorAddress", $cLMobilityAnchorAddress)

        case "4": ### ciscoLwappMobilityAnchorDataPathUp

            ##########
            # $1 = cLMobilityAnchorAddressType 
            # $2 = cLMobilityAnchorAddress 
            ##########

            $cLMobilityAnchorAddressType = lookup($1, InetAddressType)
            $cLMobilityAnchorAddress = $2

            $OS_EventId = "SNMPTRAP-cisco-CISCO-LWAPP-MOBILITY-MIB-ciscoLwappMobilityAnchorDataPathUp"

            @AlertGroup = "Cisco Lwapp Mobility Anchor Data Path Status"
            @AlertKey = ""
            @Summary = "Cisco Lwapp Mobility Anchor Data Path Path Up, address : " + $cLMobilityAnchorAddressType + " " + $cLMobilityAnchorAddress

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            $cLMobilityAnchorAddressType = $cLMobilityAnchorAddressType + " ( " + $1 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cLMobilityAnchorAddressType,$cLMobilityAnchorAddress)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cLMobilityAnchorAddressType", $cLMobilityAnchorAddressType, "cLMobilityAnchorAddress", $cLMobilityAnchorAddress)

        case "5": ### ciscoLwappMobilityAllAnchorsOnWlanDown

            ##########
            # $1 = cLMobilityAnchorWlanId 
            ##########

            $cLMobilityAnchorWlanId = $1

            $OS_EventId = "SNMPTRAP-cisco-CISCO-LWAPP-MOBILITY-MIB-ciscoLwappMobilityAllAnchorsOnWlanDown"

            @AlertGroup = "Cisco Lwapp Mobility All Anchors On Wlan Satus"
            @AlertKey = ""
            @Summary = "Cisco Lwapp Mobility All Anchors On Wlan Down, ID : " + $cLMobilityAnchorWlanId 

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cLMobilityAnchorWlanId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cLMobilityAnchorWlanId", $cLMobilityAnchorWlanId)

        case "6": ### ciscoLwappMobilityOneAnchorOnWlanUp

            ##########
            # $1 = cLMobilityAnchorWlanId 
            ##########

            $cLMobilityAnchorWlanId = $1

            $OS_EventId = "SNMPTRAP-cisco-CISCO-LWAPP-MOBILITY-MIB-ciscoLwappMobilityOneAnchorOnWlanUp"

            @AlertGroup = "Cisco Lwapp Mobility All Anchors On Wlan Satus"
            @AlertKey = ""
            @Summary = "Cisco Lwapp Mobility All Anchors On Wlan Up, ID : " + $cLMobilityAnchorWlanId 

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($cLMobilityAnchorWlanId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "cLMobilityAnchorWlanId", $cLMobilityAnchorWlanId)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_cisco, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, cisco-CISCO-LWAPP-MOBILITY-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, cisco-CISCO-LWAPP-MOBILITY-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-LWAPP-MOBILITY-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/cisco/cisco-CISCO-LWAPP-MOBILITY-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... cisco-CISCO-LWAPP-MOBILITY-MIB.include.snmptrap.rules >>>>>")
