###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  CISCO-LWAPP-MESH-MIB
#
###############################################################################

log(DEBUG, "<<<<< Entering... cisco-CISCO-LWAPP-MESH-MIB.adv.include.snmptrap.rules >>>>>")

switch($specific-trap)
{
    case "1": ### ciscoLwappMeshAuthFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoLwappMeshAuthFailure"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "LWAPP AP MAC Addr: " + $clMeshNodeMacAddress
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "2": ### ciscoLwappMeshChildExcludedParent

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoLwappMeshChildExcludedParent"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "cLApEntry." + $cLApSysMacAddress_Raw
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "3": ### ciscoLwappMeshParentChange

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoLwappMeshParentChange"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "cLApEntry." + $cLApSysMacAddress_Raw
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "4": ### ciscoLwappMeshChildMoved

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoLwappMeshChildMoved"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "cLApEntry." + $cLApSysMacAddress_Raw
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

        $OS_RemotePriObj = "clMeshNeighborEntry." + $cLApSysMacAddress + "." + $clMeshNeighborMacAddress
        $OS_RemoteRootObj = "cLApEntry." + $cLApSysMacAddress
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "5": ### ciscoLwappMeshExcessiveParentChange

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoLwappMeshExcessiveParentChange"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "cLApEntry." + $cLApSysMacAddress_Raw
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

        $OS_RemotePriObj = "clMeshNeighborEntry." + $cLApSysMacAddress + "." + $clMeshNeighborMacAddress
        $OS_RemoteRootObj = "cLApEntry." + $cLApSysMacAddress
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "6": ### ciscoLwappMeshOnsetSNR

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoLwappMeshOnsetSNR"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "cLApEntry." + $cLApSysMacAddress_Raw 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

        $OS_RemotePriObj = "clMeshNeighborEntry." + $cLApSysMacAddress + "." + $clMeshNeighborMacAddress
        $OS_RemoteRootObj = "cLApEntry." + $cLApSysMacAddress
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "7": ### ciscoLwappMeshAbateSNR

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoLwappMeshAbateSNR"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "cLApEntry." + $cLApSysMacAddress_Raw 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

        $OS_RemotePriObj = "clMeshNeighborEntry." + $cLApSysMacAddress + "." + $clMeshNeighborMacAddress
        $OS_RemoteRootObj = "cLApEntry." + $cLApSysMacAddress
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "8": ### ciscoLwappMeshConsoleLogin

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ciscoLwappMeshConsoleLogin"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "cLApEntry." + $cLApSysMacAddress_Raw 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    default:
}

log(DEBUG, "<<<<< Leaving... cisco-CISCO-LWAPP-MESH-MIB.adv.include.snmptrap.rules >>>>>")
