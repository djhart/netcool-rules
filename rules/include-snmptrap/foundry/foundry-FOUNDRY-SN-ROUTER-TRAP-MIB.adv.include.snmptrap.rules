###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  FOUNDRY-SN-ROUTER-TRAP-MIB
#
###############################################################################

log(DEBUG, "<<<<< Entering... foundry-FOUNDRY-SN-ROUTER-TRAP-MIB.adv.include.snmptrap.rules >>>>>")

switch($specific-trap)
{
    case "3": ### snTrapOspfIfStateChange

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "snTrapOspfIfStateChange"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "snOspfIfStatusEntry." + $snOspfIfStatusEntryIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "4": ### snTrapOspfVirtIfStateChange

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "snTrapOspfVirtIfStateChange"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "snOspfVirtIfStatusEntry." + $snOspfVirtIfStatusEntryIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "5": ### snOspfNbrStateChange

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "snOspfNbrStateChange"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "snOspfNbrEntry." + $snOspfNbrEntryIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "6": ### snOspfVirtNbrStateChange

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "snOspfVirtNbrStateChange"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "snOspfVirtNbrEntry." + $snOspfVirtNbrEntryIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "7": ### snOspfIfConfigError

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "snOspfIfConfigError"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "snOspfIfStatusEntry." + $snOspfIfStatusEntryIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "8": ### snOspfVirtIfConfigError

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "snOspfVirtIfConfigError"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "snOspfVirtIfStatusEntry." + $snOspfVirtIfStatusEntryIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "9": ### snOspfIfAuthFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "snOspfIfAuthFailure"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "snOspfIfStatusEntry." + $snOspfIfStatusEntryIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "10": ### snOspfVirtIfAuthFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "snOspfVirtIfAuthFailure"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "snOspfVirtIfStatusEntry." + $snOspfVirtIfStatusEntryIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "11": ### snOspfIfRxBadPacket

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "snOspfIfRxBadPacket"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "snOspfIfStatusEntry." + $snOspfIfStatusEntryIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "12": ### snOspfVirtIfRxBadPacket

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "snOspfVirtIfRxBadPacket"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "snOspfVirtIfStatusEntry." + $snOspfVirtIfStatusEntryIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "13": ### snOspfTxRetransmit

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "snOspfTxRetransmit"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "snOspfIfStatusEntry." + $snOspfIfStatusEntryIndex + ", snOspfNbrEntry." + $snOspfNbrEntryIndex + ", snOspfLsdbEntry." + $snOspfLsdbEntryIndex
        $OS_LocalSecObj = "snOspfIfStatusEntry." + $snOspfIfStatusEntryIndex + ", snOspfNbrEntry." + $snOspfNbrEntryIndex
        $OS_LocalRootObj = "snOspfIfStatusEntry." + $snOspfIfStatusEntryIndex
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 3
        $VAR_RelateLSO2LPO = 3
        
    case "14": ### ospfVirtIfTxRetransmit

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "ospfVirtIfTxRetransmit"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "snOspfVirtIfStatusEntry." + $snOspfVirtIfStatusEntryIndex + ", snOspfLsdbEntry." + $snOspfLsdbEntry
        $OS_LocalRootObj = "snOspfVirtIfStatusEntry." + $snOspfVirtIfStatusEntryIndex
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "15": ### snOspfOriginateLsa

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "snOspfOriginateLsa"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "snOspfLsdbEntry." + $snOspfLsdbEntryIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "16": ### snOspfMaxAgeLsa

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "snOspfMaxAgeLsa"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "snOspfLsdbEntry." + $snOspfLsdbEntryIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "17": ### snOspfLsdbOverflow

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "snOspfLsdbOverflow"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Router ID : " + $snOspfRouterId
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    case "18": ### snOspfLsdbApproachingOverflow

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "snOspfLsdbApproachingOverflow"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Router ID : " + $snOspfRouterId
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0
        
    default:
}

log(DEBUG, "<<<<< Leaving... foundry-FOUNDRY-SN-ROUTER-TRAP-MIB.adv.include.snmptrap.rules >>>>>")


