###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  FOUNDRY-SN-ROUTER-NOTIFICATION-MIB
#
###############################################################################

# Commented out line below - main entry point is foundry-FOUNDRY-SN-TRAP-MIB--FOUNDRY-MPLS-MIB.include.snmptrap.rules
#case ".1.3.6.1.4.1.1991": ### - Notifications from FOUNDRY-SN-ROUTER-NOTIFICATION-MIB (200707180000Z)

log(DEBUG, "<<<<< Entering... foundry-FOUNDRY-SN-ROUTER-NOTIFICATION-MIB.include.snmptrap.rules >>>>>")

    @Agent = "foundry-FOUNDRY-SN-ROUTER-NOTIFICATION-MIB"
    @Class = "40660"
    
    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
     
        case "1020": ### snTrapNotEnoughFapLinks
        
            ##########
            # $1 = snAgentBrdIndex
            # $2 = snAgGblTrapMessage
            ##########
            
            $snAgentBrdIndex = $1
            $snAgGblTrapMessage = $2
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-ROUTER-NOTIFICATION-MIB-snTrapNotEnoughFapLinks"

            @AlertGroup = "LP FAP Links-Line Rate Status"
            @AlertKey = "snAgentBrdEntry." + $snAgentBrdIndex
            if(!match($snAgGblTrapMessage, ""))
{
	@Summary = $snAgGblTrapMessage 
}
else
{
	@Summary = "LP FAP (Fabric Adapter Processor) has not Enough Links to Maintain the Line Rate"
}

            
            @Summary = @Summary + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snAgentBrdIndex, $snAgGblTrapMessage)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snAgentBrdIndex", $snAgentBrdIndex, "snAgGblTrapMessage", $snAgGblTrapMessage)
         
        case "1021": ### snTrapEnoughFapLinks
        
            ##########
            # $1 = snAgentBrdIndex
            # $2 = snAgGblTrapMessage
            ##########
            
            $snAgentBrdIndex = $1
            $snAgGblTrapMessage = $2
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-ROUTER-NOTIFICATION-MIB-snTrapEnoughFapLinks"

            @AlertGroup = "LP FAP Links-Line Rate Status"
            @AlertKey = "snAgentBrdEntry." + $snAgentBrdIndex
            if(!match($snAgGblTrapMessage, ""))
{
	@Summary = $snAgGblTrapMessage 
}
else
{
	@Summary = "LP FAP (Fabric Adapter Processor) has re-established Enough Links to Maintain the Line Rate"
}
            
            @Summary = @Summary + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snAgentBrdIndex, $snAgGblTrapMessage)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snAgentBrdIndex", $snAgentBrdIndex, "snAgGblTrapMessage", $snAgGblTrapMessage)
         
        case "1022": ### snTrapFapEccErrors
        
            ##########
            # $1 = snAgentBrdIndex
            # $2 = snAgGblTrapMessage
            ##########
            
            $snAgentBrdIndex = $1
            $snAgGblTrapMessage = $2
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-ROUTER-NOTIFICATION-MIB-snTrapFapEccErrors"

            @AlertGroup = "LP FAP Memory Status"
            @AlertKey = "snAgentBrdEntry." + $snAgentBrdIndex
            if(!match($snAgGblTrapMessage, ""))
{
	@Summary = $snAgGblTrapMessage
}
else
{
	@Summary = "LP FAP (Fabric Adapter Processor) has memory errors (Error Correction Code)"
}
            
            @Summary = @Summary + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snAgentBrdIndex, $snAgGblTrapMessage)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snAgentBrdIndex", $snAgentBrdIndex, "snAgGblTrapMessage", $snAgGblTrapMessage)
         
        case "1023": ### snTrapFapShutdown
        
            ##########
            # $1 = snAgentBrdIndex
            # $2 = snAgGblTrapMessage
            ##########
            
            $snAgentBrdIndex = $1
            $snAgGblTrapMessage = $2
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-ROUTER-NOTIFICATION-MIB-snTrapFapShutdown"

            @AlertGroup = "LP FAP Status"
            @AlertKey = "snAgentBrdEntry." + $snAgentBrdIndex
            if(!match($snAgGblTrapMessage, ""))
{
	@Summary = $snAgGblTrapMessage 
}
else
{
	@Summary = "LP FAP (Fabric Adapter Processor) has Shutdown"
}
            
            @Summary = @Summary + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snAgentBrdIndex, $snAgGblTrapMessage)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snAgentBrdIndex", $snAgentBrdIndex, "snAgGblTrapMessage", $snAgGblTrapMessage)
         
        case "1024": ### snTrapFapClockSyncErrors
        
            ##########
            # $1 = snAgentBrdIndex
            # $2 = snAgGblTrapMessage
            ##########
            
            $snAgentBrdIndex = $1
            $snAgGblTrapMessage = $2
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-ROUTER-NOTIFICATION-MIB-snTrapFapClockSyncErrors"

            @AlertGroup = "LP FAP Clock Synchronization Status"
            @AlertKey = "snAgentBrdEntry." + $snAgentBrdIndex
            if(!match($snAgGblTrapMessage, ""))
{
	@Summary = $snAgGblTrapMessage
}
else
{
	@Summary = "LP FAP (Fabric Adapter Processor) has Clock Synchronization Errors"
}
            
            @Summary = @Summary + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snAgentBrdIndex, $snAgGblTrapMessage)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snAgentBrdIndex", $snAgentBrdIndex, "snAgGblTrapMessage", $snAgGblTrapMessage)
         
        case "1025": ### snTrapFapRegisterErrors
        
            ##########
            # $1 = snAgentBrdIndex
            # $2 = snAgGblTrapMessage
            ##########
            
            $snAgentBrdIndex = $1
            $snAgGblTrapMessage = $2
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-ROUTER-NOTIFICATION-MIB-snTrapFapRegisterErrors"

            @AlertGroup = "LP FAP Register Status"
            @AlertKey = "snAgentBrdEntry." + $snAgentBrdIndex
            if(!match($snAgGblTrapMessage, ""))
{
	@Summary = $snAgGblTrapMessage 
}
else
{
	@Summary = "LP FAP (Fabric Adapter Processor) has Register Errors"
}
            
            @Summary = @Summary + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snAgentBrdIndex, $snAgGblTrapMessage)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snAgentBrdIndex", $snAgentBrdIndex, "snAgGblTrapMessage", $snAgGblTrapMessage)
         
        case "1026": ### snTrapFapCamErrors
        
            ##########
            # $1 = snAgentBrdIndex
            # $2 = snAgGblTrapMessage
            ##########
            
            $snAgentBrdIndex = $1
            $snAgGblTrapMessage = $2
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-ROUTER-NOTIFICATION-MIB-snTrapFapCamErrors"

            @AlertGroup = "LP FAP CAM Status"
            @AlertKey = "snAgentBrdEntry." + $snAgentBrdIndex
            if(!match($snAgGblTrapMessage, ""))
{
	@Summary = $snAgGblTrapMessage 
}
else
{
	@Summary = "LP FAP (Fabric Adapter Processor) has CAM Errors"
}
            
            @Summary = @Summary + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snAgentBrdIndex, $snAgGblTrapMessage)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snAgentBrdIndex", $snAgentBrdIndex, "snAgGblTrapMessage", $snAgGblTrapMessage)
         
        case "1027": ### snTrapFapQueueErrors
        
            ##########
            # $1 = snAgentBrdIndex
            # $2 = snAgGblTrapMessage
            ##########
            
            $snAgentBrdIndex = $1
            $snAgGblTrapMessage = $2
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-ROUTER-NOTIFICATION-MIB-snTrapFapQueueErrors"

            @AlertGroup = "LP FAP Queue Status"
            @AlertKey = "snAgentBrdEntry." + $snAgentBrdIndex
            if(!match($snAgGblTrapMessage, ""))
{
	@Summary = $snAgGblTrapMessage 
}
else
{
	@Summary = "LP FAP (Fabric Adapter Processor) has Queue Errors"
}
            
            @Summary = @Summary + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snAgentBrdIndex, $snAgGblTrapMessage)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snAgentBrdIndex", $snAgentBrdIndex, "snAgGblTrapMessage", $snAgGblTrapMessage)
         
        case "1028": ### snTrapFapLinkErrors
        
            ##########
            # $1 = snAgentBrdIndex
            # $2 = snAgGblTrapMessage
            ##########
            
            $snAgentBrdIndex = $1
            $snAgGblTrapMessage = $2
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-ROUTER-NOTIFICATION-MIB-snTrapFapLinkErrors"

            @AlertGroup = "LP FAP Link Status"
            @AlertKey = "snAgentBrdEntry." + $snAgentBrdIndex
            if(!match($snAgGblTrapMessage, ""))
{
	@Summary = $snAgGblTrapMessage 
}
else
{
	@Summary = "LP FAP (Fabric Adapter Processor) has Link Errors"
}
            
            @Summary = @Summary + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snAgentBrdIndex, $snAgGblTrapMessage)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snAgentBrdIndex", $snAgentBrdIndex, "snAgGblTrapMessage", $snAgGblTrapMessage)
         
        case "1029": ### snTrapPktPathErrors
        
            ##########
            # $1 = snAgentBrdIndex
            # $2 = snAgGblTrapMessage
            ##########
            
            $snAgentBrdIndex = $1
            $snAgGblTrapMessage = $2
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-ROUTER-NOTIFICATION-MIB-snTrapPktPathErrors"

            @AlertGroup = "LP PPCR Path Status"
            @AlertKey = "snAgentBrdEntry." + $snAgentBrdIndex
            if(!match($snAgGblTrapMessage, ""))
{
	@Summary = $snAgGblTrapMessage 
}
else
{
	@Summary = "LP PPCR (Packet Processor) has Path Errors"
}
            
            @Summary = @Summary + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snAgentBrdIndex, $snAgGblTrapMessage)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snAgentBrdIndex", $snAgentBrdIndex, "snAgGblTrapMessage", $snAgGblTrapMessage)
         
        case "1030": ### snTrapFapQdpReinit
        
            ##########
            # $1 = snAgentBrdIndex
            # $2 = snAgGblTrapMessage
            ##########
            
            $snAgentBrdIndex = $1
            $snAgGblTrapMessage = $2
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-ROUTER-NOTIFICATION-MIB-snTrapFapQdpReinit"

            @AlertGroup = "LP FAP QDP Initialize Status"
            @AlertKey = "snAgentBrdEntry." + $snAgentBrdIndex
            if(!match($snAgGblTrapMessage, ""))
{
	@Summary = $snAgGblTrapMessage 
}
else
{
	@Summary = "LP PPCR (Packet Processor) Queue Controller Reinitializes"
}
            
            @Summary = @Summary + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snAgentBrdIndex, $snAgGblTrapMessage)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snAgentBrdIndex", $snAgentBrdIndex, "snAgGblTrapMessage", $snAgGblTrapMessage)
         
        case "1031": ### snTrapFapReinit
        
            ##########
            # $1 = snAgentBrdIndex
            # $2 = snAgGblTrapMessage
            ##########
            
            $snAgentBrdIndex = $1
            $snAgGblTrapMessage = $2
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-ROUTER-NOTIFICATION-MIB-snTrapFapReinit"

            @AlertGroup = "LP FAP Initialize Status"
            @AlertKey = "snAgentBrdEntry." + $snAgentBrdIndex
            if(!match($snAgGblTrapMessage, ""))
{
	@Summary = $snAgGblTrapMessage 
}
else
{
	@Summary = "LP PPCR (Packet Processor) Reinitializes"
}
            
            @Summary = @Summary + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snAgentBrdIndex, $snAgGblTrapMessage)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snAgentBrdIndex", $snAgentBrdIndex, "snAgGblTrapMessage", $snAgGblTrapMessage)
         
        case "1040": ### snTrapSnmLinkErrors
        
            ##########
            # $1 = snAgGblTrapMessage
            ##########
            
            $snAgGblTrapMessage = $1
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-ROUTER-NOTIFICATION-MIB-snTrapSnmLinkErrors"

            @AlertGroup = "LP SNM Link Status"
            if(!match($snAgGblTrapMessage, ""))
{
	@Summary = $snAgGblTrapMessage 
}
else
{
	@Summary = "SNM (Switch Fabric Module) has Link Errors"
}
            
            @Summary = @Summary
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snAgGblTrapMessage)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snAgGblTrapMessage", $snAgGblTrapMessage)
         
        case "1041": ### snTrapSnmFifoErrors
        
            ##########
            # $1 = snAgGblTrapMessage
            ##########
            
            $snAgGblTrapMessage = $1
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-ROUTER-NOTIFICATION-MIB-snTrapSnmFifoErrors"

            @AlertGroup = "LP SFM Link Status"
            if(!match($snAgGblTrapMessage, ""))
{
	@Summary = $snAgGblTrapMessage 
}
else
{
	@Summary = "LP SNM has FIFO Errors"
}
            
            @Summary = @Summary
            
            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snAgGblTrapMessage)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snAgGblTrapMessage", $snAgGblTrapMessage)
                 
        case "1204": ### snTrapSTPRootGuardViolation
        
            ##########
            # $1 = snAgGblTrapMessage
            ##########
            
            $snAgGblTrapMessage = $1
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-ROUTER-NOTIFICATION-MIB-snTrapSTPRootGuardViolation"

            @AlertGroup = "STP Root Guard Status"
            if(!match($snAgGblTrapMessage, ""))
{
	@Summary = $snAgGblTrapMessage 
}
else
{
	@Summary = "STP Root Guard Violation occurs on a Port"
}
            
            @Summary = @Summary
            
            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snAgGblTrapMessage)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snAgGblTrapMessage", $snAgGblTrapMessage)
         
        case "1205": ### snTrapRSTPRootGuardViolation
        
            ##########
            # $1 = snAgGblTrapMessage
            ##########
            
            $snAgGblTrapMessage = $1
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-ROUTER-NOTIFICATION-MIB-snTrapRSTPRootGuardViolation"

            @AlertGroup = "RSTP Root Guard Status"
            if(!match($snAgGblTrapMessage, ""))
{
	@Summary = $snAgGblTrapMessage 
}
else
{
	@Summary = "RSTP Root Guard Violation occurs on a Port"
}
            
            @Summary = @Summary
            
            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snAgGblTrapMessage)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snAgGblTrapMessage", $snAgGblTrapMessage)
         
        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, foundry-FOUNDRY-SN-ROUTER-NOTIFICATION-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, foundry-FOUNDRY-SN-ROUTER-NOTIFICATION-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/foundry/foundry-FOUNDRY-SN-ROUTER-NOTIFICATION-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/foundry/foundry-FOUNDRY-SN-ROUTER-NOTIFICATION-MIB.user.include.snmptrap.rules"


##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... foundry-FOUNDRY-SN-ROUTER-NOTIFICATION-MIB.include.snmptrap.rules >>>>>")


