###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  FOUNDRY-SN-TRAP-MIB
#
###############################################################################

# Commented out line below - main entry point is foundry-FOUNDRY-SN-TRAP-MIB--FOUNDRY-MPLS-MIB.include.snmptrap.rules
#case ".1.3.6.1.4.1.1991": ### - Notifications from FOUNDRY-SN-TRAP-MIB (Not specificed)

log(DEBUG, "<<<<< Entering... foundry-FOUNDRY-SN-TRAP-MIB.include.snmptrap.rules >>>>>")

    @Agent = "foundry-FOUNDRY-SN-TRAP-MIB"
    @Class = "40660"
    
    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
     
        case "1": ### snTrapChasPwrSupply
        
            ##########
            # $1 = snChasPwrSupplyStatus
            ##########
            
            $snChasPwrSupplyStatus = $1
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-TRAP-MIB-snTrapChasPwrSupply"

            @AlertGroup = "Power Supply Status"
            @Summary = "Power Supply Fails, Error Status: " + $snChasPwrSupplyStatus  
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snChasPwrSupplyStatus)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snChasPwrSupplyStatus", $snChasPwrSupplyStatus)
         
        case "2": ### snTrapLockedAddressViolation
        
            ##########
            # $1 = snSwViolatorPortNumber
            # $2 = snSwViolatorMacAddress
            ##########
            
            $snSwViolatorPortNumber = $1
            $snSwViolatorMacAddress = $2
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-TRAP-MIB-snTrapLockedAddressViolation"

            @AlertGroup = "MAC Addresses Status"
            @Summary = "Lock Address Violation on Port " + $snSwViolatorPortNumber + " with MAC Address " + $snSwViolatorMacAddress  
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snSwViolatorPortNumber, $snSwViolatorMacAddress)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snSwViolatorPortNumber", $snSwViolatorPortNumber, "snSwViolatorMacAddress", $snSwViolatorMacAddress)
         
        case "500": ### snTrapSynCookieAttackThreshReached
        
            ##########
            # $1 = snAgGblTrapMessage
            ##########
            
            $snAgGblTrapMessage = $1
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-TRAP-MIB-snTrapSynCookieAttackThreshReached"

            @AlertGroup = "SYN-COOKIE Attack Threshold Status"
            if(!match($snAgGblTrapMessage, ""))
{
	@Summary = $snAgGblTrapMessage 
}
else
{
	@Summary = "Syn Cookie Attack Rate Exceeds the Configured Threshold Value"
}
                        
            @Summary = @Summary
            
            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0   

            update(@Summary)
                       
            @Identifier = @Node + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snAgGblTrapMessage)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snAgGblTrapMessage", $snAgGblTrapMessage)
         
        case "501": ### snTrapL4SymBecomeStandby
        
            ##########
            # $1 = snAgGblTrapMessage
            ##########
            
            $snAgGblTrapMessage = $1
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-TRAP-MIB-snTrapL4SymBecomeStandby"

            @AlertGroup = "L4-Sym Status"
            if(!match($snAgGblTrapMessage, ""))
{
	@Summary = $snAgGblTrapMessage 
}
else
{
	@Summary = "Sym VIP Changed State from Active to Standby"
}
                
            @Summary = @Summary
            
            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800   

            update(@Summary)
            
            @Identifier = @Node + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snAgGblTrapMessage)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snAgGblTrapMessage", $snAgGblTrapMessage)
         
        case "502": ### snTrapL4SymBecomeActive
        
            ##########
            # $1 = snAgGblTrapMessage
            ##########
            
            $snAgGblTrapMessage = $1
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-TRAP-MIB-snTrapL4SymBecomeActive"

            @AlertGroup = "L4-Sym Status"
            if(!match($snAgGblTrapMessage, ""))
{
	@Summary = $snAgGblTrapMessage 
}
else
{
	@Summary = "Sym VIP Changed State from Standby to Active"
}
                        
            @Summary = @Summary
            
            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800     

            update(@Summary)
            
            @Identifier = @Node + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snAgGblTrapMessage)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snAgGblTrapMessage", $snAgGblTrapMessage)
         
        case "503": ### snTrapL4UdpTRLConnectRateExceedThreshold
        
            ##########
            # $1 = snAgGblTrapMessage
            ##########
            
            $snAgGblTrapMessage = $1
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-TRAP-MIB-snTrapL4UdpTRLConnectRateExceedThreshold"

            @AlertGroup = "L4 UDP TRL Connect Rate Threshold Status"
            if(!match($snAgGblTrapMessage, ""))
{
	@Summary = $snAgGblTrapMessage 
}
else
{
	@Summary = "UDP Transaction Rate Limiting Connection Rate Exceeds Threshold"
}
                       
            @Summary = @Summary
            
            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0      

            update(@Summary)
            
            @Identifier = @Node + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snAgGblTrapMessage)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snAgGblTrapMessage", $snAgGblTrapMessage)
         
        case "504": ### snTrapL4UdpTRLConnectRateExceedMax
        
            ##########
            # $1 = snAgGblTrapMessage
            ##########
            
            $snAgGblTrapMessage = $1
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-TRAP-MIB-snTrapL4UdpTRLConnectRateExceedMax"

            @AlertGroup = "L4 UDP TRL Connect Rate Threshold Status"
            if(!match($snAgGblTrapMessage, ""))
{
	@Summary = $snAgGblTrapMessage 
}
else
{
	@Summary = "UDP Transaction Rate Limiting Connection Rate Exceeds Configured Maximum"
}
                     
            @Summary = @Summary
            
            $DEFAULT_Severity = 5
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0   

            update(@Summary)
            
            @Identifier = @Node + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snAgGblTrapMessage)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snAgGblTrapMessage", $snAgGblTrapMessage)
         
        case "509": ### snTrapUtilizationExceeded
        
            ##########
            # $1 = snAgGblTrapMessage
            ##########
            
            $snAgGblTrapMessage = $1
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-TRAP-MIB-snTrapUtilizationExceeded"

            @AlertGroup = "CPU Utilization Threshold Status"
            if(!match($snAgGblTrapMessage, ""))
{
	@Summary = $snAgGblTrapMessage 
}
else
{
	@Summary = "CPU Utilization Exceeds the Configured Threshold"
}
            @Summary = @Summary
            
            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800   

            update(@Summary)
                      
            @Identifier = @Node + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snAgGblTrapMessage)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snAgGblTrapMessage", $snAgGblTrapMessage)
                 
        case "1005": ### snTrapL4VirtualServerPortUp
        
            ##########
            # $1 = snL4TrapVirtualServerIP
            # $2 = snL4TrapVirtualServerName
            # $3 = snL4TrapVirtualServerPort
            # $4 = snL4TrapVirtualServerMessage
            ##########
            
            $snL4TrapVirtualServerIP = $1
            $snL4TrapVirtualServerName = $2
            $snL4TrapVirtualServerPort = $3
            $snL4TrapVirtualServerMessage = $4
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-TRAP-MIB-snTrapL4VirtualServerPortUp"

            @AlertGroup = "L4 Virtual Server Port Status"
            @AlertKey = "Server: " + $snL4TrapVirtualServerName + ", IP: " + $snL4TrapVirtualServerIP + ", Port: " +$snL4TrapVirtualServerPort
            @Summary = "SLB Virtual Server Port: " + $snL4TrapVirtualServerIP + " " + $snL4TrapVirtualServerName + " " + $snL4TrapVirtualServerPort + " is Up" + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snL4TrapVirtualServerIP, $snL4TrapVirtualServerName, $snL4TrapVirtualServerPort, $snL4TrapVirtualServerMessage)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snL4TrapVirtualServerIP", $snL4TrapVirtualServerIP, "snL4TrapVirtualServerName", $snL4TrapVirtualServerName, "snL4TrapVirtualServerPort", $snL4TrapVirtualServerPort,
                 "snL4TrapVirtualServerMessage", $snL4TrapVirtualServerMessage)
         
        case "1006": ### snTrapL4VirtualServerPortDown
        
            ##########
            # $1 = snL4TrapVirtualServerIP
            # $2 = snL4TrapVirtualServerName
            # $3 = snL4TrapVirtualServerPort
            # $4 = snL4TrapVirtualServerMessage
            ##########
            
            $snL4TrapVirtualServerIP = $1
            $snL4TrapVirtualServerName = $2
            $snL4TrapVirtualServerPort = $3
            $snL4TrapVirtualServerMessage = $4
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-TRAP-MIB-snTrapL4VirtualServerPortDown"

            @AlertGroup = "L4 Virtual Server Port Status"
            @AlertKey = "Server: " + $snL4TrapVirtualServerName + ", IP: " + $snL4TrapVirtualServerIP + ", Port: " +$snL4TrapVirtualServerPort
            @Summary = "SLB Virtual Server Port: " + $snL4TrapVirtualServerIP + " " + $snL4TrapVirtualServerName + " " + $snL4TrapVirtualServerPort + " is Down" + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snL4TrapVirtualServerIP, $snL4TrapVirtualServerName, $snL4TrapVirtualServerPort, $snL4TrapVirtualServerMessage)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snL4TrapVirtualServerIP", $snL4TrapVirtualServerIP, "snL4TrapVirtualServerName", $snL4TrapVirtualServerName, "snL4TrapVirtualServerPort", $snL4TrapVirtualServerPort,
                 "snL4TrapVirtualServerMessage", $snL4TrapVirtualServerMessage)
         
        case "1008": ### snTrapifLinkDownRemoteFault
        
            ##########
            # $1 = snAgGblTrapMessage
            ##########
            
            $snAgGblTrapMessage = $1
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-TRAP-MIB-snTrapifLinkDownRemoteFault"

            @AlertGroup = "Physical Interface Link Status"
            if(!match($snAgGblTrapMessage, ""))
{
	@Summary = $snAgGblTrapMessage 
}
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snAgGblTrapMessage)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snAgGblTrapMessage", $snAgGblTrapMessage)
         
        case "1021": ### snTrapL4RealServerPortConnectionRateExceedMax
        
            ##########
            # $1 = snAgGblTrapMessage
            ##########
            
            $snAgGblTrapMessage = $1
            
            $OS_EventId = "SNMPTRAP-foundry-FOUNDRY-SN-TRAP-MIB-snTrapL4RealServerPortConnectionRateExceedMax"

            @AlertGroup = "L4 Port Connection Rate"
            if(!match($snAgGblTrapMessage, ""))
{
	@Summary = $snAgGblTrapMessage 
}
else
{
	@Summary = "L4 Real Server Port Connection Rate Exceeds Configured Maximum"
}
            @Summary = @Summary
            
            $DEFAULT_Severity = 5
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0  

            update(@Summary)
                    
            @Identifier = @Node + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($snAgGblTrapMessage)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "snAgGblTrapMessage", $snAgGblTrapMessage)
         
        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_foundry, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, foundry-FOUNDRY-SN-TRAP-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, foundry-FOUNDRY-SN-TRAP-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/foundry/foundry-FOUNDRY-SN-TRAP-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/foundry/foundry-FOUNDRY-SN-TRAP-MIB.user.include.snmptrap.rules"


##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... foundry-FOUNDRY-SN-TRAP-MIB.include.snmptrap.rules >>>>>")


