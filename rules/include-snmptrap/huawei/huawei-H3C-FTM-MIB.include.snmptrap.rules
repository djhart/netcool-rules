###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 3.0 - Updated release for NIM-03
#
#          -  Supports "Advanced" and "User" include files
#          -  Supports "Severity" lookup tables
#
###############################################################################
#
# 2.0 - Updated release
#
#          -  Repackage for NIM-02
#          -  H3C-FTM-MIB release V2.3
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#          -  H3C-FTM-MIB release V2.2
#
###############################################################################

case ".1.3.6.1.4.1.2011.10.2.1.1.3": ### Huawei - Notifications from H3C-FTM-MIB

    log(DEBUG, "<<<<< Entering... huawei-H3C-FTM-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Huawei AR- and S-Series"
    @Class = "40579"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### h3cFtmUnitIDChange

            ##########
            # When unit id changes in fabric, this trap is sent
            # with unit index and its new id .
            #
            # $1 = h3cFtmIndex - The unique index of a unit.
            # $2 = h3cFtmUnitID - Identifer of unit in fabric.
            #        Its value is between 1 and the maximum which
            #        defines in product specification.
            ##########

            $h3cFtmIndex = $1
            $h3cFtmUnitID = $2
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_huawei, "1")) {
                details($h3cFtmIndex,$h3cFtmUnitID)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "h3cFtmIndex", $h3cFtmIndex, "h3cFtmUnitID", $h3cFtmUnitID)

            $OS_EventId = "SNMPTRAP-huawei-H3C-FTM-MIB-h3cFtmUnitIDChange"

            @AlertGroup = "Unit ID Chg"
            @AlertKey = "h3cFtmUnitEntry." + $1
            @Summary = "FTM Unit ID Changed to " + $2 + "  ( " + @AlertKey + " )"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        case "2": ### h3cFtmUnitNameChange

            ##########
            # When unit name changes in fabric, this trap is sent
            # with unit index and its new name.
            #
            # $1 = h3cFtmIndex - The unique index of a unit.
            # $2 = h3cFtmUnitName - Name of unit in XRN-Fabric.
            #        It consists of no more than 64 characters.
            ##########

            $h3cFtmIndex = $1
            $h3cFtmUnitName = $2
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_huawei, "1")) {
                details($h3cFtmIndex,$h3cFtmUnitName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "h3cFtmIndex", $h3cFtmIndex, "h3cFtmUnitName", $h3cFtmUnitName)

            $OS_EventId = "SNMPTRAP-huawei-H3C-FTM-MIB-h3cFtmUnitNameChange"

            @AlertGroup = "Unit Name Chg"
            @AlertKey = "h3cFtmUnitEntry." + $1
            @Summary = "FTM Unit Name Changed to " + $2 + "  ( " + @AlertKey + " )"
            update(@Summary)

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_huawei, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

#########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, huawei-H3C-FTM-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, huawei-H3C-FTM-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/huawei/huawei-H3C-FTM-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/huawei/huawei-H3C-FTM-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

log(DEBUG, "<<<<< Leaving... huawei-H3C-FTM-MIB.include.snmptrap.rules >>>>>")
