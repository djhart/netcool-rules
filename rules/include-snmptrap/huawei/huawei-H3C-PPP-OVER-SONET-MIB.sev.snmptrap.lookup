###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  H3C-PPP-OVER-SONET-MIB
#
# 1.1 - Changed the format
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table huawei-H3C-PPP-OVER-SONET-MIB_sev =
{
	    {"SNMPTRAP-huawei-H3C-PPP-OVER-SONET-MIB-h3cPosLOSAlarm","3","1","0"},
	    {"SNMPTRAP-huawei-H3C-PPP-OVER-SONET-MIB-h3cPosLOFAlarm","3","1","0"},
	    {"SNMPTRAP-huawei-H3C-PPP-OVER-SONET-MIB-h3cPosOOFAlarm","3","1","0"},
	    {"SNMPTRAP-huawei-H3C-PPP-OVER-SONET-MIB-h3cPosLAISAlarm","3","1","0"},
	    {"SNMPTRAP-huawei-H3C-PPP-OVER-SONET-MIB-h3cPosLRDIAlarm","3","1","0"},
	    {"SNMPTRAP-huawei-H3C-PPP-OVER-SONET-MIB-h3cPosPRDIAlarm","3","1","0"},
	    {"SNMPTRAP-huawei-H3C-PPP-OVER-SONET-MIB-h3cPosPAISAlarm","3","1","0"},
	    {"SNMPTRAP-huawei-H3C-PPP-OVER-SONET-MIB-h3cPosLOPAlarm","3","1","0"},
	    {"SNMPTRAP-huawei-H3C-PPP-OVER-SONET-MIB-h3cPosSTIMAlarm","3","1","0"},
	    {"SNMPTRAP-huawei-H3C-PPP-OVER-SONET-MIB-h3cPosPTIMAlarm","3","1","0"},
	    {"SNMPTRAP-huawei-H3C-PPP-OVER-SONET-MIB-h3cPosPSLMAlarm","3","1","0"},
	    {"SNMPTRAP-huawei-H3C-PPP-OVER-SONET-MIB-h3cPosLSDAlarm","3","1","0"},
	    {"SNMPTRAP-huawei-H3C-PPP-OVER-SONET-MIB-h3cPosLSFAlarm","3","1","0"}
}
default = {"Unknown","Unknown","Unknown"}
