###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 3.0 - Updated release for NIM-03
#
#          -  Supports "Advanced" and "User" include files
#          -  Supports "Severity" lookup tables
#
###############################################################################
#
# 2.0 - Updated release
#
#          -  Repackage for NIM-02
#          -  HUAWEI-LswMix-MIB release V1.2
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#          -  HUAWEI-LswMix-MIB release V1.1
#
###############################################################################

case ".1.3.6.1.4.1.2011.2.23.1.17.10": ### Huawei - Notifications from HUAWEI-LswMix-MIB

    log(DEBUG, "<<<<< Entering... huawei-HUAWEI-LswMix-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Huawei AR- and S-Series"
    @Class = "40579"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### hwSlaveSwitchOver

            ##########
            # Back board mode set OK
            #
            ##########

            $OS_EventId = "SNMPTRAP-huawei-HUAWEI-LswMix-MIB-hwSlaveSwitchOver"

            @AlertGroup = "Slave Switchover"
            @AlertKey = ""
            @Summary = "Slave Switchover, Back Board Mode Set OK"

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_huawei, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

#########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, huawei-HUAWEI-LswMix-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, huawei-HUAWEI-LswMix-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/huawei/huawei-HUAWEI-LswMix-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/huawei/huawei-HUAWEI-LswMix-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

log(DEBUG, "<<<<< Leaving... huawei-HUAWEI-LswMix-MIB.include.snmptrap.rules >>>>>")
