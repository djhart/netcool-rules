###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 3.0 - Updated release for NIM-03
#
#          -  Supports "Advanced" and "User" include files
#          -  Supports "Severity" lookup tables
#
###############################################################################
#
# 2.0 - Updated release
#
#          -  Repackage for NIM-02
#          -  HUAWEI-ENTITY-EXTENT-MIB release V1.0
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#          -  HUAWEI-ENTITY-EXTENT-MIB release V1.0
#
###############################################################################

case ".1.3.6.1.4.1.2011.5.25.31.2": ### Huawei - Traps from HUAWEI-ENTITY-EXTENT-MIB

    log(DEBUG, "<<<<< Entering... huawei-HUAWEI-ENTITY-EXTENT-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Huawei NE-Series"
    @Class = "40579"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### hwEntityExtTemperatureThresholdNotification

            ##########
            # $1 = hwEntityTemperature
            # $2 = hwEntityTemperatureThreshold
            # $3 = hwEntityAdminStatus
            # $4 = hwEntityAlarmLight
            ##########

            $hwEntityTemperature = $1
            $hwEntityTemperatureThreshold = $2
            $hwEntityAdminStatus = lookup($3,HwAdminState)
            $hwEntityAlarmLight = lookup($4,HwAlarmStatus)
	    $entPhysicalIndex = extract($OID3, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-huawei-HUAWEI-ENTITY-EXTENT-MIB-hwEntityExtTemperatureThresholdNotification"

            @AlertGroup = "Temperature Threshold"
            @AlertKey = "hwEntityStateEntry." + $entPhysicalIndex
            @Summary = "Temperature Threshold Exceeded, Actual: " + $1 + ", Threshold: " + $2 + "  ( " + @AlertKey + " )"
            update(@Summary)

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            $hwEntityAdminStatus = $hwEntityAdminStatus + " ( " + $3 + " )"
            $hwEntityAlarmLight = $hwEntityAlarmLight + " ( " + $4 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_huawei, "1")) {
                details($hwEntityTemperature,$hwEntityTemperatureThreshold,
                        $hwEntityAdminStatus,$hwEntityAlarmLight,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hwEntityTemperature", $hwEntityTemperature, "hwEntityTemperatureThreshold", $hwEntityTemperatureThreshold, "hwEntityAdminStatus", $hwEntityAdminStatus,
                 "hwEntityAlarmLight", $hwEntityAlarmLight, "entPhysicalIndex", $entPhysicalIndex)

        case "2": ### hwEntityExtVoltageLowThresholdNotification

            ##########
            # $1 = hwEntityVoltage
            # $2 = hwEntityVoltageLowThreshold 
            # $3 = hwEntityAdminStatus 
            # $4 = hwEntityAlarmLight 
            ##########

            $hwEntityVoltage = $1
            $hwEntityVoltageLowThreshold = $2
            $hwEntityAdminStatus = lookup($3,HwAdminState) 
            $hwEntityAlarmLight = lookup($4,HwAlarmStatus) 
	    $entPhysicalIndex = extract($OID3, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-huawei-HUAWEI-ENTITY-EXTENT-MIB-hwEntityExtVoltageLowThresholdNotification"

            @AlertGroup = "Voltage Low Threshold"
            @AlertKey = "hwEntityStateEntry." + $entPhysicalIndex
            @Summary = "Voltage Low Threshold Exceeded, Actual: " + $1 + ", Threshold: " + $2 + "  ( " + @AlertKey + " )"
            update(@Summary)

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            $hwEntityAdminStatus = $hwEntityAdminStatus + " ( " + $3 + " )"
            $hwEntityAlarmLight = $hwEntityAlarmLight + " ( " + $4 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_huawei, "1")) {
                details($hwEntityVoltage,$hwEntityVoltageLowThreshold,
                        $hwEntityAdminStatus,$hwEntityAlarmLight,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hwEntityVoltage", $hwEntityVoltage, "hwEntityVoltageLowThreshold", $hwEntityVoltageLowThreshold, "hwEntityAdminStatus", $hwEntityAdminStatus,
                 "hwEntityAlarmLight", $hwEntityAlarmLight, "entPhysicalIndex", $entPhysicalIndex)

        case "3": ### hwEntityExtVoltageHighThresholdNotification

            ##########
            # $1 = hwEntityVoltage 
            # $2 = hwEntityVoltageHighThreshold 
            # $3 = hwEntityAdminStatus
            # $4 = hwEntityAlarmLight
            ##########

            $hwEntityVoltage = $1
            $hwEntityVoltageHighThreshold = $2
            $hwEntityAdminStatus = lookup($3,HwAdminState) 
            $hwEntityAlarmLight = lookup($4,HwAlarmStatus) 
	    $entPhysicalIndex = extract($OID3, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-huawei-HUAWEI-ENTITY-EXTENT-MIB-hwEntityExtVoltageHighThresholdNotification"

            @AlertGroup = "Voltage High Threshold"
            @AlertKey = "hwEntityStateEntry." + $entPhysicalIndex
            @Summary = "Voltage High Threshold Exceeded, Actual: " + $1 + ", Threshold: " + $2 + "  ( " + @AlertKey + " )"
            update(@Summary)

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            $hwEntityAdminStatus = $hwEntityAdminStatus + " ( " + $3 + " )"
            $hwEntityAlarmLight = $hwEntityAlarmLight + " ( " + $4 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_huawei, "1")) {
                details($hwEntityVoltage,$hwEntityVoltageHighThreshold,
                        $hwEntityAdminStatus,$hwEntityAlarmLight,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hwEntityVoltage", $hwEntityVoltage, "hwEntityVoltageHighThreshold", $hwEntityVoltageHighThreshold, "hwEntityAdminStatus", $hwEntityAdminStatus,
                 "hwEntityAlarmLight", $hwEntityAlarmLight, "entPhysicalIndex", $entPhysicalIndex)

        case "4": ### hwEntityExtCpuUsageThresholdNotfication

            ##########
            # $1 = hwEntityCpuUsage
            # $2 = hwEntityCpuUsageThreshold
            # $3 = hwEntityTemperature
            # $4 = hwEntityTemperatureThreshold
            # $5 = hwEntityAdminStatus 
            # $6 = hwEntityAlarmLight
            ##########

            $hwEntityCpuUsage = $1
            $hwEntityCpuUsageThreshold = $2
            $hwEntityTemperature = $3
            $hwEntityTemperatureThreshold = $4
            $hwEntityAdminStatus = lookup($5,HwAdminState)
            $hwEntityAlarmLight = lookup($6,HwAlarmStatus) 
	    $entPhysicalIndex = extract($OID5, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-huawei-HUAWEI-ENTITY-EXTENT-MIB-hwEntityExtCpuUsageThresholdNotfication"

            @AlertGroup = "CPU Usage Threshold"
            @AlertKey = "hwEntityStateEntry." + $entPhysicalIndex
            @Summary = "CPU Usage Threshold Exceeded, Actual: " + $1 + ", Threshold: " + $2 + "  ( " + @AlertKey + " )"
            update(@Summary)

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            $hwEntityAdminStatus = $hwEntityAdminStatus + " ( " + $5 + " )"
            $hwEntityAlarmLight = $hwEntityAlarmLight + " ( " + $6 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_huawei, "1")) {
                details($hwEntityCpuUsage,$hwEntityCpuUsageThreshold,
                        $hwEntityTemperature,$hwEntityTemperatureThreshold,
                        $hwEntityAdminStatus,$hwEntityAlarmLight,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hwEntityCpuUsage", $hwEntityCpuUsage, "hwEntityCpuUsageThreshold", $hwEntityCpuUsageThreshold, "hwEntityTemperature", $hwEntityTemperature,
                 "hwEntityTemperatureThreshold", $hwEntityTemperatureThreshold, "hwEntityAdminStatus", $hwEntityAdminStatus, "hwEntityAlarmLight", $hwEntityAlarmLight,
                 "entPhysicalIndex", $entPhysicalIndex)

        case "5": ### hwEntityExtMemUsageThresholdNotification

            ##########
            # $1 = hwEntityMemUsage
            # $2 = hwEntityMemUsageThreshold 
            # $3 = hwEntityMemSize
            # $4 = hwEntityAdminStatus
            # $5 = hwEntityAlarmLight
            ##########

            $hwEntityMemUsage = $1
            $hwEntityMemUsageThreshold = $2
            $hwEntityMemSize = $3
            $hwEntityAdminStatus = lookup($4,HwAdminState) 
            $hwEntityAlarmLight = lookup($5,HwAlarmStatus) 
	    $entPhysicalIndex = extract($OID4, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-huawei-HUAWEI-ENTITY-EXTENT-MIB-hwEntityExtMemUsageThresholdNotification"

            @AlertGroup = "Memory Usage Threshold"
            @AlertKey = "hwEntityStateEntry." + $entPhysicalIndex
            @Summary = "Memory Usage Threshold Exceeded, Actual: " + $1 + ", Threshold: " + $2 + "  ( " + @AlertKey + " )" 
            update(@Summary)

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            $hwEntityAdminStatus = $hwEntityAdminStatus + " ( " + $4 + " )"
            $hwEntityAlarmLight = $hwEntityAlarmLight + " ( " + $5 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_huawei, "1")) {
                details($hwEntityMemUsage,$hwEntityMemUsageThreshold,
                        $hwEntityMemSize,$hwEntityAdminStatus,$hwEntityAlarmLight,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hwEntityMemUsage", $hwEntityMemUsage, "hwEntityMemUsageThreshold", $hwEntityMemUsageThreshold, "hwEntityMemSize", $hwEntityMemSize,
                 "hwEntityAdminStatus", $hwEntityAdminStatus, "hwEntityAlarmLight", $hwEntityAlarmLight, "entPhysicalIndex", $entPhysicalIndex)

        case "6": ### hwEntityExtOperEnabled

            ##########
            # $1 = hwEntityAdminStatus 
            # $2 = hwEntityAlarmLight
            ##########

            $hwEntityAdminStatus = lookup($1,HwAdminState)
            $hwEntityAlarmLight = lookup($2,HwAlarmStatus)
	    $entPhysicalIndex = extract($OID1, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-huawei-HUAWEI-ENTITY-EXTENT-MIB-hwEntityExtOperEnabled"

            @AlertGroup = "Entity Oper Status"
            @AlertKey = "hwEntityStateEntry." + $entPhysicalIndex
            @Summary = "Entity is Operational Enabled  ( Admin Status: " + $hwEntityAdminStatus + ", Alarm Status: " + $hwEntityAlarmLight + " )"
            update(@Summary)

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            $hwEntityAdminStatus = $hwEntityAdminStatus + " ( " + $1 + " )"
            $hwEntityAlarmLight = $hwEntityAlarmLight + " ( " + $2 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_huawei, "1")) {
                details($hwEntityAdminStatus,$hwEntityAlarmLight,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hwEntityAdminStatus", $hwEntityAdminStatus, "hwEntityAlarmLight", $hwEntityAlarmLight, "entPhysicalIndex", $entPhysicalIndex)

        case "7": ### hwEntityExtOperDisabled

            ##########
            # $1 = hwEntityAdminStatus 
            # $2 = hwEntityAlarmLight
            ##########

            $hwEntityAdminStatus = lookup($1,HwAdminState)
            $hwEntityAlarmLight = lookup($2,HwAlarmStatus)
	    $entPhysicalIndex = extract($OID1, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-huawei-HUAWEI-ENTITY-EXTENT-MIB-hwEntityExtOperDisabled"

            @AlertGroup = "Entity Oper Status"
            @AlertKey = "hwEntityStateEntry." + $entPhysicalIndex
            @Summary = "Entity is Operational Disabled  ( Admin Status: " + $hwEntityAdminStatus + ", Alarm Status: " + $hwEntityAlarmLight + " )"
            update(@Summary)

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            $hwEntityAdminStatus = $hwEntityAdminStatus + " ( " + $1 + " )"
            $hwEntityAlarmLight = $hwEntityAlarmLight + " ( " + $2 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_huawei, "1")) {
                details($hwEntityAdminStatus,$hwEntityAlarmLight,$entPhysicalIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hwEntityAdminStatus", $hwEntityAdminStatus, "hwEntityAlarmLight", $hwEntityAlarmLight, "entPhysicalIndex", $entPhysicalIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_huawei, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, huawei-HUAWEI-ENTITY-EXTENT-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, huawei-HUAWEI-ENTITY-EXTENT-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/huawei/huawei-HUAWEI-ENTITY-EXTENT-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/huawei/huawei-HUAWEI-ENTITY-EXTENT-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... huawei-HUAWEI-ENTITY-EXTENT-MIB.include.snmptrap.rules >>>>>")
