###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.1- Updated Release.
#
#        Added dot1agCfmFaultAlarm trap from IEEE8021-CFM-MIB (200810150000Z)
#
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  IEEE8021-CFM-MIB
#
###############################################################################

case ".1.0.8802.1.1.3.1.6": ### Connectivity Fault Management Module - from IEEE8021-CFM-MIB 

    log(DEBUG, "<<<<< Entering... IEEE-IEEE8021-CFM-MIB.include.snmptrap.rules >>>>>")

    @Agent = "IEEE-CFM Module"
    @Class = "40090"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### dot1agCfmCCheckLossEvent

            ##########
            # $1 = dot1agCfmMaintenanceDomainName 
            # $2 = dot1agCfmMaName 
            # $3 = dot1agCfmMepIdentifier 
            ##########

            $dot1agCfmMaintenanceDomainName = $1
            $dot1agCfmMaName = $2
            $dot1agCfmMepIdentifier = $3
            $dot1agCfmMaintenanceDomainIndex = extract($OID3, "\.([0-9]+)\.[0-9]+\.[0-9]+$")
            $dot1agCfmMaIndex = extract($OID3, "\.([0-9]+)\.[0-9]+$") 
            $dot1agCfmMepIndex = extract($OID3, "\.([0-9]+)$")  

            $OS_EventId = "SNMPTRAP-IEEE-IEEE8021-CFM-MIB-dot1agCfmCCheckLossEvent"

            @AlertGroup = "Maintenance Association End Point(MEP) Status"
            @AlertKey = "dot1agCfmMepEntry." + $dot1agCfmMaintenanceDomainIndex + "." + $dot1agCfmMaIndex + "." + $dot1agCfmMepIndex
            @Summary = "MEP Lost Contact With Other MEps"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ieee, "1")) {
                details($dot1agCfmMaintenanceDomainName,$dot1agCfmMaName,$dot1agCfmMepIdentifier,$dot1agCfmMaintenanceDomainIndex,$dot1agCfmMaIndex,$dot1agCfmMepIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "dot1agCfmMaintenanceDomainName", $dot1agCfmMaintenanceDomainName, "dot1agCfmMaName", $dot1agCfmMaName, "dot1agCfmMepIdentifier", $dot1agCfmMepIdentifier,
                 "dot1agCfmMaintenanceDomainIndex", $dot1agCfmMaintenanceDomainIndex, "dot1agCfmMaIndex", $dot1agCfmMaIndex, "dot1agCfmMepIndex", $dot1agCfmMepIndex)

        case "2": ### dot1agCfmCCheckRestoredEvent

            ##########
            # $1 = dot1agCfmMaintenanceDomainName 
            # $2 = dot1agCfmMaName 
            # $3 = dot1agCfmMepIdentifier 
            ##########

            $dot1agCfmMaintenanceDomainName = $1
            $dot1agCfmMaName = $2
            $dot1agCfmMepIdentifier = $3
            $dot1agCfmMaintenanceDomainIndex = extract($OID3, "\.([0-9]+)\.[0-9]+\.[0-9]+$")
            $dot1agCfmMaIndex = extract($OID3, "\.([0-9]+)\.[0-9]+$")
            $dot1agCfmMepIndex = extract($OID3, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-IEEE-IEEE8021-CFM-MIB-dot1agCfmCCheckRestoredEvent"

            @AlertGroup = "Maintenance Association End Point(MEP) Status"
            @AlertKey = "dot1agCfmMepEntry." + $dot1agCfmMaintenanceDomainIndex + "." + $dot1agCfmMaIndex + "." + $dot1agCfmMepIndex
            @Summary = "MEP Restored Contact With Other MEPs"

            $DEFAULT_Severity = 1 
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ieee, "1")) {
                details($dot1agCfmMaintenanceDomainName,$dot1agCfmMaName,$dot1agCfmMepIdentifier,$dot1agCfmMaintenanceDomainIndex,$dot1agCfmMaIndex,$dot1agCfmMepIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "dot1agCfmMaintenanceDomainName", $dot1agCfmMaintenanceDomainName, "dot1agCfmMaName", $dot1agCfmMaName, "dot1agCfmMepIdentifier", $dot1agCfmMepIdentifier,
                 "dot1agCfmMaintenanceDomainIndex", $dot1agCfmMaintenanceDomainIndex, "dot1agCfmMaIndex", $dot1agCfmMaIndex, "dot1agCfmMepIndex", $dot1agCfmMepIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ieee, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, IEEE-IEEE8021-CFM-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, IEEE-IEEE8021-CFM-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/IEEE/IEEE-IEEE8021-CFM-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/IEEE/IEEE-IEEE8021-CFM-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########
 
include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... IEEE-IEEE8021-CFM-MIB.include.snmptrap.rules >>>>>")


case ".1.3.111.2.802.1.1.8": ### - Notifications from IEEE8021-CFM-MIB (200810150000Z)

    log(DEBUG, "<<<<< Entering... IEEE-IEEE8021-CFM-MIB.include.snmptrap.rules >>>>>")

    @Agent = "IEEE-IEEE8021-CFM-MIB"
    @Class = "40090"
    
    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
     
        case "1": ### dot1agCfmFaultAlarm
        
            ##########
            # $1 = dot1agCfmMepHighestPrDefect
            ##########
            
            $dot1agCfmMepHighestPrDefect = lookup($1, Dot1agCfmHighestDefectPri)
            
            $dot1agCfmMdIndex = extract($OID1, "\.([0-9]+)\.[0-9]+\.[0-9]+$")
            $dot1agCfmMaIndex = extract($OID1, "\.([0-9]+)\.[0-9]+$")
            $dot1agCfmMepIdentifier = extract($OID1, "\.([0-9]+)$")
            
            $OS_EventId = "SNMPTRAP-IEEE-IEEE8021-CFM-MIB-dot1agCfmFaultAlarm"

            @AlertGroup = "Maintenance Association End Point(MEP) Status"
            @AlertKey = "dot1agCfmMepEntry." + $dot1agCfmMdIndex + "." + $dot1agCfmMaIndex + "." + $dot1agCfmMepIdentifier
            @Summary = "MEP Has Persistent Defect, Highest-Priority Defect: " + $dot1agCfmMepHighestPrDefect + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $dot1agCfmMepHighestPrDefect = $dot1agCfmMepHighestPrDefect + " ( " + $1 + " )"
            
             if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ieee, "1")) {
                 details($dot1agCfmMepHighestPrDefect, $dot1agCfmMdIndex, $dot1agCfmMaIndex, $dot1agCfmMepIdentifier)
             }
             @ExtendedAttr = nvp_add(@ExtendedAttr, "dot1agCfmMepHighestPrDefect", $dot1agCfmMepHighestPrDefect, "dot1agCfmMdIndex", $dot1agCfmMdIndex, "dot1agCfmMaIndex", $dot1agCfmMaIndex,
                  "dot1agCfmMepIdentifier", $dot1agCfmMepIdentifier)
         
        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ieee, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, IEEE-IEEE8021-CFM-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, IEEE-IEEE8021-CFM-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/IEEE/IEEE-IEEE8021-CFM-MIB_faultalarm.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/IEEE/IEEE-IEEE8021-CFM-MIB_faultalarm.user.include.snmptrap.rules"


##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... IEEE-IEEE8021-CFM-MIB.include.snmptrap.rules >>>>>")



