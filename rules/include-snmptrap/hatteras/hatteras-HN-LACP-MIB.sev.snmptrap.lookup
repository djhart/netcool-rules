###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 5.0 - Updated Release for HN400/HN4000 7.2.2.
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  HN-LACP-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table hatteras-HN-LACP-MIB_sev =
{
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpMacMismatchNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpMacMismatchNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpMacMismatchNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpMacMismatchNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpMacMismatchNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpMacMismatchNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpMacMismatchNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpKeyMismatchNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpKeyMismatchNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpKeyMismatchNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpKeyMismatchNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpKeyMismatchNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpKeyMismatchNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpKeyMismatchNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpLoopbackNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpLoopbackNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpLoopbackNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpLoopbackNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpLoopbackNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpLoopbackNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpLoopbackNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpLagMisConfigNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpLagMisConfigNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpLagMisConfigNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpLagMisConfigNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpLagMisConfigNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpLagMisConfigNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpLagMisConfigNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpLagDuplexNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpLagDuplexNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpLagDuplexNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpLagDuplexNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpLagDuplexNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpLagDuplexNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-LACP-MIB-hnLacpLagDuplexNotification_unknown","2","1","0"}
}
default = {"Unknown","Unknown","Unknown"}

