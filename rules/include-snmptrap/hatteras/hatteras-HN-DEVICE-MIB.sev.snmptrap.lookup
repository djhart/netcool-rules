###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 5.0 - Updated Release for HN400/HN4000 7.2.2.
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  HN-DEVICE-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table hatteras-HN-DEVICE-MIB_sev =
{
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTestNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTestNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTestNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTestNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTestNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTestNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTestNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceControlComNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceControlComNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceControlComNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceControlComNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceControlComNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceControlComNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceControlComNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceHiTempNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceHiTempNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceHiTempNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceHiTempNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceHiTempNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceHiTempNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceHiTempNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMemoryLowNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMemoryLowNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMemoryLowNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMemoryLowNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMemoryLowNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMemoryLowNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMemoryLowNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceProgramFaultNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceProgramFaultNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceProgramFaultNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceProgramFaultNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceProgramFaultNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceProgramFaultNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceProgramFaultNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceEquipMismatchNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceEquipMismatchNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceEquipMismatchNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceEquipMismatchNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceEquipMismatchNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceEquipMismatchNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceEquipMismatchNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceHiTempCriticalNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceHiTempCriticalNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceHiTempCriticalNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceHiTempCriticalNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceHiTempCriticalNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceHiTempCriticalNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceHiTempCriticalNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMasterConflictNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMasterConflictNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMasterConflictNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMasterConflictNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMasterConflictNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMasterConflictNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMasterConflictNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMacAddrTableNearFullNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMacAddrTableNearFullNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMacAddrTableNearFullNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMacAddrTableNearFullNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMacAddrTableNearFullNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMacAddrTableNearFullNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMacAddrTableNearFullNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMacAddrTableFullNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMacAddrTableFullNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMacAddrTableFullNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMacAddrTableFullNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMacAddrTableFullNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMacAddrTableFullNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMacAddrTableFullNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSoftwareMismatchNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSoftwareMismatchNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSoftwareMismatchNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSoftwareMismatchNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSoftwareMismatchNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSoftwareMismatchNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSoftwareMismatchNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSynchFailedNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSynchFailedNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSynchFailedNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSynchFailedNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSynchFailedNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSynchFailedNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSynchFailedNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceNewMasterNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceNewMasterNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceNewMasterNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceNewMasterNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceNewMasterNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceNewMasterNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceNewMasterNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMasterUnchangedNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMasterUnchangedNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMasterUnchangedNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMasterUnchangedNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMasterUnchangedNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMasterUnchangedNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMasterUnchangedNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceAutoCreatedNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceAutoCreatedNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceAutoCreatedNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceAutoCreatedNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceAutoCreatedNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceAutoCreatedNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceAutoCreatedNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceAutoDeletedNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceAutoDeletedNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceAutoDeletedNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceAutoDeletedNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceAutoDeletedNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceAutoDeletedNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceAutoDeletedNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSntpFailureNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSntpFailureNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSntpFailureNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSntpFailureNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSntpFailureNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSntpFailureNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSntpFailureNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMasterLostNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMasterLostNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMasterLostNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMasterLostNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMasterLostNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMasterLostNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMasterLostNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceImproperRemovalNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceImproperRemovalNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceImproperRemovalNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceImproperRemovalNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceImproperRemovalNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceImproperRemovalNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceImproperRemovalNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSoftwareSyncInitiatedNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSoftwareSyncInitiatedNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSoftwareSyncInitiatedNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSoftwareSyncInitiatedNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSoftwareSyncInitiatedNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSoftwareSyncInitiatedNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSoftwareSyncInitiatedNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSoftwareSyncCompletedNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSoftwareSyncCompletedNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSoftwareSyncCompletedNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSoftwareSyncCompletedNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSoftwareSyncCompletedNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSoftwareSyncCompletedNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceSoftwareSyncCompletedNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTimingHoldoverNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTimingHoldoverNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTimingHoldoverNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTimingHoldoverNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTimingHoldoverNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTimingHoldoverNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTimingHoldoverNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTimingLockFailurePrimaryNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTimingLockFailurePrimaryNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTimingLockFailurePrimaryNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTimingLockFailurePrimaryNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTimingLockFailurePrimaryNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTimingLockFailurePrimaryNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTimingLockFailurePrimaryNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTimingLockFailureSecondaryNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTimingLockFailureSecondaryNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTimingLockFailureSecondaryNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTimingLockFailureSecondaryNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTimingLockFailureSecondaryNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTimingLockFailureSecondaryNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceTimingLockFailureSecondaryNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMcastMacAddrTableNearFullNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMcastMacAddrTableNearFullNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMcastMacAddrTableNearFullNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMcastMacAddrTableNearFullNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMcastMacAddrTableNearFullNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMcastMacAddrTableNearFullNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMcastMacAddrTableNearFullNotification_unknown","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMcastMacAddrTableFullNotification_cleared","1","2","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMcastMacAddrTableFullNotification_notAlarmed","2","13","1800"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMcastMacAddrTableFullNotification_notReported","2","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMcastMacAddrTableFullNotification_minor","3","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMcastMacAddrTableFullNotification_major","4","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMcastMacAddrTableFullNotification_critical","5","1","0"},
    {"SNMPTRAP-hatteras-HN-DEVICE-MIB-hnDeviceMcastMacAddrTableFullNotification_unknown","2","1","0"}
}
default = {"Unknown","Unknown","Unknown"}

