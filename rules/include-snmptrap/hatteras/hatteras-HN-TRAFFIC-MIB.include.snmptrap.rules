###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 5.0 - Updated Release for HN400/HN4000 7.2.2.
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  HN-TRAFFIC-MIB
#
###############################################################################

case ".1.3.6.1.4.1.8550.2.9.2.3": ### Hatteras Networks - Notifications from HN-TRAFFIC-MIB (200912180000Z)

    log(DEBUG, "<<<<< Entering... hatteras-HN-TRAFFIC-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Hatteras-HN-TRAFFIC-MIB"
    @Class = "40535"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### hnTrafficGenActiveNotification

            ##########
            # $1 = hnTrafficGenStream1 
            # $2 = hnEventHistoryId 
            # $3 = hnEventHistoryType 
            # $4 = hnEventHistoryDescr 
            # $5 = hnEventHistorySeverity 
            # $6 = hnEventHistoryServiceAffected 
            # $7 = hnEventHistoryTimeValue 
            ##########

            $hnTrafficGenStream1 = $1
            $hnEventHistoryId = $2
            $hnEventHistoryType = lookup($3,HnAlarmConditionType) + " ( " + $3 + " )"
            $hnEventHistoryDescr = $4
            $hnEventHistorySeverity = lookup($5,HnAlarmSeverity) + " ( " + $5 + " )"
            $hnEventHistoryServiceAffected = lookup($6,HnAlarmServiceAffected) + " ( " + $6 + " )"
            $HexTimeValue = $7_hex
            include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-decodeTimeValue.include.snmptrap.rules"
            $hnEventHistoryTimeValue = $decodedTimeValue
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($hnTrafficGenStream1,$hnEventHistoryId,
                        $hnEventHistoryType,$hnEventHistoryDescr,
                        $hnEventHistorySeverity,
                        $hnEventHistoryServiceAffected,
                        $hnEventHistoryTimeValue)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hnTrafficGenStream1", $hnTrafficGenStream1, "hnEventHistoryId", $hnEventHistoryId, "hnEventHistoryType", $hnEventHistoryType,
                 "hnEventHistoryDescr", $hnEventHistoryDescr, "hnEventHistorySeverity", $hnEventHistorySeverity, "hnEventHistoryServiceAffected", $hnEventHistoryServiceAffected,
                 "hnEventHistoryTimeValue", $hnEventHistoryTimeValue)

            $OS_EventId = "SNMPTRAP-hatteras-HN-TRAFFIC-MIB-hnTrafficGenActiveNotification"

            @AlertGroup = "Traffic Generator Active"
            @AlertKey = "hnTrafficGenEntry." + $hnTrafficGenStream1
            @Summary = "Traffic Generator Active Alarm  ( " + @AlertKey + " )"

            switch($5)
            {
                case "1": ### cleared
                    $SEV_KEY = $OS_EventId + "_cleared"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    @Summary = "End of: " + @Summary

                case "2": ### notAlarmed
                    $SEV_KEY = $OS_EventId + "_notAlarmed"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "3": ### notReported
                    $SEV_KEY = $OS_EventId + "_notReported"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "4": ### minor
                    $SEV_KEY = $OS_EventId + "_minor"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "5": ### major
                    $SEV_KEY = $OS_EventId + "_major"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "6": ### critical
                    $SEV_KEY = $OS_EventId + "_critical"
                    $DEFAULT_Severity = 5
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            update(@Severity)
            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        case "2": ### hnTrafficMonActiveNotification

            ##########
            # $1 = hnTrafficGenStream1 
            # $2 = hnEventHistoryId 
            # $3 = hnEventHistoryType 
            # $4 = hnEventHistoryDescr 
            # $5 = hnEventHistorySeverity 
            # $6 = hnEventHistoryServiceAffected 
            # $7 = hnEventHistoryTimeValue 
            ##########

            $hnTrafficMonStream1 = $1
            $hnEventHistoryId = $2
            $hnEventHistoryType = lookup($3,HnAlarmConditionType) + " ( " + $3 + " )"
            $hnEventHistoryDescr = $4
            $hnEventHistorySeverity = lookup($5,HnAlarmSeverity) + " ( " + $5 + " )"
            $hnEventHistoryServiceAffected = lookup($6,HnAlarmServiceAffected) + " ( " + $6 + " )"
            $HexTimeValue = $7_hex
            include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-decodeTimeValue.include.snmptrap.rules"
            $hnEventHistoryTimeValue = $decodedTimeValue
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($hnTrafficMonStream1,$hnEventHistoryId,
                        $hnEventHistoryType,$hnEventHistoryDescr,
                        $hnEventHistorySeverity,
                        $hnEventHistoryServiceAffected,
                        $hnEventHistoryTimeValue)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "hnTrafficMonStream1", $hnTrafficMonStream1, "hnEventHistoryId", $hnEventHistoryId, "hnEventHistoryType", $hnEventHistoryType,
                 "hnEventHistoryDescr", $hnEventHistoryDescr, "hnEventHistorySeverity", $hnEventHistorySeverity, "hnEventHistoryServiceAffected", $hnEventHistoryServiceAffected,
                 "hnEventHistoryTimeValue", $hnEventHistoryTimeValue)

            $OS_EventId = "SNMPTRAP-hatteras-HN-TRAFFIC-MIB-hnTrafficMonActiveNotification"

            @AlertGroup = "Traffic Monitor Active"
            @AlertKey = "hnTrafficMonEntry." + $hnTrafficMonStream1
            @Summary = "Traffic Monitor Active Alarm  ( " + @AlertKey + " )"

            switch($5)
            {
                case "1": ### cleared
                    $SEV_KEY = $OS_EventId + "_cleared"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                    @Summary = "End of: " + @Summary

                case "2": ### notAlarmed
                    $SEV_KEY = $OS_EventId + "_notAlarmed"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 13
                    $DEFAULT_ExpireTime = 1800

                case "3": ### notReported
                    $SEV_KEY = $OS_EventId + "_notReported"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "4": ### minor
                    $SEV_KEY = $OS_EventId + "_minor"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "5": ### major
                    $SEV_KEY = $OS_EventId + "_major"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                case "6": ### critical
                    $SEV_KEY = $OS_EventId + "_critical"
                    $DEFAULT_Severity = 5
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0

                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            update(@Severity)
            update(@Summary)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_hatteras, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, hatteras-HN-TRAFFIC-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, hatteras-HN-TRAFFIC-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-HN-TRAFFIC-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/hatteras/hatteras-HN-TRAFFIC-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... hatteras-HN-TRAFFIC-MIB.include.snmptrap.rules >>>>>")
