###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  BAY-STACK-NOTIFICATION-MIB
#
###############################################################################

case ".1.3.6.1.4.1.45.5.2.2": ### Nortel's BayStack Products - Notifications from BAY-STACK-NOTIFICATIONS-MIB

    log(DEBUG, "<<<<< Entering... synoptics-BAY-STACK-NOTIFICATIONS-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Nortel-Bay Stack Products"
    @Class = "87018"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### bsnConfigurationSavedToNvram

            $OS_EventId = "SNMPTRAP-synoptics-BAY-STACK-NOTIFICATIONS-MIB-bsnConfigurationSavedToNvram"

            @AlertGroup = "Device Configuration Status"
            @AlertKey = ""
            @Summary = "Device Configuration Saved to Non-Volatile Storage"

            $DEFAULT_Severity = 2 
            $DEFAULT_Type = 13 
            $DEFAULT_ExpireTime = 1800 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        case "2": ### bsnEapAccessViolation

            ##########
            # $1 = dot1xAuthPaeState 
            # $2 = dot1xAuthBackendAuthState 
            # $3 = bsnEapAccessViolationMacAddress
            ##########

            $dot1xAuthPaeState = lookup($1, dot1xAuthPaeState) 
            $dot1xAuthBackendAuthState = lookup($2, dot1xAuthBackendAuthState) 
            $bsnEapAccessViolationMacAddress = $3
            $dot1xPaePortNumber = extract($OID1, "\.([0-9]+)$")
            
            $OS_EventId = "SNMPTRAP-synoptics-BAY-STACK-NOTIFICATIONS-MIB-bsnEapAccessViolation"

            @AlertGroup = "EAP Access Violation"
            @AlertKey = "dot1xAuthConfigEntry." + $dot1xPaePortNumber + ", MAC Address: " + $bsnEapAccessViolationMacAddress
            switch($2)
             {
               case "1": ### request 
                  switch($1)
                    {
                      case "1" : ### Initialize
                        $SEV_KEY = $OS_EventId + "_request_Initialize"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 12 
                      	$DEFAULT_ExpireTime = 0 
                      case "2" : ### disconnected
                        $SEV_KEY = $OS_EventId + "_request_disconnected"

                      	$DEFAULT_Severity = 3
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      case "3" : ### connecting
                        $SEV_KEY = $OS_EventId + "_request_connecting"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 12
                      	$DEFAULT_ExpireTime = 0 
                      case "4" : ### authenticating
                        $SEV_KEY = $OS_EventId + "_request_authenticating"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 12
                      	$DEFAULT_ExpireTime = 0 
                      case "5" : ### authenticated
                        $SEV_KEY = $OS_EventId + "_request_authenticated"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 12
                      	$DEFAULT_ExpireTime = 0 
                      case "6" : ### aborting
                        $SEV_KEY = $OS_EventId + "_request_aborting"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 11
                      	$DEFAULT_ExpireTime = 0
                      case "7" : ### held
                        $SEV_KEY = $OS_EventId + "_request_held"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0 
                      case "8" : ### forceAuth
                        $SEV_KEY = $OS_EventId + "_request_forceAuth"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0 
                      case "9" : ### forceUnauth
                        $SEV_KEY = $OS_EventId + "_request_forceUnauth"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      default:
                        $SEV_KEY = $OS_EventId + "_request_unknown"

                  	$DEFAULT_Severity = 2
                  	$DEFAULT_Type = 1
                  	$DEFAULT_ExpireTime = 0
             	    }
                                   	
               case "2": ### response 
                 switch($1)
                    {
                      case "1" : ### Initialize
                        $SEV_KEY = $OS_EventId + "_response_Initialize"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 12
                      	$DEFAULT_ExpireTime = 0 
                      case "2" : ### disconnected
                        $SEV_KEY = $OS_EventId + "_response_disconnected"

                      	$DEFAULT_Severity = 3
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      case "3" : ### connecting
                        $SEV_KEY = $OS_EventId + "_response_connecting"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 12
                      	$DEFAULT_ExpireTime = 0 
                      case "4" : ### authenticating
                        $SEV_KEY = $OS_EventId + "_response_authenticating"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 12
                      	$DEFAULT_ExpireTime = 0 
                      case "5" : ### authenticated
                        $SEV_KEY = $OS_EventId + "_response_authenticated"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 12
                      	$DEFAULT_ExpireTime = 0 
                      case "6" : ### aborting
                        $SEV_KEY = $OS_EventId + "_response_aborting"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 11
                      	$DEFAULT_ExpireTime = 0
                      case "7" : ### held
                        $SEV_KEY = $OS_EventId + "_response_held"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0 
                      case "8" : ### forceAuth
                        $SEV_KEY = $OS_EventId + "_response_forceAuth"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0 
                      case "9" : ### forceUnauth
                        $SEV_KEY = $OS_EventId + "_response_forceUnauth"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      default:
                        $SEV_KEY = $OS_EventId + "_response_unknown"

                  	$DEFAULT_Severity = 2
                  	$DEFAULT_Type = 1
                  	$DEFAULT_ExpireTime = 0
             	    }
               case "3": ### success 
                 switch($1)
                    {
                      case "1" : ### Initialize
                        $SEV_KEY = $OS_EventId + "_success_Initialize"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 12
                      	$DEFAULT_ExpireTime = 0 
                      case "2" : ### disconnected
                        $SEV_KEY = $OS_EventId + "_success_disconnected"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      case "3" : ### connecting
                        $SEV_KEY = $OS_EventId + "_success_connecting"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 12
                      	$DEFAULT_ExpireTime = 0 
                      case "4" : ### authenticating
                        $SEV_KEY = $OS_EventId + "_success_authenticating"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 12
                      	$DEFAULT_ExpireTime = 0 
                      case "5" : ### authenticated
                        $SEV_KEY = $OS_EventId + "_success_authenticated"

                      	$DEFAULT_Severity = 1
                      	$DEFAULT_Type = 2 
                      	$DEFAULT_ExpireTime = 0 
                      case "6" : ### aborting
                        $SEV_KEY = $OS_EventId + "_success_aborting"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      case "7" : ### held
                        $SEV_KEY = $OS_EventId + "_success_held"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0 
                      case "8" : ### forceAuth
                        $SEV_KEY = $OS_EventId + "_success_forceAuth"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0 
                      case "9" : ### forceUnauth
                        $SEV_KEY = $OS_EventId + "_success_forceUnauth"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      default:
                        $SEV_KEY = $OS_EventId + "_success_unknown"

                  	$DEFAULT_Severity = 2
                  	$DEFAULT_Type = 1
                  	$DEFAULT_ExpireTime = 0
             	    }
               case "4": ### fail 
                  switch($1)
                    {
                      case "1" : ### Initialize
                        $SEV_KEY = $OS_EventId + "_fail_Initialize"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 12
                      	$DEFAULT_ExpireTime = 0
                      case "2" : ### disconnected
                        $SEV_KEY = $OS_EventId + "_fail_disconnected"

                      	$DEFAULT_Severity = 3
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      case "3" : ### connecting
                        $SEV_KEY = $OS_EventId + "_fail_connecting"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      case "4" : ### authenticating
                        $SEV_KEY = $OS_EventId + "_fail_authenticating"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      case "5" : ### authenticated
                        $SEV_KEY = $OS_EventId + "_fail_authenticated"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      case "6" : ### aborting
                        $SEV_KEY = $OS_EventId + "_fail_aborting"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 11
                      	$DEFAULT_ExpireTime = 0
                      case "7" : ### held
                        $SEV_KEY = $OS_EventId + "_fail_held"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      case "8" : ### forceAuth
                        $SEV_KEY = $OS_EventId + "_fail_forceAuth"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      case "9" : ### forceUnauth
                        $SEV_KEY = $OS_EventId + "_fail_forceUnauth"
                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      default:
                        $SEV_KEY = $OS_EventId + "_fail_unknown"

                  	$DEFAULT_Severity = 2
                  	$DEFAULT_Type = 1
                  	$DEFAULT_ExpireTime = 0
             	    }
               case "5": ### timeout 
                 switch($1)
                    {
                      case "1" : ### Initialize
                        $SEV_KEY = $OS_EventId + "_timeout_Initialize"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 12
                      	$DEFAULT_ExpireTime = 0
                      case "2" : ### disconnected
                        $SEV_KEY = $OS_EventId + "_timeout_disconnected"
                      	$DEFAULT_Severity = 3
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      case "3" : ### connecting
                        $SEV_KEY = $OS_EventId + "_timeout_connecting"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      case "4" : ### authenticating
                        $SEV_KEY = $OS_EventId + "_timeout_authenticating"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      case "5" : ### authenticated
                        $SEV_KEY = $OS_EventId + "_timeout_authenticated"
                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 12
                      	$DEFAULT_ExpireTime = 0 
                      case "6" : ### aborting
                        $SEV_KEY = $OS_EventId + "_timeout_aborting"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 11
                      	$DEFAULT_ExpireTime = 0
                      case "7" : ### held
                        $SEV_KEY = $OS_EventId + "_timeout_held"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      case "8" : ### forceAuth
                        $SEV_KEY = $OS_EventId + "_timeout_forceAuth"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      case "9" : ### forceUnauth
                        $SEV_KEY = $OS_EventId + "_timeout_forceUnauth"
                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      default:
                        $SEV_KEY = $OS_EventId + "_timeout_unknown"
                  	$DEFAULT_Severity = 2
                  	$DEFAULT_Type = 1
                  	$DEFAULT_ExpireTime = 0
             	    }
               case "6": ### idle 
                  switch($1)
                    {
                      case "1" : ### Initialize
                        $SEV_KEY = $OS_EventId + "_idle_Initialize"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 12 
                      	$DEFAULT_ExpireTime = 0 
                      case "2" : ### disconnected
                        $SEV_KEY = $OS_EventId + "_idle_disconnected"

                      	$DEFAULT_Severity = 3
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      case "3" : ### connecting
                        $SEV_KEY = $OS_EventId + "_idle_connecting"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 12
                      	$DEFAULT_ExpireTime = 0 
                      case "4" : ### authenticating
                        $SEV_KEY = $OS_EventId + "_idle_authenticating"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 12
                      	$DEFAULT_ExpireTime = 0 
                      case "5" : ### authenticated
                        $SEV_KEY = $OS_EventId + "_idle_authenticated"
                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 12
                      	$DEFAULT_ExpireTime = 0 
                      case "6" : ### aborting
                        $SEV_KEY = $OS_EventId + "_idle_aborting"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 11
                      	$DEFAULT_ExpireTime = 0
                      case "7" : ### held
                        $SEV_KEY = $OS_EventId + "_idle_held"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      case "8" : ### forceAuth
                        $SEV_KEY = $OS_EventId + "_idle_forceAuth"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      case "9" : ### forceUnauth
                        $SEV_KEY = $OS_EventId + "_idle_forceUnauth"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      default:
                        $SEV_KEY = $OS_EventId + "_idle_unknown"

                  	$DEFAULT_Severity = 2
                  	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
             	    }
               case "7": ### initialize 
                  switch($1)
                    {
                      case "1" : ### Initialize
                        $SEV_KEY = $OS_EventId + "_intialize_Initialize"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 12
                      	$DEFAULT_ExpireTime = 0
                      case "2" : ### disconnected
                        $SEV_KEY = $OS_EventId + "_initialize_disconnected"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      case "3" : ### connecting
                        $SEV_KEY = $OS_EventId + "_initialize_connecting"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 12
                      	$DEFAULT_ExpireTime = 0
                      case "4" : ### authenticating
                        $SEV_KEY = $OS_EventId + "_initialize_authenticating"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 12
                      	$DEFAULT_ExpireTime = 0
                      case "5" : ### authenticated
                        $SEV_KEY = $OS_EventId + "_initialize_authenticated"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 12
                      	$DEFAULT_ExpireTime = 0
                      case "6" : ### aborting
                        $SEV_KEY = $OS_EventId + "_initialize_aborting"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      case "7" : ### held
                        $SEV_KEY = $OS_EventId + "_initialize_held"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      case "8" : ### forceAuth
                        $SEV_KEY = $OS_EventId + "_initialize_forceAuth"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      case "9" : ### forceUnauth
                        $SEV_KEY = $OS_EventId + "_initialize_forceUnauth"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      default:
                        $SEV_KEY = $OS_EventId + "_initialize_unknown"

                  	$DEFAULT_Severity = 2
                  	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
             	    }
               default:
                switch($1)
                    {
                      case "1" : ### Initialize
                        $SEV_KEY = $OS_EventId + "_unknown_Initialize"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 12
                      	$DEFAULT_ExpireTime = 0
                      case "2" : ### disconnected
                        $SEV_KEY = $OS_EventId + "_unknown_disconnected"

                      	$DEFAULT_Severity = 3
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      case "3" : ### connecting
                        $SEV_KEY = $OS_EventId + "_unknown_connecting"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 12
                      	$DEFAULT_ExpireTime = 0
                        case "4" : ### authenticating
                        $SEV_KEY = $OS_EventId + "_unknown_authenticating"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 12
                      	$DEFAULT_ExpireTime = 0
                      case "5" : ### authenticated
                        $SEV_KEY = $OS_EventId + "_unknown_authenticated"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 12
                      	$DEFAULT_ExpireTime = 0
                      case "6" : ### aborting
                        $SEV_KEY = $OS_EventId + "_unknown_aborting"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 11
                      	$DEFAULT_ExpireTime = 0
                      case "7" : ### held
                        $SEV_KEY = $OS_EventId + "_unknown_held"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      case "8" : ### forceAuth
                        $SEV_KEY = $OS_EventId + "_unknown_forceAuth"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      case "9" : ### forceUnauth
                        $SEV_KEY = $OS_EventId + "_unknown_forceUnauth"

                      	$DEFAULT_Severity = 2
                      	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                      default:
                        $SEV_KEY = $OS_EventId + "_unknown_unknown"

                  	$DEFAULT_Severity = 2
                  	$DEFAULT_Type = 1
                      	$DEFAULT_ExpireTime = 0
                    }
             }
            @Summary = "EAP Access Violation: Backend " + $dot1xAuthBackendAuthState + ", Client " + $dot1xAuthPaeState 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $1 + " " + $2
            
            $dot1xAuthPaeState = $dot1xAuthPaeState + " ( " + $1 + " )" 
            $dot1xAuthBackendAuthState = $dot1xAuthBackendAuthState + " ( " + $2 + " )"
            
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_synoptics, "1")) {
                details($dot1xAuthPaeState,$dot1xAuthBackendAuthState,$bsnEapAccessViolationMacAddress,$dot1xPaePortNumber)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "dot1xAuthPaeState", $dot1xAuthPaeState, "dot1xAuthBackendAuthState", $dot1xAuthBackendAuthState, "bsnEapAccessViolationMacAddress", $bsnEapAccessViolationMacAddress,
                 "dot1xPaePortNumber", $dot1xPaePortNumber)

        case "3": ### bsnPortSpeedDuplexMismatch

            ##########
            # $1 = ifIndex
            ##########

            $ifIndex = $1
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_synoptics, "1")) {
                details($ifIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "ifIndex", $ifIndex)

            $OS_EventId = "SNMPTRAP-synoptics-BAY-STACK-NOTIFICATIONS-MIB-bsnPortSpeedDuplexMismatch"

            @AlertGroup = "Port Speed or Duplex Mismatch"
            @AlertKey = "ifEntry." + $ifIndex
            @Summary = "Port Speed or Duplex Mismatch  ( " + @AlertKey + " )"

            $DEFAULT_Severity = 3 
            $DEFAULT_Type = 1 
            $DEFAULT_ExpireTime = 0 

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_synoptics, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, synoptics-BAY-STACK-NOTIFICATIONS-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, synoptics-BAY-STACK-NOTIFICATIONS-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

###########
# End of Severity via Lookup.
###########

###########
# Enter "Advanced" and "User" includes.
###########

include "$NC_RULES_HOME/include-snmptrap/synoptics/synoptics-BAY-STACK-NOTIFICATIONS-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/synoptics/synoptics-BAY-STACK-NOTIFICATIONS-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... synoptics-BAY-STACK-NOTIFICATIONS-MIB.include.snmptrap.rules >>>>>")
