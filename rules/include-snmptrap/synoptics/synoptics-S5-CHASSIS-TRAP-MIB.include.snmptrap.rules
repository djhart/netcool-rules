###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  S5-CHASSIS-TRAP-MIB
#
###############################################################################

case ".1.3.6.1.4.1.45.1.6.2.4": ### Bay Networks/Synoptics 5000 Chassis - Notifications from S5-CHASSIS-TRAP-MIB 

    log(DEBUG, "<<<<< Entering... synoptics-S5-CHASSIS-TRAP-MIB.include.snmptrap.rules >>>>>")

    @Agent = "BayNetworks-Synoptics 5000 Chassis"
    @Class = "87018"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### s5CtrHotSwap 

            ##########
            # $1 = s5ChasComType
            # $2 = s5ChasComOperState
            ##########

            $s5ChasComType = $1
            $s5ChasComOperState = lookup($2, s5ChasComOperState) + " ( " + $2 + " )" 
            $s5ChasComGrpIndx = extract($OID1, "\.([0-9]+)$")
            $s5ChasComIndx = extract($OID1,"\.([0-9]+)\.[0-9]+$")
            $s5ChasComSubIndx = extract($OID1,"\.([0-9]+)\.[0-9]+\.[0-9]+$")
            
            $OS_EventId = "SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrHotSwap"

            @AlertGroup = "Chassis Component Status"
            @AlertKey = "s5ChasComEntry." + $s5ChasComGrpIndx + "." + $s5ChasComIndx + "." + $s5ChasComSubIndx 
            switch($2)
            {
                case "1": ### other
                    $SEV_KEY = $OS_EventId + "_other"
                    @Summary = $1 + " Status Unknown"
         
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "2": ### notAvail 
                    $SEV_KEY = $OS_EventId + "_notAvail"
                    @Summary = $1 + " Not Available"
        
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "3": ### removed 
                    $SEV_KEY = $OS_EventId + "_removed"
                    @Summary = $1 + " Removed"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 1 
                    $DEFAULT_ExpireTime = 0
                case "4": ### disabled 
                    $SEV_KEY = $OS_EventId + "_disabled"
                    @Summary = $1 + " Disabled"
        
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1 
                    $DEFAULT_ExpireTime = 0
                case "5": ### normal 
                    $SEV_KEY = $OS_EventId + "_normal"
                    @Summary = $1 + " Normal"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 12 
                    $DEFAULT_ExpireTime = 0
                case "6": ### resetInProg 
                    $SEV_KEY = $OS_EventId + "_resetInProg"
                    @Summary = $1 + " Reset in Progress"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 12 
                    $DEFAULT_ExpireTime = 0
                case "7": ### testing 
                    $SEV_KEY = $OS_EventId + "_testing"
                    @Summary = $1 + " Testing"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 12 
                    $DEFAULT_ExpireTime = 0
                case "8": ### warning 
                    $SEV_KEY = $OS_EventId + "_warning"
                    @Summary = $1 + " Warning"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 1 
                    $DEFAULT_ExpireTime = 0
                case "9": ### nonFatalErr 
                    $SEV_KEY = $OS_EventId + "_nonFatalErr"
                    @Summary = $1 + " Non-Fatal Error"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 1 
                    $DEFAULT_ExpireTime = 0
                case "10": ### fatalErr 
                    $SEV_KEY = $OS_EventId + "_fatalErr"
                    @Summary = $1 + " Fatal Error"
        
                    $DEFAULT_Severity = 3 
                    $DEFAULT_Type = 1 
                    $DEFAULT_ExpireTime = 0
                case "11": ### notConfig 
                    $SEV_KEY = $OS_EventId + "_notConfig"
                    @Summary = $1 + " Not Configured"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 12 
                    $DEFAULT_ExpireTime = 0
                case "12": ### obsoleted 
                    $SEV_KEY = $OS_EventId + "_obsolated"
                    @Summary = $1 + " Obsoleted"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 1 
                    $DEFAULT_ExpireTime = 0
                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = $1 + " Status Unknown"
        
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }
            @Summary = @Summary + "  ( " + @AlertKey + " )"
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $2

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_synoptics, "1")) {
                details($s5ChasComType,$s5ChasComOperState,$s5ChasComGrpIndx,$s5ChasComIndx,$s5ChasComIndx)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "s5ChasComType", $s5ChasComType, "s5ChasComOperState", $s5ChasComOperState, "s5ChasComGrpIndx", $s5ChasComGrpIndx,
                 "s5ChasComIndx", $s5ChasComIndx, "s5ChasComIndx", $s5ChasComIndx)
        
        case "2": ### s5CtrProblem

            ##########
            # $1 = s5ChasComType
            # $2 = s5ChasComOperState
            ##########

            $s5ChasComType = $1
            $s5ChasComOperState = lookup($2, s5ChasComOperState) + " ( " + $2 + " )" 
            $s5ChasComGrpIndx = extract($OID1, "\.([0-9]+)$")
            $s5ChasComIndx = extract($OID1,"\.([0-9]+)\.[0-9]+$")
            $s5ChasComSubIndx = extract($OID1,"\.([0-9]+)\.[0-9]+\.[0-9]+$")

            $OS_EventId = "SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrProblem"

            @AlertGroup = "Chassis Component Status"
            @AlertKey = "s5ChasComEntry." + $s5ChasComGrpIndx + "." + $s5ChasComIndx + "." + $s5ChasComSubIndx
                        switch($2)
            {
                case "1": ### other
                    $SEV_KEY = $OS_EventId + "_other"
                    @Summary = $1 + " Status Unknown"
         
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "2": ### notAvail 
                    $SEV_KEY = $OS_EventId + "_notAvail"
                    @Summary = $1 + " Not Available"
        
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "3": ### removed 
                    $SEV_KEY = $OS_EventId + "_removed"
                    @Summary = $1 + " Removed"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 1 
                    $DEFAULT_ExpireTime = 0
                case "4": ### disabled 
                    $SEV_KEY = $OS_EventId + "_disabled"
                    @Summary = $1 + " Disabled"
        
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1 
                    $DEFAULT_ExpireTime = 0
                case "5": ### normal 
                    $SEV_KEY = $OS_EventId + "_normal"
                    @Summary = $1 + " Normal"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 12 
                    $DEFAULT_ExpireTime = 0
                case "6": ### resetInProg 
                    $SEV_KEY = $OS_EventId + "_resetInProg"
                    @Summary = $1 + " Reset in Progress"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 12 
                    $DEFAULT_ExpireTime = 0
                case "7": ### testing 
                    $SEV_KEY = $OS_EventId + "_testing"
                    @Summary = $1 + " Testing"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 12 
                    $DEFAULT_ExpireTime = 0
                case "8": ### warning 
                    $SEV_KEY = $OS_EventId + "_warning"
                    @Summary = $1 + " Warning"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 1 
                    $DEFAULT_ExpireTime = 0
                case "9": ### nonFatalErr 
                    $SEV_KEY = $OS_EventId + "_nonFatalErr"
                    @Summary = $1 + " Non-Fatal Error"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 1 
                    $DEFAULT_ExpireTime = 0
                case "10": ### fatalErr 
                    $SEV_KEY = $OS_EventId + "_fatalErr"
                    @Summary = $1 + " Fatal Error"
        
                    $DEFAULT_Severity = 3 
                    $DEFAULT_Type = 1 
                    $DEFAULT_ExpireTime = 0
                case "11": ### notConfig 
                    $SEV_KEY = $OS_EventId + "_notConfig"
                    @Summary = $1 + " Not Configured"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 12 
                    $DEFAULT_ExpireTime = 0
                case "12": ### obsoleted 
                    $SEV_KEY = $OS_EventId + "_obsolated"
                    @Summary = $1 + " Obsoleted"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 1 
                    $DEFAULT_ExpireTime = 0
                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = $1 + " Status Unknown"
        
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }
            @Summary = @Summary + "  ( " + @AlertKey + " )"

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $2

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_synoptics, "1")) {
                details($s5ChasComType,$s5ChasComOperState,$s5ChasComGrpIndx,$s5ChasComIndx,$s5ChasComIndx)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "s5ChasComType", $s5ChasComType, "s5ChasComOperState", $s5ChasComOperState, "s5ChasComGrpIndx", $s5ChasComGrpIndx,
                 "s5ChasComIndx", $s5ChasComIndx, "s5ChasComIndx", $s5ChasComIndx)

        case "3": ### s5CtrUnitUp

            ##########
            # $1 = s5ChasComType
            # $2 = s5ChasComOperState 
            ##########

            $s5ChasComType = $1
            $s5ChasComOperState = lookup($2, s5ChasComOperState) + " ( " + $2 + " )" 
            $s5ChasComGrpIndx = extract($OID1, "\.([0-9]+)$")
            $s5ChasComIndx = extract($OID1,"\.([0-9]+)\.[0-9]+$")
            $s5ChasComSubIndx = extract($OID1,"\.([0-9]+)\.[0-9]+\.[0-9]+$")

            $OS_EventId = "SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitUp"

            @AlertGroup = "Chassis Component Status"
            @AlertKey = "s5ChasComEntry." + $s5ChasComGrpIndx + "." + $s5ChasComIndx + "." + $s5ChasComSubIndx 
            switch($2)
            {
                case "1": ### other
                    $SEV_KEY = $OS_EventId + "_other"
                    @Summary = $1 + "Up, Status Unknown"
         
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "2": ### notAvail 
                    $SEV_KEY = $OS_EventId + "_notAvail"
                    @Summary = $1 + "Up, Not Available"
        
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "3": ### removed 
                    $SEV_KEY = $OS_EventId + "_removed"
                    @Summary = $1 + "Up, Removed"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 1 
                    $DEFAULT_ExpireTime = 0
                case "4": ### disabled 
                    $SEV_KEY = $OS_EventId + "_disabled"
                    @Summary = $1 + "Up, Disabled"
        
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1 
                    $DEFAULT_ExpireTime = 0
                case "5": ### normal 
                    $SEV_KEY = $OS_EventId + "_normal"
                    @Summary = $1 + "Up, Normal"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 12 
                    $DEFAULT_ExpireTime = 0
                case "6": ### resetInProg 
                    $SEV_KEY = $OS_EventId + "_resetInProg"
                    @Summary = $1 + "Up, Reset in Progress"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 12 
                    $DEFAULT_ExpireTime = 0
                case "7": ### testing 
                    $SEV_KEY = $OS_EventId + "_testing"
                    @Summary = $1 + "Up, Testing"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 12 
                    $DEFAULT_ExpireTime = 0
                case "8": ### warning 
                    $SEV_KEY = $OS_EventId + "_warning"
                    @Summary = $1 + "Up, Warning"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 1 
                    $DEFAULT_ExpireTime = 0
                case "9": ### nonFatalErr 
                    $SEV_KEY = $OS_EventId + "_nonFatalErr"
                    @Summary = $1 + "Up, Non-Fatal Error"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 1 
                    $DEFAULT_ExpireTime = 0
                case "10": ### fatalErr 
                    $SEV_KEY = $OS_EventId + "_fatalErr"
                    @Summary = $1 + "Up, Fatal Error"
        
                    $DEFAULT_Severity = 3 
                    $DEFAULT_Type = 1 
                    $DEFAULT_ExpireTime = 0
                case "11": ### notConfig 
                    $SEV_KEY = $OS_EventId + "_notConfig"
                    @Summary = $1 + "Up, Not Configured"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 12 
                    $DEFAULT_ExpireTime = 0
                case "12": ### obsoleted 
                    $SEV_KEY = $OS_EventId + "_obsolated"
                    @Summary = $1 + "Up, Obsoleted"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 1 
                    $DEFAULT_ExpireTime = 0
                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = $1 + "Up, Status Unknown"
        
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }
            @Summary = @Summary + "  ( " + @AlertKey + " )"

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $2

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_synoptics, "1")) {
                details($s5ChasComType,$s5ChasComOperState,$s5ChasComGrpIndx,$s5ChasComIndx,$s5ChasComIndx)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "s5ChasComType", $s5ChasComType, "s5ChasComOperState", $s5ChasComOperState, "s5ChasComGrpIndx", $s5ChasComGrpIndx,
                 "s5ChasComIndx", $s5ChasComIndx, "s5ChasComIndx", $s5ChasComIndx)

        case "4": ### s5CtrUnitDown

            ##########
            # $1 = s5ChasComType
            # $2 = s5ChasComOperState 
            ##########

            $s5ChasComType = $1
            $s5ChasComOperState = lookup($2, s5ChasComOperState) + " ( " + $2 + " )" 
            $s5ChasComGrpIndx = extract($OID1, "\.([0-9]+)$")
            $s5ChasComIndx = extract($OID1,"\.([0-9]+)\.[0-9]+$")
            $s5ChasComSubIndx = extract($OID1,"\.([0-9]+)\.[0-9]+\.[0-9]+$")

            $OS_EventId = "SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitDown"

            @AlertGroup = "Chassis Component Status"
            @AlertKey = "s5ChasComEntry." + $s5ChasComGrpIndx + "." + $s5ChasComIndx + "." + $s5ChasComSubIndx 
                        switch($2)
            {
                case "1": ### other
                    $SEV_KEY = $OS_EventId + "_other"
                    @Summary = $1 + "Down, Status Unknown"
         
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "2": ### notAvail 
                    $SEV_KEY = $OS_EventId + "_notAvail"
                    @Summary = $1 + "Down, Not Available"
        
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "3": ### removed 
                    $SEV_KEY = $OS_EventId + "_removed"
                    @Summary = $1 + "Down, Removed"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 1 
                    $DEFAULT_ExpireTime = 0
                case "4": ### disabled 
                    $SEV_KEY = $OS_EventId + "_disabled"
                    @Summary = $1 + "Down, Disabled"
        
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1 
                    $DEFAULT_ExpireTime = 0
                case "5": ### normal 
                    $SEV_KEY = $OS_EventId + "_normal"
                    @Summary = $1 + "Down, Normal"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 12 
                    $DEFAULT_ExpireTime = 0
                case "6": ### resetInProg 
                    $SEV_KEY = $OS_EventId + "_resetInProg"
                    @Summary = $1 + "Down, Reset in Progress"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 12 
                    $DEFAULT_ExpireTime = 0
                case "7": ### testing 
                    $SEV_KEY = $OS_EventId + "_testing"
                    @Summary = $1 + "Down, Testing"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 12 
                    $DEFAULT_ExpireTime = 0
                case "8": ### warning 
                    $SEV_KEY = $OS_EventId + "_warning"
                    @Summary = $1 + "Down, Warning"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 1 
                    $DEFAULT_ExpireTime = 0
                case "9": ### nonFatalErr 
                    $SEV_KEY = $OS_EventId + "_nonFatalErr"
                    @Summary = $1 + "Down, Non-Fatal Error"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 1 
                    $DEFAULT_ExpireTime = 0
                case "10": ### fatalErr 
                    $SEV_KEY = $OS_EventId + "_fatalErr"
                    @Summary = $1 + "Down, Fatal Error"
        
                    $DEFAULT_Severity = 3 
                    $DEFAULT_Type = 1 
                    $DEFAULT_ExpireTime = 0
                case "11": ### notConfig 
                    $SEV_KEY = $OS_EventId + "_notConfig"
                    @Summary = $1 + "Down, Not Configured"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 12 
                    $DEFAULT_ExpireTime = 0
                case "12": ### obsoleted 
                    $SEV_KEY = $OS_EventId + "_obsolated"
                    @Summary = $1 + "Down, Obsoleted"
        
                    $DEFAULT_Severity = 2 
                    $DEFAULT_Type = 1 
                    $DEFAULT_ExpireTime = 0
                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = $1 + "Down, Status Unknown"
        
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }
            @Summary = @Summary + "  ( " + @AlertKey + " )"
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $2

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_synoptics, "1")) {
                details($s5ChasComType,$s5ChasComOperState,$s5ChasComGrpIndx,$s5ChasComIndx,$s5ChasComIndx)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "s5ChasComType", $s5ChasComType, "s5ChasComOperState", $s5ChasComOperState, "s5ChasComGrpIndx", $s5ChasComGrpIndx,
                 "s5ChasComIndx", $s5ChasComIndx, "s5ChasComIndx", $s5ChasComIndx)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_synoptics, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, synoptics-S5-CHASSIS-TRAP-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, synoptics-S5-CHASSIS-TRAP-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

###########
# End of Severity via Lookup.
###########

###########
# Enter "Advanced" and "User" includes.
###########

include "$NC_RULES_HOME/include-snmptrap/synoptics/synoptics-S5-CHASSIS-TRAP-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/synoptics/synoptics-S5-CHASSIS-TRAP-MIB.user.include.snmptrap.rules"

###########
# End of "Advanced" and "User" includes.
###########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... synoptics-S5-CHASSIS-TRAP-MIB.include.snmptrap.rules >>>>>")
