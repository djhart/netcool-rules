###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  S5-CHASSIS-TRAP-MIB
#
# 1.1 - Changed the format
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table synoptics-S5-CHASSIS-TRAP-MIB_sev =
{
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrHotSwap_other","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrHotSwap_notAvail","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrHotSwap_removed","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrHotSwap_disabled","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrHotSwap_normal","2","12","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrHotSwap_resetInProg","2","12","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrHotSwap_testing","2","12","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrHotSwap_warning","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrHotSwap_nonFatalErr","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrHotSwap_fatalErr","3","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrHotSwap_notConfig","2","12","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrHotSwap_obsolated","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrHotSwap_unknown","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrProblem_other","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrProblem_notAvail","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrProblem_removed","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrProblem_disabled","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrProblem_normal","2","12","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrProblem_resetInProg","2","12","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrProblem_testing","2","12","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrProblem_warning","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrProblem_nonFatalErr","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrProblem_fatalErr","3","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrProblem_notConfig","2","12","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrProblem_obsolated","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrProblem_unknown","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitUp_other","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitUp_notAvail","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitUp_removed","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitUp_disabled","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitUp_normal","2","12","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitUp_resetInProg","2","12","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitUp_testing","2","12","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitUp_warning","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitUp_nonFatalErr","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitUp_fatalErr","3","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitUp_notConfig","2","12","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitUp_obsolated","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitUp_unknown","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitDown_other","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitDown_notAvail","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitDown_removed","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitDown_disabled","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitDown_normal","2","12","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitDown_resetInProg","2","12","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitDown_testing","2","12","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitDown_warning","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitDown_nonFatalErr","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitDown_fatalErr","3","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitDown_notConfig","2","12","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitDown_obsolated","2","1","0"},
    {"SNMPTRAP-synoptics-S5-CHASSIS-TRAP-MIB-s5CtrUnitDown_unknown","2","1","0"}
}
default = {"Unknown","Unknown","Unknown"}
