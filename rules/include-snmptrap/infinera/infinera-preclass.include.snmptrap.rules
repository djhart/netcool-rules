###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 15.0 - Updated Release for Infinera R10.0
#
#       Added new lookup entries in alarmProbableCause (563-577),
#       infineraObjectType (FSM - XOFX) and 
#       InfnManagedObjectType (123-152) table.
#
#       Compatible with:
#
#         - Infinera DTN and DTN-X release 10.0
#         - Infinera ATN release 4.0
#         - Infinera FIS release 10.0
#
# 14.0 - Updated Release for Infinera R9.0
#
#       Added new lookup entries in alarmProbableCause, tcaProbableCause and 
#       infineraObjectType table.
#
#       Compatible with:
#
#         - Infinera DTN and DTN-X release 9.0
#         - Infinera ATN release 4.0
#         - Infinera FIS release 9.0
#
# 13.0 - Updated Release for Infinera R8.2
#
#       Added new lookup entry (degradedConfig) in alarmProbableCause table.
#
#       Compatible with:
#
#         - Infinera DTN and DTN-X release 8.2
#         - Infinera ATN release 4.0
#         - Infinera FIS release 8.2
#
# 12.0 - Updated Release for Infinera R8.1
#
#       Added new lookup entries in infineraObjectType and alarmProbableCause tables.
#
#       Compatible with:
#
#         - Infinera DTN and DTN-X release 8.1
#         - Infinera ATN release 4.0
#         - Infinera FIS release 8.1
#
# 11.0 - Updated Release for Infinera DTN 8.0
#
#       Added new lookup entries
#
#       Compatible with:
#
#         - Infinera DTN and DTN-X release 8.0
#         - Infinera ATN release 3.0
#         - Infinera FIS release 8.0
#
# 10.0 - Updated Release for Infinera DTN 7.0
#
#       Compatible with:
#
#         - Infinera DTN release 7.0
#         - Infinera ATN release 3.0
#         - Infinera FIS release 7.0
#
# 9.0 - Updated Release for Infinera DTN 6.0, Infinera ATN 3.0 and FIS 7.0
#
#       Compatible with:
#
#         - Infinera DTN release 6.0
#         - Infinera ATN release 3.0
#         - Infinera FIS release 7.0
#
# 2.0 - Updated Release for Infinera ATN 2.0 and FIS 6.1
# 
#       Compatible with:
#
#         -  Infinera DTN release 6.0
#         -  Infinera ATN release 2.0
#         -  Infinera FIS release 6.1
#
# 1.0 - Initial Release.
#
#       Compatible with:
#
#         -  Infinera DTN release 6.0
#         -  Infinera ATN release 1.0
#         -  Infinera FIS release 6.0
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
###############################################################################

log(DEBUG, "<<<<< Entering... infinera-preclass.include.snmptrap.rules >>>>>")

if(match(@Type, "2"))
{
    $OS_AdvCorrCauseType_infinera = 4
}
else
{
    if(exists($SEV_KEY))
    {
        $OS_AdvCorrCauseType_infinera = lookup($SEV_KEY, infinera_preclass)
    }
    else
    {
        $OS_AdvCorrCauseType_infinera = lookup($OS_EventId, infinera_preclass)
    }
}

log(DEBUG, "<<<<< Leaving... infinera-preclass.include.snmptrap.rules >>>>>")
