###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 15.0 - Updated Release for Infinera R10.0
#
#       Added new lookup entries in alarmProbableCause (563-577),
#       infineraObjectType (FSM - XOFX) and 
#       InfnManagedObjectType (123-152) table.
#
#       Compatible with:
#
#         - Infinera DTN and DTN-X release 10.0
#         - Infinera ATN release 4.0
#         - Infinera FIS release 10.0
#
# 14.0 - Updated Release for Infinera R9.0
#
#       Added new lookup entries in alarmProbableCause, tcaProbableCause and 
#       infineraObjectType table.
#
#       Compatible with:
#
#         - Infinera DTN and DTN-X release 9.0
#         - Infinera ATN release 4.0
#         - Infinera FIS release 9.0
#
# 13.0 - Updated Release for Infinera R8.2
#
#       Added new lookup entry (degradedConfig) in alarmProbableCause table.
#
#       Compatible with:
#
#         - Infinera DTN and DTN-X release 8.2
#         - Infinera ATN release 4.0
#         - Infinera FIS release 8.2
#
# 12.0 - Updated Release for Infinera R8.1
#
#       Added new lookup entries in infineraObjectType and alarmProbableCause tables.
#
#       Compatible with:
#
#         - Infinera DTN and DTN-X release 8.1
#         - Infinera ATN release 4.0
#         - Infinera FIS release 8.1
#
# 11.0 - Updated Release for Infinera DTN 8.0
#
#       Added new lookup entries
#
#       Compatible with:
#
#         - Infinera DTN and DTN-X release 8.0
#         - Infinera ATN release 3.0
#         - Infinera FIS release 8.0
#
# 10.0 - Updated Release for Infinera DTN 7.0
#
#       Compatible with:
#
#         - Infinera DTN release 7.0
#         - Infinera ATN release 3.0
#         - Infinera FIS release 7.0
#
# 9.0 - Updated Release for Infinera DTN 6.0, Infinera ATN 3.0 and FIS 7.0
#
#       Compatible with:
#
#         - Infinera DTN release 6.0
#         - Infinera ATN release 3.0
#         - Infinera FIS release 7.0
#
# 5.0 - Updated Release for Infinera ATN 2.0 and FIS 6.1
# 
#       Compatible with:
#
#         -  Infinera DTN release 6.0
#         -  Infinera ATN release 2.0
#         -  Infinera FIS release 6.1
#
# 4.0 - Updated Release for Infinera FIS 6.0 and DTN 6.0
#
#       Compatible with:
#
#         -  Infinera DTN release 6.0
#         -  Infinera ATN release 1.0
#         -  Infinera FIS release 6.0
#
# 3.0 - Additional support for Infinera ATN, release 1.0
#       Package renamed to Infinera-ATN-DTN-NIM-03-SnmpTrap.zip
#
# 2.0 - Incremental support for Infinera DTN, release 5.1
#
# 1.0 - Initial Release.
#
#       Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#         -  Infinera DTN, release 5.0
#         -  INFINERA-NOTIFICATION-MIB
#
###############################################################################

log(DEBUG, "<<<<< Entering... infinera-INFINERA-NOTIFICATION-MIB.user.include.snmptrap.rules >>>>>")

switch($specific-trap)
{
    case "1": ### infnAlarmNotification

        

    case "2": ### infnTcaNotification

        

    case "3": ### infnAdminEventNotification

      ##########
      # The next line can be used to turn deduplication for
      # infnAdminEventNotification events "ON" or "OFF".
      # The default value is "OFF".
      ##########

      $infnAdminEventNotification_dedup_toggle = "OFF"

      if(match($infnAdminEventNotification_dedup_toggle,"ON"))
      {
        @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
        @Identifier = $infnAdminNodeId + " " + @Identifier
      }

    case "4": ### infnAuditEventNotification

        
    case "5": ### infnSecurityEventNotification

      ##########
      # The next line can be used to turn deduplication for
      # infnSecurityEventNotification events "ON" or "OFF".
      # The default value is "OFF".
      ##########

      $infnSecurityEventNotification_dedup_toggle = "OFF"

      if(match($infnSecurityEventNotification_dedup_toggle,"ON"))
      {
        @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
        @Identifier = $infnSecurityNodeId + " " + @Identifier
      }

    default:
}

log(DEBUG, "<<<<< Leaving... infinera-INFINERA-NOTIFICATION-MIB.user.include.snmptrap.rules >>>>>")
