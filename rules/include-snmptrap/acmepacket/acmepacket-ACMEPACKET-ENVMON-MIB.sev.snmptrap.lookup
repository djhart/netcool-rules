###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 2.0 - Updated release for
#     - Acme Packet Net-Net C Series (3000/4000) 6.2.0 and
#     - Acme Packet Net-Net D Series (9000) 7.0.0
#
#     - Repackaged for NIM-02
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#          -  Acme Packet Net-Net 4000 Series - release 5.0, 5.1
#
#          -  ACMEPACKET-ENVMON-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table acmepacket-ACMEPACKET-ENVMON-MIB_sev =
{
    {"SNMPTRAP-acmepacket-ACMEPACKET-ENVMON-MIB-apEnvMonI2CFailNotification","4","1","0"}, 
    {"SNMPTRAP-acmepacket-ACMEPACKET-ENVMON-MIB-apEnvMonStatusChangeNotification_initial","2","13","1800"}, 
    {"SNMPTRAP-acmepacket-ACMEPACKET-ENVMON-MIB-apEnvMonStatusChangeNotification_normal","1","2","0"}, 
    {"SNMPTRAP-acmepacket-ACMEPACKET-ENVMON-MIB-apEnvMonStatusChangeNotification_minor","3","1","0"}, 
    {"SNMPTRAP-acmepacket-ACMEPACKET-ENVMON-MIB-apEnvMonStatusChangeNotification_major","4","1","0"}, 
    {"SNMPTRAP-acmepacket-ACMEPACKET-ENVMON-MIB-apEnvMonStatusChangeNotification_critical","5","1","0"}, 
    {"SNMPTRAP-acmepacket-ACMEPACKET-ENVMON-MIB-apEnvMonStatusChangeNotification_shutdown","4","1","0"}, 
    {"SNMPTRAP-acmepacket-ACMEPACKET-ENVMON-MIB-apEnvMonStatusChangeNotification_notPresent","3","1","0"}, 
    {"SNMPTRAP-acmepacket-ACMEPACKET-ENVMON-MIB-apEnvMonStatusChangeNotification_notFunctioning","3","1","0"}, 
    {"SNMPTRAP-acmepacket-ACMEPACKET-ENVMON-MIB-apEnvMonStatusChangeNotification_unknown","2","1","0"}, 
    {"SNMPTRAP-acmepacket-ACMEPACKET-ENVMON-MIB-apEnvMonTempChangeNotification","3","1","0"}, 
    {"SNMPTRAP-acmepacket-ACMEPACKET-ENVMON-MIB-apEnvMonVoltageChangeNotification","3","1","0"}, 
    {"SNMPTRAP-acmepacket-ACMEPACKET-ENVMON-MIB-apEnvMonPortChangeNotification_inserted","1","2","0"}, 
    {"SNMPTRAP-acmepacket-ACMEPACKET-ENVMON-MIB-apEnvMonPortChangeNotification_removed","5","1","0"}, 
    {"SNMPTRAP-acmepacket-ACMEPACKET-ENVMON-MIB-apEnvMonPortChangeNotification_unknown","2","1","0"} 
}
default = {"Unknown","Unknown","Unknown"}
