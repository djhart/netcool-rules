###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 2.0 - Updated release for
#     - Acme Packet Net-Net C Series (3000/4000) 6.2.0 and
#     - Acme Packet Net-Net D Series (9000) 7.0.0
#
#     - Repackaged for NIM-02
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#          -  Acme Packet Net-Net 4000 Series - release 5.0, 5.1
#
#          -  APSYSLOG-MIB
#
#
###############################################################################

case ".1.3.6.1.4.1.9148.3.1.2": ###  - Notifications from APSYSLOG-MIB (0006211500Z)

    log(DEBUG, "<<<<< Entering... acmepacket-APSYSLOG-MIB.include.snmptrap.rules >>>>>")

    @Agent = "APSYSLOG-MIB"
    @Class = "40682"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### apSyslogMessageGenerated

            ##########
            # $1 = apSyslogHistFrom 
            # $2 = apSyslogHistLevel 
            # $3 = apSyslogHistType 
            # $4 = apSyslogHistContent 
            # $5 = apSyslogHistTimestamp 
            ##########
            
            ##########
            # Syslog messages can be safely discarded (per Acme Packet recommendation)
            ##########

            discard

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_acmepacket, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

#if(exists($SEV_KEY))
#{
#    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, acmepacket-APSYSLOG-MIB_sev)
#}
#else
#{
#    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, acmepacket-APSYSLOG-MIB_sev)
#}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

#include "$NC_RULES_HOME/include-snmptrap/acmepacket-APSYSLOG-MIB.adv.include.snmptrap.rules"
#include "$NC_RULES_HOME/include-snmptrap/acmepacket-APSYSLOG-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... acmepacket-APSYSLOG-MIB.include.snmptrap.rules >>>>>")
