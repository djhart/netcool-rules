###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# Compatible with JUNOS 12.2
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  JUNIPER-IF-MIB
#
###############################################################################

case ".1.3.6.1.4.1.2636.3.3.3": ### - Notifications from JUNIPER-IF-MIB (201109221523Z)

	log(DEBUG, "<<<<< Entering... juniper-JUNIPER-IF-MIB.include.snmptrap.rules >>>>>")

    @Agent = "juniper-JUNIPER-IF-MIB"
    @Class = "40200"
    
    $OPTION_TypeFieldUsage = "3.6"
    

    switch($specific-trap)
    {
    
     
        case "1": ### ifJnxErrors
        
            ##########
            # $1 = ifJnxCrcErrors
            # $2 = ifJnxFcsErrors
            ##########
            
            $ifJnxCrcErrors = $1
            $ifJnxFcsErrors = $2
            
            $ifIndex = extract($OID1, "\.([0-9]+)$")
            
            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-IF-MIB-ifJnxErrors"

            @AlertGroup = "JNX Error"
            @AlertKey = "ifJnxEntry" + "." + $ifIndex
            @Summary = "Value of ifJnxCrcErrors or ifJnxFcsErrors increases" + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
 			if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
     			details($ifJnxCrcErrors, $ifJnxFcsErrors, $ifIndex)
 			}
 			@ExtendedAttr = nvp_add(@ExtendedAttr, "ifJnxCrcErrors", $ifJnxCrcErrors, "ifJnxFcsErrors", $ifJnxFcsErrors, "ifIndex", $ifIndex)
         
        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, juniper-JUNIPER-IF-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, juniper-JUNIPER-IF-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-IF-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-IF-MIB.user.include.snmptrap.rules"


##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... juniper-JUNIPER-IF-MIB.include.snmptrap.rules >>>>>")


