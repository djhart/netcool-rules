###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 8.0 - Updated release for JUNOS 9.4, JUNOSe 10.3 and BX-OS 4.1
#
#          - Repackaged for NIM-08
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  JUNIPER-ERX-SYSTEM-MIB
#
###############################################################################

table JuniSysCardType =
{
    ##########
    # This object indicates the type of card in a system slot
    ##########

    {"1","Switch/Route Processor"}, ### srp
    {"2","Channelized T3"}, ### ct3
    {"3","OC-3 (SONET/SDH)"}, ### oc3
    {"4","Unchannelized T3 (ATM service)"}, ### ut3Atm
    {"5","Unchannelized T3 (Frame service)"}, ### ut3Frame
    {"6","Unchannelized E3 (ATM service)"}, ### ue3Atm
    {"7","Unchannelized E3 (Frame service)"}, ### ue3Frame
    {"8","Channelized E1"}, ### ce1
    {"9","Channelized T1"}, ### ct1
    {"10","Dual Port Fast Ethernet"}, ### dpfe
    {"11","OC-12 (POS/SDH)"}, ### oc12Pos
    {"12","OC-12 (ATM service)"}, ### oc12Atm
    {"13","Quad OC-3 (POS/SDH)"}, ### oc3Pos
    {"14","Quad OC-3 (ATM service)"}, ### oc3Atm
    {"15","Gigabit Ethernet"}, ### ge
    {"16","Fast Ethernet 8-port"}, ### fe8
    {"17","Generic OC3/OC12 POS (multi-personality)"}, ### oc3oc12Pos
    {"18","Generic OC3/OC12 ATM (multi-personality)"}, ### oc3oc12Atm
    {"19","Channelized generic OC3/OC12 (multi-personality)"}, ### coc3oc12
    {"20","Channelized OC3"}, ### coc3
    {"21","Channelized OC12"}, ### coc12
    {"22","OC-12 Rate Server Card"}, ### oc12Server
    {"23","High Speed Serial Interface"}, ### hssi
    {"24","Generic GE/FE (multi-personality)"}, ### geFe
    {"25","Channelized T3 12-port"}, ### ct3P12
    {"26","X.21/V.35 Card"}, ### v35
    {"27","Unchannelized T3 12-port"}, ### ut3f12
    {"28","Unchannelized E3 12-port"}, ### ue3f12
    {"29","OC-12 channelized to T3"}, ### coc12F3
    {"30","OC-3 channelized to T3"}, ### coc3F3
    {"31","12-port T3/E3 or OC3/OC12 (multi-personality)"}, ### cocxF3
    {"32","Virtual Tunnel Server"}, ### vts
    {"33","OC-48 (SONET/SDH)"}, ### oc48
    {"34","4-port Unchannelized T3 ATM"}, ### ut3Atm4
    {"35","Generic ATM/POS/GE Hybrid (multi-personality)"}, ### hybrid
    {"36","OC3 ATM 2-port Gigabit Ethernet 1-port"}, ### oc3AtmGe
    {"37","OC3 ATM 2-port OC3 POS  2-port"}, ### oc3AtmPos
    {"38","Gigabit Ethernet 2-port"} ### ge2
}
default = "Unknown"

table JuniEnable  =
{
    ##########
    # This object indicates the enterprise-standard SYNTAX for MIB objects. Used for both admin (configurable) and oper (read-only) objects 
    ##########

    {"0","Disable"}, ### disable
    {"1","Enable"} ### enable
}
default = "Unknown"

table JuniERXSysSlotOperStatus =
{
    {"1","Empty"}, ### empty
    {"2","Disabled"}, ### disabled
    {"3","Failed"}, ### failed
    {"4","Booting"}, ### booting
    {"5","Initializing"}, ### initializing
    {"6","Online"}, ### online
    {"7","Standby"}, ### standby
    {"8","Inactive"} ### inactive
}
default = "Unknown"

table JuniERXSysSlotDisableReason =
{
    {"0","None"}, ### none
    {"2","Assessing"}, ### assessing
    {"3","Admin"}, ### admin
    {"4","Card Mismatch"}, ### cardMismatch
    {"5","Fabric Limit"}, ### fabricLimit
    {"6","Image Error"} ### imageError
}
default = "Unknown"

table JuniERXSysPowerStatus = 
{
    {"0","Inactive"}, ### inactive
    {"1","Active"} ### active
}
default = "Unknown"

table JuniERXSysTempFanStatus = 
{
    {"0","Failed"}, ### failed
    {"1","OK"}, ### ok
    {"2","Warning"} ### warning
}
default = "Unknown"

table JuniERXSysTempStatus = 
{
    {"1","Failed"}, ### failed
    {"2","Too Low"}, ### tooLow
    {"3","Nominal"}, ### nominal
    {"4","Too High"}, ### tooHigh
    {"5","Too Low Warning"}, ### tooLowWarning
    {"6","Too High Warning"} ### tooHighWarning
}
default = "Unknown"

table JuniERXSysTempProtectionStatus =
{
    {"1","Monitoring"}, ### monitoring
    {"2","In Hold Off"}, ### inHoldOff
    {"3","Activated Hold Off Expired"}, ### activatedHoldOffExpired
    {"4","Activated Temp Too High"} ### activatedTempTooHigh
}
default = "Unknown"

