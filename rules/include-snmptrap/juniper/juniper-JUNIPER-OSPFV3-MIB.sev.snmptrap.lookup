###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
################################################################################
#
# 8.0 - Updated release for JUNOS 9.4, JUNOSe 10.3 and BX-OS 4.1
#
#          - Repackaged for NIM-08
#
###############################################################################
#
#       Compatible with:
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#         -  JUNIPER-OSPFV3-MIB
#
###############################################################################

#
# Entries in a "Severity" lookup table have the following format:
#
# {"<EventId>","<severity>","<type>","<expiretime>"}
#
# <EventId> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table juniper-JUNIPER-OSPFV3-MIB_sev =
{
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3VirtIfStateChange_down","3","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3VirtIfStateChange_pointToPoint","1","2","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3VirtIfStateChange_unknown","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3NbrStateChange_down","3","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3NbrStateChange_attempt","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3NbrStateChange_init","1","2","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3NbrStateChange_twoWay","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3NbrStateChange_exchangeStart","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3NbrStateChange_exchange","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3NbrStateChange_loading","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3NbrStateChange_full","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3NbrStateChange_unknown","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3VirtNbrStateChange_down","3","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3VirtNbrStateChange_attempt","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3VirtNbrStateChange_init","1","2","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3VirtNbrStateChange_twoWay","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3VirtNbrStateChange_exchangeStart","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3VirtNbrStateChange_exchange","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3VirtNbrStateChange_loading","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3VirtNbrStateChange_full","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3VirtNbrStateChange_unknown","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3IfConfigError_down","3","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3IfConfigError_loopback","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3IfConfigError_waiting","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3IfConfigError_pointToPoint","1","2","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3IfConfigError_designatedRouter","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3IfConfigError_backupDesignatedRouter","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3IfConfigError_otherDesignatedRouter","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3IfConfigError_unknown","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3VirtIfConfigError_down","3","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3VirtIfConfigError_pointToPoint","1","2","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3VirtIfConfigError_unknown","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3IfRxBadPacket_down","3","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3IfRxBadPacket_loopback","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3IfRxBadPacket_waiting","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3IfRxBadPacket_pointToPoint","1","2","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3IfRxBadPacket_designatedRouter","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3IfRxBadPacket_backupDesignatedRouter","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3IfRxBadPacket_otherDesignatedRouter","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3IfRxBadPacket_unknown","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3VirtIfRxBadPacket_down","3","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3VirtIfRxBadPacket_pointToPoint","1","2","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3VirtIfRxBadPacket_unknown","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3LsdbOverflow","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3LsdbApproachingOverflow","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3IfStateChange_down","3","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3IfStateChange_loopback","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3IfStateChange_waiting","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3IfStateChange_pointToPoint","1","2","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3IfStateChange_designatedRouter","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3IfStateChange_backupDesignatedRouter","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3IfStateChange_otherDesignatedRouter","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3IfStateChange_unknown","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3NssaTranslatorStatusChange_enabled","1","2","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3NssaTranslatorStatusChange_elected","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3NssaTranslatorStatusChange_disabled","3","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3NssaTranslatorStatusChange_unknown","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3RestartStatusChange_none","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3RestartStatusChange_inProgress","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3RestartStatusChange_completed","1","2","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3RestartStatusChange_timedOut","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3RestartStatusChange_topologyChanged","3","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3RestartStatusChange_unknown","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3NbrRestartHelperStatusChange_none","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3NbrRestartHelperStatusChange_inProgress","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3NbrRestartHelperStatusChange_completed","1","2","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3NbrRestartHelperStatusChange_timedOut","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3NbrRestartHelperStatusChange_topologyChanged","3","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3NbrRestartHelperStatusChange_unknown","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3VirtNbrRestartHelperStatusChange_none","2","13","1800"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3VirtNbrRestartHelperStatusChange_inProgress","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3VirtNbrRestartHelperStatusChange_completed","1","2","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3VirtNbrRestartHelperStatusChange_timedOut","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3VirtNbrRestartHelperStatusChange_topologyChanged","3","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-OSPFV3-MIB-jnxOspfv3VirtNbrRestartHelperStatusChange_unknown","2","1","0"}
}
default = {"Unknown","Unknown","Unknown"}
