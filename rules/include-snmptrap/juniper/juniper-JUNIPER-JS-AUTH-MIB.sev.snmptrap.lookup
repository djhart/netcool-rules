###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
################################################################################
#
# 8.0 - Updated release for JUNOS 9.4, JUNOSe 10.3 and BX-OS 4.1
#
#          - Repackaged for NIM-08
#
###############################################################################
#
#       Compatible with:
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#         -  JUNIPER-JS-AUTH-MIB
#
###############################################################################

#
# Entries in a "Severity" lookup table have the following format:
#
# {"<EventId>","<severity>","<type>","<expiretime>"}
#
# <EventId> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table juniper-JUNIPER-JS-AUTH-MIB_sev =
{
    {"SNMPTRAP-juniper-JUNIPER-JS-AUTH-MIB-jnxJsFwAuthFailure","2","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-JS-AUTH-MIB-jnxJsFwAuthServiceUp","1","2","0"},
    {"SNMPTRAP-juniper-JUNIPER-JS-AUTH-MIB-jnxJsFwAuthServiceDown","3","1","0"},
    {"SNMPTRAP-juniper-JUNIPER-JS-AUTH-MIB-jnxJsFwAuthCapacityExceeded","2","1","0"}
}
default = {"Unknown","Unknown","Unknown"}
