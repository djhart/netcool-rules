###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 9.0 - Updated release for JUNOS 10.4, JUNOSe 11.3
#
# 8.0 - Updated release for JUNOS 9.4, JUNOSe 10.3 and BX-OS 4.1
#
#          - Repackaged for NIM-08
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  JUNIPER-L2CP-FEATURES-MIB
#
###############################################################################

case ".1.3.6.1.4.1.2636.3.53.1.2": ###  Notifications from JUNIPER-L2CP-FEATURES-MIB (201006110000Z)

    log(DEBUG, "<<<<< Entering... juniper-JUNIPER-L2CP-FEATURES-MIB.include.snmptrap.rules >>>>>")

    @Agent = "JUNIPER-L2CP-FEATURES-MIB"
    @Class = "40200"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### jnxPortRootProtectStateChangeTrap

            ##########
            # $1 = jnxDot1dStpPortRootProtectState 
            ##########

            $jnxDot1dStpPortRootProtectState = lookup($1, Dot1dStpPortRootProtectState) + " ( " + $1 + " )"

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-L2CP-FEATURES-MIB-jnxPortRootProtectStateChangeTrap"
            
            $dot1dStpPort =  extract($OID1, "\.([0-9]+)$")
	    @AlertKey = "dot1dStpPortEntry." + $dot1dStpPort

            @AlertGroup = "Root Protect State"

	    switch($1)
            {
                case "0": ### no-error
                    $SEV_KEY = $OS_EventId + "_no-error"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
		    @Summary = "Port - " + @AlertKey + " can be a root port."

                case "1": ### root-prevented
                    $SEV_KEY = $OS_EventId + "_root-prevented"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
		    @Summary = "Port - " + @AlertKey + " is prevented from being a root port."
 
		default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
		    @Summary = "Root State unknown for the port - " + @AlertKey

            }

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($jnxDot1dStpPortRootProtectState,$dot1dStpPort)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "jnxDot1dStpPortRootProtectState", $jnxDot1dStpPortRootProtectState, "dot1dStpPort", $dot1dStpPort)

        case "2": ### jnxPortLoopProtectStateChangeTrap

            ##########
            # $1 = jnxDot1dStpPortLoopProtectState 
            ##########

            $jnxDot1dStpPortLoopProtectState = lookup($1, Dot1dStpPortLoopProtectState) + " ( " + $1 + " ) "

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-L2CP-FEATURES-MIB-jnxPortLoopProtectStateChangeTrap"
            
            $dot1dStpPort = extract($OID1, "\.([0-9]+)$")
	    @AlertKey = "dot1dStpPortEntry." + $dot1dStpPort

            @AlertGroup = "Loop Protect State"

	    switch($1)
            {
                case "0": ### no-error
                    $SEV_KEY = $OS_EventId + "_no-error"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
		    @Summary = "Port - " + @AlertKey + " can have potential loop."

                case "1": ### loop-prevented
                    $SEV_KEY = $OS_EventId + "_loop-prevented"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
		    @Summary = "Potential loop was prevented on the Port - " + @AlertKey
 
		default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
		    @Summary = "Loop State unknown for the port - " + @AlertKey
            }

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($jnxDot1dStpPortLoopProtectState,$dot1dStpPort)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "jnxDot1dStpPortLoopProtectState", $jnxDot1dStpPortLoopProtectState, "dot1dStpPort", $dot1dStpPort)

        case "3": ### jnxPortBpduErrorStatusChangeTrap

            ##########
            # $1 = jnxL2cpPortBpduError 
            ##########

            $jnxL2cpPortBpduError = lookup($1, L2cpPortBpduError) + " ( " + $1 + " ) "

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-L2CP-FEATURES-MIB-jnxPortBpduErrorStatusChangeTrap"

            $ifIndex =  extract($OID1, "\.([0-9]+)$")
	    @AlertKey = "ifEntry." + $ifIndex
            
            @AlertGroup = "BPDU Protect Functionality"

	    switch($1)
            {
                case "0": ### no-error
                    $SEV_KEY = $OS_EventId + "_no-error"
                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
		    @Summary = "BPDU protect Functionality is not enabled on the Port - " + @AlertKey

                case "1": ### detected
                    $SEV_KEY = $OS_EventId + "_detected"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
		    @Summary = "Port - " + @AlertKey + " will be disabled on receipt of a BPDU."
 
		default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
		    @Summary = "BPDU protect Functionality unknown for the Port - " + @AlertKey
            }

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($jnxL2cpPortBpduError,$ifIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "jnxL2cpPortBpduError", $jnxL2cpPortBpduError, "ifIndex", $ifIndex)


        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, juniper-JUNIPER-L2CP-FEATURES-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, juniper-JUNIPER-L2CP-FEATURES-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-L2CP-FEATURES-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-L2CP-FEATURES-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... juniper-JUNIPER-L2CP-FEATURES-MIB.include.snmptrap.rules >>>>>")

case ".1.3.6.1.4.1.2636.3.53.1.2.1": ###  - Notifications from JUNIPER-L2CP-FEATURES-MIB (201006110000Z)

    log(DEBUG, "<<<<< Entering... juniper-JUNIPER-L2CP-FEATURES-MIB.include.snmptrap.rules >>>>>")

    @Agent = "JUNIPER-L2CP-FEATURES-MIB"
    @Class = "40200"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### jnxLacpTimeOut

            ##########
            # $1 = jnxLacpInterfaceName 
            # $2 = jnxLacpifIndex 
            # $3 = jnxLacpAggregateInterfaceName 
            # $4 = jnxLacpAggregateifIndex 
            # $5 = jnxLacpAggPortActorOperState 
            ##########

            $jnxLacpInterfaceName = $1
            $jnxLacpifIndex = $2
            $jnxLacpAggregateInterfaceName = $3
            $jnxLacpAggregateifIndex = $4
            $jnxLacpAggPortActorOperState = $5

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-L2CP-FEATURES-MIB-jnxLacpTimeOut"

            @AlertGroup = "LACP Status"
            @AlertKey = "LACP Interface Name: " + $jnxLacpInterfaceName
            @Summary = "LACP Times Out" + " ( " + @AlertKey + " ) "

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($jnxLacpInterfaceName,$jnxLacpifIndex,$jnxLacpAggregateInterfaceName,$jnxLacpAggregateifIndex,$jnxLacpAggPortActorOperState)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "jnxLacpInterfaceName", $jnxLacpInterfaceName, "jnxLacpifIndex", $jnxLacpifIndex, "jnxLacpAggregateInterfaceName", $jnxLacpAggregateInterfaceName,
                 "jnxLacpAggregateifIndex", $jnxLacpAggregateifIndex, "jnxLacpAggPortActorOperState", $jnxLacpAggPortActorOperState)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, juniper-JUNIPER-L2CP-FEATURES-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, juniper-JUNIPER-L2CP-FEATURES-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-L2CP-FEATURES-MIB_Notifications.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-L2CP-FEATURES-MIB_Notifications.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... juniper-JUNIPER-L2CP-FEATURES-MIB.include.snmptrap.rules >>>>>")
