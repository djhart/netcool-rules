###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 8.0 - Updated release for JUNOS 9.4, JUNOSe 10.3 and BX-OS 4.1
#
#          - Repackaged for NIM-08
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  JUNIPER-SYRAH-MIB
#
###############################################################################

log(DEBUG, "<<<<< Entering... juniper-JUNIPER-SYRAH-MIB.adv.include.snmptrap.rules >>>>>")

switch($specific-trap)
{
    case "1": ### jnxZerotouchConfigSuccess

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxZerotouchConfigSuccess"
        $OS_OsiLayer = 0

    case "2": ### jnxZerotouchConfigFail

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxZerotouchConfigFail"
        $OS_OsiLayer = 0

    case "3": ### jnxSwUpgradeSuccess

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxSwUpgradeSuccess"
        $OS_OsiLayer = 0

    case "4": ### jnxSwUpgradeFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxSwUpgradeFailure"
        $OS_OsiLayer = 0

    case "5": ### jnxInterfaceAdminStatusChange

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxInterfaceAdminStatusChange"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ifEntry." + $ifIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "6": ### jnxInterfaceOperStatusChange

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxInterfaceOperStatusChange"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "ifEntry." + $ifIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "7": ### jnxChassisTemperatureRaised

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxChassisTemperatureRaised"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Temperature Instance : " + $jnxSyrahLastTempInstance
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "8": ### jnxChassisTemperatureCleared

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxChassisTemperatureCleared"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Temperature Instance : " + $jnxSyrahLastTempInstance
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "9": ### jnxChassisPowerOn

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxChassisPowerOn"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Power Instance : " + $jnxSyrahLastPowerInstance
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "10": ### jnxChassisPowerOff

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxChassisPowerOff"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Power Instance : " + $jnxSyrahLastPowerInstance
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "11": ### jnxRmonRiseThreshold

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxRmonRiseThreshold"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "JnxAlarmEntry." + $jnxAlarmIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "12": ### jnxRmonFallThreshold

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxRmonFallThreshold"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "JnxAlarmEntry." + $jnxAlarmIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "13": ### jnxChassisPowerInserted

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxChassisPowerInserted"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Power Instance : " + $jnxSyrahLastPowerInstance 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "14": ### jnxChassisPowerRemoved

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxChassisPowerRemoved"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Power Instance : " + $jnxSyrahLastPowerInstance
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "15": ### jnxMplsTunnelUp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxMplsTunnelUp"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "MplsTunnelEntry." + $mplsTunnelIndex + "." + $mplsTunnelInstance + "." + $mplsTunnelIngressLSRId + "." + $mplsTunnelEgressLSRId
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "16": ### jnxMplsTunnelDown

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxMplsTunnelDown"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "MplsTunnelEntry." + $mplsTunnelIndex + "." + $mplsTunnelInstance + "." + $mplsTunnelIngressLSRId + "." + $mplsTunnelEgressLSRId
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "17": ### jnxMplsTunnelRerouted

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxMplsTunnelRerouted"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "MplsTunnelEntry." + $mplsTunnelIndex + "." + $mplsTunnelInstance + "." + $mplsTunnelIngressLSRId + "." + $mplsTunnelEgressLSRId
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "18": ### jnxMplsTunnelReoptimized

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxMplsTunnelReoptimized"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "MplsTunnelEntry." + $mplsTunnelIndex + "." + $mplsTunnelInstance + "." + $mplsTunnelIngressLSRId + "." + $mplsTunnelEgressLSRId
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    default:
}

log(DEBUG, "<<<<< Leaving... juniper-JUNIPER-SYRAH-MIB.adv.include.snmptrap.rules >>>>>")
