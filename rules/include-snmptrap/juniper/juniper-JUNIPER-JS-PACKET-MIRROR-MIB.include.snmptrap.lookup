###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 9.0 - Updated release for JUNOS 10.4, JUNOSe 11.3
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  JUNIPER-JS-PACKET-MIRROR-MIB
#
###############################################################################

table JnxJsPacketMirrorConfigurationSource =
{

    {"0","Radius Login"}, ### radiusLogin
    {"1","Radius COA"}, ### radiusCoa
    {"2","CLI Trigger"}, ### cliTrigger
    {"3","CLI Static"}, ### cliStatic
    {"4","DTCP"} ### dtcp
}
default = "Unknown"


table JnxJsPacketMirrorTriggerType =
{

    {"0","Interface String"}, ### interfaceString
    {"1","IP Address"}, ### ipAddress
    {"2","NAS Port Id"}, ### nasPortId
    {"3","Username"}, ### username
    {"4","Calling Station Id"}, ### callingStationId
    {"5","ACCT Session Id"} ### acctSessionId
}
default = "Unknown"


table JnxJsPacketMirrorErrorCause =
{

    {"0","Generic Failure"}, ### genericFailure
    {"1","No Resources Available"}, ### noResourcesAvailable
    {"2","Memory Exhausted"}, ### memoryExhausted
    {"3","No Such Name"}, ### noSuchName
    {"4","Invalid Analyzer Address"}, ### invalidAnalyzerAddress
    {"5","No Such User Or Interface"}, ### noSuchUserOrInterface
    {"6","Feature Not Supported"}, ### featureNotSupported
    {"7","Missing Or Invalid Attribute"}, ### missingOrInvalidAttribute
    {"8","Router Mismatch"}, ### routerMismatch
    {"9","Name Length Exceeded"}, ### nameLengthExceeded
    {"10","DFCD NAK"} ### dfcdNak
}
default = "Unknown"


table JnxJsPacketMirrorApplicationName =
{

    {"0","Authd"} ### authd
}
default = "Unknown"


table JnxJsPacketMirrorDirection =
{

    {"0","Ingress"}, ### ingress
    {"1","Egress"}, ### egress
    {"2","Bidirection"}, ### bidirection
}
default = "Unknown"


table JnxJsPacketMirrorTerminationReason =
{

    {"0","Generic Failure"}, ### genericFailure
    {"1","User Request"}, ### userRequest
    {"2","Lost Carrier"}, ### lostCarrier
    {"3","Lost Service"}, ### lostService
    {"4","Idle Timeout"}, ### idleTimeout
    {"5","Session Timeout"}, ### sessionTimeout
    {"6","Admin Reset"}, ### adminReset
    {"7","Admin Reboot"}, ### adminReboot
    {"8","Port Error"}, ### portError
    {"9","NAS Error"}, ### nasError
    {"10","NAS Request0"}, ### nasRequest0
    {"11","NAS Reboot1"}, ### nasReboot1
    {"12","Port Unneeded"}, ### portUnneeded
    {"13","Port Preempted"}, ### portPreempted
    {"14","Port Suspended"}, ### portSuspended
    {"15","Service Unavailable"}, ### serviceUnavailable
    {"16","Callback"}, ### callback
    {"17","User Error"}, ### userError
    {"18","Host Request"}, ### hostRequest
    {"19","Supplicant Restart"}, ### supplicantRestart
    {"20","Reauthentication Failure"}, ### reauthenticationFailure
    {"21","Port Reinitialized"}, ### portReinitialized
    {"22","Port Administratively Disabled"}, ### portAdministrativelyDisabled
    {"23","Authentication Reject"}, ### authenticationReject
    {"24","Interface Deleted"}, ### interfaceDeleted
}
default = "Unknown"
