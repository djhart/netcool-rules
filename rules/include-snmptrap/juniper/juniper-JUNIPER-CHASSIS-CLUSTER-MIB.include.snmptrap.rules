###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 10.0 - Updated release for JUNOS 11.4
#
#          -  Add jnxJsChClusterIntfTrap trap
# 
# 9.0 - Updated release for JUNOS 10.4, JUNOSe 11.3
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  JUNIPER-CHASSIS-CLUSTER-MIB
#
###############################################################################

case ".1.3.6.1.4.1.2636.3.39.1.14.1": ### - Notifications from JUNIPER-CHASSIS-CLUSTER-MIB (201106280000Z)

    log(DEBUG, "<<<<< Entering... juniper-JUNIPER-CHASSIS-CLUSTER-MIB.include.snmptrap.rules >>>>>")

    @Agent = "juniper-JUNIPER-CHASSIS-CLUSTER-MIB"
    @Class = "40200"
    
    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {

        case "1": ### jnxJsChassisClusterSwitchover
        
            ##########
            # $1 = jnxJsChClusterSwitchoverInfoRedundancyGroup
            # $2 = jnxJsChClusterSwitchoverInfoClusterId
            # $3 = jnxJsChClusterSwitchoverInfoNodeId
            # $4 = jnxJsChClusterSwitchoverInfoPreviousState
            # $5 = jnxJsChClusterSwitchoverInfoCurrentState
            # $6 = jnxJsChClusterSwitchoverInfoReason
            ##########
            
            $jnxJsChClusterSwitchoverInfoRedundancyGroup = $1
            $jnxJsChClusterSwitchoverInfoClusterId = $2
            $jnxJsChClusterSwitchoverInfoNodeId = $3
            $jnxJsChClusterSwitchoverInfoPreviousState = $4
            $jnxJsChClusterSwitchoverInfoCurrentState = $5
            $jnxJsChClusterSwitchoverInfoReason = $6
            
            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-CHASSIS-CLUSTER-MIB-jnxJsChassisClusterSwitchover"

            @AlertGroup = "Chassis Cluster Status"
            @AlertKey = "Redundancy Group ID: " + $jnxJsChClusterSwitchoverInfoRedundancyGroup + ", Cluster ID: " + $jnxJsChClusterSwitchoverInfoClusterId + ", Node ID: " + $jnxJsChClusterSwitchoverInfoNodeId
            @Summary = "Chassis Cluster Switchover or Failover, Reason: " + $jnxJsChClusterSwitchoverInfoReason + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
             if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                 details($jnxJsChClusterSwitchoverInfoRedundancyGroup, $jnxJsChClusterSwitchoverInfoClusterId, $jnxJsChClusterSwitchoverInfoNodeId, $jnxJsChClusterSwitchoverInfoPreviousState, $jnxJsChClusterSwitchoverInfoCurrentState, $jnxJsChClusterSwitchoverInfoReason)
             }
             @ExtendedAttr = nvp_add(@ExtendedAttr, "jnxJsChClusterSwitchoverInfoRedundancyGroup", $jnxJsChClusterSwitchoverInfoRedundancyGroup, "jnxJsChClusterSwitchoverInfoClusterId", $jnxJsChClusterSwitchoverInfoClusterId, "jnxJsChClusterSwitchoverInfoNodeId", $jnxJsChClusterSwitchoverInfoNodeId,
                  "jnxJsChClusterSwitchoverInfoPreviousState", $jnxJsChClusterSwitchoverInfoPreviousState, "jnxJsChClusterSwitchoverInfoCurrentState", $jnxJsChClusterSwitchoverInfoCurrentState, "jnxJsChClusterSwitchoverInfoReason", $jnxJsChClusterSwitchoverInfoReason)
         
         
         case "2": ### jnxJsChClusterIntfTrap
        
            ##########
            # $1 = jnxJsChClusterSwitchoverInfoClusterId
            # $2 = jnxJsChClusterIntfName
            # $3 = jnxJsChClusterIntfState
            # $4 = jnxJsChClusterIntfSeverity
            # $5 = jnxJsChClusterIntfStateReason
            ##########
            
            $jnxJsChClusterSwitchoverInfoClusterId = $1
            $jnxJsChClusterIntfName = $2
            $jnxJsChClusterIntfState = $3
            $jnxJsChClusterIntfSeverity = $4
            $jnxJsChClusterIntfStateReason = $5
            
            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-CHASSIS-CLUSTER-MIB-jnxJsChClusterIntfTrap"

            @AlertGroup = "Chassis Cluster Interface Status"
            @AlertKey = "Cluster ID: " + $jnxJsChClusterSwitchoverInfoClusterId + ", Interface Name: " + $jnxJsChClusterIntfName
            @Summary = "Chassis Cluster HA Interface has Changed, Reason: " + $jnxJsChClusterIntfStateReason + " ( " + @AlertKey + " ) "
         
            if (regmatch($jnxJsChClusterIntfState,"[Uu][Pp]"))
            {
                $SEV_KEY = $OS_EventId + "_up"
                $DEFAULT_Severity = 1
                $DEFAULT_Type = 2
                $DEFAULT_ExpireTime = 0 
            }
            else
            {

            if (regmatch($jnxJsChClusterIntfSeverity,"[Mm][Aa][Jj][Oo][Rr]"))
            {
                $SEV_KEY = $OS_EventId + "_down_major"
                $DEFAULT_Severity = 4
                $DEFAULT_Type = 1
                $DEFAULT_ExpireTime = 0 
            }
            else
            {
                $SEV_KEY = $OS_EventId + "_down_minor"
                $DEFAULT_Severity = 3
                $DEFAULT_Type = 1
                $DEFAULT_ExpireTime = 0               
            
            }}
            update(@Severity)
            update(@Summary)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
             if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                 details($jnxJsChClusterSwitchoverInfoClusterId, $jnxJsChClusterIntfName, $jnxJsChClusterIntfState, $jnxJsChClusterIntfSeverity, $jnxJsChClusterIntfStateReason)
             }
             @ExtendedAttr = nvp_add(@ExtendedAttr, "jnxJsChClusterSwitchoverInfoClusterId", $jnxJsChClusterSwitchoverInfoClusterId, "jnxJsChClusterIntfName", $jnxJsChClusterIntfName, "jnxJsChClusterIntfState", $jnxJsChClusterIntfState,
                  "jnxJsChClusterIntfSeverity", $jnxJsChClusterIntfSeverity, "jnxJsChClusterIntfStateReason", $jnxJsChClusterIntfStateReason)
                 
     
        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, juniper-JUNIPER-CHASSIS-CLUSTER-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, juniper-JUNIPER-CHASSIS-CLUSTER-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-CHASSIS-CLUSTER-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-CHASSIS-CLUSTER-MIB.user.include.snmptrap.rules"


##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... juniper-JUNIPER-CHASSIS-CLUSTER-MIB.include.snmptrap.rules >>>>>")


