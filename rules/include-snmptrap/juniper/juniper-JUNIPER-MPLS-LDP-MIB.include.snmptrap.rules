###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 8.0 - Updated release for JUNOS 9.4, JUNOSe 10.3 and BX-OS 4.1
#
#          - Repackaged for NIM-08
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#          -  JUNIPER-MPLS-LDP-MIB
#
###############################################################################

case ".1.3.6.1.4.1.2636.3.36.2": ###  Juniper MPLS Label Distribution Protocol (LDP)
                                 ###  Notifications from JUNIPER-MPLS-LDP-MIB 

    log(DEBUG, "<<<<< Entering... juniper-JUNIPER-MPLS-LDP-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Juniper-MPLS-LDP"
    @Class = "40200"

    $OPTION_TypeFieldUsage = "3.6"

    include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-RemapVarbinds.include.snmptrap.rules"

    if(exists($snmpTrapEnterprise))
    {
        if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
            details($snmpTrapEnterprise,$mplsLspName,$mplsPathName)
        }
        @ExtendedAttr = nvp_add(@ExtendedAttr, "snmpTrapEnterprise", $snmpTrapEnterprise, "mplsLspName", $mplsLspName, "mplsPathName", $mplsPathName)
    }
    else
    {
        if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
            details($mplsLspName,$mplsPathName)
        }
        @ExtendedAttr = nvp_add(@ExtendedAttr, "mplsLspName", $mplsLspName, "mplsPathName", $mplsPathName)
    }
	
	
    switch($specific-trap)
    {
        case "1": ### jnxMplsLdpInitSesThresholdExceeded

            ##########
            # $1 = jnxMplsLdpEntityInitSesThreshold
            ##########

            $jnxMplsLdpEntityInitSesThreshold = $1
			
            $jnxMplsLdpEntityLdpId = extract($OID1, "\.([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)\.[0-9]+$")
            $jnxMplsLdpEntityIndex = extract($OID1, "\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.([0-9]+)$")
            $LsrIpAddr = extract($OID1, "\.([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)\.[0-9]+\.[0-9]+\.[0-9]+$")
            $LsrLabelSpace = extract($OID1, "\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.([0-9]+\.[0-9]+)\.[0-9]+$")
			
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($jnxMplsLdpEntityInitSesThreshold,$jnxMplsLdpEntityLdpId,$jnxMplsLdpEntityIndex,$LsrIpAddr,$LsrLabelSpace)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "jnxMplsLdpEntityInitSesThreshold", $jnxMplsLdpEntityInitSesThreshold, "jnxMplsLdpEntityLdpId", $jnxMplsLdpEntityLdpId, "jnxMplsLdpEntityIndex", $jnxMplsLdpEntityIndex,
                 "LsrIpAddr", $LsrIpAddr, "LsrLabelSpace", $LsrLabelSpace)

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-MPLS-LDP-MIB-jnxMplsLdpInitSesThresholdExceeded"

            @AlertGroup = "MPLS LDP Initialization Session Threshold"
            @AlertKey = "jnxMplsLdpEntityEntry." + $jnxMplsLdpEntityLdpId + "." + $jnxMplsLdpEntityIndex
            @Summary = "MPLS LDP Initialization Session Messages Sent Exceeds Threshold, " + $1 + "  ( LSR: " + $LsrIpAddr + ", Label Space: " + $LsrLabelSpace + " )"
            update(@Summary)  

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        case "2": ### jnxMplsLdpPathVectorLimitMismatch

            ##########
            # $1 = jnxMplsLdpEntityPathVectorLimit
            # $2 = jnxMplsLdpPeerPathVectorLimit
            ##########

            $jnxMplsLdpEntityPathVectorLimit = $1
            $jnxMplsLdpPeerPathVectorLimit = $2
			
            $jnxMplsLdpEntityLdpId = extract($OID1, "\.([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)\.[0-9]+$")
            $jnxMplsLdpEntityIndex = extract($OID1, "\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.([0-9]+)$")
            $LsrIpAddr = extract($OID1, "\.([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)\.[0-9]+\.[0-9]+\.[0-9]+$")
            $LsrLabelSpace = extract($OID1, "\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.([0-9]+\.[0-9]+)\.[0-9]+$")
            $jnxMplsLdpPeerLdpId = extract($OID2, "\.([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)$")
            $PeerLsrIpAddr = extract($OID2, "\.([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)\.[0-9]+\.[0-9]+$")
            $PeerLsrLabelSpace = extract($OID2, "\.([0-9]+\.[0-9]+)$")
			
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($jnxMplsLdpEntityPathVectorLimit,$jnxMplsLdpEntityLdpId,$jnxMplsLdpEntityIndex,$LsrIpAddr,$LsrLabelSpace, $jnxMplsLdpPeerPathVectorLimit,$jnxMplsLdpPeerLdpId,$PeerLsrIpAddr,$PeerLsrLabelSpace)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "jnxMplsLdpEntityPathVectorLimit", $jnxMplsLdpEntityPathVectorLimit, "jnxMplsLdpEntityLdpId", $jnxMplsLdpEntityLdpId, "jnxMplsLdpEntityIndex", $jnxMplsLdpEntityIndex,
                 "LsrIpAddr", $LsrIpAddr, "LsrLabelSpace", $LsrLabelSpace, "jnxMplsLdpPeerPathVectorLimit", $jnxMplsLdpPeerPathVectorLimit,
                 "jnxMplsLdpPeerLdpId", $jnxMplsLdpPeerLdpId, "PeerLsrIpAddr", $PeerLsrIpAddr, "PeerLsrLabelSpace", $PeerLsrLabelSpace)

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-MPLS-LDP-MIB-jnxMplsLdpPathVectorLimitMismatch"

            @AlertGroup = "MPLS LDP Path Vector Limit Mismatch"
            @AlertKey = "jnxMplsLdpSessionEntry." + $jnxMplsLdpEntityLdpId + "." + $jnxMplsLdpEntityIndex + "." + $jnxMplsLdpPeerLdpId 
            @Summary = "MPLS LDP Path Vector Limit Mismatch  ( LSR: " + $LsrIpAddr + ", Label Space: " + $LsrLabelSpace + ", Peer LSR: " + $PeerLsrIpAddr + ", Peer Label Space: " + $PeerLsrLabelSpace + " )"
			
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

        case "3": ### jnxMplsLdpSessionUp

            ##########
            # $1 = jnxMplsLdpSesState
            # $2 = jnxMplsLdpSesDiscontinuityTime
            # $3 = jnxMplsLdpSesStatsUnkMesTypeErrors
            # $4 = jnxMplsLdpSesStatsUnkTlvErrors
            ##########

            $jnxMplsLdpSesState = lookup($1,jnxMplsLdpSesState)

            $jnxMplsLdpEntityLdpId = extract($OID1, "\.([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$")
            $jnxMplsLdpEntityIndex = extract($OID1, "\.([0-9]+)\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$")
            $LsrIpAddr = extract($OID1, "\.([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$")
            $LsrLabelSpace = extract($OID1, "\.([0-9]+\.[0-9]+)\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$")
            $jnxMplsLdpPeerLdpId = extract($OID1, "\.([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)$")
            $PeerLsrIpAddr = extract($OID1, "\.([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)\.[0-9]+\.[0-9]+$")
            $PeerLsrLabelSpace = extract($OID1, "\.([0-9]+\.[0-9]+)$")
			
            $jnxMplsLdpSesDiscontinuityTime = $2
            $jnxMplsLdpSesStatsUnkMesTypeErrors = $3
            $jnxMplsLdpSesStatsUnkTlvErrors = $4

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-MPLS-LDP-MIB-jnxMplsLdpSessionUp"

            @AlertGroup = "MPLS LDP Session Status"
            @AlertKey = "jnxMplsLdpSessionEntry."  + $jnxMplsLdpEntityLdpId + "." + $jnxMplsLdpEntityIndex + "." + $jnxMplsLdpPeerLdpId

            switch($1) ## jnxMplsLdpSesState
            {
                case "1": ### nonexistent
                    $SEV_KEY = $OS_EventId + "_nonexistent"
                    @Summary = "MPLS LDP Session Non-Existent"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "2": ### initialized
                    $SEV_KEY = $OS_EventId + "_initialized"
                    @Summary = "MPLS LDP Session Initialized"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 12
                    $DEFAULT_ExpireTime = 0
                case "3": ### openrec
                    $SEV_KEY = $OS_EventId + "_openrec"
                    @Summary = "MPLS LDP Session Open Received"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 12
                    $DEFAULT_ExpireTime = 0
                case "4": ### opensent
                    $SEV_KEY = $OS_EventId + "_opensent"
                    @Summary = "MPLS LDP Session Open Sent"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 12
                    $DEFAULT_ExpireTime = 0
                case "5": ### operational
                    $SEV_KEY = $OS_EventId + "_operational"
                    @Summary = "MPLS LDP Session Operational"

                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = "MPLS LDP Session Status Unknown"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }

            @Summary = @Summary + "  ( LSR: " + $LsrIpAddr + ", Label Space: " + $LsrLabelSpace + ", Peer LSR: " + $PeerLsrIpAddr + ", Peer Label Space: " + $PeerLsrLabelSpace + " )"
			
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $1

            $jnxMplsLdpSesState = $jnxMplsLdpSesState + " ( " + $1 + " )"
			
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($jnxMplsLdpSesState,$jnxMplsLdpEntityLdpId,$jnxMplsLdpEntityIndex,$LsrIpAddr,$LsrLabelSpace,$jnxMplsLdpPeerLdpId,$PeerLsrIpAddr,$PeerLsrLabelSpace,$jnxMplsLdpSesDiscontinuityTime,$jnxMplsLdpSesStatsUnkMesTypeErrors,$jnxMplsLdpSesStatsUnkTlvErrors)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "jnxMplsLdpSesState", $jnxMplsLdpSesState, "jnxMplsLdpEntityLdpId", $jnxMplsLdpEntityLdpId, "jnxMplsLdpEntityIndex", $jnxMplsLdpEntityIndex,
                 "LsrIpAddr", $LsrIpAddr, "LsrLabelSpace", $LsrLabelSpace, "jnxMplsLdpPeerLdpId", $jnxMplsLdpPeerLdpId,
                 "PeerLsrIpAddr", $PeerLsrIpAddr, "PeerLsrLabelSpace", $PeerLsrLabelSpace, "jnxMplsLdpSesDiscontinuityTime", $jnxMplsLdpSesDiscontinuityTime,
                 "jnxMplsLdpSesStatsUnkMesTypeErrors", $jnxMplsLdpSesStatsUnkMesTypeErrors, "jnxMplsLdpSesStatsUnkTlvErrors", $jnxMplsLdpSesStatsUnkTlvErrors)
		
        case "4": ### jnxMplsLdpSessionDown

            ##########
            # $1 = jnxMplsLdpSesState
            # $2 = jnxMplsLdpSesDiscontinuityTime
            # $3 = jnxMplsLdpSesStatsUnkMesTypeErrors
            # $4 = jnxMplsLdpSesStatsUnkTlvErrors
            ##########

            $jnxMplsLdpSesState = lookup($1,jnxMplsLdpSesState)
			
            $jnxMplsLdpEntityLdpId = extract($OID1, "\.([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$")
            $jnxMplsLdpEntityIndex = extract($OID1, "\.([0-9]+)\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$")
            $LsrIpAddr = extract($OID1, "\.([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$")
            $LsrLabelSpace = extract($OID1, "\.([0-9]+\.[0-9]+)\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$")
            $jnxMplsLdpPeerLdpId = extract($OID1, "\.([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)$")
            $PeerLsrIpAddr = extract($OID1, "\.([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)\.[0-9]+\.[0-9]+$")
            $PeerLsrLabelSpace = extract($OID1, "\.([0-9]+\.[0-9]+)$")
			
            $jnxMplsLdpSesDiscontinuityTime = $2
            $jnxMplsLdpSesStatsUnkMesTypeErrors = $3
            $jnxMplsLdpSesStatsUnkTlvErrors = $4

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-MPLS-LDP-MIB-jnxMplsLdpSessionDown"

            @AlertGroup = "MPLS LDP Session Status"
            @AlertKey = "jnxMplsLdpSessionEntry." + $jnxMplsLdpEntityLdpId + "." + $jnxMplsLdpEntityIndex + "." + $jnxMplsLdpPeerLdpId 

            switch($1) ## jnxMplsLdpSesState
            {
                case "1": ### nonexistent
                    $SEV_KEY = $OS_EventId + "_nonexistent"
                    @Summary = "MPLS LDP Session Non-Existent"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
                case "2": ### initialized
                    $SEV_KEY = $OS_EventId + "_initialized"
                    @Summary = "MPLS LDP Session Initialized"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 12
                    $DEFAULT_ExpireTime = 0
                case "3": ### openrec
                    $SEV_KEY = $OS_EventId + "_openrec"
                    @Summary = "MPLS LDP Session Open Received"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 12
                    $DEFAULT_ExpireTime = 0
                case "4": ### opensent
                    $SEV_KEY = $OS_EventId + "_opensent"
                    @Summary = "MPLS LDP Session Open Sent"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 12
                    $DEFAULT_ExpireTime = 0
                case "5": ### operational
                    $SEV_KEY = $OS_EventId + "_operational"
                    @Summary = "MPLS LDP Session Operational"

                    $DEFAULT_Severity = 1
                    $DEFAULT_Type = 2
                    $DEFAULT_ExpireTime = 0
                default:
                    $SEV_KEY = $OS_EventId + "_unknown"
                    @Summary = "MPLS LDP Session Status Unknown"

                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }
			
            @Summary = @Summary + "  ( LSR: " + $LsrIpAddr + ", Label Space: " + $LsrLabelSpace + ", Peer LSR: " + $PeerLsrIpAddr + ", Peer Label Space: " + $PeerLsrLabelSpace + " )"


            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap + " " + $1
			
            $jnxMplsLdpSesState = $jnxMplsLdpSesState + " ( " + $1 + " )"
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($jnxMplsLdpSesState,$jnxMplsLdpEntityLdpId,$jnxMplsLdpEntityIndex,$LsrIpAddr,$LsrLabelSpace,$jnxMplsLdpPeerLdpId,$PeerLsrIpAddr,$PeerLsrLabelSpace,$jnxMplsLdpSesDiscontinuityTime,$jnxMplsLdpSesStatsUnkMesTypeErrors,$jnxMplsLdpSesStatsUnkTlvErrors)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "jnxMplsLdpSesState", $jnxMplsLdpSesState, "jnxMplsLdpEntityLdpId", $jnxMplsLdpEntityLdpId, "jnxMplsLdpEntityIndex", $jnxMplsLdpEntityIndex,
                 "LsrIpAddr", $LsrIpAddr, "LsrLabelSpace", $LsrLabelSpace, "jnxMplsLdpPeerLdpId", $jnxMplsLdpPeerLdpId,
                 "PeerLsrIpAddr", $PeerLsrIpAddr, "PeerLsrLabelSpace", $PeerLsrLabelSpace, "jnxMplsLdpSesDiscontinuityTime", $jnxMplsLdpSesDiscontinuityTime,
                 "jnxMplsLdpSesStatsUnkMesTypeErrors", $jnxMplsLdpSesStatsUnkMesTypeErrors, "jnxMplsLdpSesStatsUnkTlvErrors", $jnxMplsLdpSesStatsUnkTlvErrors)

	default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, juniper-JUNIPER-MPLS-LDP-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, juniper-JUNIPER-MPLS-LDP-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-MPLS-LDP-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-MPLS-LDP-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... juniper-JUNIPER-MPLS-LDP-MIB.include.snmptrap.rules >>>>>")
