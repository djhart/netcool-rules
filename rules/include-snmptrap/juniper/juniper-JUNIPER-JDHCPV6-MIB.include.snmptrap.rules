###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  JUNIPER-JDHCPV6-MIB
#
###############################################################################

case ".1.3.6.1.4.1.2636.3.62.62.2.3": ### - Notifications from JUNIPER-JDHCPV6-MIB (201103150000Z)

    log(DEBUG, "<<<<< Entering... juniper-JUNIPER-JDHCPV6-MIB.include.snmptrap.rules >>>>>")

    @Agent = "juniper-JUNIPER-JDHCPV6-MIB"
    @Class = "40200"
    
    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
     
        case "1": ### jnxJdhcpv6LocalServerInterfaceLimitExceeded
        
            ##########
            # $1 = jnxJdhcpv6RouterName
            # $2 = jnxJdhcpv6LocalServerInterfaceName
            # $3 = jnxJdhcpv6LocalServerInterfaceLimit
            ##########
            
            $jnxJdhcpv6RouterName = $1
            $jnxJdhcpv6LocalServerInterfaceName = $2
            $jnxJdhcpv6LocalServerInterfaceLimit = $3
            
            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-JDHCPV6-MIB-jnxJdhcpv6LocalServerInterfaceLimitExceeded"

            @AlertGroup = "DHCP V6 Local Server Interface Status"
            @AlertKey = "Interface Name: " + $jnxJdhcpv6LocalServerInterfaceName
            @Summary = "The Number Of Clients On An Interface Has Exceeded The Limit" + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0               
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
             if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                 details($jnxJdhcpv6RouterName, $jnxJdhcpv6LocalServerInterfaceName, $jnxJdhcpv6LocalServerInterfaceLimit)
             }
             @ExtendedAttr = nvp_add(@ExtendedAttr, "jnxJdhcpv6RouterName", $jnxJdhcpv6RouterName, "jnxJdhcpv6LocalServerInterfaceName", $jnxJdhcpv6LocalServerInterfaceName, "jnxJdhcpv6LocalServerInterfaceLimit", $jnxJdhcpv6LocalServerInterfaceLimit)
         
        case "2": ### jnxJdhcpv6LocalServerInterfaceLimitAbated
        
            ##########
            # $1 = jnxJdhcpv6RouterName
            # $2 = jnxJdhcpv6LocalServerInterfaceName
            # $3 = jnxJdhcpv6LocalServerInterfaceLimit
            ##########
            
            $jnxJdhcpv6RouterName = $1
            $jnxJdhcpv6LocalServerInterfaceName = $2
            $jnxJdhcpv6LocalServerInterfaceLimit = $3
            
            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-JDHCPV6-MIB-jnxJdhcpv6LocalServerInterfaceLimitAbated"

            @AlertGroup = "DHCP V6 Local Server Interface Status"
            @AlertKey = "Interface Name: " + $jnxJdhcpv6LocalServerInterfaceName
            @Summary = "The Number Of Clients On An Interface Has Fallen Below The Allowed Limit" + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0               
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
             if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                 details($jnxJdhcpv6RouterName, $jnxJdhcpv6LocalServerInterfaceName, $jnxJdhcpv6LocalServerInterfaceLimit)
             }
             @ExtendedAttr = nvp_add(@ExtendedAttr, "jnxJdhcpv6RouterName", $jnxJdhcpv6RouterName, "jnxJdhcpv6LocalServerInterfaceName", $jnxJdhcpv6LocalServerInterfaceName, "jnxJdhcpv6LocalServerInterfaceLimit", $jnxJdhcpv6LocalServerInterfaceLimit)
         
        case "4": ### jnxJdhcpv6LocalServerHealth
        
            ##########
            # $1 = jnxJdhcpv6RouterName
            # $2 = jnxJdhcpv6LocalServerEventSeverity
            # $3 = jnxJdhcpv6LocalServerEventString
            ##########
            
            $jnxJdhcpv6RouterName = $1
            $jnxJdhcpv6LocalServerEventSeverity = lookup($2, JnxJdhcpv6LocalServerEventSeverity)
            $jnxJdhcpv6LocalServerEventString = $3
            
            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-JDHCPV6-MIB-jnxJdhcpv6LocalServerHealth"

            @AlertGroup = "DHCP V6 Local Server Health Status"
            @AlertKey = "Router Name: " + $jnxJdhcpv6RouterName
            @Summary = "Local Server V6 Application Health Event Occured" + " ( " + @AlertKey + " ) "
            
            switch($2)
            {
                case "0":### debug
                    $SEV_KEY = $OS_EventId + "_debug"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "1":### warning
                    $SEV_KEY = $OS_EventId + "_warning"
                    $DEFAULT_Severity = 3
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                case "2":### critical
                    $SEV_KEY = $OS_EventId + "_critical"
                    $DEFAULT_Severity = 4
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0               
                default: 
                    $SEV_KEY = $OS_EventId + "_unknown"
                    $DEFAULT_Severity = 2
                    $DEFAULT_Type = 1
                    $DEFAULT_ExpireTime = 0
            }
                     
            update(@Severity)
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
            $jnxJdhcpv6LocalServerEventSeverity = $jnxJdhcpv6LocalServerEventSeverity + " ( " + $2 + " )"
             if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                 details($jnxJdhcpv6RouterName, $jnxJdhcpv6LocalServerEventSeverity, $jnxJdhcpv6LocalServerEventString)
             }
             @ExtendedAttr = nvp_add(@ExtendedAttr, "jnxJdhcpv6RouterName", $jnxJdhcpv6RouterName, "jnxJdhcpv6LocalServerEventSeverity", $jnxJdhcpv6LocalServerEventSeverity, "jnxJdhcpv6LocalServerEventString", $jnxJdhcpv6LocalServerEventString)
         
        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, juniper-JUNIPER-JDHCPV6-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, juniper-JUNIPER-JDHCPV6-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-JDHCPV6-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-JDHCPV6-MIB.user.include.snmptrap.rules"


##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... juniper-JUNIPER-JDHCPV6-MIB.include.snmptrap.rules >>>>>")


