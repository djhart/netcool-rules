###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 9.0 - Updated release for JUNOS 10.4, JUNOSe 11.3
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  JUNIPER-COS-MIB
#
###############################################################################

case ".1.3.6.1.4.1.2636.4.17": ###  - Notifications from JUNIPER-COS-MIB (200912040000Z)

    log(DEBUG, "<<<<< Entering... juniper-JUNIPER-COS-MIB.include.snmptrap.rules >>>>>")

    @Agent = "JUNIPER-COS-MIB"
    @Class = "40200"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### jnxCosOutOfDedicatedQueues

            ##########
            # $1 = jnxCosInterfaceName 
            ##########

            $jnxCosInterfaceName = $1

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-COS-MIB-jnxCosOutOfDedicatedQueues"

            @AlertGroup = "Class Of Service Queues Status"
            @AlertKey = "Class Of Service Interface: " + $jnxCosInterfaceName
            @Summary = "No More Dedicated CoS Queues Available" + " ( " + @AlertKey + " ) " 

            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($jnxCosInterfaceName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "jnxCosInterfaceName", $jnxCosInterfaceName)

        case "2": ### jnxCosAlmostOutOfDedicatedQueues

            ##########
            # $1 = jnxCosInterfaceName 
            ##########

            $jnxCosInterfaceName = $1

            $OS_EventId = "SNMPTRAP-juniper-JUNIPER-COS-MIB-jnxCosAlmostOutOfDedicatedQueues"

            @AlertGroup = "Class Of Service Queues Status"
            @AlertKey = "Class Of Service Interface: " + $jnxCosInterfaceName
            @Summary = "Only 10% of Dedicated CoS Queues Are Available" + " ( " + @AlertKey + " ) " 

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($jnxCosInterfaceName)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "jnxCosInterfaceName", $jnxCosInterfaceName)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_juniper, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, juniper-JUNIPER-COS-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, juniper-JUNIPER-COS-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-COS-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/juniper/juniper-JUNIPER-COS-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... juniper-JUNIPER-COS-MIB.include.snmptrap.rules >>>>>")
