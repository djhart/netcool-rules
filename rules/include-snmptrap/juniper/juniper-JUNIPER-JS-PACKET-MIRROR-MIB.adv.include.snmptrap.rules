###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
#
# 9.0 - Updated release for JUNOS 10.4, JUNOSe 11.3
#
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  JUNIPER-JS-PACKET-MIRROR-MIB
#
###############################################################################

log(DEBUG, "<<<<< Entering... juniper-JUNIPER-JS-PACKET-MIRROR-MIB.adv.include.snmptrap.rules >>>>>")

switch($specific-trap)
{
    case "1": ### jnxJsPacketMirrorMirroringFailure

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxJsPacketMirrorMirroringFailure"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Mirror ID: " + $jnxJsPacketMirrorIdentifier + ", Route ID: " + $jnxJsPacketMirrorRouterId
        $OS_LocalRootObj = "Mirror ID: " + $jnxJsPacketMirrorIdentifier
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "2": ### jnxJsPacketMirrorLiSubscriberLoggedIn

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxJsPacketMirrorLiSubscriberLoggedIn"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Mirror ID: " + $jnxJsPacketMirrorIdentifier + ", Route ID: " + $jnxJsPacketMirrorRouterId + ", Target IP Addr: " + $jnxJsPacketMirrorTargetIpAddress + ", Analyzer IP Addr: " + $jnxJsPacketMirrorAnalyzerAddress 
        $OS_LocalSecObj = "Mirror ID: " + $jnxJsPacketMirrorIdentifier + ", Route ID: " + $jnxJsPacketMirrorRouterId
        $OS_LocalRootObj = "Mirror ID: " + $jnxJsPacketMirrorIdentifier
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 3
        $VAR_RelateLSO2LPO = 3

    case "3": ### jnxJsPacketMirrorLiSubscriberLogInFailed

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxJsPacketMirrorLiSubscriberLogInFailed"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Mirror ID: " + $jnxJsPacketMirrorIdentifier + ", Route ID: " + $jnxJsPacketMirrorRouterId + ", Target IP Addr: " + $jnxJsPacketMirrorTargetIpAddress + ", Analyzer IP Addr: " + $jnxJsPacketMirrorAnalyzerAddress 
        $OS_LocalSecObj = "Mirror ID: " + $jnxJsPacketMirrorIdentifier + ", Route ID: " + $jnxJsPacketMirrorRouterId
        $OS_LocalRootObj = "Mirror ID: " + $jnxJsPacketMirrorIdentifier
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 3
        $VAR_RelateLSO2LPO = 3

    case "4": ### jnxJsPacketMirrorLiSubscriberLoggedOut

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxJsPacketMirrorLiSubscriberLoggedOut"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Mirror ID: " + $jnxJsPacketMirrorIdentifier + ", Route ID: " + $jnxJsPacketMirrorRouterId + ", Target IP Addr: " + $jnxJsPacketMirrorTargetIpAddress + ", Analyzer IP Addr: " + $jnxJsPacketMirrorAnalyzerAddress 
        $OS_LocalSecObj = "Mirror ID: " + $jnxJsPacketMirrorIdentifier + ", Route ID: " + $jnxJsPacketMirrorRouterId
        $OS_LocalRootObj = "Mirror ID: " + $jnxJsPacketMirrorIdentifier
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 3
        $VAR_RelateLSO2LPO = 3

    case "5": ### jnxJsPacketMirrorLiServiceActivated

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxJsPacketMirrorLiServiceActivated"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Mirror ID: " + $jnxJsPacketMirrorIdentifier + ", Route ID: " + $jnxJsPacketMirrorRouterId + ", Target IP Addr: " + $jnxJsPacketMirrorTargetIpAddress + ", Analyzer IP Addr: " + $jnxJsPacketMirrorAnalyzerAddress 
        $OS_LocalSecObj = "Mirror ID: " + $jnxJsPacketMirrorIdentifier + ", Route ID: " + $jnxJsPacketMirrorRouterId
        $OS_LocalRootObj = "Mirror ID: " + $jnxJsPacketMirrorIdentifier
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 3
        $VAR_RelateLSO2LPO = 3

    case "6": ### jnxJsPacketMirrorLiServiceActivationFailed

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxJsPacketMirrorLiServiceActivationFailed"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Mirror ID: " + $jnxJsPacketMirrorIdentifier + ", Route ID: " + $jnxJsPacketMirrorRouterId + ", Target IP Addr: " + $jnxJsPacketMirrorTargetIpAddress + ", Analyzer IP Addr: " + $jnxJsPacketMirrorAnalyzerAddress 
        $OS_LocalSecObj = "Mirror ID: " + $jnxJsPacketMirrorIdentifier + ", Route ID: " + $jnxJsPacketMirrorRouterId
        $OS_LocalRootObj = "Mirror ID: " + $jnxJsPacketMirrorIdentifier
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 3
        $VAR_RelateLSO2LPO = 3

    case "7": ### jnxJsPacketMirrorLiServiceDeactivated

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "jnxJsPacketMirrorLiServiceDeactivated"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "Mirror ID: " + $jnxJsPacketMirrorIdentifier + ", Route ID: " + $jnxJsPacketMirrorRouterId + ", Target IP Addr: " + $jnxJsPacketMirrorTargetIpAddress + ", Analyzer IP Addr: " + $jnxJsPacketMirrorAnalyzerAddress 
        $OS_LocalSecObj = "Mirror ID: " + $jnxJsPacketMirrorIdentifier + ", Route ID: " + $jnxJsPacketMirrorRouterId
        $OS_LocalRootObj = "Mirror ID: " + $jnxJsPacketMirrorIdentifier
        $VAR_RelateLRO2LPO = 3
        $VAR_RelateLRO2LSO = 3
        $VAR_RelateLSO2LPO = 3

    default:
}

log(DEBUG, "<<<<< Leaving... juniper-JUNIPER-JS-PACKET-MIRROR-MIB.adv.include.snmptrap.rules >>>>>")
