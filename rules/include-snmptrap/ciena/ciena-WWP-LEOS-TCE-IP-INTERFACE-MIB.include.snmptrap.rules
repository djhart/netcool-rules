###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  WWP-LEOS-TCE-IP-INTERFACE-MIB
#
###############################################################################

case ".1.3.6.1.4.1.6141.2.61.10.2": ### - Notifications from WWP-LEOS-TCE-IP-INTERFACE-MIB (200104031700Z)

	log(DEBUG, "<<<<< Entering... ciena-WWP-LEOS-TCE-IP-INTERFACE-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Ciena-WWP-LEOS-TCE-IP-INTERFACE-MIB"
    @Class = "40506"
    
    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
     
        case "1": ### wwpLeosTceIpMgmtInterfaceAddrChgNotification
        
            ##########
            # $1 = wwpLeosTceIpMgmtInterfaceName
            # $2 = wwpLeosTceIpMgmtInterfaceOperIpAddr
            # $3 = wwpLeosTceIpMgmtInterfaceOperSubnet
            ##########
            
            $wwpLeosTceIpMgmtInterfaceName = $1
            $wwpLeosTceIpMgmtInterfaceOperIpAddr = $2
            $wwpLeosTceIpMgmtInterfaceOperSubnet = $3
            
            $wwpLeosTceIpMgmtInterfaceIndex = extract($OID1, "\.([0-9]+)$")
            
            $OS_EventId = "SNMPTRAP-ciena-WWP-LEOS-TCE-IP-INTERFACE-MIB-wwpLeosTceIpMgmtInterfaceAddrChgNotification"

            @AlertGroup = "WWP Leos TCE Interface IP Address Status"
            @AlertKey = "wwpLeosTceIpMgmtInterfaceEntry." + $wwpLeosTceIpMgmtInterfaceIndex
            @Summary = "WWP Leos TCE Interface IP Address Has Changed, Interface Name : " + $wwpLeosTceIpMgmtInterfaceName + " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800               
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
 			if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
     			details($wwpLeosTceIpMgmtInterfaceName, $wwpLeosTceIpMgmtInterfaceOperIpAddr, $wwpLeosTceIpMgmtInterfaceOperSubnet, $wwpLeosTceIpMgmtInterfaceIndex)
 			}
 			@ExtendedAttr = nvp_add(@ExtendedAttr, "wwpLeosTceIpMgmtInterfaceName", $wwpLeosTceIpMgmtInterfaceName, "wwpLeosTceIpMgmtInterfaceOperIpAddr", $wwpLeosTceIpMgmtInterfaceOperIpAddr, "wwpLeosTceIpMgmtInterfaceOperSubnet", $wwpLeosTceIpMgmtInterfaceOperSubnet,
 			     "wwpLeosTceIpMgmtInterfaceIndex", $wwpLeosTceIpMgmtInterfaceIndex)
         
        case "2": ### wwpLeosTceIpMgmtInterfaceGatewayChgNotification
        
            ##########
            # $1 = wwpLeosTceIpGatewayAddr
            ##########
            
            $wwpLeosTceIpGatewayAddr = $1
            
            $wwpLeosTceIpGatewayType = extract($OID1, "\.([0-9]+)$")
            
            $OS_EventId = "SNMPTRAP-ciena-WWP-LEOS-TCE-IP-INTERFACE-MIB-wwpLeosTceIpMgmtInterfaceGatewayChgNotification"

            @AlertGroup = "WWP Leos TCE Gateway IP Address Status"
            @AlertKey = "wwpLeosTceIpGatewayEntry." + $wwpLeosTceIpGatewayType
            @Summary = "WWP Leos TCE Gateway IP Address Has Changed, Gateway Type : " + $wwpLeosTceIpGatewayType +  " ( " + @AlertKey + " ) "
            
            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800               
            
            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            
 			if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
     			details($wwpLeosTceIpGatewayAddr, $wwpLeosTceIpGatewayType)
 			}
 			@ExtendedAttr = nvp_add(@ExtendedAttr, "wwpLeosTceIpGatewayAddr", $wwpLeosTceIpGatewayAddr, "wwpLeosTceIpGatewayType", $wwpLeosTceIpGatewayType)
         
        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, ciena-WWP-LEOS-TCE-IP-INTERFACE-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, ciena-WWP-LEOS-TCE-IP-INTERFACE-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-WWP-LEOS-TCE-IP-INTERFACE-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-WWP-LEOS-TCE-IP-INTERFACE-MIB.user.include.snmptrap.rules"


##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... ciena-WWP-LEOS-TCE-IP-INTERFACE-MIB.include.snmptrap.rules >>>>>")


