###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 2.0 - Updated to support notifications from WWP-LEOS-CFM-MIB (201101311300Z)
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  WWP-LEOS-CFM-MIB
#
###############################################################################

case ".1.3.6.1.4.1.6141.2.60.35.2": ###  - Notifications from WWP-LEOS-CFM-MIB (201101311300Z)

    log(DEBUG, "<<<<< Entering... ciena-WWP-LEOS-CFM-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Ciena-WWP-LEOS-CFM-MIB"
    @Class = "40506"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### wwpLeosCfmFaultTrap

            ##########
            # $1 = wwpLeosCfmServiceName 
            # $2 = wwpLeosCfmServiceVlan 
            # $3 = wwpLeosCfmServiceMdLevel 
            # $4 = wwpLeosCfmServiceFaultTime 
            # $5 = wwpLeosCfmServiceFaultType 
            # $6 = wwpLeosCfmServiceFaultDesc 
            # $7 = wwpLeosCfmServiceFaultMep 
            ##########

            $wwpLeosCfmServiceName = $1
            $wwpLeosCfmServiceVlan = $2
            $wwpLeosCfmServiceMdLevel = $3
            $wwpLeosCfmServiceFaultTime = $4
            $wwpLeosCfmServiceFaultType = lookup($5, wwpLeosCfmServiceFaultType)
            $wwpLeosCfmServiceFaultDesc = $6
            $wwpLeosCfmServiceFaultMep = $7

            $wwpLeosCfmServiceIndex = extract($OID1, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-ciena-WWP-LEOS-CFM-MIB-wwpLeosCfmFaultTrap"

            @AlertGroup = "WWP Leos Cfm Fault Trap"
            @AlertKey = "wwpLeosCfmServiceEntry." + $wwpLeosCfmServiceIndex
            @Summary = "WWP Leos Cfm Fault Is Detected, Type: " + $wwpLeosCfmServiceFaultType + " ( " + @AlertKey + " )"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            $wwpLeosCfmServiceFaultType = $wwpLeosCfmServiceFaultType + " ( " + $5 + " )"

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpLeosCfmServiceName,$wwpLeosCfmServiceVlan,$wwpLeosCfmServiceMdLevel,$wwpLeosCfmServiceFaultTime,$wwpLeosCfmServiceFaultType,$wwpLeosCfmServiceFaultDesc,$wwpLeosCfmServiceFaultMep,$wwpLeosCfmServiceIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpLeosCfmServiceName", $wwpLeosCfmServiceName, "wwpLeosCfmServiceVlan", $wwpLeosCfmServiceVlan, "wwpLeosCfmServiceMdLevel", $wwpLeosCfmServiceMdLevel,
                 "wwpLeosCfmServiceFaultTime", $wwpLeosCfmServiceFaultTime, "wwpLeosCfmServiceFaultType", $wwpLeosCfmServiceFaultType, "wwpLeosCfmServiceFaultDesc", $wwpLeosCfmServiceFaultDesc,
                 "wwpLeosCfmServiceFaultMep", $wwpLeosCfmServiceFaultMep, "wwpLeosCfmServiceIndex", $wwpLeosCfmServiceIndex)


		case "2": ### wwpLeosCfmPbtFaultTrap - deprecated
	   
			discard

        case "3": ### wwpLeosCfmDelayFaultTrap

            ##########
            # $1 = wwpLeosCfmServiceName 
            # $2 = wwpLeosCfmServiceVlan 
            # $3 = wwpLeosCfmRemoteMEPDelay 
            # $4 = wwpLeosCfmDelayMsgDelayThreshold 
            ##########

            $wwpLeosCfmServiceName = $1
            $wwpLeosCfmServiceVlan = $2
            $wwpLeosCfmRemoteMEPDelay = $3
            $wwpLeosCfmDelayMsgDelayThreshold = $4

            $wwpLeosCfmServiceIndex = extract($OID1, "\.([0-9]+)$")
            $wwpLeosCfmRemoteMEPID = extract($OID3, "\.([0-9]+)$")
            $wwpLeosCfmDelayMsgPortId= extract($OID4, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-ciena-WWP-LEOS-CFM-MIB-wwpLeosCfmDelayFaultTrap"

            @AlertGroup = "WWP Leos Cfm Delay Fault Trap"

            @AlertKey = "wwpLeosCfmRemoteMEPEntry." + $wwpLeosCfmServiceIndex + "." + $wwpLeosCfmRemoteMEPID + 
                        ", wwpLeosCfmDelayMsgEntry." + $wwpLeosCfmServiceIndex + "." + $wwpLeosCfmDelayMsgPortId

            @Summary = "WWP Leos Cfm Delay Measurement Test Fault Is Detected ( " + @AlertKey + " )"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpLeosCfmServiceName,$wwpLeosCfmServiceVlan,$wwpLeosCfmRemoteMEPDelay,$wwpLeosCfmDelayMsgDelayThreshold,$wwpLeosCfmServiceIndex,$wwpLeosCfmRemoteMEPID,$wwpLeosCfmDelayMsgPortId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpLeosCfmServiceName", $wwpLeosCfmServiceName, "wwpLeosCfmServiceVlan", $wwpLeosCfmServiceVlan, "wwpLeosCfmRemoteMEPDelay", $wwpLeosCfmRemoteMEPDelay,
                 "wwpLeosCfmDelayMsgDelayThreshold", $wwpLeosCfmDelayMsgDelayThreshold, "wwpLeosCfmServiceIndex", $wwpLeosCfmServiceIndex, "wwpLeosCfmRemoteMEPID", $wwpLeosCfmRemoteMEPID,
                 "wwpLeosCfmDelayMsgPortId", $wwpLeosCfmDelayMsgPortId)
            

        case "4": ### wwpLeosCfmJitterFaultTrap

            ##########
            # $1 = wwpLeosCfmServiceName 
            # $2 = wwpLeosCfmServiceVlan 
            # $3 = wwpLeosCfmRemoteMEPJitter 
            # $4 = wwpLeosCfmDelayMsgJitterThreshold 
            ##########

            $wwpLeosCfmServiceName = $1
            $wwpLeosCfmServiceVlan = $2
            $wwpLeosCfmRemoteMEPJitter = $3
            $wwpLeosCfmDelayMsgJitterThreshold = $4

            $wwpLeosCfmServiceIndex = extract($OID1, "\.([0-9]+)$")
            $wwpLeosCfmRemoteMEPID = extract($OID3, "\.([0-9]+)$")
            $wwpLeosCfmDelayMsgPortId= extract($OID4, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-ciena-WWP-LEOS-CFM-MIB-wwpLeosCfmJitterFaultTrap"

            @AlertGroup = "WWP Leos Cfm Jitter Fault Trap"
            @AlertKey = "wwpLeosCfmRemoteMEPEntry." + $wwpLeosCfmServiceIndex + "." + $wwpLeosCfmRemoteMEPID + 
                        ", wwpLeosCfmDelayMsgEntry." + $wwpLeosCfmServiceIndex + "." + $wwpLeosCfmDelayMsgPortId

            @Summary = "WWP Leos Cfm Jitter Delay Measurement Test Fault Is Detected ( " + @AlertKey + " )"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpLeosCfmServiceName,$wwpLeosCfmServiceVlan,$wwpLeosCfmRemoteMEPJitter,$wwpLeosCfmDelayMsgJitterThreshold,$wwpLeosCfmServiceIndex,$wwpLeosCfmRemoteMEPID,$wwpLeosCfmDelayMsgPortId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpLeosCfmServiceName", $wwpLeosCfmServiceName, "wwpLeosCfmServiceVlan", $wwpLeosCfmServiceVlan, "wwpLeosCfmRemoteMEPJitter", $wwpLeosCfmRemoteMEPJitter,
                 "wwpLeosCfmDelayMsgJitterThreshold", $wwpLeosCfmDelayMsgJitterThreshold, "wwpLeosCfmServiceIndex", $wwpLeosCfmServiceIndex, "wwpLeosCfmRemoteMEPID", $wwpLeosCfmRemoteMEPID,
                 "wwpLeosCfmDelayMsgPortId", $wwpLeosCfmDelayMsgPortId)


        case "5": ### wwpLeosCfmFrameLossNearFaultTrap

            ##########
            # $1 = wwpLeosCfmServiceName 
            # $2 = wwpLeosCfmServiceVlan 
            # $3 = wwpLeosCfmRemoteMEPFrameLossNear 
            # $4 = wwpLeosCfmFrameLossMsgFlnThreshold 
            ##########

            $wwpLeosCfmServiceName = $1
            $wwpLeosCfmServiceVlan = $2
            $wwpLeosCfmRemoteMEPFrameLossNear = $3
            $wwpLeosCfmFrameLossMsgFlnThreshold = $4

            $wwpLeosCfmServiceIndex = extract($OID1, "\.([0-9]+)$")
            $wwpLeosCfmRemoteMEPID = extract($OID3, "\.([0-9]+)$")
            $wwpLeosCfmFrameLossMsgPortId = extract($OID4, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-ciena-WWP-LEOS-CFM-MIB-wwpLeosCfmFrameLossNearFaultTrap"

            @AlertGroup = "WWP Leos Cfm Frame Loss Near Fault Trap"
            @AlertKey = "wwpLeosCfmRemoteMEPEntry." + $wwpLeosCfmServiceIndex + "." + $wwpLeosCfmRemoteMEPID + 
                        ", wwpLeosCfmFrameLossMsgEntry." + $wwpLeosCfmServiceIndex + "." + $wwpLeosCfmFrameLossMsgPortId

            @Summary = "WWP Leos Cfm Frame Loss Near Measurement Test Fault Is Detected ( " + @AlertKey + " )"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpLeosCfmServiceName,$wwpLeosCfmServiceVlan,$wwpLeosCfmRemoteMEPFrameLossNear,$wwpLeosCfmFrameLossMsgFlnThreshold,$wwpLeosCfmServiceIndex,$wwpLeosCfmRemoteMEPID,$wwpLeosCfmFrameLossMsgPortId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpLeosCfmServiceName", $wwpLeosCfmServiceName, "wwpLeosCfmServiceVlan", $wwpLeosCfmServiceVlan, "wwpLeosCfmRemoteMEPFrameLossNear", $wwpLeosCfmRemoteMEPFrameLossNear,
                 "wwpLeosCfmFrameLossMsgFlnThreshold", $wwpLeosCfmFrameLossMsgFlnThreshold, "wwpLeosCfmServiceIndex", $wwpLeosCfmServiceIndex, "wwpLeosCfmRemoteMEPID", $wwpLeosCfmRemoteMEPID,
                 "wwpLeosCfmFrameLossMsgPortId", $wwpLeosCfmFrameLossMsgPortId)


        case "6": ### wwpLeosCfmFrameLossFarFaultTrap

            ##########
            # $1 = wwpLeosCfmServiceName 
            # $2 = wwpLeosCfmServiceVlan 
            # $3 = wwpLeosCfmRemoteMEPFrameLossFar 
            # $4 = wwpLeosCfmFrameLossMsgFlfThreshold 
            ##########

            $wwpLeosCfmServiceName = $1
            $wwpLeosCfmServiceVlan = $2
            $wwpLeosCfmRemoteMEPFrameLossFar = $3
            $wwpLeosCfmFrameLossMsgFlfThreshold = $4

            $wwpLeosCfmServiceIndex = extract($OID1, "\.([0-9]+)$")
            $wwpLeosCfmRemoteMEPID = extract($OID3, "\.([0-9]+)$")
            $wwpLeosCfmFrameLossMsgPortId = extract($OID4, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-ciena-WWP-LEOS-CFM-MIB-wwpLeosCfmFrameLossFarFaultTrap"

            @AlertGroup = "WWP Leos Cfm Frame Loss Far Fault Trap"
            @AlertKey = "wwpLeosCfmRemoteMEPEntry." + $wwpLeosCfmServiceIndex + "." + $wwpLeosCfmRemoteMEPID + 
                        ", wwpLeosCfmFrameLossMsgEntry." + $wwpLeosCfmServiceIndex + "." + $wwpLeosCfmFrameLossMsgPortId

            @Summary = "WWP Leos Cfm Frame Loss Far Measurement Test Fault Is Detected ( " + @AlertKey + " )"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpLeosCfmServiceName,$wwpLeosCfmServiceVlan,$wwpLeosCfmRemoteMEPFrameLossFar,$wwpLeosCfmFrameLossMsgFlfThreshold,$wwpLeosCfmServiceIndex,$wwpLeosCfmRemoteMEPID,$wwpLeosCfmFrameLossMsgPortId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpLeosCfmServiceName", $wwpLeosCfmServiceName, "wwpLeosCfmServiceVlan", $wwpLeosCfmServiceVlan, "wwpLeosCfmRemoteMEPFrameLossFar", $wwpLeosCfmRemoteMEPFrameLossFar,
                 "wwpLeosCfmFrameLossMsgFlfThreshold", $wwpLeosCfmFrameLossMsgFlfThreshold, "wwpLeosCfmServiceIndex", $wwpLeosCfmServiceIndex, "wwpLeosCfmRemoteMEPID", $wwpLeosCfmRemoteMEPID,
                 "wwpLeosCfmFrameLossMsgPortId", $wwpLeosCfmFrameLossMsgPortId)


        case "7": ### wwpLeosCfmExtDelayFaultTrap

            ##########
            # $1 = wwpLeosCfmServiceName 
            # $2 = wwpLeosCfmServiceVlan 
            # $3 = wwpLeosCfmRemoteMEPDelay 
            # $4 = wwpLeosCfmExtDelayMsgDelayThreshold 
            ##########

            $wwpLeosCfmServiceName = $1
            $wwpLeosCfmServiceVlan = $2
            $wwpLeosCfmRemoteMEPDelay = $3
            $wwpLeosCfmExtDelayMsgDelayThreshold = $4

            $wwpLeosCfmServiceIndex = extract($OID1, "\.([0-9]+)$")
            $wwpLeosCfmRemoteMEPID = extract($OID3, "\.([0-9]+)$")
            $wwpLeosCfmExtDelayMsgLocalMEPId = extract($OID4, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-ciena-WWP-LEOS-CFM-MIB-wwpLeosCfmExtDelayFaultTrap"

            @AlertGroup = "WWP Leos Cfm Ext Delay Fault Trap"
            @AlertKey = "wwpLeosCfmRemoteMEPEntry." + $wwpLeosCfmServiceIndex + "." + $wwpLeosCfmRemoteMEPID + 
                        ", wwpLeosCfmExtDelayMsgEntry." + $wwpLeosCfmServiceIndex + "." + $wwpLeosCfmExtDelayMsgLocalMEPId

            @Summary = "WWP Leos Cfm Ext Delay Measurement Test Fault Is Detected ( " + @AlertKey + " )"


            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpLeosCfmServiceName,$wwpLeosCfmServiceVlan,$wwpLeosCfmRemoteMEPDelay,$wwpLeosCfmExtDelayMsgDelayThreshold,$wwpLeosCfmServiceIndex,$wwpLeosCfmRemoteMEPID,$wwpLeosCfmExtDelayMsgLocalMEPId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpLeosCfmServiceName", $wwpLeosCfmServiceName, "wwpLeosCfmServiceVlan", $wwpLeosCfmServiceVlan, "wwpLeosCfmRemoteMEPDelay", $wwpLeosCfmRemoteMEPDelay,
                 "wwpLeosCfmExtDelayMsgDelayThreshold", $wwpLeosCfmExtDelayMsgDelayThreshold, "wwpLeosCfmServiceIndex", $wwpLeosCfmServiceIndex, "wwpLeosCfmRemoteMEPID", $wwpLeosCfmRemoteMEPID,
                 "wwpLeosCfmExtDelayMsgLocalMEPId", $wwpLeosCfmExtDelayMsgLocalMEPId)
            

        case "8": ### wwpLeosCfmExtJitterFaultTrap

            ##########
            # $1 = wwpLeosCfmServiceName 
            # $2 = wwpLeosCfmServiceVlan 
            # $3 = wwpLeosCfmRemoteMEPJitter 
            # $4 = wwpLeosCfmExtDelayMsgJitterThreshold 
            ##########

            $wwpLeosCfmServiceName = $1
            $wwpLeosCfmServiceVlan = $2
            $wwpLeosCfmRemoteMEPJitter = $3
            $wwpLeosCfmExtDelayMsgJitterThreshold = $4

            $wwpLeosCfmServiceIndex = extract($OID1, "\.([0-9]+)$")
            $wwpLeosCfmRemoteMEPID = extract($OID3, "\.([0-9]+)$")
            $wwpLeosCfmExtDelayMsgLocalMEPId = extract($OID4, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-ciena-WWP-LEOS-CFM-MIB-wwpLeosCfmExtJitterFaultTrap"

            @AlertGroup = "WWP Leos Cfm Ext Jitter Fault Trap"
            @AlertKey = "wwpLeosCfmRemoteMEPEntry." + $wwpLeosCfmServiceIndex + "." + $wwpLeosCfmRemoteMEPID + 
                        ", wwpLeosCfmExtDelayMsgEntry." + $wwpLeosCfmServiceIndex + "." + $wwpLeosCfmExtDelayMsgLocalMEPId

            @Summary = "WWP Leos Cfm Ext Jitter Delay Measurement Test Fault Is Detected ( " + @AlertKey + " )"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpLeosCfmServiceName,$wwpLeosCfmServiceVlan,$wwpLeosCfmRemoteMEPJitter,$wwpLeosCfmExtDelayMsgJitterThreshold,$wwpLeosCfmServiceIndex,$wwpLeosCfmRemoteMEPID,$wwpLeosCfmExtDelayMsgLocalMEPId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpLeosCfmServiceName", $wwpLeosCfmServiceName, "wwpLeosCfmServiceVlan", $wwpLeosCfmServiceVlan, "wwpLeosCfmRemoteMEPJitter", $wwpLeosCfmRemoteMEPJitter,
                 "wwpLeosCfmExtDelayMsgJitterThreshold", $wwpLeosCfmExtDelayMsgJitterThreshold, "wwpLeosCfmServiceIndex", $wwpLeosCfmServiceIndex, "wwpLeosCfmRemoteMEPID", $wwpLeosCfmRemoteMEPID,
                 "wwpLeosCfmExtDelayMsgLocalMEPId", $wwpLeosCfmExtDelayMsgLocalMEPId)

            
        case "9": ### wwpLeosCfmExtFrameLossNearFaultTrap

            ##########
            # $1 = wwpLeosCfmServiceName 
            # $2 = wwpLeosCfmServiceVlan 
            # $3 = wwpLeosCfmRemoteMEPFrameLossNear 
            # $4 = wwpLeosCfmExtFrameLossMsgFlnThreshold 
            ##########

            $wwpLeosCfmServiceName = $1
            $wwpLeosCfmServiceVlan = $2
            $wwpLeosCfmRemoteMEPFrameLossNear = $3
            $wwpLeosCfmExtFrameLossMsgFlnThreshold = $4
            $wwpLeosCfmServiceIndex = extract($OID1, "\.([0-9]+)$")

            $wwpLeosCfmRemoteMEPID = extract($OID3, "\.([0-9]+)$")
            $wwpLeosCfmExtFrameLossMsgLocalMEPId = extract($OID4, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-ciena-WWP-LEOS-CFM-MIB-wwpLeosCfmExtFrameLossNearFaultTrap"

            @AlertGroup = "WWP Leos Cfm Ext Frame Loss Near Fault Trap"
            @AlertKey = "wwpLeosCfmRemoteMEPEntry." + $wwpLeosCfmServiceIndex + "." + $wwpLeosCfmRemoteMEPID + 
                        ", wwpLeosCfmExtFrameLossMsgEntry." + $wwpLeosCfmServiceIndex + "." + $wwpLeosCfmExtFrameLossMsgLocalMEPId

            @Summary = "WWP Leos Cfm Ext Frame Loss Near Measurement Test Fault Is Detected ( " + @AlertKey + " )"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpLeosCfmServiceName,$wwpLeosCfmServiceVlan,$wwpLeosCfmRemoteMEPFrameLossNear,$wwpLeosCfmExtFrameLossMsgFlnThreshold,$wwpLeosCfmServiceIndex,$wwpLeosCfmRemoteMEPID,$wwpLeosCfmExtFrameLossMsgLocalMEPId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpLeosCfmServiceName", $wwpLeosCfmServiceName, "wwpLeosCfmServiceVlan", $wwpLeosCfmServiceVlan, "wwpLeosCfmRemoteMEPFrameLossNear", $wwpLeosCfmRemoteMEPFrameLossNear,
                 "wwpLeosCfmExtFrameLossMsgFlnThreshold", $wwpLeosCfmExtFrameLossMsgFlnThreshold, "wwpLeosCfmServiceIndex", $wwpLeosCfmServiceIndex, "wwpLeosCfmRemoteMEPID", $wwpLeosCfmRemoteMEPID,
                 "wwpLeosCfmExtFrameLossMsgLocalMEPId", $wwpLeosCfmExtFrameLossMsgLocalMEPId)
            

        case "20": ### wwpLeosCfmExtFrameLossFarFaultTrap

            ##########
            # $1 = wwpLeosCfmServiceName 
            # $2 = wwpLeosCfmServiceVlan 
            # $3 = wwpLeosCfmRemoteMEPFrameLossFar 
            # $4 = wwpLeosCfmExtFrameLossMsgFlfThreshold 
            ##########

            $wwpLeosCfmServiceName = $1
            $wwpLeosCfmServiceVlan = $2
            $wwpLeosCfmRemoteMEPFrameLossFar = $3
            $wwpLeosCfmExtFrameLossMsgFlfThreshold = $4

            $wwpLeosCfmServiceIndex = extract($OID1, "\.([0-9]+)$")
            $wwpLeosCfmRemoteMEPID = extract($OID3, "\.([0-9]+)$")
            $wwpLeosCfmExtFrameLossMsgLocalMEPId = extract($OID4, "\.([0-9]+)$") 

            $OS_EventId = "SNMPTRAP-ciena-WWP-LEOS-CFM-MIB-wwpLeosCfmExtFrameLossFarFaultTrap"

            @AlertGroup = "WWP Leos Cfm Ext Frame Loss Far Fault Trap"
            @AlertKey = "wwpLeosCfmRemoteMEPEntry." + $wwpLeosCfmServiceIndex + "." + $wwpLeosCfmRemoteMEPID + 
                        ", wwpLeosCfmExtFrameLossMsgEntry." + $wwpLeosCfmServiceIndex + "." + $wwpLeosCfmExtFrameLossMsgLocalMEPId

            @Summary = "WWP Leos Cfm Ext Frame Loss Far Measurement Test Fault Is Detected ( " + @AlertKey + " )"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpLeosCfmServiceName,$wwpLeosCfmServiceVlan,$wwpLeosCfmRemoteMEPFrameLossFar,$wwpLeosCfmExtFrameLossMsgFlfThreshold,$wwpLeosCfmServiceIndex,$wwpLeosCfmRemoteMEPID,$wwpLeosCfmExtFrameLossMsgLocalMEPId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpLeosCfmServiceName", $wwpLeosCfmServiceName, "wwpLeosCfmServiceVlan", $wwpLeosCfmServiceVlan, "wwpLeosCfmRemoteMEPFrameLossFar", $wwpLeosCfmRemoteMEPFrameLossFar,
                 "wwpLeosCfmExtFrameLossMsgFlfThreshold", $wwpLeosCfmExtFrameLossMsgFlfThreshold, "wwpLeosCfmServiceIndex", $wwpLeosCfmServiceIndex, "wwpLeosCfmRemoteMEPID", $wwpLeosCfmRemoteMEPID,
                 "wwpLeosCfmExtFrameLossMsgLocalMEPId", $wwpLeosCfmExtFrameLossMsgLocalMEPId)


        case "21": ### wwpLeosCfmExtFaultTrapSet

            ##########
            # $1 = wwpLeosCfmServiceName 
            # $2 = wwpLeosCfmServiceType 
            # $3 = wwpLeosCfmServiceValue 
            # $4 = wwpLeosCfmServiceAdminState 
            # $5 = wwpLeosCfmServiceOperState 
            # $6 = wwpLeosCfmServiceMdLevel 
            # $7 = wwpLeosCfmServiceFaultTime 
            # $8 = wwpLeosCfmServiceFaultType 
            # $9 = wwpLeosCfmServiceFaultDesc 
            # $10 = wwpLeosCfmServiceFaultMep 
            ##########

            $wwpLeosCfmServiceName = $1
            $wwpLeosCfmServiceType = lookup($2, wwpLeosCfmServiceType)
            $wwpLeosCfmServiceValue = $3
            $wwpLeosCfmServiceAdminState = lookup($4, wwpLeosCfmServiceAdminState)
            $wwpLeosCfmServiceOperState = lookup($5, wwpLeosCfmServiceOperState)
            $wwpLeosCfmServiceMdLevel = $6
            $wwpLeosCfmServiceFaultTime = $7
            $wwpLeosCfmServiceFaultType = lookup($8, wwpLeosCfmServiceFaultType)
            $wwpLeosCfmServiceFaultDesc = $9
            $wwpLeosCfmServiceFaultMep = $10

            $wwpLeosCfmServiceIndex = extract($OID1, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-ciena-WWP-LEOS-CFM-MIB-wwpLeosCfmExtFaultTrapSet"

            @AlertGroup = "WWP Leos Cfm Ext Fault Trap"
            @AlertKey = "wwpLeosCfmServiceEntry." + $wwpLeosCfmServiceIndex
            @Summary = "WWP Leos Cfm Ext Fault Trap Is Detected ( " + @AlertKey + " )"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            $wwpLeosCfmServiceType = $wwpLeosCfmServiceType + " ( " + $2 + " )"
            $wwpLeosCfmServiceAdminState = $wwpLeosCfmServiceAdminState + " ( " + $4 + " )"
            $wwpLeosCfmServiceOperState = $wwpLeosCfmServiceOperState + " ( " + $5 + " )"

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpLeosCfmServiceName,$wwpLeosCfmServiceType,$wwpLeosCfmServiceValue,$wwpLeosCfmServiceAdminState,$wwpLeosCfmServiceOperState,$wwpLeosCfmServiceMdLevel,$wwpLeosCfmServiceFaultTime,$wwpLeosCfmServiceFaultType,$wwpLeosCfmServiceFaultDesc,$wwpLeosCfmServiceFaultMep,$wwpLeosCfmServiceIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpLeosCfmServiceName", $wwpLeosCfmServiceName, "wwpLeosCfmServiceType", $wwpLeosCfmServiceType, "wwpLeosCfmServiceValue", $wwpLeosCfmServiceValue,
                 "wwpLeosCfmServiceAdminState", $wwpLeosCfmServiceAdminState, "wwpLeosCfmServiceOperState", $wwpLeosCfmServiceOperState, "wwpLeosCfmServiceMdLevel", $wwpLeosCfmServiceMdLevel,
                 "wwpLeosCfmServiceFaultTime", $wwpLeosCfmServiceFaultTime, "wwpLeosCfmServiceFaultType", $wwpLeosCfmServiceFaultType, "wwpLeosCfmServiceFaultDesc", $wwpLeosCfmServiceFaultDesc,
                 "wwpLeosCfmServiceFaultMep", $wwpLeosCfmServiceFaultMep, "wwpLeosCfmServiceIndex", $wwpLeosCfmServiceIndex)


        case "22": ### wwpLeosCfmExtFaultTrapClear

            ##########
            # $1 = wwpLeosCfmServiceName 
            # $2 = wwpLeosCfmServiceType 
            # $3 = wwpLeosCfmServiceValue 
            # $4 = wwpLeosCfmServiceAdminState 
            # $5 = wwpLeosCfmServiceOperState 
            # $6 = wwpLeosCfmServiceMdLevel 
            # $7 = wwpLeosCfmServiceFaultTime 
            # $8 = wwpLeosCfmServiceFaultType 
            # $9 = wwpLeosCfmServiceFaultDesc 
            # $10 = wwpLeosCfmServiceFaultMep 
            ##########

            $wwpLeosCfmServiceName = $1
            $wwpLeosCfmServiceType = lookup($2, wwpLeosCfmServiceType)
            $wwpLeosCfmServiceValue = $3
            $wwpLeosCfmServiceAdminState = lookup($4, wwpLeosCfmServiceAdminState)
            $wwpLeosCfmServiceOperState = lookup($5, wwpLeosCfmServiceOperState)
            $wwpLeosCfmServiceMdLevel = $6
            $wwpLeosCfmServiceFaultTime = $7
            $wwpLeosCfmServiceFaultType = lookup($8, wwpLeosCfmServiceFaultType)
            $wwpLeosCfmServiceFaultDesc = $9
            $wwpLeosCfmServiceFaultMep = $10

            $wwpLeosCfmServiceIndex = extract($OID1, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-ciena-WWP-LEOS-CFM-MIB-wwpLeosCfmExtFaultTrapClear"

            @AlertGroup = "WWP Leos Cfm Ext Fault Trap"
            @AlertKey = "wwpLeosCfmServiceEntry." + $wwpLeosCfmServiceIndex
            @Summary = "WWP Leos Cfm Ext Fault Trap Is Resolved And Cleared ( " + @AlertKey + " )"

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            $wwpLeosCfmServiceType = $wwpLeosCfmServiceType + " ( " + $2 + " )"
            $wwpLeosCfmServiceAdminState = $wwpLeosCfmServiceAdminState + " ( " + $4 + " )"
            $wwpLeosCfmServiceOperState = $wwpLeosCfmServiceOperState + " ( " + $5 + " )"

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpLeosCfmServiceName,$wwpLeosCfmServiceType,$wwpLeosCfmServiceValue,$wwpLeosCfmServiceAdminState,$wwpLeosCfmServiceOperState,$wwpLeosCfmServiceMdLevel,$wwpLeosCfmServiceFaultTime,$wwpLeosCfmServiceFaultType,$wwpLeosCfmServiceFaultDesc,$wwpLeosCfmServiceFaultMep,$wwpLeosCfmServiceIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpLeosCfmServiceName", $wwpLeosCfmServiceName, "wwpLeosCfmServiceType", $wwpLeosCfmServiceType, "wwpLeosCfmServiceValue", $wwpLeosCfmServiceValue,
                 "wwpLeosCfmServiceAdminState", $wwpLeosCfmServiceAdminState, "wwpLeosCfmServiceOperState", $wwpLeosCfmServiceOperState, "wwpLeosCfmServiceMdLevel", $wwpLeosCfmServiceMdLevel,
                 "wwpLeosCfmServiceFaultTime", $wwpLeosCfmServiceFaultTime, "wwpLeosCfmServiceFaultType", $wwpLeosCfmServiceFaultType, "wwpLeosCfmServiceFaultDesc", $wwpLeosCfmServiceFaultDesc,
                 "wwpLeosCfmServiceFaultMep", $wwpLeosCfmServiceFaultMep, "wwpLeosCfmServiceIndex", $wwpLeosCfmServiceIndex)


        case "23": ### wwpLeosCfmBadSequenceFaultTrap

            ##########
            # $1 = wwpLeosCfmServiceName 
            # $2 = wwpLeosCfmServiceVlan 
            # $3 = wwpLeosCfmExtMEPLMMBadSequence 
            # $4 = wwpLeosCfmExtFrameLossMsgSeqThreshold 
            ##########

            $wwpLeosCfmServiceName = $1
            $wwpLeosCfmServiceVlan = $2
            $wwpLeosCfmExtMEPLMMBadSequence = $3
            $wwpLeosCfmExtFrameLossMsgSeqThreshold = $4

            $wwpLeosCfmServiceIndex = extract($OID1, "\.([0-9]+)$")
            $wwpLeosCfmExtMEPId = extract($OID3, "\.([0-9]+)$")
            $wwpLeosCfmExtFrameLossMsgLocalMEPId = extract($OID4, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-ciena-WWP-LEOS-CFM-MIB-wwpLeosCfmBadSequenceFaultTrap"

            @AlertGroup = "WWP Leos Cfm Bad Sequence Fault Trap"
            @AlertKey = "wwpLeosCfmExtMEPEntry." + $wwpLeosCfmServiceIndex + "." + $wwpLeosCfmExtMEPId + 
						", wwpLeosCfmExtFrameLossMsgEntry." + $wwpLeosCfmServiceIndex + "." + $wwpLeosCfmExtFrameLossMsgLocalMEPId
            @Summary = "WWP Leos Cfm Bad Sequence Exceeds: " + $wwpLeosCfmExtFrameLossMsgSeqThreshold + " In Frame Loss Measurement Test ( " + @AlertKey + " )"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpLeosCfmServiceName,$wwpLeosCfmServiceVlan,$wwpLeosCfmExtMEPLMMBadSequence,$wwpLeosCfmExtFrameLossMsgSeqThreshold,$wwpLeosCfmServiceIndex,$wwpLeosCfmExtMEPId,$wwpLeosCfmExtFrameLossMsgLocalMEPId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpLeosCfmServiceName", $wwpLeosCfmServiceName, "wwpLeosCfmServiceVlan", $wwpLeosCfmServiceVlan, "wwpLeosCfmExtMEPLMMBadSequence", $wwpLeosCfmExtMEPLMMBadSequence,
                 "wwpLeosCfmExtFrameLossMsgSeqThreshold", $wwpLeosCfmExtFrameLossMsgSeqThreshold, "wwpLeosCfmServiceIndex", $wwpLeosCfmServiceIndex, "wwpLeosCfmExtMEPId", $wwpLeosCfmExtMEPId,
                 "wwpLeosCfmExtFrameLossMsgLocalMEPId", $wwpLeosCfmExtFrameLossMsgLocalMEPId)
			
		case "24": ### wwpLeosCfmBlockOppositeMEPSetTrap

            ##########
            # $1 = sysName 
            # $2 = wwpLeosCfmServiceMdLevel 
            # $3 = wwpLeosCfmServiceMAID 
            # $4 = wwpLeosCfmExtMEPId 
            # $5 = wwpLeosCfmExtVlanId 
            # $6 = wwpLeosCfmExtMEPTagVID 
            # $7 = wwpLeosCfmExtMEPBlockOppositeFaultCurrent 
            ##########

            $sysName = $1
            $wwpLeosCfmServiceMdLevel = $2
            $wwpLeosCfmServiceMAID = $3
            $wwpLeosCfmExtMEPId = $4
            $wwpLeosCfmExtVlanId = $5
            $wwpLeosCfmExtMEPTagVID = $6
            $wwpLeosCfmExtMEPBlockOppositeFaultCurrent = $7

            $OS_EventId = "SNMPTRAP-ciena-WWP-LEOS-CFM-MIB-wwpLeosCfmBlockOppositeMEPSetTrap"
			
			$wwpLeosCfmServiceIndex = extract($OID2, "\.([0-9]+)$")

            @AlertGroup = "WWP Leos Cfm Blocked Opposite MEP Trap"
            @AlertKey = "wwpLeosCfmServiceEntry." + $wwpLeosCfmServiceIndex + 
						", wwpLeosCfmExtMEPEntry." + $wwpLeosCfmServiceIndex + "." + $wwpLeosCfmExtMEPId
            @Summary = "WWP Leos Cfm Blocked Opposite MEP Fault Trap Is Detected ( " + @AlertKey + " ) "

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($sysName,$wwpLeosCfmServiceMdLevel,$wwpLeosCfmServiceMAID,$wwpLeosCfmExtMEPId,$wwpLeosCfmExtVlanId,$wwpLeosCfmExtMEPTagVID,$wwpLeosCfmExtMEPBlockOppositeFaultCurrent,$wwpLeosCfmServiceIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "sysName", $sysName, "wwpLeosCfmServiceMdLevel", $wwpLeosCfmServiceMdLevel, "wwpLeosCfmServiceMAID", $wwpLeosCfmServiceMAID,
                 "wwpLeosCfmExtMEPId", $wwpLeosCfmExtMEPId, "wwpLeosCfmExtVlanId", $wwpLeosCfmExtVlanId, "wwpLeosCfmExtMEPTagVID", $wwpLeosCfmExtMEPTagVID,
                 "wwpLeosCfmExtMEPBlockOppositeFaultCurrent", $wwpLeosCfmExtMEPBlockOppositeFaultCurrent, "wwpLeosCfmServiceIndex", $wwpLeosCfmServiceIndex)

        case "25": ### wwpLeosCfmBlockOppositeMEPClearTrap

            ##########
            # $1 = sysName 
            # $2 = wwpLeosCfmServiceMdLevel 
            # $3 = wwpLeosCfmServiceMAID 
            # $4 = wwpLeosCfmExtMEPId 
            # $5 = wwpLeosCfmExtVlanId 
            # $6 = wwpLeosCfmExtMEPTagVID 
            ##########

            $sysName = $1
            $wwpLeosCfmServiceMdLevel = $2
            $wwpLeosCfmServiceMAID = $3
            $wwpLeosCfmExtMEPId = $4
            $wwpLeosCfmExtVlanId = $5
            $wwpLeosCfmExtMEPTagVID = $6

            $OS_EventId = "SNMPTRAP-ciena-WWP-LEOS-CFM-MIB-wwpLeosCfmBlockOppositeMEPClearTrap"
		
			$wwpLeosCfmServiceIndex = extract($OID2, "\.([0-9]+)$")

            @AlertGroup = "WWP Leos Cfm Blocked Opposite MEP Trap"
            @AlertKey = "wwpLeosCfmServiceEntry." + $wwpLeosCfmServiceIndex + 
						", wwpLeosCfmExtMEPEntry." + $wwpLeosCfmServiceIndex + "." + $wwpLeosCfmExtMEPId
            @Summary = "WWP Leos Cfm Blocked Opposite MEP Fault Trap Is Resolved And Cleared ( " + @AlertKey + " ) "

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($sysName,$wwpLeosCfmServiceMdLevel,$wwpLeosCfmServiceMAID,$wwpLeosCfmExtMEPId,$wwpLeosCfmExtVlanId,$wwpLeosCfmExtMEPTagVID,$wwpLeosCfmServiceIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "sysName", $sysName, "wwpLeosCfmServiceMdLevel", $wwpLeosCfmServiceMdLevel, "wwpLeosCfmServiceMAID", $wwpLeosCfmServiceMAID,
                 "wwpLeosCfmExtMEPId", $wwpLeosCfmExtMEPId, "wwpLeosCfmExtVlanId", $wwpLeosCfmExtVlanId, "wwpLeosCfmExtMEPTagVID", $wwpLeosCfmExtMEPTagVID,
                 "wwpLeosCfmServiceIndex", $wwpLeosCfmServiceIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, ciena-WWP-LEOS-CFM-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, ciena-WWP-LEOS-CFM-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-WWP-LEOS-CFM-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-WWP-LEOS-CFM-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... ciena-WWP-LEOS-CFM-MIB.include.snmptrap.rules >>>>>")
