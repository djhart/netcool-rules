###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  WWP-QOS-410-MIB
#
###############################################################################

case ".1.3.6.1.4.1.6141.2.29.2": ###  - Notifications from WWP-QOS-410-MIB (200104031700Z)

    log(DEBUG, "<<<<< Entering... ciena-WWP-QOS-410-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Ciena-WWP-QOS-410-MIB"
    @Class = "40506"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### wwpQos410PortOverProvisionedTrap

            ##########
            # $1 = wwpQos410PortIndex 
            ##########

            $wwpQos410PortIndex = $1

            $OS_EventId = "SNMPTRAP-ciena-WWP-QOS-410-MIB-wwpQos410PortOverProvisionedTrap"

            @AlertGroup = "WWP Qos410 Port Provisioned Bandwidth Status"
            @AlertKey = "wwpQos410PortEntry." + $wwpQos410PortIndex
            @Summary = "WWP Qos410 Port Provisioned Bandwidth, Is Exceeds Total Bandwidth Available For Port ( " + @AlertKey + " )"

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpQos410PortIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpQos410PortIndex", $wwpQos410PortIndex)


        case "2": ### wwpQos410PortUnderProvisionedTrap

            ##########
            # $1 = wwpQos410PortIndex 
            ##########

            $wwpQos410PortIndex = $1

            $OS_EventId = "SNMPTRAP-ciena-WWP-QOS-410-MIB-wwpQos410PortUnderProvisionedTrap"

            @AlertGroup = "WWP Qos410 Port Provisioned Bandwidth Status"
            @AlertKey = "wwpQos410PortEntry." + $wwpQos410PortIndex
            @Summary = "WWP Qos410 Port Previous Over-Provisioned Situation Is Resolved ( " + @AlertKey + " )"

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpQos410PortIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpQos410PortIndex", $wwpQos410PortIndex)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, ciena-WWP-QOS-410-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, ciena-WWP-QOS-410-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-WWP-QOS-410-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-WWP-QOS-410-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... ciena-WWP-QOS-410-MIB.include.snmptrap.rules >>>>>")
