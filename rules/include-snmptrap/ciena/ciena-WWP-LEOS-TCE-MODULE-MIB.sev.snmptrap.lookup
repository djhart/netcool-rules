###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  WWP-LEOS-TCE-MODULE-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table ciena-WWP-LEOS-TCE-MODULE-MIB_sev =
{
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-MODULE-MIB-wwpLeosTceModuleStateChange_uninstalled","3","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-MODULE-MIB-wwpLeosTceModuleStateChange_unequipped","3","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-MODULE-MIB-wwpLeosTceModuleStateChange_init","2","13","1800"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-MODULE-MIB-wwpLeosTceModuleStateChange_disabled","3","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-MODULE-MIB-wwpLeosTceModuleStateChange_enabled","1","2","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-MODULE-MIB-wwpLeosTceModuleStateChange_faulted","3","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-MODULE-MIB-wwpLeosTceModuleStateChange_hotswap","2","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-MODULE-MIB-wwpLeosTceModuleStateChange_poweroff","3","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-MODULE-MIB-wwpLeosTceModuleStateChange_hitlessReinit","2","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-MODULE-MIB-wwpLeosTceModuleStateChange_fastReload","2","1","0"}, 
        
    
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-MODULE-MIB-wwpLeosTceModuleStateChange_unknown","2","1","0"},
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-MODULE-MIB-wwpLeosTceModuleHealthChange_unknown","2","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-MODULE-MIB-wwpLeosTceModuleHealthChange_good","1","2","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-MODULE-MIB-wwpLeosTceModuleHealthChange_warning","2","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-MODULE-MIB-wwpLeosTceModuleHealthChange_degraded","3","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-MODULE-MIB-wwpLeosTceModuleHealthChange_faulted","4","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-MODULE-MIB-wwpLeosTceModuleSensorHighTempNotification","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-MODULE-MIB-wwpLeosTceModuleLowTempNotification","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-MODULE-MIB-wwpLeosTceModuleNormalTempNotification","1","2","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-MODULE-MIB-wwpLeosTceModuleHASwitchOverNotification","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-MODULE-MIB-wwpLeosTceModuleProtectionModeChangeNotification","3","1","0"},
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-MODULE-MIB-wwpLeosTceModulePostErrorNotification","3","1","0"}
}
default = {"Unknown","Unknown","Unknown"}

