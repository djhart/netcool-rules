###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  WWP-EXT-BRIDGE-TRAP-MIB
#
###############################################################################

case ".1.3.6.1.4.1.6141.2.41.2": ###  - Notifications from WWP-EXT-BRIDGE-TRAP-MIB (200210271700Z)

    log(DEBUG, "<<<<< Entering... ciena-WWP-EXT-BRIDGE-TRAP-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Ciena-WWP-EXT-BRIDGE-TRAP-MIB"
    @Class = "40506"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### wwpLinkUp

            ##########
            # $1 = sysName 
            # $2 = sysLocation 
            # $3 = wwpPortId 
            # $4 = wwpPortName 
            # $5 = wwpPortType 
            # $6 = wwpPortAdminStatus 
            # $7 = wwpPortOperStatus 
            ##########

            $sysName = $1
            $sysLocation = $2
            $wwpPortId = $3
            $wwpPortName = $4
            $wwpPortType = lookup($5, wwpPortType)
            $wwpPortAdminStatus = lookup($6, wwpPortAdminStatus)
            $wwpPortOperStatus = lookup($7, wwpPortOperStatus)


            $OS_EventId = "SNMPTRAP-ciena-WWP-EXT-BRIDGE-TRAP-MIB-wwpLinkUp"

            @AlertGroup = "WWP Link Status"
            @AlertKey = "wwpPortEntry." + $wwpPortId
            @Summary = "ifOperStatus Object Communication Links has Entered the Up State: " + " ( " + @AlertKey + " )"

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            $wwpPortType = $wwpPortType + " ( " + $5 + " )"
            $wwpPortAdminStatus = $wwpPortAdminStatus + " ( " + $6 + " )"
            $wwpPortOperStatus = $wwpPortOperStatus + " ( " + $7 + " )"

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($sysName,$sysLocation,$wwpPortId,$wwpPortName,$wwpPortType,$wwpPortAdminStatus,$wwpPortOperStatus)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "sysName", $sysName, "sysLocation", $sysLocation, "wwpPortId", $wwpPortId,
                 "wwpPortName", $wwpPortName, "wwpPortType", $wwpPortType, "wwpPortAdminStatus", $wwpPortAdminStatus,
                 "wwpPortOperStatus", $wwpPortOperStatus)


        case "2": ### wwpLinkDown

            ##########
            # $1 = sysName 
            # $2 = sysLocation 
            # $3 = wwpPortId 
            # $4 = wwpPortType 
            # $5 = wwpPortName 
            # $6 = wwpPortAdminStatus 
            # $7 = wwpPortOperStatus 
            ##########

            $sysName = $1
            $sysLocation = $2
            $wwpPortId = $3
            $wwpPortType = lookup($4, wwpPortType)
            $wwpPortName = $5
            $wwpPortAdminStatus = lookup($6, wwpPortAdminStatus)
            $wwpPortOperStatus = lookup($7, wwpPortOperStatus)

            $OS_EventId = "SNMPTRAP-ciena-WWP-EXT-BRIDGE-TRAP-MIB-wwpLinkDown"

            @AlertGroup = "WWP Link Status"
            @AlertKey = "wwpPortEntry." + $wwpPortId
            @Summary = "ifOperStatus Object Communication Links has Entered the Down State: " + " ( " + @AlertKey + " )"

            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            $wwpPortType = $wwpPortType + " ( " + $4 + " )"
            $wwpPortAdminStatus = $wwpPortAdminStatus + " ( " + $6 + " )"
            $wwpPortOperStatus = $wwpPortOperStatus + " ( " + $7 + " )"

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($sysName,$sysLocation,$wwpPortId,$wwpPortType,$wwpPortName,$wwpPortAdminStatus,$wwpPortOperStatus)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "sysName", $sysName, "sysLocation", $sysLocation, "wwpPortId", $wwpPortId,
                 "wwpPortType", $wwpPortType, "wwpPortName", $wwpPortName, "wwpPortAdminStatus", $wwpPortAdminStatus,
                 "wwpPortOperStatus", $wwpPortOperStatus)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, ciena-WWP-EXT-BRIDGE-TRAP-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, ciena-WWP-EXT-BRIDGE-TRAP-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-WWP-EXT-BRIDGE-TRAP-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-WWP-EXT-BRIDGE-TRAP-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... ciena-WWP-EXT-BRIDGE-TRAP-MIB.include.snmptrap.rules >>>>>")
