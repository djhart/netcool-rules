###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  WWP-LEOS-TCE-FILE-TRANSFER-MIB
#
###############################################################################
#
# Entries in a Severity lookup table have the following format:
#
# {<"EventId">,<"severity">,<"type">,<"expiretime">}
#
# <"EventId"> is defined in the Rules File.  In some cases it is necessary to
# add a modifier to the EventId to further clarify the condition.
#
# Valid values for <severity> are:
#   0 - clear
#   1 - indeterminate
#   2 - warning
#   3 - minor
#   4 - major
#   5 - critical
#   d - discard
#
# Valid values for <type> are:
#   1 - problem
#   2 - resolution, NOTE: resolution alarms MUST have a <severity> of 1.
#   11 - more severe
#   12 - less severe
#   13 - information
#
# <expiretime> is the number of seconds the event will remain in the
# ObjectServer before being automatically cleared. Alarms which should not
# expire MUST have <expiretime> set to 0 (zero).
#
###############################################################################

table ciena-WWP-LEOS-TCE-FILE-TRANSFER-MIB_sev =
{
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-FILE-TRANSFER-MIB-wwpLeosTceFTransferCompletion_downloadSuccessful","1","2","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-FILE-TRANSFER-MIB-wwpLeosTceFTransferCompletion_putSuccessful","1","2","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-FILE-TRANSFER-MIB-wwpLeosTceFTransferCompletion_noStatus","3","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-FILE-TRANSFER-MIB-wwpLeosTceFTransferCompletion_fileAlreadyExist","3","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-FILE-TRANSFER-MIB-wwpLeosTceFTransferCompletion_tftpServerNotFound","3","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-FILE-TRANSFER-MIB-wwpLeosTceFTransferCompletion_fileGetError","3","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-FILE-TRANSFER-MIB-wwpLeosTceFTransferCompletion_filePutError","3","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-FILE-TRANSFER-MIB-wwpLeosTceFTransferCompletion_fileSystemError","3","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-FILE-TRANSFER-MIB-wwpLeosTceFTransferCompletion_fileContentsInvalid","3","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-FILE-TRANSFER-MIB-wwpLeosTceFTransferCompletion_flashOffLine","3","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-FILE-TRANSFER-MIB-wwpLeosTceFTransferCompletion_fileCRCError","3","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-FILE-TRANSFER-MIB-wwpLeosTceFTransferCompletion_allFilesSkipped","2","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-FILE-TRANSFER-MIB-wwpLeosTceFTransferCompletion_serverIpAddrInvalid","3","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-FILE-TRANSFER-MIB-wwpLeosTceFTransferCompletion_filePathInvalid","3","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-FILE-TRANSFER-MIB-wwpLeosTceFTransferCompletion_fileNameInvalid","3","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-FILE-TRANSFER-MIB-wwpLeosTceFTransferCompletion_sourceNotFound","3","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-FILE-TRANSFER-MIB-wwpLeosTceFTransferCompletion_fileNameNeeded","3","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-FILE-TRANSFER-MIB-wwpLeosTceFTransferCompletion_notEnoughSpace","3","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-FILE-TRANSFER-MIB-wwpLeosTceFTransferCompletion_internalError","3","1","0"}, 
        
    {"SNMPTRAP-ciena-WWP-LEOS-TCE-FILE-TRANSFER-MIB-wwpLeosTceFTransferCompletion_unknown","2","1","0"}
        
}
default = {"Unknown","Unknown","Unknown"}

