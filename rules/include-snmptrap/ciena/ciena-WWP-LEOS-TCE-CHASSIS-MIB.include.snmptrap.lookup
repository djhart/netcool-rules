###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  WWP-LEOS-TCE-CHASSIS-MIB
#
###############################################################################

table WwpLeosTceChassisPowerSupplyState =
{
    {"1","Online"}, ### online
    {"2","Faulted"}, ### faulted
    {"3","Offline"}, ### offline
    {"4","Uninstalled"} ### uninstalled
}
default = "Unknown"

table WwpLeosTceChassisPowerSupplyType =
{
    {"1","AC"}, ### ac
    {"2","DC"}, ### dc
    {"3","Unequipped"} ### unequipped
}
default = "Unknown"

table WwpLeosTceChassisFanTrayType =
{
    {"1","Fixed"}, ### fixed
    {"2","Hot Swappable"}, ### hotSwappable
    {"3","Unequipped"} ### unequipped
}
default = "Unknown"

table WwpLeosTceChassisFanTrayStatus =
{
    {"1","OK"}, ### ok
    {"2","Pending"}, ### pending
    {"3","Failure"}, ### failure
    {"4","Device Not Found"} ### deviceNotFound
}
default = "Unknown"

table WwpLeosTceChassisRebootReasonErrorType =
{
    {"1","Unknown"}, ### unknown
    {"2","User"}, ### user
    {"3","Power Failure"}, ### powerFailure
    {"4","Upgrade"}, ### upgrade
    {"5","Reset Button"}, ### resetButton
    {"6","Cold Failover"}, ### coldFailover
    {"7","Fault Manager"}, ### faultManager
    {"8","Communication Failure"}, ### communicationFailure
    {"9","Auto Revert"}, ### autoRevert
    {"10","Unprotected Failure"}, ### unprotectedFailure
    {"11","Boot Failure"}, ### bootFailure
    {"12","Software Revert"} ### softwareRevert
}
default = "Unknown"

table SystemHealthCatSubCat = 
{
	{"1_1","Unknown"}, ### unknown
	{"2_1","Usage"}, ### cpu_usage	
	{"3_1","Usage"}, ### datapath_status
	{"4_1","Dropped Packets"}, ### ethernet_DroppedPackets
	{"4_2","CRC Errors"}, ### ethernet_CRCErrors
	{"4_3","Error Packets"}, ### ethernet_ErrorPackets
	{"5_3","Status"}, ### fabric_Status
	{"6_1","Flash 0"}, ### disk_Flash0
	{"6_2","Flash 1"}, ### disk_Flash1
	{"6_3","System 0"}, ### disk_Sys0
	{"6_4","System 1"}, ### disk_Sys1
	{"6_5","RAM"}, ### disk_Ram
	{"6_6","Cf0"}, ### disk_Cf0
	{"7_1","Inlet Maximum"}, ### tempModule_InletMax
	{"7_2","Inlet Minimum"}, ### tempModule_InletMin
	{"7_3","Outlet Maximum"}, ### tempModule_OutletMax
	{"7_4","Outlet Minimum"}, ### tempModule_OutletMin
	{"8_1","Status"}, ### fanTray_Status
	{"9_1","Maximum"}, ### tempFan_Max
	{"9_2","Minimum"}, ### tempFan_Min
	{"10_1","Minumum Speed"}, ### fanRpm_MinSpeed
	{"11_1","Status"}, ### power_Status
	{"12_1","Maximum"}, ### tempPower_Max
	{"12_2","Minumum"}, ### tempPower_Min
	{"13_1","Agg Table"}, ### systemResource_AggTable
	{"13_2","Meter Profile Attachment Table"}, ### systemResource_MeterProfileAttachmentTable
	{"13_3","Meter Profile Table"}, ### systemResource_MeterProfileTable
	{"13_4","Pbt Decap Table"}, ### systemResource_PbtDecapTable
	{"13_5","Pbt Encap Table"}, ### systemResource_PbtEncapTable
	{"13_6","Pbt Service Table"}, ### systemResource_PbtServiceTable
	{"13_7","Pbt Transmit Table"}, ### systemResource_PbtTransitTable
	{"13_8","Pbt Tunnel Group Table"}, ### systemResource_PbtTunnelGroupTable
	{"13_9","Pbt State Group Table"}, ### systemResource_PortStateGrpTable
	{"13_10","QOS Flow Table"}, ### systemResource_QosFlowTable
	{"13_11","Sub-Port Table"}, ### systemResource_SubportTable
	{"13_12","VSS Table"}, ### systemResource_VssTable
	{"13_13","Traffic Class Term Table"}, ### systemResource_TrafficClassTermTable
	{"13_14","Flood Container Table"}, ### systemResource_FloodContainerTable
	{"13_15","Logical Interfaces"}, ### systemResource_LogicalInterfaces
	{"13_16","Shared Rate Profiles"}, ### systemResource_SharedRateProfiles
	{"13_17","Shared Rate Attachments"}, ### systemResource_SharedRateAttachments
	{"13_18","Shared Rate TCEs"}, ### systemResource_SharedRateTCEs
	{"13_19","Shared Rate ACL TCEs"}, ### systemResource_SharedRateAclTCEs
	{"13_20","Shaping Profiles"}, ### systemResource_ShapingProfiles
	{"13_21","Shaping Profiles Attachments"}, ### systemResource_ShapingProfileAttachments
	{"14_1","Global Heap"}, ### memory_GlobalHeap
	{"14_2","Heap 1"}, ### memory_Heap1
	{"14_3","Heap 2"}, ### memory_Heap2
	{"14_4","Pool 1"}, ### memory_Pool1
	{"14_5","Pool 2"}, ### memory_Pool2
	{"15_1","Table"}, ### mac_Table
	{"16_1","PS1"}, ### i2c_PS1
	{"16_2","PS2"}, ### i2c_PS2
	{"16_3","Alarm Card"}, ### i2c_AlarmCard
	{"16_4","Fan Tray"}, ### i2c_FanTray
	{"16_5","IOM3"}, ### i2c_IOM3
	{"16_6","IOM4"}, ### i2c_IOM4
	{"16_7","IOM5"}, ### i2c_IOM5
	{"16_8","IOM6"}, ### i2c_IOM6
	{"16_9","IOM7"}, ### i2c_IOM7
	{"17_1","Write Erase Part 1"}, ### flash_WriteErasePart1
	{"17_2","Write Erase Part 2"}, ### flash_WriteErasePart2
	{"18_1","Temp Maximum"}, ### transceiver_TempMax
	{"18_2","Temp Maximum"}, ### transceiver_TempMin
	{"18_3","Rx Power Maximum"}, ### transceiver_RxPowerMax
	{"18_4","Rx Power Minimum"}, ### transceiver_RxPowerMin
	{"18_5","Tx Power Maximum"}, ### transceiver_TxPowerMax
	{"18_6","Tx Power Minimum"}, ### transceiver_TxPowerMin
	{"19_1","State"}, ### link_State
}
default = "Unknown"
