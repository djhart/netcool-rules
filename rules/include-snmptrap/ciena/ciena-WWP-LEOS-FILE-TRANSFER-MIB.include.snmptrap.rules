###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  WWP-LEOS-FILE-TRANSFER-MIB
#
###############################################################################

case ".1.3.6.1.4.1.6141.2.60.23.2": ###  - Notifications from WWP-LEOS-FILE-TRANSFER-MIB (200104031700Z)

    log(DEBUG, "<<<<< Entering... ciena-WWP-LEOS-FILE-TRANSFER-MIB.include.snmptrap.rules >>>>>")

    @Agent = "Ciena-WWP-LEOS-FILE-TRANSFER-MIB"
    @Class = "40506"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### wwpLeosFTransferCompletion

            ##########
            # $1 = wwpLeosFTransferRemoteFilename 
            # $2 = wwpLeosFTransferLocalFilename 
            # $3 = wwpLeosFTransferNotificationStatus 
            # $4 = wwpLeosFTransferNotificationInfo 
            ##########

            $wwpLeosFTransferRemoteFilename = $1
            $wwpLeosFTransferLocalFilename = $2
            $wwpLeosFTransferNotificationStatus = lookup($3, wwpLeosFTransferNotificationStatus) 
            $wwpLeosFTransferNotificationInfo = $4

            $OS_EventId = "SNMPTRAP-ciena-WWP-LEOS-FILE-TRANSFER-MIB-wwpLeosFTransferCompletion"

            @AlertGroup = "WWP Leos FTransfer Completion Status"
            @AlertKey = "Local File Name: " + $wwpLeosFTransferLocalFilename

            switch($3)
            {
                        case "0": ### downloadSuccess
                            $SEV_KEY = $OS_EventId + "_downloadSuccess"
                            @Summary = "WWP Leos FTransfer Completion Status : " + $wwpLeosFTransferNotificationStatus + ", Info : " + $wwpLeosFTransferNotificationInfo
                            $DEFAULT_Severity = 1
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0

                        case "1": ### tftpServerNotFound
                            $SEV_KEY = $OS_EventId + "_tftpServerNotFound"
                            @Summary = "WWP Leos FTransfer Completion Status : " + $wwpLeosFTransferNotificationStatus + ", Info : " + $wwpLeosFTransferNotificationInfo
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "2": ### couldNotGetFile
                            $SEV_KEY = $OS_EventId + "_couldNotGetFile"
                            @Summary = "WWP Leos FTransfer Completion Status : " + $wwpLeosFTransferNotificationStatus + ", Info : " + $wwpLeosFTransferNotificationInfo
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "3": ### cmdFileParseError
                            $SEV_KEY = $OS_EventId + "_cmdFileParseError"
                            @Summary = "WWP Leos FTransfer Completion Status : " + $wwpLeosFTransferNotificationStatus + ", Info : " + $wwpLeosFTransferNotificationInfo
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "4": ### internalFilesystemError
                            $SEV_KEY = $OS_EventId + "_internalFilesystemError"
                            @Summary = "WWP Leos FTransfer Completion Status : " + $wwpLeosFTransferNotificationStatus + ", Info : " + $wwpLeosFTransferNotificationInfo
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "5": ### inValidFileContents
                            $SEV_KEY = $OS_EventId + "_inValidFileContents"
                            @Summary = "WWP Leos FTransfer Completion Status : " + $wwpLeosFTransferNotificationStatus + ", Info : " + $wwpLeosFTransferNotificationInfo
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "6": ### flashOffline
                            $SEV_KEY = $OS_EventId + "_flashOffline"
                            @Summary = "WWP Leos FTransfer Completion Status : " + $wwpLeosFTransferNotificationStatus + ", Info : " + $wwpLeosFTransferNotificationInfo
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "7": ### noStatus
                            $SEV_KEY = $OS_EventId + "_noStatus"
                            @Summary = "WWP Leos FTransfer Completion Status : " + $wwpLeosFTransferNotificationStatus + ", Info : " + $wwpLeosFTransferNotificationInfo
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "8": ### putSuccessful
                            $SEV_KEY = $OS_EventId + "_putSuccessful"
                            @Summary = "WWP Leos FTransfer Completion Status : " + $wwpLeosFTransferNotificationStatus + ", Info : " + $wwpLeosFTransferNotificationInfo
                            $DEFAULT_Severity = 1
                            $DEFAULT_Type = 2
                            $DEFAULT_ExpireTime = 0

                        case "9": ### couldNotPutFile
                            $SEV_KEY = $OS_EventId + "_couldNotPutFile"
                            @Summary = "WWP Leos FTransfer Completion Status : " + $wwpLeosFTransferNotificationStatus + ", Info : " + $wwpLeosFTransferNotificationInfo
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "10": ### badFileCrc
                            $SEV_KEY = $OS_EventId + "_badFileCrc"
                            @Summary = "WWP Leos FTransfer Completion Status : " + $wwpLeosFTransferNotificationStatus + ", Info : " + $wwpLeosFTransferNotificationInfo
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        case "11": ### allFilesSkipped
                            $SEV_KEY = $OS_EventId + "_allFilesSkipped"
                            @Summary = "WWP Leos FTransfer Completion Status : " + $wwpLeosFTransferNotificationStatus + ", Info : " + $wwpLeosFTransferNotificationInfo
                            $DEFAULT_Severity = 3
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0

                        default:
                            $SEV_KEY = $OS_EventId + "_unknown"
                            @Summary = "WWP Leos FTransfer Completion Status Is unknown : " + $wwpLeosFTransferNotificationStatus + ", Info : " + $wwpLeosFTransferNotificationInfo
                            $DEFAULT_Severity = 2
                            $DEFAULT_Type = 1
                            $DEFAULT_ExpireTime = 0
            }

            @Summary = @Summary + " ( " + @AlertKey + " )"
            update(@Summary)
            update(@Severity)

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
            $wwpLeosFTransferNotificationStatus = $wwpLeosFTransferNotificationStatus + " ( " + $3 + " )"

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($wwpLeosFTransferRemoteFilename,$wwpLeosFTransferLocalFilename,$wwpLeosFTransferNotificationStatus,$wwpLeosFTransferNotificationInfo)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "wwpLeosFTransferRemoteFilename", $wwpLeosFTransferRemoteFilename, "wwpLeosFTransferLocalFilename", $wwpLeosFTransferLocalFilename, "wwpLeosFTransferNotificationStatus", $wwpLeosFTransferNotificationStatus,
                 "wwpLeosFTransferNotificationInfo", $wwpLeosFTransferNotificationInfo)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_ciena, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, ciena-WWP-LEOS-FILE-TRANSFER-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, ciena-WWP-LEOS-FILE-TRANSFER-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-WWP-LEOS-FILE-TRANSFER-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/ciena/ciena-WWP-LEOS-FILE-TRANSFER-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... ciena-WWP-LEOS-FILE-TRANSFER-MIB.include.snmptrap.rules >>>>>")
