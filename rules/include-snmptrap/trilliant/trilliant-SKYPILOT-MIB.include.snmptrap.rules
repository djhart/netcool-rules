###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  SKYPILOT-MIB
#
###############################################################################

case ".1.3.6.1.4.1.15319.5": ###  - Notifications from SKYPILOT-MIB (200902060000Z)

    log(DEBUG, "<<<<< Entering... trilliant-SKYPILOT-MIB.include.snmptrap.rules >>>>>")

    @Agent = "SKYPILOT-MIB"
    @Class = "87010"

    $OPTION_TypeFieldUsage = "3.6"

    switch($specific-trap)
    {
        case "1": ### spTrapReboot

            ##########
            # $1 = spNodeId 
            # $2 = spNodeType 
            ##########

            $spNodeId = upper($1_hex)
            $spNodeType = lookup($2,MeshNodeType)

            $OS_EventId = "SNMPTRAP-trilliant-SKYPILOT-MIB-spTrapReboot"

            @AlertGroup = "SkyGateway/SkyExtender Reboot Status"
            @AlertKey = $spNodeId
            @Summary = "SkyGateway/SkyExtender Rebooted" + " ( "+ @AlertKey + " ) "

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
			
            $spNodeType = $spNodeType + " ( " + $2 + " ) "
			
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_trilliant, "1")) {
                details($spNodeId,$spNodeType)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "spNodeId", $spNodeId, "spNodeType", $spNodeType)

        case "2": ### spTrapChangeMeshGw

            ##########
            # $1 = spNodeId 
            # $2 = spNodeType 
            # $3 = spDefaultMeshGateway 
            ##########

            $spNodeId = upper($1_hex)
            $spNodeType = lookup($2,MeshNodeType)
            $spDefaultMeshGateway = $3

            $OS_EventId = "SNMPTRAP-trilliant-SKYPILOT-MIB-spTrapChangeMeshGw"

            @AlertGroup = "SkyConnector/SkyExtender Re-Routes Status"
            @AlertKey = $spNodeId
            @Summary = "SkyConnector/SkyExtender Re-Routes a Path to a Different SkyGateway" + " ( "+ @AlertKey + " ) "

            $DEFAULT_Severity = 5
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
			
            $spNodeType = $spNodeType + " ( " + $2 + " ) "
			
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_trilliant, "1")) {
                details($spNodeId,$spNodeType,$spDefaultMeshGateway)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "spNodeId", $spNodeId, "spNodeType", $spNodeType, "spDefaultMeshGateway", $spDefaultMeshGateway)

        case "3": ### spTrapModulationChangeDown

            ##########
            # $1 = spNodeId 
            # $2 = spNodeType 
            # $3 = spMeshLinkType 
            # $4 = spMeshLinkState 
            # $5 = spMeshLinkNodeId 
            # $6 = spMeshLinkNodeType 
            # $7 = spMeshLinkLocalModulationCurr 
            ##########

            $spNodeId = upper($1_hex)
            $spNodeType = lookup($2,MeshNodeType)
            $spMeshLinkType = lookup($3,MeshLinkType)
            $spMeshLinkState = lookup($4,MeshLinkState)
            $spMeshLinkNodeId = $5
            $spMeshLinkNodeType = lookup($6,MeshNodeType)
            $spMeshLinkLocalModulationCurr = $7
	    
            $spMeshLinkIndex = extract($OID3, "\.([0-9]+)$")

            $OS_EventId = "SNMPTRAP-trilliant-SKYPILOT-MIB-spTrapModulationChangeDown"

            @AlertGroup = "Active-Routed Link Modulation Rate Status"
            @AlertKey = "spMeshLinkEntry." + $spMeshLinkIndex 
            @Summary = "Modulation Rate For An Active-Routed Link Descreases" + " ( "+ @AlertKey + " ) "

            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
			
            $spNodeType = $spNodeType + " ( " + $2 + " ) "
            $spMeshLinkType = $spMeshLinkType + " ( " + $3 + " ) "
            $spMeshLinkState = $spMeshLinkState + " ( " + $4 + " ) "
            $spMeshLinkNodeType = $spMeshLinkNodeType + " ( " + $6 + " ) "
			
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_trilliant, "1")) {
                details($spNodeId,$spNodeType,$spMeshLinkType,$spMeshLinkState,$spMeshLinkNodeId,$spMeshLinkNodeType,$spMeshLinkLocalModulationCurr,$spMeshLinkIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "spNodeId", $spNodeId, "spNodeType", $spNodeType, "spMeshLinkType", $spMeshLinkType,
                 "spMeshLinkState", $spMeshLinkState, "spMeshLinkNodeId", $spMeshLinkNodeId, "spMeshLinkNodeType", $spMeshLinkNodeType,
                 "spMeshLinkLocalModulationCurr", $spMeshLinkLocalModulationCurr, "spMeshLinkIndex", $spMeshLinkIndex)

        case "4": ### spTrapModulationChangeUp

            ##########
            # $1 = spNodeId 
            # $2 = spNodeType 
            # $3 = spMeshLinkType 
            # $4 = spMeshLinkState 
            # $5 = spMeshLinkNodeId 
            # $6 = spMeshLinkNodeType 
            # $7 = spMeshLinkLocalModulationCurr 
            ##########

            $spNodeId = upper($1_hex)
            $spNodeType = lookup($2,MeshNodeType)
            $spMeshLinkType = lookup($3,MeshLinkType)
            $spMeshLinkState = lookup($4,MeshLinkState)
            $spMeshLinkNodeId = $5
            $spMeshLinkNodeType = lookup($6,MeshNodeType)
            $spMeshLinkLocalModulationCurr = $7
	    
            $spMeshLinkIndex = extract($OID3, "\.([0-9]+)$")
			
            $OS_EventId = "SNMPTRAP-trilliant-SKYPILOT-MIB-spTrapModulationChangeUp"

            @AlertGroup = "Active-Routed Link Modulation Rate Status"
            @AlertKey = "spMeshLinkEntry." + $spMeshLinkIndex 
            @Summary = "Modulation Rate For An Active-Routed Link Increases" + " ( "+ @AlertKey + " ) "

            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
			
            $spNodeType = $spNodeType + " ( " + $2 + " ) "
            $spMeshLinkType = $spMeshLinkType + " ( " + $3 + " ) "
            $spMeshLinkState = $spMeshLinkState + " ( " + $4 + " ) "
            $spMeshLinkNodeType = $spMeshLinkNodeType + " ( " + $6 + " ) "
			
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_trilliant, "1")) {
                details($spNodeId,$spNodeType,$spMeshLinkType,$spMeshLinkState,$spMeshLinkNodeId,$spMeshLinkNodeType,$spMeshLinkLocalModulationCurr,$spMeshLinkIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "spNodeId", $spNodeId, "spNodeType", $spNodeType, "spMeshLinkType", $spMeshLinkType,
                 "spMeshLinkState", $spMeshLinkState, "spMeshLinkNodeId", $spMeshLinkNodeId, "spMeshLinkNodeType", $spMeshLinkNodeType,
                 "spMeshLinkLocalModulationCurr", $spMeshLinkLocalModulationCurr, "spMeshLinkIndex", $spMeshLinkIndex)

        case "5": ### spTrapLinkDown

            ##########
            # $1 = spNodeId 
            # $2 = spNodeType 
            # $3 = spMeshLinkType 
            # $4 = spMeshLinkState 
            # $5 = spMeshLinkNodeId 
            # $6 = spMeshLinkNodeType 
            ##########

            $spNodeId = upper($1_hex)
            $spNodeType = lookup($2,MeshNodeType)
            $spMeshLinkType = lookup($3,MeshLinkType)
            $spMeshLinkState = lookup($4,MeshLinkState)
            $spMeshLinkNodeId = $5
            $spMeshLinkNodeType = lookup($6,MeshNodeType)
	    
            $spMeshLinkIndex = extract($OID3, "\.([0-9]+)$")
			
            $OS_EventId = "SNMPTRAP-trilliant-SKYPILOT-MIB-spTrapLinkDown"

            @AlertGroup = "Link Status"
            @AlertKey = "spMeshLinkEntry." + $spMeshLinkIndex 
            @Summary = "Link State is No Longer Active-Management" + " ( "+ @AlertKey + " ) "

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
			
            $spNodeType = $spNodeType + " ( " + $2 + " ) "
            $spMeshLinkType = $spMeshLinkType + " ( " + $3 + " ) "
            $spMeshLinkState = $spMeshLinkState + " ( " + $4 + " ) "
            $spMeshLinkNodeType = $spMeshLinkNodeType + " ( " + $6 + " ) "
			
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_trilliant, "1")) {
                details($spNodeId,$spNodeType,$spMeshLinkType,$spMeshLinkState,$spMeshLinkNodeId,$spMeshLinkNodeType,$spMeshLinkIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "spNodeId", $spNodeId, "spNodeType", $spNodeType, "spMeshLinkType", $spMeshLinkType,
                 "spMeshLinkState", $spMeshLinkState, "spMeshLinkNodeId", $spMeshLinkNodeId, "spMeshLinkNodeType", $spMeshLinkNodeType,
                 "spMeshLinkIndex", $spMeshLinkIndex)

        case "6": ### spTrapLinkUp

            ##########
            # $1 = spNodeId 
            # $2 = spNodeType 
            # $3 = spMeshLinkType 
            # $4 = spMeshLinkState 
            # $5 = spMeshLinkNodeId 
            # $6 = spMeshLinkNodeType 
            ##########

            $spNodeId = upper($1_hex)
            $spNodeType = lookup($2,MeshNodeType)
            $spMeshLinkType = lookup($3,MeshLinkType)
            $spMeshLinkState = lookup($4,MeshLinkState)
            $spMeshLinkNodeId = $5
            $spMeshLinkNodeType = lookup($6,MeshNodeType)
	    
            $spMeshLinkIndex = extract($OID3, "\.([0-9]+)$")
			
            $OS_EventId = "SNMPTRAP-trilliant-SKYPILOT-MIB-spTrapLinkUp"

            @AlertGroup = "Link Status"
            @AlertKey = "spMeshLinkEntry." + $spMeshLinkIndex 
            @Summary = "Link State Becomes Active-Management" + " ( "+ @AlertKey + " ) "

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap
			
            $spNodeType = $spNodeType + " ( " + $2 + " ) "
            $spMeshLinkType = $spMeshLinkType + " ( " + $3 + " ) "
            $spMeshLinkState = $spMeshLinkState + " ( " + $4 + " ) "
            $spMeshLinkNodeType = $spMeshLinkNodeType + " ( " + $6 + " ) "
			
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_trilliant, "1")) {
                details($spNodeId,$spNodeType,$spMeshLinkType,$spMeshLinkState,$spMeshLinkNodeId,$spMeshLinkNodeType,$spMeshLinkIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "spNodeId", $spNodeId, "spNodeType", $spNodeType, "spMeshLinkType", $spMeshLinkType,
                 "spMeshLinkState", $spMeshLinkState, "spMeshLinkNodeId", $spMeshLinkNodeId, "spMeshLinkNodeType", $spMeshLinkNodeType,
                 "spMeshLinkIndex", $spMeshLinkIndex)

        case "7": ### spTrapMaxRegisteredNodesExceeded

            ##########
            # $1 = spNodeId 
            ##########

            $spNodeId = upper($1_hex)

            $OS_EventId = "SNMPTRAP-trilliant-SKYPILOT-MIB-spTrapMaxRegisteredNodesExceeded"

            @AlertGroup = "Registered Nodes Status"
            @AlertKey = $spNodeId
            @Summary = "Maximum Number of Registered Nodes has been Exceeded" + " ( "+ @AlertKey + " ) "

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_trilliant, "1")) {
                details($spNodeId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "spNodeId", $spNodeId)

        case "8": ### spTrapRadarDetected

            ##########
            # $1 = spNodeId 
            ##########

            $spNodeId = upper($1_hex)

            $OS_EventId = "SNMPTRAP-trilliant-SKYPILOT-MIB-spTrapRadarDetected"

            @AlertGroup = "Radar Signal Status"
            @AlertKey = $spNodeId
            @Summary = "Radar Signal Detected" + " ( "+ @AlertKey + " ) "

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_trilliant, "1")) {
                details($spNodeId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "spNodeId", $spNodeId)

        case "9": ### spTrapColdStart

            ##########
            # $1 = spNodeId 
            # $2 = spSysDate 
            # $3 = spSysUptime 
            ##########

            $spNodeId = upper($1_hex)
            $spSysDate = $2
            $spSysUptime = $3

            $OS_EventId = "SNMPTRAP-trilliant-SKYPILOT-MIB-spTrapColdStart"

            @AlertGroup = "Node Online Status"
            @AlertKey = $spNodeId
            @Summary = "Node Comes Back Online After Having Been Power Cycled" + " ( "+ @AlertKey + " ) "

            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_trilliant, "1")) {
                details($spNodeId,$spSysDate,$spSysUptime)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "spNodeId", $spNodeId, "spSysDate", $spSysDate, "spSysUptime", $spSysUptime)

        case "10": ### spTrapWarmStart

            ##########
            # $1 = spNodeId 
            # $2 = spSysDate 
            # $3 = spSysUptime 
            ##########

            $spNodeId = upper($1_hex)
            $spSysDate = $2
            $spSysUptime = $3

            $OS_EventId = "SNMPTRAP-trilliant-SKYPILOT-MIB-spTrapWarmStart"

            @AlertGroup = "Node Online Status"
            @AlertKey = $spNodeId
            @Summary = "Node Comes Back Online After Having Been Rebooted Via Software Controls" + " ( "+ @AlertKey + " ) "

            $DEFAULT_Severity = 5
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_trilliant, "1")) {
                details($spNodeId,$spSysDate,$spSysUptime)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "spNodeId", $spNodeId, "spSysDate", $spSysDate, "spSysUptime", $spSysUptime)

        case "11": ### spTrapInvalidSoftwareSchedule

            ##########
            # $1 = spNodeId 
            ##########

            $spNodeId = upper($1_hex)

            $OS_EventId = "SNMPTRAP-trilliant-SKYPILOT-MIB-spTrapInvalidSoftwareSchedule"

            @AlertGroup = "Configuration File Status"
            @AlertKey = $spNodeId
            @Summary = "Configuration File Contains an Invalid Software Schedule Date" + " ( "+ @AlertKey + " ) "

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_trilliant, "1")) {
                details($spNodeId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "spNodeId", $spNodeId)

        case "12": ### spTrapUnsynchronizedSysTime

            ##########
            # $1 = spNodeId 
            # $2 = spSysDate 
            ##########

            $spNodeId = upper($1_hex)
            $spSysDate = $2

            $OS_EventId = "SNMPTRAP-trilliant-SKYPILOT-MIB-spTrapUnsynchronizedSysTime"

            @AlertGroup = "Node System Time-External Time Synchronization Status"
            @AlertKey = $spNodeId
            @Summary = "Node is not Synchronize External Time with an External Time Source" + " ( "+ @AlertKey + " ) "

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_trilliant, "1")) {
                details($spNodeId,$spSysDate)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "spNodeId", $spNodeId, "spSysDate", $spSysDate)

        case "13": ### spTrapIpConflict

            ##########
            # $1 = spNodeId 
            # $2 = spSysDate 
            # $3 = spMac2IpMacAddress 
            # $4 = spMac2IpMacAddress 
            # $5 = spMac2IpIpAddress 
            ##########

            $spNodeId = upper($1_hex)
            $spSysDate = $2
            $spMac2IpMacAddress = $3
            $spMac2IpMacAddress = $4
            $spMac2IpIpAddress = $5
	    
            $spMac2IpIndex = extract($OID2, "\.([0-9]+)$")
			
            $OS_EventId = "SNMPTRAP-trilliant-SKYPILOT-MIB-spTrapIpConflict"

            @AlertGroup = "Registration Message Status"
            @AlertKey = "spMac2IpEntry." + $spMac2IpIndex
            @Summary = "Registration Message Contain a Conflict IP" + " ( "+ @AlertKey + " ) "

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_trilliant, "1")) {
                details($spNodeId,$spSysDate,$spMac2IpMacAddress,$spMac2IpMacAddress,$spMac2IpIpAddress,$spMac2IpIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "spNodeId", $spNodeId, "spSysDate", $spSysDate, "spMac2IpMacAddress", $spMac2IpMacAddress,
                 "spMac2IpMacAddress", $spMac2IpMacAddress, "spMac2IpIpAddress", $spMac2IpIpAddress, "spMac2IpIndex", $spMac2IpIndex)

        case "14": ### spTrapGpsUnavailable

            ##########
            # $1 = spNodeId 
            # $2 = spNodeType 
            ##########

            $spNodeId = upper($1_hex)
            $spNodeType = lookup($2,MeshNodeType)

            $OS_EventId = "SNMPTRAP-trilliant-SKYPILOT-MIB-spTrapGpsUnavailable"

            @AlertGroup = "GPS Signal Status"
            @AlertKey = $spNodeId
            @Summary = "GPS Signal is Unavailable, Node May Be Rebooted to Prevent Loss of Synch" + " ( "+ @AlertKey + " ) "

            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            $spNodeType = $spNodeType + " ( " + $2 + " ) "
	    
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_trilliant, "1")) {
                details($spNodeId,$spNodeType)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "spNodeId", $spNodeId, "spNodeType", $spNodeType)

        case "15": ### spTrapSameChannelDetected

            ##########
            # $1 = spNodeId 
            ##########

            $spNodeId = upper($1_hex)

            $OS_EventId = "SNMPTRAP-trilliant-SKYPILOT-MIB-spTrapSameChannelDetected"

            @AlertGroup = "Equipment Operating Status"
            @AlertKey = $spNodeId
            @Summary = "Equipment Operating On The Same Channel But Using Different Domain" + " ( "+ @AlertKey + " ) "

            $DEFAULT_Severity = 3
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_trilliant, "1")) {
                details($spNodeId)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "spNodeId", $spNodeId)

        case "16": ### spTrapNonPreferredParent

            ##########
            # $1 = spNodeId 
            # $2 = spMeshLinkNodeId 
            ##########

            $spNodeId = upper($1_hex)
            $spMeshLinkNodeId = $2
	    
            $spMeshLinkIndex = extract($OID2, "\.([0-9]+)$")
			
            $OS_EventId = "SNMPTRAP-trilliant-SKYPILOT-MIB-spTrapNonPreferredParent"

            @AlertGroup = "Active-Path Link Status"
            @AlertKey = "spMeshLinkEntry." + $spMeshLinkIndex 
            @Summary = "Node Forms An Active-Path Link With A Non-Preferred Parent" + " ( "+ @AlertKey + " ) "

            $DEFAULT_Severity = 2
            $DEFAULT_Type = 13
            $DEFAULT_ExpireTime = 1800

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_trilliant, "1")) {
                details($spNodeId,$spMeshLinkNodeId,$spMeshLinkIndex)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "spNodeId", $spNodeId, "spMeshLinkNodeId", $spMeshLinkNodeId, "spMeshLinkIndex", $spMeshLinkIndex)

        case "17": ### spTrapBackhaulDown

            ##########
            # $1 = spNodeId 
            # $2 = spIpAddr 
            # $3 = spIpAddr 
            ##########

            $spNodeId = upper($1_hex)
            $spIpAddr = $2
            $spIpAddr = $3

            $OS_EventId = "SNMPTRAP-trilliant-SKYPILOT-MIB-spTrapBackhaulDown"

            @AlertGroup = "ICMP Ping Respond Status"
            @AlertKey = $spNodeId
            @Summary = "ICMP Ping Responses Not Received From User Specified IP Addresses" + " ( "+ @AlertKey + " ) " 

            $DEFAULT_Severity = 4
            $DEFAULT_Type = 1
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_trilliant, "1")) {
                details($spNodeId,$spIpAddr,$spIpAddr)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "spNodeId", $spNodeId, "spIpAddr", $spIpAddr, "spIpAddr", $spIpAddr)

        case "18": ### spTrapBackhaulUp

            ##########
            # $1 = spNodeId 
            # $2 = spIpAddr 
            # $3 = spIpAddr 
            ##########

            $spNodeId = upper($1_hex)
            $spIpAddr = $2
            $spIpAddr = $3

            $OS_EventId = "SNMPTRAP-trilliant-SKYPILOT-MIB-spTrapBackhaulUp"

            @AlertGroup = "ICMP Ping Respond Status"
            @AlertKey = $spNodeId
            @Summary = "ICMP Ping Responses Starts To Receive From User Specified IP Addresses" + " ( "+ @AlertKey + " ) " 

            $DEFAULT_Severity = 1
            $DEFAULT_Type = 2
            $DEFAULT_ExpireTime = 0

            @Identifier = @Node + " " + @AlertKey + " " + @AlertGroup + " " + $DEFAULT_Type + " " + @Agent + " " + @Manager + " " + $specific-trap

            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_trilliant, "1")) {
                details($spNodeId,$spIpAddr,$spIpAddr)
            }
            @ExtendedAttr = nvp_add(@ExtendedAttr, "spNodeId", $spNodeId, "spIpAddr", $spIpAddr, "spIpAddr", $spIpAddr)

        default:

            @Summary = "Unknown Specific Trap Number (" + $specific-trap + ") Received for Enterprise " + $enterprise
            @Severity = 1
            @Identifier = @Node + " " + @Agent + " " + @Manager + " " + $enterprise + " " + $generic-trap + " " + $specific-trap
            if(match($OPTION_EnableDetails, "1") or match($OPTION_EnableDetails_trilliant, "1")) {
                details($*)
            }
            @ExtendedAttr = nvp_add($*)
    }

##########
# Handle Severity via Lookup.
##########

if(exists($SEV_KEY))
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($SEV_KEY, trilliant-SKYPILOT-MIB_sev)
}
else
{
    [$OS_Severity,$OS_Type,$OS_ExpireTime] = lookup($OS_EventId, trilliant-SKYPILOT-MIB_sev)
}
include "$NC_RULES_HOME/include-common/AssignSev.include.common.rules"

##########
# End of Severity via Lookup.
##########

##########
# Enter "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-snmptrap/trilliant/trilliant-SKYPILOT-MIB.adv.include.snmptrap.rules"
include "$NC_RULES_HOME/include-snmptrap/trilliant/trilliant-SKYPILOT-MIB.user.include.snmptrap.rules"

##########
# End of "Advanced" and "User" includes.
##########

include "$NC_RULES_HOME/include-common/load_include.rules"

log(DEBUG, "<<<<< Leaving... trilliant-SKYPILOT-MIB.include.snmptrap.rules >>>>>")
