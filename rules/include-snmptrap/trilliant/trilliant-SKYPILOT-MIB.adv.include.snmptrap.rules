###############################################################################
#
#    Licensed Materials - Property of IBM
#    "Restricted Materials of IBM"
#
#    5724-S44
#
#    (C) Copyright IBM Corp. 2005, 2015
#
#    Netcool Knowledge Library
#
###############################################################################
###############################################################################
#
# 1.0 - Initial Release.
#
#        Compatible with:
#
#          -  Supported IBM Tivoli Netcool/OMNIBus versions.
#
#          -  SKYPILOT-MIB
#
###############################################################################

log(DEBUG, "<<<<< Entering... trilliant-SKYPILOT-MIB.adv.include.snmptrap.rules >>>>>")

switch($specific-trap)
{
    case "1": ### spTrapReboot

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "spTrapReboot"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = $spNodeId
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "2": ### spTrapChangeMeshGw

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "spTrapChangeMeshGw"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = $spNodeId
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "3": ### spTrapModulationChangeDown

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "spTrapModulationChangeDown"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "spMeshLinkEntry." + $spMeshLinkIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "4": ### spTrapModulationChangeUp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "spTrapModulationChangeUp"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "spMeshLinkEntry." + $spMeshLinkIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "5": ### spTrapLinkDown

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "spTrapLinkDown"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "spMeshLinkEntry." + $spMeshLinkIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "6": ### spTrapLinkUp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "spTrapLinkUp"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "spMeshLinkEntry." + $spMeshLinkIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "7": ### spTrapMaxRegisteredNodesExceeded

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "spTrapMaxRegisteredNodesExceeded"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = $spNodeId
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "8": ### spTrapRadarDetected

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "spTrapRadarDetected"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = $spNodeId 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "9": ### spTrapColdStart

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "spTrapColdStart"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = $spNodeId
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "10": ### spTrapWarmStart

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "spTrapWarmStart"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = $spNodeId
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "11": ### spTrapInvalidSoftwareSchedule

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "spTrapInvalidSoftwareSchedule"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = $spNodeId 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "12": ### spTrapUnsynchronizedSysTime

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "spTrapUnsynchronizedSysTime"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = $spNodeId
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "13": ### spTrapIpConflict

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "spTrapIpConflict"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "spMac2IpEntry." + $spMac2IpIndex
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "14": ### spTrapGpsUnavailable

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "spTrapGpsUnavailable"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = $spNodeId 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "15": ### spTrapSameChannelDetected

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "spTrapSameChannelDetected"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = $spNodeId
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "16": ### spTrapNonPreferredParent

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "spTrapNonPreferredParent"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = "spMeshLinkEntry." + $spMeshLinkIndex 
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "17": ### spTrapBackhaulDown

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "spTrapBackhaulDown"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = $spNodeId
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    case "18": ### spTrapBackhaulUp

        $OS_X733EventType = 1
        $OS_X733ProbableCause = 1008
        $OS_X733SpecificProb = "spTrapBackhaulUp"
        $OS_OsiLayer = 0

        $OS_LocalPriObj = $spNodeId
        $OS_LocalRootObj = $OS_LocalPriObj
        $VAR_RelateLRO2LPO = 1
        $VAR_RelateLRO2LSO = 0
        $VAR_RelateLSO2LPO = 0

    default:
}

log(DEBUG, "<<<<< Leaving... trilliant-SKYPILOT-MIB.adv.include.snmptrap.rules >>>>>")
